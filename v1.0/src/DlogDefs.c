/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Dialog templates
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <Toolbox/Dialog.h>
#include <Toolbox/Utility.h>

#include "Draw.h"

/*
 *	External variables
 */

extern TextChar	strCancel[];

/*
	Repeated and general purpose items
*/

static TextChar	strOK[]			= "OK";
static TextChar	strYes[]		= "Yes";
static TextChar strUse[]		= "Use";
static TextChar	strNo[]			= "No";
static TextChar strDone[]		= "Done";
static TextChar	strAvailMem[]	= "Available memory:";
static TextChar	graphicsText[]	= "Graphics";
static TextChar	strExpansion[]	= "Expansion";

/*
	Init info
*/

static GadgetTemplate initGadgets[] = {
	{ GADG_STAT_TEXT | GADG_NO_REPORT,  20, 16, 0, 0, 0, 0, 0, 0, "DesignWorks" },
	{ GADG_STAT_TEXT | GADG_NO_REPORT, 108, 10, 0, 0, 0, 0, 0, 0, "tm" },
	{ GADG_STAT_TEXT | GADG_NO_REPORT,  20, 40, 0, 0, 0, 0, 0, 0, "Copyright \251 1991" },
	{ GADG_STAT_TEXT | GADG_NO_REPORT,  20, 55, 0, 0, 0, 0, 0, 0, "New Horizons Software, Inc." },
	{ GADG_STAT_TEXT | GADG_NO_REPORT,  20, 70, 0, 0, 0, 0, 0, 0, "All Rights Reserved" },
	{ GADG_ITEM_NONE }
};

static DialogTemplate initDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 250, 95, &initGadgets[0], NULL
};

/*
	Save changes
*/

static GadgetTemplate saveGadgets[] = {
	{ GADG_PUSH_BUTTON,  30, 60, 0, 0, 60, 20, 0, 0, &strYes[0] },
	{ GADG_PUSH_BUTTON, -90, 90, 0, 0, 60, 20, 0, 0, &strCancel[0] },
	{ GADG_PUSH_BUTTON,  30, 90, 0, 0, 60, 20, 0, 0, &strNo[0] },
	{ GADG_STAT_TEXT | GADG_NO_REPORT,  55, 10,  0,  0, 0, 0, 0, 0, "Save changes to" },
	{ GADG_STAT_TEXT | GADG_NO_REPORT,  55, 25,  0,  0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT | GADG_NO_REPORT,  55, 40,  0,  0, 0, 0, 0, 0, "before closing?" },
	{ GADG_STAT_STDIMAGE | GADG_NO_REPORT,  10, 10,  0,  0, 0, 0, 0, 0, (Ptr) IMAGE_CAUTION_ICON },
	{ GADG_ITEM_NONE }
};

static DialogTemplate saveDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 220, 120, &saveGadgets[0], NULL
};

/*
	Revert
*/

static GadgetTemplate revertGadgets[] = {
	{ GADG_PUSH_BUTTON,  60, 60, 0, 0, 60, 20, 0, 0, &strYes[0] },
	{ GADG_PUSH_BUTTON, 180, 60, 0, 0, 60, 20, 0, 0, &strNo[0] },
	{ GADG_STAT_TEXT | GADG_NO_REPORT,  55, 15,  0,  0, 0, 0, 0, 0, "Revert to saved version?" },
	{ GADG_STAT_STDIMAGE | GADG_NO_REPORT,  10, 10,  0,  0, 0, 0, 0, 0, (Ptr) IMAGE_CAUTION_ICON },
	{ GADG_ITEM_NONE }
};

static DialogTemplate revertDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 300, 90, &revertGadgets[0], NULL
};

/*
	Page Setup
*/

static GadgetTemplate pageGadgets[] = {
	{ GADG_PUSH_BUTTON, -80, 10, 0, 0, 60, 20, 0, 0, &strOK[0] },
	{ GADG_PUSH_BUTTON, -80, 40, 0, 0, 60, 20, 0, 0, &strCancel[0]},

	{ GADG_RADIO_BUTTON,  80, 30, 0, 0,  90, 12, 0, 0, "US Letter" },
	{ GADG_RADIO_BUTTON,  80, 50, 0, 0,  80, 12, 0, 0, "US Legal" },
	{ GADG_RADIO_BUTTON, 180, 30, 0, 0,  90, 12, 0, 0, "A4 Letter" },
	{ GADG_RADIO_BUTTON, 180, 50, 0, 0, 120, 12, 0, 0, "Wide Carriage" },
	{ GADG_RADIO_BUTTON,  80, 70, 0, 0,  80, 12, 0, 0, "Custom:" },

	{ GADG_EDIT_TEXT, 170, 90, 0, 0, 32, 11, 0, 0, NULL },

	{ GADG_EDIT_TEXT, 220, 70, 0, 0, 48, 11, 0, 0, NULL },
	{ GADG_EDIT_TEXT, 340, 70, 0, 0, 48, 11, 0, 0, NULL },

	{ GADG_ACTIVE_IMAGE, 25, 130, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_ACTIVE_IMAGE, 70, 130, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_CHECK_BOX, 180, 130, 0, 0, 190, 12, 0, 0, "Aspect Adjusted" },
	{ GADG_CHECK_BOX, 180, 145, 0, 0, 190, 12, 0, 0, "No Gaps Between Pages" },

	{ GADG_STAT_TEXT | GADG_NO_REPORT, 130,  10, 0, 0, NULL },

	{ GADG_STAT_TEXT | GADG_NO_REPORT,  20,  10, 0, 0, 0, 0, 0, 0, "Page Setup" },
	{ GADG_STAT_TEXT | GADG_NO_REPORT,  20,  30, 0, 0, 0, 0, 0, 0, "Paper" },
	{ GADG_STAT_TEXT | GADG_NO_REPORT,  28,  43, 0, 0, 0, 0, 0, 0, "size:" },
	{ GADG_STAT_TEXT | GADG_NO_REPORT, 170,  70, 0, 0, 0, 0, 0, 0, "width" },
	{ GADG_STAT_TEXT | GADG_NO_REPORT, 285,  70, 0, 0, 0, 0, 0, 0, "height" },
	{ GADG_STAT_TEXT | GADG_NO_REPORT,  20,  90, 0, 0, 0, 0, 0, 0, "Reduce or enlarge:" },
	{ GADG_STAT_TEXT | GADG_NO_REPORT, 210,  90, 0, 0, 0, 0, 0, 0, "%" },
	{ GADG_STAT_TEXT | GADG_NO_REPORT,  20, 110, 0, 0, 0, 0, 0, 0, "Orientation" },
	{ GADG_STAT_TEXT | GADG_NO_REPORT, 170, 110, 0, 0, 0, 0, 0, 0, "Options:" },

	{ GADG_ITEM_NONE }
};

static DialogTemplate pageDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 410, 170, &pageGadgets[0], NULL
};

/*
	Print
*/

static GadgetTemplate printGadgets[] = {
	{ GADG_PUSH_BUTTON,  -80,  10, 0,  0, 60, 20, 0, 0, "Print" },
	{ GADG_PUSH_BUTTON,  -80,  40, 0,  0, 60, 20, 0, 0, &strCancel[0] },

	{ GADG_RADIO_BUTTON, 120,  30, 0,  0, 50, 12, 0, 0, "High" },
	{ GADG_RADIO_BUTTON, 220,  30, 0,  0, 60, 12, 0, 0, "Normal" },

	{ GADG_RADIO_BUTTON, 120,  50, 0,  0, 40, 12, 0, 0, "All" },
	{ GADG_RADIO_BUTTON, 180,  50, 0,  0, 50, 12, 0, 0, "From" },

	{ GADG_RADIO_BUTTON, 120,  90, 0,  0, 90, 12, 0, 0, "Automatic" },
	{ GADG_RADIO_BUTTON, 220,  90, 0,  0, 90, 12, 0, 0, "Hand Feed" },

	{ GADG_RADIO_BUTTON, 120, 110, 0,  0, 50, 12, 0, 0, "4,096" },
	{ GADG_RADIO_BUTTON, 220, 110, 0,  0, 50, 12, 0, 0, "64" },
	{ GADG_RADIO_BUTTON, 320, 110, 0,  0, 50, 12, 0, 0, "8" },

	{ GADG_CHECK_BOX,    150, 160, 0,  0, 70, 12, 0, 0, "Collate" },

	{ GADG_EDIT_TEXT, 120, 70,  0, 0, 40, 11,  0, 0, NULL },
	{ GADG_EDIT_TEXT, 240, 50,  0, 0, 32, 11,  0, 0, NULL },
	{ GADG_EDIT_TEXT, 305, 50,  0, 0, 32, 11,  0, 0, NULL },

	{ GADG_ACTIVE_STDIMAGE,
		140, 135, 0, -IMAGE_ARROW_HEIGHT/2, 0, 0, IMAGE_ARROW_WIDTH, IMAGE_ARROW_HEIGHT,
		(Ptr) IMAGE_UP_ARROW },
	{ GADG_ACTIVE_STDIMAGE,
		140, 135, 0, IMAGE_ARROW_HEIGHT/2 + 1, 0, 0, IMAGE_ARROW_WIDTH, IMAGE_ARROW_HEIGHT,
		(Ptr) IMAGE_DOWN_ARROW },
	{ GADG_STAT_TEXT | GADG_NO_REPORT, 150, 135, IMAGE_ARROW_WIDTH, 0, 0, 0, 0, 0, NULL },

	{ GADG_STAT_TEXT | GADG_NO_REPORT,  90,  10, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_STAT_TEXT | GADG_NO_REPORT,  20,  10, 0, 0, 0, 0, 0, 0, "Print" },
	{ GADG_STAT_TEXT | GADG_NO_REPORT,  20,  30, 0, 0, 0, 0, 0, 0, "Quality:" },
	{ GADG_STAT_TEXT | GADG_NO_REPORT,  20,  50, 0, 0, 0, 0, 0, 0, "Pages:" },
	{ GADG_STAT_TEXT | GADG_NO_REPORT, 280,  50, 0, 0, 0, 0, 0, 0, "to" },
	{ GADG_STAT_TEXT | GADG_NO_REPORT,  20,  70, 0, 0, 0, 0, 0, 0, "Copies:" },
	{ GADG_STAT_TEXT | GADG_NO_REPORT,  20,  90, 0, 0, 0, 0, 0, 0, "Paper feed:" },
	{ GADG_STAT_TEXT | GADG_NO_REPORT,  20, 110, 0, 0, 0, 0, 0, 0, "Colors:" },
	{ GADG_STAT_TEXT | GADG_NO_REPORT,  20, 135, 0, 0, 0, 0, 0, 0, "Print density:" },
	{ GADG_STAT_TEXT | GADG_NO_REPORT,  20, 160, 0, 0, 0, 0, 0, 0, "Paper handling:" },

	{ GADG_ITEM_NONE }
};

static DialogTemplate printDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 440, 180, &printGadgets[0], NULL
};

/*
	Cancel print
*/

static GadgetTemplate cancelPrintGadgets[] = {
	{ GADG_STAT_TEXT | GADG_NO_REPORT, 60, 15, 0, 0, 0, 0, 0, 0, "Printing in progress." },
	{ GADG_PUSH_BUTTON, -80, 50, 0, 0, 60, 20, 0, 0, &strCancel[0] },
	{ GADG_STAT_TEXT | GADG_NO_REPORT, 60, 30, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_STDIMAGE | GADG_NO_REPORT, 10, 10, 0, 0, 0, 0, 0, 0, (Ptr) IMAGE_NOTE_ICON },
	{ GADG_ITEM_NONE }
};

static DialogTemplate cancelPrintDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 300, 80, &cancelPrintGadgets[0], NULL
};

/*
	Next Page
*/

static GadgetTemplate nextPageGadgets[] = {
	{ GADG_PUSH_BUTTON, 290, 10, 0, 0, 60, 20, 0, 0, &strOK[0] },
	{ GADG_PUSH_BUTTON, 290, 40, 0, 0, 60, 20, 0, 0, &strCancel[0] },
	{ GADG_STAT_TEXT | GADG_NO_REPORT, 60, 15, 0, 0, 0, 0, 0, 0, "Please insert the next" },
	{ GADG_STAT_TEXT | GADG_NO_REPORT, 60, 35, 0, 0, 0, 0, 0, 0, "sheet of paper." },
	{ GADG_STAT_STDIMAGE | GADG_NO_REPORT, 10, 15, 0, 0, 0, 0, 0, 0, (Ptr) IMAGE_STOP_ICON },
	{ GADG_ITEM_NONE }
};

static DialogTemplate nextPageDialog = {
	DLG_TYPE_ALERT, 0, -1, 0, 370, 70, &nextPageGadgets[0], NULL
};

/*
	Save Prefs
*/

static GadgetTemplate savePrefsGadgets[] = {
	{ GADG_PUSH_BUTTON,  60, 60, 0, 0, 60, 20, 0, 0, &strYes[0] },
	{ GADG_PUSH_BUTTON, 180, 60, 0, 0, 60, 20, 0, 0, &strNo[0] },
	{ GADG_STAT_TEXT | GADG_NO_REPORT, 55, 15, 0, 0, 0, 0, 0, 0, "Save current settings" },
	{ GADG_STAT_TEXT | GADG_NO_REPORT, 55, 30, 0, 0, 0, 0, 0, 0, "as default values?" },
	{ GADG_STAT_STDIMAGE | GADG_NO_REPORT, 10, 10, 0, 0, 0, 0, 0, 0, (Ptr) IMAGE_CAUTION_ICON },
	{ GADG_ITEM_NONE }
};

static DialogTemplate savePrefsDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 300, 90, &savePrefsGadgets[0], NULL
};

/*
	Scale
*/

static GadgetTemplate scaleGadgets[] = {
	{ GADG_PUSH_BUTTON, -80, 10, 0, 0, 60, 20, 0, 0, "Scale" },
	{ GADG_PUSH_BUTTON, -80, 40, 0, 0, 60, 20, 0, 0, &strCancel[0] },

	{ GADG_EDIT_TEXT, 80, 30, 0, 0, 40, 11, 0, 0, NULL },
	{ GADG_EDIT_TEXT, 80, 50, 0, 0, 40, 11, 0, 0, NULL },

	{ GADG_STAT_TEXT | GADG_NO_REPORT,  20, 10, 0, 0, 0, 0, 0, 0, "Scale Objects" },
	{ GADG_STAT_TEXT | GADG_NO_REPORT,  20, 30, 0, 0, 0, 0, 0, 0, "Width" },
	{ GADG_STAT_TEXT | GADG_NO_REPORT,  20, 50, 0, 0, 0, 0, 0, 0, "Height" },
	{ GADG_STAT_TEXT | GADG_NO_REPORT, 130, 30, 0, 0, 0, 0, 0, 0, "%" },
	{ GADG_STAT_TEXT | GADG_NO_REPORT, 130, 50, 0, 0, 0, 0, 0, 0, "%" },
	{ GADG_ITEM_NONE }
};

static DialogTemplate scaleDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 240, 70, &scaleGadgets[0], NULL
};

/*
	Font
*/

#define FONT_WIDTH	(19*8+18)
#define FONT_NUM	7
#define SIZE_NUM	(FONT_NUM - 2)

static GadgetTemplate fontGadgets[] = {
	{ GADG_PUSH_BUTTON, -80, 30 + FONT_NUM*11 - 50, 0, FONT_NUM, 60, 20, 0, 0, &strUse[0] },
	{ GADG_PUSH_BUTTON, -80, 30 + FONT_NUM*11 - 20, 0, FONT_NUM, 60, 20, 0, 0, &strDone[0] },

	{ GADG_ACTIVE_BORDER,
		20, 30, 0, 0,
		FONT_WIDTH, FONT_NUM*11, -IMAGE_ARROW_WIDTH - 1, FONT_NUM,
		NULL },
	{ GADG_ACTIVE_STDIMAGE,
		20 + FONT_WIDTH, 30 + FONT_NUM*11, -IMAGE_ARROW_WIDTH, FONT_NUM - 2*IMAGE_ARROW_HEIGHT,
		0, 0, IMAGE_ARROW_WIDTH, IMAGE_ARROW_HEIGHT,
		(Ptr) IMAGE_UP_ARROW },
	{ GADG_ACTIVE_STDIMAGE,
		20 + FONT_WIDTH, 30 + FONT_NUM*11, -IMAGE_ARROW_WIDTH, FONT_NUM - IMAGE_ARROW_HEIGHT,
		0, 0, IMAGE_ARROW_WIDTH, IMAGE_ARROW_HEIGHT,
		(Ptr) IMAGE_DOWN_ARROW },
	{ GADG_PROP_VERT,
		20 + FONT_WIDTH, 30, -IMAGE_ARROW_WIDTH, 0,
		0, FONT_NUM*11, IMAGE_ARROW_WIDTH, FONT_NUM - 2*IMAGE_ARROW_HEIGHT,
		NULL },

	{ GADG_ACTIVE_BORDER,
		30 + FONT_WIDTH, 30, 0, 0,
		50, SIZE_NUM*11, -IMAGE_ARROW_WIDTH - 1, SIZE_NUM,
		NULL },
	{ GADG_ACTIVE_STDIMAGE,
		80 + FONT_WIDTH, 30 + SIZE_NUM*11, -IMAGE_ARROW_WIDTH, SIZE_NUM - 2*IMAGE_ARROW_HEIGHT,
		0, 0, IMAGE_ARROW_WIDTH, IMAGE_ARROW_HEIGHT,
		(Ptr) IMAGE_UP_ARROW },
	{ GADG_ACTIVE_STDIMAGE,
		80 + FONT_WIDTH, 30 + SIZE_NUM*11, -IMAGE_ARROW_WIDTH, SIZE_NUM - IMAGE_ARROW_HEIGHT,
		0, 0, IMAGE_ARROW_WIDTH, IMAGE_ARROW_HEIGHT,
		(Ptr) IMAGE_DOWN_ARROW },
	{ GADG_PROP_VERT,
		80 + FONT_WIDTH, 30, -IMAGE_ARROW_WIDTH, 0,
		0, SIZE_NUM*11, IMAGE_ARROW_WIDTH, SIZE_NUM - 2*IMAGE_ARROW_HEIGHT,
		NULL },

	{ GADG_CHECK_BOX, 30, 30 + FONT_NUM*11 + 10, 0, FONT_NUM, 150, 12, 0, 0, "List In Font Menu" },

	{ GADG_STAT_TEXT | GADG_NO_REPORT, 20, 10, 0, 0, 0, 0, 0, 0, "Font name:" },
	{ GADG_STAT_TEXT | GADG_NO_REPORT, 30 + FONT_WIDTH, 10, 0, 0, 0, 0, 0, 0, "Size:" },

	{ GADG_EDIT_TEXT,
		30 + FONT_WIDTH, 30 + (FONT_NUM - 1)*11, 1, FONT_NUM,
		48, 11, 0, 0,
		NULL },

	{ GADG_ITEM_NONE }
};

static DialogTemplate fontDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 180 + FONT_WIDTH, 30 + 85 + 20 + 60, &fontGadgets[0], NULL
};

/*
	Preferences
*/

static GadgetTemplate optionsGadgets[] = {
	{ GADG_PUSH_BUTTON, -80, 10, 0, 0, 60, 20, 0, 0, &strOK[0] },
	{ GADG_PUSH_BUTTON, -80, 40, 0, 0, 60, 20, 0, 0, &strCancel[0]},
	{ GADG_PUSH_BUTTON, -80, 70, 0, 0, 60, 20, 0, 0, "Reset" },

	{ GADG_RADIO_BUTTON, 30, 45, 0, 0,  70, 12, 0, 0, "Inches" },
	{ GADG_RADIO_BUTTON, 30, 60, 0, 0, 100, 12, 0, 0, "Centimeters" },

	{ GADG_RADIO_BUTTON, 210, 45, 0, 0, 110, 12, 0, 0, "Flash Screen" },
	{ GADG_RADIO_BUTTON, 210, 60, 0, 0, 100, 12, 0, 0, "Sound Beep" },
	{ GADG_RADIO_BUTTON, 210, 75, 0, 0,  50, 12, 0, 0, "Both" },

	{ GADG_CHECK_BOX,  30, 110, 0, 0, 170, 12, 0, 0, "Auto-Close Polygons" },
	{ GADG_CHECK_BOX,  30, 125, 0, 0, 150, 12, 0, 0, "Drag As Outlines" },
	{ GADG_CHECK_BOX,  30, 140, 0, 0, 120, 12, 0, 0, "Show Pictures" },
	{ GADG_CHECK_BOX, 210, 110, 0, 0, 100, 12, 0, 0, "Save Icons" },
	{ GADG_CHECK_BOX, 210, 125, 0, 0, 200, 12, 0, 0, "Screen Color Correction" },

	{ GADG_STAT_TEXT | GADG_NO_REPORT,  20, 10, 0, 0, 0, 0, 0, 0, "Preferences" },
	{ GADG_STAT_TEXT | GADG_NO_REPORT,  20, 30, 0, 0, 0, 0, 0, 0, "Unit of measurement:" },
	{ GADG_STAT_TEXT | GADG_NO_REPORT, 200, 30, 0, 0, 0, 0, 0, 0, "Error notification:" },
	{ GADG_STAT_TEXT | GADG_NO_REPORT,  20, 95, 0, 0, 0, 0, 0, 0, "General:" },

	{ GADG_ITEM_NONE }
};

static DialogTemplate optionsDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 460, 160, &optionsGadgets[0], NULL
};

/*
	Align
*/

static GadgetTemplate alignGadgets[] = {
	{ GADG_PUSH_BUTTON, -80, 10, 0, 0, 60, 20, 0, 0, "Align" },
	{ GADG_PUSH_BUTTON, -80, 40, 0, 0, 60, 20, 0, 0, &strCancel[0]},

	{ GADG_RADIO_BUTTON, 30, 30, 0, 0, 100, 12, 0, 0, "Left edges" },
	{ GADG_RADIO_BUTTON, 30, 50, 0, 0, 160, 12, 0, 0, "Left/right centers" },
	{ GADG_RADIO_BUTTON, 30, 70, 0, 0, 110, 12, 0, 0, "Right edges" },
	{ GADG_RADIO_BUTTON, 30, 90, 0, 0,  50, 12, 0, 0, "None" },

	{ GADG_RADIO_BUTTON, 200, 30, 0, 0,  90, 12, 0, 0, "Top edges" },
	{ GADG_RADIO_BUTTON, 200, 50, 0, 0, 160, 12, 0, 0, "Top/bottom centers" },
	{ GADG_RADIO_BUTTON, 200, 70, 0, 0, 120, 12, 0, 0, "Bottom edges" },
	{ GADG_RADIO_BUTTON, 200, 90, 0, 0,  50, 12, 0, 0, "None" },

	{ GADG_STAT_TEXT | GADG_NO_REPORT, 20, 10, 0, 0, 0, 0, 0, 0, "Align objects:" },

	{ GADG_ITEM_NONE }
};

static DialogTemplate alignDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 450, 110, &alignGadgets[0], NULL
};

/*
	Drawing size
*/

static GadgetTemplate drawSizeGadgets[] = {
	{ GADG_PUSH_BUTTON,  20, -30, 0, 0, 60, 20, 0, 0, &strOK[0] },
	{ GADG_PUSH_BUTTON, 100, -30, 0, 0, 60, 20, 0, 0, &strCancel[0] },

	{ GADG_STAT_BORDER, 180, 10, 0, 0, 200, 180, 0, 0, NULL },

	{ GADG_STAT_TEXT | GADG_NO_REPORT, 100, 40, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT | GADG_NO_REPORT, 100, 60, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT | GADG_NO_REPORT, 100, 90, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_STAT_TEXT | GADG_NO_REPORT,  20, 10, 0, 0, 0, 0, 0, 0, "Drawing Size" },
	{ GADG_STAT_TEXT | GADG_NO_REPORT,  30, 40, 0, 0, 0, 0, 0, 0, "Width:" },
	{ GADG_STAT_TEXT | GADG_NO_REPORT,  30, 60, 0, 0, 0, 0, 0, 0, "Height:" },
	{ GADG_STAT_TEXT | GADG_NO_REPORT,  30, 90, 0, 0, 0, 0, 0, 0, "Pages:" },

	{ GADG_ITEM_NONE }
};

static DialogTemplate drawSizeDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 400, 200, &drawSizeGadgets[0], NULL
};

/*
	Pen size
*/

static GadgetTemplate penSizeGadgets[] = {
	{ GADG_PUSH_BUTTON,  20, 40, 0, 0, 60, 20, 0, 0, &strUse[0] },
	{ GADG_PUSH_BUTTON, 100, 40, 0, 0, 60, 20, 0, 0, &strCancel[0] },

	{ GADG_EDIT_TEXT, 120, 10, 0, 0, 40, 11, 0, 0, NULL },

	{ GADG_STAT_TEXT | GADG_NO_REPORT, 20, 10, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_ITEM_NONE }
};

static DialogTemplate penSizeDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 180, 70, &penSizeGadgets[0], NULL
};

/*
	Macro name dialog
*/

static GadgetTemplate macroNameGadgets[] = {
	{ GADG_PUSH_BUTTON,  60, -30, 0, 0, 60, 20, 0, 0, &strOK[0] },
	{ GADG_PUSH_BUTTON, 180, -30, 0, 0, 60, 20, 0, 0, &strCancel[0] },

	{ GADG_EDIT_TEXT, 80, 10, 0, 0, 200, 11, 0, 0, NULL },

	{ GADG_STAT_TEXT | GADG_NO_REPORT, 20, 10, 0, 0, 0, 0, 0, 0, "Macro:" },

	{ GADG_ITEM_NONE }
};

static DialogTemplate macroNameDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 300, 70, &macroNameGadgets[0], NULL
};

/*
	Error dialog
*/

static GadgetTemplate errorGadgets[] = {
	{ GADG_PUSH_BUTTON, 220, 50, 0, 0, 60, 20, 0, 0, &strOK[0] },
	{ GADG_STAT_TEXT | GADG_NO_REPORT, 60, 10, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT | GADG_NO_REPORT, 60, 30, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_STDIMAGE | GADG_NO_REPORT, 10, 10, 0, 0, 0, 0, 0, 0, (Ptr) IMAGE_STOP_ICON },
	{ GADG_ITEM_NONE }
};

static DialogTemplate errorDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 300, 80, &errorGadgets[0], NULL
};

/*
	Help dialog
*/

static GadgetTemplate helpGadgets[] = {
	{ GADG_ITEM_NONE }
};

static DialogTemplate helpDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 330, 135, &helpGadgets[0], NULL
};

/*
	About (and memory) dialog
*/

static GadgetTemplate aboutGadgets[] = {
	{ GADG_PUSH_BUTTON, -80, 10, 0, 0, 60, 20, 0, 0, &strOK[0] },

	{ GADG_STAT_TEXT | GADG_NO_REPORT, 150, 160, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT | GADG_NO_REPORT, 150, 175, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT | GADG_NO_REPORT,  20,  16, 0, 0, 0, 0, 0, 0, "DesignWorks" },
	{ GADG_STAT_TEXT | GADG_NO_REPORT, 108,  10, 0, 0, 0, 0, 0, 0, "tm" },
	{ GADG_STAT_TEXT | GADG_NO_REPORT,  20,  40, 0, 0, 0, 0, 0, 0, "Designed and developed" },
	{ GADG_STAT_TEXT | GADG_NO_REPORT,  40,  55, 0, 0, 0, 0, 0, 0, "by James Bayless" },
	{ GADG_STAT_TEXT | GADG_NO_REPORT,  20,  85, 0, 0, 0, 0, 0, 0, "Copyright \251 1991" },
	{ GADG_STAT_TEXT | GADG_NO_REPORT,  20, 100, 0, 0, 0, 0, 0, 0, "New Horizons Software, Inc." },
	{ GADG_STAT_TEXT | GADG_NO_REPORT,  20, 115, 0, 0, 0, 0, 0, 0, "All Rights Reserved" },
	{ GADG_STAT_TEXT | GADG_NO_REPORT,  20, 145, 0, 0, 0, 0, 0, 0, &strAvailMem[0]},
	{ GADG_STAT_TEXT | GADG_NO_REPORT,  40, 160, 0, 0, 0, 0, 0, 0, &graphicsText[0]},
	{ GADG_STAT_TEXT | GADG_NO_REPORT,  40, 175, 0, 0, 0, 0, 0, 0, &strExpansion[0]},

	{ GADG_ITEM_NONE }
};

static DialogTemplate aboutDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 260, 200, &aboutGadgets[0], NULL
};

static GadgetTemplate memoryGadgets[] = {
	{ GADG_PUSH_BUTTON, -80, 10, 0, 0, 60, 20, 0, 0, &strOK[0] },

	{ GADG_STAT_TEXT | GADG_NO_REPORT, 150, 25, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT | GADG_NO_REPORT, 150, 40, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT | GADG_NO_REPORT,  20, 10, 0, 0, 0, 0, 0, 0, &strAvailMem[0]},
	{ GADG_STAT_TEXT | GADG_NO_REPORT,  40, 25, 0, 0, 0, 0, 0, 0, &graphicsText[0]},
	{ GADG_STAT_TEXT | GADG_NO_REPORT,  40, 40, 0, 0, 0, 0, 0, 0, &strExpansion[0]},

	{ GADG_ITEM_NONE }
};

static DialogTemplate memoryDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 280, 65, &memoryGadgets[0], NULL
};

/*
	Pen colors
*/

static GadgetTemplate penColorsGadgets[] = {
	{ GADG_PUSH_BUTTON, -80, 10, 0, 0,  60, 20, 0, 0, &strOK[0] },
	{ GADG_PUSH_BUTTON, -80, 40, 0, 0,  60, 20, 0, 0, &strCancel[0] },

	{ GADG_STAT_BORDER,  20, 30, 0, 0, 160, 20, 0, 0, NULL },
	{ GADG_PUSH_BUTTON,  70, 60, 0, 0,  60, 20, 0, 0, "Change" },

	{ GADG_STAT_TEXT | GADG_NO_REPORT,  20, 10, 0, 0, 0, 0, 0, 0, "Pen Colors" },

	{ GADG_ITEM_NONE }
};

static DialogTemplate penColorsDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 280, 90, &penColorsGadgets[0], NULL
};

/*
	Screen colors
*/

static GadgetTemplate screenColorsGadgets[] = {
	{ GADG_PUSH_BUTTON, -80, 10, 0, 0,  60, 20, 0, 0, &strOK[0] },
	{ GADG_PUSH_BUTTON, -80, 40, 0, 0,  60, 20, 0, 0, &strCancel[0] },
	{ GADG_PUSH_BUTTON, -80, 70, 0, 0,  60, 20, 0, 0, "Reset" },

	{ GADG_STAT_BORDER,  20, 30, 0, 0, 160, 40, 0, 0, NULL },
	{ GADG_PUSH_BUTTON,  70, 80, 0, 0,  60, 20, 0, 0, "Change" },

	{ GADG_STAT_TEXT | GADG_NO_REPORT,  20, 10, 0, 0, 0, 0, 0, 0, "Screen Colors" },

	{ GADG_ITEM_NONE }
};

static DialogTemplate screenColorsDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 280, 110, &screenColorsGadgets[0], NULL
};

/*
	Fill patterns (superset of pen colors dialog)
*/

static GadgetTemplate fillPatternsGadgets[] = {
	{ GADG_PUSH_BUTTON,  50, -30, 0, 0,  60, 20, 0, 0, &strOK[0] },
	{ GADG_PUSH_BUTTON, 160, -30, 0, 0,  60, 20, 0, 0, &strCancel[0] },

	{ GADG_STAT_BORDER,  20, 155, 0, 0, 160, 20, 0, 0, NULL },
	{ GADG_PUSH_BUTTON, 190, 155, 0, 0,  60, 20, 0, 0, "Change" },

	{ GADG_STAT_BORDER,  35,  30, 0, 0, 200, 40, 0, 0, NULL },

	{ GADG_STAT_BORDER,  20,  80, 0, 0,  64, 64, 0, 0, NULL },
	{ GADG_STAT_BORDER, 180-64,  80, 0, 0,  64, 64, 0, 0, NULL },
	{ GADG_PUSH_BUTTON, 190,  80, 0, 0,  60, 20, 0, 0, "Clear" },
	{ GADG_PUSH_BUTTON, 190, 110, 0, 0,  60, 20, 0, 0, "Revert" },

	{ GADG_STAT_TEXT | GADG_NO_REPORT,  20, 10, 0, 0, 0, 0, 0, 0, "Fill Patterns" },

	{ GADG_ITEM_NONE }
};

static DialogTemplate fillPatternsDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 270, 215, &fillPatternsGadgets[0], NULL
};

/*
	Layers
*/

#define LAYERS_WIDTH	(19*8+18)
#define LAYERS_NUM		7

static GadgetTemplate layersGadgets[] = {
	{ GADG_PUSH_BUTTON, 30 + LAYERS_WIDTH,  60, 0, 0,  60, 20, 0, 0, "New" },

	{ GADG_ACTIVE_STDIMAGE,
		30 + LAYERS_WIDTH, 35, 0, -IMAGE_ARROW_HEIGHT/2, 0, 0, IMAGE_ARROW_WIDTH, IMAGE_ARROW_HEIGHT,
		(Ptr) IMAGE_UP_ARROW },
	{ GADG_ACTIVE_STDIMAGE,
		30 + LAYERS_WIDTH, 35, 0, IMAGE_ARROW_HEIGHT/2 + 1, 0, 0, IMAGE_ARROW_WIDTH, IMAGE_ARROW_HEIGHT,
		(Ptr) IMAGE_DOWN_ARROW },

	{ GADG_PUSH_BUTTON,  30 + LAYERS_WIDTH,  90, 0, 0,  60, 20, 0, 0, "Rename" },
	{ GADG_PUSH_BUTTON,  30 + LAYERS_WIDTH, 120, 0, 0,  60, 20, 0, 0, "Delete" },
	{ GADG_PUSH_BUTTON, 100 + LAYERS_WIDTH,  60, 0, 0,  60, 20, 0, 0, "Show" },
	{ GADG_PUSH_BUTTON, 100 + LAYERS_WIDTH,  90, 0, 0,  60, 20, 0, 0, "Hide" },
	{ GADG_PUSH_BUTTON, 100 + LAYERS_WIDTH, 120, 0, 0,  60, 20, 0, 0, &strDone[0] },

	{ GADG_ACTIVE_BORDER,
		20, 30, 0, 0,
		LAYERS_WIDTH, LAYERS_NUM*11, -IMAGE_ARROW_WIDTH - 1, LAYERS_NUM,
		NULL },
	{ GADG_ACTIVE_STDIMAGE,
		20 + LAYERS_WIDTH, 30 + LAYERS_NUM*11, -IMAGE_ARROW_WIDTH, LAYERS_NUM - 2*IMAGE_ARROW_HEIGHT,
		0, 0, IMAGE_ARROW_WIDTH, IMAGE_ARROW_HEIGHT,
		(Ptr) IMAGE_UP_ARROW },
	{ GADG_ACTIVE_STDIMAGE,
		20 + LAYERS_WIDTH, 30 + LAYERS_NUM*11, -IMAGE_ARROW_WIDTH, LAYERS_NUM - IMAGE_ARROW_HEIGHT,
		0, 0, IMAGE_ARROW_WIDTH, IMAGE_ARROW_HEIGHT,
		(Ptr) IMAGE_DOWN_ARROW },
	{ GADG_PROP_VERT,
		20 + LAYERS_WIDTH, 30, -IMAGE_ARROW_WIDTH, 0,
		0, LAYERS_NUM*11, IMAGE_ARROW_WIDTH, LAYERS_NUM - 2*IMAGE_ARROW_HEIGHT,
		NULL },

	{ GADG_EDIT_TEXT,
		20, 40 + LAYERS_NUM*11, 1, LAYERS_NUM,
		LAYERS_WIDTH, 11, -2, 0,
		NULL },

	{ GADG_STAT_TEXT | GADG_NO_REPORT,  40 + LAYERS_WIDTH, 35, IMAGE_ARROW_WIDTH, 0, 0, 0, 0, 0, "Move" },
	{ GADG_STAT_TEXT | GADG_NO_REPORT,  20, 10, 0, 0, 0, 0, 0, 0, "Layers" },

	{ GADG_ITEM_NONE }
};

static DialogTemplate layersDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 180 + LAYERS_WIDTH, 150, &layersGadgets[0], NULL
};

/*
	Delete layer dialog
*/

static GadgetTemplate deleteLayerGadgets[] = {
	{ GADG_PUSH_BUTTON,   50, 50, 0, 0, 60, 20, 0, 0, &strYes[0] },
	{ GADG_PUSH_BUTTON, -110, 50, 0, 0, 60, 20, 0, 0, &strNo[0] },
	{ GADG_STAT_TEXT | GADG_NO_REPORT, 60, 10, 0, 0, 0, 0, 0, 0, "Permanently delete layer" },
	{ GADG_STAT_TEXT | GADG_NO_REPORT, 60, 25, 0, 0, 0, 0, 0, 0, "and everything on it?" },
	{ GADG_STAT_STDIMAGE | GADG_NO_REPORT, 10, 10, 0, 0, 0, 0, 0, 0, (Ptr) IMAGE_CAUTION_ICON },
	{ GADG_ITEM_NONE }
};

static DialogTemplate deleteLayerDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 260, 80, &deleteLayerGadgets[0], NULL
};

/*
	Grid dialog
*/

static GadgetTemplate gridGadgets[] = {
	{ GADG_PUSH_BUTTON,  30, -30, 0, 0, 60, 20, 0, 0, &strUse[0] },
	{ GADG_PUSH_BUTTON, -90, -30, 0, 0, 60, 20, 0, 0, &strCancel[0] },

	{ GADG_ACTIVE_STDIMAGE,
		110, 15, 0, -IMAGE_ARROW_HEIGHT/2, 0, 0, IMAGE_ARROW_WIDTH, IMAGE_ARROW_HEIGHT,
		(Ptr) IMAGE_UP_ARROW },
	{ GADG_ACTIVE_STDIMAGE,
		110, 15, 0, IMAGE_ARROW_HEIGHT/2 + 1, 0, 0, IMAGE_ARROW_WIDTH, IMAGE_ARROW_HEIGHT,
		(Ptr) IMAGE_DOWN_ARROW },
	{ GADG_STAT_TEXT | GADG_NO_REPORT, 130, 15, IMAGE_ARROW_WIDTH, 0, 0, 0, 0, 0, NULL },

	{ GADG_STAT_TEXT | GADG_NO_REPORT, 20, 15, 0, 0, 0, 0, 0, 0, "Grid size:" },
	{ GADG_ITEM_NONE }
};

static DialogTemplate gridDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 220, 70, &gridGadgets[0], NULL
};

/*
	Dialog list
*/

DlgTemplPtr dlgList[] = {
	&initDialog,
	&saveDialog, &revertDialog, &savePrefsDialog,
	&pageDialog, &printDialog, &cancelPrintDialog, &nextPageDialog,
	&optionsDialog, &drawSizeDialog, &alignDialog,
	&fontDialog,
	&macroNameDialog,
	&errorDialog, &helpDialog, &aboutDialog, &memoryDialog,
	&penSizeDialog, &scaleDialog, &penColorsDialog, &screenColorsDialog,
	&fillPatternsDialog, &layersDialog, &deleteLayerDialog, &gridDialog
};
