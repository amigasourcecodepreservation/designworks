/*
 *	IFFW.C Support routines for writing IFF-85 files		1/23/86
 *
 *	By Jerry Morrison and Steve Shaw, Electronic Arts
 *	Modified by James Bayless, New Horizons Software
 * 
 *  	This software is in the public domain.
 *
 *	This version for the Commodore-Amiga computer
 */

#include <exec/types.h>
#include <libraries/dos.h>

#include "IFF.h"
#include "GIO.h"

#include <proto/dos.h>

/*
 *	A macro to test if a chunk size is definite, i.e. not szNotYetKnown
 */

#define Known(size)	((size)!=szNotYetKnown)

/*
 *	OpenWIFF
 */

IFFP OpenWIFF(BPTR file, register GroupContext *new, LONG limit)
{
	register IFFP iffp = IFF_OKAY;

	new->parent			= NULL;
	new->clientFrame	= NULL;
	new->file			= file;
	new->position		= 0;
	new->bound			= limit;
	new->ckHdr.ckID		= NULL_CHUNK;
	new->ckHdr.ckSize	= new->bytesSoFar = 0;
	if (Seek(file, 0, OFFSET_BEGINNING) < 0)	/* Go to start of the file */
		iffp = DOS_ERROR;
	else if (Known(limit) && IS_ODD(limit))
		iffp = CLIENT_ERROR;
	return (iffp);
}

/*
 *	StartWGroup
 */

IFFP StartWGroup(GroupContext *parent, ID groupType, LONG groupSize, ID subtype,
				 GroupContext *new)
{
	register IFFP iffp;

	iffp = PutCkHdr(parent, groupType, groupSize);
	if (iffp == IFF_OKAY)
		iffp = IFFWriteBytes(parent, &subtype, sizeof(ID));
	if (iffp == IFF_OKAY)
		iffp = OpenWGroup(parent, new);
	return (iffp);
}

/*
 *	OpenWGroup
 */

IFFP OpenWGroup(register GroupContext *parent, register GroupContext *new)
{
	register IFFP iffp = IFF_OKAY;
	register LONG ckEnd;

	new->parent			= parent;
	new->clientFrame	= parent->clientFrame;
	new->file			= parent->file;
	new->position		= parent->position;
	new->bound			= parent->bound;
	new->ckHdr.ckID		= NULL_CHUNK;
	new->ckHdr.ckSize	= new->bytesSoFar = 0;
	if (Known(parent->ckHdr.ckSize)) {
		ckEnd = new->position + ChunkMoreBytes(parent);
		if (new->bound == szNotYetKnown || new->bound > ckEnd)
			new->bound = ckEnd;
	}
	if (parent->ckHdr.ckID == NULL_CHUNK || IS_ODD(new->position) ||
		(Known(new->bound) && IS_ODD(new->bound)))
		iffp = CLIENT_ERROR;
	return (iffp);
}

/*
 *	CloseWGroup
 */

IFFP CloseWGroup(register GroupContext *old)
{
	register IFFP iffp = IFF_OKAY;

	if (old->ckHdr.ckID != NULL_CHUNK)
		iffp = CLIENT_ERROR;
	else if (old->parent == NULL) {		/* Top level file context */
		if (GWriteFlush(old->file) < 0)
			iffp = DOS_ERROR;
	}
	else {
		old->parent->bytesSoFar += old->position - old->parent->position;
		old->parent->position = old->position;
	}
	return (iffp);
}

/*
 *	EndWGroup
 */

IFFP EndWGroup(GroupContext *old)
{
	register GroupContext *parent = old->parent;
	register IFFP iffp;

	if ((iffp = CloseWGroup(old)) == IFF_OKAY)
		iffp = PutCkEnd(parent);
	return (iffp);
}

/*
 *	PutCk
 */

IFFP PutCk(GroupContext *context, ID ckID, LONG ckSize, void *data)
{
	register IFFP iffp;

	if (ckSize == szNotYetKnown)
		iffp = CLIENT_ERROR;
	else {
		iffp = PutCkHdr(context, ckID, ckSize);
		if (iffp == IFF_OKAY)
			iffp = IFFWriteBytes(context, data, ckSize);
		if (iffp == IFF_OKAY)
			iffp = PutCkEnd(context);
	}
	return (iffp);
}

/*
 *	PutCkHdr
 */

IFFP PutCkHdr(register GroupContext *context, ID ckID, LONG ckSize)
{
	LONG minPSize = sizeof(ChunkHeader);

/*
	CLIENT_ERROR if we're already inside a chunk or asked to write
	other than one FORM, LIST, or CAT at the top level of a file
	Also, non-positive ID values are illegal and used for error codes
	(We could check for other illegal IDs...)
*/
	if (context->ckHdr.ckID != NULL_CHUNK || ckID <= 0)
		return (CLIENT_ERROR);
	if (context->parent == NULL) {
		if ((ckID != FORM && ckID != LIST && ckID != CAT) || context->position != 0)
			return (CLIENT_ERROR);
	}
	if (Known(ckSize)) {
		if (ckSize < 0)
			return (CLIENT_ERROR);
		minPSize += ckSize;
	}
	if (Known(context->bound) && context->position + minPSize > context->bound)
		return (CLIENT_ERROR);
	context->ckHdr.ckID		= ckID;
	context->ckHdr.ckSize	= ckSize;
	context->bytesSoFar		= 0;
	if (GWrite(context->file, &context->ckHdr, sizeof(ChunkHeader)) < 0)
		return (DOS_ERROR);
	context->position += sizeof(ChunkHeader);
	return (IFF_OKAY);
}

/*
 *	IFFWriteBytes
 */

IFFP IFFWriteBytes(register GroupContext *context, void *data, LONG nBytes)
{
	if (context->ckHdr.ckID == NULL_CHUNK || nBytes < 0 ||
		(Known(context->bound) && context->position + nBytes > context->bound) ||
		(Known(context->ckHdr.ckSize) && context->bytesSoFar + nBytes > context->ckHdr.ckSize))
		return (CLIENT_ERROR);
	if (GWrite(context->file, data, nBytes) < 0)
		return (DOS_ERROR);
	context->bytesSoFar	+= nBytes;
	context->position	+= nBytes;
	return (IFF_OKAY);
}

/*
 *	PutCkEnd
 */

IFFP PutCkEnd(register GroupContext *context)
{
	WORD zero = 0;

	if (context->ckHdr.ckID == NULL_CHUNK)
		return (CLIENT_ERROR);
/*
	Go back and set the chunk size to bytesSoFar
*/
	if (context->ckHdr.ckSize == szNotYetKnown) {
		if (GSeek(context->file, -(context->bytesSoFar + sizeof(LONG)), OFFSET_CURRENT) < 0 ||
			GWrite(context->file, &context->bytesSoFar, sizeof(LONG)) < 0 ||
			GSeek(context->file, context->bytesSoFar, OFFSET_CURRENT) < 0)
			return (DOS_ERROR);
	}
/*
	Make sure the client wrote as many bytes as planned
*/
	else if (context->ckHdr.ckSize != context->bytesSoFar)
		return (CLIENT_ERROR);
/*
	Write a pad byte if needed to bring us up to an even boundary
	Since the context end must be even, and since we haven't overwritten the
	context, if we're on an odd position there must be room for a pad byte
*/
	if (IS_ODD(context->bytesSoFar)) {
		if (GWrite(context->file, &zero, 1) < 0)
			return (DOS_ERROR);
		context->position += 1;
	}
	context->ckHdr.ckID		= NULL_CHUNK;
	context->ckHdr.ckSize	= context->bytesSoFar = 0;
	return (IFF_OKAY);
}
