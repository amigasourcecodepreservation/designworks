/*
 *	IFFR.C  Support routines for reading IFF-85 files		1/23/86
 *	(IFF is Interchange Format File)
 *
 *	By Jerry Morrison and Steve Shaw, Electronic Arts
 *	Modified by James Bayless, New Horizons Software
 *  	This software is in the public domain.
 *	
 *	This version for the Commodore-Amiga computer
 */

#include "IFF.h"
#include "GIO.h"

#include <proto/dos.h>

/*
 *	Local prototypes
 */

LONG	FileLength(BPTR);
IFFP	SkipFwd(GroupContext *context, LONG bytes);

/*
 *	Private routine FileLength()
 *	Returns the length of the file or else a negative IFFP error code
 *	(NO_FILE or DOS_ERROR). AmigaDOS-specific implementation.
 *	SIDE EFFECT: Thanks to AmigaDOS, we have to change the file's position
 *	to find its length.
 */

static LONG FileLength(BPTR file)
{
	LONG fileLength = NO_FILE;

	if (file) {
		GSeek(file, 0, OFFSET_END);		/* Seek to end of file */
		fileLength = GSeek(file, 0, OFFSET_CURRENT);
		if (fileLength < 0)
			fileLength = DOS_ERROR;		/* DOS being absurd */
	}
	return (fileLength);
}

/*
 *	OpenRIFF
 */

IFFP OpenRIFF(register BPTR file, register GroupContext *new, ClientFrame *clientFrame)
{
	register IFFP iffp = IFF_OKAY;

	new->parent			= NULL;		/* "whole file" has no parent */
	new->clientFrame	= clientFrame;
	new->file			= file;
	new->position		= 0;
	new->ckHdr.ckID		= new->subtype = NULL_CHUNK;
	new->ckHdr.ckSize	= new->bytesSoFar = 0;
/*
	Set new->bound and go to the file's beginning
*/
	if ((new->bound = FileLength(file)) < 0)
		iffp = new->bound;					/* File system error! */
	else if (new->bound < sizeof(ChunkHeader))
		iffp = NOT_IFF;						/* Too small for an IFF file */
	else
		GSeek(file, 0, OFFSET_BEGINNING);	/* Go to file start */
	return (iffp);
}

/*
 *	OpenRGroup
 */

IFFP OpenRGroup(register GroupContext *parent, register GroupContext *new)
{
	register IFFP iffp = IFF_OKAY;

	new->parent			= parent;
	new->clientFrame	= parent->clientFrame;
	new->file			= parent->file;
	new->position		= parent->position;
	new->bound			= parent->position + ChunkMoreBytes(parent);
	new->ckHdr.ckID		= new->subtype	= NULL_CHUNK;
	new->ckHdr.ckSize	= new->bytesSoFar = 0;
	if (new->bound > parent->bound || IS_ODD(new->bound))
		iffp = BAD_IFF;
	return (iffp);
}

/*
 *	CloseRGroup
 */

IFFP CloseRGroup(register GroupContext *context)
{
	register LONG position;

	if (context->parent) {
		position = context->position;
		context->parent->bytesSoFar += position - context->parent->position;
		context->parent->position = position;
	}
	return (IFF_OKAY);
}

/*
 *	SkipFwd
 *	Skip over bytes in a context. Won't go backwards
 *	Updates context->position but not context->bytesSoFar
 *	This implementation is AmigaDOS specific
 */

static IFFP SkipFwd(GroupContext *context, LONG bytes)
{
	register IFFP iffp = IFF_OKAY;

	if (bytes > 0) {
		if (GSeek(context->file, bytes, OFFSET_CURRENT) < 0)
			iffp = BAD_IFF;		/* Ran out of bytes before chunk complete */
		else
			context->position += bytes;
	}
	return (iffp);
}

/*
 *	GetChunkHdr
 */

ID GetChunkHdr(register GroupContext *context)
{
	register IFFP iffp;
	register LONG remaining;

/*
	Skip remainder of previous chunk & padding
*/
	iffp = SkipFwd(context, ChunkMoreBytes(context) + IS_ODD(context->ckHdr.ckSize));
	CheckIFFP();
/*
	Set up to read the new header
*/
	context->ckHdr.ckID	= BAD_IFF;	/* Until we know it's okay, mark BAD */
	context->subtype	= NULL_CHUNK;
	context->bytesSoFar	= 0;
/*
	Generate a psuedo-chunk if at end-of-context
*/
	remaining = context->bound - context->position;
	if (remaining == 0) {
		context->ckHdr.ckSize	= 0;
		context->ckHdr.ckID		= END_MARK;
	}
/*
	BAD_IFF if not enough bytes in the context for a ChunkHeader
*/
	else if (remaining < sizeof(ChunkHeader))
		context->ckHdr.ckSize = remaining;
/*
	Read the chunk header (finally)
*/
	else {
		switch (GRead(context->file, &context->ckHdr, sizeof(ChunkHeader))) {
		case -1:
			return (context->ckHdr.ckID = DOS_ERROR);
		case 0:
			return (context->ckHdr.ckID = BAD_IFF);
		}
/*
	Check: Top level chunk must be LIST or FORM or CAT
*/
		if (context->parent == NULL) {
			switch(context->ckHdr.ckID) {
			case FORM:
			case LIST:
			case CAT:
				break;
			default:
				return (context->ckHdr.ckID = NOT_IFF);
			}
		}
/*
	Update the context
*/
		context->position	+= sizeof(ChunkHeader);
		remaining			-= sizeof(ChunkHeader);
/*
	Non-positive ID values are illegal and used for error codes
	We could check for other illegal IDs...
*/
		if (context->ckHdr.ckID <= 0)
			 context->ckHdr.ckID = BAD_IFF;
/*
	Check: ckSize negative or larger than # bytes left in context?
*/
		else if (context->ckHdr.ckSize < 0  || context->ckHdr.ckSize > remaining) {
			context->ckHdr.ckSize = remaining;
			context->ckHdr.ckID   = BAD_IFF;
		}
/*
	Automatically read the LIST, FORM, PROP, or CAT subtype ID
*/
		else {
			switch (context->ckHdr.ckID) {
			case LIST:
			case FORM:
			case PROP:
			case CAT:
				iffp = IFFReadBytes(context, &context->subtype, sizeof(ID));
				if (iffp != IFF_OKAY)
					context->ckHdr.ckID = iffp;
				break;
			}
		}
	}
	return (context->ckHdr.ckID);
}

/*
 *	IFFReadBytes
 */

IFFP IFFReadBytes(GroupContext *context, void *buffer, LONG nBytes)
{
	register IFFP iffp = IFF_OKAY;

	if (nBytes < 0)
		iffp = CLIENT_ERROR;
	else if (nBytes > ChunkMoreBytes(context))
		iffp = SHORT_CHUNK;
	else if (nBytes > 0) {
		switch (GRead(context->file, buffer, nBytes)) {
		case -1:
			iffp = DOS_ERROR;
			break;
		case 0:
			iffp = BAD_IFF;
			break;
		default:
			context->position	+= nBytes;
			context->bytesSoFar	+= nBytes;
			break;
		}
	}
	return (iffp);
}

/*
 *	SkipGroup
 *	(Nothing to do, thanks to GetChunkHdr)
 */

IFFP SkipGroup(GroupContext *context)
{
	return (IFF_OKAY);
}

/*
 *	ReadIFF
 */

IFFP ReadIFF(BPTR file, ClientFrame *clientFrame)
{
	register IFFP iffp;
	GroupContext context;

	iffp = OpenRIFF(file, &context, clientFrame);
	if (iffp == IFF_OKAY) {
		switch (iffp = GetChunkHdr(&context)) {
		case FORM:
			iffp = (*clientFrame->getForm)(&context);
			break;
		case LIST:
			iffp = (*clientFrame->getList)(&context);
			break;
		case CAT:
			iffp = (*clientFrame->getCat)(&context);
			break;
		}
	}
	CloseRGroup(&context);
	if (iffp > 0)			/* Make sure we don't return an ID */
		iffp = NOT_IFF;		/* GetChunkHdr should've caught this */
	return (iffp);
}

/*
 *	ReadIList
 */

IFFP ReadIList(GroupContext *parent, ClientFrame *clientFrame)
{
	register IFFP iffp;
	register BOOL propOk;
	GroupContext listContext;

	iffp = OpenRGroup(parent, &listContext);
	CheckIFFP();
/*
	One special case test lets us handle CATs as well as LISTs
*/
	if (parent->ckHdr.ckID == CAT)
		propOk = FALSE;
	else {
		propOk = TRUE;
		listContext.clientFrame = clientFrame;
	}
	do {
		switch (iffp = GetChunkHdr(&listContext)) {
		case PROP:
			iffp = (propOk) ? (*clientFrame->getProp)(&listContext) : BAD_IFF;
			break;
		case FORM:
			iffp = (*clientFrame->getForm)(&listContext);
			break;
		case LIST:
			iffp = (*clientFrame->getList)(&listContext);
			break;
		case CAT:
			iffp = (*clientFrame->getCat)(&listContext);
			break;
		}
		if (listContext.ckHdr.ckID != PROP)
			propOk = FALSE;		/* No PROPs allowed after this point */
	} while (iffp == IFF_OKAY);
	CloseRGroup(&listContext);
	if (iffp > 0)				/* Only chunk types above are allowed in a LIST/CAT */
		iffp = BAD_IFF;
	return ((iffp == END_MARK) ? IFF_OKAY : iffp);
}

/*
 *	ReadICat
 *	By special arrangement with the ReadIList implementation, this is trivial
 */

IFFP ReadICat(GroupContext *parent)
{
	return (ReadIList(parent, NULL));
}

/*
 *	GetFChunkHdr
 */

ID GetFChunkHdr(GroupContext *context)
{
	register ID id;

	id = GetChunkHdr(context);
	if (id == PROP)
		context->ckHdr.ckID = id = BAD_IFF;
	return (id);
}

/*
 *	GetF1ChunkHdr
 */

ID GetF1ChunkHdr(GroupContext *context)
{
	register ID id;
	register ClientFrame *clientFrame = context->clientFrame;

	switch (id = GetChunkHdr(context))  {
		case PROP:
		id = BAD_IFF;
		break;
	case FORM:
		id = (*clientFrame->getForm)(context);
		break;
	case LIST:
		id = (*clientFrame->getList)(context);
		break;
	case CAT:
		id = (*clientFrame->getCat)(context);
		break;
	}
	return (context->ckHdr.ckID = id);
}

/*
 *	GetPChunkHdr
 */

ID GetPChunkHdr(GroupContext *context)
{
	register ID id;

	id = GetChunkHdr(context);
	if (id == LIST || id == FORM || id == PROP || id == CAT)
		id = context->ckHdr.ckID = BAD_IFF;
	return (id);
}
