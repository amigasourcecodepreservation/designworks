/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Amiga Toolbox
 *	Copyright (c) 1989 New Horizons Software, Inc.
 *
 *	Misc graphics routines
 */

#ifndef TOOLBOX_GRAPHICS_H
#define TOOLBOX_GRAPHICS_H

#ifndef EXEC_TYPES_H
#include <exec/types.h>
#endif

#ifndef TYPEDEFS_H
#include "typedefs.h"
#endif

/*
 *	Maximum blitter blit width
 */

#define MAX_BLIT_WIDTH	1008

/*
 *	Routine prototypes
 */

void		LocalToGlobal(WindowPtr, WORD *, WORD *);
ScreenPtr	WhichScreen(WORD, WORD);
void		ScrollRect(LayerPtr, WORD, WORD, WORD, WORD, WORD, WORD);
void		LoadRGB4CM(ColorMapPtr, UWORD *, WORD);
RastPtr		OpenPort(void);
void		ClosePort(RastPtr);
void		SetRect(Rectangle *, WORD, WORD, WORD, WORD);
RegionPtr	GetClip(LayerPtr);
void		SetClip(LayerPtr, RegionPtr);
void		SetRectClip(LayerPtr, Rectangle *);
BOOL		LayerObscured(LayerPtr);

#endif
