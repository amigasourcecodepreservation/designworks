/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Toolbox library
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Standard File Package routines
 */

#include <clib/macros.h>
#include <exec/types.h>
#include <exec/memory.h>
#include <intuition/intuition.h>
#include <libraries/dos.h>
#include <libraries/dosextens.h>
#include <workbench/workbench.h>

#include <string.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/layers.h>
#include <proto/intuition.h>
#include <proto/dos.h>
#include <proto/icon.h>

#include "typedefs.h"

#include "Memory.h"
#include "Graphics.h"
#include "Request.h"
#include "StdFile.h"
#include "List.h"
#include "ScrollList.h"
#include "Utility.h"

/*
 *	External variables
 */

extern struct DosLibrary	*DOSBase;
extern struct Library		*IconBase;

extern BOOL			_tbHiRes;
extern TextAttrPtr	_tbTextAttr;

extern WORD	_tbBlackColor, _tbWhiteColor;
extern WORD	_tbDarkColor, _tbLightColor;

/*
 *	Local variables and definitions
 */

typedef struct FileInfoBlock	FIB, *FIBPtr;
typedef struct FileLock			FileLock, *FileLockPtr;
typedef struct DeviceList		DevList, *DevListPtr;
typedef struct DosInfo			DosInfo, *DosInfoPtr;
typedef struct RootNode			RootNode, *RootNodePtr;
typedef struct DiskObject		DiskObject, *DiskObjPtr;

#define VOLUME(lock)	\
	((DevListPtr) BADDR(((FileLockPtr) BADDR(lock))->fl_Volume))

#define NEXT_DEVICE(device)	((DevListPtr) BADDR(device->dl_Next))

#define MAX_FILENAME_LEN	(30-5)			/* 5 chars for ".info" */

#define GET_DIALOG		0
#define PUT_DIALOG		1

static DialogPtr	sfpDialog;
static GadgetPtr	gadgList;
static MsgPortPtr	sfpMsgPort;
static BOOL			(*dialogFilter)(IntuiMsgPtr, WORD *);

static BOOL			atTop;

static ScrollListPtr	scrollList;

static WORD	dlgType;

#define SHIFTKEYS	(IEQUALIFIER_LSHIFT | IEQUALIFIER_RSHIFT)
#define ALTKEYS		(IEQUALIFIER_LALT | IEQUALIFIER_RALT)

/*
 *	Text for dialogs and requesters
 */

static TextChar systemText[] = "Mounted disks and assignments";

static TextChar okText[] = "OK";
static TextChar yesText[] = "Yes";
static TextChar noText[] = "No";
static TextChar	openText[] = "Open";
static TextChar saveText[] = "Save";
static TextChar	cancelText[] = "Cancel";
static TextChar	enterText[] = "Enter";
static TextChar	backText[] = "Back";
static TextChar	diskText[] = "Disk";
static TextChar badNameText[] = "Improper file name.";
static TextChar	noFileText[] = "No such file.";
static TextChar	diskLockText[] = "Disk is locked.";
static TextChar fileExists1Text[] = "File already exists,";
static TextChar fileExists2Text[] = "OK to replace?";

/*
 *	Disk and drawer icons
 */

static UWORD chip hrSystemIconData[] = {
	0xFC00, 0x0000, 0x7E00, 0xFDFF, 0xFFFF, 0x7E00,
	0xFD80, 0x0003, 0x7E00, 0xFD80, 0x0003, 0x7E00,
	0xFD80, 0x0003, 0x7E00, 0xFD80, 0x0003, 0x7E00,
	0xFD80, 0x0003, 0x7E00, 0xFD80, 0x0003, 0x7E00,
	0xFD80, 0x0003, 0x7E00, 0xFD80, 0x0003, 0x7E00,
	0xFD80, 0x0003, 0x7E00, 0xFDFF, 0xFFFF, 0x7E00,
	0xFC00, 0x0000, 0x7E00, 0xF000, 0x0000, 0x1E00,
	0xEFFF, 0xFFFF, 0xEE00, 0xD924, 0x9249, 0x3600,
	0xB24C, 0x9264, 0x9A00, 0x6499, 0x9332, 0x4C00,
	0x7FFF, 0xFFFF, 0xFC00, 0x8000, 0x0000, 0x0200,

	0x03FF, 0xFFFF, 0x8000, 0x0200, 0x0000, 0x8000,
	0x0200, 0x0000, 0x8000, 0x0200, 0x0000, 0x8000,
	0x0200, 0x0000, 0x8000, 0x0200, 0x0000, 0x8000,
	0x0200, 0x0000, 0x8000, 0x0200, 0x0000, 0x8000,
	0x0200, 0x0000, 0x8000, 0x0200, 0x0000, 0x8000,
	0x0200, 0x0000, 0x8000, 0x0200, 0x0000, 0x8000,
	0x03FF, 0xFFFF, 0x8000, 0x0FFF, 0xFFFF, 0xE000,
	0x1000, 0x0000, 0x1000, 0x26DB, 0x6DB6, 0xC800,
	0x4DB3, 0x6D9B, 0x6400, 0x9B66, 0x6CCD, 0xB200,
	0x8000, 0x0000, 0x0200, 0x7FFF, 0xFFFF, 0xFC00,

	0x03FF, 0xFFFF, 0x8000, 0x0200, 0x0000, 0x8000,
	0x0200, 0x0000, 0x8000, 0x0200, 0x0000, 0x8000,
	0x0200, 0x0000, 0x8000, 0x0200, 0x0000, 0x8000,
	0x0200, 0x0000, 0x8000, 0x0200, 0x0000, 0x8000,
	0x0200, 0x0000, 0x8000, 0x0200, 0x0000, 0x8000,
	0x0200, 0x0000, 0x8000, 0x0200, 0x0000, 0x8000,
	0x03FF, 0xFFFF, 0x8000, 0x0FFF, 0xFFFF, 0xE000,
	0x1000, 0x0000, 0x1000, 0x26DB, 0x6DB6, 0xC800,
	0x4DB3, 0x6D9B, 0x6400, 0x9B66, 0x6CCD, 0xB200,
	0x8000, 0x0000, 0x0200, 0x7FFF, 0xFFFF, 0xFC00,

	0x03FF, 0xFFFF, 0x8000, 0x0200, 0x0000, 0x8000,
	0x0200, 0x0000, 0x8000, 0x0200, 0x0000, 0x8000,
	0x0200, 0x0000, 0x8000, 0x0200, 0x0000, 0x8000,
	0x0200, 0x0000, 0x8000, 0x0200, 0x0000, 0x8000,
	0x0200, 0x0000, 0x8000, 0x0200, 0x0000, 0x8000,
	0x0200, 0x0000, 0x8000, 0x0200, 0x0000, 0x8000,
	0x03FF, 0xFFFF, 0x8000, 0x0FFF, 0xFFFF, 0xE000,
	0x1000, 0x0000, 0x1000, 0x26DB, 0x6DB6, 0xC800,
	0x4DB3, 0x6D9B, 0x6400, 0x9B66, 0x6CCD, 0xB200,
	0x8000, 0x0000, 0x0200, 0x7FFF, 0xFFFF, 0xFC00,
};

static UWORD chip hrDiskIconData[] = {
	0x0003, 0x0FF1, 0x0F90, 0x0F90, 0x0F90, 0x0FF0, 0x0000, 0x0000,
	0x0000, 0x3FFC, 0x3FFC, 0x3FFC, 0x3FFC, 0x3FFC, 0x3FFC, 0x0000,

	0xFFFC, 0xFFFE, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF,
	0xFFFF, 0xC003, 0xC003, 0xC003, 0xC003, 0xC003, 0xC003, 0xFFFF,

	0xFFFC, 0xFFFE, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF,
	0xFFFF, 0xC003, 0xC003, 0xC003, 0xC003, 0xC003, 0xC003, 0xFFFF,

	0xFFFC, 0xFFFE, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF,
	0xFFFF, 0xC003, 0xC003, 0xC003, 0xC003, 0xC003, 0xC003, 0xFFFF,
};

static UWORD chip hrDrawerIconData[] = {
    0x0000, 0x0000, 0x7FFF, 0xFFFE, 0x6000, 0x0006, 0x6FFF, 0xFFF6,
    0x6FFF, 0xFFF6, 0x6FF7, 0xEFF6, 0x6FF0, 0x0FF6, 0x6FFF, 0xFFF6,
    0x6FFF, 0xFFF6, 0x6000, 0x0006, 0x7FFF, 0xFFFE, 0x0000, 0x0000,

    0xFFFF, 0xFFFF, 0x8000, 0x0001, 0x9FFF, 0xFFF9, 0x9000, 0x0009,
    0x9000, 0x0009, 0x9008, 0x1009, 0x900F, 0xF009, 0x9000, 0x0009,
    0x9000, 0x0009, 0x9FFF, 0xFFF9, 0x8000, 0x0001, 0xFFFF, 0xFFFF,

    0xFFFF, 0xFFFF, 0x8000, 0x0001, 0x9FFF, 0xFFF9, 0x9000, 0x0009,
    0x9000, 0x0009, 0x9008, 0x1009, 0x900F, 0xF009, 0x9000, 0x0009,
    0x9000, 0x0009, 0x9FFF, 0xFFF9, 0x8000, 0x0001, 0xFFFF, 0xFFFF,

    0xFFFF, 0xFFFF, 0x8000, 0x0001, 0x9FFF, 0xFFF9, 0x9000, 0x0009,
    0x9000, 0x0009, 0x9008, 0x1009, 0x900F, 0xF009, 0x9000, 0x0009,
    0x9000, 0x0009, 0x9FFF, 0xFFF9, 0x8000, 0x0001, 0xFFFF, 0xFFFF,
};

static Image hrSystemIcon = {
	0, -5, 39, 20, 4, &hrSystemIconData[0], 0x0F, 0, NULL
};

static Image hrDiskIcon = {
	8, -3, 16, 16, 4, &hrDiskIconData[0], 0x0F, 0, NULL
};

static Image hrDrawerIcon = {
	0, -1, 32, 12, 4, &hrDrawerIconData[0], 0x0F, 0, NULL
};

static UWORD chip mrSystemIconData[] = {
	0xFC00, 0x0000, 0x7E00, 0xFCFF, 0xFFFE, 0x7E00,
	0xFCC0, 0x0006, 0x7E00, 0xFCC0, 0x0006, 0x7E00,
	0xFCC0, 0x0006, 0x7E00, 0xFCC0, 0x0006, 0x7E00,
	0xFCC0, 0x0006, 0x7E00, 0xFCFF, 0xFFFE, 0x7E00,
	0xE000, 0x0000, 0x0E00, 0xCFFF, 0xFFFF, 0xE600,
	0x9924, 0x9249, 0x3200, 0x3249, 0x9324, 0x9800,
	0x3FFF, 0xFFFF, 0xF800, 0x8000, 0x0000, 0x0200,

	0x03FF, 0xFFFF, 0x8000, 0x0300, 0x0001, 0x8000,
	0x0300, 0x0001, 0x8000, 0x0300, 0x0001, 0x8000,
	0x0300, 0x0001, 0x8000, 0x0300, 0x0001, 0x8000,
	0x0300, 0x0001, 0x8000, 0x0300, 0x0001, 0x8000,
	0x1FFF, 0xFFFF, 0xF000, 0x3000, 0x0000, 0x1800,
	0x66DB, 0x6DB6, 0xCC00, 0xCDB6, 0x6CDB, 0x6600,
	0xC000, 0x0000, 0x0600, 0x7FFF, 0xFFFF, 0xFC00,

	0x03FF, 0xFFFF, 0x8000, 0x0300, 0x0001, 0x8000,
	0x0300, 0x0001, 0x8000, 0x0300, 0x0001, 0x8000,
	0x0300, 0x0001, 0x8000, 0x0300, 0x0001, 0x8000,
	0x0300, 0x0001, 0x8000, 0x0300, 0x0001, 0x8000,
	0x1FFF, 0xFFFF, 0xF000, 0x3000, 0x0000, 0x1800,
	0x66DB, 0x6DB6, 0xCC00, 0xCDB6, 0x6CDB, 0x6600,
	0xC000, 0x0000, 0x0600, 0x7FFF, 0xFFFF, 0xFC00,

	0x03FF, 0xFFFF, 0x8000, 0x0300, 0x0001, 0x8000,
	0x0300, 0x0001, 0x8000, 0x0300, 0x0001, 0x8000,
	0x0300, 0x0001, 0x8000, 0x0300, 0x0001, 0x8000,
	0x0300, 0x0001, 0x8000, 0x0300, 0x0001, 0x8000,
	0x1FFF, 0xFFFF, 0xF000, 0x3000, 0x0000, 0x1800,
	0x66DB, 0x6DB6, 0xCC00, 0xCDB6, 0x6CDB, 0x6600,
	0xC000, 0x0000, 0x0600, 0x7FFF, 0xFFFF, 0xFC00,
};

static UWORD chip mrDiskIconData[] = {
	0x0FF3, 0x0F90, 0x0F90, 0x0FF0, 0x0000, 0x3FFC, 0x3FFC, 0x3FFC,
	0x3FFC, 0x0000,

	0xFFFC, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xC003, 0xC003, 0xC003,
	0xC003, 0xFFFF,

	0xFFFC, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xC003, 0xC003, 0xC003,
	0xC003, 0xFFFF,

	0xFFFC, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xC003, 0xC003, 0xC003,
	0xC003, 0xFFFF,
};

static UWORD chip mrDrawerIconData[] = {
	0x0000, 0x0000, 0x3FFF, 0xFFFC, 0x3000, 0x000C, 0x33FF, 0xFFCC,
	0x33E7, 0xE7CC, 0x33E0, 0x07CC, 0x33FF, 0xFFCC, 0x3000, 0x000C,
	0x3FFF, 0xFFFC, 0x0000, 0x0000,

	0xFFFF, 0xFFFF, 0xC000, 0x0003, 0xCFFF, 0xFFF3, 0xCC00, 0x0033,
	0xCC18, 0x1833, 0xCC1F, 0xF833, 0xCC00, 0x0033, 0xCFFF, 0xFFF3,
	0xC000, 0x0003, 0xFFFF, 0xFFFF,

	0xFFFF, 0xFFFF, 0xC000, 0x0003, 0xCFFF, 0xFFF3, 0xCC00, 0x0033,
	0xCC18, 0x1833, 0xCC1F, 0xF833, 0xCC00, 0x0033, 0xCFFF, 0xFFF3,
	0xC000, 0x0003, 0xFFFF, 0xFFFF,

	0xFFFF, 0xFFFF, 0xC000, 0x0003, 0xCFFF, 0xFFF3, 0xCC00, 0x0033,
	0xCC18, 0x1833, 0xCC1F, 0xF833, 0xCC00, 0x0033, 0xCFFF, 0xFFF3,
	0xC000, 0x0003, 0xFFFF, 0xFFFF,
};

static Image mrSystemIcon = {
	0, -2, 39, 14, 4, &mrSystemIconData[0], 0x0F, 0, NULL
};

static Image mrDiskIcon = {
	8, -1, 16, 10, 4, &mrDiskIconData[0], 0x0F, 0, NULL
};

static Image mrDrawerIcon = {
	0, -1, 32, 10, 4, &mrDrawerIconData[0], 0x0F, 0, NULL
};

static ImagePtr systemIcon, diskIcon, drawerIcon;

/*
 *	Gadgets in common between get and put dialogs
 */

#define ENTER_BUTTON	2
#define BACK_BUTTON		3
#define NEXTDISK_ARROW	4
#define PREVDISK_ARROW	5
#define FILE_LIST		7
#define FILE_UP			8
#define FILE_DOWN		9
#define FILE_SCROLL		10
#define FILENAME_TEXT	11
#define DISK_NAME		12
#define DISK_ICON		13

/*
 *	Structures for SFPGetFile()
 */

#define LIST_WIDTH	(24*8)

#define OPEN_NUM	10

static GadgetTemplate getGadgets[] = {
	{ GADG_PUSH_BUTTON, -80, -60, 0, 0, 60, 20, 0, 0, &openText[0] },
	{ GADG_PUSH_BUTTON, -80, -30, 0, 0, 60, 20, 0, 0, &cancelText[0] },

	{ GADG_PUSH_BUTTON, -80,  50, 0, 0, 60, 20, 0, 0, &enterText[0] },
	{ GADG_PUSH_BUTTON, -80,  80, 0, 0, 60, 20, 0, 0, &backText[0] },

	{ GADG_ACTIVE_STDIMAGE,
		-50+(2*8), 30, 6, 0, 0, 0, IMAGE_ARROW_WIDTH, IMAGE_ARROW_HEIGHT,
		(Ptr) IMAGE_RIGHT_ARROW },
	{ GADG_ACTIVE_STDIMAGE,
		-50-(2*8), 30, -IMAGE_ARROW_WIDTH - 6, 0, 0, 0, IMAGE_ARROW_WIDTH, IMAGE_ARROW_HEIGHT,
		(Ptr) IMAGE_LEFT_ARROW },

	{ GADG_STAT_TEXT | GADG_NO_REPORT, -50-(2*8), 30, 0, 0, 0, 0, 0, 0, &diskText[0] },

	{  GADG_ACTIVE_BORDER,
		20,  30, 0, 0, LIST_WIDTH, OPEN_NUM*11, -IMAGE_ARROW_WIDTH - 1, OPEN_NUM,
		NULL },
	{ GADG_ACTIVE_STDIMAGE,
		20 + LIST_WIDTH, 30 + OPEN_NUM*11, -IMAGE_ARROW_WIDTH, OPEN_NUM - 2*IMAGE_ARROW_HEIGHT,
		0, 0, IMAGE_ARROW_WIDTH, IMAGE_ARROW_HEIGHT,
		(Ptr) IMAGE_UP_ARROW },
	{ GADG_ACTIVE_STDIMAGE,
		20 + LIST_WIDTH, 30 + OPEN_NUM*11, -IMAGE_ARROW_WIDTH, OPEN_NUM - IMAGE_ARROW_HEIGHT,
		0, 0, IMAGE_ARROW_WIDTH, IMAGE_ARROW_HEIGHT,
		(Ptr) IMAGE_DOWN_ARROW },
	{ GADG_PROP_VERT,
		20 + LIST_WIDTH, 30, -IMAGE_ARROW_WIDTH, 0,
		0, OPEN_NUM*11, IMAGE_ARROW_WIDTH, OPEN_NUM - 2*IMAGE_ARROW_HEIGHT,
		NULL },

	{ GADG_EDIT_TEXT, 20, -20, 0, 0, LIST_WIDTH, 11, 0, 0, NULL },

	{ GADG_STAT_TEXT | GADG_NO_REPORT,  65, 10, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_IMAGE | GADG_NO_REPORT, 20, 10, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_ITEM_NONE }
};

static DialogTemplate getFileDlgTempl = {
	DLG_TYPE_WINDOW, 0, -1, -1, 120 + LIST_WIDTH, 180, &getGadgets[0], NULL
};

/*
 *	Structures for SFPPutFile()
 */

#define SAVE_NUM	7

#define PUT_PROMPT	14

static GadgetTemplate putGadgets[] = {
	{ GADG_PUSH_BUTTON, -80, -60, 0, 0, 60, 20, 0, 0, &saveText[0] },
	{ GADG_PUSH_BUTTON, -80, -30, 0, 0, 60, 20, 0, 0, &cancelText[0] },

	{ GADG_PUSH_BUTTON, -80,  50, 0, 0, 60, 20, 0, 0, &enterText[0] },
	{ GADG_PUSH_BUTTON, -80,  80, 0, 0, 60, 20, 0, 0, &backText[0] },

	{ GADG_ACTIVE_STDIMAGE,
		-50+(2*8), 30, 6, 0, 0, 0, IMAGE_ARROW_WIDTH, IMAGE_ARROW_HEIGHT,
		(Ptr) IMAGE_RIGHT_ARROW },
	{ GADG_ACTIVE_STDIMAGE,
		-50-(2*8), 30, -IMAGE_ARROW_WIDTH - 6, 0, 0, 0, IMAGE_ARROW_WIDTH, IMAGE_ARROW_HEIGHT,
		(Ptr) IMAGE_LEFT_ARROW },

	{ GADG_STAT_TEXT | GADG_NO_REPORT, -50-(2*8), 30, 0, 0, 0, 0, 0, 0, &diskText[0] },

	{ GADG_ACTIVE_BORDER,
		20,  30, 0, 0, LIST_WIDTH, SAVE_NUM*11, -IMAGE_ARROW_WIDTH - 1, SAVE_NUM,
		NULL },
	{ GADG_ACTIVE_STDIMAGE,
		20 + LIST_WIDTH, 30 + SAVE_NUM*11, -IMAGE_ARROW_WIDTH, SAVE_NUM - 2*IMAGE_ARROW_HEIGHT,
		0, 0, IMAGE_ARROW_WIDTH, IMAGE_ARROW_HEIGHT,
		(Ptr) IMAGE_UP_ARROW },
	{ GADG_ACTIVE_STDIMAGE,
		20 + LIST_WIDTH, 30 + SAVE_NUM*11, -IMAGE_ARROW_WIDTH, SAVE_NUM - IMAGE_ARROW_HEIGHT,
		0, 0, IMAGE_ARROW_WIDTH, IMAGE_ARROW_HEIGHT,
		(Ptr) IMAGE_DOWN_ARROW },
	{ GADG_PROP_VERT,
		20 + LIST_WIDTH, 30, -IMAGE_ARROW_WIDTH, 0,
		0, SAVE_NUM*11, IMAGE_ARROW_WIDTH, SAVE_NUM - 2*IMAGE_ARROW_HEIGHT,
		NULL },

	{ GADG_EDIT_TEXT, 20, -40, 0, 0, LIST_WIDTH, 11, 0, 0, NULL },

	{ GADG_STAT_TEXT | GADG_NO_REPORT,  65, 10, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_IMAGE | GADG_NO_REPORT, 20, 10, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_STAT_TEXT | GADG_NO_REPORT,  20, -60, 0, 2, 0, 0, 0, 0, NULL },

	{ GADG_ITEM_NONE }
};

static DialogTemplate putFileDlgTempl = {
	DLG_TYPE_ALERT, 0, -1, -1, 120 + LIST_WIDTH, 180, &putGadgets[0], NULL
};

/*
 *	Misc requesters
 */

#define REQ_FILEEXISTS	0
#define REQ_BADNAME		1
#define REQ_NOFILE		2
#define REQ_DISKLOCK	3

#define REQ1_STATTEXT	1
#define REQ2_STATTEXT1	2
#define REQ2_STATTEXT2	3

static GadgetTemplate req1Gadgets[] = {
	{ GADG_PUSH_BUTTON, 160, 40, 0, 0, 60, 20, 0, 0, &okText[0] },
	{ GADG_STAT_TEXT | GADG_NO_REPORT, 60, 10, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_STDIMAGE | GADG_NO_REPORT, 10, 10, 0, 0, 0, 0, 0, 0, (Ptr) IMAGE_STOP_ICON },
	{ GADG_ITEM_NONE }
};

static RequestTemplate req1ReqTempl = {
	-1, -1, 240, 70, &req1Gadgets[0]
};

static GadgetTemplate req2Gadgets[] = {
	{ GADG_PUSH_BUTTON,  40, 50, 0, 0, 60, 20, 0, 0, &yesText[0] },
	{ GADG_PUSH_BUTTON, 140, 50, 0, 0, 60, 20, 0, 0, &noText[0] },
	{ GADG_STAT_TEXT | GADG_NO_REPORT, 60, 10, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT | GADG_NO_REPORT, 60, 30, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_STDIMAGE | GADG_NO_REPORT, 10, 10, 0, 0, 0, 0, 0, 0, (Ptr) IMAGE_CAUTION_ICON },
	{ GADG_ITEM_NONE }
};

static RequestTemplate req2ReqTempl = {
	-1, -1, 240, 80, &req2Gadgets[0]
};

/*
 *	Local routine prototypes
 */

void	SetDialogIcons(void);
Dir		GetCurrentDir(void);
WORD	ShowDirName(void);
void	GetFileList(BOOL (*)(TextPtr), WORD, TextPtr *);
void	ShowFileList(ListHeadPtr, ListHeadPtr);
void	ClearFileList(void);
BOOL	IsDirItem(WORD);
void	OffButtons(void);
void	SetBackButton(void);
void	SetButtons(void);
WORD	CheckButtons(void);
WORD	EnterDir(void);
WORD	ExitDir(BOOL);
WORD	ChangeVolume(BOOL);
Dir		CheckFileName(TextPtr);
BOOL	DiskIsLocked(void);
void	NewSelItem(WORD);
WORD	SFPDialogFilter(IntuiMsgPtr, WORD *);
void	DrawDlgBorders(void);
WORD	SimpleRequest(MsgPortPtr, WindowPtr, WORD, BOOL (*)(IntuiMsgPtr, WORD *));
void	DoDirSwitch(WORD, BOOL (*)(TextPtr), WORD, TextPtr *);
WORD	DoFileList(void);

/*
 *	Set requester template pointers to appropriate templates
 */

static void SetDialogIcons()
{
	if (_tbHiRes) {
		systemIcon			= &hrSystemIcon;
		diskIcon			= &hrDiskIcon;
		drawerIcon			= &hrDrawerIcon;
	}
	else {
		systemIcon			= &mrSystemIcon;
		diskIcon			= &mrDiskIcon;
		drawerIcon			= &mrDrawerIcon;
	}
}

/*
 *	Return pointer to lock of current directory
 *	Does not return a duplicate of the lock
 */

static Dir GetCurrentDir()
{
	return (((ProcessPtr) FindTask(NULL))->pr_CurrentDir);
}

/*
 *	Display the new directory/volume name and icon in dialog
 *	Return success status
 */

static BOOL ShowDirName()
{
	WORD xMin, yMin, xMax, yMax, position;
	BOOL success;
	Dir currentDir, parentDir;
	FIBPtr dirFIB;
	register GadgetPtr nameGadget, iconGadget;
	RastPtr rPort = sfpDialog->RPort;
	ImagePtr iconImage;
#ifdef NEW_LOOK
	BorderPtr border;
#endif
	TextPtr dirName;

	if ((dirFIB = AllocMem(sizeof(FIB), MEMF_CLEAR)) == NULL)
		return (FALSE);
	nameGadget = GadgetItem(gadgList, DISK_NAME);
	iconGadget = GadgetItem(gadgList, DISK_ICON);
	success = FALSE;
	currentDir = GetCurrentDir();
/*
	Display the dir/volume name
*/
	if (atTop)
		dirName = systemText;
	else if (Examine(currentDir, dirFIB))
		dirName = dirFIB->fib_FileName;
	else
		goto Exit;
	SetGadgetText(nameGadget, sfpDialog, NULL, dirName);
/*
	Display the icon (erasing old one first)
*/
	if ((iconImage = (ImagePtr) iconGadget->GadgetRender) != NULL) {
#ifdef NEW_LOOK
		xMin = iconGadget->LeftEdge + iconImage->LeftEdge - 4;
		yMin = iconGadget->TopEdge + iconImage->TopEdge - 2;
		xMax = xMin + iconImage->Width + 7;
		yMax = yMin + iconImage->Height + 3;
#else
		xMin = iconGadget->LeftEdge + iconImage->LeftEdge;
		yMin = iconGadget->TopEdge + iconImage->TopEdge;
		xMax = xMin + iconImage->Width - 1;
		yMax = yMin + iconImage->Height - 1;
#endif
		SetAPen(rPort, _tbLightColor);
		RectFill(rPort, xMin, yMin, xMax, yMax);
	}
	position = RemoveGadget(sfpDialog, iconGadget);
	if (atTop)
		iconGadget->GadgetRender = (APTR) systemIcon;
	else if ((parentDir = ParentDir(currentDir)) != NULL) {
		UnLock(parentDir);
		iconGadget->GadgetRender = (APTR) drawerIcon;
	}
	else
		iconGadget->GadgetRender = (APTR) diskIcon;
	AddGList(sfpDialog, iconGadget, position, 1, NULL);
	RefreshGList(iconGadget, sfpDialog, NULL, 1);
#ifdef NEW_LOOK
	iconImage = (ImagePtr) iconGadget->GadgetRender;
	xMin = iconGadget->LeftEdge + iconImage->LeftEdge;
	yMin = iconGadget->TopEdge + iconImage->TopEdge;
	if ((border = BoxBorder(iconImage->Width, iconImage->Height, _tbWhiteColor, -1)) != NULL) {
		DrawBorder(rPort, border, xMin, yMin);
		FreeBorder(border);
	}
	if ((border = BoxBorder(iconImage->Width, iconImage->Height, _tbDarkColor, -2)) != NULL) {
		DrawBorder(rPort, border, xMin, yMin);
		FreeBorder(border);
	}
#endif
	success = TRUE;
Exit:
	FreeMem(dirFIB, sizeof(FIB));
	return (success);
}

/*
 *	Get directory/file list for current directory and display
 *	If at top of tree, save list of mounted volumes in file list
 *		and list of path assignments in directory list
 */

static void GetFileList(BOOL (*fileFilter)(TextPtr), WORD numTypes, TextPtr typeList[])
{
	register TextPtr toolType, fileName;
	register WORD i, len;
	register BOOL addFile;
	register DevListPtr volume;
	DevListPtr firstDevice;
	DosInfoPtr dosInfo;
	RootNodePtr rootNode;
	Dir currentDir;
	DiskObjPtr diskObj;
	ListHeadPtr	dirList, fileList, list;
	FIBPtr fib;
	TextChar nameBuff[50];

/*
	Allocate needed items
*/
	if ((fib = AllocMem(sizeof(FIB), MEMF_CLEAR)) == NULL)
		goto Exit4;
	if ((dirList = CreateList()) == NULL)
		goto Exit3;
	if ((fileList = CreateList()) == NULL)
		goto Exit2;
/*
	If at top, get list of mounted volumes
*/
	if (atTop) {
		Forbid();
		rootNode = (RootNodePtr) DOSBase->dl_Root;
		dosInfo = (DosInfoPtr) BADDR(rootNode->rn_Info);
		firstDevice = (DevListPtr) BADDR(dosInfo->di_DevInfo);
		for (volume = firstDevice; volume; volume = NEXT_DEVICE(volume)) {
			if ((volume->dl_Type == DLT_VOLUME && volume->dl_Task != NULL) ||
				volume->dl_Type == DLT_DIRECTORY) {
				fileName = (TextPtr) BADDR(volume->dl_Name);
				len = *fileName;
				BlockMove(fileName + 1, nameBuff, len);
				nameBuff[len++] = ':';
				nameBuff[len] = '\0';
				list = (volume->dl_Type == DLT_VOLUME) ? fileList : dirList;
				if (AddListItem(list, nameBuff, len, TRUE) == -1)
					break;
			}
		}
		Permit();
	}
/*
	Otherwise, get directory list
*/
	else {
		currentDir = GetCurrentDir();
		if (!Examine(currentDir, fib) || fib->fib_DirEntryType <= 0)
			goto Exit1;
		fileName = fib->fib_FileName;
		while (ExNext(currentDir, fib)) {
			if (CheckButtons())
				goto Exit1;
			len = strlen(fileName);
/*
	If directory entry, add entry to directory list
*/
			if (fib->fib_DirEntryType > 0) {
				if (AddListItem(dirList, fileName, len, TRUE) == -1)
					break;
			}
/*
	Else if file, then add to list of files (ignore files that end in ".info")
*/
			else if (fib->fib_DirEntryType < 0) {
				if (*fileName == '.' ||
					(len > 4 && CmpString(fileName+len-5, ".info", 5, 5, FALSE) == 0) ||
					fib->fib_Size == 0 ||
					(len == 1 && *fileName == '*'))
					continue;
/*
	Check to see if this file should be included in list
	(avoid reading icon if fileFilter says to list it)
*/
				addFile = FALSE;
				if (fileFilter)
					addFile = (*fileFilter)(fileName);
				else if (numTypes == 0)
					addFile = TRUE;
				if (!addFile && numTypes && IconBase &&
					(diskObj = GetDiskObject(fileName)) != NULL) {
					if ((toolType = FindToolType(diskObj->do_ToolTypes, "FILETYPE"))
						!= NULL) {
						for (i = 0; !addFile && i < numTypes; i++)
							addFile = MatchToolValue(toolType, typeList[i]);
					}
					FreeDiskObject(diskObj);
				}
/*
	Add file to list
*/
				if (addFile && AddListItem(fileList, fileName, len, TRUE) == -1)
					break;
			}
		}
	}
/*
	Show the file lists and dispose of items
*/
Exit0:
	ShowFileList(dirList, fileList);
Exit1:
	FreeMem(fib, sizeof(FIB));
Exit2:
	DisposeList(dirList);
Exit3:
	DisposeList(fileList);
Exit4:
	return;			/* Keeps compiler happy */
}

/*
 *	Display the new list of directories/files in dialog
 *	Mark directory names with a non-space character before name
 */

static void ShowFileList(ListHeadPtr dirList, ListHeadPtr fileList)
{
	WORD i, j, len, numItems, numScroll, dashWidth;
	TextPtr text;
	IntuiTextPtr intuiText;
	TextChar buff[50];

	SLDoDraw(scrollList, FALSE);
/*
	Add file names
*/
	numItems = NumListItems(fileList);
	buff[0] = ' ';
	for (i = 0; i < numItems; i++) {
		text = GetListItem(fileList, i);
		strcpy(buff + 1, text);
		SLAddItem(scrollList, buff, (WORD) strlen(buff), i);
	}
/*
	If at top, put separator between volume and path lists
*/
	if (atTop) {
		intuiText = NewIntuiText(0, 0, "-", FS_NORMAL, 0);
		dashWidth = IntuiTextLength(intuiText);
		FreeIntuiText(intuiText);
		len = (scrollList->ListBox->Width - 1)/dashWidth;
		memset(buff, '-', len);
		numScroll = SLNumItems(scrollList);
		SLAddItem(scrollList, buff, len, numScroll);
		SLEnableItem(scrollList, numScroll, FALSE);
	}
/*
	Sort directory names into file list
	Don't put flag in front of path assignments
*/
	numItems = NumListItems(dirList);
	for (i = 0; i < numItems; i++) {
		text = GetListItem(dirList, i);
		len = strlen(text);
		numScroll = SLNumItems(scrollList);
		if (atTop)
			j = numScroll;
		else {
			for (j = 0; j < numScroll; j++) {
				SLGetItem(scrollList, j, buff);
				if (CmpString(text, buff + 1, len, (WORD) strlen(buff + 1), FALSE) < 0)
					break;
			}
		}
		buff[0] = (atTop) ? ' ' : '-';
		strcpy(buff + 1, text);
		SLAddItem(scrollList, buff, (WORD) strlen(buff), j);
		SLSetItemStyle(scrollList, j, FSF_ITALIC);
	}
	SLDoDraw(scrollList, TRUE);
}

/*
 *	Clear the file list in dialog
 */

static void ClearFileList()
{
	SLRemoveAll(scrollList);
}

/*
 *	Return TRUE if specified list item is a directory (not a file name)
 */

static BOOL IsDirItem(WORD item)
{
	TextChar buff[50];

	SLGetItem(scrollList, item, buff);
	return ((BOOL) (atTop || buff[0] != ' '));
}

/*
 *	Disable all but "Disk" and "Cancel" buttons
 */

static void OffButtons()
{
	EnableGadgetItem(gadgList, OK_BUTTON, sfpDialog, NULL, FALSE);
	EnableGadgetItem(gadgList, ENTER_BUTTON, sfpDialog, NULL, FALSE);
	EnableGadgetItem(gadgList, BACK_BUTTON, sfpDialog, NULL, FALSE);
}

/*
 *	Set "Back" button on or off
 */

static void SetBackButton()
{
	EnableGadgetItem(gadgList, BACK_BUTTON, sfpDialog, NULL, (BOOL) !atTop);
}

/*
 *	Set dialog gadgets to enabled or disabled as appropriate
 */

static void SetButtons()
{
	WORD selItem;
	BOOL enable;
	TextChar fileName[GADG_MAX_STRING];

	SetBackButton();
/*
	Enable "Enter" button if a directory is selected
*/
	selItem = SLNextSelect(scrollList, -1);
	enable = (selItem >= 0 && IsDirItem(selItem));
	EnableGadgetItem(gadgList, ENTER_BUTTON, sfpDialog, NULL, enable);
/*
	Enable "Open"/"Save" button if there is a file name
*/
	GetEditItemText(gadgList, FILENAME_TEXT, fileName);
	enable = (strlen(fileName) != 0);
	EnableGadgetItem(gadgList, OK_BUTTON, sfpDialog, NULL, enable);
}

/*
 *	Check to see if "Disk", "Back", or "Cancel" button is pressed
 *	Return TRUE if yes, FALSE if not
 */

static BOOL CheckButtons()
{
	register WORD gadgNum;
	register BOOL found;
	register IntuiMsgPtr msg, nextMsg;
	GadgetPtr gadget;

	found = FALSE;
	Forbid();
	msg = (IntuiMsgPtr) sfpMsgPort->mp_MsgList.lh_Head;
	while (nextMsg = (IntuiMsgPtr) msg->ExecMessage.mn_Node.ln_Succ) {
		if (msg->Class == GADGETUP && msg->IDCMPWindow == sfpDialog) {
			gadget = (GadgetPtr) msg->IAddress;
			if (gadget &&
				((gadgNum = GadgetNumber(gadget)) == CANCEL_BUTTON ||
				 gadgNum == NEXTDISK_ARROW || gadgNum == PREVDISK_ARROW ||
				 gadgNum == BACK_BUTTON)) {
				found = TRUE;
				break;
			}
		}
		msg = nextMsg;
	}
	Permit();
	return (found);
}

/*
 *	Enter the currently selected subdirectory
 *	Return TRUE if entered subdirectory, FALSE if not
 */

static BOOL EnterDir()
{
	WORD selItem;
	register Dir dir;
	TextChar dirName[50];

	selItem = SLNextSelect(scrollList, -1);
	if (selItem < 0 || !IsDirItem(selItem))
		return (FALSE);
	SLGetItem(scrollList, selItem, dirName);
	if ((dir = Lock(dirName + 1, ACCESS_READ)) == NULL)
		return (FALSE);
	SetCurrentDir(dir);
	UnLock(dir);
	atTop = FALSE;
	return (TRUE);
}

/*
 *	Back up one or all directory level(s)
 *	Return TRUE if backed up, FALSE if not
 */

static BOOL ExitDir(BOOL toTop)
{
	register Dir dir, prevDir, currentDir;

	if (atTop)
		return (FALSE);
	currentDir = GetCurrentDir();
	if (toTop) {
		prevDir = NULL;
		for (dir = DupLock(currentDir); dir; dir = ParentDir(dir)) {
			if (prevDir)
				UnLock(prevDir);
			prevDir = dir;
		}
		SetCurrentDir(prevDir);
		UnLock(prevDir);
		atTop = TRUE;
	}
	else if ((dir = ParentDir(currentDir)) == NULL)
		atTop = TRUE;
	else {
		SetCurrentDir(dir);
		UnLock(dir);
	}
	return (TRUE);
}

/*
 *	Change the current volume to next or previous in device list
 *	Return TRUE if volume changed, FALSE if not
 *	Contains work-arounds to fact that volume pointer of RAM disk is actually
 *		pointer to RAM: device, not "RAM Disk:" volume
 */

static BOOL ChangeVolume(BOOL next)
{
	BOOL foundCurr;
	register TextPtr volName;
	register Dir dir;
	register DevListPtr volume, currVolume;
	DevListPtr prevVolume, nextVolume, firstVolume, lastVolume;
	DevListPtr firstDevice;
	DosInfoPtr dosInfo;
	RootNodePtr rootNode;
	TextChar nameBuff[50];

	Forbid();
	rootNode = (RootNodePtr) DOSBase->dl_Root;
	dosInfo = (DosInfoPtr) BADDR(rootNode->rn_Info);
	firstDevice = (DevListPtr) BADDR(dosInfo->di_DevInfo);
/*
	Get pointer to current volume
	(If volume pointer does not point to volume, then search all mounted
		volumes for correct volume entry--1.2 ram disk does this)
*/
	currVolume = VOLUME(GetCurrentDir());	/* Might be pointer to device */
	if (currVolume->dl_Type != DLT_VOLUME) {
		for (volume = firstDevice; volume; volume = NEXT_DEVICE(volume)) {
			if (volume->dl_Type == DLT_VOLUME &&
				volume->dl_Task == currVolume->dl_Task)
				break;
		}
		if (volume)
			currVolume = volume;
	}
/*
	Find the next and previous (mounted) volumes from current one
*/
	prevVolume = nextVolume = firstVolume = lastVolume = NULL;
	foundCurr = FALSE;
	for (volume = firstDevice; volume; volume = NEXT_DEVICE(volume)) {
		if (volume->dl_Type == DLT_VOLUME &&
			(volume->dl_Task != NULL || volume == currVolume)) {
			lastVolume = volume;
			if (firstVolume == NULL)
				firstVolume = volume;
			if (foundCurr && nextVolume == NULL)
				nextVolume = volume;
			else if (volume == currVolume)
				foundCurr = TRUE;
			else if (!foundCurr)
				prevVolume = volume;
		}
	}
	if (prevVolume == NULL)
		prevVolume = lastVolume;
	if (nextVolume == NULL)
		nextVolume = firstVolume;
/*
	If there are no other mounted volumes then return failure
*/
	volume = (next) ? nextVolume : prevVolume;
	if (volume == currVolume) {
		Permit();
		return (FALSE);
	}
/*
	Switch to new volume
	(Get lock on device name, since we may have two volumes with the same name)
*/
	currVolume = volume;
	for (volume = firstDevice; volume; volume = NEXT_DEVICE(volume)) {
		if (volume->dl_Type == DLT_DEVICE && volume->dl_Task == currVolume->dl_Task)
			break;
	}
	if (volume == NULL)		/* If not found, get lock on volume name */
		volume = currVolume;
	volName = (TextPtr) BADDR(volume->dl_Name);
	BlockMove(volName + 1, nameBuff, *volName);
	nameBuff[*volName] = ':';
	nameBuff[*volName + 1] = '\0';
	Permit();
	if ((dir = Lock(nameBuff, ACCESS_READ)) == NULL)
		return (FALSE);
	SetCurrentDir(dir);
	UnLock(dir);
	atTop = FALSE;
	return (TRUE);
}

/*
 *	Check file name
 *	Change directory to path in file name and strip path
 *	Return directory lock if valid name, NULL if not
 *	Directory will be switched to path specification
 *	Note:  If file name is a path only, then result is lock with empty file name
 */

static Dir CheckFileName(TextPtr fileName)
{
	register WORD i, len;
	Dir dir, subDir;
	FIBPtr fib;

/*
	Check for illegal characters
*/
	len = strlen(fileName);
	for (i = 0; i < len; i++) {
		if ((fileName[i] & 0x7F) < ' ')
			return (NULL);
	}
/*
	Strip path from name
*/
	dir = ConvertFileName(fileName);
	if (dir == NULL)
		return (NULL);
	if (strlen(fileName) > MAX_FILENAME_LEN) {
		UnLock(dir);
		return (NULL);
	}
/*
	Valid name, so switch to this directory
*/
	SetCurrentDir(dir);
/*
	Check to see if remainder of file name is a directory name
*/
	if (*fileName && (fib = AllocMem(sizeof(FIB), MEMF_CLEAR)) != NULL) {
		subDir = Lock(fileName, ACCESS_READ);
		if (subDir) {
			if (Examine(subDir, fib) && fib->fib_DirEntryType > 0) {
				UnLock(dir);
				SetCurrentDir(subDir);
				dir = subDir;
				*fileName = '\0';
			}
			else
				UnLock(subDir);
		}
		FreeMem(fib, sizeof(FIB));
	}
	return (dir);
}

/*
 *	Check to see if current directory is on locked disk
 *	Return TRUE if so
 */

static BOOL DiskIsLocked()
{
	BOOL locked;
	struct InfoData *infoData;

	if ((infoData = MemAlloc(sizeof(struct InfoData), 0)) == NULL)
		return (FALSE);
	Info(GetCurrentDir(), infoData);
	locked = (infoData->id_DiskState == ID_WRITE_PROTECTED);
	MemFree(infoData, sizeof(struct InfoData));
	return (locked);
}

/*
 *	Set new scroll list selection
 */

static void NewSelItem(register WORD newSelItem)
{
	WORD numItems;
	TextChar fileName[50];

	numItems = SLNumItems(scrollList);
	if (newSelItem < 0)
		newSelItem = 0;
	else if (newSelItem >= numItems)
		newSelItem = numItems - 1;
	SLSelectItem(scrollList, newSelItem, TRUE);
	SLAutoScroll(scrollList, newSelItem);
	if (!IsDirItem(newSelItem)) {
		SLGetItem(scrollList, newSelItem, fileName);
		SetEditItemText(sfpDialog, NULL, FILENAME_TEXT, fileName + 1);
	}
	SetButtons();
}

/*
 *	Handle GetFile and PutFile dialog intuiMsg
 */

static BOOL SFPDialogFilter(IntuiMsgPtr intuiMsg, WORD *item)
{
	WORD gadgNum, selItem;
	ULONG class = intuiMsg->Class;
	UWORD code = intuiMsg->Code;
	GadgetPtr gadget;

	switch (class) {
/*
	Handle gadget up/down in scroll list gadgets
		and ALT key down for "Back" button
*/
	case GADGETDOWN:
	case GADGETUP:
		gadgNum = GadgetNumber((GadgetPtr) intuiMsg->IAddress);
		if (gadgNum == FILE_LIST || gadgNum == FILE_SCROLL ||
			gadgNum == FILE_UP || gadgNum == FILE_DOWN) {
			SLGadgetMessage(scrollList, sfpMsgPort, intuiMsg);
			*item = gadgNum;
			return (TRUE);
		}
		if (gadgNum == BACK_BUTTON && class == GADGETUP &&
			(intuiMsg->Qualifier & ALTKEYS)) {
			ReplyMsg((MsgPtr) intuiMsg);
			*item = SFPMSG_DIRTOP;
			return (TRUE);
		}
		break;
/*
	Handle cursor keys
*/
	case RAWKEY:
		if (code == CURSORUP || code == CURSORDOWN) {
			ReplyMsg((MsgPtr) intuiMsg);
			selItem = SLNextSelect(scrollList, -1);
			if (selItem == -1)
				selItem = 0;
			else if (code == CURSORUP)
				selItem--;
			else
				selItem++;
			SLSelectItem(scrollList, selItem, TRUE);
			*item = FILE_LIST;
			return (TRUE);
		}
		if (code == CURSORLEFT || code == CURSORRIGHT) {
			ReplyMsg((MsgPtr) intuiMsg);
			*item = (code == CURSORLEFT) ? PREVDISK_ARROW : NEXTDISK_ARROW;
			gadget = GadgetItem(gadgList, *item);
			HiliteGadget(gadget, sfpDialog, NULL, TRUE);
			Delay(5);
			HiliteGadget(gadget, sfpDialog, NULL, FALSE);
			return (TRUE);
		}
		break;
/*
	Handle disk inserted or removed
*/
	case DISKINSERTED:
	case DISKREMOVED:
		ReplyMsg((MsgPtr) intuiMsg);
		*item = SFPMSG_DISK;
		return (TRUE);
	}
	if (dialogFilter)
		return ((*dialogFilter)(intuiMsg, item));
	return (FALSE);
}

/*
 *	Draw border around scroll list and disk arrows
 */

static void DrawDlgBorders()
{
	WORD xPos;
	register GadgetPtr gadget;
	RastPtr rPort = sfpDialog->RPort;
	BorderPtr border;

/*
	Draw list box border
*/
	SLDrawBorder(scrollList);
/*
	Draw disk arrows border
*/
	gadget = GadgetItem(gadgList, PREVDISK_ARROW);
#ifdef NEW_LOOK
	if ((border = ShadowBoxBorder(gadget->Width, gadget->Height, 0, TRUE)) != NULL) {
#else
	if ((border = BoxBorder(gadget->Width, gadget->Height, _tbDarkColor, -1)) != NULL) {
#endif
		xPos = gadget->LeftEdge;
		if (xPos < 0)
			xPos += sfpDialog->Width - 1;
		DrawBorder(rPort, border, xPos, gadget->TopEdge);
		FreeBorder(border);
	}
	gadget = GadgetItem(gadgList, NEXTDISK_ARROW);
#ifdef NEW_LOOK
	if ((border = ShadowBoxBorder(gadget->Width, gadget->Height, 0, TRUE)) != NULL) {
#else
	if ((border = BoxBorder(gadget->Width, gadget->Height, _tbDarkColor, -1)) != NULL) {
#endif
		xPos = gadget->LeftEdge;
		if (xPos < 0)
			xPos += sfpDialog->Width - 1;
		DrawBorder(rPort, border, xPos, gadget->TopEdge);
		FreeBorder(border);
	}
}

/*
 *	Handle simple request containing only buttons
 *	Return button number, or OK_BUTTON if not enough memory
 */

static WORD SimpleRequest(MsgPortPtr msgPort, WindowPtr window, WORD reqNum,
						  BOOL (*reqFilter)(IntuiMsgPtr, WORD *))
{
	WORD item;
	ReqTemplPtr reqTempl;
	RequestPtr request;

	switch (reqNum) {
	case REQ_FILEEXISTS:
		req2ReqTempl.Gadgets[REQ2_STATTEXT1].Info = fileExists1Text;
		req2ReqTempl.Gadgets[REQ2_STATTEXT2].Info = fileExists2Text;
		reqTempl = &req2ReqTempl;
		break;
	case REQ_BADNAME:
		req1ReqTempl.Gadgets[REQ1_STATTEXT].Info = badNameText;
		reqTempl = &req1ReqTempl;
		break;
	case REQ_NOFILE:
		req1ReqTempl.Gadgets[REQ1_STATTEXT].Info = noFileText;
		reqTempl = &req1ReqTempl;
		break;
	case REQ_DISKLOCK:
		req1ReqTempl.Gadgets[REQ1_STATTEXT].Info = diskLockText;
		reqTempl = &req1ReqTempl;
		break;
	default:
		return (OK_BUTTON);
	}
	if ((request = GetRequest(reqTempl, window, TRUE)) == NULL)
		return (OK_BUTTON);
	OutlineButton(request->ReqGadget, window, request, TRUE);
	SysBeep(5);
	item = ModalRequest(msgPort, window, reqFilter);
	EndRequest(request, window);
	DisposeRequest(request);
	return (item);
}

/*
 *	Handle directory switch operations
 */

static void DoDirSwitch(WORD item, BOOL (*fileFilter)(TextPtr), WORD numTypes,
						TextPtr typeList[])
{
	BOOL success;

	OffButtons();
	if (dlgType == GET_DIALOG)
		SetEditItemText(sfpDialog, NULL, FILENAME_TEXT, "");
	SetStdPointer(sfpDialog, POINTER_WAIT);
	if (item == OK_BUTTON) {
		atTop = FALSE;
		success = TRUE;		/* Have already switched to directory */
	}
	else if (item == ENTER_BUTTON)
		success = EnterDir();
	else if (item == BACK_BUTTON || item == SFPMSG_DIRTOP)
		success = ExitDir((BOOL) (item == SFPMSG_DIRTOP));
	else if (item == NEXTDISK_ARROW || item == PREVDISK_ARROW)
		success = ChangeVolume((BOOL) (item == NEXTDISK_ARROW));
	else if ((item == SFPMSG_DISK && atTop) ||
			 item == SFPMSG_RELIST)
		success = TRUE;
	else
		success = FALSE;
	if (success) {
		ClearFileList();
		ShowDirName();
		SetBackButton();
		GetFileList(fileFilter, numTypes, typeList);
		if (dlgType == GET_DIALOG && SLNumItems(scrollList) > 0)
			NewSelItem(0);
	}
	SetButtons();
	SetStdPointer(sfpDialog, POINTER_ARROW);
}

/*
 *	Handle click in file list
 *	Return FILE_LIST if single-click, OK_BUTTON if double-click on file,
 *		ENTER_BUTTON if double-click on directory
 */

static WORD DoFileList()
{
	WORD selItem;

	selItem = SLNextSelect(scrollList, -1);
	if (selItem == -1)
		return (FILE_LIST);
	NewSelItem(selItem);
	if (SLDoubleClick(scrollList)) {
		if (IsDirItem(selItem))
			return (ENTER_BUTTON);
		return (OK_BUTTON);
	}
	return (FILE_LIST);
}


/*
 *	SFPGetFile
 *
 *	Display a dialog to get a file name for input
 *	Parameters:
 *		screen is the screen in which the dialog is to appear
 *		msgPort is the Message Port to for intuition messages
 *		prompt is the window title
 *		dlgFilter(intuiMsg) is called by ModalDialog during dialog
 *		dlgHook(item, Dialog) is called after ModalDialog with the
 *			item number and pointer to the dialog
 *			Should return with item number or -1
 *		dlgTempl is dialog template to be used instead of default one
 *		numTypes is number of types in typeList array
 *			If 0 then tool types are not checked
 *		typeList is array of pointers to NULL terminated tool types
 *		fileFilter(fileName) will be called for each file
 *			Should return TRUE if file should be shown in list
 *			(reverse of Macintosh convention)
 *		sfReply is pointer to reply record
 */

void SFPGetFile(ScreenPtr screen,
				MsgPortPtr msgPort,
				TextPtr prompt,
				BOOL (*dlgFilter)(IntuiMsgPtr, WORD *),
				WORD (*dlgHook)(WORD, DialogPtr),
				DlgTemplPtr dlgTempl,
				WORD numTypes,
				TextPtr typeList[],
				BOOL (*fileFilter)(TextPtr),
				SFReplyPtr sfReply)
{
	register WORD item, result;
	register BOOL done, success;
	register GadgetPtr gadget;
	LONG lock;
	Dir dir;
	TextChar fileName[GADG_MAX_STRING];

	SetDialogIcons();
	if (dlgTempl == NULL)
		dlgTempl = &getFileDlgTempl;
	dlgTempl->Title = prompt;
	dlgType = GET_DIALOG;
	atTop = FALSE;
	sfpMsgPort = msgPort;
	dialogFilter = dlgFilter;	/* So that SFPDialogFilter() can call them */
	result = SFP_NOMEM;
	if ((scrollList = NewScrollList(FALSE)) == NULL)
		goto Exit2;
/*
	Bring up dialog
*/
	if ((sfpDialog = GetDialog(dlgTempl, screen, msgPort)) == NULL)
		goto Exit1;
	SetStdPointer(sfpDialog, POINTER_WAIT);
	gadgList = sfpDialog->FirstGadget;
	InitScrollList(scrollList, GadgetItem(gadgList, FILE_LIST), sfpDialog, NULL);
	DrawDlgBorders();
	OutlineButton(GadgetItem(gadgList, OK_BUTTON), sfpDialog, NULL, TRUE);
/*
	Read file names, and activate appropriate gadgets
*/
	OffButtons();
	if (dlgHook)
		(void) (*dlgHook)(SFPMSG_INIT, sfpDialog);
	success = ShowDirName();
	SetBackButton();
	if (success)
		GetFileList(fileFilter, numTypes, typeList);
	if (SLNumItems(scrollList) > 0)
		NewSelItem(0);
	SetButtons();
/*
	Get and process dialog messages
*/
	done = FALSE;
	SetStdPointer(sfpDialog, POINTER_ARROW);
	do {
		WaitPort(msgPort);
		item = CheckDialog(msgPort, sfpDialog, SFPDialogFilter);
		if (item != -1 && dlgHook)
			item = (*dlgHook)(item, sfpDialog);
/*
	Process item
*/
		switch (item) {
		case -1:				/* INTUITICKS message */
			SetButtons();
			break;
		case CANCEL_BUTTON:
			result = SFP_CANCEL;
			done = TRUE;
			break;
		case FILE_LIST:
			item = DoFileList();
			if (item == ENTER_BUTTON)
				goto DoNewDir;
			if (item != OK_BUTTON)
				break;				/* Else fall through */
		case OK_BUTTON:
			GetEditItemText(gadgList, FILENAME_TEXT, fileName);
			if (strlen(fileName) == 0 || (dir = CheckFileName(fileName)) == NULL) {
				(void) SimpleRequest(msgPort, sfpDialog, REQ_BADNAME, dlgFilter);
				break;
			}
			if (strlen(fileName) && (lock = Lock(fileName, ACCESS_READ)) != NULL) {
				UnLock(lock);
				strcpy(sfReply->Name, fileName);
				sfReply->DirLock = dir;
				result = SFP_OK;
				done = TRUE;
				break;
			}
			if (strlen(fileName))
				(void) SimpleRequest(msgPort, sfpDialog, REQ_NOFILE, dlgFilter);
			SetEditItemText(sfpDialog, NULL, FILENAME_TEXT, fileName);
			UnLock(dir);			/* Else fall through */
		case ENTER_BUTTON:
		case BACK_BUTTON:
		case NEXTDISK_ARROW:
		case PREVDISK_ARROW:
		case SFPMSG_DIRTOP:
		case SFPMSG_DISK:
		case SFPMSG_RELIST:
DoNewDir:
			DoDirSwitch(item, fileFilter, numTypes, typeList);
			break;
		}
		if (!done && item != -1 && !GadgetMsgAvail(msgPort, sfpDialog) &&
			!GadgetSelected(gadgList, CANCEL_BUTTON)) {
			gadget = GadgetItem(gadgList, FILENAME_TEXT);
			ActivateGadget(gadget, sfpDialog, NULL);
		}
	} while (!done);
/*
	Dispose of dialog
*/
	DisposeDialog(sfpDialog);
/*
	Dispose of scroll list
*/
Exit1:
	DisposeScrollList(scrollList);
/*
	Return with specified result
*/
Exit2:
	sfReply->Result = result;
}

/*
 *	SFPPutFile
 *
 *	Display a dialog to get a file name for output
 *	Parameters:
 *		screen is the screen in which the dialog is to appear
 *		msgPort is the Message Port to for intuition messages
 *		prompt is NULL terminated prompt string
 *		origName is NULL terminated string for original file name
 *		dlgFilter(intuiMsg) is called by ModalDialog during dialog
 *		dlgHook(item, dialog) is called after ModalDialog with the
 *			item number and pointer to the dialog
 *			Should return with item number or -1
 *		dlgTempl is dialog template to be used instead of default one
 *		sfReply is pointer to reply record
 */

void SFPPutFile(ScreenPtr screen,
				MsgPortPtr msgPort,
				TextPtr prompt,
				TextPtr origName,
				BOOL (*dlgFilter)(IntuiMsgPtr, WORD *),
				WORD (*dlgHook)(WORD, DialogPtr),
				DlgTemplPtr dlgTempl,
				SFReplyPtr sfReply)
{
	register WORD item, result;
	register BOOL done, success;
	register GadgetPtr gadget;
	WORD origLen, newLen;
	BPTR fileLock;
	Dir dir;
	TextChar fileName[GADG_MAX_STRING];

	SetDialogIcons();
	if (dlgTempl == NULL)
		dlgTempl = &putFileDlgTempl;
	dlgTempl->Gadgets[PUT_PROMPT].Info = prompt;
	dlgTempl->Gadgets[FILENAME_TEXT].Info = origName;
	dlgType = PUT_DIALOG;
	atTop = FALSE;
	sfpMsgPort = msgPort;
	dialogFilter = dlgFilter;	/* So that SFPDialogFilter() can call them */
	result = SFP_NOMEM;
	if ((scrollList = NewScrollList(FALSE)) == NULL)
		goto Exit2;
/*
	Set up dialog
*/
	if ((sfpDialog = GetDialog(dlgTempl, screen, msgPort)) == NULL)
		goto Exit1;
	SetStdPointer(sfpDialog, POINTER_WAIT);
	gadgList = sfpDialog->FirstGadget;
	InitScrollList(scrollList, GadgetItem(gadgList, FILE_LIST), sfpDialog, NULL);
	ModifyIDCMP(sfpDialog, sfpDialog->IDCMPFlags | (DISKINSERTED|DISKREMOVED));
	DrawDlgBorders();
	OutlineButton(GadgetItem(gadgList, OK_BUTTON), sfpDialog, NULL, TRUE);
/*
	Read file names, and activate appropriate gadgets
*/
	OffButtons();
	if (dlgHook)
		(void) (*dlgHook)(SFPMSG_INIT, sfpDialog);
	success = ShowDirName();
	SetBackButton();
	if (success)
		GetFileList(NULL, 0, NULL);
	SetButtons();
/*
	Get and process dialog messages
*/
	done = FALSE;
	SetStdPointer(sfpDialog, POINTER_ARROW);
	do {
		WaitPort(msgPort);
		item = CheckDialog(msgPort, sfpDialog, SFPDialogFilter);
		if (item != -1 && dlgHook)
			item = (*dlgHook)(item, sfpDialog);
/*
	Process item
*/
		switch (item) {
		case -1:				/* INTUITICKS message */
			SetButtons();
			break;
		case CANCEL_BUTTON:
			result = SFP_CANCEL;
			done = TRUE;
			break;
		case FILE_LIST:
			item = DoFileList();
			if (item == ENTER_BUTTON)
				goto DoNewDir;
			if (item != OK_BUTTON)
				break;				/* Else fall through */
		case OK_BUTTON:
			GetEditItemText(gadgList, FILENAME_TEXT, fileName);
			origLen = strlen(fileName);
			if (origLen == 0 || (dir = CheckFileName(fileName)) == NULL) {
				(void) SimpleRequest(msgPort, sfpDialog, REQ_BADNAME, dlgFilter);
				break;
			}
			newLen = strlen(fileName);
			if (newLen) {
				if (DiskIsLocked()) {
					(void) SimpleRequest(msgPort, sfpDialog, REQ_DISKLOCK, dlgFilter);
					item = CANCEL_BUTTON;
				}
				else if ((fileLock = Lock(fileName, ACCESS_READ)) != NULL) {
					UnLock(fileLock);
					item = SimpleRequest(msgPort, sfpDialog, REQ_FILEEXISTS, dlgFilter);
				}
				if (item == OK_BUTTON) {
					strcpy(sfReply->Name, fileName);
					sfReply->DirLock = dir;
					result = SFP_OK;
					done = TRUE;
					break;
				}
				else if (newLen == origLen)
					break;	/* Don't re-display directory if it didn't change */
				item = OK_BUTTON;
			}
			SetEditItemText(sfpDialog, NULL, FILENAME_TEXT, fileName);
			UnLock(dir);			/* Else fall through */
		case ENTER_BUTTON:
		case BACK_BUTTON:
		case NEXTDISK_ARROW:
		case PREVDISK_ARROW:
		case SFPMSG_DIRTOP:
		case SFPMSG_DISK:
		case SFPMSG_RELIST:
DoNewDir:
			DoDirSwitch(item, NULL, 0, NULL);
			break;
		}
		if (!done && item != -1 && !GadgetMsgAvail(msgPort, sfpDialog) &&
			!GadgetSelected(gadgList, CANCEL_BUTTON)) {
			gadget = GadgetItem(gadgList, FILENAME_TEXT);
			ActivateGadget(gadget, sfpDialog, NULL);
		}
	} while (!done);
/*
	Dispose of dialog
*/
	DisposeDialog(sfpDialog);
/*
	Dispose of scroll list
*/
Exit1:
	DisposeScrollList(scrollList);
/*
	Return with specified result
*/
Exit2:
	sfReply->Result = result;
}

/*
 *	Convert file name to be a pure file name (no path specifiers)
 *	Path name is relative to current directory
 *	Return directory lock of path to file, or NULL if no such path
 */

Dir ConvertFileName(fileName)
register TextPtr fileName;
{
	register TextChar ch;
	register WORD i, start, len;
	register Dir dir;

	len = strlen(fileName);
/*
	Scan for ':' or '/' from end of name back
	If found, get lock on directory portion and change text to simple file name
*/
	for (start = len; start > 0; start--) {
		if (fileName[start - 1] == ':' || fileName[start - 1] == '/') {
			ch = fileName[start];
			fileName[start] = '\0';
			dir = Lock(fileName, ACCESS_READ);
			fileName[start] = ch;
			for (i = start; i <= len; i++)
				fileName[i - start] = fileName[i];
			return (dir);
		}
	}
/*
	No path specifier found, so return duplicate of current directory lock
*/
	return (DupLock(((ProcessPtr) FindTask(NULL))->pr_CurrentDir));
}

/*
 *	Set current directory to directory specified by given lock
 *	Uses a copy of the supplied lock
 */

void SetCurrentDir(Dir dir)
{
	register Dir oldLock, newLock;

	newLock = DupLock(dir);
	oldLock = CurrentDir(newLock);
	UnLock(oldLock);
}
