/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Pattern table routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <proto/exec.h>

#include <string.h>

#include <Toolbox/Memory.h>
#include <Toolbox/List.h>

#include "Draw.h"
#include "Proto.h"

/*
 *	Local variables and definitions
 */

typedef struct PatEntry {
	struct PatEntry	*Next;
	RGBPat8			Pat;
} PatEntry, *PatEntryPtr;

static PatEntry	*patList;		/* Pointer to first entry in table */
								/* Inited to NULL */

/*
 *	Local prototypes
 */

static BOOL	EqualPat(RGBPat8Ptr, RGBPat8Ptr);

/*
 *	Compare two patterns
 *	Return TRUE if they are the same
 */

static BOOL EqualPat(pat1, pat2)
register RGBPat8Ptr pat1, pat2;
{
	register WORD i;

	for (i = 0; i < 8*8; i++) {
		if ((*pat1)[i] != (*pat2)[i])
			return (FALSE);
	}
	return (TRUE);
}

/*
 *	Add pattern entry to pattern table
 *	Do not add if entry for pattern already exists
 *	Return FALSE if not enough memory
 */

BOOL AddPatEntry(RGBPat8Ptr pat)
{
	register PatEntryPtr newEntry, prevEntry;

	prevEntry = NULL;
	for (newEntry = patList; newEntry; newEntry = newEntry->Next) {
		if (EqualPat(pat, &newEntry->Pat))
			return (TRUE);
		prevEntry = newEntry;
	}
	if ((newEntry = MemAlloc(sizeof(PatEntry), 0)) == NULL)
		return (FALSE);
	newEntry->Next = NULL;
	CopyRGBPat8(pat, &newEntry->Pat);
	if (prevEntry)
		prevEntry->Next = newEntry;
	else
		patList = newEntry;
	return (TRUE);
}

/*
 *	Get pat number of pattern
 *	Return -1 if pattern not found
 */

FillPatNum GetPatEntry(RGBPat8Ptr pat)
{
	register FillPatNum i;
	register PatEntryPtr patEntry;

	for (patEntry = patList, i = 0; patEntry; patEntry = patEntry->Next, i++) {
		if (EqualPat(pat, &patEntry->Pat))
			return (i);
	}
	return (-1);
}

/*
 *	Get pointer to pattern of given table entry number
 *	Return NULL if no such item
 */

RGBPat8Ptr PatTableItem(FillPatNum itemNum)
{
	register PatEntryPtr patEntry;

	patEntry = patList;
	while (patEntry && itemNum--)
		patEntry = patEntry->Next;
	return ((patEntry) ? &patEntry->Pat : NULL);
}

/*
 *	Dispose of all table entries
 */

void DisposePatTable()
{
	register PatEntryPtr patEntry, nextEntry;

	for (patEntry = patList; patEntry; patEntry = nextEntry) {
		nextEntry = patEntry->Next;
		MemFree(patEntry, sizeof(PatEntry));
	}
	patList = NULL;
}
