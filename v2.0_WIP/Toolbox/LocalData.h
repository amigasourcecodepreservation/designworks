/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Toolbox library
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	Localization information
 */

#ifndef TOOLBOX_LANGUAGE_H
#include <Toolbox/Language.h>
#endif

/*
 *	Default time type
 */

#if (AMERICAN | SPANISH)

#define TIME_12HOUR	(TRUE)

#elif (BRITISH | GERMAN | FRENCH | SWEDISH)

#define TIME_12HOUR	(FALSE)

#endif

/*
 *	Decimal and thousand separator chars
 */

#if (AMERICAN | BRITISH | SPANISH)

#define DECIMAL_CHAR	('.')
#define THOUSAND_SEP	(',')

#elif (GERMAN | FRENCH | SWEDISH)

#define DECIMAL_CHAR	(',')
#define THOUSAND_SEP	('.')

#endif

/*
 *	Measurement systems
 */

enum {
	MEASURE_INCH,	MEASURE_CM
};

#if (AMERICAN | BRITISH)

#define MEASURE_DEFAULT	MEASURE_INCH

#elif (GERMAN | FRENCH | SPANISH | SWEDISH)

#define MEASURE_DEFAULT	MEASURE_CM

#endif
