/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Toolbox library
 *	Copyright (c) 1993 New Horizons Software, Inc.
 *
 *	Packet I/O routines
 */

#ifndef TOOLBOX_PACKET_H
#define TOOLBOX_PACKET_H

#ifndef EXEC_TYPES_H
#include <exec/types.h>
#endif

#ifndef TYPEDEFS_H
#include <Typedefs.h>
#endif

#ifndef DOS_DOS_H
#include <dos/dos.h>
#endif

#ifndef DOS_DOSEXTENS_H
#include <dos/dosextens.h>
#endif

/*
 *	Definitions
 */

typedef struct StandardPacket		StandardPacket, *StdPktPtr;

/*
 *	Prototypes
 */

BOOL	DoPacket(MsgPortPtr, LONG, LONG [], WORD);
LONG	PacketError(void);

#endif
