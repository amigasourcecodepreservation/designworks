/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Designworks
 *	Copyright (c) 1990 New Horizons Software, Inc.
 *
 *	Clipboard routines
 */

#include <exec/types.h>
#include <devices/clipboard.h>
#include <libraries/iffparse.h>

#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/iffparse.h>
#include <proto/graphics.h>

#include <Toolbox/Utility.h>
#include <Toolbox/Memory.h>
#include <Toolbox/Screen.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Font.h>
#include <Toolbox/Window.h>

#include <IFF/Packer.h>
#include <IFF/ILBM.h>

#include <string.h>

#include "Draw.h"
#include "Proto.h"

#define ID_FTXT	MakeID('F','T','X','T')
#define ID_CHRS	MakeID('C','H','R','S')
#define ID_CLIP MakeID('C','L','I','P')
#define ID_EPSF MakeID('E','P','S','F')

#define CLIP_OK 	0L
#define CLIP_ERR	1L

extern ScreenPtr	screen;

extern DocLayer	pasteLayer;

extern UBYTE	fileBuff[];

extern TextChar	strBuff[];

extern Defaults	defaults;
extern Options 	options;

extern struct IFFParseBase *IFFParseBase;
extern MsgPortPtr	clipboardMsgPort;

extern RGBColor	export4[], export8[], export16[], export32[], export64[];

extern struct 	ClipboardHandle *clipboard;

/*
	local variables
 */

static BOOL		pasteText;

static struct 	IFFHandle *iff;

static LONG		lastID, postID;

static LONG		ilbmProps[] =
	{ ID_ILBM, ID_BMHD,	ID_ILBM, ID_CMAP, ID_ILBM, ID_CAMG, ID_ILBM, ID_BODY };
/*
 *  local prototypes
 */

static LONG GetClipCMAP(Color4 *, WORD *,LONG);
static LONG GetClipBODY(struct ContextNode *, struct BitMap *, PLANEPTR ,
			 BitMapHeader *, BYTE *, LONG);
static LONG GetClipFormILBM(WindowPtr, DocLayerPtr);
static LONG GetClipFormEPSF(DocLayerPtr);
static BOOL	MustReadClip(void);

static LONG PutClipCAMG(LONG);
static LONG PutClipBMHD(BitMapHeader *);
static LONG PutClipCMAP(Color4 *, WORD );
static LONG PutClipBODY(struct BitMap *, PLANEPTR , BitMapHeader *, BYTE *, LONG);
static LONG PutFormILBM(DocLayerPtr);

static LONG WriteText(DocLayerPtr);
static LONG WriteFTXT(DocLayerPtr);
static LONG WriteILBM(DocLayerPtr);
static LONG WriteEPSF(EPSFObjPtr);

void InitClipboard()
{
	if (IFFParseBase == NULL) {
		if (options.FullClipboard)
			Error(ERR_NO_PARSE);
		return;
	}
	if ((clipboard = OpenClipboard(PRIMARY_CLIP)) == NULL) {
		Error(ERR_NO_CLIP);
		return;
	}

	iff = AllocIFF();
	if (iff)
		iff->iff_Stream = (LONG)clipboard;
	InitIFFasClip(iff);

	if ((clipboardMsgPort = CreatePort(NULL, 0)) == NULL) {
		ShutDownClipboard();
		return;
	}
}

void ShutDownClipboard()
{
	struct SatisfyMsg *msg;

	if (IFFParseBase == NULL)
		return;

	if (clipboard)
		CloseClipboard(clipboard);
	if (iff)
		FreeIFF(iff);

	if (clipboardMsgPort) {
		while ((msg = (struct SatisfyMsg *)GetMsg(clipboardMsgPort)) != NULL)
			ReplyMsg((MsgPtr) msg);
		DeletePort(clipboardMsgPort);
	}
}

#define GetClipBMHD(iff, bmHdr) \
	ReadChunkBytes(iff, bmHdr, sizeof(BitMapHeader))

#define GetClipCAMG(iff, type) \
	ReadChunkBytes(iff, type, sizeof(LONG))

/*
 *	GetCMAP
 *	pNColorRegs is passed in as a pointer to the number of ColorRegisters
 *	caller has space to hold.  GetCMAP sets to the number actually read
 */

static LONG GetClipCMAP(Color4 *colorMap, WORD *pNColorRegs,LONG size)
{
	register WORD nColorRegs;
	ColorRegister colorReg;

	nColorRegs = size/sizeofColorRegister;
	if (*pNColorRegs < nColorRegs)
		nColorRegs = *pNColorRegs;
	*pNColorRegs = nColorRegs;		/* Set to the number actually there */
	for (; nColorRegs > 0; --nColorRegs)  {
		if (ReadChunkBytes(iff, &colorReg, sizeofColorRegister) == 0)
			return (CLIP_ERR);
		*colorMap++ = ((colorReg.red   >> 4 ) << 8) |
					  ((colorReg.green >> 4 ) << 4) |
					  ((colorReg.blue  >> 4 )	 );
	}
	return (CLIP_OK);
}

/*
 *	GetBODY
 *	NOTE: This implementation could be a LOT faster if it used more of the
 *	supplied buffer. It would make far fewer calls to IFFReadBytes (and
 *	therefore to DOS Read) and to movemem
 */

static LONG GetClipBODY(struct ContextNode *context, struct BitMap *bitmap, PLANEPTR mask,
			 BitMapHeader *bmHdr, BYTE *buffer, LONG bufsize)
{
	UBYTE srcPlaneCnt = bmHdr->nPlanes;	/* Haven't counted for mask plane yet */
	WORD srcRowBytes = RowBytes(bmHdr->w);
	LONG bufRowBytes = MaxPackedSize(srcRowBytes);
	WORD nRows;
	register WORD iPlane, iRow, nEmpty;
	register WORD nFilled;
	BYTE *buf, *nullDest, *nullBuf, **pDest;
	PLANEPTR planes[MaxSrcPlanes];	/* Array of ptrs to planes & mask */

/*
	Complain if client asked for a conversion GetBODY doesn't handle
*/
	if (bmHdr->compression > cmpByteRun1 || srcRowBytes != bitmap->BytesPerRow ||
		bufsize < bufRowBytes*2 || srcPlaneCnt > MaxSrcPlanes)
		return (CLIP_ERR);
	nRows = bmHdr->h;
	if (nRows > bitmap->Rows)
		nRows = bitmap->Rows;
/*
	Initialize array "planes" with bitmap ptrs; NULL in empty slots
*/
	for (iPlane = 0; iPlane < bitmap->Depth; iPlane++)
		planes[iPlane] = bitmap->Planes[iPlane];
	while (iPlane < MaxSrcPlanes)
		planes[iPlane++] = NULL;
/*
	Copy any mask plane ptr into corresponding "planes" slot
	If there are more srcPlanes than dstPlanes, there will be
		NULL plane-pointers before this
*/
	if (bmHdr->masking == mskHasMask)
			planes[srcPlaneCnt++] = mask;
/*
	Setup a sink for dummy destination of rows from unwanted planes
*/
	nullDest = buffer;
	buffer  += srcRowBytes;
	bufsize -= srcRowBytes;
/*
	Read the BODY contents into client's bitmap
	De-interleave planes and decompress rows
	MODIFIES: Last iteration modifies bufsize
*/
	buf = buffer + bufsize;				/* Buffer is currently empty */
	for (iRow = nRows; iRow > 0; iRow--)  {
		for (iPlane = 0; iPlane < srcPlaneCnt; iPlane++)  {
			NextBusyPointer();
			pDest = &planes[iPlane];
/*
	Establish a sink for any unwanted plane
*/
			if (*pDest == NULL) {
				nullBuf = nullDest;
				pDest   = &nullBuf;
			}
/*
	Read in at least enough bytes to uncompress next row
*/
			nEmpty  = buf - buffer;		/* Size of empty part of buffer */
			nFilled = bufsize - nEmpty;		/* This part has data */
			if (nFilled < bufRowBytes) {
/*
	Need to read more
	Move the existing data to the front of the buffer
	Now covers range buffer[0]..buffer[nFilled-1]
*/
				BlockMove(buf, buffer, nFilled);		/* Could be moving 0 bytes */
				if (nEmpty > (context->cn_Size - context->cn_Scan )) {
/*
	There aren't enough bytes left to fill the buffer
*/
					nEmpty = (context->cn_Size - context->cn_Scan );
					bufsize = nFilled + nEmpty;				/* heh-heh */
				}
/*
	Append new data to the existing data
*/
				if (ReadChunkBytes(iff, &buffer[nFilled], nEmpty) < 0)
					return (CLIP_ERR);
				buf	 = buffer;
				nFilled = bufsize;
				nEmpty  = 0;
			}
/*
	Copy uncompressed row to destination plane
*/
			if (bmHdr->compression == cmpNone) {
				if (nFilled < srcRowBytes)
					return (BAD_FORM);
				BlockMove(buf, *pDest, srcRowBytes);
				buf	+= srcRowBytes;
				*pDest += srcRowBytes;
			}
/*
	Decompress row to destination plane
*/
			else if (UnPackRow(&buf, pDest, nFilled, srcRowBytes))
				return (CLIP_ERR);
		}
	}
	return (CLIP_OK);
}

/*
 *	Read ILBM FORM
 */

static LONG GetClipFormILBM(WindowPtr window, DocLayerPtr pasteLayer)
{
	register LONG iffp;
	register WORD depth;
	BOOL 			foundBMHD;
	LONG viewMode;				/* Need to take address of this */
	register IFFPictPtr		pict;
	struct ContextNode	 	*context;
	BitMapHeader			bmHdr;
	LONG					length;
	LONG					result;

	if ((pict = MemAlloc(sizeof(IFFPict), MEMF_CLEAR)) == NULL) {
		Error(ERR_NO_MEM);
		return (CLIP_ERR);
	}

	GetColorTable(screen,&pict->ColorTable);
	pict->NumColors = 1 << screen->BitMap.Depth;

	context = CurrentChunk(iff);
	length = context->cn_Size;
	viewMode = 0;

	do {
		if (length <= 0)
			return (CLIP_ERR);
		switch (context->cn_ID) {
/*
	Get Bit Map Header chunk
*/
		case ID_BMHD:
			foundBMHD = TRUE;
			if ((iffp = GetClipBMHD(iff, &bmHdr)) == sizeof(BitMapHeader))
				iffp = CLIP_OK;
			pict->Width = bmHdr.w;
			pict->Height = bmHdr.h;
			pict->TranspColor = (bmHdr.masking == mskHasTransparentColor) ?
								   bmHdr.transparentColor : -1;
			break;
/*
	Get Color Map chunk
*/
		case ID_CMAP:
			pict->NumColors = MAX_COLOR_REGS;
			iffp = GetClipCMAP(&pict->ColorTable[0], &pict->NumColors,length);
			break;
/*
	Get Amiga viewport modes chunk
*/
		case ID_CAMG:
			if ((iffp = GetClipCAMG(iff, &viewMode)) == sizeof(LONG))
				iffp = CLIP_OK;
			break;
/*
	Get Body chunk
*/
		case ID_BODY:
			if (!foundBMHD || pict->NumColors == 0) {
				iffp = BAD_FORM;
				break;
			}
			if (pict->BitMap) {
				if (pict->Mask)
					FreeRaster(pict->Mask, pict->BitMap->BytesPerRow*8,
							   pict->BitMap->Rows);
					DisposeBitMap(pict->BitMap);
			}
			pict->BitMap = NULL;
			pict->Mask = NULL;
			depth = MIN(bmHdr.nPlanes, MAX_DEPTH);
			if (depth > 8 && depth != 24) {
				iffp = BAD_FORM;
				break;
			}
			pict->BitMap = CreateBitMap(bmHdr.w, bmHdr.h, depth, FALSE);
			if (pict->BitMap == NULL) {
				iffp = CLIENT_ERROR;
				break;
			}
			if (bmHdr.masking == mskHasMask) {
				pict->Mask = AllocRaster(bmHdr.w, bmHdr.h);
				if (pict->Mask == NULL) {
					iffp = CLIENT_ERROR;
					break;
				}
			}
			iffp = GetClipBODY(context, pict->BitMap, pict->Mask, &bmHdr,
							   fileBuff, FILEBUFF_SIZE);
			if (iffp != CLIP_OK) {
				DisposePict(pict);
				break;
			}
			break;
		}
		if (iffp != CLIP_OK)
			break;
		result = ParseIFF(iff,IFFPARSE_SCAN);
		if (result == IFFERR_NOTIFF) {
			iffp = 1;
			break;
		}
		if (result == IFFERR_EOF)
			break;
		context = CurrentChunk(iff);
		length = context->cn_Size;
		NextBusyPointer();
	} while (context->cn_Type == ID_ILBM);

/*
	Fix up improper half-brite pictures
*/
	if (iffp == CLIP_OK) {
    	if (pict->BitMap) {
			depth = pict->BitMap->Depth;
			if (pict->NumColors == (1 << depth)/2 && (pict->ViewMode & HAM) == 0)
				pict->ViewMode |= EXTRA_HALFBRITE;
		}
		iffp = BMapConvert(window,pict, NULL, NULL, TRUE) ? CLIP_OK : 1;
	}
	else
		DisposePict(pict);

	return (iffp);
}

static LONG GetClipFormEPSF(DocLayerPtr layer)
{
	EPSFObjPtr				epsfObj;
	struct ContextNode	 	*context;
	LONG					length;
	BOOL					success;

	context = CurrentChunk(iff);
	length = context->cn_Size;

	if ((epsfObj = EPSFAllocate()) == NULL)
		return (CLIP_ERR);
	epsfObj->DocObj.Type = TYPE_EPSF;

	success = FALSE;
	if ((epsfObj->File = MemAlloc(length, MEMF_CLEAR)) != NULL) {
		success = (ReadChunkBytes(iff, epsfObj->File, length) == length);
	}
	if (success) {
		epsfObj->FileSize = length;
		success = SetUpEPSFObj(epsfObj);
		epsfObj->DocObj.Frame = epsfObj->BoundingBox;
		OffsetRect(&epsfObj->DocObj.Frame, -epsfObj->BoundingBox.MinX, -epsfObj->BoundingBox.MinY);
	}
	if (!success)
		EPSFDispose(epsfObj);
	else {
		ClearPaste();
		AppendObject(layer, (DocObjPtr) epsfObj);
	}

	return ((success) ? CLIP_OK : CLIP_ERR);
}

/*
 *	check current clipboard ID to see if prowrite ID or if clipboard is empty
 */

static BOOL MustReadClip()
{
	struct IOClipReq *clipIO;

	if ((IFFParseBase == NULL) || (clipboard == NULL))
		return (CLIP_ERR);

	clipIO = &(clipboard->cbh_Req);
	clipIO->io_Command = CBD_CURRENTREADID;

	DoIO((IOReqPtr)clipIO);

// must read if clip was not last posted or written by prowrite
	return (clipIO->io_ClipID != lastID && clipIO->io_ClipID != postID);
}

/*
 *	check current clipboard ID to see if flow ID or if clipboard is empty
 */

BOOL MustWriteClip()
{
	struct IOClipReq *clipIO;

	if ((IFFParseBase == NULL) || (clipboard == NULL))
		return (CLIP_ERR);

	clipIO = &(clipboard->cbh_Req);
	clipIO->io_Command = CBD_CURRENTWRITEID;

	DoIO((IOReqPtr)clipIO);

	return (clipIO->io_ClipID <= lastID);
}

/*
 *	Create a text object of the default values and put it in the paste layer
 */

static void MakeTextObject(WindowPtr window, DocLayerPtr layer, TextPtr text)
{
	WORD xStart, yStart, xEnd, yEnd;
	WORD xSize, ySize, baseline;
	TextPort textPort;
	RastPort rPort;
	FontInfo fontInfo;
	WORD len;
	TextObjPtr textObj;
	DocDataPtr	docData = GetWRefCon(window);

	if ((textObj = (TextObjPtr) NewDocObject(layer, TYPE_TEXT)) == NULL)
		return;

	WindowToDoc(window, 0, 0, &xStart, &yStart);
	SnapToGrid(docData, &xStart, &yStart);
	xEnd = xStart;
	yEnd = yStart;
	InitRastPort(&rPort);
	textPort.RastPort = &rPort;
	TPTextFont(&textPort, defaults.FontNum);
	TPTextSize(&textPort, defaults.FontSize);
	TPTextFace(&textPort, defaults.Style, defaults.MiscStyle);
	TPGetFontInfo(&textPort, &fontInfo);
	if (fontInfo.Ascent + fontInfo.Descent == 0) {
		TextDispose(textObj);
		return;
	}
	xSize = fontInfo.WidMax;
	ySize = fontInfo.Ascent + fontInfo.Descent;
	baseline = fontInfo.Ascent;
	if (xEnd < xStart + xSize && yEnd < yStart + ySize) {
		if (defaults.Justify == JUSTIFY_CENTER)
			xStart -= 10*xSize;
		else if (defaults.Justify == JUSTIFY_RIGHT)
			xStart -= 20*xSize;
		if (xStart < 0)
			xStart = 0;
		xEnd = xStart + 20*xSize;
		if (xEnd >= docData->DocWidth)
			xEnd = docData->DocWidth - 1;
		yStart -= baseline;
		if (yStart < 0)
			yStart = 0;
	}
	yEnd = yStart + ySize;
/*
	Create text object
*/
	SetFrameRect(&textObj->DocObj.Frame, xStart, yStart, xEnd, yEnd);
	textObj->DocObj.Flags	= (defaults.ObjFlags & ~OBJ_DO_FILL) | OBJ_DO_PEN;
	textObj->TextFlags		= 0;
	textObj->PenColor		= (defaults.PenColor != RGBCOLOR_WHITE) ?
							  defaults.PenColor : RGBCOLOR_BLACK;
	textObj->SelStart		= textObj->SelEnd = 0;
	textObj->FontNum		= defaults.FontNum;
	textObj->FontSize		= defaults.FontSize;
	textObj->Style			= defaults.Style;
	textObj->MiscStyle		= defaults.MiscStyle;
	textObj->Justify		= defaults.Justify;
	textObj->Spacing		= defaults.Spacing;
	textObj->Rotate			= 0;
	CopyRGBPat8(&defaults.FillPat, &textObj->FillPat);
/*
	Add text to object
*/
	if (text == NULL) {
		textObj->Text		= NULL;
		textObj->TextLen	= 0;
	}
	else {
		len = strlen(text);
		if ((textObj->Text = MemAlloc(len, 0)) == NULL) {
			TextDispose(textObj);
			return;
		}
		strcpy(textObj->Text, text);
		textObj->TextLen = len;
	}
	TextAdjustFrame(textObj);
}

/*
 *  Get readable formats from system clipboard
 */

void GetClipboard(WindowPtr window)
{
	LONG 			result;
	LONG			length=0L;
	TextPtr			buff=NULL;
	struct 			ContextNode *context;
	BOOL			opened = FALSE;

	if (IFFParseBase == NULL || clipboard == NULL)
		return; /* no library or no clipboard.device */

	if (!MustReadClip())
		return;

	SetBusyPointer(window);
	if (iff) {
		StopChunk(iff,ID_FTXT,ID_CHRS);
		StopChunks(iff,ilbmProps,4);
		StopChunk(iff, ID_EPSF, ID_BODY);
		if (OpenIFF(iff,IFFF_READ) == CLIP_OK) {
			opened = TRUE;
			do {
				result = ParseIFF(iff,IFFPARSE_SCAN);
				if (result == IFFERR_EOF || result == IFFERR_NOTIFF)
					goto Exit;
				if ((context = CurrentChunk(iff)) && ((length = context->cn_Size) > 0L)) {
					switch (context->cn_Type) {
						case ID_ILBM :
							result = GetClipFormILBM(window, &pasteLayer);
							break;
						case ID_FTXT :
							buff = MemAlloc(length + 1, MEMF_CLEAR);
							if (ReadChunkBytes(iff,buff,length) == length)
								result = CLIP_OK;
							break;
						case ID_EPSF :
							result = GetClipFormEPSF(&pasteLayer);
							break;
					}
					if (result == CLIP_OK)
						goto Exit;
				}
			} while (TRUE);
		}
	}
Exit:
	if (opened)
		CloseIFF(iff);
	if (buff) {
		ClearPaste();
		MakeTextObject(window, &pasteLayer, buff);
	}
}

/*
 *	Put view mode chunk
 */

#define ID_CAMG	MakeID('C','A','M','G')

static LONG PutClipCAMG(LONG viewMode)
{
	LONG	retval;

	if ((retval = PushChunk(iff, 0, ID_CAMG, sizeof(LONG))) == 0) {
		if (WriteChunkBytes(iff, (BYTE *)viewMode, sizeof(LONG)) >= 0) {
			retval = 0;
			PopChunk(iff);
		}
	}
	return (retval);
}

/*
 *	PutCMAP
 */

static LONG PutClipCMAP(register Color4 *colorMap, WORD depth)
{
	register LONG iffp;
	register LONG nColorRegs;
	ColorRegister colorReg;

	if (depth > MaxAmDepth)
		depth = MaxAmDepth;
	nColorRegs = 1 << depth;
	iffp = PushChunk(iff, 0, ID_CMAP, nColorRegs*sizeofColorRegister);
	while (iffp == CLIP_OK && nColorRegs--) {
		colorReg.red   = (*colorMap >> 4) & 0xF0;
		colorReg.green = (*colorMap	 ) & 0xF0;
		colorReg.blue  = (*colorMap << 4) & 0xF0;
		if ((iffp = WriteChunkBytes(iff, &colorReg, sizeofColorRegister)) >= 0)
			iffp = CLIP_OK;
		colorMap++;
		NextBusyPointer();
	}
	if (iffp == CLIP_OK)
		PopChunk(iff);
	return (iffp);
}

/*
 *	PutBODY
 *	NOTE: This implementation could be a LOT faster if it used more of the
 *		supplied buffer
 *	It would make far fewer calls to IFFWriteBytes (and therefore to DOS Write)
 */

static LONG PutClipBODY(register struct BitMap *bitMap, PLANEPTR mask,
			 register BitMapHeader *bmHdr, BYTE *buffer, LONG bufSize)
{
	register LONG iffp;
	LONG rowBytes = bitMap->BytesPerRow;
	Compression compression = bmHdr->compression;
	WORD dstDepth = bmHdr->nPlanes;
	register WORD iPlane, iRow;
	register LONG packedRowBytes;
	BYTE *buf;
	PLANEPTR planes[MaxAmDepth + 1];	/* Array of ptrs to planes and mask */

	if (bufSize < MaxPackedSize(rowBytes) || compression > cmpByteRun1 ||
		bitMap->Rows != bmHdr->h || rowBytes != RowBytes(bmHdr->w) ||
		dstDepth > bitMap->Depth || dstDepth > MaxAmDepth)
		return (CLIP_ERR);
/*
	Copy the ptrs to bit & mask planes into local array
*/
	for (iPlane = 0; iPlane < dstDepth; iPlane++)
		planes[iPlane] = bitMap->Planes[iPlane];
	if (mask)
		planes[dstDepth++] = mask;
/*
	Write out a BODY chunk header
*/
	iffp = PushChunk(iff, 0,ID_BODY, -1L);
/*
	Write out the BODY contents
*/
	for (iRow = 0; iffp == CLIP_OK && iRow < bmHdr->h; iRow++) {
		for (iPlane = 0; iffp == CLIP_OK && iPlane < dstDepth; iPlane++) {
/*
	Compress and write next row
*/
			if (compression == cmpByteRun1) {
				buf = buffer;
				packedRowBytes = PackRow(&planes[iPlane], &buf, rowBytes);
				if ((iffp = WriteChunkBytes(iff, buffer, packedRowBytes)) >= 0)
					iffp = CLIP_OK;
			}
/*
	Write next row
*/
			else {
				if ((iffp = WriteChunkBytes(iff, planes[iPlane], rowBytes)) >= 0)
					iffp = CLIP_OK;
				planes[iPlane] += rowBytes;
			}
		}
		NextBusyPointer();
	}
/*
	Finish the chunk
*/
	if (iffp == CLIP_OK)
		PopChunk(iff);
	return (iffp);
}

static LONG PutClipBMHD(BitMapHeader *bmHdr)
{
	LONG	retval;

	if ((retval = PushChunk(iff, 0, ID_BMHD, sizeof(BitMapHeader))) == 0) {
		if (WriteChunkBytes(iff, (BYTE *)bmHdr, sizeof(BitMapHeader)) >= 0) {
			retval = 0;
			PopChunk(iff);
		}
	}
	return (retval);
}

/*
	get rectangle containing selected items including pen widths
 */

static void GetClipboardRect(DocLayerPtr layer, RectPtr rect)
{
	BOOL 		success;
	DocObjPtr 	docObj;
	WORD		maxPenX, maxPenY;
	WORD 		dx, dy, r, len;
	LineObjPtr	lineObj;

	maxPenX = maxPenY = 0;
	success = FALSE;
	for (docObj = BottomObject(layer); docObj; docObj = NextObj(docObj)) {
		if (!success)
			*rect = docObj->Frame;
		else
			UnionRect(&docObj->Frame, rect, rect);
		success = TRUE;
		switch (docObj->Type) {
			case TYPE_RECT:
				maxPenX = MAX(maxPenX, ((RectObjPtr) docObj)->PenWidth);
				maxPenY = MAX(maxPenY, ((RectObjPtr) docObj)->PenHeight);
				break;
			case TYPE_OVAL:
				maxPenX = MAX(maxPenX, ((OvalObjPtr) docObj)->PenWidth);
				maxPenY = MAX(maxPenY, ((OvalObjPtr) docObj)->PenHeight);
				break;
			case TYPE_POLY:
				maxPenX = MAX(maxPenX, ((PolyObjPtr) docObj)->PenWidth);
				maxPenY = MAX(maxPenY, ((PolyObjPtr) docObj)->PenHeight);
				break;
			case TYPE_LINE:
				lineObj = (LineObjPtr) docObj;
				maxPenX = MAX(maxPenX, lineObj->PenWidth);
				maxPenY = MAX(maxPenY, lineObj->PenHeight);
				dx = ABS(lineObj->Start.x - lineObj->End.x);
				dy = ABS(lineObj->Start.y - lineObj->End.y);
				r = LineLength(&lineObj->Start, &lineObj->End);
				len = 4 + (lineObj->PenWidth + lineObj->PenHeight)/2;
				if (lineObj->LineFlags & (LINE_ARROWSTART | LINE_ARROWEND)) {
					maxPenX += 2*(len*(dx*2 + dy)/r);
					maxPenY += 2*(len*(dy*2 - dx)/r);
				}
				break;
		}
	}
	InsetRect(rect, -maxPenX, -maxPenY);
}

/*
 *	Put on clipboard selected picture as ILBM form
 *	Return success status
 */

static LONG PutFormILBM(DocLayerPtr layer)
{
	WORD		width, height, depth;
	Rectangle	rect;
	LONG 		success;
	RastPtr		rPort;
	BitMapHeader bmHdr;
	PalettePtr 	palette;
	LONG		viewMode;
/*
	Create bitmap to clip
*/
	GetClipboardRect(layer, &rect);
	depth = options.ClipboardDepth;
	width = RowBytes(rect.MaxX - rect.MinX + 1)*8;
	height = rect.MaxY - rect.MinY + 1;
	if (width > MAX_BLIT_SIZE || height > MAX_BLIT_SIZE ||
		(rPort = CreateRastPort(width, height, depth)) == NULL ||
		(palette = MemAlloc(sizeof(Palette), 0)) == NULL)
			goto Exit;
	BuildExportPalette(palette, depth);
	MakeInvColorTable(palette);
	ColorCorrectEnable(palette, FALSE);
	SetPalette(rPort, palette);
	DrawLayerObjects(rPort, layer);
	RGBForeColor(rPort, RGBCOLOR_WHITE);	/* To get FgPen of white */
	viewMode = (depth == 6) ? EXTRA_HALFBRITE : 0;
/*
	put the clip
*/
	success = InitBMHdr(&bmHdr, rPort->BitMap, mskHasTransparentColor,
								cmpByteRun1, rPort->FgPen, 320,200);
	bmHdr.w = width;
/*
	Save bitmap data
*/
	if (success == CLIP_OK)
		success = PutClipBMHD(&bmHdr);
	if (success == CLIP_OK)
		success = PutClipCMAP(palette->ColorTable,
								(viewMode & EXTRA_HALFBRITE) ? (depth - 1) : depth);
	if (success == CLIP_OK && viewMode != 0)
		success = PutClipCAMG(viewMode);
	if (success == CLIP_OK)
		success = PutClipBODY(rPort->BitMap, NULL, &bmHdr, fileBuff, FILEBUFF_SIZE);
/*
	Finish up
*/
Exit:
	if (rPort)
		DisposeRastPort(rPort);
	if (palette)
		MemFree(palette, sizeof(Palette));

	return (success);
}

/*
 *  write text string to clipboard
 */

static LONG WriteText(DocLayerPtr layer)
{
	LONG		retval;
	TextObjPtr 	textObj;
	UBYTE		zero = 0;

	textObj = (TextObjPtr) BottomObject(layer);
	if ((retval = PushChunk(iff,ID_FTXT,ID_FORM, -1L)) == CLIP_OK) {

		/* write text */
		if (PushChunk(iff,0,ID_CHRS,-1L) == CLIP_OK) {
			if (WriteChunkBytes(iff,textObj->Text, textObj->TextLen) == 0L)
					return (0L);
			if (WriteChunkBytes(iff, &zero, 1) == 0L)
					return (0L);
			PopChunk(iff);
		}
		PopChunk(iff);
	}

	return (retval);
}

/*
 *  write text string to clipboard
 */

static LONG WriteFTXT(DocLayerPtr layer)
{
	LONG	retval;
	struct IOClipReq *clipIO = &(clipboard->cbh_Req);

	if ((retval = OpenIFF(iff,IFFF_WRITE)) == CLIP_OK) {
		retval = WriteText(layer);
		CloseIFF(iff);
	}

	return (clipIO->io_ClipID);
}

/*
 *  write picture to clipboard
 */

static LONG WriteILBM(DocLayerPtr layer)
{
	LONG	retval;
	struct IOClipReq *clipIO = &(clipboard->cbh_Req);

	if ((retval = OpenIFF(iff,IFFF_WRITE)) == CLIP_OK) {
		if ((retval = PushChunk(iff,ID_ILBM,ID_FORM, -1L)) == CLIP_OK) {
				PutFormILBM(layer);
			PopChunk(iff);
		}
		retval = clipIO->io_ClipID;
		CloseIFF(iff);
	}

	return (retval);
}

static LONG WriteEPSF(EPSFObjPtr epsf)
{
	LONG	retval;
	struct IOClipReq *clipIO = &(clipboard->cbh_Req);

	if ((retval = OpenIFF(iff,IFFF_WRITE)) == CLIP_OK) {
		if ((retval = PushChunk(iff,ID_EPSF,ID_FORM, -1L)) == CLIP_OK) {
		/* write file */
			if (PushChunk(iff,0,ID_BODY,-1L) == CLIP_OK) {
				if (WriteChunkBytes(iff, epsf->File ,epsf->FileSize) == 0L)
					return (0L);
				PopChunk(iff);
			}
			PopChunk(iff);
		}
		retval = clipIO->io_ClipID;
		CloseIFF(iff);
	}

	return (retval);
}

void PutClipboard()
{
	DocObjPtr 	docObj;

	if (!options.FullClipboard)
		return;

	if (IFFParseBase == NULL) {
		if ((IFFParseBase = (struct IFFParseBase *)
				OpenLibrary("iffparse.library",OSVERSION_2_0_4)) == NULL) {
			Error(ERR_NO_PARSE);
			return;
		}
	}
	if (clipboard == NULL)
		InitClipboard();

	if (clipboard == NULL)
		return;

	docObj = BottomObject(&pasteLayer);
	if (NextObj(docObj) == NULL && docObj->Type == TYPE_EPSF)
		docObj = docObj;
	else
		docObj = NULL;

	if (pasteText)
		lastID = WriteFTXT(&pasteLayer);
	else if (docObj)
		lastID = WriteEPSF((EPSFObjPtr) docObj);
	else
		lastID = WriteILBM(&pasteLayer);
}

/*
 *	send a post message to the clipboard
 */

void PutPost(WindowPtr window)
{
	struct IOClipReq *clipIO;

	if ((IFFParseBase == NULL) || (clipboard == NULL))
		return;

	clipIO = &(clipboard->cbh_Req);
	clipIO->io_Data = (STRPTR) clipboardMsgPort;
	clipIO->io_ClipID = 0L;
	clipIO->io_Command = CBD_POST;

	DoIO((IOReqPtr)clipIO);

	postID = clipIO->io_ClipID;
	pasteText = TextInEdit(window);
}

/*
 *	process message received by clipboard
 */

void DoClipboardMsg(struct SatisfyMsg *msg)
{
	PutClipboard();
}