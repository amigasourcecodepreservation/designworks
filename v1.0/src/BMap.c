/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Bitmap handling routines
 */

#include <exec/types.h>
#include <graphics/gfxmacros.h>
#include <intuition/intuition.h>

#include <proto/graphics.h>

#include <Toolbox/Memory.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Window.h>

#include "Draw.h"
#include "Proto.h"

/*
 *	External variables
 */

extern Options	options;

/*
 *	Local variables and definitions
 */

static BMapObjPtr	growRectObj;
static BOOL			wasMoved;
static WORD			growHandle;

/*
 *	Allocate a new bitmap object
 */

BMapObjPtr BMapAllocate()
{
	return ((BMapObjPtr) MemAlloc(sizeof(BMapObj), MEMF_CLEAR));
}

/*
 *	Dispose of bitmap object
 */

void BMapDispose(BMapObjPtr bMapObj)
{
	LONG dataSize;

	if (bMapObj->Data) {
		dataSize = (LONG) bMapObj->Width*bMapObj->Height*sizeof(RGBColor);
		MemFree(bMapObj->Data, dataSize);
	}
	MemFree(bMapObj, sizeof(BMapObj));
}


/*
 *	Draw bitmap object
 */

void BMapDrawObj(RastPtr rPort, BMapObjPtr bMapObj, RectPtr rect, RectPtr clipRect)
{
	register WORD x, y, width, height, xStart;
	RGBColor rgbColor, oldColor;
	Point pt;
	Rectangle drawRect, bounds;

/*
	If not showing pictures, draw place holder
*/
	if (!options.ShowPictures) {
		PenNormal(rPort);
		FrameRect(rPort, rect, 1, 1);
		Move(rPort, rect->MinX, rect->MinY);
		Draw(rPort, rect->MaxX, rect->MaxY);
		Move(rPort, rect->MaxX, rect->MinY);
		Draw(rPort, rect->MinX, rect->MaxY);
		return;
	}
/*
	Draw picture
	Try to minimize calls to RGBForeColor()
*/
	if (!SectRect(rect, clipRect, &drawRect))
		return;
	width = bMapObj->Width;
	height = bMapObj->Height;
	SetRect(&bounds, 0, 0, width - 1, height - 1);
	for (y = drawRect.MinY; y <= drawRect.MaxY; y++) {
		xStart = -1;
		for (x = drawRect.MinX; x <= drawRect.MaxX; x++) {
			pt.x = x;
			pt.y = y;
			MapPt(&pt, rect, &bounds);
			rgbColor = bMapObj->Data[pt.y*width + pt.x];
/*
			if (rgbColor != RGB_TRANSPARENT) {
				RGBForeColor(rPort, rgbColor);
				WritePixel(rPort, x, y);
			}
*/
			if (rgbColor != RGB_TRANSPARENT) {
				if (xStart == -1) {
					oldColor = rgbColor;
					xStart = x;
				}
				else if (rgbColor != oldColor) {
					RGBForeColor(rPort, oldColor);
					if (xStart == x - 1)
						WritePixel(rPort, xStart, y);
					else {
						Move(rPort, xStart, y);
						Draw(rPort, x - 1, y);
					}
					oldColor = rgbColor;
					xStart = x;
				}
			}
			else if (xStart != -1) {
				RGBForeColor(rPort, oldColor);
				if (xStart == x - 1)
					WritePixel(rPort, xStart, y);
				else {
					Move(rPort, xStart, y);
					Draw(rPort, x - 1, y);
				}
				xStart = -1;
			}
		}
		if (xStart != -1) {
			RGBForeColor(rPort, oldColor);
			if (xStart == x - 1)
				WritePixel(rPort, xStart, y);
			else {
				Move(rPort, xStart, y);
				Draw(rPort, x - 1, y);
			}
		}
	}
}

/*
 *	Draw outline of bitmap (for creation/dragging)
 *	Offset is in document coordinates
 */

void BMapDrawOutline(WindowPtr window, BMapObjPtr bMapObj, WORD xOffset, WORD yOffset)
{
	RectDrawOutline(window, (RectObjPtr) bMapObj, xOffset, yOffset);
}

/*
 *	Draw selection handles for bitmap
 */

void BMapHilite(WindowPtr window, BMapObjPtr bMapObj)
{
	RectHilite(window, (RectObjPtr) bMapObj);
}

/*
 *	Set bitmap pen color (does nothing)
 */

void BMapSetPenColor(BMapObjPtr bMapObj, RGBColor penColor)
{
}

/*
 *	Set bitmap pen size (does nothing)
 */

void BMapSetPenSize(BMapObjPtr bMapObj, WORD penWidth, WORD penHeight)
{
}

/*
 *	Set bitmap fill pattern (does nothing)
 */

void BMapSetFillPat(BMapObjPtr bMapObj, RGBPat8Ptr fillPat)
{
}

/*
 *	Rotate bitmap by given angle about center
 */

void BMapRotate(BMapObjPtr bMapObj, WORD cx, WORD cy, WORD angle)
{
	register LONG x, y, width, height;	/* To force LONG calculations */
	LONG dataSize;
	register RGBColor *newData, *temp;

	angle = NormalizeAngle(angle);
	if (angle % 90)				/* Only 90 degree rotations allowed */
		return;
	RotateRect(&bMapObj->DocObj.Frame, cx, cy, angle);
	dataSize = (LONG) bMapObj->Width*bMapObj->Height*sizeof(RGBColor);
	if ((newData = MemAlloc(dataSize, 0)) == NULL)
		return;
	for (; angle > 0; angle -= 90) {
		width = bMapObj->Width;
		height = bMapObj->Height;
		for (x = 0; x < width; x++) {
			for (y = 0; y < height; y++)
				newData[x*height + (height - 1 - y)] = bMapObj->Data[y*width + x];
		}
		bMapObj->Width = height;
		bMapObj->Height = width;
		temp = bMapObj->Data;
		bMapObj->Data = newData;
		newData = temp;
	}
	MemFree(newData, dataSize);
}

/*
 *	Flip bitmap horizontally or vertically about center
 */

void BMapFlip(BMapObjPtr bMapObj, WORD cx, WORD cy, BOOL horiz)
{
	register WORD x, y, width, height;
	register LONG i1, i2;
	register RGBColor temp;

	width = bMapObj->Width;
	height = bMapObj->Height;
	if (horiz) {
		for (y = 0; y < height; y++) {
			for (x = 0; x < width/2; x++) {
				i1 = (LONG) y*width + x;
				i2 = (LONG) y*width + width - 1 - x;
				temp = bMapObj->Data[i1];
				bMapObj->Data[i1] = bMapObj->Data[i2];
				bMapObj->Data[i2] = temp;
			}
		}
	}
	else {
		for (x = 0; x < width; x++) {
			for (y = 0; y < height/2; y++) {
				i1 = y*width + x;
				i2 = (height - 1 - y)*width + x;
				temp = bMapObj->Data[i1];
				bMapObj->Data[i1] = bMapObj->Data[i2];
				bMapObj->Data[i2] = temp;
			}
		}
	}
	FlipRect(&bMapObj->DocObj.Frame, cx, cy, horiz);
}

/*
 *	Scale bitmap to new frame
 */

void BMapScale(BMapObjPtr bMapObj, RectPtr frame)
{
	bMapObj->DocObj.Frame = *frame;
}

/*
 *	Determine if point is in bitamp
 *	Point is relative to object rectangle
 */

BOOL BMapSelect(BMapObjPtr bMapObj, PointPtr pt)
{
	Point dataPt;
	Rectangle frame, rect;

	if (!options.ShowPictures)
		return (TRUE);
	frame = bMapObj->DocObj.Frame;
	OffsetRect(&frame, -frame.MinX, -frame.MinY);
	SetRect(&rect, 0, 0, bMapObj->Width - 1, bMapObj->Height - 1);
	dataPt = *pt;
	MapPt(&dataPt, &frame, &rect);		/* Map to point inside object data */
	if (PtInRect(&dataPt, &rect) &&
		bMapObj->Data[dataPt.y*bMapObj->Width + dataPt.x] != RGB_TRANSPARENT)
		return (TRUE);
	return (FALSE);
}

/*
 *	Return handle number of given point, or -1 if not in handle
 *	Point is relative to object rectangle
 */

WORD BMapHandle(BMapObjPtr bMapObj, PointPtr pt, RectPtr handleRect)
{
	return (RectHandle((RectObjPtr) bMapObj, pt, handleRect));
}

/*
 *	Duplicate bitmap data to new object
 *	Return success status
 */

BOOL BMapDupData(BMapObjPtr bMapObj, BMapObjPtr newObj)
{
	LONG dataSize;

	dataSize = (LONG) bMapObj->Width*bMapObj->Height*sizeof(RGBColor);
	if ((newObj->Data = MemAlloc(dataSize, 0)) == NULL)
		return (FALSE);
	newObj->Rotate		= bMapObj->Rotate;
	newObj->Width		= bMapObj->Width;
	newObj->Height		= bMapObj->Height;
	BlockMove(bMapObj->Data, newObj->Data, dataSize);
	return (TRUE);
}

/*
 *	Track mouse and change bitmap shape
 */

void BMapGrow(WindowPtr window, BMapObjPtr bMapObj, UWORD modifier, WORD mouseX, WORD mouseY)
{
	RectGrow(window, (RectObjPtr) bMapObj, modifier, mouseX, mouseY);
}

/*
 *	Convert IFF picture to bitmap object, disposing iffPict
 *	Return success status
 */

BOOL BMapConvert(WindowPtr window, IFFPictPtr iffPict)
{
	register WORD i, byte, colorNum, x, y;
	WORD red, green, blue;
	LONG dataSize;
	register BitMapPtr bitMap;
	register BMapObjPtr bMapObj;
	RGBColor *data, lastColor;
	DocDataPtr docData = GetWRefCon(window);
	Rectangle frame, contRect;

	if (iffPict == NULL || iffPict->Width == 0 || iffPict->Height == 0)
		return (FALSE);
/*
	Allocate object and set frame (centered in window)
*/
	if ((bMapObj = (BMapObjPtr) NewDocObject(CurrLayer(docData), TYPE_BMAP)) == NULL) {
		DisposePict(iffPict);
		return (FALSE);
	}
	SetRect(&frame, 0, 0, iffPict->Width - 1, iffPict->Height - 1);
	GetContentRect(window, &contRect);
	WindowToDocRect(window, &contRect, &contRect);
	x = ((contRect.MaxX - contRect.MinX) - (frame.MaxX - frame.MinX))/2 + contRect.MinX;
	y = ((contRect.MaxY - contRect.MinY) - (frame.MaxY - frame.MinY))/2 + contRect.MinY;
	OffsetRect(&frame, x, y);
	AdjustToDocBounds(docData, &frame);
	bMapObj->DocObj.Frame = frame;
/*
	Allocate bitmap data and copy from iffPict
*/
	dataSize = (LONG) iffPict->Width*iffPict->Height*sizeof(RGBColor);
	if ((bMapObj->Data = MemAlloc(dataSize, 0)) == NULL) {
		BMapDispose(bMapObj);
		DisposePict(iffPict);
		return (FALSE);
	}
	bMapObj->Width = iffPict->Width;
	bMapObj->Height = iffPict->Height;
	bitMap = iffPict->BitMap;
	data = bMapObj->Data;
	SetBusyPointer(window);
	for (y = 0; y < bMapObj->Height; y++) {
		lastColor = iffPict->ColorTable[0];
		for (x = 0; x < bMapObj->Width; x++) {
			if (iffPict->Mask) {
				byte = iffPict->Mask[y*bitMap->BytesPerRow + x/8];
				colorNum = (byte >> (7 - (x & 0x07))) & 1;
			}
			else
				colorNum = 1;
			if (colorNum == 0)
				*data++ = RGB_TRANSPARENT;
			else {
				colorNum = 0;
				for (i = bitMap->Depth - 1; i >= 0; i--) {
					byte = bitMap->Planes[i][y*bitMap->BytesPerRow + x/8];
					colorNum = (colorNum << 1) | ((byte >> (7 - (x & 0x07))) & 1);
				}
				if (iffPict->IsHAM && colorNum >= 16) {
					red		= RED(lastColor);
					green	= GREEN(lastColor);
					blue	= BLUE(lastColor);
					if (colorNum < 32)
						blue	= colorNum - 16;
					else if (colorNum < 48)
						red		= colorNum - 32;
					else
						green	= colorNum - 48;
					lastColor = RGBCOLOR(red, green, blue);
				}
				else
					lastColor = iffPict->ColorTable[colorNum];
				*data++ = (colorNum != iffPict->TranspColor) ? lastColor : RGB_TRANSPARENT;
			}
		}
		NextBusyPointer();
	}
/*
	Select new object and return
*/
	HiliteSelectOff(window);
	UnSelectAllObjects(docData);
	SelectObject((DocObjPtr) bMapObj);
	HiliteSelectOn(window);
	DisposePict(iffPict);
	DocModified(docData);
	InvalObjectRect(window, (DocObjPtr) bMapObj);
	return (TRUE);
}
