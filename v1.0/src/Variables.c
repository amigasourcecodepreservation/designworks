/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Global variables
 */

#include <exec/types.h>
#include <intuition/intuition.h>
#include <libraries/dos.h>
#include <devices/console.h>

#include <Toolbox/List.h>

#include <REXX/rxslib.h>

#include "Draw.h"

/*
 *	Library and device base addresses
 */

struct IntuitionBase	*IntuitionBase;
struct GfxBase			*GfxBase;
struct Library			*LayersBase;
struct Library			*IconBase;
struct Library			*WorkbenchBase;
struct Library			*DiskfontBase;
struct Library			*TranslatorBase;
struct Device			*ConsoleDevice;
struct RexxLib			*RexxSysBase;

/*
 *	General use buffer
 */

TextChar	strBuff[256];

/*
 *	General info
 */

TextChar	progPathName[100];				/* Path name to program */
TextChar	printerName[FILENAME_SIZE + 3];

BOOL	onPubScreen;	/* On public screen */
BOOL	fromCLI;		/* Started from CLI */

WORD	intuiVersion;	/* Intuition version */

ScreenPtr	screen;
WindowPtr	backWindow;
MsgPortPtr	mainMsgPort;
MsgPortPtr	appIconMsgPort;
MsgPort		monitorMsgPort;			/* Not a pointer! */
MsgPort		rexxMsgPort;

WindowPtr	toolWindow, penWindow, fillWindow;

WORD		numWindows;				/* Inited to 0 */
WindowPtr	windowList[MAX_WINDOWS];
MenuPtr		docMenuStrip;
MenuItemPtr	headerSubMenu, footerSubMenu;

WindowPtr	cmdWindow;				/* Last active window */

WindowPtr	closeWindow;
BOOL		closeFlag, closeAllFlag, quitFlag;	/* Inited to FALSE */

BOOL	smartWindows;				/* User wants smart refresh windows */

/*
 *	User options
 */

Options	options;

/*
 *	Valid when dragging in a window
 *	In document coordinates
 */

Rectangle	dragRect;
BOOL		autoScroll = TRUE;	/* FALSE for no autoscroll during TrackMouse() */

/*
 *	Toolbox settings
 */

WORD	drawTool = TOOL_SELECT;

/*
 *	Edit menu variables
 */

DocLayer	pasteLayer;			/* Inited to all zeros */
DocLayer	undoLayer;			/* Inited to all zeros */

BOOL	scrapValid;			/* TRUE if internal clip is same as clipboard */

/*
 *	Table of standard pen sizes (indexed by menu item number)
 */

UBYTE	penSizes[] = { 1, 2, 4, 6, 8, 10 };

/*
 *	Grid snap values
 */

UBYTE	gridSixteenthUnits[] = { 1, 2, 3, 4, 6, 8, 10, 12, 16 };
UBYTE	gridMMUnits[] = { 1, 2, 3, 4, 5, 10, 15, 20, 25 };

/*
 *	General parameters
 */

TextChar	fileBuff[FILEBUFF_SIZE];
LONG		iffError;			/* Error number from IFF load/save */

BOOL	cursorRepeat;			/* Repeated up/down cursor movement */

BOOL	titleChanged;			/* Screen title was changed by Error() */

BOOL	inMacro;				/* Executing macro, inited to FALSE */
BOOL	drawOff;				/* When in macros drawing is off, inited to FALSE */

BOOL	busy;					/* Busy processing keypress, ignore repeats */
WORD	waitCount;				/* BeginWait() called, inited to 0 */

BOOL	doubleClick, tripleClick;		/* Inited to FALSE */

WORD	xAspectShift, yAspectShift;		/* Screen aspect adjustments */

/*
 *	Parameters determined from printer driver
 */

BOOL	graphicPrinter, colorPrinter, pagePrinter;

/*
 *	Default attributes
 */

Defaults	defaults;

/*
 *	Fonts used for display
 */

TextFontPtr	screenFont, smallFont;

/*
 *	RGB values for display colors
 */

RGBColor	stdColors[NUM_STDCOLORS];		/* Standard color palette */

RGBColor		screenColors[256];
InvColorTable	scrnInvColorTable;

/*
 *	Colors used for "new look"
 */

UBYTE	blackColor, whiteColor, darkColor, lightColor;

/*
 *	Gray patterns
 */

UWORD	blackPat[]	= { 0xFFFF, 0xFFFF };
UWORD	grayPat[]	= { 0xAAAA, 0x5555 };
UWORD	ltGrayPat[]	= { 0x8888, 0x2222 };
UWORD	dkGrayPat[]	= { 0xDDDD, 0x7777 };

/*
 *	Function key menu command and AREXX command tables (without/with shift key)
 */

FKey	fKeyTable1[] = {
	{ FKEY_MENU, (Ptr) MENUITEM(PROJECT_MENU, NEW_ITEM, NOSUB) },
	{ FKEY_MENU, (Ptr) MENUITEM(PROJECT_MENU, OPEN_ITEM, NOSUB) },
	{ FKEY_MENU, (Ptr) MENUITEM(PROJECT_MENU, CLOSE_ITEM, NOSUB) },
	{ FKEY_MENU, (Ptr) MENUITEM(PROJECT_MENU, PAGESETUP_ITEM, NOSUB) },
	{ FKEY_MENU, (Ptr) MENUITEM(PROJECT_MENU, PRINT_ITEM,NOSUB) },
	{ FKEY_MENU, (Ptr) MENUITEM(VIEW_MENU, SHOWGRID_ITEM, NOSUB) },
	{ FKEY_MENU, (Ptr) MENUITEM(VIEW_MENU, SHOWPAGE_ITEM, NOSUB) },
	{ FKEY_MENU, (Ptr) MENUITEM(VIEW_MENU, SHOWTOOLBOX_ITEM, NOSUB) },
	{ FKEY_MENU, (Ptr) MENUITEM(VIEW_MENU, SHOWPENPALETTE_ITEM, NOSUB) },
	{ FKEY_MENU, (Ptr) MENUITEM(VIEW_MENU, SHOWFILLPALETTE_ITEM, NOSUB) },
};

FKey	fKeyTable2[] = {
	{ FKEY_MACRO, "Macro_1" },
	{ FKEY_MACRO, "Macro_2" },
	{ FKEY_MACRO, "Macro_3" },
	{ FKEY_MACRO, "Macro_4" },
	{ FKEY_MACRO, "Macro_5" },
	{ FKEY_MACRO, "Macro_6" },
	{ FKEY_MACRO, "Macro_7" },
	{ FKEY_MACRO, "Macro_8" },
	{ FKEY_MACRO, "Macro_9" },
	{ FKEY_MACRO, "Macro_10" }
};

/*
 *	Icons for toolbox window
 */

static UWORD chip arrowIconData[] = {
	0x8000, 0xC000, 0xE000, 0xF000, 0xF800, 0xFC00, 0xFE00, 0xFF00,
	0xF800, 0xD800, 0x8C00, 0x0C00, 0x0600, 0x0600
};

static UWORD chip textIconData[] = {
	0x0100, 0x0380, 0x0380, 0x07C0, 0x04C0, 0x0CE0, 0x0860, 0x1870,
	0x1FF0, 0x3038, 0x3018, 0x601C, 0xF03E
};

static UWORD chip lineIconData[] = {
	0x8000, 0x6000, 0x1800, 0x0600, 0x0180, 0x0060, 0x0018, 0x0004
};

static UWORD chip hvLineIconData[] = {
	0x0200, 0x0200, 0x0200, 0x0200, 0x0200, 0xFFF8, 0x0200, 0x0200,
	0x0200, 0x0200, 0x0200
};

static UWORD chip rectIconData[] = {
	0xFFFC, 0x8004, 0x8004, 0x8004, 0x8004, 0x8004, 0x8004, 0x8004,
	0x8004, 0xFFFC
};

static UWORD chip ovalIconData[] = {
	0x0FC0, 0x3030, 0x4008, 0x8004, 0x8004, 0x8004, 0x8004, 0x4008,
	0x3030, 0x0FC0
};

static UWORD chip curveIconData[] = {
	0x0300, 0x0C80, 0x3080, 0xC080, 0x0104, 0x0208, 0x0230, 0x01C0
};

static UWORD chip polyIconData[] = {
	0x0FFC, 0x1008, 0x2010, 0x4020, 0x8040, 0x8080, 0x8040, 0x8020,
	0x8010, 0xFFF8
};

Image arrowIcon = {
	0, 0, 8, 14, 1, &arrowIconData[0], 1, 0, NULL
};

Image textIcon = {
	0, 0, 15, 13, 1, &textIconData[0], 1, 0, NULL
};

Image lineIcon = {
	0, 0, 14, 8, 1, &lineIconData[0], 1, 0, NULL
};

Image hvLineIcon = {
	0, 0, 13, 11, 1, &hvLineIconData[0], 1, 0, NULL
};

Image rectIcon = {
	0, 0, 14, 10, 1, &rectIconData[0], 1, 0, NULL
};

Image ovalIcon = {
	0, 0, 14, 10, 1, &ovalIconData[0], 1, 0, NULL
};

Image curveIcon = {
	0, 0, 14, 8, 1, &curveIconData[0], 1, 0, NULL
};

Image polyIcon = {
	0, 0, 14, 10, 1, &polyIconData[0], 1, 0, NULL
};
