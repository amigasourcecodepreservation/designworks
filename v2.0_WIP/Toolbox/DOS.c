/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Toolbox library
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	DOS utility routines
 */

#include <exec/types.h>
#include <dos/dos.h>
#include <dos/dosextens.h>

#include <string.h>

#include <proto/exec.h>
#include <proto/dos.h>

#include <Typedefs.h>

#include <Toolbox/Memory.h>
#include <Toolbox/Utility.h>
#include <Toolbox/DOS.h>

/*
 *	External variables
 */

extern struct DosLibrary	*DOSBase;

/*
 *	Simulate LockDosList() for 1.3 and 2.0 OS
 */

DosListPtr LockDosList1(ULONG flags)
{
	DosListPtr	dosEntry;

	if (LibraryVersion((struct Library *) DOSBase) >= OSVERSION_2_0_4)
		dosEntry = LockDosList(flags);
	else {
		Forbid();
		dosEntry = NULL;		/* So NextDosEntry1() will return first entry */
	}
	return (dosEntry);
}

/*
 *	Simulate UnLockDosList() for 1.3 and 2.0 OS
 */

void UnLockDosList1(ULONG flags)
{
	if (LibraryVersion((struct Library *) DOSBase) >= OSVERSION_2_0_4)
		UnLockDosList(flags);
	else
		Permit();
}

/*
 *	Simulate NextDosEntry for 1.3 and 2.0 OS
 *	Ignores flags under 1.3
 */

DosListPtr NextDosEntry1(DosListPtr dosEntry, ULONG flags)
{
	struct DosInfo	*dosInfo;
	struct RootNode	*rootNode;

	if (LibraryVersion((struct Library *) DOSBase) >= OSVERSION_2_0_4)
		dosEntry = NextDosEntry(dosEntry, flags);
	else if (dosEntry)
		dosEntry = (DosListPtr) BADDR(dosEntry->dol_Next);
	else {
		rootNode = (struct RootNode *) DOSBase->dl_Root;
		dosInfo = (struct DosInfo *) BADDR(rootNode->rn_Info);
		dosEntry = (DosListPtr) BADDR(dosInfo->di_DevInfo);
	}
	return (dosEntry);
}

/*
 *	Return full path name from lock
 *	
 */

BOOL NameFromLock1(BPTR lock, TextPtr name, LONG maxLen)
{
	TextPtr					newName;
	WORD					nameLen, newLen;
	BOOL					success;
	BPTR					dir, parentDir;
	struct FileInfoBlock	*fib;

	if (LibraryVersion((struct Library *) DOSBase) >= OSVERSION_2_0_4)
		return (NameFromLock(lock, name, maxLen));
/*
	Handle it ourselves
*/
	success = FALSE;
	name[0] = '\0';			// Start with empty string
	if ((fib = MemAlloc(sizeof(struct FileInfoBlock), 0)) == NULL)
		return (FALSE);
	newName = fib->fib_FileName;
	dir = DupLock(lock);
	while (dir) {
		if (!Examine(dir, fib))
			break;
		nameLen = strlen(name);
		newLen = strlen(newName);
		if (nameLen + newLen + 2 > maxLen)
			break;
		BlockMove(name, name + newLen + 1, nameLen + 1);
		BlockMove(newName, name, newLen);
		parentDir = ParentDir(dir);
		name[newLen] = (nameLen || parentDir == NULL) ? ((parentDir) ? '/' : ':') : '\0';
		UnLock(dir);
		dir = parentDir;
	}
	if (dir)
		UnLock(dir);
	else
		success = TRUE;
	MemFree(fib, sizeof(struct FileInfoBlock));
	return (success);
}

/*
 *	Return pointer to lock of current directory
 *	Does not return a duplicate of the lock
 */

Dir GetCurrentDir()
{
	return (((ProcessPtr) FindTask(NULL))->pr_CurrentDir);
}

/*
 *	Set current directory to directory specified by given lock
 *	Uses a copy of the supplied lock
 */

void SetCurrentDir(Dir dir)
{
	Dir	oldLock, newLock;

	newLock = DupLock(dir);
	oldLock = CurrentDir(newLock);
	UnLock(oldLock);
}
