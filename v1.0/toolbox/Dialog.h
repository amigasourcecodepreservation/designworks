/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Amiga library support
 *	Copyright (c) 1990 New Horizons Software
 *
 *	Dialog template definitions
 *	Based upon requester handler
 */

#ifndef TOOLBOX_DIALOG_H
#define TOOLBOX_DIALOG_H

#ifndef INTUITION_INTUITION_H
#include <intuition/intuition.h>
#endif

#ifndef TYPEDEFS_H
#include "typedefs.h"
#endif

#ifndef TOOLBOX_GADGET_H
#include "Gadget.h"
#endif

/*
 *	Dialog template
 *
 *	Bounds fields are inner dimension of dialog window, and are relative to toolbox
 *		font x & y size
 *	Gadgets field points to first item in gadget template list
 *	Title points to title (for dialogs with title bar)
 *
 *	A value of -1 in LeftEdge or TopEdge will center the dialog in the
 *		indicated direction within the screen's borders
 */

typedef struct {
	UBYTE			Type;
	UBYTE			Flags;
	WORD			LeftEdge, TopEdge, Width, Height;
	GadgTemplPtr	Gadgets;
	TextPtr			Title;
} DialogTemplate, *DlgTemplPtr;

/*
 *	Dialog types and flags
 */

#define DLG_TYPE_ALERT	0			/* No drag bar or borders */
#define DLG_TYPE_WINDOW	1			/* Has drag bar and window borders */

#define DLG_FLAG_CLOSE	0x01		/* Has close box */
#define DLG_FLAG_DEPTH	0x02		/* Has depth arrange gadget */
#define DLG_FLAG_RESIZE	0x04		/* Has resizing gadget */

/*
 *	Dialogs are really windows
 */

typedef Window	Dialog, *DialogPtr;

/*
 *	Suggested dialog gadget item numbers
 */

#define OK_BUTTON		0
#define CANCEL_BUTTON	1

/*
 *	Routines prototypes
 */

void	SetDefaultButtons(WORD, WORD);

DialogPtr	GetDialog(DlgTemplPtr, ScreenPtr, MsgPortPtr);
void		DisposeDialog(DialogPtr);

BOOL	IsDialogMsg(IntuiMsgPtr);
WORD	CheckDialog(MsgPortPtr, DialogPtr, BOOL (*)(IntuiMsgPtr, WORD *));
WORD	ModalDialog(MsgPortPtr, DialogPtr, BOOL (*)(IntuiMsgPtr, WORD *));
BOOL	DialogSelect(IntuiMsgPtr, DialogPtr *, WORD *);

#endif
