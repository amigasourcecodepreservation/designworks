/*
 *	Packer.c Convert data to/from cmpByteRun1 run compression	11/15/85
 *
 *	By Jerry Morrison and Steve Shaw, Electronic Arts
 *	Modified by James Bayless, New Horizons Software
 *
 *	Control bytes:
 *		[0..127]	: followed by n+1 bytes of data
 *		[-1..-127]	: followed by byte to be repeated (-n)+1 times
 *		-128		: NOOP
 *
 *  	This software is in the public domain.
 *
 *	This version for the Commodore-Amiga computer
 */

#include <exec/types.h>

#include "Packer.h"

/*
 *	Local definitions and prototypes
 */

#define MIN_RUN	3
#define MAX_RUN	128
#define MAX_DAT	128

static LONG putSize;

BYTE	*PutDump(BYTE *, WORD, BYTE *);
BYTE	*PutRun(BYTE *, WORD, BYTE);

/*
 *	Put nn bytes of data as dump
 */

static BYTE *PutDump(register BYTE *dest, WORD nn, register BYTE *buf)
{
	register WORD i;

	*dest++ = (nn - 1);
	for (i = 0; i < nn; i++)
		*dest++ = *buf++;
	putSize += nn + 1;
	return (dest);
}

/*
 *	Put nn bytes of data as a run
 */

static BYTE *PutRun(register BYTE *dest, WORD nn, BYTE cc)
{
	*dest++ = -nn + 1;
	*dest++ = cc;
	putSize += 2;
	return (dest);
}

/*
 *	PackRow
 *	Given POINTERS to POINTER, packs one row, updating the source and
 *	destination pointers
 *	Returns count of packed bytes
 */

LONG PackRow(BYTE **pSource, BYTE **pDest, register LONG srcBytes)
{
	register BYTE *source, *dest;
	register BYTE c, lastc;
	register BOOL modeDump;
	register WORD numBuf;		/* Number of chars in buffer */
	register WORD runStart;		/* Buffer index current run starts */
	BYTE buf[MAX_DAT + MAX_RUN + 1];

	source = *pSource;
	dest = *pDest;
	putSize = 0;
	buf[0] = lastc = c = *source++;	/* So have valid lastc */
	numBuf = 1;
	srcBytes--;						/* Since one byte eaten */
	runStart = 0;
	modeDump = TRUE;
	for (; srcBytes; --srcBytes) {
		buf[numBuf++] = c = *source++;
		if (modeDump) {
/*
	If the buffer is full, write the length byte, then the data
*/
			if (numBuf > MAX_DAT) {
				dest = PutDump(dest, (WORD) (numBuf - 1), buf);
				buf[0] = c;
				numBuf = 1;
				runStart = 0;
			}
			else if (c == lastc) {
				if (numBuf - runStart >= MIN_RUN) {
					if (runStart > 0)
						dest = PutDump(dest, runStart, buf);
					modeDump = FALSE;
				}
				else if (runStart == 0)
/*
	No dump in progress, so can't lose by making these 2 a run
*/
					modeDump = FALSE;
			}
			else
				runStart = numBuf - 1;
		}
		else {				/* modeDump = FALSE */
			if (c != lastc || numBuf - runStart > MAX_RUN) {
				dest = PutRun(dest, numBuf - 1 - runStart, lastc);
				buf[0] = c;
				numBuf = 1;
				runStart = 0;
				modeDump = TRUE;
			}
		}
		lastc = c;
	}
	dest = (modeDump) ?
		   PutDump(dest, numBuf, buf) :
		   PutRun(dest, numBuf - runStart, lastc);
	*pSource = source;
	*pDest = dest;
	return (putSize);
}

/*
 *	UnPackRow
 *	Given POINTERS to POINTER variables, unpacks one row, updating the source
 *	and destination pointers until it produces dstBytes bytes
 */

BOOL UnPackRow(BYTE **pSource, BYTE **pDest,
			   register LONG srcBytes, register LONG dstBytes)
{
	register BYTE *source, *dest;
	register WORD n;
	register BYTE c;
	BOOL error;

	source = *pSource;
	dest = *pDest;
	error = TRUE;		/* Assume error until we make it through loop */
	while (dstBytes > 0) {
		if (--srcBytes < 0)
			goto ErrorExit;
		if ((n = *source++) >= 0) {
			n++;
			if ((srcBytes -= n) < 0 || (dstBytes -= n) < 0)
				goto ErrorExit;
			while (n--)
				*dest++ = *source++;
		}
		else if (n != (WORD) -128) {
			n = -n + 1;
			if (--srcBytes < 0 || (dstBytes -= n) < 0)
				goto ErrorExit;
			c = *source++;
			while (n--)
				*dest++ = c;
		}
	}
	error = FALSE;
ErrorExit:
	*pSource = source;
	*pDest = dest;
	return (error);
}
