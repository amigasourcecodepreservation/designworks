/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991-92 New Horizons Software, Inc.
 */

#include "Object.h"
#include "Version.h"

#include <Toolbox/Color.h>

#undef NULL
#define NULL	0			// Keep C++ happy (doesn't like 0L for NULL)

/*
 *	Misc items
 */

#define LYINDIC_EDGE	(2*ARROW_WIDTH + 2)
#define LYINDIC_WIDTH	(8*8 + 2)

#define MAX_WINDOWS		10
#define FILEBUFF_SIZE	512

#define MAX_MENU_FONTS	18
#define MAX_PENSIZE		100

#define MAX_DEPTH		8
#define MAX_COLOR_REGS	(1 << MAX_DEPTH)

#define MAX_MERGE_POLY	10

#define NUM_STDPENS		14			// white and black not counted
#define MAX_NUM_PENS	254
#define NUM_STDPATS		20
#define MAX_NUM_PATS	254
#define NUM_PENSIZES	7
#define NUM_GRIDINDEX	9

/*
 *	Icon numbers
 */

enum {
	ICON_DOC,	ICON_BMAP,	ICON_EPSF,	ICON_PREFS,	ICON_FKEYS
};

/*
 *	Error numbers
 */

enum {
	INIT_NOSCREEN,	INIT_NOMEM
};

enum {
	ERR_UNKNOWN,
	ERR_NO_MEM,
	ERR_OPEN,
	ERR_SAVE,
	ERR_SAVE_NO_MEM,
	ERR_PRINT,
	ERR_PAGE_SIZE,
	ERR_PAGE_NUM,			// Invalid page number
	ERR_BAD_COPIES,
	ERR_NO_PAGE,			// No such page
	ERR_NO_FONT,
	ERR_NO_REXX,
	ERR_BAD_MACRO,
	ERR_MACRO_FAIL,
	ERR_OBJ_LOCKED,
	ERR_PEN_SIZE,
	ERR_SCALE,
	ERR_BAD_PICT,
	ERR_BAD_EPSF,
	ERR_NO_VIS_LAYER,
	ERR_LAST_LAYER,
	ERR_NO_DISPLAY,
	ERR_NO_CLIP,
	ERR_NO_PARSE,
	ERR_HL_READ,
	ERR_HL_WRITE,
	ERR_WRITE_LOCK,			/* no hotlinks write lock */
	ERR_NO_HOTLINKS,		/* hotlinks not present */
	ERR_BAD_LINK,			/* Unable to update designworks link */
};

#define ERR_MAX_ERROR	ERR_BAD_LINK

/*
 *	Window gadget definitions
 */

enum {
	UP_ARROW,		DOWN_ARROW,		LEFT_ARROW,		RIGHT_ARROW,
	VERT_SCROLL,	HORIZ_SCROLL,	LAYERUP_ARROW,	LAYERDOWN_ARROW
};

/*
 *	Toolbox draw tool
 */

enum {
	TOOL_SELECT,	TOOL_TEXT,	TOOL_LINE,	TOOL_EYEDROP,
	TOOL_RECT,		TOOL_OVAL,	TOOL_CURVE,	TOOL_POLY
};

/*
 *	Dialog ID's
 */

enum {
	DLG_INIT,
	DLG_SAVECHANGES,	DLG_REVERT,			DLG_SAVEPREFS,
	DLG_PAGESETUP,		DLG_PRINT,			DLG_CANCELPRINT,	DLG_NEXTPAGE,
	DLG_OPTIONS,		DLG_DRAWSIZE,		DLG_ALIGN,
	DLG_ERROR,			DLG_ABOUT,			DLG_MEMORY,
	DLG_OTHERPEN,		DLG_SCALE,			DLG_PENCOLORS,		DLG_EDITPEN,
	DLG_FILLPATTERNS,	DLG_EDITFILL,		DLG_LAYERS,			DLG_DELETELAYER,
	DLG_GRIDSIZE,		DLG_POSTSCRIPT,
	DLG_CUSTOMMACRO,   	DLG_SAVEHOTLINK,	DLG_BREAKLINK,
	DLG_USERREQ1,		DLG_USERREQ2,		DLG_USERREQ3,		DLG_USERREQTEXT,
	DLG_INFO,			DLG_EXPORT,			DLG_EPSFEXPORT,		DLG_PRINTOPTS,
	DLG_LINK,			DLG_LINKINFO,		DLG_ROTATE
};

/*
 *	Undo ID's (Redo ID's are one higher than Undo's)
 */
/*
enum {
	UNDO_NONE	= 0,
	UNDO_TYPING	= 2,
	UNDO_DELETE	= 4,
	UNDO_CUT	= 6,
	UNDO_COPY	= 8,
	UNDO_PASTE	= 10,
	UNDO_ERASE	= 12
};
*/
/*
 *	Undo commands passed by undo handler to undo routines
 */

enum {
	UNDOCMD_CLEAR,		// Clear undo buffer
	UNDOCMD_UNDO,		// Undo the last operation
	UNDOCMD_REDO,		// Redo the last operation
	UNDOCMD_INIT		// Init menu item text
};

/*
 *	Menu item definitions
 */

enum {							// Menus
	PROJECT_MENU,
	EDIT_MENU,
	LAYOUT_MENU,
	ARRANGE_MENU,
	PEN_MENU,
	TEXT_MENU,
	VIEW_MENU,
	MACRO_MENU
};

enum {							// Project menu
	NEW_ITEM			= 0,
	OPEN_ITEM,
	CLOSE_ITEM,
	IMPORT_ITEM		= 4,
	EXPORT_ITEM,
	LINK_ITEM,
	SAVE_ITEM			= 8,
	SAVEAS_ITEM,
	REVERT_ITEM,
	PAGESETUP_ITEM		= 12,
	PRINTONE_ITEM,
	PRINT_ITEM,
	DEFAULTS_ITEM		= 16,
	QUIT_ITEM			= 18
};

enum {							// Edit menu
	CUT_ITEM			= 0,
	COPY_ITEM,
	PASTE_ITEM,
	ERASE_ITEM,
	HOTLINKS_ITEM		= 5,
	DUPLICATE_ITEM		= 7,
	FLIP_ITEM,
	ROTATE_ITEM,
	SCALE_ITEM,
	CONVERTTOPOLY_ITEM	= 12,
	POLYGON_ITEM,
	SELECTALL_ITEM		= 15,
};

enum {							// Layout menu
	NORMALSIZE_ITEM		= 0,
	ENLARGE_ITEM,
	REDUCE_ITEM,
	FITTOWINDOW_ITEM,
	GRIDSNAP_ITEM		= 5,
	GRIDSIZE_ITEM,
	PENCOLORS_ITEM		= 8,
	FILLPATTERNS_ITEM,
	LAYERS_ITEM,
	DRAWINGSIZE_ITEM,
	PREFERENCES_ITEM	= 13,
	SCREENCOLORS_ITEM
};

enum {							// Arrange menu
	MOVEFORWARD_ITEM	= 0,
	MOVETOFRONT_ITEM,
	MOVEBACKWARD_ITEM,
	MOVETOBACK_ITEM,
	ALIGNTOGRID_ITEM	= 5,
	ALIGNOBJECTS_ITEM,
	GROUP_ITEM			= 8,
	UNGROUP_ITEM,
	LOCK_ITEM			= 11,
	UNLOCK_ITEM
};

enum {							// Pen menu
	WIDTH_ITEM			= 0,
	HEIGHT_ITEM,
	NOARROWS_ITEM		= 3,
	ARROWSTART_ITEM,
	ARROWEND_ITEM
};

enum {							// Text menu
	FONT_ITEM			= 0,
	FONTSIZE_ITEM,
	PLAIN_ITEM			= 3,
	BOLD_ITEM,
	ITALIC_ITEM,
	UNDERLINE_ITEM,
	CONDENSED_ITEM,
	EXPANDED_ITEM,
	LEFTALIGN_ITEM		= 10,
	CENTERALIGN_ITEM,
	RIGHTALIGN_ITEM,
	SINGLESPACE_ITEM	= 14,
	ONEANDHALFSPACE_ITEM,
	DOUBLESPACE_ITEM
};

enum {							// View menu
	ABOUT_ITEM			= 0,
	SHOWBACKLAYERS_ITEM	= 2,
	SHOWRULER_ITEM,
	SHOWGRID_ITEM,
	SHOWPAGE_ITEM,
	SHOWTOOLBOX_ITEM	= 7,
	SHOWPENPALETTE_ITEM,
	SHOWFILLPALETTE_ITEM,
	DISPLAY_ITEM		= 11,
	WINDOW_ITEM			= 13	// First window in list
};

enum {							// Macro menu
	OTHERMACRO_ITEM		= 11,
	CUSTOMMACRO_ITEM	= 13
};

enum {							// Import/export submenu
	PICT_SUBITEM,
	EPSF_SUBITEM
};

enum {							// Defaults submenu
	SAVEDEFAULTS_SUBITEM	= 0,
	SAVEASDEFAULTS_SUBITEM,
	LOADDEFAULTS_SUBITEM	= 3
};

enum {							// HotLinks submenu
	PUBLISH_SUBITEM,
	SUBSCRIBE_SUBITEM,
	UPDATE_SUBITEM,
	INFO_SUBITEM,
	BRKLINK_SUBITEM
};

enum {							// Flip submenu
	HORIZONTAL_SUBITEM	= 0,
	VERTICAL_SUBITEM
};

enum {							// Polygon submenu
	CLOSEPOLY_SUBITEM	= 0,
	OPENPOLY_SUBITEM,
	SMOOTH_SUBITEM,
	UNSMOOTH_SUBITEM,
	MERGE_SUBITEM,
	UNMERGE_SUBITEM
};

enum {							// Font submenu
	OTHERFONT_SUBITEM	= 0,
	FONT_SUBITEM		= 2		// First font in list
};

enum {							// FontSize submenu
	OTHERSIZE_SUBITEM	= 0,
	SIZE_SUBITEM		= 2		// First size in list
};

enum {							// Ruler submenu
	INCH_SUBITEM		= 0,
	CM_SUBITEM
};

enum {							// Display submenu
	A1600GX_SUBITEM		= 0,
	GDA1_SUBITEM
};

/*
 *	First long word of special files identifies file type
 */

#define ID_PREFSFILE	0x06660137L

#define ID_FKEYSFILE	0x0666013AL

/*
 *	Misc definitions
 */

#define WKIND_DOC	0
#define WKIND_TOOL	1
#define WKIND_BACK	2

#define QUOTE			'\''
#define QUOTE_OPEN		QUOTE
#define QUOTE_CLOSE		QUOTE
#define DQUOTE			'\"'
#define DQUOTE_OPEN		DQUOTE
#define DQUOTE_CLOSE	DQUOTE

#define BS		0x08
#define TAB		'\t'
#define LF		'\n'
#define CR		0x0D
#define DEL		0x7F
#define NBSP	0xA0	// Non-breaking space
#define SHYPH	0xAD	// Soft hyphen

#define BLINK_FAST	3
#define BLINK_SLOW	6
#define BLINK_OFF	0

#define SHIFTKEYS	(IEQUALIFIER_LSHIFT | IEQUALIFIER_RSHIFT)
#define ALTKEYS		(IEQUALIFIER_LALT | IEQUALIFIER_RALT)
#define CMDKEY		AMIGARIGHT

#define MODIFIER_WORD	SHIFTKEYS
#define MODIFIER_SENT	ALTKEYS
#define MODIFIER_SEL	IEQUALIFIER_CONTROL

/*
 *	Useful macros
 */

#define IS_UPPER(ch)	((ch) != toLower[(ch)])	// Only alphabetics can be upper

#define MAX(a,b)	((a)>(b)?(a):(b))
#define MIN(a,b)	((a)<(b)?(a):(b))
#define ABS(x)		((x)<0?(-(x)):(x))

#define DotsToDecipoints(a,dpi)	(((LONG)(a)*720L)/(dpi))
#define DecipointsToDots(a,dpi)	(((LONG)(a)*(dpi))/720L)

#define MENUITEM(menu, item, sub)	\
	(SHIFTMENU(menu) | SHIFTITEM(item) | SHIFTSUB(sub))

/*
 *	Function key definition
 */

#define FKEY_MENU	0
#define FKEY_MACRO	1

typedef struct {
	WORD	Type;		// Menu equiv. or macro
	Ptr		Data;		// Menu item number or pointer to macro name
} FKey;

/*
 *	User options
 */

typedef struct {
	UBYTE	MeasureUnit;	// Inches or centimeters
	UBYTE	BlinkPeriod;	// Insertion point blinks 5/(n+1) per sec, 0 = off
	UBYTE	BeepFlash, BeepSound;	// Sound and/or flash screen on errors
	UBYTE	SaveIcons;		// Save icons with files
	UBYTE	DragOutlines;	// Drag outlines of objects, or bounding rect
	UBYTE	PolyAutoClose;	// Automatically close new polygons
	UBYTE	ShowPictures;	// Draw actual bitmap pictures
	UBYTE	ColorCorrect;	// Apply color correction when drawing in window
	RGBColor	PrtCyan, PrtMagenta, PrtYellow;	// For screen color correction
	UBYTE	CachePictures;	// Cache pictures for faster drawing
	UBYTE	FullClipboard;	// use system clipboard
	WORD	ClipboardDepth;	// depth of pictures placed on system clipboard
	WORD	HotLinksDepth;  // depth of pictures published as hotlinks edition
	UBYTE	MakeBackups;	// Make backups when saving
	UBYTE	AutoSave;		// Auto-save
	UBYTE	AutoSaveTime;	// Minutes between auto-saves
	UBYTE	pad[13];
} Options;

/*
 *	Definitions for color tables and inverse color tables
 */

typedef PenNum			InvColorTable[4096];
typedef InvColorTable	*InvColorTablePtr;

typedef struct {					// Palette information attached to RP_User
	WORD		NumPens;
	WORD		Checksum;
	BYTE		Type;
	BYTE		ColorCorrect;
	RGBColor	ColorTable[256];
	PenNum		InvColorTable[4096];
} Palette, *PalettePtr;

/*
 *	Actual colors for dot-matrix printer ribbon (to match screen colors)
 *	Screen colors for red, green, and blue are derived from these
 */

#define PRT_RGBCOLOR_CYAN		0x3AC
#define PRT_RGBCOLOR_MAGENTA	0xE07
#define PRT_RGBCOLOR_YELLOW		0xFE5

/*
 *	TextPort structure
 *	Extention to RastPort for Mac-like font handling
 */

typedef struct {
	RastPtr		RastPort;
	FontNum		FontNum;
	FontSize	FontSize;
	UBYTE		Style, MiscStyle;
} TextPort, *TextPortPtr;

typedef struct {
	WORD	Ascent, Descent, WidMax, Leading;
} FontInfo, *FontInfoPtr;

/*
	cursor key equivalents
*/

#define CURSORKEY_UP	0x1C
#define CURSORKEY_DOWN	0x1D
#define CURSORKEY_RIGHT	0x1E
#define CURSORKEY_LEFT	0x1F

/*
	ILBM color items for export, clipboard, and hotlinks
 */

enum {
	COLOR32_ITEM = 0,
	COLOR64_ITEM,
	COLOR256_ITEM
};

#define LINE_PAT	0xFFFF		/* Line pattern for object creation */