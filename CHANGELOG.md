# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.1] - 2018-04-29
### Added
- New smakefiles. under v1.0 now builds with SAS C, 6.5x, with inspiration and help from work by Olaf Barthel (OB), Gilles Pelletier (GP)
- Added CHANGELOG.md
- Added .gitignore
- REXX.c Rewrite InitPort() and FreePort() to avoid to link with rexxglue.lib (GP)
- typedefs.h reconstructed (OB, GP)

### Changed
- Project now builds with NDK 3.9
- Information that a few of the files in the IFF module is public domain and not GPL.
- Added build instructions to README.md and designworks_src.readme
- \_Main.c extern struct UFB \_ufbs[] -> struct UFB \_ufbs[3] (GP)
- Icon.c MTYPE_APPICON must be changed into AMTYPE_APPICON.  MTYPE_APPMENUITEM into AMTYPE_APPMENUITEM (GP)


### Removed
- Old original custom buildfiles, using lattice c

