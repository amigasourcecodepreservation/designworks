/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991-92 New Horizons Software, Inc.
 *
 *	File save routines
 */

#include <exec/types.h>
#include <exec/memory.h>
#include <intuition/intuition.h>

#include <proto/exec.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include <string.h>

#include <Toolbox/Memory.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Font.h>

#include <IFF/DRAW.h>
#include <IFF/ILBM.h>
#include <IFF/GIO.h>
#include <IFF/Packer.h>

#include "Draw.h"
#include "Proto.h"

/*
 *	External variables
 */

extern Options	options;

extern TextChar	strBuff[];

extern UBYTE	fileBuff[];
extern LONG		iffError;

extern RGBColor	export4[], export8[], export16[], export32[], export64[];

extern TextChar	strBackupSuffix[];

/*
 *	Local prototypes
 */

static WORD	*PutWordDump(WORD *, WORD, WORD *);
static WORD	*PutWordRun(WORD *, WORD, WORD);
static LONG	PackWordRow(WORD *, WORD *, LONG);

static BOOL	AddObjectFonts(DocObjPtr);

static BOOL	AddObjectPats(DocObjPtr);
static BOOL	BuildPatTable(DocDataPtr);

static void	Point2FixPoint(PointPtr, FixPtPtr);
static void	Rect2FixRect(RectPtr, FixRectPtr);
static void	GetAbsFrame(DocObjPtr, RectPtr);

static IFFP	PutFONT(GroupContext *, DocDataPtr);
static IFFP	PutPATS(GroupContext *);
static IFFP	PutPREC(GroupContext *, DocDataPtr);
static IFFP	PutDOC(GroupContext *, DocDataPtr);
static IFFP	PutLAYR(GroupContext *, DocLayerPtr);
static IFFP	PutGRP(GroupContext *, GroupObjPtr);
static IFFP	PutEGRP(GroupContext *);
static IFFP	PutLINE(GroupContext *, LineObjPtr);
static IFFP	PutRECT(GroupContext *, RectObjPtr);
static IFFP	PutOVAL(GroupContext *, OvalObjPtr);
static IFFP	PutPOLY(GroupContext *, PolyObjPtr);
static IFFP	PutSTXT(GroupContext *, TextObjPtr);
static IFFP PutLocalLink(GroupContext *, TextPtr, struct DateStamp);
static IFFP	PutBMAP(GroupContext *, BMapObjPtr, DocDataPtr);
static IFFP PutEPSF(GroupContext *, EPSFObjPtr);
static IFFP	PutObject(GroupContext *, DocObjPtr, DocDataPtr);
static IFFP	PutLayer(GroupContext *, DocLayerPtr, DocDataPtr);
static IFFP PutHotLink(GroupContext *, DocDataPtr);

/*
 *	Put num words of data as a dump
 */

static WORD *PutWordDump(register WORD *dest, WORD num, register WORD *src)
{
	register WORD i;

	*dest++ = (num - 1);
	for (i = 0; i < num; i++)
		*dest++ = *src++;
	return (dest);
}

/*
 *	Put num words of data as a run
 */

static WORD *PutWordRun(register WORD *dest, WORD num, WORD value)
{
	*dest++ = -num + 1;
	*dest++ = value;
	return (dest);
}

/*
 *	Packs one row of words, updating the source and
 *	destination pointers
 *	Returns count of packed words
 */

static LONG PackWordRow(register WORD *src, register WORD *dst, register LONG srcWords)
{
	register WORD i, runStart;
	LONG putSize;
	BOOL modeDump;

	if (srcWords <= 0)
		return (0);
	putSize = runStart = 0;
	modeDump = TRUE;
	for (i = 2; i < srcWords; i++) {
		if (modeDump) {			/* Need at least three identical to switch to run */
			if (src[i] == src[i - 1] && src[i - 1] == src[i - 2]) {
				if (i - runStart > 2) {
					PutWordDump(dst + putSize, i - runStart - 2, src + runStart);
					putSize += i - runStart - 1;
					runStart = i - 2;
				}
				modeDump = FALSE;
			}
		}
		else {
			if (src[i] != src[i - 1]) {
				PutWordRun(dst + putSize, i - runStart, src[i - 1]);
				putSize += 2;
				runStart = i;
				modeDump = TRUE;
			}
		}
	}
	if (modeDump) {
		PutWordDump(dst + putSize, srcWords - runStart, src + runStart);
		putSize += srcWords - runStart + 1;
	}
	else {
		PutWordRun(dst + putSize, srcWords - runStart, src[srcWords - 1]);
		putSize += 2;
	}
	return (putSize);
}

/*
 *	Add fontNums from object list to font table
 *	Return success status
 */

static BOOL AddObjectFonts(DocObjPtr docObj)
{
	while (docObj) {
		switch (docObj->Type) {
		case TYPE_GROUP:
			if (!AddObjectFonts(((GroupObjPtr) docObj)->Objects))
				return (FALSE);
			break;
		case TYPE_TEXT:
			if (!AddFontEntry(((TextObjPtr) docObj)->FontNum, ((TextObjPtr) docObj)->FontNum))
				return (FALSE);
			break;
		}
		docObj = NextObj(docObj);
	}
	return (TRUE);
}

/*
 *	Add patterns from object list to pattern table
 *	Return success status
 */

static BOOL AddObjectPats(DocObjPtr docObj)
{
	while (docObj) {
		switch (docObj->Type) {
		case TYPE_GROUP:
			if (!AddObjectPats(((GroupObjPtr) docObj)->Objects))
				return (FALSE);
			break;
		case TYPE_RECT:
			if (!AddPatEntry(&((RectObjPtr) docObj)->FillPat))
				return (FALSE);
			break;
		case TYPE_OVAL:
			if (!AddPatEntry(&((OvalObjPtr) docObj)->FillPat))
				return (FALSE);
			break;
		case TYPE_POLY:
			if (!AddPatEntry(&((PolyObjPtr) docObj)->FillPat))
				return (FALSE);
			break;
		case TYPE_TEXT:
			if (!AddPatEntry(&((TextObjPtr) docObj)->FillPat))
				return (FALSE);
			break;
		}
		docObj = NextObj(docObj);
	}
	return (TRUE);
}

/*
 *	Build pattern table containing all patterns used in document
 *	Return success status
 */

static BOOL BuildPatTable(DocDataPtr docData)
{
	DocLayerPtr docLayer;

	DisposePatTable();
	for (docLayer = BottomLayer(docData); docLayer; docLayer = NextLayer(docLayer)) {
		if (!AddObjectPats(BottomObject(docLayer)))
			return (FALSE);
	}
	return (TRUE);
}

/*
 *	Convert point to fixed point
 */

static void Point2FixPoint(PointPtr pt, FixPtPtr fixPt)
{
	fixPt->X = Long2Fix(pt->x);
	fixPt->Y = Long2Fix(pt->y);
}

/*
 *	Convert rect to fixed rect
 */

static void Rect2FixRect(RectPtr rect, FixRectPtr fixRect)
{
	fixRect->MinX = Long2Fix(rect->MinX);
	fixRect->MinY = Long2Fix(rect->MinY);
	fixRect->MaxX = Long2Fix(rect->MaxX);
	fixRect->MaxY = Long2Fix(rect->MaxY);
}

/*
 *	Return object frame in absolute coordinates
 */

static void GetAbsFrame(DocObjPtr docObj, RectPtr rect)
{
	GroupObjPtr group;

	*rect = docObj->Frame;
	for (group = docObj->Group; group; group = group->DocObj.Group)
		OffsetRect(rect, group->DocObj.Frame.MinX, group->DocObj.Frame.MinY);
}

/*
 *	Put FONT chunk
 */

static IFFP PutFONT(GroupContext *context, DocDataPtr docData)
{
	IFFP iffp;
	WORD i, fontNum, len;
	DocLayerPtr docLayer;
	FontID *fontID;
	TextChar buff[sizeof(FontID) + 100];

/*
	First build table of fonts used
*/
	DisposeFontTable();
	for (docLayer = BottomLayer(docData); docLayer; docLayer = NextLayer(docLayer)) {
		if (!AddObjectFonts(BottomObject(docLayer))) {
			DisposeFontTable();
			return (CLIENT_ERROR);
		}
	}
/*
	Now save font chunks of fonts used
*/
	fontID = (FontID *) buff;
	iffp = IFF_OKAY;			/* In case there are no text objects */
	for (i = 0; FontTableItem(i, &fontNum); i++) {
		fontID->FontNum = fontNum;
		GetFontName(fontNum, fontID->Name);
		len = strlen(fontID->Name);
		if ((iffp = PutCk(context, ID_FONT, sizeof(FontID) + len, fontID)) != IFF_OKAY)
			break;
	}
	DisposeFontTable();
	return (iffp);
}

/*
 *	Put PATS chunk
 */

static IFFP PutPATS(GroupContext *context)
{
	IFFP iffp;
	FillPatNum patNum;
	RGBPat8Ptr fillPat;

	iffp = PutCkHdr(context, ID_PATS, szNotYetKnown);
	for (patNum = 0; (fillPat = PatTableItem(patNum)) != NULL; patNum++) {
		if (iffp != IFF_OKAY)
			break;
		iffp = IFFWriteBytes(context, fillPat, sizeof(RGBPat8));
	}
	if (iffp == IFF_OKAY)
		iffp = PutCkEnd(context);
	return (iffp);
}

/*
 *  Put PREC chunk
 */

static IFFP PutPREC(GroupContext *context, DocDataPtr docData)
{
	PrintRecInfo printRecInfo;

	BlockMove(docData->PrintRec, &printRecInfo.PrintRec, sizeof(PrintRecord));
    return (PutCk(context, ID_PREC, sizeof(PrintRecInfo), &printRecInfo));
}

/*
 *	Put DOC chunk
 */

static IFFP PutDOC(GroupContext *context, DocDataPtr docData)
{
	DocInfo docInfo;

	BlockClear(&docInfo, sizeof(DocInfo));
	docInfo.Width		= docData->DocWidth;
	docInfo.Height		= docData->DocHeight;
	docInfo.RulerOffset	= docData->RulerOffset;
	return (PutCk(context, ID_DOC, sizeof(DocInfo), &docInfo));
}

/*
 *	Put LAYR chunk
 */

static IFFP PutLAYR(GroupContext *context, DocLayerPtr docLayer)
{
	IFFP iffp;
	WORD len;
	TextPtr name;
	DocLayerInfo *docLayerInfo;
	TextChar buff[sizeof(DocLayerInfo) + 100];

	BlockClear(buff, sizeof(DocLayerInfo) + 100);
	docLayerInfo = (DocLayerInfo *) buff;
	docLayerInfo->Flags = docLayer->Flags;
	name = (docLayer->Name) ? docLayer->Name : "";
	strcpy(docLayerInfo->Name, name);
	len = strlen(name);
	iffp = PutCk(context, ID_LAYR, sizeof(DocLayerInfo) + len, docLayerInfo);
	return (iffp);
}

/*
 *	Put GRP chunk
 */

static IFFP PutGRP(GroupContext *context, GroupObjPtr groupObj)
{
	GroupInfo groupInfo;

	BlockClear(&groupInfo, sizeof(GroupInfo));
	groupInfo.Flags = groupObj->DocObj.Flags & ~OBJ_SELECTED;
	return (PutCk(context, ID_GRP, sizeof(GroupInfo), &groupInfo));
}

/*
 *	Put EGRP chunk
 */

static IFFP PutEGRP(GroupContext *context)
{
	return (PutCk(context, ID_EGRP, 0, NULL));
}

/*
 *	Put LINE chunk
 */

static IFFP PutLINE(GroupContext *context, LineObjPtr lineObj)
{
	Point pt;
	Rectangle rect;
	LineInfo lineInfo;

	BlockClear(&lineInfo, sizeof(LineInfo));
	lineInfo.Flags		= lineObj->DocObj.Flags & ~OBJ_SELECTED;
	lineInfo.LineFlags	= lineObj->LineFlags;
	lineInfo.PenColor	= lineObj->PenColor;
	pt.x = lineObj->PenWidth;
	pt.y = lineObj->PenHeight;
	Point2FixPoint(&pt, &lineInfo.PenSize);
	pt = lineObj->Start;
	GetAbsFrame((DocObjPtr) lineObj, &rect);
	OffsetPoint(&pt, rect.MinX, rect.MinY);
	Point2FixPoint(&pt, &lineInfo.Start);
	pt = lineObj->End;
	OffsetPoint(&pt, rect.MinX, rect.MinY);
	Point2FixPoint(&pt, &lineInfo.End);
	return (PutCk(context, ID_LINE, sizeof(LineInfo), &lineInfo));
}

/*
 *	Put RECT chunk
 */

static IFFP PutRECT(GroupContext *context, RectObjPtr rectObj)
{
	Point pt;
	Rectangle rect;
	RectInfo rectInfo;

	BlockClear(&rectInfo, sizeof(RectInfo));
	rectInfo.Flags		= rectObj->DocObj.Flags & ~OBJ_SELECTED;
	rectInfo.PenColor	= rectObj->PenColor;
	pt.x = rectObj->PenWidth;
	pt.y = rectObj->PenHeight;
	Point2FixPoint(&pt, &rectInfo.PenSize);
	rectInfo.FillPatNum	= GetPatEntry(&rectObj->FillPat);
	GetAbsFrame((DocObjPtr) rectObj, &rect);
	Rect2FixRect(&rect, &rectInfo.Frame);
	return (PutCk(context, ID_RECT, sizeof(RectInfo), &rectInfo));
}

/*
 *	Put OVAL chunk
 */

static IFFP PutOVAL(GroupContext *context, OvalObjPtr ovalObj)
{
	Point pt;
	Rectangle rect;
	OvalInfo ovalInfo;

	BlockClear(&ovalInfo, sizeof(OvalInfo));
	ovalInfo.Flags		= ovalObj->DocObj.Flags & ~OBJ_SELECTED;
	ovalInfo.PenColor	= ovalObj->PenColor;
	pt.x = ovalObj->PenWidth;
	pt.y = ovalObj->PenHeight;
	Point2FixPoint(&pt, &ovalInfo.PenSize);
	ovalInfo.FillPatNum = GetPatEntry(&ovalObj->FillPat);
	GetAbsFrame((DocObjPtr) ovalObj, &rect);
	Rect2FixRect(&rect, &ovalInfo.Frame);
	return (PutCk(context, ID_OVAL, sizeof(OvalInfo), &ovalInfo));
}

/*
 *	Put POLY chunk
 */

static IFFP PutPOLY(GroupContext *context, PolyObjPtr polyObj)
{
	IFFP 		iffp;
	WORD 		i;
	PolyInfo 	polyInfo;
	PolyPath 	polyPath;
	UWORD		pathNum;
	Point 		pt, *points;
	Rectangle 	frame, rect;
	FixPoint 	fixPt;

	BlockClear(&polyInfo, sizeof(PolyInfo));
	polyInfo.Flags		= polyObj->DocObj.Flags & ~OBJ_SELECTED;
	polyInfo.PolyFlags	= polyObj->PolyFlags;
	polyInfo.PenColor	= polyObj->PenColor;
	pt.x = polyObj->PenWidth;
	pt.y = polyObj->PenHeight;
	Point2FixPoint(&pt, &polyInfo.PenSize);
	polyInfo.FillPatNum	= GetPatEntry(&polyObj->FillPat);
	GetAbsFrame((DocObjPtr) polyObj, &rect);
	Rect2FixRect(&rect, &polyInfo.Frame);

	polyInfo.NumPaths	= polyObj->NumPaths;
	iffp = PutCkHdr(context, ID_POLY, sizeof(PolyInfo) + sizeof(PolyPath)*polyInfo.NumPaths
									  + NumPolyPoints(polyObj)*sizeof(FixPoint));
	if (iffp == IFF_OKAY)
		iffp = IFFWriteBytes(context, &polyInfo, sizeof(PolyInfo));

	for (pathNum = 0; pathNum < polyObj->NumPaths; pathNum++) {
		polyPath.NumPoints	= polyObj->NumPoints[pathNum];
		points = polyObj->Points[pathNum];
		if (iffp == IFF_OKAY)
			iffp = IFFWriteBytes(context, &polyPath, sizeof(PolyPath));
		frame = polyObj->DocObj.Frame;
		OffsetRect(&frame, (WORD) -frame.MinX, (WORD) -frame.MinY);
		SetRect(&rect, 0, 0, 0x7FFF, 0x7FFF);
		for (i = 0; i < polyPath.NumPoints; i++) {
			if (iffp != IFF_OKAY)
				break;
			pt = points[i];
			MapPt(&pt, &frame, &rect);
			Point2FixPoint(&pt, &fixPt);
			iffp = IFFWriteBytes(context, &fixPt, sizeof(FixPoint));
		}
	}
	if (iffp == IFF_OKAY)
		iffp = PutCkEnd(context);
	return (iffp);
}

/*
 *	Put STXT chunk
 */

static IFFP PutSTXT(GroupContext *context, TextObjPtr textObj)
{
	IFFP iffp;
	WORD cx, cy;
	Rectangle rect;
	TextInfo textInfo;

	BlockClear(&textInfo, sizeof(TextInfo));
	textInfo.Flags		= textObj->DocObj.Flags & ~OBJ_SELECTED;
	textInfo.TextFlags	= textObj->TextFlags;
	textInfo.PenColor	= textObj->PenColor;
	textInfo.FillPatNum	= GetPatEntry(&textObj->FillPat);
	GetAbsFrame((DocObjPtr) textObj, &rect);
	cx = (rect.MaxX + rect.MinX + 1)/2;
	cy = (rect.MaxY + rect.MinY + 1)/2;
	RotateRect(&rect, cx, cy, -textObj->Rotate);
	Rect2FixRect(&rect, &textInfo.Frame);
	textInfo.Rotate		= Long2Fix(textObj->Rotate);
	textInfo.XScale		= textInfo.YScale = FIXED_UNITY;
	textInfo.FontNum	= textObj->FontNum;
	textInfo.FontSize	= textObj->FontSize;
	textInfo.Style		= textObj->Style;
	textInfo.MiscStyle	= textObj->MiscStyle;
	textInfo.Justify	= textObj->Justify;
	textInfo.Spacing	= textObj->Spacing;
	iffp = PutCkHdr(context, ID_STXT, sizeof(TextInfo) + textObj->TextLen);
	if (iffp == IFF_OKAY)
		iffp = IFFWriteBytes(context, &textInfo, sizeof(TextInfo));
	if (iffp == IFF_OKAY)
		iffp = IFFWriteBytes(context, textObj->Text, textObj->TextLen);
	if (iffp == IFF_OKAY)
		iffp = PutCkEnd(context);
	return (iffp);
}

/*
 *	Put Local Link chunk
 */

static IFFP PutLocalLink(GroupContext *context, TextPtr fileName, struct DateStamp fileDate)
{
	LinkInfo	link;
	WORD		len;
	IFFP		iffp;

	link.FileDate 	= fileDate.ds_Days;
	link.FileTime 	= fileDate.ds_Minute;
	link.FileTick 	= fileDate.ds_Tick;

	len = strlen(fileName);

	iffp = PutCkHdr(context, ID_Link, sizeof(LinkInfo) + len);
	if (iffp == IFF_OKAY)
		iffp = IFFWriteBytes(context, &link, sizeof(LinkInfo));
	if (iffp == IFF_OKAY)
		iffp = IFFWriteBytes(context, fileName, len);
	if (iffp == IFF_OKAY)
		iffp = PutCkEnd(context);

	return (iffp);
}

/*
 *	Put BMAP chunk
 */

static IFFP PutBMAP(GroupContext *context, BMapObjPtr bMapObj, DocDataPtr docData)
{
	IFFP iffp;
	WORD row, buffWords, width, height;
	LONG dataWords;
	WORD *cmpData;
	Rectangle frame, viewRect;
	BitMapInfo bitMapInfo;
	GroupObjPtr	group;

	BlockClear(&bitMapInfo, sizeof(BitMapInfo));
	bitMapInfo.Flags		= bMapObj->DocObj.Flags & ~OBJ_SELECTED;

	frame = bMapObj->DocObj.Frame;
	viewRect = bMapObj->ViewRect;
	for (group = bMapObj->DocObj.Group; group; group = group->DocObj.Group) {
		OffsetRect(&frame, group->DocObj.Frame.MinX, group->DocObj.Frame.MinY);
		OffsetRect(&viewRect, group->DocObj.Frame.MinX, group->DocObj.Frame.MinY);
	}
	Rect2FixRect(&frame, &bitMapInfo.Frame);

	width = docData->DocWidth;
	height = docData->DocHeight;
	bitMapInfo.ViewMinX 	= (viewRect.MinX * 255 + width/2)/width;
	bitMapInfo.ViewMinY		= (viewRect.MinY * 255 + height/2)/height;
	bitMapInfo.ViewMaxX 	= (viewRect.MaxX * 255 + width/2)/width;
	bitMapInfo.ViewMaxY 	= (viewRect.MaxY * 255 + height/2)/height;

	bitMapInfo.Width		= bMapObj->Width;
	bitMapInfo.Height		= bMapObj->Height;
	buffWords = bMapObj->Width*2;			/* To be sure */
	if ((cmpData = MemAlloc(buffWords*sizeof(WORD), 0)) == NULL)
		return (CLIENT_ERROR);
	bitMapInfo.Compression	= BMAP_CMPWORDRUN;
	iffp = PutCkHdr(context, ID_BMAP, szNotYetKnown);
	if (iffp == IFF_OKAY)
		iffp = IFFWriteBytes(context, &bitMapInfo, sizeof(BitMapInfo));
	for (row = 0; row < bitMapInfo.Height; row++) {
		if (iffp != IFF_OKAY)
			break;
		dataWords = PackWordRow(bMapObj->Data + row*bitMapInfo.Width, cmpData, bMapObj->Width);
		iffp = IFFWriteBytes(context, cmpData, dataWords*sizeof(WORD));
	}
	if (iffp == IFF_OKAY)
		iffp = PutCkEnd(context);
	MemFree(cmpData, buffWords*sizeof(WORD));

	if (bMapObj->FileName)
		iffp = PutLocalLink(context, bMapObj->FileName, bMapObj->FileDate);
	return (iffp);
}

/*
 *	Put EPSF chunk
 */

static IFFP PutEPSF(GroupContext *context,EPSFObjPtr epsfObj)
{
	IFFP 		iffp;
	EPSFInfo	epsfInfo;
	Rectangle	rect;
	WORD 		cx, cy;

	BlockClear(&epsfInfo, sizeof(EPSFInfo));
	epsfInfo.Flags		= epsfObj->DocObj.Flags & ~OBJ_SELECTED;
	epsfInfo.EPSFFlags	= epsfObj->EPSFFlags;
	GetAbsFrame((DocObjPtr) epsfObj, &rect);
	cx = (rect.MaxX + rect.MinX + 1)/2;
	cy = (rect.MaxY + rect.MinY + 1)/2;
	RotateRect(&rect, cx, cy, -epsfObj->Rotate);
	Rect2FixRect(&rect, &epsfInfo.Frame);
	epsfInfo.Rotate		= Long2Fix(epsfObj->Rotate);
	epsfInfo.FileSize	= epsfObj->FileSize;
	iffp = PutCkHdr(context, ID_EPSF, sizeof(EPSFInfo) + epsfObj->FileSize);
	if (iffp == IFF_OKAY)
		iffp = IFFWriteBytes(context, &epsfInfo, sizeof(EPSFInfo));
	if (iffp == IFF_OKAY)
		iffp = IFFWriteBytes(context, epsfObj->File, epsfObj->FileSize);
	if (iffp == IFF_OKAY)
		iffp = PutCkEnd(context);

	if (epsfObj->FileName)
		iffp = PutLocalLink(context, epsfObj->FileName, epsfObj->FileDate);

	return (iffp);
}

/*
 *	Save object
 */

static IFFP PutObject(GroupContext *context, DocObjPtr docObj, DocDataPtr docData)
{
	IFFP iffp;

	switch (docObj->Type) {
	case TYPE_GROUP:
		iffp = PutGRP(context, (GroupObjPtr) docObj);
		for (docObj = ((GroupObjPtr) docObj)->Objects; docObj; docObj = NextObj(docObj)) {
			if (iffp != IFF_OKAY)
				break;
			iffp = PutObject(context, docObj, docData);
		}
		if (iffp == IFF_OKAY)
			iffp = PutEGRP(context);
		break;
	case TYPE_LINE:
		iffp = PutLINE(context, (LineObjPtr) docObj);
		break;
	case TYPE_RECT:
		iffp = PutRECT(context, (RectObjPtr) docObj);
		break;
	case TYPE_OVAL:
		iffp = PutOVAL(context, (OvalObjPtr) docObj);
		break;
	case TYPE_POLY:
		iffp = PutPOLY(context, (PolyObjPtr) docObj);
		break;
	case TYPE_TEXT:
		iffp = PutSTXT(context, (TextObjPtr) docObj);
		break;
	case TYPE_BMAP:
		iffp = PutBMAP(context, (BMapObjPtr) docObj, docData);
		break;
	case TYPE_EPSF:
		iffp = PutEPSF(context, (EPSFObjPtr) docObj);
		break;
	default:
		iffp = CLIENT_ERROR;
		break;
	}
	return (iffp);
}

/*
 *	Save layer data
 */

static IFFP PutLayer(GroupContext *context, DocLayerPtr docLayer, DocDataPtr docData)
{
	IFFP iffp;
	DocObjPtr docObj;

	iffp = PutLAYR(context, docLayer);
	if (iffp == IFF_OKAY) {
		for (docObj = BottomObject(docLayer); docObj; docObj = NextObj(docObj)) {
			if (iffp != IFF_OKAY)
				break;
			iffp = PutObject(context, docObj, docData);
		}
	}
	return (iffp);
}

/*
 *	Put Hot Link chunk
 */

static IFFP PutHotLink(GroupContext *context,DocDataPtr docData)
{
	IFFP 		iffp;
	HotLinkInfo	link;

	CopyPubRecord(&docData->PubBlock->PRec,&link.PubRecord);
	link.GetUpdates = docData->GetUpdates;
	link.Version = docData->EditionVersion + 1;

	iffp = PutCk(context, ID_LINK, sizeof(HotLinkInfo), &link);

	return (iffp);
}

/*
 *	Save document in normal format
 *	Return success status
 */

BOOL SaveDRAWFile(DocDataPtr docData, TextPtr fileName)
{
	register File file;
	DocLayerPtr docLayer;
	GroupContext fileContext, formContext;

	iffError = DOS_ERROR;
/*
	Create a new file (delete existing file if any)
*/
	if ((file = GOpen(fileName, MODE_NEWFILE)) == NULL)
		return (FALSE);
	(void) GWriteDeclare(file, fileBuff, FILEBUFF_SIZE);
/*
	Save the document
*/
	iffError = OpenWIFF(file, &fileContext, szNotYetKnown);
	if (iffError == IFF_OKAY)
		iffError = StartWGroup(&fileContext, FORM, szNotYetKnown, ID_DRAW, &formContext);
/*
	Save document info
*/
	if (!BuildPatTable(docData))
		iffError = CLIENT_ERROR;
	if (iffError == IFF_OKAY)
		iffError = PutFONT(&formContext, docData);
	if (iffError == IFF_OKAY)
		iffError = PutPATS(&formContext);
	if (iffError == IFF_OKAY)
		iffError = PutPREC(&formContext, docData);
/*
	Save main document and pictures
*/
	if (iffError == IFF_OKAY)
		iffError = PutDOC(&formContext, docData);
	for (docLayer = BottomLayer(docData); docLayer; docLayer = NextLayer(docLayer)) {
		if (iffError != IFF_OKAY)
			break;
		iffError = PutLayer(&formContext, docLayer, docData);
	}
	if (iffError == IFF_OKAY && docData->PubBlock)
		iffError = PutHotLink(&formContext,docData);
/*
	Finish up
*/
	if (iffError == IFF_OKAY)
		iffError = EndWGroup(&formContext);
	if (iffError == IFF_OKAY)
		iffError = CloseWGroup(&fileContext);
	GClose(file);
	DisposePatTable();
	if (iffError != IFF_OKAY)
		return (FALSE);
	docData->Flags &= ~DOC_MODIFIED;
	SaveIcon(fileName, ICON_DOC);
	return (TRUE);
}

/*
 *	Make export palette appropriate for given depth
 *	64-color palette is designed for half-brite export
 */

void BuildExportPalette(PalettePtr palette, WORD depth)
{
	register WORD			i, j, r, g, b;
	register ColorTablePtr	colorTable = &palette->ColorTable;

	palette->NumPens = 1 << depth;
	switch (depth) {
	case 2:
		BlockMove(export4, colorTable, 4*sizeof(RGBColor));
		break;
	case 3:
		BlockMove(export8, colorTable, 8*sizeof(RGBColor));
		break;
	case 4:
		BlockMove(export16, colorTable, 16*sizeof(RGBColor));
		break;
	case 5:
		BlockMove(export32, colorTable, 32*sizeof(RGBColor));
		break;
	case 6:				/* Half-brite palette */
		BlockMove(export64, colorTable, 32*sizeof(RGBColor));
		for (i = 32; i < 64; i++)
			(*colorTable)[i] = RGBDARKEN(export64[i - 32]);
		break;
	case 8:
		i = 0;
		for (r = 0; r < 16; r+= 3) {
			for (g = 0; g < 16; g += 3) {
				for (b = 0; b < 16; b += 3)
					(*colorTable)[i++] = RGBCOLOR(r, g, b);
			}
		}
		for (j = 0; j < 16; j++) {
			if (j % 3 > 0) {
				(*colorTable)[i]	  = RGBCOLOR(j, 0, 0);
				(*colorTable)[i + 10] = RGBCOLOR(0, j, 0);
				(*colorTable)[i + 20] = RGBCOLOR(0, 0, j);
				(*colorTable)[i + 30] = RGBCOLOR(j, j, j);
				i++;
			}
		}
		break;
	default:
		(*colorTable)[0] = RGBCOLOR_BLACK;
		for (i = 1; i < palette->NumPens; i++)
			(*colorTable)[i] = RGBCOLOR_WHITE;
		break;
	}
}

/*
 *	Draw selected objects into rPort
 */

void DrawSelObjects(RastPtr rPort, DocDataPtr docData)
{
	WORD xOffset, yOffset;
	DocObjPtr docObj;
	Rectangle objRect, selRect;

	GetSelectRect(docData, &selRect);
	xOffset = -selRect.MinX;
	yOffset = -selRect.MinY;
	OffsetRect(&selRect, xOffset, yOffset);
/*
	Draw objects
*/
	ClearRast(rPort);
	for (docObj = FirstSelected(docData); docObj; docObj = NextSelected(docObj)) {
		objRect = docObj->Frame;
		OffsetRect(&objRect, xOffset, yOffset);
		DrawObject(rPort, docObj, &objRect, &selRect, docData->Scale);
	}
}

/*
 *	Put view mode chunk
 */

#define ID_CAMG	MakeID('C','A','M','G')

#define PutCAMG(context, viewMode)	PutCk(context, ID_CAMG, sizeof(LONG), &(viewMode))

/*
 *	Export selected objects as 64-color (half-brite) ILBM file
 *	Return success status
 */

BOOL ExportILBMFile(DocDataPtr docData, TextPtr fileName, WORD depth)
{
	WORD width, height;
	LONG viewMode;
	BOOL success;
	RastPtr rPort;
	register File file;
	GroupContext fileContext, formContext;
	Rectangle rect;
	BitMapHeader bmHdr;
	PalettePtr palette;

	rPort = NULL;
	palette = NULL;
	success = FALSE;
/*
	Create bitmap
*/
	iffError = CLIENT_ERROR;
	if (!GetSelectRect(docData, &rect))
		goto Exit;
	width = RowBytes(rect.MaxX - rect.MinX + 1)*8;
	height = rect.MaxY - rect.MinY + 1;
	if (width > MAX_BLIT_SIZE || height > MAX_BLIT_SIZE ||
		(rPort = CreateRastPort(width, height, depth)) == NULL ||
		(palette = MemAlloc(sizeof(Palette), 0)) == NULL)
		goto Exit;
	BuildExportPalette(palette, depth);
	MakeInvColorTable(palette);
	ColorCorrectEnable(palette, FALSE);
	SetPalette(rPort, palette);
	DrawSelObjects(rPort, docData);
	RGBForeColor(rPort, RGBCOLOR_WHITE);	/* To get FgPen of white */
	viewMode = (depth == 6) ? EXTRA_HALFBRITE : 0;
/*
	Create a new file (delete existing file if any)
*/
	iffError = DOS_ERROR;
	if ((file = GOpen(fileName, MODE_NEWFILE)) == NULL)
		goto Exit;
/*
	Save the document
*/
	iffError = InitBMHdr(&bmHdr, rPort->BitMap, mskHasTransparentColor, cmpByteRun1,
						 rPort->FgPen, 320, 200);
	if (iffError == IFF_OKAY)
		iffError = OpenWIFF(file, &fileContext, szNotYetKnown);
	if (iffError == IFF_OKAY)
		iffError = StartWGroup(&fileContext, FORM, szNotYetKnown, ID_ILBM, &formContext);
/*
	Save bitmap data
*/
	bmHdr.w = width;		/* Override bitMap full-width with actual value */
	bmHdr.h = height;
	if (iffError == IFF_OKAY)
		iffError = PutBMHD(&formContext, &bmHdr);
	if (iffError == IFF_OKAY)
		iffError = PutCMAP(&formContext, palette->ColorTable,
						   (viewMode & EXTRA_HALFBRITE) ? (depth - 1) : depth);
	if (iffError == IFF_OKAY && viewMode != 0)
		iffError = PutCAMG(&formContext, viewMode);
	if (iffError == IFF_OKAY)
		iffError = PutBODY(&formContext, rPort->BitMap, NULL, &bmHdr, fileBuff, FILEBUFF_SIZE);
/*
	Finish up
*/
	if (iffError == IFF_OKAY)
		iffError = EndWGroup(&formContext);
	if (iffError == IFF_OKAY)
		iffError = CloseWGroup(&fileContext);
	GClose(file);
	if (iffError == IFF_OKAY) {
		SaveIcon(fileName, ICON_BMAP);
		success = TRUE;
	}
Exit:
	if (palette)
		MemFree(palette, sizeof(Palette));
	if (rPort)
		DisposeRastPort(rPort);
	return (success);
}

/*
 *	Make backup of specified file
 *	Return success status
 */

BOOL MakeBackup(TextPtr fileName)
{
	BPTR lock;
	TextChar iconName[50], backupFileName[50], backupIconName[50];

	strcpy(iconName, fileName);
	strcat(iconName, ".info");
	strcpy(backupFileName, fileName);
	strcat(backupFileName, strBackupSuffix);
	strcpy(backupIconName, backupFileName);
	strcat(backupIconName, ".info");
	(void) DeleteFile(backupFileName);
	(void) DeleteFile(backupIconName);
	if ((lock = Lock(fileName, ACCESS_READ)) != NULL) {
		UnLock(lock);
		if (!Rename(fileName, backupFileName))
			return (FALSE);
	}
	if ((lock = Lock(iconName, ACCESS_READ)) != NULL) {
		UnLock(lock);
		if (!Rename(iconName, backupIconName))
			return (FALSE);
	}
	return (TRUE);
}

