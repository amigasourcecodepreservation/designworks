/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Toolbox library
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Global variables
 */

#ifndef TOOLBOX_GLOBALS_H
#define TOOLBOX_GLOBALS_H

#ifndef EXEC_TYPES_H
#include <exec/types.h>
#endif

#ifndef INTUITION_INTUITION_H
#include <intuition/intuition.h>
#endif

#ifndef TOOLBOX_COLOR_H
#include <Toolbox/Color.h>
#endif

#ifndef TYPEDEFS_H
#include <Typedefs.h>
#endif

/*
 *	Global toolbox variables
 */

extern ScreenPtr	_tbScreen;

extern PenNum		_tbPenBlack, _tbPenWhite, _tbPenDark, _tbPenLight, _tbPenRed, _tbPenMenu;

extern TextAttr		_tbTextAttr;

extern BOOL			_tbOnPubScreen;
extern BOOL			_tbSmartWindows;
extern BOOL			_tbNoShadows;
extern BOOL			_tbAutoActivate;

extern WORD			_tbXSize, _tbYSize, _tbBaseline;

#endif
