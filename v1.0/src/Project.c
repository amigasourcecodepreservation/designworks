/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Project menu routines
 */

#include <exec/types.h>
#include <exec/memory.h>
#include <intuition/intuition.h>
#include <libraries/dos.h>

#include <proto/exec.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include <string.h>

#include <IFF/ILBM.h>

#include <Toolbox/Menu.h>
#include <Toolbox/Window.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Utility.h>
#include <Toolbox/StdFile.h>

#include "Draw.h"
#include "Proto.h"

/*
 *	External variables
 */

extern MsgPortPtr	mainMsgPort;
extern ScreenPtr	screen;
extern WindowPtr	backWindow, toolWindow, penWindow, fillWindow;

extern WindowPtr	windowList[];
extern WORD			numWindows;

extern WindowPtr	closeWindow;
extern BOOL			closeFlag, closeAllFlag, quitFlag;

extern BOOL	titleChanged;

extern LONG	iffError;

extern TextChar	strUntitled[];
extern TextChar	strOpenFile[], strImportPict[], strExportPict[];
extern TextChar	strSaveAs[], strEmergencySave[];
extern TextChar	strBuff[];

extern DlgTemplPtr	dlgList[];

/*
 *	Local variables and definitions
 */

#define MAX_FILENAME_LEN	31

#define SAVE_NO_BTN		2
#define SAVE_NAME_TEXT	4

static WORD	newCount = 1;			/* Number of "Untitled" window */

/*
 *	Local prototypes
 */

BOOL	UniqueTitle(TextPtr);
BOOL	OpenFileFilter(TextPtr);
BOOL	ImportPictFilter(TextPtr);
BOOL	SaveFile(WindowPtr, TextPtr, Dir);

/*
 *	Determine if given title is a unique window title
 */

static BOOL UniqueTitle(TextPtr title)
{
	register WORD i;

	for (i = 0; i < numWindows; i++) {
		GetWTitle(windowList[i], strBuff);
		if (CmpString(title, strBuff, strlen(title), strlen(strBuff), FALSE) == 0)
			return (FALSE);
	}
	return (TRUE);
}

/*
 *	Create new document window
 */

BOOL DoNew()
{
	register WindowPtr window;
	register DocDataPtr docData;
	Rectangle rect;

	if (numWindows == MAX_WINDOWS) {
		ErrBeep();
		return (FALSE);
	}
	strcpy(strBuff, strUntitled);
	strcat(strBuff, " #");
	NumToString((LONG) newCount, strBuff + strlen(strBuff));
	if ((window = CreateWindow(strBuff)) == NULL) {
		Error(ERR_NO_MEM);
		return (FALSE);
	}
	docData = GetWRefCon(window);
	docData->DirLock = NULL;		/* To be sure */
/*
	Create new document
*/
	if (!NewDocument(docData)) {
		RemoveWindow(window);
		Error(ERR_NO_MEM);
		return (FALSE);
	}
	newCount++;
/*
	Finish window setup
*/
	DoNewSize(window);
	GetWindowRect(window, &rect);
	InvalRect(window, &rect);
	DoWindowActivate(window, TRUE);
	return (TRUE);
}

/*
 *	Open new window and load file
 *	Return success status
 */

BOOL OpenFile(TextPtr fileName, Dir dir)
{
	register BOOL success;
	WORD len;
	LONG error;
	BOOL newWindow;
	register WindowPtr window;
	register DocDataPtr docData;
	Rectangle rect;
	TextChar oldTitle[100];

/*
	Open new window
	If active window (or last window openned) is untitled with nothing in it,
		then use it instead
*/
	window = NULL;
	if (numWindows) {
		window = ActiveWindow();
		if (!IsDocWindow(window))
			window = windowList[numWindows - 1];
		docData = GetWRefCon(window);
		len = strlen(strUntitled);
		GetWTitle(window, oldTitle);
		if (strlen(oldTitle) > len &&
			CmpString(oldTitle, strUntitled, len, len, TRUE) == 0 &&
			docData->DirLock == NULL && (docData->Flags & DOC_MODIFIED) == 0) {
			DisposeAll(docData);
			strcpy(docData->WindowName, fileName);
			SetWTitle(window, fileName);
			ChangeWindowItem(window);
			ActivateWindow(window);			/* In case it wasn't active */
		}
		else
			window = NULL;
	}
	newWindow = FALSE;
	if (window == NULL) {
		if (numWindows == MAX_WINDOWS)
			return (FALSE);
		if ((window = CreateWindow(fileName)) == NULL) {
			Error(ERR_NO_MEM);
			return (FALSE);
		}
		newWindow = TRUE;
	}
	docData = GetWRefCon(window);
/*
	Load the file
*/
	success = LoadFile(window, docData, fileName, dir);
/*
	Finish window setup
*/
	if (!success) {
		if (iffError == DOS_ERROR)
			error = IoErr();
		else if (iffError == CLIENT_ERROR)
			error = 0;								/* No memory */
		else
			error = -1;								/* Bad file */
		DOSError(ERR_OPEN, error);
		if (newWindow) {
			RemoveWindow(window);
			return (FALSE);
		}
		(void) NewDocument(docData);				/* This better not fail */
		strcpy(docData->WindowName, oldTitle);
		SetWTitle(window, oldTitle);
		ChangeWindowItem(window);
	}
	AdjustWindowTitle(window);
	DoNewSize(window);
	GetWindowRect(window, &rect);
	InvalRect(window, &rect);
	DoWindowActivate(window, TRUE);
	return (success);
}

/*
 *	Check file for loadability
 *	Returns TRUE if file should be shown in SFPGetFile() list
 */

static BOOL OpenFileFilter(TextPtr fileName)
{
	register BOOL success;
	register File file;
	GroupHeader grpHeader;

	if ((file = Open(fileName, MODE_OLDFILE)) == NULL)
		return (FALSE);
	success = FALSE;
	if (Read(file, &grpHeader, sizeof(GroupHeader)) == sizeof(GroupHeader) &&
		grpHeader.ckID == FORM && grpHeader.grpSubID == ID_DRAW)
		success = TRUE;
	Close(file);
	return (success);
}

/*
 *	Open document
 *	If fileName is not NULL, open specified file, otherwise put up "Open" requester
 */

BOOL DoOpen(WindowPtr window, UWORD modifier, TextPtr fileName)
{
	WORD len;
	BOOL success;
	SFReply sfReply;

	if (numWindows == MAX_WINDOWS) {
		ErrBeep();
		return (FALSE);
	}
/*
	If file name then open this file
*/
	if (fileName) {
		sfReply.DirLock = ConvertFileName(fileName);
		len = strlen(fileName);
		if (len == 0 || len > MAX_FILENAME_LEN) {
			UnLock(sfReply.DirLock);
			return (FALSE);
		}
		strcpy(sfReply.Name, fileName);
	}
/*
	Otherwise get file name from user
*/
	else {
		DoWindowActivate(window, FALSE);	/* Since INACTIVE event will be lost */
		BeginWait();
		SFPGetFile(screen, mainMsgPort, strOpenFile, DialogFilter, NULL,
				   NULL, 0, NULL, OpenFileFilter, &sfReply);
		RefreshWindows();
		EndWait();
		DoWindowActivate(window, TRUE);
		if (sfReply.Result != SFP_OK) {
			if (sfReply.Result == SFP_NOMEM)
				Error(ERR_NO_MEM);
			return (FALSE);
		}
	}
/*
	Open new window and load file
*/
	success = OpenFile(sfReply.Name, sfReply.DirLock);
	if (!success && IsDocWindow(window))
		HiliteSelectOn(window);
	return (success);
}

/*
 *	Save file to specified fileName, and dirLock
 *	Return success status
 */

static BOOL SaveFile(WindowPtr window, TextPtr fileName, Dir dir)
{
#ifdef DEMO
	DOSError(ERR_SAVE, -2);
	return (FALSE);
#else
	register LONG error;
	register BOOL success;
	register DocDataPtr docData = GetWRefCon(window);

/*
	Save the file
*/
	BeginWait();
	SetStdPointer(window, POINTER_WAIT);
	SetCurrentDir(dir);
	success = SaveDRAWFile(docData, fileName);
	EndWait();
/*
	If not successful, show error
*/
	if (!success) {
		if (iffError == DOS_ERROR) {
			error = IoErr();
			if (error != ERROR_DISK_WRITE_PROTECTED)
				DeleteFile(fileName);
		}
		else
			error = 0;								/* No memory (?) */
		DOSError(ERR_SAVE, error);
		if (dir != docData->DirLock)
			UnLock(dir);
		return (FALSE);
	}
/*
	Save file name and set window to new title
*/
	if (fileName != docData->FileName) {
		strcpy(docData->FileName, fileName);
		SetWTitle(window, docData->FileName);
		ChangeWindowItem(window);
	}
/*
	An Aegis Draw save is not considered a real save
*/
	if (docData->DirLock && dir != docData->DirLock) {
		UnLock(docData->DirLock);
		docData->DirLock = NULL;
	}
	docData->DirLock = dir;
	SetProjectMenu();
	return (TRUE);
#endif
}

/*
 *	Close document window
 *	If close is successful return TRUE; if Canceled return FALSE
 */

BOOL DoClose(WindowPtr window)
{
	register WORD item;
	Dir lock;
	LayerPtr layer;
	register DialogPtr dlg;
	register WindowPtr layerWindow, activeWindow;
	register DocDataPtr docData;

	if (titleChanged)
		FixTitle();
	UndoOff();
/*
	Close tool windows
*/
	if (window == toolWindow) {
		CloseToolWindow();
		SetViewMenu();
		return (TRUE);
	}
	if (window == penWindow) {
		ClosePenWindow();
		SetViewMenu();
		return (TRUE);
	}
	if (window == fillWindow) {
		CloseFillWindow();
		SetViewMenu();
		return (TRUE);
	}
	if (!IsDocWindow(window))
		return (FALSE);
/*
	If modified, ask to save changes
*/
	docData = GetWRefCon(window);
	if (docData->Flags & DOC_MODIFIED) {
		for (layer = screen->LayerInfo.top_layer; layer; layer = layer->back) {
			layerWindow = (WindowPtr) layer->Window;
			if (layerWindow == window)
				break;
			if (IsDocWindow(layerWindow)) {
				while (layer && layer->Window == layerWindow)
					layer = layer->back;
				WindowToBack(layerWindow);
				Delay(5);
			}
		}
		activeWindow = ActiveWindow();
		if (window != activeWindow) {
			if (IsDocWindow(activeWindow))
				DoWindowActivate(activeWindow, FALSE);
			DoWindowActivate(window, TRUE);
		}
		BeginWait();
		GetWTitle(window, strBuff);
		dlgList[DLG_SAVECHANGES]->Gadgets[SAVE_NAME_TEXT].Info = strBuff;
		if ((dlg = GetDialog(dlgList[DLG_SAVECHANGES], screen, mainMsgPort)) == NULL) {
			EndWait();
			Error(ERR_SAVE_NO_MEM);
			if ((lock = Lock("DF0:", ACCESS_READ)) == NULL ||
				!SaveFile(window, strEmergencySave, lock))
				return (FALSE);
		}
		else {
			OutlineOKButton(dlg);
			StdBeep();
			do {
				item = ModalDialog(mainMsgPort, dlg, DialogFilter);
			} while (item != OK_BUTTON && item != CANCEL_BUTTON &&
					 item != SAVE_NO_BTN);
			DisposeDialog(dlg);
			EndWait();
			if (item == CANCEL_BUTTON ||
				(item == OK_BUTTON && !DoSave(window, 0)))
				return (FALSE);
		}
	}
	DoWindowActivate(window, FALSE);	/* Does various nice things */
	SetStdPointer(window, POINTER_WAIT);
	if (docData->DirLock)
		UnLock(docData->DirLock);
/*
	Dispose of document and remove window
*/
	DisposeAll(docData);
	RemoveWindow(window);
	SetAllMenus();
	return (TRUE);
}

/*
 *	Check for IFF picture file
 */

static BOOL ImportPictFilter(TextPtr fileName)
{
	register BOOL success;
	register File file;
	GroupHeader grpHeader;

	if ((file = Open(fileName, MODE_OLDFILE)) == NULL)
		return (FALSE);
	success = FALSE;
	if (Read(file, &grpHeader, sizeof(GroupHeader)) == sizeof(GroupHeader) &&
		grpHeader.ckID == FORM && grpHeader.grpSubID == ID_ILBM)
		success = TRUE;
	Close(file);
	return (success);
}

/*
 *	Import picture as bitmap object
 */

BOOL DoImportPict(WindowPtr window, TextPtr fileName)
{
	WORD len;
	BOOL success;
	IFFPictPtr iffPict;
	SFReply sfReply;

/*
	If file name then open this file
*/
	if (fileName) {
		sfReply.DirLock = ConvertFileName(fileName);
		len = strlen(fileName);
		if (len == 0 || len > MAX_FILENAME_LEN) {
			UnLock(sfReply.DirLock);
			return (FALSE);
		}
		strcpy(sfReply.Name, fileName);
	}
/*
	Otherwise get file name from user
*/
	else {
		BeginWait();
		SFPGetFile(screen, mainMsgPort, strImportPict, DialogFilter, NULL,
				   NULL, 0, NULL, ImportPictFilter, &sfReply);
		RefreshWindows();			/* Looks nicer */
		EndWait();
		if (sfReply.Result != SFP_OK) {
			if (sfReply.Result == SFP_NOMEM)
				Error(ERR_NO_MEM);
			return (FALSE);
		}
	}
/*
	Import picture
*/
	SetStdPointer(window, POINTER_WAIT);
	iffPict = LoadPict(sfReply.Name, sfReply.DirLock);
	success = (iffPict) ? BMapConvert(window, iffPict) : FALSE;
	if (!success)
		Error(ERR_BAD_PICT);
	SetAllMenus();
	return (success);
}

/*
 *	Export selected objects as IFF picture
 */

BOOL DoExportPict(WindowPtr window, TextPtr fileName)
{
	WORD len;
	BOOL success;
	DocDataPtr docData = GetWRefCon(window);
	SFReply sfReply;

/*
	If file name then open this file
*/
	if (fileName) {
		sfReply.DirLock = ConvertFileName(fileName);
		len = strlen(fileName);
		if (len == 0 || len > MAX_FILENAME_LEN) {
			UnLock(sfReply.DirLock);
			return (FALSE);
		}
		strcpy(sfReply.Name, fileName);
	}
/*
	Otherwise, get file name from user
*/
	else {
		BeginWait();
		SFPPutFile(screen, mainMsgPort, strExportPict, fileName,
				   DialogFilter, NULL, NULL, &sfReply);
		RefreshWindows();
		EndWait();
		if (sfReply.Result == SFP_CANCEL)
			return (FALSE);
	}
/*
	Export to specified file
*/
	SetStdPointer(window, POINTER_WAIT);
	SetCurrentDir(sfReply.DirLock);
	UnLock(sfReply.DirLock);
	success = ExportILBMFile(docData, sfReply.Name);
	return (success);
}

/*
 *	Save document
 *	Return success status
 */

BOOL DoSave(WindowPtr window, UWORD modifier)
{
	register BOOL success;
	register DocDataPtr docData = GetWRefCon(window);

	if (((modifier & SHIFTKEYS) && (modifier & CMDKEY)) || docData->DirLock == NULL)
		success = DoSaveAs(window);
	else
		success = SaveFile(window, docData->FileName, docData->DirLock);
	return (success);
}

/*
 *	Save document to specified file
 *	Return success status
 */

BOOL DoSaveAs(WindowPtr window)
{
	TextPtr fileName;
	register BOOL success;
	Dir lock;
	register DocDataPtr docData = GetWRefCon(window);
	SFReply sfReply;

/*
	Get file name for save
*/
	BeginWait();
	fileName = docData->FileName;
	if (docData->DirLock)
		SetCurrentDir(docData->DirLock);
/*
	Get new name for file
*/
	SFPPutFile(screen, mainMsgPort, strSaveAs, fileName,
			   DialogFilter, NULL, NULL, &sfReply);
	RefreshWindows();
	EndWait();
	if (sfReply.Result == SFP_CANCEL)
		return (FALSE);
/*
	If document is untitled and cannot get name, do emergency save
*/
	if (sfReply.Result == SFP_NOMEM) {
		if ((docData->Flags & DOC_MODIFIED) == 0) {
			Error(ERR_NO_MEM);
			return (FALSE);
		}
		Error(ERR_SAVE_NO_MEM);
		if ((lock = Lock("DF0:", ACCESS_READ)) == NULL ||
			!SaveFile(window, strEmergencySave, lock))
			return (FALSE);
		return (TRUE);
	}
/*
	Save document to specified file
*/
	success = SaveFile(window, sfReply.Name, sfReply.DirLock);
	return (success);
}

/*
 *	Revert to previously saved document
 */

BOOL DoRevert(WindowPtr window)
{
	register BOOL success;
	register WORD item;
	register LONG error;
	register DialogPtr dlg;
	register DocDataPtr docData = GetWRefCon(window);
	Rectangle rect;

	if (docData->DirLock == NULL) {
		ErrBeep();
		return (FALSE);
	}
/*
	Confirm the revert operation
*/
	BeginWait();
	if ((dlg = GetDialog(dlgList[DLG_REVERT], screen, mainMsgPort)) == NULL) {
		EndWait();
		Error(ERR_NO_MEM);
		return (FALSE);
	}
	OutlineOKButton(dlg);
	StdBeep();
	do {
		item = ModalDialog(mainMsgPort, dlg, DialogFilter);
	} while (item != OK_BUTTON && item != CANCEL_BUTTON);
	DisposeDialog(dlg);
	EndWait();
	if (item == CANCEL_BUTTON)
		return (FALSE);
/*
	Dispose of current document and load original one
*/
	SetStdPointer(window, POINTER_WAIT);
	DisposeAll(docData);
	success = LoadFile(window, docData, docData->FileName, docData->DirLock);
/*
	Finish window setup
*/
	if (!success) {
		if (iffError == DOS_ERROR)
			error = IoErr();
		else if (iffError == CLIENT_ERROR)
			error = 0;								/* No memory */
		else
			error = -1;								/* Bad file */
		DOSError(ERR_OPEN, error);
		(void) NewDocument(docData);				/* If this fails !!!! */
	}
	else
		docData->Flags &= ~DOC_MODIFIED;
	DoNewSize(window);
	GetWindowRect(window, &rect);
	InvalRect(window, &rect);
	DoWindowActivate(window, TRUE);
	return (success);
}

/*
 *	Handle Page Setup
 */

BOOL DoPageSetup(WindowPtr window)
{
	DocDataPtr docData = GetWRefCon(window);
	Rectangle rect;

	if (!PageSetupDialog(docData->PrintRec))
		return (FALSE);
	SetPageParams(docData);
	GetWindowRect(window, &rect);
	InvalRect(window, &rect);
	AdjustScrollBars(window);
	DocModified(docData);
	return (TRUE);
}

/*
 *	Print document
 */

BOOL DoPrint(WindowPtr window, UWORD modifier, BOOL printOne)
{
	BOOL success;
	PrintRecPtr printRec;
	DocDataPtr docData = GetWRefCon(window);

	if (modifier & ALTKEYS)
		printOne = TRUE;
	if (!IsDocWindow(window))
		return (FALSE);
	printRec = docData->PrintRec;
/*
	Get print parameters
*/
	printRec->Copies = 1;
	printRec->FirstPage = 1;
	printRec->LastPage = PagesAcross(docData)*PagesDown(docData);
	if (printOne)
		(void) PrValidate(printRec);
	else if (!PrintDialog(printRec))
		return (FALSE);
/*
	Now print the pages
*/
#ifdef DEMO
	DOSError(ERR_PRINT, -2);
	success = FALSE;
#else
	success = PrintDocument(window, docData);
	if (!success && PrintError())
		Error(ERR_PRINT);
#endif
	return (success);
}

/*
 *	Save program preferences
 */

BOOL DoSavePrefs(WindowPtr window)
{
	WORD item;
	BOOL success;
	DialogPtr dlg;

/*
	Confirm the save prefs operation
*/
	BeginWait();
	if ((dlg = GetDialog(dlgList[DLG_SAVEPREFS], screen, mainMsgPort)) == NULL) {
		EndWait();
		Error(ERR_NO_MEM);
		return (FALSE);
	}
	OutlineOKButton(dlg);
	StdBeep();
	do {
		item = ModalDialog(mainMsgPort, dlg, DialogFilter);
	} while (item != OK_BUTTON && item != CANCEL_BUTTON);
	DisposeDialog(dlg);
	EndWait();
	if (item == CANCEL_BUTTON)
		return (FALSE);
/*
	Save settings
*/
	SetStdPointer(window, POINTER_WAIT);
	success = SaveProgPrefs(window);
	if (!success)
		DOSError(ERR_SAVE, IoErr());
	return (success);
}

/*
 *	Handle Project menu commands
 */

BOOL DoProjectMenu(WindowPtr window, UWORD item, UWORD sub, UWORD modifier)
{
	BOOL success;

	if (!IsDocWindow(window) &&
		item != NEW_ITEM && item != OPEN_ITEM && item != QUIT_ITEM)
		return (FALSE);
	UndoOff();
	success = FALSE;
	switch (item) {
	case NEW_ITEM:
		success = DoNew();
		break;
	case OPEN_ITEM:
		success = DoOpen(window, modifier, NULL);
		break;
	case CLOSE_ITEM:
		if (modifier & ALTKEYS)
			closeAllFlag = TRUE;
		else {
			closeWindow = window;
			closeFlag = TRUE;
		}
		success = TRUE;
		break;
	case IMPORTPICT_ITEM:
		success = DoImportPict(window, NULL);
		break;
	case EXPORTPICT_ITEM:
		success = DoExportPict(window, NULL);
		break;
	case SAVE_ITEM:
		success = DoSave(window, modifier);
		break;
	case SAVEAS_ITEM:
		success = DoSaveAs(window);
		break;
	case REVERT_ITEM:
		success = DoRevert(window);
		break;
	case PAGESETUP_ITEM:
		success = DoPageSetup(window);
		break;
	case PRINTONE_ITEM:
	case PRINT_ITEM:
		success = DoPrint(window, modifier, (BOOL) (item == PRINTONE_ITEM));
		break;
	case SAVEDEFAULTS_ITEM:
		success = DoSavePrefs(window);
		break;
	case QUIT_ITEM:
		quitFlag = TRUE;
		success = TRUE;
		break;
	}
	return (success);
}
