/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Layer handling routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <string.h>

#include <Toolbox/Memory.h>
#include <Toolbox/Window.h>

#include "Draw.h"
#include "Proto.h"

/*
 *	Create new layer and add to end of list
 *	Make layer visible and inactive
 *	Return pointer to layer
 */

DocLayerPtr NewDocLayer(DocDataPtr docData, TextPtr name)
{
	DocLayerPtr newLayer, topLayer;

	if ((newLayer = MemAlloc(sizeof(DocLayer), MEMF_CLEAR)) == NULL)
		return (NULL);
	if (name) {
		if ((newLayer->Name = MemAlloc(strlen(name) + 1, 0)) == NULL) {
			MemFree(newLayer, sizeof(DocLayer));
			return (NULL);
		}
		strcpy(newLayer->Name, name);
	}
	newLayer->Flags = LAYER_VISIBLE;
	if (docData->Layers == NULL)
		docData->Layers = newLayer;
	else {
		topLayer = TopLayer(docData);
		topLayer->Next = newLayer;
		newLayer->Prev = topLayer;
	}
	return (newLayer);
}

/*
 *	Detach layer from document layer list
 */

void DetachLayer(DocDataPtr docData, DocLayerPtr docLayer)
{
	if (docLayer->Prev)
		docLayer->Prev->Next = docLayer->Next;
	if (docLayer->Next)
		docLayer->Next->Prev = docLayer->Prev;
	if (docData->Layers == docLayer)
		docData->Layers = docLayer->Next;
	if (docData->CurrLayer == docLayer)
		docData->CurrLayer = NULL;
	docLayer->Next = docLayer->Prev = NULL;
}

/*
 *	Delete layer and all objects it contains
 *	Does not adjust layer links
 */

void DisposeDocLayer(DocLayerPtr docLayer)
{
	if (docLayer) {
		DisposeAllDocObjects(docLayer->Objects);
		if (docLayer->Name)
			MemFree(docLayer->Name, strlen(docLayer->Name) + 1);
		MemFree(docLayer, sizeof(DocLayer));
	}
}

/*
 *	Return pointer to last (i.e. top) layer in document
 */

DocLayerPtr TopLayer(DocDataPtr docData)
{
	register DocLayerPtr docLayer;

	if (docData->Layers == NULL)
		return (NULL);
	for (docLayer = docData->Layers; docLayer->Next; docLayer = docLayer->Next) ;
	return (docLayer);
}

/*
 *	Return pointer to first (i.e. bottom) layer in document
 */

DocLayerPtr BottomLayer(DocDataPtr docData)
{
	return (docData->Layers);
}

/*
 *	Return pointer to next layer
 */

DocLayerPtr NextLayer(DocLayerPtr docLayer)
{
	return (docLayer->Next);
}

/*
 *	Return pointer to prev layer
 */

DocLayerPtr PrevLayer(DocLayerPtr docLayer)
{
	return (docLayer->Prev);
}

/*
 *	Return pointer to next visible layer
 */

DocLayerPtr NextVisLayer(DocLayerPtr docLayer)
{
	for (docLayer = docLayer->Next; docLayer && !LayerVisible(docLayer); docLayer = docLayer->Next) ;
	return (docLayer);
}

/*
 *	Return pointer to prev visible layer
 */

DocLayerPtr PrevVisLayer(DocLayerPtr docLayer)
{
	for (docLayer = docLayer->Prev; docLayer && !LayerVisible(docLayer); docLayer = docLayer->Prev) ;
	return (docLayer);
}

/*
 *	Return pointer to last (i.e. top) visible layer in document
 */

DocLayerPtr TopVisLayer(DocDataPtr docData)
{
	register DocLayerPtr docLayer;

	if (docData->Layers == NULL)
		return (NULL);
	for (docLayer = TopLayer(docData); docLayer; docLayer = docLayer->Prev) {
		if (LayerVisible(docLayer))
			break;
	}
	return (docLayer);
}

/*
 *	Return pointer to first (i.e. bottom) visible layer in document
 */

DocLayerPtr BottomVisLayer(DocDataPtr docData)
{
	register DocLayerPtr docLayer;

	if (docData->Layers == NULL)
		return (NULL);
	for (docLayer = BottomLayer(docData); docLayer; docLayer = docLayer->Next) {
		if (LayerVisible(docLayer))
			break;
	}
	return (docLayer);
}

/*
 *	Insert layer into layer list after given layer
 *	If afterLayer is NULL, insert at start of list
 */

void InsertLayer(DocDataPtr docData, DocLayerPtr afterLayer, DocLayerPtr docLayer)
{
	if (docLayer == NULL)
		return;
	if (afterLayer == NULL) {
		docLayer->Prev = NULL;
		docLayer->Next = docData->Layers;
		if (docData->Layers)
			docData->Layers->Prev = docLayer;
		docData->Layers = docLayer;
	}
	else {
		docLayer->Prev = afterLayer;
		docLayer->Next = afterLayer->Next;
		if (afterLayer->Next)
			afterLayer->Next->Prev = docLayer;
		afterLayer->Next = docLayer;
	}
}

/*
 *	Move layer forward
 */

void LayerForward(DocDataPtr docData, DocLayerPtr docLayer)
{
	DocLayerPtr nextLayer, currLayer;

	if (docLayer == NULL || (nextLayer = NextLayer(docLayer)) == NULL)
		return;
	currLayer = docData->CurrLayer;		/* Could change */
	DetachLayer(docData, docLayer);
	InsertLayer(docData, nextLayer, docLayer);
	docData->CurrLayer = currLayer;
}

/*
 *	Move layer backward
 */

void LayerBackward(DocDataPtr docData, DocLayerPtr docLayer)
{
	DocLayerPtr prevLayer, currLayer;

	if (docLayer == NULL || (prevLayer = docLayer->Prev) == NULL)
		return;
	currLayer = docData->CurrLayer;		/* Could change */
	DetachLayer(docData, prevLayer);
	InsertLayer(docData, docLayer, prevLayer);
	docData->CurrLayer = currLayer;
}

/*
 *	Return pointer to current layer
 */

DocLayerPtr CurrLayer(DocDataPtr docData)
{
	return (docData->CurrLayer);
}

/*
 *	Set current layer to given layer
 */

void SetCurrLayer(DocDataPtr docData, DocLayerPtr docLayer)
{
	docData->CurrLayer = docLayer;
}

/*
 *	Return number of layer in document, bottom layer is number 0
 */

WORD LayerNum(DocDataPtr docData, DocLayerPtr docLayer)
{
	WORD layerNum;
	DocLayerPtr layer;

	layerNum = 0;
	for (layer = docData->Layers; layer && layer != docLayer; layer = layer->Next)
		layerNum++;
	return (layerNum);
}

/*
 *	Return total number of layers in document
 */

WORD NumLayers(DocDataPtr docData)
{
	WORD num;
	DocLayerPtr layer;

	num = 0;
	for (layer = docData->Layers; layer; layer = layer->Next)
		num++;
	return (num);
}

/*
 *	Return pointer to layer given layer number (from bottom)
 */

DocLayerPtr GetLayer(DocDataPtr docData, WORD num)
{
	DocLayerPtr layer;

	layer = docData->Layers;
	while (layer && num--)
		layer = layer->Next;
	return (layer);
}

/*
 *	Return pointer to layer name (or NULL if no name)
 */

TextPtr GetLayerName(DocLayerPtr docLayer)
{
	if (docLayer == NULL)
		return (NULL);
	return (docLayer->Name);
}

/*
 *	Set layer name (or NULL for no name)
 */

void SetLayerName(DocLayerPtr docLayer, TextPtr name)
{
	TextPtr newName;

	if (docLayer == NULL)
		return;
	if (name == NULL)
		newName = NULL;
	else if ((newName = MemAlloc(strlen(name) + 1, 0)) == NULL)
		return;
	else
		strcpy(newName, name);
	if (docLayer->Name)
		MemFree(docLayer->Name, strlen(docLayer->Name) + 1);
	docLayer->Name = newName;
}

/*
 *	Set view to specified layer
 */

static void SetLayer(WindowPtr window, DocLayerPtr docLayer)
{
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
	Rectangle rect;

	UnSelectAllObjects(docData);
	SetCurrLayer(docData, docLayer);
	SetLayerIndic(window);
	AdjustWindowTitle(window);
	GetContentRect(window, &rect);
	InvalRect(window, &rect);
}

/*
 *	Move up to next layer
 */

BOOL DoLayerUp(WindowPtr window, BOOL top)
{
	DocLayerPtr docLayer;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);

	docLayer = (top) ? TopVisLayer(docData) : NextVisLayer(CurrLayer(docData));
	if (docLayer == NULL) {
		ErrBeep();
		return (FALSE);
	}
	SetLayer(window, docLayer);
	return (TRUE);
}

/*
 *	Move down to previous layer
 */

BOOL DoLayerDown(WindowPtr window, BOOL bottom)
{
	DocLayerPtr docLayer;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);

	docLayer = (bottom) ? BottomVisLayer(docData) : PrevVisLayer(CurrLayer(docData));
	if (docLayer == NULL) {
		ErrBeep();
		return (FALSE);
	}
	SetLayer(window, docLayer);
	return (TRUE);
}

/*
 *	Set to given layer number
 */

BOOL DoLayerNum(WindowPtr window, WORD num)
{
	DocLayerPtr docLayer;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);

	docLayer = GetLayer(docData, num);
	if (docLayer == NULL)
		return (FALSE);
	SetLayer(window, docLayer);
	return (TRUE);
}
