/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Toolbox library
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Memory allocation definitions
 */

#ifndef TOOLBOX_MEMORY_H
#define TOOLBOX_MEMORY_H

#ifndef EXEC_TYPES_H
#include <exec/types.h>
#endif

#ifndef EXEC_MEMORY_H
#include <exec/memory.h>
#endif

#ifndef TYPEDEF_H
#include "typedefs.h"
#endif

/*
 *	Memory allocation routines
 *	All memory allocations are long word aligned
 */

Ptr		MemAlloc(ULONG, LONG);
void	MemFree(Ptr, ULONG);
ULONG	MemAvail(LONG);
void	BlockClear(Ptr, ULONG);
void	BlockMove(Ptr, Ptr, ULONG);
void	SwapMem(Ptr, Ptr, ULONG);

#endif
