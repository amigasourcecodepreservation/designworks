/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Object handling routines
 */

#include <exec/types.h>
#include <graphics/gfxmacros.h>
#include <intuition/intuition.h>

#include <proto/graphics.h>

#include <Toolbox/Memory.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Window.h>

#include "Draw.h"
#include "Proto.h"

/*
 *	External variables and definitions
 */

extern Options	options;

/*
 *	Local prototypes
 */

static void	UnCachePicts(DocObjPtr);

/*
 *	Create a new object and append to given layer
 *	If layer is NULL, just create object
 *	(Can't call this NewObject() since intuition uses that name)
 */

typedef DocObjPtr	(*ObjAllocateProc)(void);

static ObjAllocateProc	objAllocate[] = {
	(ObjAllocateProc) GroupAllocate,	(ObjAllocateProc) TextAllocate,
	(ObjAllocateProc) BMapAllocate,		(ObjAllocateProc) LineAllocate,
	(ObjAllocateProc) RectAllocate,		(ObjAllocateProc) OvalAllocate,
	(ObjAllocateProc) PolyAllocate,		(ObjAllocateProc) EPSFAllocate
};

DocObjPtr NewDocObject(DocLayerPtr docLayer, WORD type)
{
	DocObjPtr	newObj, topObj;

	newObj = (*objAllocate[type])();
	if (newObj == NULL)
		return (NULL);
	newObj->Type = type;
	if (docLayer) {
		if (docLayer->Objects == NULL)
			docLayer->Objects = newObj;
		else {
			topObj = TopObject(docLayer);
			topObj->Next = newObj;
			newObj->Prev = topObj;
		}
	}
	return (newObj);
}

/*
 *	Duplicate specified object and add to top of given layer
 *	Return pointer to object
 */

typedef BOOL	(*ObjDupDataProc)(DocObjPtr, DocObjPtr);

static ObjDupDataProc	objDupData[] = {
	(ObjDupDataProc) GroupDupData,	(ObjDupDataProc) TextDupData,
	(ObjDupDataProc) BMapDupData,	(ObjDupDataProc) LineDupData,
	(ObjDupDataProc) RectDupData,	(ObjDupDataProc) OvalDupData,
	(ObjDupDataProc) PolyDupData,	(ObjDupDataProc) EPSFDupData
};

DocObjPtr DuplicateObject(DocLayerPtr docLayer, DocObjPtr docObj)
{
	BOOL		success;
	DocObjPtr	newObj;

	if ((newObj = NewDocObject(docLayer, docObj->Type)) == NULL)
		return (NULL);
	newObj->Flags = docObj->Flags;
	newObj->Frame = docObj->Frame;
	success = (*objDupData[docObj->Type])(docObj, newObj);
	if (!success) {
		if (docLayer)
			DetachObject(docLayer, newObj);
		DisposeDocObject(newObj);
		newObj = NULL;
	}
	return (newObj);
}

/*
 *	Detach object from layer list (but don't dispose)
 */

void DetachObject(DocLayerPtr docLayer, DocObjPtr docObj)
{
	if (docObj->Group && docObj->Group->Objects == docObj)
		docObj->Group->Objects = docObj->Next;
	if (docObj->Next)
		docObj->Next->Prev = docObj->Prev;
	if (docObj->Prev)
		docObj->Prev->Next = docObj->Next;
	if (docLayer->Objects == docObj)
		docLayer->Objects = docObj->Next;
	docObj->Next = docObj->Prev = NULL;
}

/*
 *	Free memory used by object
 *	Does not adjust object links
 *	(Can't call this DisposeObject() since intuition uses that name)
 */

typedef void	(*ObjDisposeProc)(DocObjPtr);

static ObjDisposeProc	objDispose[] = {
	(ObjDisposeProc) GroupDupData,	(ObjDisposeProc) TextDupData,
	(ObjDisposeProc) BMapDupData,	(ObjDisposeProc) LineDupData,
	(ObjDisposeProc) RectDupData,	(ObjDisposeProc) OvalDupData,
	(ObjDisposeProc) PolyDupData,	(ObjDisposeProc) EPSFDupData
};

void DisposeDocObject(DocObjPtr docObj)
{
	if (docObj == NULL)
		return;
	(*objDispose[docObj->Type])(docObj);
}

/*
 *	Free of all objects in list
 *	Does not adjust object links
 */

void DisposeAllDocObjects(DocObjPtr docObj)
{
	DocObjPtr nextObj;

	while (docObj) {
		nextObj = NextObj(docObj);
		DisposeDocObject(docObj);
		docObj = nextObj;
	}
}

/*
 *	Return pointer to last (i.e. top) object in layer
 */

DocObjPtr TopObject(DocLayerPtr docLayer)
{
	register DocObjPtr docObj;

	if (docLayer->Objects == NULL)
		return (NULL);
	for (docObj = docLayer->Objects; NextObj(docObj); docObj = NextObj(docObj)) ;
	return (docObj);
}

/*
 *	Return pointer to first (i.e. bottom) object in layer
 */

DocObjPtr BottomObject(DocLayerPtr docLayer)
{
	return (docLayer->Objects);
}

/*
 *	Insert object into layer object list after given object
 *	If afterObj is NULL, insert at start of list
 */

void InsertObject(DocLayerPtr docLayer, DocObjPtr afterObj, DocObjPtr docObj)
{
	if (docObj == NULL)
		return;
	docObj->Group = NULL;
	if (afterObj == NULL) {
		docObj->Prev = NULL;
		docObj->Next = docLayer->Objects;
		if (docLayer->Objects)
			docLayer->Objects->Prev = docObj;
		docLayer->Objects = docObj;
	}
	else {
		docObj->Prev = afterObj;
		docObj->Next = afterObj->Next;
		if (afterObj->Next)
			afterObj->Next->Prev = docObj;
		afterObj->Next = docObj;
	}
}

/*
 *	Insert all objects in list after given object
 *	If afterObj is NULL, insert at start of list
 */

void InsertAllObjects(DocLayerPtr docLayer, DocObjPtr afterObj, DocObjPtr firstObj)
{
	DocObjPtr lastObj;

	if (firstObj == NULL)
		return;
	for (lastObj = firstObj; NextObj(lastObj); lastObj = NextObj(lastObj))
		lastObj->Group = NULL;
	lastObj->Group = NULL;
	if (afterObj == NULL) {
		firstObj->Prev = NULL;
		lastObj->Next = docLayer->Objects;
		if (docLayer->Objects)
			docLayer->Objects->Prev = lastObj;
		docLayer->Objects = firstObj;
	}
	else {
		firstObj->Prev = afterObj;
		lastObj->Next = afterObj->Next;
		if (afterObj->Next)
			afterObj->Next->Prev = lastObj;
		afterObj->Next = firstObj;
	}
}

/*
 *	Append object to end of list
 */

void AppendObject(DocLayerPtr docLayer, DocObjPtr docObj)
{
	InsertObject(docLayer, TopObject(docLayer), docObj);
}

/*
 *	Append object to end of group
 */

void AppendToGroup(GroupObjPtr groupObj, DocObjPtr docObj)
{
	DocObjPtr topObj;

	docObj->Group = groupObj;
	if (groupObj->Objects == NULL)
		groupObj->Objects = docObj;
	else {
		for (topObj = groupObj->Objects; NextObj(topObj); topObj = NextObj(topObj)) ;
		topObj->Next = docObj;
		docObj->Prev = topObj;
		docObj->Next = NULL;
	}
}

/*
 *	Move object forward
 */

void ObjectForward(DocLayerPtr docLayer, DocObjPtr docObj)
{
	DocObjPtr nextObj;

	if (docObj == NULL || (nextObj = NextObj(docObj)) == NULL)
		return;
	DetachObject(docLayer, docObj);
	InsertObject(docLayer, nextObj, docObj);
}

/*
 *	Move object to front
 */

void ObjectToFront(DocLayerPtr docLayer, DocObjPtr docObj)
{
	DocObjPtr topObj;

	if (docObj == NULL)
		return;
	topObj = TopObject(docLayer);
	if (topObj == NULL || topObj == docObj)
		return;
	DetachObject(docLayer, docObj);
	AppendObject(docLayer, docObj);
}

/*
 *	Move object backward
 */

void ObjectBackward(DocLayerPtr docLayer, DocObjPtr docObj)
{
	DocObjPtr prevObj;

	if (docObj == NULL || (prevObj = docObj->Prev) == NULL)
		return;
	DetachObject(docLayer, prevObj);
	InsertObject(docLayer, docObj, prevObj);
}

/*
 *	Move object to back
 */

void ObjectToBack(DocLayerPtr docLayer, DocObjPtr docObj)
{
	DocObjPtr bottomObj;

	if (docObj == NULL)
		return;
	bottomObj = BottomObject(docLayer);
	if (bottomObj == NULL || bottomObj == docObj)
		return;
	DetachObject(docLayer, docObj);
	InsertObject(docLayer, NULL, docObj);
}

/*
 *	Return pointer to first object in current layer
 */

DocObjPtr FirstObject(DocDataPtr docData)
{
	DocLayerPtr currLayer = CurrLayer(docData);

	if (currLayer == NULL)
		return (NULL);
	return (BottomObject(currLayer));
}

/*
 *	Return pointer to last object in current layer
 */

DocObjPtr LastObject(DocDataPtr docData)
{
	DocLayerPtr currLayer = CurrLayer(docData);

	if (currLayer == NULL)
		return (NULL);
	return (TopObject(currLayer));
}

/*
 *	Return pointer to first selected object in current layer (or NULL)
 */

DocObjPtr FirstSelected(DocDataPtr docData)
{
	register DocObjPtr docObj;

	for (docObj = FirstObject(docData); docObj; docObj = NextObj(docObj)) {
		if (ObjectSelected(docObj))
			break;
	}
	return (docObj);
}

/*
 *	Return pointer to last selected object in current layer (or NULL)
 */

DocObjPtr LastSelected(DocDataPtr docData)
{
	register DocObjPtr docObj;

	for (docObj = LastObject(docData); docObj; docObj = docObj->Prev) {
		if (ObjectSelected(docObj))
			break;
	}
	return (docObj);
}

/*
 *	Return pointer to next selected object in current layer (or NULL)
 */

DocObjPtr NextSelected(DocObjPtr docObj)
{
	for (docObj = NextObj(docObj); docObj; docObj = NextObj(docObj)) {
		if (ObjectSelected(docObj))
			break;
	}
	return (docObj);
}

/*
 *	Return pointer to prev selected object in current layer (or NULL)
 */

DocObjPtr PrevSelected(DocObjPtr docObj)
{
	for (docObj = docObj->Prev; docObj; docObj = docObj->Prev) {
		if (ObjectSelected(docObj))
			break;
	}
	return (docObj);
}

/*
 *	Un-select all objects in current layer
 */

void UnSelectAllObjects(DocDataPtr docData)
{
	DocObjPtr docObj;

	for (docObj = FirstObject(docData); docObj; docObj = NextObj(docObj))
		UnSelectObject(docObj);
}

/*
 *	Return TRUE if any selected object is locked
 */

BOOL ObjectsLocked(DocDataPtr docData)
{
	register DocObjPtr docObj;

	if (!LockDocument(docData))
		return (TRUE);

	for (docObj = FirstSelected(docData); docObj; docObj = NextSelected(docObj)) {
		if (ObjectLocked(docObj))
			return (TRUE);
	}
	return (FALSE);
}

/*
 *	Draw object, scaling frame into dstRect
 *	Should only draw portions in clipRect (in rPort coordinates)
 *	Return TRUE if object was in clipRect
 */

typedef void	(*ObjDrawProc)(RastPtr, DocObjPtr, RectPtr, RectPtr);

static ObjDrawProc	objDraw[] = {
	(ObjDrawProc) GroupDrawObj,	(ObjDrawProc) TextDrawObj,
	(ObjDrawProc) BMapDrawObj,	(ObjDrawProc) LineDrawObj,
	(ObjDrawProc) RectDrawObj,	(ObjDrawProc) OvalDrawObj,
	(ObjDrawProc) PolyDrawObj,	(ObjDrawProc) EPSFDrawObj
};

BOOL DrawObject(RastPtr rPort, DocObjPtr docObj, RectPtr dstRect, RectPtr clipRect)
{
	Rectangle	newClipRect;

/*
	First see if object rect is within clip rect
*/
	if (!SectRect(dstRect, clipRect, &newClipRect))
		return (FALSE);
/*
	Draw object
*/
	(*objDraw[docObj->Type])(rPort, docObj, dstRect, &newClipRect);
	return (TRUE);
}

/*
 *	Draw object outline
 *	Offset is in document coordinates
 */

typedef void	(*ObjDrawOutlineProc)(WindowPtr, DocObjPtr, WORD, WORD);

static ObjDrawOutlineProc	objDrawOutline[] = {
	(ObjDrawOutlineProc) GroupDrawOutline,	(ObjDrawOutlineProc) TextDrawOutline,
	(ObjDrawOutlineProc) BMapDrawOutline,	(ObjDrawOutlineProc) LineDrawOutline,
	(ObjDrawOutlineProc) RectDrawOutline,	(ObjDrawOutlineProc) OvalDrawOutline,
	(ObjDrawOutlineProc) PolyDrawOutline,	(ObjDrawOutlineProc) EPSFDrawOutline
};

void DrawObjectOutline(WindowPtr window, DocObjPtr docObj, WORD xOffset, WORD yOffset)
{
	RastPtr		rPort = window->RPort;
	Rectangle	rect;

	if (options.DragOutlines || docObj->Type == TYPE_LINE)
		(*objDrawOutline[docObj->Type])(window, docObj, xOffset, yOffset);
	else {
		rect = docObj->Frame;
		OffsetRect(&rect, xOffset, yOffset);
		DocToWindowRect(window, &rect, &rect);
		SetDrMd(rPort, COMPLEMENT);
		SetDrPt(rPort, LINE_PAT);
		FrameRect(rPort, &rect);
	}
}

/*
 *	Hilite object
 */

typedef void	(*ObjHiliteProc)(WindowPtr, DocObjPtr);

static ObjHiliteProc	objHilite[] = {
	(ObjHiliteProc) GroupHilite,	(ObjHiliteProc) TextHilite,
	(ObjHiliteProc) BMapHilite,		(ObjHiliteProc) LineHilite,
	(ObjHiliteProc) RectHilite,		(ObjHiliteProc) OvalHilite,
	(ObjHiliteProc) PolyHilite,		(ObjHiliteProc) EPSFHilite
};

void HiliteObject(WindowPtr window, DocObjPtr docObj)
{
	(*objHilite[docObj->Type])(window, docObj);
}

/*
 *	Enable or disable object pen
 */

void EnableObjectPen(DocObjPtr docObj, BOOL enable)
{
	if (enable)
		docObj->Flags |= OBJ_DO_PEN;
	else
		docObj->Flags &= ~OBJ_DO_PEN;
	if (docObj->Type == TYPE_GROUP) {
		for (docObj = ((GroupObjPtr) docObj)->Objects; docObj; docObj = NextObj(docObj))
			EnableObjectPen(docObj, enable);
	}
}

/*
 *	Set object pen color
 */

typedef void	(*ObjSetPenColorProc)(DocObjPtr, RGBColor);

static ObjSetPenColorProc	objSetPenColor[] = {
	(ObjSetPenColorProc) GroupSetPenColor,	(ObjSetPenColorProc) TextSetPenColor,
	(ObjSetPenColorProc) NULL,				(ObjSetPenColorProc) LineSetPenColor,
	(ObjSetPenColorProc) RectSetPenColor,	(ObjSetPenColorProc) OvalSetPenColor,
	(ObjSetPenColorProc) PolySetPenColor,	(ObjSetPenColorProc) NULL
};

void SetObjectPenColor(DocObjPtr docObj, RGBColor penColor)
{
	if (objSetPenColor[docObj->Type])
		(*objSetPenColor[docObj->Type])(docObj, penColor);
}

/*
 *	Return object pen size
 *	Return FALSE (and -1 for size) if object does not have a pen size
 */

BOOL GetObjectPenSize(DocObjPtr docObj, WORD *penWidth, WORD *penHeight)
{
	switch (docObj->Type) {
	case TYPE_LINE:
		*penWidth  = ((LineObjPtr) docObj)->PenWidth;
		*penHeight = ((LineObjPtr) docObj)->PenHeight;
		break;
	case TYPE_RECT:
		*penWidth  = ((RectObjPtr) docObj)->PenWidth;
		*penHeight = ((RectObjPtr) docObj)->PenHeight;
		break;
	case TYPE_OVAL:
		*penWidth  = ((OvalObjPtr) docObj)->PenWidth;
		*penHeight = ((OvalObjPtr) docObj)->PenHeight;
		break;
	case TYPE_POLY:
		*penWidth  = ((PolyObjPtr) docObj)->PenWidth;
		*penHeight = ((PolyObjPtr) docObj)->PenHeight;
		break;
	default:
		*penWidth = *penHeight = -1;
		break;
	}
	return ((BOOL) (*penWidth != -1 && *penHeight != -1));
}

/*
 *	Set object pen size
 *	If size is -1 then do not change
 */

typedef void	(*ObjSetPenSizeProc)(DocObjPtr, WORD, WORD);

static ObjSetPenSizeProc	objSetPenSize[] = {
	(ObjSetPenSizeProc) GroupSetPenSize,	(ObjSetPenSizeProc) TextSetPenSize,
	(ObjSetPenSizeProc) NULL,				(ObjSetPenSizeProc) LineSetPenSize,
	(ObjSetPenSizeProc) RectSetPenSize,		(ObjSetPenSizeProc) OvalSetPenSize,
	(ObjSetPenSizeProc) PolySetPenSize,		(ObjSetPenSizeProc) NULL
};

void SetObjectPenSize(DocObjPtr docObj, WORD penWidth, WORD penHeight)
{
	if (objSetPenSize[docObj->Type])
		(*objSetPenSize[docObj->Type])(docObj, penWidth, penHeight);
}

/*
 *	Enable or disable object fill
 */

void EnableObjectFill(DocObjPtr docObj, BOOL enable)
{
	if (enable)
		docObj->Flags |= OBJ_DO_FILL;
	else
		docObj->Flags &= ~OBJ_DO_FILL;
	if (docObj->Type == TYPE_GROUP) {
		for (docObj = ((GroupObjPtr) docObj)->Objects; docObj; docObj = NextObj(docObj))
			EnableObjectFill(docObj, enable);
	}
}

/*
 *	Set object fill pattern
 */

typedef void	(*ObjSetFillPatProc)(DocObjPtr, RGBPat8Ptr);

static ObjSetFillPatProc	objSetFillPat[] = {
	(ObjSetFillPatProc) GroupSetFillPat,	(ObjSetFillPatProc) TextSetFillPat,
	(ObjSetFillPatProc) NULL,				(ObjSetFillPatProc) LineSetFillPat,
	(ObjSetFillPatProc) RectSetFillPat,		(ObjSetFillPatProc) OvalSetFillPat,
	(ObjSetFillPatProc) PolySetFillPat,		(ObjSetFillPatProc) NULL
};

void SetObjectFillPat(DocObjPtr docObj, RGBPat8Ptr fillPat)
{
	if (objSetFillPat[docObj->Type])
		(*objSetFillPat[docObj->Type])(docObj, fillPat);
}

/*
 *	Get object fill pattern
 */

typedef BOOL	(*ObjGetFillPatProc)(DocObjPtr, RGBPat8Ptr);

static ObjGetFillPatProc	objGetFillPat[] = {
	(ObjGetFillPatProc) GroupGetFillPat,	(ObjGetFillPatProc) TextGetFillPat,
	(ObjGetFillPatProc) NULL,				(ObjGetFillPatProc) LineGetFillPat,
	(ObjGetFillPatProc) RectGetFillPat,		(ObjGetFillPatProc) OvalGetFillPat,
	(ObjGetFillPatProc) PolyGetFillPat,		(ObjGetFillPatProc) NULL
};

BOOL GetObjectFillPat(DocObjPtr docObj, RGBPat8Ptr fillPat)
{
	BOOL	success;

	if (objGetFillPat[docObj->Type])
		success = (*objGetFillPat[docObj->Type])(docObj, fillPat);
	else
		success = FALSE;
	return (success);
}

/*
 *	Set object text parameters
 *	If parameter is -1 then do not change
 */

void SetObjectTextParams(DocObjPtr docObj, FontNum fontNum, FontSize fontSize,
						 WORD style, WORD miscStyle, WORD justify, WORD spacing)
{
	switch (docObj->Type) {
	case TYPE_TEXT:
		TextSetTextParams((TextObjPtr) docObj, fontNum, fontSize, style, miscStyle,
						  justify, spacing);
		break;
	case TYPE_GROUP:
		GroupSetTextParams((GroupObjPtr) docObj, fontNum, fontSize, style, miscStyle,
						   justify, spacing);
		break;
	}
}

/*
 *	Rotate object by given angle about center
 */

typedef void	(*ObjRotateProc)(DocObjPtr, WORD, WORD, WORD);

static ObjRotateProc	objRotate[] = {
	(ObjRotateProc) GroupRotate,	(ObjRotateProc) TextRotate,
	(ObjRotateProc) BMapRotate,		(ObjRotateProc) LineRotate,
	(ObjRotateProc) RectRotate,		(ObjRotateProc) OvalRotate,
	(ObjRotateProc) PolyRotate,		(ObjRotateProc) EPSFRotate
};

void RotateObject(DocObjPtr docObj, WORD cx, WORD cy, WORD angle)
{
	(*objRotate[docObj->Type])(docObj, cx, cy, angle);
}

/*
 *	Flip object horizontally or vertically
 */

typedef void	(*ObjFlipProc)(DocObjPtr, WORD, WORD, BOOL);

static ObjFlipProc	objFlip[] = {
	(ObjFlipProc) GroupFlip,	(ObjFlipProc) TextFlip,
	(ObjFlipProc) BMapFlip,		(ObjFlipProc) LineFlip,
	(ObjFlipProc) RectFlip,		(ObjFlipProc) OvalFlip,
	(ObjFlipProc) PolyFlip,		(ObjFlipProc) EPSFFlip
};

void FlipObject(DocObjPtr docObj, WORD cx, WORD cy, BOOL horiz)
{
	(*objFlip[docObj->Type])(docObj, cx, cy, horiz);
}

/*
 *	Determine if point is in object
 */

typedef BOOL	(*ObjSelectProc)(DocObjPtr, PointPtr);

static ObjSelectProc	objSelect[] = {
	(ObjSelectProc) GroupSelect,	(ObjSelectProc) TextSelect,
	(ObjSelectProc) BMapSelect,		(ObjSelectProc) LineSelect,
	(ObjSelectProc) RectSelect,		(ObjSelectProc) OvalSelect,
	(ObjSelectProc) PolySelect,		(ObjSelectProc) EPSFSelect
};

BOOL PointInObject(DocObjPtr docObj, PointPtr pt)
{
	WORD		penWidth, penHeight;
	Rectangle	rect;

/*
	Pre-screen for point inside of object frame
	(Include slop on either side)
*/
	rect = docObj->Frame;
	GetObjectPenSize(docObj, &penWidth, &penHeight);
	InsetRect(&rect, -penWidth, -penHeight);
	InsetRect(&rect, -2, -2);
	if (!PtInRect(pt, &rect))
		return (FALSE);
/*
	In rect, so do specific check
*/
	return ((*objSelect[docObj->Type])(docObj, pt));
}

/*
 *	Return handle number of point, or -1 if not in object handle
 */

typedef WORD	(*ObjHandleProc)(DocObjPtr, PointPtr, RectPtr);

static ObjHandleProc	objHandle[] = {
	(ObjHandleProc) GroupHandle,	(ObjHandleProc) TextHandle,
	(ObjHandleProc) BMapHandle,		(ObjHandleProc) LineHandle,
	(ObjHandleProc) RectHandle,		(ObjHandleProc) OvalHandle,
	(ObjHandleProc) PolyHandle,		(ObjHandleProc) EPSFHandle
};

WORD PointToHandle(DocObjPtr docObj, PointPtr pt, RectPtr handleRect)
{
	Rectangle	rect;

/*
	Pre-screen for point inside of object handle rectangle
*/
	rect = docObj->Frame;
	InsetRect(&rect, handleRect->MinX, handleRect->MinY);
	if (!PtInRect(pt, &rect))
		return (-1);
/*
	In rect, so do specific check
*/
	return ((*objHandle[docObj->Type])(docObj, pt, handleRect));
}

/*
 *	Offset object by given x and y displacement
 */

typedef void (*ObjOffsetProc)(DocObjPtr, WORD, WORD);

static ObjOffsetProc	objOffset[] = {
	(ObjOffsetProc) NULL,		(ObjOffsetProc) NULL,
	(ObjOffsetProc) NULL,		(ObjOffsetProc) NULL,
	(ObjOffsetProc) RectOffset,	(ObjOffsetProc) NULL,
	(ObjOffsetProc) NULL,		(ObjOffsetProc) NULL
};

void OffsetObject(DocObjPtr docObj, WORD xOffset, WORD yOffset)
{
	if (objOffset[docObj->Type])
		(*objOffset[docObj->Type])(docObj, xOffset, yOffset);
	else
		OffsetRect(&docObj->Frame, xOffset, yOffset);
}

/*
 *	Scale object to new frame rectangle
 */

typedef void	(*ObjScaleProc)(DocObjPtr, RectPtr);

static ObjScaleProc	objScale[] = {
	(ObjScaleProc) GroupScale,	(ObjScaleProc) TextScale,
	(ObjScaleProc) BMapScale,	(ObjScaleProc) LineScale,
	(ObjScaleProc) RectScale,	(ObjScaleProc) OvalScale,
	(ObjScaleProc) PolyScale,	(ObjScaleProc) EPSFScale
};

void ScaleObject(DocObjPtr docObj, RectPtr frame)
{
/*
	Don't allow resize to single pixel size
*/
	if (frame->MaxX <= frame->MinX)
		frame->MaxX = frame->MinX + 1;
	if (frame->MaxY <= frame->MinY)
		frame->MaxY = frame->MinY + 1;
/*
	Do scale
*/
	(*objScale[docObj->Type])(docObj, frame);
}

/*
 *	Convert object to poly
 */

typedef PolyObjPtr	(*ObjConvertToPolyProc)(DocObjPtr);

static ObjConvertToPolyProc	objConvertToPoly[] = {
	(ObjConvertToPolyProc) NULL,				(ObjConvertToPolyProc) NULL,
	(ObjConvertToPolyProc) NULL,				(ObjConvertToPolyProc) LineConvertToPoly,
	(ObjConvertToPolyProc) RectConvertToPoly,	(ObjConvertToPolyProc) OvalConvertToPoly,
	(ObjConvertToPolyProc) NULL,				(ObjConvertToPolyProc) NULL
};

PolyObjPtr ConvertObjectToPoly(DocObjPtr docObj)
{
	PolyObjPtr	polyObj;

	if (objConvertToPoly[docObj->Type])
		polyObj = (*objConvertToPoly[docObj->Type])(docObj);
	else
		polyObj = NULL;
	return (polyObj);
}

/*
 *	Adjust rectangle to be within document bounds
 */

void AdjustToDocBounds(DocDataPtr docData, RectPtr rect)
{
	if (rect->MaxX >= docData->DocWidth)
		OffsetRect(rect, docData->DocWidth - rect->MaxX - 1, 0);
	if (rect->MaxY >= docData->DocHeight)
		OffsetRect(rect, 0, docData->DocHeight - rect->MaxY - 1);
	if (rect->MinX < 0)
		OffsetRect(rect, -rect->MinX, 0);
	if (rect->MinY < 0)
		OffsetRect(rect, 0, -rect->MinY);
}

/*
 *	Invalidate display rectangle of object in window
 *	(Adjusts for line widths and arrows)
 */

void InvalObjectRect(WindowPtr window, DocObjPtr docObj)
{
	LineObjPtr	lineObj;
	GroupObjPtr	group;
	Point		pen;
	Rectangle	rect, contRect;

/*
	Special case for groups:  invalidate each individual object rect
	(Since objects with thick pens may be outside the group rect)
*/
	if (docObj->Type == TYPE_GROUP) {
		for (docObj = ((GroupObjPtr) docObj)->Objects; docObj; docObj = NextObj(docObj))
			InvalObjectRect(window, docObj);
	}
/*
	Handle non-group objects
*/
	else {
		rect = docObj->Frame;
		for (group = docObj->Group; group; group = group->DocObj.Group)
			OffsetRect(&rect, group->DocObj.Frame.MinX, group->DocObj.Frame.MinY);
		if (GetObjectPenSize(docObj, &pen.x, &pen.y)) {
			InsetRect(&rect, -pen.x/2 - 1, -pen.y/2 - 1);	// Extra for security
			if (docObj->Type == TYPE_LINE) {
				lineObj = (LineObjPtr) docObj;
				if (lineObj->LineFlags & (LINE_ARROWSTART | LINE_ARROWEND))
					InsetRect(&rect, -4, -4);
			}
		}
		DocToWindowRect(window, &rect, &rect);
		GetContentRect(window, &contRect);
		if (SectRect(&rect, &contRect, &rect))
			InvalRect(window, &rect);
	}
}

/*
 *	Draw object to PostScript file, scaling frame into dstRect
 */

typedef BOOL	(*ObjDrawPSObjProc)(PrintRecPtr, DocObjPtr, RectPtr, RectPtr);

static ObjDrawPSObjProc	objDrawPSObj[] = {
	(ObjDrawPSObjProc) GroupDrawPSObj,	(ObjDrawPSObjProc) TextDrawPSObj,
	(ObjDrawPSObjProc) BMapDrawPSObj,	(ObjDrawPSObjProc) LineDrawPSObj,
	(ObjDrawPSObjProc) RectDrawPSObj,	(ObjDrawPSObjProc) OvalDrawPSObj,
	(ObjDrawPSObjProc) PolyDrawPSObj,	(ObjDrawPSObjProc) EPSFDrawPSObj
};

BOOL DrawPSObject(PrintRecPtr printRec, DocObjPtr docObj, RectPtr dstRect, RectPtr clipRect)
{
	Rectangle	rect;

	if (!SectRect(dstRect, clipRect, &rect))
		return (TRUE);
	return ((*objDrawPSObj[docObj->Type])(printRec, docObj, dstRect, clipRect));
}

/*
 *	Create a new REXX object in window
 */

typedef DocObjPtr	(*ObjNewREXXProc)(DocDataPtr, TextPtr);

static ObjNewREXXProc	objNewREXX[] = {
	(ObjNewREXXProc) NULL,			(ObjNewREXXProc) TextNewREXX,
	(ObjNewREXXProc) NULL,			(ObjNewREXXProc) LineNewREXX,
	(ObjNewREXXProc) RectNewREXX,	(ObjNewREXXProc) OvalNewREXX,
	(ObjNewREXXProc) PolyNewREXX,	(ObjNewREXXProc) NULL
};

BOOL NewREXXObject(WindowPtr window, WORD type, TextPtr args)
{
	DocObjPtr	docObj;
	DocDataPtr	docData = GetWRefCon(window);

	HiliteSelectOff(window);
	UnSelectAllObjects(docData);
	if (objNewREXX[type])
		docObj = (*objNewREXX[type])(docData, args);
	else
		docObj = NULL;
	if (docObj) {
		SelectObject(docObj);
		InvalObjectRect(window, docObj);
		DocModified(docData);
	}
	if (window->Flags & WINDOWACTIVE)
		HiliteSelectOn(window);
	return ((BOOL) (docObj != NULL));
}

/*
 *	Uncache all pictures in object list
 */

static void UnCachePicts(DocObjPtr docObj)
{
	while (docObj) {
		switch (docObj->Type) {
		case TYPE_BMAP:
			BMapDisposeCache((BMapObjPtr) docObj);
			break;
		case TYPE_GROUP:
			UnCachePicts(((GroupObjPtr) docObj)->Objects);
			break;
		}
		docObj = NextObj(docObj);
	}
}

/*
 *	Uncache all pictures in document
 */

void UnCacheAllPicts(DocDataPtr docData)
{
	DocLayerPtr layer;

	for (layer = BottomLayer(docData); layer; layer = NextLayer(layer))
		UnCachePicts(layer->Objects);
}
