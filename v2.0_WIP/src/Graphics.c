/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Graphics support routines
 */

#include <exec/types.h>
#include <graphics/gfxbase.h>
#include <graphics/gfxmacros.h>
#include <graphics/scale.h>
#include <intuition/intuition.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/layers.h>

#include <Toolbox/Memory.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Window.h>
#include <Toolbox/Screen.h>
#include <Toolbox/Utility.h>
#include <Toolbox/FixMath.h>

#include "Draw.h"
#include "Proto.h"

/*
 *	External variables
 */

extern struct GfxBase	*GfxBase;

extern ScreenPtr	screen;
extern Palette		screenPalette;

extern WindowPtr	windowList[], backWindow;
extern WORD			numWindows;

extern Options	options;

/*
 *	Local variables and definitions
 */

#define MAX_BLIT_SIZE	1008			/* General use, OS 1.3 and prior */
#define MAX_BLIT_XSIZE	976				/* BltTemplate special case */
#define MAX_BLIT_YSIZE	1023

#define RGB_CYAN(rgb)		(0x0F - RED(rgb))
#define RGB_MAGENTA(rgb)	(0x0F - GREEN(rgb))
#define RGB_YELLOW(rgb)		(0x0F - BLUE(rgb))

#define MIN3(a,b,c)		(((a)<(b))?(((a)<(c))?(a):(c)):(((b)<(c))?(b):(c)))

/*
 *	Local prototypes
 */

static PenNum	RGBPenNumber(RastPtr, RGBColor);
static UWORD	*GetDisplayPat(RastPtr, RGBPat8Ptr);
static void		DisposeScreenPat(RastPtr, UWORD *);

static void	ScaleBitPlane(PLANEPTR, PLANEPTR, WORD, WORD, WORD, WORD, WORD, WORD);

/*
 *	Return TRUE if advanced graphics (big blits) are available
 */

BOOL AdvancedGraphics()
{
	return ((BOOL) (LibraryVersion((struct Library *) GfxBase) >= OSVERSION_2_0 &&
					(GfxBase->ChipRevBits0 & GFXF_BIG_BLITS) != 0));
}

/*
 *	Do subtractive mix of two colors, returning result
 */

RGBColor MixColors2(register RGBColor color1, register RGBColor color2)
{
	register WORD	red, green, blue;

	red		= (RED(color1)  *RED(color2)   + 7)/15;
	green	= (GREEN(color1)*GREEN(color2) + 7)/15;
	blue	= (BLUE(color1) *BLUE(color2)  + 7)/15;
	return (RGBCOLOR(red, green, blue));
}

/*
 *	Set rPort palette
 */

void SetPalette(RastPtr rPort, PalettePtr palette)
{
	rPort->RP_User = (APTR *) palette;
}

/*
 *	Get rPort palette
 */

PalettePtr GetPalette(RastPtr rPort)
{
	return ((PalettePtr) rPort->RP_User);
}

/*
 *	Turn print->screen color mapping on or off
 */

void ColorCorrectEnable(PalettePtr palette, BOOL enable)
{
	palette->ColorCorrect = enable;
}

/*
 *	Do color correction on given color
 */

RGBColor ColorCorrect(register RGBColor color)
{
	register LONG	r, g, b, c, m, y, k;

	switch (color) {
	case RGBCOLOR_BLACK:
	case RGBCOLOR_WHITE:
		break;						// No change for black & white
	case RGBCOLOR_CYAN:
		color = options.PrtCyan;
		break;
	case RGBCOLOR_MAGENTA:
		color = options.PrtMagenta;
		break;
	case RGBCOLOR_YELLOW:
		color = options.PrtYellow;
		break;
	case RGBCOLOR_RED:
		color = MixColors2(options.PrtMagenta, options.PrtYellow);
		break;
	case RGBCOLOR_GREEN:
		color = MixColors2(options.PrtCyan, options.PrtYellow);
		break;
	case RGBCOLOR_BLUE:
		color = MixColors2(options.PrtCyan, options.PrtMagenta);
		break;
	default:
		c = RGB_CYAN(color);
		m = RGB_MAGENTA(color);
		y = RGB_YELLOW(color);
		k = MIN3(c, m, y);
		c -= k;
		m -= k;
		y -= k;
		r = ( (15 - (c*RGB_CYAN(options.PrtCyan)    + 7)/15)
			 *(15 - (m*RGB_CYAN(options.PrtMagenta) + 7)/15)
			 *(15 - (y*RGB_CYAN(options.PrtYellow)  + 7)/15)
			 *(15 - k)
			 + (15*15*15L)/2)/(15*15*15L);
		g = ( (15 - (c*RGB_MAGENTA(options.PrtCyan)    + 7)/15)
			 *(15 - (m*RGB_MAGENTA(options.PrtMagenta) + 7)/15)
			 *(15 - (y*RGB_MAGENTA(options.PrtYellow)  + 7)/15)
			 *(15 - k)
			 + (15*15*15L)/2)/(15*15*15L);
		b = ( (15 - (c*RGB_YELLOW(options.PrtCyan)    + 7)/15)
			 *(15 - (m*RGB_YELLOW(options.PrtMagenta) + 7)/15)
			 *(15 - (y*RGB_YELLOW(options.PrtYellow)  + 7)/15)
			 *(15 - k)
			 + (15*15*15L)/2)/(15*15*15L);
		color = RGBCOLOR(r, g, b);
		break;
	}
	return (color);
}

/*
 *	Call-back color correction hook for GetColor() when setting pen colors
 */

RGBColor ColorCorrectHook(RGBColor color)
{
	if (options.ColorCorrect)
		color = ColorCorrect(color);
	return (color);
}

/*
 *	Create inverse color lookup table
 *	Supports up to 256 colors in colorMap
 */

void MakeInvColorTable(register PalettePtr palette)
{
	register WORD	pen, red, green, blue, redDiff, greenDiff, blueDiff;
	WORD			diff, newDiff;
	RGBColor		color;
	UBYTE			redTable[256], greenTable[256], blueTable[256];	// Faster with UBYTEs

/*
	Pre-build rgb tables for pens (for speed)
*/
	for (pen = 0; pen < palette->NumPens; pen++) {
		color = palette->ColorTable[pen];
		redTable[pen] = RED(color);
		greenTable[pen] = GREEN(color);
		blueTable[pen] = BLUE(color);
	}
/*
	Set inverse color table entries to closest matching color
*/
	for (color = 0; color < 0x1000; color++) {
		red   = RED(color);
		green = GREEN(color);
		blue  = BLUE(color);
		diff = 0x7FFF;
		palette->InvColorTable[color] = 0;
		for (pen = 0; pen < palette->NumPens; pen++) {
			if ((redDiff = red - (WORD) redTable[pen]) < 0)
				redDiff = -redDiff;
			if ((greenDiff = green - (WORD) greenTable[pen]) < 0)
				greenDiff = -greenDiff;
			if ((blueDiff = blue - (WORD) blueTable[pen]) < 0)
				blueDiff = -blueDiff;
			if ((newDiff = redDiff + greenDiff + blueDiff) < diff) {
				palette->InvColorTable[color] = pen;
				diff = newDiff;
				if (diff == 0)
					break;
			}
		}
	}
}

/*
 *	Get screen pen number for RGBColor
 *	Uses color table attached to rPort RP_User field
 *	If no color table, returns 0 for RGBCOLOR_WHITE, 1 for all other colors
 */

static PenNum RGBPenNumber(RastPtr rPort, RGBColor color)
{
	register PalettePtr	palette = (PalettePtr) rPort->RP_User;

	if (palette == NULL)
		return ((PenNum) ((color == RGBCOLOR_WHITE) ? 0 : 1));
/*
	Map screen color and get pen number
*/
	if (palette->ColorCorrect)
		color = ColorCorrect(color);
	return (palette->InvColorTable[color]);
}

/*
 *	Convert RGB pattern to Amiga display pattern
 *	Patterns are 8 by 8 pixels in size
 */

static UWORD *GetDisplayPat(RastPtr rPort, RGBPat8Ptr pat)
{
	register UBYTE	*scrnPat;
	register WORD	i, depth;
	register PenNum	pen;
	WORD			x, y, srcIndx, dstIndx;
	WORD			planes[12];		// Up to 12 bit-planes

	depth = rPort->BitMap->Depth;
	if ((scrnPat = MemAlloc(depth*8*2, 0)) == NULL)	/* Amiga patterns are 16 bits wide */
		return (NULL);
	srcIndx = dstIndx = 0;
	for (y = 0; y < 8; y++) {
		for (x = 0; x < 8; x++) {
			pen = RGBPenNumber(rPort, (*pat)[srcIndx++]);
			for (i = 0; i < depth; i++) {
				planes[i] = (planes[i] << 1) | (pen & 1);
				pen >>= 1;
			}
		}
		for (i = 0; i < depth; i++)
			scrnPat[dstIndx + i*16] = scrnPat[dstIndx + i*16 + 1] = planes[i];
		dstIndx += 2;
	}
	return ((UWORD *) scrnPat);
}

/*
 *	Dispose of pattern created by GetDisplayPat()
 */

static void DisposeScreenPat(RastPtr rPort, UWORD *scrnPat)
{
	MemFree(scrnPat, rPort->BitMap->Depth*8*2);
}

/*
 *	Init RastPort pen to black solid line
 */

void PenNormal(RastPtr rPort)
{
	RGBForeColor(rPort, RGBCOLOR_BLACK);
	SetDrMd(rPort, JAM1);
	SetDrPt(rPort, 0xFFFF);
}

/*
 *	Set foreground drawing color from RGB value
 */

void RGBForeColor(RastPtr rPort, RGBColor color)
{
	PenNum	penNum;

	if ((penNum = RGBPenNumber(rPort, color)) != rPort->FgPen)
		SetAPen(rPort, penNum);
}

/*
 *	Set background drawing color from RGB value
 */

void RGBBackColor(RastPtr rPort, RGBColor color)
{
	PenNum	penNum;

	if ((penNum = RGBPenNumber(rPort, color)) != rPort->BgPen)
		SetBPen(rPort, penNum);
}

/*
 *	Set draw mode to complement
 */

void SetDrawComplement(RastPtr rPort)
{
	SetDrMd(rPort, COMPLEMENT);
	SetWrMsk(rPort, RGBPenNumber(rPort, RGBCOLOR_BLACK) ^ RGBPenNumber(rPort, RGBCOLOR_WHITE));
}

/*
 *	Clear from drawing in complement mode
 */

void ClearDrawComplement(RastPtr rPort)
{
	SetWrMsk(rPort, 0xFF);
}

/*
 *	Normalize angle to range 0..360
 */

WORD NormalizeAngle(register WORD angle)
{
	while (angle < 0)
		angle += 360;
	return ((WORD) (angle % 360));
}

/*
 *	Offset point
 */

void OffsetPoint(PointPtr pt, WORD dx, WORD dy)
{
	pt->x += dx;
	pt->y += dy;
}

/*
 *	Rotate point clockwise about given center
 */

void RotatePoint(PointPtr pt, WORD cx, WORD cy, WORD angle)
{
	LONG	x, y;
	Fixed	fixAngle;

/*
	Note: LONG * Fixed -> Fixed
*/
	x = pt->x - cx;
	y = pt->y - cy;
	fixAngle = Long2Fix(angle);
	pt->x = Fix2Long(x*FixCos(fixAngle) - y*FixSin(fixAngle)) + cx;
	pt->y = Fix2Long(x*FixSin(fixAngle) + y*FixCos(fixAngle)) + cy;
}

/*
 *	Flip point about given center
 */

void FlipPoint(PointPtr pt, WORD cx, WORD cy, BOOL horiz)
{
	if (horiz)
		pt->x = cx - (pt->x - cx);
	else
		pt->y = cy - (pt->y - cy);
}

/*
 *	Return (approximate) distance between two points
 */

WORD LineLength(PointPtr pt1, PointPtr pt2)
{
	LONG	dx, dy, max, min;				// Force LONG calculations

	dx = ABS(pt2->x - pt1->x);
	dy = ABS(pt2->y - pt1->y);
	max = MAX(dx, dy);
	min = MIN(dx, dy);
	return ((WORD) ((max) ? (max + (min*min)/(max*2)) : 0));
}

/*
 *	Return the distance between a point and the line defined by two points
 */

WORD DistanceFromLine(PointPtr pt, PointPtr pt1, PointPtr pt2)
{
	LONG	r, dist;

	r = LineLength(pt1, pt2);
	if (r == 0)
		return (LineLength(pt, pt1));
	dist = (LONG) pt2->x*pt1->y - (LONG) pt1->x*pt2->y +
		   (LONG) pt1->x* pt->y - (LONG)  pt->x*pt1->y +
		   (LONG)  pt->x*pt2->y - (LONG) pt2->x* pt->y;
	if (dist < 0)
		dist = -dist;
	dist = (dist + r/2)/r;
	return ((WORD) dist);
}

/*
 *	Determine if point is within the given slop of the specified line segment
 */

BOOL PtNearLine(PointPtr pt, PointPtr start, PointPtr end, WORD width, WORD height,
				WORD slop)
{
	LONG	r, dx, dy;			// To force LONG calculations

/*
	First check to see if point is within line segment's boundary
*/
	dx = width/2 + slop;
	dy = height/2 + slop;
	if (pt->x < MIN(start->x, end->x) - dx || pt->x > MAX(start->x, end->x) + dx ||
		pt->y < MIN(start->y, end->y) - dy || pt->y > MAX(start->y, end->y) + dy)
		return (FALSE);
/*
	Now check to see if point is near line
*/
	r = LineLength(start, end);
	if (r > 0) {
		dx = end->x - start->x;
		dy = end->y - start->y;
		if (dx < 0)
			dx = -dx;
		if (dy < 0)
			dy = -dy;
		slop += (dx*height + dy*width + r)/(r*2);
	}
	return ((BOOL) (DistanceFromLine(pt, start, end) <= slop));
}

/*
 *	Return TRUE if two points are equal
 */

BOOL EqualPt(PointPtr pt1, PointPtr pt2)
{
	return ((BOOL) (pt1->x == pt2->x && pt1->y == pt2->y));
}

/*
 *	Rotate rectangle about given center
 *	Return rectangle containing rotated rectangle
 */

void RotateRect(RectPtr rect, WORD cx, WORD cy, WORD angle)
{
	Point	pt1, pt2, pt3, pt4;

	pt1.x = rect->MinX;		pt1.y = rect->MinY;
	pt2.x = rect->MaxX;		pt2.y = rect->MinY;
	pt3.x = rect->MinX;		pt3.y = rect->MaxY;
	pt4.x = rect->MaxX;		pt4.y = rect->MaxY;
	RotatePoint(&pt1, cx, cy, angle);
	RotatePoint(&pt2, cx, cy, angle);
	RotatePoint(&pt3, cx, cy, angle);
	RotatePoint(&pt4, cx, cy, angle);

	rect->MinX = rect->MaxX = pt1.x;
	rect->MinY = rect->MaxY = pt1.y;

	if (pt2.x < rect->MinX)
		rect->MinX = pt2.x;
	if (pt3.x < rect->MinX)
		rect->MinX = pt3.x;
	if (pt4.x < rect->MinX)
		rect->MinX = pt4.x;

	if (pt2.x > rect->MaxX)
		rect->MaxX = pt2.x;
	if (pt3.x > rect->MaxX)
		rect->MaxX = pt3.x;
	if (pt4.x > rect->MaxX)
		rect->MaxX = pt4.x;

	if (pt2.y < rect->MinY)
		rect->MinY = pt2.y;
	if (pt3.y < rect->MinY)
		rect->MinY = pt3.y;
	if (pt4.y < rect->MinY)
		rect->MinY = pt4.y;

	if (pt2.y > rect->MaxY)
		rect->MaxY = pt2.y;
	if (pt3.y > rect->MaxY)
		rect->MaxY = pt3.y;
	if (pt4.y > rect->MaxY)
		rect->MaxY = pt4.y;
}

/*
 *	Flip rectangle horizontally or vertically about given center
 */

void FlipRect(RectPtr rect, WORD cx, WORD cy, BOOL horiz)
{
	WORD	x, y;
	Point	pt;

	pt.x = rect->MinX;
	pt.y = rect->MinY;
	FlipPoint(&pt, cx, cy, horiz);
	rect->MinX = pt.x;
	rect->MinY = pt.y;
	pt.x = rect->MaxX;
	pt.y = rect->MaxY;
	FlipPoint(&pt, cx, cy, horiz);
	rect->MaxX = pt.x;
	rect->MaxY = pt.y;
	if (rect->MinX > rect->MaxX) {
		x = rect->MinX;
		rect->MinX = rect->MaxX;
		rect->MaxX = x;
	}
	if (rect->MinY > rect->MaxY) {
		y = rect->MinY;
		rect->MinY = rect->MaxY;
		rect->MaxY = y;
	}
}

/*
 *	Draw rectangle frame
 */

void FrameRectNew(RastPtr rPort, RectPtr rect, WORD penWidth, WORD penHeight)
{
	WORD		x, y;
	Rectangle	drawRect;

	for (x = 0; x < penWidth; x++) {
		for (y = 0; y < penHeight; y++) {
			if (x != 0 && y != 0 && x != penWidth - 1 && y != penHeight - 1)
				continue;
			drawRect = *rect;
			if (x != 0 || y != 0)
				OffsetRect(&drawRect, x, y);
			FrameRect(rPort, &drawRect);
		}
	}
}

/*
 *	Fill rect with pattern
 */

void FillRectNew(RastPtr rPort, RectPtr rect, RGBPat8Ptr pat)
{
	BYTE	oldBgPen, oldPatSize;
	WORD	xStart, xEnd, yStart, yEnd, maxSize;
	UWORD	*oldPat, *newPat;

	if (EmptyRect(rect))
		return;
/*
	Convert pattern
*/
	oldBgPen = rPort->BgPen;
	oldPat = rPort->AreaPtrn;
	oldPatSize = rPort->AreaPtSz;
	if (pat && (newPat = GetDisplayPat(rPort, pat)) != NULL) {
		SetAfPt(rPort, newPat, -3);
		SetAPen(rPort, 255);
		SetBPen(rPort, 0);
	}
	else {
		newPat = NULL;
		SetAfPt(rPort, NULL, 0);
		if (pat)
			RGBForeColor(rPort, RGBCOLOR_BLACK);
	}
/*
	Do fill
	If rect is larger than system can handle, do it in pieces
*/
	maxSize = (AdvancedGraphics()) ? 0x7FFF : MAX_BLIT_SIZE;
	SetDrMd(rPort, JAM2);
	BNDRYOFF(rPort);
	yStart = rect->MinY;
	do {
		yEnd = rect->MaxY;
		if (yEnd - yStart + 1 > maxSize)
			yEnd = yStart + maxSize - 1;
		xStart = rect->MinX;
		do {
			xEnd = rect->MaxX;
			if (xEnd - xStart + 1 > maxSize)
				xEnd = xStart + maxSize - 1;
			RectFill(rPort, xStart, yStart, xEnd, yEnd);
			xStart = xEnd + 1;
		} while (xStart <= rect->MaxX);
		yStart = yEnd + 1;
	} while (yStart <= rect->MaxY);
	SetBPen(rPort, oldBgPen);
	SetAfPt(rPort, oldPat, oldPatSize);
	WaitBlit();
	if (newPat)
		DisposeScreenPat(rPort, newPat);
}

/*
 *	Draw single pixel wide oval
 *	Note: DrawEllipse has width/height of (2*radius + 1)
 */

static void FrameOval(RastPtr rPort, RectPtr rect)
{
	WORD	cx, cy, rx, ry;

	cx = (rect->MinX + rect->MaxX)/2;
	cy = (rect->MinY + rect->MaxY)/2;
	rx = (rect->MaxX - rect->MinX)/2;
	ry = (rect->MaxY - rect->MinY)/2;
	if (rx == 0 || ry == 0) {
		Move(rPort, cx - rx, cy - ry);
		Draw(rPort, cx + rx, cy + ry);
	}
	else
		DrawEllipse(rPort, cx, cy, rx, ry);
	WaitBlit();
}

/*
 *	Draw oval frame
 */

void FrameOvalNew(RastPtr rPort, RectPtr rect, WORD penWidth, WORD penHeight)
{
	WORD		x, y;
	Rectangle	drawRect;

	for (x = 0; x < penWidth; x++) {
		for (y = 0; y < penHeight; y++) {
			if (x != 0 && y != 0 && x != penWidth - 1 && y != penHeight - 1)
				continue;
			drawRect = *rect;
			if (x != 0 || y != 0)
				OffsetRect(&drawRect, x, y);
			FrameOval(rPort, &drawRect);
		}
	}
}

/*
 *	Fill oval with pattern
 */

void FillOvalNew(RastPtr rPort, RectPtr rect, RGBPat8Ptr pat)
{
	WORD			cx, cy, rx, ry;
	WORD			width, height;
	BYTE			oldBgPen, oldPatSize;
	UWORD			*oldPat, *newPat;
	PLANEPTR		planePtr;
	struct AreaInfo	areaInfo;
	struct TmpRas	tmpRas;
	UWORD			areaBuff[20];		// 40 bytes = 8 vectors (to be sure)

	if (EmptyRect(rect))
		return;
/*
	If oval is wider than system can handle, do filled rect instead
*/
	cx = (rect->MinX + rect->MaxX)/2;
	cy = (rect->MinY + rect->MaxY)/2;
	rx = (rect->MaxX - rect->MinX)/2;
	ry = (rect->MaxY - rect->MinY)/2;
	width = rect->MaxX - rect->MinX + 1 + 16;	/* System needs extra size */
	height = rect->MaxY - rect->MinY + 1;
	if (rx == 0 || ry == 0 ||
		(!AdvancedGraphics() && (width > MAX_BLIT_SIZE || height > MAX_BLIT_SIZE))) {
		FillRectNew(rPort, rect, pat);
		return;
	}
/*
	Convert pattern
*/
	oldBgPen = rPort->BgPen;
	oldPat = rPort->AreaPtrn;
	oldPatSize = rPort->AreaPtSz;
	if (pat && (newPat = GetDisplayPat(rPort, pat)) != NULL) {
		SetAfPt(rPort, newPat, -3);
		SetAPen(rPort, 255);
		SetBPen(rPort, 0);
	}
	else {
		newPat = NULL;
		SetAfPt(rPort, NULL, 0);
		if (pat)
			RGBForeColor(rPort, RGBCOLOR_BLACK);
	}
/*
	Do fill
*/
	SetDrMd(rPort, JAM2);
	BNDRYOFF(rPort);
	if ((planePtr = AllocRaster(width, height)) != NULL) {
		BlockClear(areaBuff, sizeof(areaBuff));
		InitArea(&areaInfo, areaBuff, sizeof(areaBuff)/5);
		InitTmpRas(&tmpRas, planePtr, RASSIZE((LONG) width, (LONG) height));
		rPort->AreaInfo = &areaInfo;
		rPort->TmpRas = &tmpRas;
		if (AreaEllipse(rPort, cx, cy, rx, ry) == 0)
			AreaEnd(rPort);
		WaitBlit();
		FreeRaster(planePtr, width, height);
		rPort->AreaInfo = NULL;
		rPort->TmpRas = NULL;
	}
	SetBPen(rPort, oldBgPen);
	SetAfPt(rPort, oldPat, oldPatSize);
	if (newPat)
		DisposeScreenPat(rPort, newPat);
}

/*
 *	Offset list of points in polygon
 */

void OffsetPoly(WORD numPoints, PointPtr pts, WORD dx, WORD dy)
{
	while (numPoints--) {
		pts[numPoints].x += dx;
		pts[numPoints].y += dy;
	}
}

/*
 *	Draw polygon frame
 */

void FramePolyNew(RastPtr rPort, UWORD numPaths, WORD numPoints[],
							PointPtr pts[], WORD penWidth, WORD penHeight)
{
	WORD	x, y, j;

	for (j = 0; j < numPaths; j++) {
		for (x = 0; x < penWidth; x++) {
			for (y = 0; y < penHeight; y++) {
				if (x != 0 && y != 0 && x != penWidth - 1 && y != penHeight - 1)
					continue;
				if (x != 0 || y != 0)
					OffsetPoly(numPoints[j], pts[j], x, y);
				FramePoly(rPort, numPoints[j], pts[j]);
				if (x != 0 || y != 0)
					OffsetPoly(numPoints[j], pts[j], (WORD) -x, (WORD) -y);
			}
		}
	}
}

/*
 *	Fill polygon with pattern
 */

void FillPolyNew(RastPtr rPort, UWORD numPaths, WORD numPoints[],
						PointPtr pts[], RGBPat8Ptr pat)
{
	WORD			i, j, width, height;
	BYTE			oldBgPen, oldPatSize;
	UWORD			*oldPat, *newPat;
	UWORD			*areaBuff;
	PLANEPTR		planePtr;
	struct AreaInfo	areaInfo;
	struct TmpRas	tmpRas;
	Rectangle		rect;
	PointPtr		tempPts;
	WORD			tempNumPts, totPoints;

	totPoints = 0;
	for (j = 0; j < numPaths; j++)
		totPoints += numPoints[j];
/*
	If poly is wider than system can handle, do filled rect instead
*/
	tempPts = pts[0];
	rect.MinX = rect.MaxX = tempPts[0].x;
	rect.MinY = rect.MaxY = tempPts[0].y;
	for (j = 0; j < numPaths; j++) {
		tempPts = pts[j];
		tempNumPts = numPoints[j];
		for (i = 0; i < tempNumPts; i++) {
			if (tempPts[i].x < rect.MinX)
				rect.MinX = tempPts[i].x;
			if (tempPts[i].x > rect.MaxX)
				rect.MaxX = tempPts[i].x;
			if (tempPts[i].y < rect.MinY)
				rect.MinY = tempPts[i].y;
			if (tempPts[i].y > rect.MaxY)
				rect.MaxY = tempPts[i].y;
		}
	}
	width = rect.MaxX - rect.MinX + 1 + 16;		/* System needs extra size */
	height = rect.MaxY - rect.MinY + 1;
	if (!AdvancedGraphics() && (width > MAX_BLIT_SIZE || height > MAX_BLIT_SIZE)) {
		FillRectNew(rPort, &rect, pat);
		return;
	}
/*
	Convert pattern
*/
	oldBgPen = rPort->BgPen;
	oldPat = rPort->AreaPtrn;
	oldPatSize = rPort->AreaPtSz;
	if (pat && (newPat = GetDisplayPat(rPort, pat)) != NULL) {
		SetAfPt(rPort, newPat, -3);
		SetAPen(rPort, 255);
		SetBPen(rPort, 0);
	}
	else {
		newPat = NULL;
		SetAfPt(rPort, NULL, 0);
		if (pat)
			RGBForeColor(rPort, RGBCOLOR_BLACK);
	}
/*
	Do fill
*/
	SetDrMd(rPort, JAM2);
	BNDRYOFF(rPort);
	if ((areaBuff = MemAlloc((totPoints + numPaths)*5, MEMF_CLEAR)) != NULL) {
		if ((planePtr = AllocRaster(width, height)) != NULL) {
			InitArea(&areaInfo, areaBuff, totPoints + numPaths);
			InitTmpRas(&tmpRas, planePtr, RASSIZE((LONG) width, (LONG) height));
			rPort->AreaInfo = &areaInfo;
			rPort->TmpRas = &tmpRas;
			for (j=0; j < numPaths; j++) {
				tempPts = pts[j];
				tempNumPts = numPoints[j];
				AreaMove(rPort, tempPts[0].x, tempPts[0].y);
				for (i = 1; i < tempNumPts; i++)
					AreaDraw(rPort, tempPts[i].x, tempPts[i].y);
			}
			AreaEnd(rPort);
			WaitBlit();
			FreeRaster(planePtr, width, height);
			rPort->AreaInfo = NULL;
			rPort->TmpRas = NULL;
		}
		MemFree(areaBuff, (totPoints + numPaths)*5);
	}
	SetBPen(rPort, oldBgPen);
	SetAfPt(rPort, oldPat, oldPatSize);
	if (newPat)
		DisposeScreenPat(rPort, newPat);
}

/*
 *	Do BltTemplate() in pieces when too large for blitter
 */

void ExtBltTemplate(PLANEPTR source, WORD srcX, WORD srcMod, RastPtr destRPort,
					WORD destX, WORD destY, WORD sizeX, WORD sizeY)
{
	WORD	x, y, width, height;
	WORD	destWidth, destHeight;
	LONG	offset;

	if (AdvancedGraphics())
		BltTemplate(source, srcX, srcMod, destRPort, destX, destY, sizeX, sizeY);
	else {
		destWidth = destRPort->BitMap->BytesPerRow << 3;
		destHeight = destRPort->BitMap->Rows;
		y = destY;
		do {
			height = sizeY - (y - destY);
			if (height > MAX_BLIT_YSIZE)
				height = MAX_BLIT_YSIZE;
			x = destX;
			do {
				width = sizeX - (x - destX);
				if (width > MAX_BLIT_XSIZE)
					width = MAX_BLIT_XSIZE;
				if (x < destWidth && y < destHeight &&
					x + width - 1 >= 0 && y + height - 1 >= 0) {
					offset = srcX + (x - destX) + ((LONG) (y - destY)*srcMod << 3);
					BltTemplate(source + ((offset & 0xFFFFFFF0L) >> 3),
								offset & 0x0F, srcMod, destRPort,
								x, y, width, height);
				}
				x += width;
			} while (x < destX + sizeX);
			y += height;
		} while (y < destY + sizeY);
	}
	WaitBlit();
}

/*
 *	Scale point by width/height of srcRect to dstRect
 *	Used to scale pen sizes
 *	Will not return a value less than (1,1)
 */

void ScalePt(register PointPtr pt, register RectPtr srcRect, register RectPtr dstRect)
{
	register LONG	oldDX, oldDY, newDX, newDY;	// To force LONG calculations

	oldDX = srcRect->MaxX - srcRect->MinX;
	oldDY = srcRect->MaxY - srcRect->MinY;
	newDX = dstRect->MaxX - dstRect->MinX;
	newDY = dstRect->MaxY - dstRect->MinY;
	if (oldDX != newDX)
		pt->x = (oldDX == 0) ? 1 : (LONG) pt->x*newDX/oldDX;
	if (oldDY != newDY)
		pt->y = (oldDY == 0) ? 1 : (LONG) pt->y*newDY/oldDY;
	if (pt->x < 1)
		pt->x = 1;
	if (pt->y < 1)
		pt->y = 1;
}

/*
 *	Map a point in srcRect to dstRect
 *	Maps 0 to 0 and Max to Max
 */

void MapPt(register PointPtr pt, register RectPtr srcRect, register RectPtr dstRect)
{
	register LONG	x, y, oldDX, oldDY, newDX, newDY;	// To force LONG calculations

	x = pt->x - srcRect->MinX;
	y = pt->y - srcRect->MinY;
	oldDX = srcRect->MaxX - srcRect->MinX;
	oldDY = srcRect->MaxY - srcRect->MinY;
	newDX = dstRect->MaxX - dstRect->MinX;
	newDY = dstRect->MaxY - dstRect->MinY;
	if (oldDX != newDX)
		x = (oldDX == 0) ? 0 : x*newDX/oldDX;
	if (oldDY != newDY)
		y = (oldDY == 0) ? 0 : y*newDY/oldDY;
	pt->x = x + dstRect->MinX;
	pt->y = y + dstRect->MinY;
}

/*
 *	Map rectangle from srcRect to dstRect
 *	(0 maps to 0 and Max maps to Max)
 */

void MapRect(RectPtr rect, RectPtr srcRect, RectPtr dstRect)
{
	Point	pt;

	pt.x = rect->MinX;
	pt.y = rect->MinY;
	MapPt(&pt, srcRect, dstRect);
	rect->MinX = pt.x;
	rect->MinY = pt.y;
	pt.x = rect->MaxX;
	pt.y = rect->MaxY;
	MapPt(&pt, srcRect, dstRect);
	rect->MaxX = pt.x;
	rect->MaxY = pt.y;
}

/*
 *	Scale a single plane of data from one rect to another
 */

static void ScaleBitPlane(PLANEPTR src, PLANEPTR dst,
						  WORD srcWidth, WORD srcHeight,
						  WORD dstWidth, WORD dstHeight,
						  WORD srcRowBytes, WORD dstRowBytes)
{
	register PLANEPTR	srcPlane, dstPlane;
	register UWORD		srcByte, dstByte, srcShift, dstShift;
	register WORD		dstX, xAccum;
	WORD				dstY, yAccum;

	yAccum = -dstHeight;
	for (dstY = 0; dstY < dstHeight; dstY++) {
		while (yAccum >= 0) {
			src += srcRowBytes;
			yAccum -= dstHeight;
		}
		yAccum += srcHeight;
		srcPlane = src;
		dstPlane = dst;
		srcShift = dstShift = 0;
		srcByte = *srcPlane;
		dstByte = 0;
		xAccum = -dstWidth;
		for (dstX = 0; dstX < dstWidth; dstX++) {
			while (xAccum >= 0) {
				srcByte <<= 1;
				if (srcShift++ == 7) {
					srcByte = *(++srcPlane);
					srcShift = 0;
				}
				xAccum -= dstWidth;
			}
			xAccum += srcWidth;
			dstByte = (dstByte | (srcByte & 0x80)) << 1;
			if (dstShift++ == 7) {
				*dstPlane++ = dstByte >> 8;
				dstShift = dstByte = 0;
			}
		}
		if (dstShift)
			*dstPlane = dstByte >> dstShift;
		dst += dstRowBytes;
	}
}

/*
 *	Scale bitmap from old size to new size
 *	Bitmap planes must be in graphics memory
 *	Assumes bit planes are initially cleared
 */

void ScaleBitMap(RastPtr srcPort, RastPtr dstPort, RectPtr srcRect, RectPtr dstRect)
{
	WORD				i, srcWidth, srcHeight, dstWidth, dstHeight;
	BitMapPtr			srcBitMap, dstBitMap;
	struct BitScaleArgs	bitScale;

	srcWidth  = srcRect->MaxX - srcRect->MinX + 1;
	srcHeight = srcRect->MaxY - srcRect->MinY + 1;
	dstWidth  = dstRect->MaxX - dstRect->MinX + 1;
	dstHeight = dstRect->MaxY - dstRect->MinY + 1;
/*
	Use graphics library function if available
	(Includes kludge for bug in KS 2.0)
*/
	if (LibraryVersion((struct Library *) GfxBase) >= OSVERSION_2_0 &&
		srcWidth < MAX_BLIT_SIZE && dstWidth < MAX_BLIT_SIZE) {
		bitScale.bsa_SrcX		= srcRect->MinX;
		bitScale.bsa_SrcY		= srcRect->MinY;
		bitScale.bsa_SrcWidth	= bitScale.bsa_XSrcFactor = srcWidth;
		bitScale.bsa_SrcHeight	= bitScale.bsa_YSrcFactor = srcHeight;
		bitScale.bsa_DestX		= dstRect->MinX;
		bitScale.bsa_DestY		= dstRect->MinY;
		bitScale.bsa_XDestFactor	= dstWidth;
		bitScale.bsa_YDestFactor	= dstHeight;
		bitScale.bsa_SrcBitMap	= srcPort->BitMap;
		bitScale.bsa_DestBitMap	= dstPort->BitMap;
		bitScale.bsa_Flags		= 0;
		BitMapScale(&bitScale);
		WaitBlit();
	}
/*
	Else do it ourselves
*/
	else {
		WaitBlit();
		srcBitMap = srcPort->BitMap;
		dstBitMap = dstPort->BitMap;
		for (i = 0; i < srcBitMap->Depth; i++)
			ScaleBitPlane(srcBitMap->Planes[i], dstBitMap->Planes[i],
						  srcWidth, srcHeight, dstWidth, dstHeight,
						  srcBitMap->BytesPerRow, dstBitMap->BytesPerRow);
	}
}

/*
 *	Transform bitmap from one rect to another
 *	Assumes dstPort planes are cleared to 0
 */

void TransformBitMap(RastPtr srcPort, RastPtr dstPort, RectPtr srcRect, RectPtr dstRect,
					 WORD angle, BOOL flipHoriz, BOOL flipVert)
{
	WORD		pen;
	LONG		srcWidth, srcHeight, dstWidth, dstHeight;	// To force LONG calculations
	Point		srcPt, dstPt;
	Rectangle	rotRect;

	if (angle == 0 && !flipHoriz && !flipVert) {
		ScaleBitMap(srcPort, dstPort, srcRect, dstRect);
		return;
	}
	srcWidth  = srcRect->MaxX - srcRect->MinX + 1;
	srcHeight = srcRect->MaxY - srcRect->MinY + 1;
	rotRect = *dstRect;
	if (angle)
		RotateRect(&rotRect, 0, 0, -angle);
	dstWidth  = rotRect.MaxX - rotRect.MinX + 1;
	dstHeight = rotRect.MaxY - rotRect.MinY + 1;
	SetDrMd(dstPort, JAM1);
	for (dstPt.y = dstRect->MinY; dstPt.y <= dstRect->MaxY; dstPt.y++) {
		for (dstPt.x = dstRect->MinX; dstPt.x <= dstRect->MaxX; dstPt.x++) {
			srcPt.x = dstPt.x - dstRect->MinX;
			srcPt.y = dstPt.y - dstRect->MinY;
			if (angle) {
				RotatePoint(&srcPt, 0, 0, -angle);
				srcPt.x -= rotRect.MinX;
				srcPt.y -= rotRect.MinY;
			}
			if (dstWidth != srcWidth)
				srcPt.x = ((LONG) srcPt.x*srcWidth)/dstWidth;
			if (dstHeight != srcHeight)
				srcPt.y = ((LONG) srcPt.y*srcHeight)/dstHeight;
			srcPt.x = (flipHoriz) ? srcRect->MaxX - srcPt.x : srcRect->MinX + srcPt.x;
			srcPt.y = (flipVert)  ? srcRect->MaxY - srcPt.y : srcRect->MinY + srcPt.y;
			if (PtInRect(&srcPt, srcRect)) {
				pen = ReadPixel(srcPort, srcPt.x, srcPt.y);
				if (pen) {
					SetAPen(dstPort, pen);
					WritePixel(dstPort, dstPt.x, dstPt.y);
				}
			}
		}
	}
	WaitBlit();
}

/*
 *	Clear RastPort to white
 *	Needed since SetRast() doesn't work with RastPorts wider than MAX_BLIT_SIZE pixels
 *	May not respect clipping regions in rPort
 */

void ClearRast(RastPtr rPort)
{
	register UBYTE	value;
	register WORD	i, depth;
	register LONG	j, size;
	BitMapPtr		bitMap = rPort->BitMap;

	RGBForeColor(rPort, RGBCOLOR_WHITE);
	if (AdvancedGraphics() ||
		(bitMap->BytesPerRow <= MAX_BLIT_SIZE/8 && bitMap->Rows <= MAX_BLIT_SIZE)) {
		SetRast(rPort, rPort->FgPen);
		WaitBlit();
	}
	else {
		WaitBlit();
		size = (LONG) bitMap->BytesPerRow*bitMap->Rows;
		depth = bitMap->Depth;
		for (i = 0; i < depth; i++) {
			value = ((rPort->FgPen >> i) & 1) ? 0xFF : 0;
			for (j = 0; j < size; j++)
				bitMap->Planes[i][j] = value;
		}
	}
}

/*
 *	Check current screen color table and adapt to any changes
 *	Assumes screen color depth does not change
 */

void CheckColorTable()
{
	WORD		i;
	BOOL		changed;
	Rectangle	rect;
	RGBColor	colorTable[256];

	if (screen->Flags & BEEPING)
		return;
/*
	Check to see if color table has changed, and copy new color table
*/
	GetColorTable(screen, &colorTable);
	changed = FALSE;
	for (i = 0; i < screenPalette.NumPens; i++) {
		if (colorTable[i] != screenPalette.ColorTable[i]) {
			screenPalette.ColorTable[i] = colorTable[i];	/* Save new value */
			changed = TRUE;
		}
	}
/*
	If changed, generate new inverse look-up table, and redraw all windows
*/
	if (changed) {
		InitToolbox(screen);
		MakeInvColorTable(&screenPalette);
		for (i = 0; i < numWindows; i++) {
			GetWindowRect(windowList[i], &rect);
			InvalRect(windowList[i], &rect);
			UnCacheAllPicts(GetWRefCon(windowList[i]));
		}
		SetRect(&rect, 0, 0, backWindow->Width - 1, backWindow->Height - 1);
		ClearBackWindow(backWindow->RPort, &rect);
		DrawToolWindow();
		DrawPenWindow();
		DrawFillWindow();
	}
}
