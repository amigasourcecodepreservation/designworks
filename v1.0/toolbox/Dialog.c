/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Toolbox library
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Dialog routines
 */

#define INTUI_V36_NAMES_ONLY	1

#include <exec/types.h>
#include <intuition/intuition.h>
#include <intuition/intuitionbase.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include "TypeDefs.h"
#include "Request.h"
#include "Window.h"
#include "Dialog.h"
#include "Utility.h"
#include "Memory.h"

/*
 *	External variables
 */

extern struct IntuitionBase	*IntuitionBase;

extern TextAttrPtr	_tbTextAttr;

extern WORD	_tbBlackColor, _tbWhiteColor;
extern WORD	_tbDarkColor, _tbLightColor;

extern BOOL	_tbAutoActivate;

/*
 *	Local variables and definitions
 */

#define TAB	0x09
#define CR	0x0D
#define ESC	0x1B

static WORD	defaultOKBtn = OK_BUTTON;
static WORD	defaultCancelBtn = CANCEL_BUTTON;

/*
 *	Local routine prototypes
 */

WORD	HandleKey(WindowPtr, TextChar);

/*
 *	Handle key down intuition messages in dialog and requester
 *	If RETURN or ENTER, then return defaultOKBtn
 *	If ESC then return defaultCancelBtn
 *	If match to first letter of button then return button number
 *	Otherwise returns -1
 */

static WORD HandleKey(WindowPtr window, register TextChar ch)
{
	WORD gadgType, item;
	TextPtr text;
	RequestPtr request;
	register GadgetPtr gadget, gadgList;

	if ((request = window->FirstRequest) == NULL)
		gadgList = window->FirstGadget;
	else
		gadgList = request->ReqGadget;
	if (gadgList == NULL)
		return (-1);
/*
	If RETURN/ENTER key then simulate OK button
	If ESC key then simulate Cancel button
*/
	if (ch == CR || ch == ESC) {
		item = (ch == CR) ? defaultOKBtn : defaultCancelBtn;
		gadget = GadgetItem(gadgList, item);
		if (gadget && (gadget->Flags & GFLG_DISABLED) == 0 &&
			GadgetType(gadget) == GADG_PUSH_BUTTON) {
			HiliteGadget(gadget, window, request, TRUE);
			Delay(5);
			HiliteGadget(gadget, window, request, FALSE);
			if (gadget->Activation & GACT_ENDGADGET)
				EndRequest(request, window);
			return (item);
		}
	}
/*
	Otherwise, search for matching button name
*/
	else {
		item = -1;
		for (gadget = gadgList; gadget; gadget = gadget->NextGadget) {
			gadgType = GadgetType(gadget);
			if (gadgType == GADG_PUSH_BUTTON || gadgType == GADG_RADIO_BUTTON ||
				gadgType == GADG_CHECK_BOX) {
				text = gadget->GadgetText->IText;
				if (*text == ch) {
					item = GadgetNumber(gadget);
					break;
				}
				else if (toUpper[*text] == toUpper[ch] && item == -1)
					item = GadgetNumber(gadget);	/* Keep looking for exact match */
			}
		}
		if (item != -1) {
			gadget = GadgetItem(gadgList, item);
			if ((gadget->Flags & GFLG_DISABLED) == 0) {
				HiliteGadget(gadget, window, request, TRUE);
				Delay(5);
				HiliteGadget(gadget, window, request, FALSE);
				if (gadget->Activation & GACT_ENDGADGET)
					EndRequest(request, window);
				return (item);
			}
		}
	}
	return (-1);
}

/*
 *	Set default OK and Cancel buttons
 *	If -1 then don't change
 */

void SetDefaultButtons(WORD okBtn, WORD cancelBtn)
{
	if (okBtn != -1)
		defaultOKBtn = okBtn;
	if (cancelBtn != -1)
		defaultCancelBtn = cancelBtn;
}

/*
 *	GetDialog
 *	Process dialog template, and return pointer to Dialog structure
 *	Return NULL if error
 *	If screen is NULL then open on Workbench screen
 */

DialogPtr GetDialog(DlgTemplPtr dlgTemplate, ScreenPtr screen, MsgPortPtr msgPort)
{
	WORD leftEdge, topEdge, width, height;
	WORD xSize, ySize;
	ULONG IDCMPFlags, intuiLock;
	DialogPtr dlg;
	BorderPtr border;
	IntuiTextPtr intuiText;
	GadgetPtr gadget, gadgList;
	ScreenPtr firstScreen;
	Rectangle rect;
	Screen wbScreen;
	struct NewWindow newWind;

/*
	If no screen, then get workbench screen data
*/
	if (screen == NULL) {
		if (!GetScreenData((BYTE *) &wbScreen, sizeof(Screen), WBENCHSCREEN, NULL))
			return (NULL);
		screen = &wbScreen;
	}
/*
	Get font dimensions
	(Use width of "e" rather than font's XSize,
		since this works better for proportional fonts)
*/
	intuiText = NewIntuiText(0, 0, "e", FS_NORMAL, 0);
	if (intuiText == NULL)
		xSize = 8;
	else {
		xSize = IntuiTextLength(intuiText);
		FreeIntuiText(intuiText);
	}
	ySize = _tbTextAttr->ta_YSize;
/*
	Initialize NewWindow struct
*/
	BlockClear(&newWind, sizeof(struct NewWindow));
	newWind.DetailPen = newWind.BlockPen = -1;
	width  = (dlgTemplate->Width*xSize)/8;
	height = (dlgTemplate->Height*ySize)/11;
	switch (dlgTemplate->Type) {
	case DLG_TYPE_ALERT:
		newWind.IDCMPFlags = IDCMP_MOUSEBUTTONS | IDCMP_GADGETDOWN | IDCMP_GADGETUP
							 | IDCMP_RAWKEY | IDCMP_ACTIVEWINDOW | IDCMP_INACTIVEWINDOW
							 | IDCMP_INTUITICKS;
		newWind.Flags = WFLG_SMART_REFRESH | WFLG_BORDERLESS | WFLG_ACTIVATE
						| WFLG_NOCAREREFRESH;
		break;
	case DLG_TYPE_WINDOW:
		width  += screen->WBorLeft + screen->WBorRight;
		height += screen->WBorTop + screen->WBorBottom + screen->Font->ta_YSize + 1;
		newWind.IDCMPFlags = IDCMP_MOUSEBUTTONS | IDCMP_GADGETDOWN | IDCMP_GADGETUP
							 | IDCMP_RAWKEY | IDCMP_ACTIVEWINDOW | IDCMP_INACTIVEWINDOW
							 | IDCMP_INTUITICKS;
		newWind.Flags = WFLG_DRAGBAR | WFLG_SMART_REFRESH | WFLG_ACTIVATE;
		if (dlgTemplate->Flags & DLG_FLAG_CLOSE) {
			newWind.IDCMPFlags |= IDCMP_CLOSEWINDOW;
			newWind.Flags |= WFLG_CLOSEGADGET;
		}
		if (dlgTemplate->Flags & DLG_FLAG_DEPTH)
			newWind.Flags |= WFLG_DEPTHGADGET;
		if (dlgTemplate->Flags & DLG_FLAG_RESIZE) {
			newWind.IDCMPFlags |= IDCMP_NEWSIZE;
			newWind.Flags |= WFLG_SIZEGADGET | WFLG_SIZEBRIGHT | WFLG_SIZEBBOTTOM;
		}
		else
			newWind.Flags |= WFLG_NOCAREREFRESH;
		break;
	default:
		return (NULL);
	}
	if ((leftEdge = dlgTemplate->LeftEdge) == -1)
		leftEdge = (screen->Width - width)/2;
	else
		leftEdge = (leftEdge*xSize)/8;
	if ((topEdge = dlgTemplate->TopEdge) == -1)
		topEdge = (screen->Height - height)/2;
	else
		topEdge = (topEdge*ySize)/11;
	if (leftEdge < 0)
		leftEdge = 0;
	if (topEdge < 0)
		topEdge = 0;
	if (leftEdge + width > screen->Width)
		width = screen->Width - leftEdge;
	if (topEdge + height > screen->Height)
		height = screen->Height - topEdge;
	newWind.Title		= dlgTemplate->Title;
	newWind.LeftEdge	= leftEdge;
	newWind.TopEdge		= topEdge;
	newWind.Width		= newWind.MinWidth	= newWind.MaxWidth	= width;
	newWind.Height		= newWind.MinHeight	= newWind.MaxHeight	= height;
	if (dlgTemplate->Flags & DLG_FLAG_RESIZE)
		newWind.MaxWidth	= newWind.MaxHeight = 0x7FFF;
	else {
		newWind.MaxWidth	= width;
		newWind.MaxHeight	= height;
	}
	if (screen != &wbScreen) {
		newWind.Screen	= screen;
		newWind.Type	= CUSTOMSCREEN;
	}
	else {
		newWind.Screen	= NULL;
		newWind.Type	= WBENCHSCREEN;
	}
/*
	Create gadgets (but don't attach to window until it is open and cleared)
*/
	if (dlgTemplate->Gadgets == NULL)
		return (NULL);
	gadgList = GetGadgets(dlgTemplate->Gadgets);
	if (gadgList == NULL)
		return (NULL);
	if (dlgTemplate->Type == DLG_TYPE_WINDOW) {
		for (gadget = gadgList; gadget; gadget = gadget->NextGadget) {
			if ((gadget->Flags & GFLG_RELRIGHT) == 0)
				gadget->LeftEdge += screen->WBorLeft;
			if ((gadget->Flags & GFLG_RELBOTTOM) == 0)
				gadget->TopEdge  += screen->WBorTop + screen->Font->ta_YSize + 1;
		}
	}
/*
	Open dialog window
*/
	IDCMPFlags = newWind.IDCMPFlags;
	newWind.IDCMPFlags = 0;
	dlg = OpenWindow(&newWind);
	newWind.IDCMPFlags = IDCMPFlags;
	if (dlg == NULL) {
		DisposeGadgets(gadgList);
		return (NULL);
	}
	dlg->UserPort = msgPort;
	ModifyIDCMP(dlg, IDCMPFlags);
	SetStdPointer(dlg, POINTER_ARROW);
	intuiLock = LockIBase(0);
	firstScreen = IntuitionBase->FirstScreen;
	UnlockIBase(intuiLock);
	if (dlg->WScreen != firstScreen)
		ScreenToFront(dlg->WScreen);
/*
	Clear dialog window
*/
	GetWindowRect(dlg, &rect);
	SetAPen(dlg->RPort, _tbLightColor);
	SetDrMd(dlg->RPort, JAM1);
	RectFill(dlg->RPort, rect.MinX, rect.MinY, rect.MaxX, rect.MaxY);
/*
	Draw alert dialog border
*/
	if (dlgTemplate->Type == DLG_TYPE_ALERT) {
#ifdef NEW_LOOK
		if ((border = ShadowBoxBorder(width, height, 1, TRUE)) != NULL) {
#else
		if ((border = ShadowBoxBorder(width, height, 0, TRUE)) != NULL) {
#endif
			DrawBorder(dlg->RPort, border, 0, 0);
			FreeBorder(border);
		}
	}
/*
	Attach and draw gadgets
*/
	AddGList(dlg, gadgList, 0x7FFF, -1, NULL);
	RefreshGadgets(gadgList, dlg, NULL);
/*
	Activate first text box in dialog
*/
	if (_tbAutoActivate) {
		for (gadget = gadgList; gadget; gadget = gadget->NextGadget) {
			if (GadgetType(gadget) == GADG_EDIT_TEXT)
				break;
		}
		if (gadget)
			ActivateGadget(gadget, dlg, NULL);
	}
	SetWKind(dlg, WKIND_DLOG);
	return (dlg);
}

/*
 *	DisposeDialog
 *	Release all memory used by dialog created with GetDialog
 */

void DisposeDialog(DialogPtr dlg)
{
	GadgetPtr gadgList;

	if (dlg == NULL)
		return;
	gadgList = GadgetItem(dlg->FirstGadget, 0);	/* First user gadget */
	CloseWindowSafely(dlg, dlg->UserPort);
	if (gadgList)
		DisposeGadgets(gadgList);
}

/*
 *	Return TRUE if this is a dialog message
 *	Checks the window kind for WKIND_DLOG
 */

BOOL IsDialogMsg(IntuiMsgPtr intuiMsg)
{
	return (GetWKind(intuiMsg->IDCMPWindow) == WKIND_DLOG);
}

/*
 *	CheckDialog
 *	Get and handle dialog and requester events
 *	Returns item number of gadget that was selected
 *	Will return -1 if no gadget messages
 *	Note: Will not return with -1 unless ALL messages at port have been processed
 *	See ModalDialog() below for more details
 */

WORD CheckDialog(MsgPortPtr msgPort, DialogPtr dlg, BOOL (*reqFilter)(IntuiMsgPtr, WORD *))
{
	register IntuiMsgPtr intuiMsg;
	register ULONG class;
	register UWORD code, qualifier;
	register APTR iAddress;
	WindowPtr msgWindow;
	register GadgetPtr gadget;
	WORD item;

	if (dlg->FirstRequest == NULL && dlg->WLayer->front)
		WindowToFront(dlg);				/* If dialog, make sure it is in front */
	while (intuiMsg = (IntuiMsgPtr) GetMsg(msgPort)) {
		if (intuiMsg->Class == IDCMP_RAWKEY)
			ConvertKeyMsg(intuiMsg);
		if (reqFilter && (*reqFilter)(intuiMsg, &item)) {
			if (item != -1)
				return (item);
			continue;
		}
		class     = intuiMsg->Class;
		code	  = intuiMsg->Code;
		qualifier = intuiMsg->Qualifier;
		iAddress  = intuiMsg->IAddress;
		msgWindow = intuiMsg->IDCMPWindow;
		ReplyMsg((MsgPtr) intuiMsg);
		if (msgWindow != dlg)
			continue;
/*
	Handle the message
*/
		switch (class) {
		case IDCMP_GADGETUP:
			gadget = (GadgetPtr) iAddress;
			if (gadget && (gadget->GadgetType & GTYP_STRGADGET) &&
				(item = HandleKey(dlg, CR)) != -1)
				return (item);
			if (gadget && (gadget->GadgetID & GADG_NO_REPORT) == 0)
				return (GadgetNumber(gadget));
			break;
		case IDCMP_VANILLAKEY:
			if ((qualifier & IEQUALIFIER_REPEAT) == 0 &&
				(item = HandleKey(dlg, (TextChar) code)) != -1)
				return (item);
			break;
		}
	}
	return (-1);				/* No gadget message present */
}

/*
 *	ModalDialog
 *	Get and handle dialog and requester events
 *	Returns item number of gadget that was selected
 *	Will not return to caller until a gadget was actually selected
 *	Ignore all messages that are not for specified window/dialog
 *	If reqFilter is not NULL, then call reqFilter(intuiMsg, &item) before
 *		handling event
 *	reqFilter returns TRUE if it handled the message, FALSE if not
 *		If TRUE, then reqFilter must have called ReplyMsg(); item is set
 *			to item number to return (or -1)
 *		If FALSE then must have NOT called ReplyMsg()
 */

WORD ModalDialog(MsgPortPtr msgPort, DialogPtr dlg,
				 BOOL (*reqFilter)(IntuiMsgPtr, WORD *))
{
	register WORD item;

	do {
		(void) WaitPort(msgPort);
	} while ((item = CheckDialog(msgPort, dlg, reqFilter)) == -1);
	return (item);
}

/*
 *	DialogSelect
 *	Handle messages in modeless dialogs
 *	For gadget up and key down messages, sets dlg, itemHit and returns TRUE
 *	For all other messages it returns FALSE (with dlg and itemHit undefined)
 *	This routine does not reply to the message
 *	Call IsDialogMsg() to make sure only dialog messages are passed to this routine
 */

BOOL DialogSelect(IntuiMsgPtr intuiMsg, DialogPtr *dlg, WORD *itemHit)
{
	register ULONG class;
	register UWORD code, qualifier;
	register APTR iAddress;
	register GadgetPtr gadget;

	if (intuiMsg->Class == IDCMP_RAWKEY)
		ConvertKeyMsg(intuiMsg);
	class		= intuiMsg->Class;
	code		= intuiMsg->Code;
	qualifier	= intuiMsg->Qualifier;
	iAddress	= intuiMsg->IAddress;
	*dlg		= intuiMsg->IDCMPWindow;
/*
	Handle the message
*/
	switch (class) {
	case IDCMP_GADGETUP:
		gadget = (GadgetPtr) iAddress;
		if (gadget && (gadget->GadgetType & GTYP_STRGADGET) &&
			(*itemHit = HandleKey(*dlg, CR)) != -1)
			return (TRUE);
		if (gadget && (gadget->GadgetID & GADG_NO_REPORT) == 0) {
			*itemHit = GadgetNumber(gadget);
			return (TRUE);
		}
		break;
	case IDCMP_VANILLAKEY:
		if ((qualifier & IEQUALIFIER_REPEAT) == 0 &&
			(*itemHit = HandleKey(*dlg, (TextChar) code)) != -1)
			return (TRUE);
		break;
	}
	return (FALSE);
}
