/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Polygon handling routines
 */

#include <exec/types.h>
#include <graphics/gfxmacros.h>
#include <intuition/intuition.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>

#include <Toolbox/Memory.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Utility.h>
#include <Toolbox/Window.h>

#include "Draw.h"
#include "Proto.h"

/*
 *	External variables
 */

extern MsgPortPtr	mainMsgPort;

extern Defaults	defaults;
extern Options	options;

extern BOOL	autoScroll;

/*
 *	Local variables and definitions
 */

#define LINE_PAT	0xFFFF		// Line pattern for object creation

static PolyObjPtr	newPolyObj, growPolyObj;

static WORD	growHandle;

#define SELECTBUTTON(q)	\
	((q&IEQUALIFIER_LEFTBUTTON)||((q&AMIGAKEYS)&&(q&IEQUALIFIER_LALT)))

typedef struct {
	Point	*Points;
	LONG	NumPoints, MaxPoints;
} PointList, *PtListPtr;

/*
 *	Local prototypes
 */

static PointPtr		PointListAllocat(WORD);
static void 		DisposePoints(PointPtr *, WORD *, UWORD);
static void			GetPathPoint(PolyObjPtr, WORD, WORD *, WORD *);
static PolyObjPtr	CreatePoly(DocDataPtr);

static void			CalcBezierPoints(PtListPtr, PointPtr, PointPtr, PointPtr);
static PtListPtr	GetBezierPoints(PointPtr, WORD, BOOL);
static void			DispozeBezierPoints(PtListPtr);

static void	PolyDrawObj1(RastPtr, PolyObjPtr, RectPtr, PointPtr, RGBColor, RGBPat8Ptr);

static WORD	PolyLineNumber(PolyObjPtr, PointPtr);

static void	SetPolyFrame(PolyObjPtr);
static void	UnSetPolyFrame(PolyObjPtr);
static void	CollapsePoly(PolyObjPtr);

static void	CurveTrackMouse(WindowPtr, PointPtr, PointPtr, BOOL);

static void	PolyDrawLine(WindowPtr, PointPtr, PointPtr, BOOL, RGBColor);

/*
 *	allocate memory for pointlist
 */

static PointPtr PointListAllocate(WORD numPoints)
{
	return (MemAlloc(numPoints*sizeof(Point), MEMF_CLEAR));
}

/*
 *	Allocate a new poly object
 */

PolyObjPtr PolyAllocate()
{
	PolyObjPtr	polyObj;

	if ((polyObj = MemAlloc(sizeof(PolyObj), MEMF_CLEAR)) == NULL)
		return (NULL);
	if ((polyObj->NumPoints = MemAlloc(sizeof(WORD), MEMF_CLEAR)) == NULL) {
		MemFree(polyObj, sizeof(PolyObj));
		return (NULL);
	}
	if ((polyObj->Points = MemAlloc(sizeof(PointPtr), MEMF_CLEAR)) == NULL) {
		MemFree(polyObj->NumPoints, sizeof(WORD));
		MemFree(polyObj, sizeof(PolyObj));
		return (NULL);
	}
	polyObj->NumPaths = 1;
	return (polyObj);
}

/*
 *	dispose of point array
 */

static void DisposePoints(PointPtr *points, WORD *numPoints, UWORD numPaths)
{
	UWORD	pathNum;

	if (numPaths == 0)
		return;

	if (points) {
		for (pathNum = 0; pathNum < numPaths; pathNum++) {
			if (points[pathNum])
				MemFree(points[pathNum], numPoints[pathNum] * sizeof(Point));
		}
		MemFree(points, numPaths * sizeof(PointPtr));
	}
	if (numPoints)
		MemFree(numPoints, numPaths * sizeof(WORD));
}

/*
 *	Dispose of poly object
 */

void PolyDispose(PolyObjPtr polyObj)
{
	DisposePoints(polyObj->Points, polyObj->NumPoints, polyObj->NumPaths);
	MemFree(polyObj, sizeof(PolyObj));
}

/*
 *	return number of points in polygon
 */

UWORD NumPolyPoints(PolyObjPtr polyObj)
{
	UWORD	pathNum;
	UWORD	numPoints;

	numPoints = 0;
	for (pathNum = 0; pathNum < polyObj->NumPaths; pathNum++)
		numPoints += polyObj->NumPoints[pathNum];

	return (numPoints);
}

/*
 *	Transform point number to path number and local point number
 */

static void GetPathPoint(PolyObjPtr polyObj, WORD ptNum, WORD *pathNum, WORD *localPtNum)
{
	WORD	i;

	for (i = 0; i < polyObj->NumPaths; i++) {
		if (ptNum < polyObj->NumPoints[i]) {
			*pathNum = i;
			*localPtNum = ptNum;
			break;
		}
		ptNum -= polyObj->NumPoints[i];
	}
}

/*
 *	Create new poly
 */

static PolyObjPtr CreatePoly(DocDataPtr docData)
{
	PolyObjPtr	polyObj;

	if ((polyObj = (PolyObjPtr) NewDocObject(CurrLayer(docData), TYPE_POLY)) == NULL)
		return (NULL);
	polyObj->DocObj.Flags		= defaults.ObjFlags;
	polyObj->PolyFlags			= 0;
	polyObj->PenWidth			= defaults.PenWidth;
	polyObj->PenHeight			= defaults.PenHeight;
	polyObj->NumPaths			= 1;
	polyObj->PenColor			= defaults.PenColor;
	CopyRGBPat8(&defaults.FillPat, &polyObj->FillPat);
	return (polyObj);
}

/*
 *	Calculate points on second-order Bezier curve using three control points
 *	Caller must have added at least one point to start of list
 *	Uses recursion
 */

static void CalcBezierPoints(register PtListPtr ptList, register PointPtr pt1,
							 register PointPtr pt2, register PointPtr pt3)
{
	register WORD	dx1, dx2, dy1, dy2;
	Point			midPt, endPt;

	if (ptList->NumPoints >= ptList->MaxPoints)
		return;
	dx1 = pt2->x - pt1->x;
	dy1 = pt2->y - pt1->y;
	dx2 = pt3->x - pt2->x;
	dy2 = pt3->y - pt2->y;
/*
	Two-pixel accuracy gives smoother result (and it draws faster)
*/
	if ((ABS(dx1) <= 2 && ABS(dy1) <= 2) || (ABS(dx2) <= 2 && ABS(dy2) <= 2)) {
		if (pt3->x != ptList->Points[ptList->NumPoints - 1].x ||
			pt3->y != ptList->Points[ptList->NumPoints - 1].y)
			ptList->Points[ptList->NumPoints++] = *pt3;
	}
	else {
		endPt.x = (pt1->x + (pt2->x << 1) + pt3->x + 2) >> 2;
		endPt.y = (pt1->y + (pt2->y << 1) + pt3->y + 2) >> 2;
		midPt.x = (pt1->x + pt2->x + 1) >> 1;
		midPt.y = (pt1->y + pt2->y + 1) >> 1;
		CalcBezierPoints(ptList, pt1, &midPt, &endPt);
		midPt.x = (pt2->x + pt3->x + 1) >> 1;
		midPt.y = (pt2->y + pt3->y + 1) >> 1;
		CalcBezierPoints(ptList, &endPt, &midPt, pt3);
	}
}

/*
 *	Get bezier point list for polygon points
 *	If closed, then last point will equal first and numPoints is one greater than
 *		original number in polygon (since this is how it would be drawn if
 *		it weren't smoothed)
 */

static PtListPtr GetBezierPoints(PointPtr pts, WORD numPoints, BOOL closed)
{
	register WORD	i, dx, dy;
	LONG			maxPts;
	Point			pt1, pt2, pt3;
	Point			prevPt, currPt, nextPt;
	PtListPtr		ptList;

	if (numPoints < 3 || (closed && numPoints < 4) ||
		(ptList = MemAlloc(sizeof(PointList), MEMF_CLEAR)) == NULL)
		return (NULL);
	maxPts = 0;
	for (i = 0; i < numPoints - 1; i++) {
		dx = pts[i + 1].x - pts[i].x;
		dy = pts[i + 1].y - pts[i].y;
		maxPts += MAX(ABS(dx), ABS(dy))/2 + 1;
	}
	if ((ptList->Points = MemAlloc(maxPts*sizeof(Point), 0)) == NULL) {
		MemFree(ptList, sizeof(PointList));
		return (NULL);
	}
	ptList->MaxPoints = maxPts;
	if (closed) {
		currPt = pts[numPoints - 2];	/* Since last point is same as first */
		nextPt = pts[0];
		pt3.x = (currPt.x + nextPt.x)/2;
		pt3.y = (currPt.y + nextPt.y)/2;
		ptList->Points[0] = pt3;
		ptList->NumPoints = 1;
		for (i = 0; i < numPoints - 1; i++) {
			currPt = nextPt;
			nextPt = pts[i + 1];
			pt1 = pt3;
			pt2 = currPt;
			pt3.x = (currPt.x + nextPt.x)/2;
			pt3.y = (currPt.y + nextPt.y)/2;
			CalcBezierPoints(ptList, &pt1, &pt2, &pt3);
		}
	}
	else {
		currPt = pts[0];
		nextPt = pts[1];
		pt3.x = (currPt.x + nextPt.x)/2;
		pt3.y = (currPt.y + nextPt.y)/2;
		ptList->Points[0] = currPt;
		ptList->NumPoints = 1;
		for (i = 1; i < numPoints - 1; i++) {
			prevPt = currPt;
			currPt = nextPt;
			nextPt = pts[i + 1];
			if (i == 1)
				pt1 = prevPt;
			else
				pt1 = pt3;
			pt2 = currPt;
			if (i == numPoints - 2)
				pt3 = nextPt;
			else {
				pt3.x = (currPt.x + nextPt.x)/2;
				pt3.y = (currPt.y + nextPt.y)/2;
			}
			CalcBezierPoints(ptList, &pt1, &pt2, &pt3);
		}
	}
	return (ptList);
}

/*
 *	Dispose of memory allocated by GetBezierPoints()
 */

static void DisposeBezierPoints(PtListPtr ptList)
{
	if (ptList) {
		if (ptList->Points)
			MemFree(ptList->Points, ptList->MaxPoints*sizeof(Point));
		MemFree(ptList, sizeof(PointList));
	}
}

/*
 *	Draw poly object using specified line size, line color, and pattern
 */

static void PolyDrawObj1(RastPtr rPort, PolyObjPtr polyObj, RectPtr dstRect,
				  		 PointPtr pen, RGBColor penColor, RGBPat8Ptr fillPat)
{
	WORD 		i, numPoints, numPts[MAX_MERGE_POLY], drawNum[MAX_MERGE_POLY];
	UWORD		pathNum;
	BOOL 		closed, hasFill, hasPen, smoothed;
	PointPtr	points, newPts[MAX_MERGE_POLY], drawPts[MAX_MERGE_POLY];
	PtListPtr 	ptList[MAX_MERGE_POLY];
	Point 		offset;

	hasPen  = HasPen(polyObj);
	hasFill = HasFill(polyObj);
	closed  = PolyClosed(polyObj);
	smoothed = PolySmoothed(polyObj);
	if (!hasPen && !hasFill)
		return;
	for (i = 0; i < MAX_MERGE_POLY; i++) {
		newPts[i] = NULL;
		ptList[i] = NULL;
	}
/*
	Create new point list in rPort coordinates
*/
	for (pathNum = 0; pathNum < polyObj->NumPaths; pathNum++) {
		if ((numPoints = polyObj->NumPoints[pathNum]) < 2)
			continue;
		if (closed && numPoints != 2)
			numPoints++;
		if ((newPts[pathNum] = MemAlloc(numPoints*sizeof(Point), 0)) == NULL)
			return;
		BlockMove(polyObj->Points[pathNum], newPts[pathNum], numPoints*sizeof(Point));
		points = newPts[pathNum];
		for (i = 0; i < numPoints; i++) {
			OffsetPoint(&points[i], polyObj->DocObj.Frame.MinX, polyObj->DocObj.Frame.MinY);
			MapPt(&points[i], &polyObj->DocObj.Frame, dstRect);
		}
		numPts[pathNum] = numPoints;
		if (closed && numPoints != 2)
			points[numPoints - 1] = points[0];
/*
	Get smooth point list
*/
		if (smoothed &&
			(ptList[pathNum] = GetBezierPoints(newPts[pathNum], numPoints, closed)) != NULL) {
			drawPts[pathNum] = ptList[pathNum]->Points;
			drawNum[pathNum] = ptList[pathNum]->NumPoints;
		}
		else {
			ptList[pathNum]  = NULL;
			drawPts[pathNum] = newPts[pathNum];
			drawNum[pathNum] = numPoints;
		}
	}
/*
	Fill the poly
*/
	if (hasFill) {
		if (fillPat == NULL)
			RGBForeColor(rPort, penColor);
		FillPolyNew(rPort, polyObj->NumPaths, drawNum, drawPts, fillPat);
	}
/*
	Draw poly border
*/
	if (hasPen) {
		offset.x = -pen->x/2;
		offset.y = -pen->y/2;
		if (offset.x || offset.y) {
			for (pathNum = 0; pathNum < polyObj->NumPaths; pathNum++) {
				points = drawPts[pathNum];
				for (i = 0; i < drawNum[pathNum]; i++)
					OffsetPoint(&points[i], offset.x, offset.y);
			}
		}
		PenNormal(rPort);
		RGBForeColor(rPort, penColor);
		FramePolyNew(rPort, polyObj->NumPaths, drawNum, drawPts, pen->x, pen->y);
	}
/*
	Dispose of point list
*/
Exit:
	for (i = 0; i < polyObj->NumPaths; i++) {
		if (ptList[i])
			DisposeBezierPoints(ptList[i]);
		if (newPts[i])
			MemFree(newPts[i], numPts[i]*sizeof(Point));
	}
}

void PolyDrawObj(RastPtr rPort, PolyObjPtr polyObj, RectPtr dstRect, RectPtr clipRect)
{
	Point pen;

	pen.x = polyObj->PenWidth;
	pen.y = polyObj->PenHeight;
	ScalePt(&pen, &polyObj->DocObj.Frame, dstRect);
	PolyDrawObj1(rPort, polyObj, dstRect, &pen, polyObj->PenColor, &polyObj->FillPat);
}

/*
 *	Draw poly outline (for dragging)
 *	Offset is in document coordinates
 */

void PolyDrawOutline(WindowPtr window, PolyObjPtr polyObj, WORD xOffset, WORD yOffset)
{
	WORD 		i, pathNum, numPoints, drawNum;
	BOOL 		closed, smoothed;
	RastPtr 	rPort = window->RPort;
	PointPtr	newPts, drawPts;
	PtListPtr	ptList;

	closed = PolyClosed(polyObj);
	smoothed = PolySmoothed(polyObj);
/*
	Create new point list in rPort coordinates for each path
*/
	for (pathNum = 0; pathNum < polyObj->NumPaths; pathNum++) {
		if ((numPoints = polyObj->NumPoints[pathNum]) < 2)
			continue;
		if (closed && numPoints != 2)
			numPoints++;
		if ((newPts = MemAlloc(numPoints*sizeof(Point), 0)) == NULL)
			continue;
		BlockMove(polyObj->Points[pathNum], newPts, numPoints*sizeof(Point));
		if (closed && numPoints != 2)
			newPts[numPoints - 1] = newPts[0];
		for (i = 0; i < numPoints; i++) {
			OffsetPoint(&newPts[i], xOffset + polyObj->DocObj.Frame.MinX,
						yOffset + polyObj->DocObj.Frame.MinY);
			DocToWindow(window, newPts[i].x, newPts[i].y, &newPts[i].x, &newPts[i].y);
		}
/*
	Get smooth point list
*/
		if (smoothed && (ptList = GetBezierPoints(newPts, numPoints, closed)) != NULL) {
			drawPts = ptList->Points;
			drawNum = ptList->NumPoints;
		}
		else {
			ptList  = NULL;
			drawPts = newPts;
			drawNum = numPoints;
		}
/*
	Draw poly object
*/
		SetDrPt(rPort, LINE_PAT);
		SetDrawComplement(rPort);
		FramePoly(rPort, drawNum, drawPts);
		ClearDrawComplement(rPort);
/*
	Dispose of new point list
*/
		if (ptList)
			DisposeBezierPoints(ptList);
		MemFree(newPts, numPoints*sizeof(Point));
	}
}

/*
 *	Draw selection hilighting for poly
 */

void PolyHilite(WindowPtr window, PolyObjPtr polyObj)
{
	WORD 		i, pathNum, numPoints;
	Point 		pt, prevPt, windPt, *points;

	for (pathNum = 0; pathNum < polyObj->NumPaths; pathNum++) {
		numPoints = polyObj->NumPoints[pathNum];
		points = polyObj->Points[pathNum];
		prevPt = points[numPoints - 1];
		for (i = 0; i < numPoints; i++) {
			pt = points[i];
			if (pt.x != prevPt.x || pt.y != prevPt.y) {
				windPt = pt;
				OffsetPoint(&windPt, polyObj->DocObj.Frame.MinX, polyObj->DocObj.Frame.MinY);
				DrawHandle(window, windPt.x, windPt.y);
			}
			prevPt = pt;
		}
	}
}

/*
 *	Set poly pen color
 */

void PolySetPenColor(PolyObjPtr polyObj, RGBColor penColor)
{
	polyObj->PenColor = penColor;
}

/*
 *	Set poly pen size
 *	If size is -1 then do not change
 */

void PolySetPenSize(PolyObjPtr polyObj, WORD penWidth, WORD penHeight)
{
	if (penWidth != -1)
		polyObj->PenWidth = penWidth;
	if (penHeight != -1)
		polyObj->PenHeight = penHeight;
}

/*
 *	Set poly fill pattern
 */

void PolySetFillPat(PolyObjPtr polyObj, RGBPat8Ptr fillPat)
{
	CopyRGBPat8(fillPat, &polyObj->FillPat);
}

/*
 *	Get poly fill pattern
 */

BOOL PolyGetFillPat(PolyObjPtr polyObj, RGBPat8Ptr fillPat)
{
	CopyRGBPat8(&polyObj->FillPat, fillPat);
	return (TRUE);
}

/*
 *	Rotate poly by given angle
 */

void PolyRotate(PolyObjPtr polyObj, WORD cx, WORD cy, WORD angle)
{
	WORD 		i, numPoints;
	PointPtr	points;
	UWORD		pathNum;

	UnSetPolyFrame(polyObj);
	for (pathNum = 0; pathNum < polyObj->NumPaths; pathNum++) {
		numPoints = polyObj->NumPoints[pathNum];
		points = polyObj->Points[pathNum];
		for (i = 0; i < numPoints; i++)
			RotatePoint(&points[i], cx, cy, angle);
	}
	SetPolyFrame(polyObj);
}

/*
 *	Flip poly horizontally or vertically
 */

void PolyFlip(PolyObjPtr polyObj, WORD cx, WORD cy, BOOL horiz)
{
	WORD 		i, numPoints;
	PointPtr	points;
	UWORD		pathNum;

	UnSetPolyFrame(polyObj);
	for (pathNum = 0; pathNum < polyObj->NumPaths; pathNum++) {
		numPoints = polyObj->NumPoints[pathNum];
		points = polyObj->Points[pathNum];
		for (i = 0; i < numPoints; i++)
			FlipPoint(&points[i], cx, cy, horiz);
	}
	SetPolyFrame(polyObj);
}

/*
 *	Scale poly to new frame
 */

void PolyScale(PolyObjPtr polyObj, RectPtr frame)
{
	WORD 				i;
	register PointPtr 	points;
	UWORD				pathNum;

	for (pathNum = 0; pathNum < polyObj->NumPaths; pathNum++) {
		points = polyObj->Points[pathNum];
		for (i = 0; i < polyObj->NumPoints[pathNum]; i++) {
			OffsetPoint(&points[i], polyObj->DocObj.Frame.MinX, polyObj->DocObj.Frame.MinY);
			MapPt(&points[i], &polyObj->DocObj.Frame, frame);
			OffsetPoint(&points[i], -frame->MinX, -frame->MinY);
		}
	}
	polyObj->DocObj.Frame = *frame;
}

/*
 *	Return the line number the point is near, or -1 if not near any poly line
 *	Line number is point number of line start
 *	Only valid for non-smoothed polys
 */

static WORD PolyLineNumber(PolyObjPtr polyObj, PointPtr pt)
{
	WORD 				i, numPoints, totPoints;
	register PointPtr	points;
	UWORD				pathNum;

	totPoints = 0;
	for (pathNum = 0; pathNum < polyObj->NumPaths; pathNum++) {
		numPoints = polyObj->NumPoints[pathNum];
		points = polyObj->Points[pathNum];
		for (i = 0; i < numPoints - 1; i++) {
			if (PtNearLine(pt, &points[i], &points[i + 1],
						   polyObj->PenWidth, polyObj->PenHeight, 2))
				return (i + totPoints);
		}
		if (PolyClosed(polyObj)) {
			if (PtNearLine(pt, &points[numPoints - 1], &points[0],
						   polyObj->PenWidth, polyObj->PenHeight, 2))
				return (numPoints - 1 + totPoints);
		}
		totPoints += numPoints;
	}
	return (-1);
}

/*
 *	Determine if point is in poly
 */

BOOL PolySelect(PolyObjPtr polyObj, PointPtr pt)
{
	WORD		x, y, xStart, xEnd, yStart, yEnd;
	BOOL		select;
	RastPtr		rPort;
	Point		newPt, pen;
	Rectangle	rect;
	PolyObj		tempPolyObj;

	newPt = *pt;
	rect = polyObj->DocObj.Frame;
	OffsetPoint(&newPt, -rect.MinX, -rect.MinY);
	OffsetRect(&rect, -rect.MinX, -rect.MinY);
	if ((rPort = CreateRastPort(rect.MaxX + 1, rect.MaxY + 1, 1)) == NULL)
		return (TRUE);
	tempPolyObj = *polyObj;
	tempPolyObj.DocObj.Frame = rect;
	pen.x = pen.y = 1;					// For faster drawing
	if (HasFill(polyObj))
		tempPolyObj.DocObj.Flags &= ~OBJ_DO_PEN;
	PolyDrawObj1(rPort, &tempPolyObj, &rect, &pen, RGBCOLOR_BLACK, NULL);
	xStart = newPt.x - polyObj->PenWidth/2 - 1;
	yStart = newPt.y - polyObj->PenHeight/2 - 1;
	xEnd = xStart + polyObj->PenWidth + 2;
	yEnd = yStart + polyObj->PenHeight + 2;
	if (xStart < 0)
		xStart = 0;
	if (yStart < 0)
		yStart = 0;
	if (xEnd > rect.MaxX)
		xEnd = rect.MaxX;
	if (yEnd > rect.MaxY)
		yEnd = rect.MaxY;
	select = FALSE;
	for (x = xStart; x <= xEnd; x++) {
		for (y = yStart; y <= yEnd; y++) {
			if (ReadPixel(rPort, x, y) == 1) {
				select = TRUE;
				break;
			}
		}
		if (select)
			break;
	}
	DisposeRastPort(rPort);
	return (select);
}

/*
 *	Return handle number of given point, or -1 if not in a handle
 */

WORD PolyHandle(PolyObjPtr polyObj, PointPtr pt, RectPtr handleRect)
{
	WORD		i, pathNum, totPoints;
	PointPtr 	points;
	Point		newPt;

	newPt = *pt;
	OffsetPoint(&newPt, -polyObj->DocObj.Frame.MinX, -polyObj->DocObj.Frame.MinY);
	totPoints = 0;
	for (pathNum = 0; pathNum < polyObj->NumPaths; pathNum++) {
		points = polyObj->Points[pathNum];
		for (i = 0; i < polyObj->NumPoints[pathNum]; i++) {
			if (InHandle(&newPt, &points[i], handleRect))
				return (i + totPoints);
		}
		totPoints += polyObj->NumPoints[pathNum];
	}

	return (-1);
}

/*
 *	Duplicate poly data to new object
 *	Return success status
 */

BOOL PolyDupData(PolyObjPtr polyObj, PolyObjPtr newObj)
{
	WORD 	numPoints;
	UWORD	pathNum;

	newObj->PolyFlags	= polyObj->PolyFlags;
	newObj->PenWidth	= polyObj->PenWidth;
	newObj->PenHeight	= polyObj->PenHeight;
	newObj->PenColor	= polyObj->PenColor;
	CopyRGBPat8(&polyObj->FillPat, &newObj->FillPat);

	DisposePoints(newObj->Points, newObj->NumPoints, newObj->NumPaths);
	newObj->NumPaths	= polyObj->NumPaths;
	if ((newObj->NumPoints = MemAlloc(newObj->NumPaths * sizeof(WORD), 0)) == NULL)
		return (FALSE);
	if ((newObj->Points = MemAlloc(newObj->NumPaths * sizeof(PointPtr), 0)) == NULL)
		return (FALSE);

	for (pathNum = 0; pathNum < polyObj->NumPaths; pathNum++) {
		numPoints = polyObj->NumPoints[pathNum];
		newObj->NumPoints[pathNum] = numPoints;
		if ((newObj->Points[pathNum] = PointListAllocate(numPoints)) == NULL)
			return (FALSE);
		BlockMove(polyObj->Points[pathNum], newObj->Points[pathNum], numPoints*sizeof(Point));
	}
	return (TRUE);
}

/*
 *	Add point to poly object after specified point
 *	If after is -1, add to start of poly point list
 *	Return success status
 */

BOOL PolyAddPoint(PolyObjPtr polyObj, WORD pathNum, WORD after, WORD x, WORD y)
{
	WORD 		i, numPoints;
	PointPtr 	newPts, oldPts;

	if (pathNum == -1) {
		GetPathPoint(polyObj, after, &pathNum, &after);
		if (pathNum >= polyObj->NumPaths)
			return (FALSE);
	}
	numPoints = polyObj->NumPoints[pathNum];
	oldPts = polyObj->Points[pathNum];
	if (numPoints == 0x7FFF || (newPts = PointListAllocate(numPoints + 1)) == NULL)
		return (FALSE);
	if (after > numPoints - 1)
		after = numPoints - 1;
	for (i = 0; i <= after; i++)
		newPts[i] = oldPts[i];
	newPts[i].x = x;
	newPts[i].y = y;
	while (i++ < numPoints)
		newPts[i] = oldPts[i - 1];
	if (numPoints)
		MemFree(polyObj->Points[pathNum], numPoints*sizeof(Point));
	polyObj->Points[pathNum] = newPts;
	polyObj->NumPoints[pathNum] = numPoints + 1;
	return (TRUE);
}

/*
 *	Remove the specified point from poly
 *	Will not reduce poly to fewer than two points
 */

void PolyRemPoint(PolyObjPtr polyObj, WORD ptNum)
{
	WORD		i, numPoints, pathNum;
	PointPtr	newPts, oldPts;

	GetPathPoint(polyObj, ptNum, &pathNum, &ptNum);
	if (pathNum >= polyObj->NumPaths)
		return;
	numPoints = polyObj->NumPoints[pathNum];
	oldPts = polyObj->Points[pathNum];
	if (ptNum >= numPoints || numPoints <= 2)
		return;
	if (numPoints == 1)
		newPts = NULL;
	else if (numPoints == 0 ||
		(newPts = PointListAllocate(numPoints - 1)) == NULL)
		return;
	for (i = 0; i < ptNum; i++)
		newPts[i] = oldPts[i];
	i++;
	while (i++ < numPoints)
		newPts[i - 2] = oldPts[i - 1];
	if (numPoints)
		MemFree(polyObj->Points[pathNum], numPoints*sizeof(Point));
	polyObj->Points[pathNum] = newPts;
	polyObj->NumPoints[pathNum] = numPoints - 1;
}

/*
 *	Add handle at given point if it is on a poly line
 *	Point is relative to poly frame
 *	Return TRUE if handle added
 */

BOOL PolyAddHandle(PolyObjPtr polyObj, PointPtr pt)
{
	WORD	i;
	PolyObj	tempPolyObj;

	if (PolySmoothed(polyObj))
		return (FALSE);
	tempPolyObj = *polyObj;
	EnableObjectPen(&tempPolyObj, TRUE);
	EnableObjectFill(&tempPolyObj, FALSE);
	if ((i = PolyLineNumber(&tempPolyObj, pt)) != -1) {
		PolyAddPoint(polyObj, -1, i, pt->x, pt->y);
		return (TRUE);
	}
	return (FALSE);
}

/*
 *	Remove the handle at the given point
 *	Point is relative to poly frame
 */

void PolyRemHandle(PolyObjPtr polyObj, WORD handle)
{
	if (handle < 0 || handle >= NumPolyPoints(polyObj))
		return;
	PolyRemPoint(polyObj, handle);
	PolyAdjustFrame(polyObj);
}

/*
 *	Set poly frame from point coordinates
 *	Also offset points to frame start
 */

static void SetPolyFrame(PolyObjPtr polyObj)
{
	WORD 		i, numPoints, x, y;
	Rectangle 	rect;
	PointPtr	points;
	UWORD		pathNum;

	points = polyObj->Points[0];
	rect.MinX = rect.MaxX = points[0].x;
	rect.MinY = rect.MaxY = points[0].y;
	for (pathNum = 0; pathNum < polyObj->NumPaths; pathNum++) {
		if ((numPoints = polyObj->NumPoints[pathNum]) == 0)
			continue;
		points = polyObj->Points[pathNum];
		for (i = 0; i < numPoints; i++) {
			x = points[i].x;
			y = points[i].y;
			if (x < rect.MinX)
				rect.MinX = x;
			if (x > rect.MaxX)
				rect.MaxX = x;
			if (y < rect.MinY)
				rect.MinY = y;
			if (y > rect.MaxY)
				rect.MaxY = y;
		}
	}
	for (pathNum = 0; pathNum < polyObj->NumPaths; pathNum++) {
		OffsetPoly(polyObj->NumPoints[pathNum], polyObj->Points[pathNum],
									-rect.MinX, -rect.MinY);
	}
	polyObj->DocObj.Frame = rect;
}

/*
 *	Offset all poly points by frame start
 */

static void UnSetPolyFrame(PolyObjPtr polyObj)
{
	UWORD	pathNum;

	for (pathNum = 0; pathNum < polyObj->NumPaths; pathNum++) {
		OffsetPoly(polyObj->NumPoints[pathNum], polyObj->Points[pathNum],
				   polyObj->DocObj.Frame.MinX, polyObj->DocObj.Frame.MinY);
	}
}

/*
 *	Adjust poly frame after making modifications to point list
 */

void PolyAdjustFrame(PolyObjPtr polyObj)
{
	UnSetPolyFrame(polyObj);
	SetPolyFrame(polyObj);
}

/*
 *	Set poly smoothing on or off
 */

void PolySetSmooth(PolyObjPtr polyObj, BOOL smooth)
{
	if (smooth)
		polyObj->PolyFlags |= POLY_SMOOTH;
	else
		polyObj->PolyFlags &= ~POLY_SMOOTH;
}

/*
 *	Set poly to closed on or off
 */

void PolySetClosed(PolyObjPtr polyObj, BOOL closed)
{
	if (closed)
		polyObj->PolyFlags |= POLY_CLOSED;
	else
		polyObj->PolyFlags &= ~POLY_CLOSED;
}

/*
 *	merge polyObj into mergePolyObj
 */

BOOL MergePoly(WindowPtr window, PolyObjPtr mergePolyObj, PolyObjPtr polyObj)
{
	UWORD		pathNum, totPaths;
	WORD		*numPoints, i, dx, dy;
	PointPtr	*points, pts;
	Rectangle	rect;
/*
	increase array of NumPoints
*/
	totPaths = mergePolyObj->NumPaths + polyObj->NumPaths;
	if (totPaths > MAX_MERGE_POLY)
		return (FALSE);

	if ((numPoints = MemAlloc(totPaths * sizeof(WORD), 0)) == NULL)
		return (FALSE);
	BlockMove(mergePolyObj->NumPoints, numPoints, mergePolyObj->NumPaths * sizeof(WORD));
	BlockMove(polyObj->NumPoints, &numPoints[mergePolyObj->NumPaths], polyObj->NumPaths * sizeof(WORD));
/*
	increase array of PointPtrs
*/
	if ((points = MemAlloc(totPaths * sizeof(PointPtr), 0)) == NULL) {
		MemFree(numPoints, totPaths * sizeof(WORD));
		return (FALSE);
	}
	UnionRect(&mergePolyObj->DocObj.Frame, &polyObj->DocObj.Frame, &rect);
/*
	merge array of points
 */
	for (pathNum = 0; pathNum < totPaths; pathNum++) {
		if ((points[pathNum] = PointListAllocate(numPoints[pathNum])) == NULL) {
			MemFree(numPoints, totPaths * sizeof(WORD));
			MemFree(points, totPaths * sizeof(PointPtr));
			return (FALSE);
		}
		if (pathNum < mergePolyObj->NumPaths) {
			BlockMove(mergePolyObj->Points[pathNum], points[pathNum],
						numPoints[pathNum] * sizeof(Point));
		}
		else {
			BlockMove(polyObj->Points[pathNum - mergePolyObj->NumPaths],
						points[pathNum], numPoints[pathNum] * sizeof(Point));
		}
		pts = points[pathNum];
		for (i = 0; i < numPoints[pathNum]; i++) {
			if (pathNum < mergePolyObj->NumPaths) {
				dx = mergePolyObj->DocObj.Frame.MinX - rect.MinX;
				dy = mergePolyObj->DocObj.Frame.MinY - rect.MinY;
			}
			else {
				dx = polyObj->DocObj.Frame.MinX - rect.MinX;
				dy = polyObj->DocObj.Frame.MinY - rect.MinY;
			}
			OffsetPoint(&pts[i], dx, dy);
		}
	}
	InvalObjectRect(window, mergePolyObj);
	mergePolyObj->DocObj.Frame = rect;
/*
	dispose of old arrays
*/
	DisposePoints(mergePolyObj->Points, mergePolyObj->NumPoints, mergePolyObj->NumPaths);
	mergePolyObj->NumPoints = numPoints;
	mergePolyObj->Points = points;
	mergePolyObj->NumPaths = totPaths;
/*
	dispose of polyObj
*/
	InvalObjectRect(window, polyObj);
	PolyDispose(polyObj);
	return (TRUE);
}

/*
 * 	unmerge polyObj into separate polys
 */

BOOL UnMergePoly(WindowPtr window, PolyObjPtr polyObj)
{
	UWORD		pathNum, numPaths;
	PolyObjPtr	newObj;
	PointPtr	pts, points[MAX_MERGE_POLY];
	WORD		i, numPts[MAX_MERGE_POLY];
	BOOL		success;
	DocDataPtr	docData = GetWRefCon(window);

	success = FALSE;
	numPaths = polyObj->NumPaths;
	for (pathNum = polyObj->NumPaths - 1; pathNum > 0; pathNum--) {
		if ((newObj = CreatePoly(docData)) == NULL)
			break;
		DetachObject(CurrLayer(docData), newObj);
		InsertObject(CurrLayer(docData), polyObj, newObj);
		newObj->PolyFlags	= polyObj->PolyFlags;
		newObj->PenWidth	= polyObj->PenWidth;
		newObj->PenHeight	= polyObj->PenHeight;
		newObj->PenColor	= polyObj->PenColor;
		newObj->NumPaths	= 1;
		CopyRGBPat8(&polyObj->FillPat, &newObj->FillPat);
		newObj->NumPoints[0] = polyObj->NumPoints[pathNum];
		newObj->Points[0] 	= polyObj->Points[pathNum];
		pts = newObj->Points[0];
		for (i = 0; i < newObj->NumPoints[0]; i++)
			OffsetPoint(&pts[i], polyObj->DocObj.Frame.MinX,
									polyObj->DocObj.Frame.MinY);
		SetPolyFrame(newObj);
		InvalObjectRect(window, newObj);
		polyObj->Points[pathNum] = NULL;
		SelectObject(newObj);
		numPaths--;
	}
	if (numPaths == 1)
		success = TRUE;
/*
	save old arrays
*/
	for (pathNum = 0; pathNum < numPaths; pathNum++) {
		points[pathNum] = polyObj->Points[pathNum];
		numPts[pathNum] = polyObj->NumPoints[pathNum];
	}
/*
	dispose of old arrays
*/
	MemFree(polyObj->NumPoints, polyObj->NumPaths * sizeof(WORD));
	MemFree(polyObj->Points, polyObj->NumPaths * sizeof(PointPtr));
/*
	create new arrays
	NOTE: guaranteed enough memory since just freed more than enough
*/
	polyObj->Points = MemAlloc(numPaths * sizeof(PointPtr), 0);
	polyObj->NumPoints = MemAlloc(numPaths * sizeof(WORD), 0);
/*
	save new arrays
*/
	for (pathNum = 0; pathNum < numPaths; pathNum++) {
		polyObj->Points[pathNum] = points[pathNum];
		polyObj->NumPoints[pathNum] = numPts[pathNum];
	}

	polyObj->NumPaths = numPaths;
	for (pathNum = 0; pathNum < numPaths; pathNum++) {
		pts = polyObj->Points[pathNum];
		for (i = 0; i < polyObj->NumPoints[pathNum]; i++)
			OffsetPoint(&pts[i], polyObj->DocObj.Frame.MinX, polyObj->DocObj.Frame.MinY);
	}
	SetPolyFrame(polyObj);
	InvalObjectRect(window, polyObj);
	return (success);
}

/*
 *	Remove redundant points on poly
 *	Only used after free-hand draw
 */

static void CollapsePoly(PolyObjPtr polyObj)
{
	WORD 		i, ptNum;
	BOOL 		changed;
	UWORD		pathNum;
	PointPtr	points;

	do {
		changed = FALSE;
		for (i = 1; i < NumPolyPoints(polyObj) - 1; i++) {
			GetPathPoint(polyObj, i, &pathNum, &ptNum);
			points = polyObj->Points[pathNum];
			if (PtNearLine(&points[ptNum], &points[ptNum - 1], &points[ptNum + 1],
						   0, 0, 1)) {
				PolyRemPoint(polyObj, i);
				changed = TRUE;
			}
		}
	} while (NumPolyPoints(polyObj) > 2 && changed);
}

/*
 *	Draw routine for tracking curve creation
 */

static void CurveTrackMouse(WindowPtr window, PointPtr pt1, PointPtr pt2, BOOL change)
{
	WORD 		x, y, numPoints;
	RGBColor 	color;
	RastPtr 	rPort = window->RPort;
	PointPtr	points;
	Point 		prevPt;

	if (change) {
		numPoints = newPolyObj->NumPoints[0];
		points = newPolyObj->Points[0];
		prevPt = points[numPoints - 1];
		if (!EqualPt(pt2, &prevPt)) {
			PolyAddPoint(newPolyObj, 0, NumPolyPoints(newPolyObj), pt2->x, pt2->y);
			PenNormal(rPort);
			color = newPolyObj->PenColor;
			if (color == RGBCOLOR_WHITE)
				color = RGBCOLOR_BLACK;
			RGBForeColor(rPort, color);
			DocToWindow(window, prevPt.x, prevPt.y, &x, &y);
			Move(rPort, x, y);
			DocToWindow(window, pt2->x, pt2->y, &x, &y);
			Draw(rPort, x, y);
		}
	}
}

/*
 *	Create poly object from free-hand curve input
 */

DocObjPtr CurveCreate(WindowPtr window, UWORD modifier, WORD mouseX, WORD mouseY)
{
	WORD 		endPtNum;
	ULONG 		oldDocFlags;
	PointPtr	points;
	DocDataPtr 	docData = GetWRefCon(window);
	Point 		pt1, pt2;

/*
	Create new poly object to contain points on curve
*/
	if ((newPolyObj = CreatePoly(docData)) == NULL)
		return (NULL);
/*
	Track curve
*/
	WindowToDoc(window, mouseX, mouseY, &pt1.x, &pt1.y);	// No grid snap on curves
	PolyAddPoint(newPolyObj, 0, -1, pt1.x, pt1.y);
	pt2 = pt1;
	ModifyIDCMP(window, window->IDCMPFlags | MOUSEMOVE);
	oldDocFlags = docData->Flags;
	docData->Flags &= ~DOC_GRIDSNAP;
	autoScroll = FALSE;
	TrackMouse(window, modifier, &pt2, CurveTrackMouse, NULL);
	autoScroll = TRUE;
	docData->Flags = oldDocFlags;
	ModifyIDCMP(window, window->IDCMPFlags & ~MOUSEMOVE);
/*
	If final point is within 1 pixel of start, make it a closed poly
*/
	if ((endPtNum = (newPolyObj->NumPoints[0] - 1)) > 1) {
		points = newPolyObj->Points[0];
		pt1 = points[0];
		pt2 = points[endPtNum];
		if (pt2.x <= pt1.x + 1 && pt2.x >= pt1.x - 1 &&
			pt2.y <= pt1.y + 1 && pt2.y >= pt1.y - 1) {
			PolyRemPoint(newPolyObj, endPtNum);
			newPolyObj->PolyFlags |= POLY_CLOSED;
		}
		else if (options.PolyAutoClose)
			newPolyObj->PolyFlags |= POLY_CLOSED;
	}
/*
	Set object data
*/
	newPolyObj->PolyFlags |= POLY_SMOOTH;
	SetPolyFrame(newPolyObj);
	InvalObjectRect(window, newPolyObj);	// Object rect may change
	CollapsePoly(newPolyObj);
	PolyAdjustFrame(newPolyObj);
	return (newPolyObj);
}

/*
 *	Draw routine for tracking poly creation
 *	Draw a solid or inverted line from start to end (in doc coordinates)
 */

static void PolyDrawLine(WindowPtr window, PointPtr start, PointPtr end,
						 BOOL solid, RGBColor color)
{
	WORD	x, y;
	RastPtr	rPort = window->RPort;

	if (solid) {
		SetDrMd(rPort, JAM1);
		if (color == RGBCOLOR_WHITE)
			color = RGBCOLOR_BLACK;
		RGBForeColor(rPort, color);
	}
	else
		SetDrawComplement(rPort);
	SetDrPt(rPort, LINE_PAT);
	DocToWindow(window, start->x, start->y, &x, &y);
	Move(rPort, x, y);
	DocToWindow(window, end->x, end->y, &x, &y);
	Draw(rPort, x, y);
	if (!solid)
		ClearDrawComplement(rPort);
}

/*
 *	Create poly object
 */

DocObjPtr PolyCreate(WindowPtr window, UWORD modifier, WORD mouseX, WORD mouseY)
{
	BOOL 		done, mouseDown;
	ULONG 		class;
	UWORD 		code;
	IntuiMsgPtr intuiMsg;
	Point 		pt1, pt2, newPt, *points;
	DocDataPtr 	docData = GetWRefCon(window);

/*
	Create new poly object to contain points in poly
*/
	if ((newPolyObj = CreatePoly(docData)) == NULL)
		return (NULL);
/*
	Track poly
*/
	WindowToDoc(window, mouseX, mouseY, &pt1.x, &pt1.y);
	SnapToGrid(docData, &pt1.x, &pt1.y);
	PolyAddPoint(newPolyObj, 0, -1, pt1.x, pt1.y);
	pt2 = pt1;
	PolyDrawLine(window, &pt1, &pt2, FALSE, 0);
	done = FALSE;
	mouseDown = TRUE;
	do {
		if (mouseDown)
			while (WaitMouseUp(mainMsgPort, window)) ;
		mouseDown = FALSE;
		while (!mouseDown && (intuiMsg = (IntuiMsgPtr) GetMsg(mainMsgPort)) != NULL) {
			class = intuiMsg->Class;
			code = intuiMsg->Code;
			modifier = intuiMsg->Qualifier;
			if (intuiMsg->IDCMPWindow != window ||
				(class != MOUSEMOVE && class != MOUSEBUTTONS &&
				 class != RAWKEY && class != INTUITICKS)) {
				PutMsg(mainMsgPort, (MsgPtr) intuiMsg);
				done = TRUE;
				break;
			}
			ReplyMsg((MsgPtr) intuiMsg);
			if (class == MOUSEBUTTONS && code == SELECTDOWN)
				mouseDown = TRUE;
			else if (class != INTUITICKS)
				mouseDown = SELECTBUTTON(modifier);
		}
		WindowToDoc(window, window->MouseX, window->MouseY, &newPt.x, &newPt.y);
		SnapToGrid(docData, &newPt.x, &newPt.y);
		if (newPt.x != pt2.x || newPt.y != pt2.y) {
			PolyDrawLine(window, &pt1, &pt2, FALSE, 0);
			pt2 = newPt;
			PolyDrawLine(window, &pt1, &pt2, FALSE, 0);
		}
		UpdateRulerIndic(window);
		if (mouseDown) {
			if (EqualPt(&pt1, &pt2))
				done = TRUE;
			else {
				PolyDrawLine(window, &pt1, &pt2, FALSE, 0);
				PolyDrawLine(window, &pt1, &pt2, TRUE, newPolyObj->PenColor);
				pt1 = pt2;
				PolyDrawLine(window, &pt1, &pt2, FALSE, 0);
				points = newPolyObj->Points[0];
				if (EqualPt(&pt2, &points[0])) {
					newPolyObj->PolyFlags |= POLY_CLOSED;
					done = TRUE;
				}
				else
					PolyAddPoint(newPolyObj, 0, newPolyObj->NumPoints[0], pt2.x, pt2.y);
			}
		}
	} while (!done);
	PolyDrawLine(window, &pt1, &pt2, FALSE, 0);
/*
	Set object data
*/
Exit:
	newPolyObj->PolyFlags &= ~POLY_SMOOTH;
	if (options.PolyAutoClose)
		newPolyObj->PolyFlags |= POLY_CLOSED;
	SetPolyFrame(newPolyObj);
	return (newPolyObj);
}

/*
 *	Draw routine for tracking poly shape change
 */

static void PolyTrackGrow(WindowPtr window, PointPtr pt1, PointPtr pt2, BOOL change)
{
	WORD 		pathNum, handle, numPoints, numPts[1];
	BOOL 		closed;
	PolyObj 	polyObj;
	Point 		newPt, *points, *pts[1], dragPts[3];

	closed = PolyClosed(growPolyObj);
	if (change) {
		GetPathPoint(growPolyObj, growHandle, &pathNum, &handle);
		points = growPolyObj->Points[pathNum];
		numPoints = growPolyObj->NumPoints[pathNum];
		BlockClear(&polyObj, sizeof(PolyObj));
		polyObj = *growPolyObj;
		polyObj.PolyFlags &= ~(POLY_CLOSED | POLY_SMOOTH);
		polyObj.NumPaths = 1;
		polyObj.NumPoints = numPts;
		polyObj.Points = pts;
		polyObj.Points[0] = dragPts;
		newPt = *pt2;
		OffsetPoint(&newPt, -growPolyObj->DocObj.Frame.MinX, -growPolyObj->DocObj.Frame.MinY);
		if (handle == 0) {
			if (closed && numPoints != 2) {
				dragPts[0] = points[numPoints - 1];
				dragPts[1] = newPt;
				dragPts[2] = points[1];
				polyObj.NumPoints[0] = 3;
			}
			else {
				dragPts[0] = newPt;
				dragPts[1] = points[1];
				polyObj.NumPoints[0] = 2;
			}
		}
		else if (handle == numPoints - 1) {
			dragPts[0] = points[handle - 1];
			dragPts[1] = newPt;
			if (closed && numPoints != 2) {
				dragPts[2] = points[0];
				polyObj.NumPoints[0] = 3;
			}
			else
				polyObj.NumPoints[0] = 2;
		}
		else {
			dragPts[0] = points[handle - 1];
			dragPts[1] = newPt;
			dragPts[2] = points[handle + 1];
			polyObj.NumPoints[0] = 3;
		}
		PolyDrawOutline(window, &polyObj, 0, 0);
	}
}

/*
 *	Track mouse and change poly shape
 */

void PolyGrow(WindowPtr window, PolyObjPtr polyObj, UWORD modifier, WORD handle)
{
	WORD	 	pathNum, ptNum;
	PointPtr	points;
	DocDataPtr 	docData = GetWRefCon(window);
	Point		pt1, pt2;

	growPolyObj = polyObj;
	growHandle = handle;
	GetPathPoint(polyObj, growHandle, &pathNum, &ptNum);
	points = polyObj->Points[pathNum];
/*
	Track handle movement
*/
	pt1 = points[ptNum];
	OffsetPoint(&pt1, polyObj->DocObj.Frame.MinX, polyObj->DocObj.Frame.MinY);
	pt2 = pt1;
	SnapToGrid(docData, &pt2.x, &pt2.y);
	TrackMouse(window, modifier, &pt2, PolyTrackGrow, NULL);
/*
	Set new object dimensions
*/
	if (!EqualPt(&pt1, &pt2)) {
		InvalObjectRect(window, polyObj);
		HiliteSelectOff(window);
		OffsetPoint(&pt2, -polyObj->DocObj.Frame.MinX, -polyObj->DocObj.Frame.MinY);
		points[ptNum] = pt2;
		PolyAdjustFrame(polyObj);
		HiliteSelectOn(window);
		InvalObjectRect(window, polyObj);
	}
}

/*
 *	Draw poly object to PostScript file
 */

BOOL PolyDrawPSObj(PrintRecPtr printRec, PolyObjPtr polyObj, RectPtr rect, RectPtr clipRect)
{
	WORD 		i, numPoints[MAX_MERGE_POLY];
	BOOL 		hasPen, hasFill, success;
	Point 		pen;
	PointPtr 	newPts[MAX_MERGE_POLY], points;
	UWORD		pathNum;

	for (pathNum = 0; pathNum < MAX_MERGE_POLY; pathNum++)
		newPts[pathNum] = NULL;

	hasPen = HasPen(polyObj);
	hasFill = HasFill(polyObj);
	success = TRUE;
	if (hasPen || hasFill) {
		if (hasFill) {
			if (!PSSetFill(printRec, &polyObj->FillPat))
				return (FALSE);
		}
		if (hasPen) {
			pen.x = polyObj->PenWidth;
			pen.y = polyObj->PenHeight;
			PSScalePt(&pen, &polyObj->DocObj.Frame, rect);
			if (!PSSetPen(printRec, &pen, polyObj->PenColor))
				return (FALSE);
		}
		success = FALSE;
		for (pathNum = 0; pathNum < polyObj->NumPaths; pathNum++) {
			numPoints[pathNum] = polyObj->NumPoints[pathNum];
			if ((newPts[pathNum] = MemAlloc(numPoints[pathNum]*sizeof(Point), 0)) == NULL)
				goto Exit;
			BlockMove(polyObj->Points[pathNum], newPts[pathNum], polyObj->NumPoints[pathNum]*sizeof(Point));
			points = newPts[pathNum];
			for (i = 0; i < numPoints[pathNum]; i++) {
				OffsetPoint(&points[i], polyObj->DocObj.Frame.MinX, polyObj->DocObj.Frame.MinY);
				MapPt(&points[i], &polyObj->DocObj.Frame, rect);
			}
		}
		success = PSPoly(printRec, polyObj->NumPaths, numPoints, newPts, hasPen, hasFill,
						 PolyClosed(polyObj), PolySmoothed(polyObj));
	}
Exit:
	for (pathNum = 0; pathNum < polyObj->NumPaths; pathNum++)
		if (newPts[pathNum])
			MemFree(newPts[pathNum], numPoints[pathNum] * sizeof(Point));

	return (success);
}

/*
 *	Create new poly given REXX arguments
 */

PolyObjPtr PolyNewREXX(DocDataPtr docData, TextPtr args)
{
	PolyObjPtr	polyObj;
	Point		pt;

/*
	Create new poly
*/
	if ((polyObj = CreatePoly(docData)) == NULL)
		return (NULL);
/*
	Get poly points
*/
	for (;;) {
		args = GetArgPoint(args, docData, &pt);
		if (args == NULL)
			break;
		if (pt.x < 0 || pt.y < 0 || pt.x >= docData->DocWidth || pt.y >= docData->DocHeight) {
			DetachObject(CurrLayer(docData), polyObj);
			PolyDispose(polyObj);
			return (NULL);
		}
		PolyAddPoint(polyObj, -1, (polyObj->NumPaths) ? polyObj->NumPoints[0] - 1 : -1,
							 pt.x, pt.y);
	}
	SetPolyFrame(polyObj);
/*
	Return poly
*/
	if (polyObj->NumPoints[0] < 2) {
		DetachObject(CurrLayer(docData), polyObj);
		PolyDispose(polyObj);
		polyObj = NULL;
	}
	return (polyObj);
}
