/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Toolbox library
 *	Copyright (c) 1991-93 New Horizons Software, Inc.
 *
 *	Gadget routines
 */

#define INTUI_V36_NAMES_ONLY	1

#include <exec/types.h>
#include <intuition/intuition.h>
#include <intuition/sghooks.h>

#include <string.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/layers.h>
#include <proto/intuition.h>
#include <proto/dos.h>			// For Delay() prototype

#include <Typedefs.h>

#include <Toolbox/Globals.h>
#include <Toolbox/Memory.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Border.h>
#include <Toolbox/Image.h>
#include <Toolbox/IntuiText.h>
#include <Toolbox/PopUpList.h>
#include <Toolbox/Gadget.h>
#include <Toolbox/ColorPick.h>
#include <Toolbox/Utility.h>

/*
 *	External variables
 */

extern struct Library	*IntuitionBase;

/*
 *	Local variables and definitions
 */

typedef struct {
	Gadget		Gadget;
	UBYTE		Type, Number, Value, KeyEquiv;
	UWORD		Flags;
	Ptr			Extra;
} GadgetX, *GadgetXPtr;

#define BTN_ROUND	2		// Amount of corner rounding

enum {						// For DrawButtonBox()
	BTN_OFF, BTN_IN, BTN_OUT
};

enum {				// For check boxes and radio buttons
	STATE_OFF, STATE_OFFSEL, STATE_ON, STATE_ONSEL
};

#define SELECTBUTTON(q)	\
	((q&IEQUALIFIER_LEFTBUTTON)||((q&AMIGAKEYS)&&(q&IEQUALIFIER_LALT)))

#define SCALE(x, src, dst)	(((LONG) (x)*(dst) + (src)/2)/(src))

static TextChar	textUndoBuff[GADG_MAX_STRING];	// Undo buff for all EDIT_TEXT gadgets

static struct StringExtend stringExtend;		// Initialized to all 0's

/*
 *	Local prototypes
 */

static BOOL	AddGadgText(GadgetPtr, TextPtr, WORD, WORD, WORD);
static BOOL	SetGadgetIntuiText(GadgetPtr, TextPtr, WORD);

static void	SetKeyEquivText(GadgetPtr);

static void	DrawButtonBox(RastPtr, RectPtr, WORD, BOOL);
static void	DrawPushButton(ImagePtr, TextPtr, TextChar, BOOL);
static BOOL	MakePushButton(GadgetPtr, TextPtr);
static void	DisposePushButton(GadgetPtr);

static void	DrawCheckBox(ImagePtr, WORD);
static BOOL	MakeCheckBox(GadgetPtr, TextPtr);
static void	DisposeCheckBox(GadgetPtr);

static void	DrawRadioButton(ImagePtr, WORD);
static BOOL	MakeRadioButton(GadgetPtr, TextPtr);
static void	DisposeRadioButton(GadgetPtr);

static BOOL	MakeSpecialBtn(GadgetPtr, TextPtr, BOOL);
static void	DisposeSpecialBtn(GadgetPtr);

static void	DrawPopUp(ImagePtr, TextPtr, BOOL);
static BOOL	MakePopUp(GadgetXPtr, TextPtr []);
static void	DisposePopUp(GadgetPtr);

static void EraseGadgetText(GadgetPtr, WindowPtr, RequestPtr);

/*
 *	Add or append intuiText to gadget
 */

static BOOL AddGadgText(GadgetPtr gadget, TextPtr text, WORD len, WORD left, WORD top)
{
	TextChar		ch;
	IntuiTextPtr	intuiText;

	if (text) {
		ch = text[len];
		text[len] = '\0';
	}
	intuiText = NewIntuiText(left, top, text, FS_NORMAL, _tbPenBlack);
	if (text)
		text[len] = ch;
	if (intuiText == NULL)
		return (FALSE);
	if (gadget->GadgetText)
		AppendIntuiText(gadget->GadgetText, intuiText);
	else
		gadget->GadgetText = intuiText;
	return (TRUE);
}

/*
 *	Set gadget text, making new lines for each line feed character
 */

static BOOL SetGadgetIntuiText(GadgetPtr gadget, TextPtr text, WORD left)
{
	register WORD	len, top;

	if (text) {
		len = top = 0;
		for (;;) {
			if (text[len] != '\n' && text[len] != '\0')
				len++;
			else {
				if (!AddGadgText((GadgetPtr) gadget, text, len, left, top))
					return (FALSE);
				if (text[len] == '\0')
					break;
				text += len + 1;
				len = 0;
				top += _tbYSize + 2;
			}
		}
	}
	else {
		if (!AddGadgText((GadgetPtr) gadget, NULL, 0, left, 0))
			return (FALSE);
	}
	return (TRUE);
}

/*
 *	Add marker for gadget key equivalent
 */

static void SetKeyEquivText(GadgetPtr gadget)
{
	WORD			barWidth, charWidth;
	TextChar		keyEquiv;
	TextPtr			text;
	IntuiTextPtr	intuiText, newIntuiText;

	keyEquiv = toUpper[((GadgetXPtr) gadget)->KeyEquiv];
	if (keyEquiv == '\0')
		return;
	intuiText = gadget->GadgetText;
	newIntuiText = NewIntuiText(intuiText->LeftEdge, intuiText->TopEdge + 1,
								"_", FS_NORMAL, intuiText->FrontPen);
	if (newIntuiText == NULL)
		return;
	for (text = intuiText->IText; *text; text++) {
		if (toUpper[*text] == keyEquiv)
			break;
	}
	if (*text) {
		barWidth = IntuiTextLength(newIntuiText);
		newIntuiText->IText[0] = *text;
		*text = '\0';
		charWidth = IntuiTextLength(newIntuiText);
		newIntuiText->LeftEdge += IntuiTextLength(intuiText) + (charWidth - barWidth)/2;
		*text = newIntuiText->IText[0];
		newIntuiText->IText[0] = '_';
		AppendIntuiText(intuiText, newIntuiText);
	}
	else
		FreeIntuiText(newIntuiText);
}

/*
 *	Draw rounded shadow box, optionally filling with _tbPenDark
 */

static void DrawButtonBox(RastPtr rPort, register RectPtr rect, WORD state, BOOL fill)
{
	PenNum	penBlack, penWhite, oldPen;
	Point	ptList[9];

	oldPen = rPort->FgPen;
	ptList[0].x = rect->MinX + BTN_ROUND;	ptList[0].y = rect->MaxY;
	ptList[1].x = rect->MinX;				ptList[1].y = rect->MaxY - BTN_ROUND;
	ptList[2].x = rect->MinX;				ptList[2].y = rect->MinY + BTN_ROUND;
	ptList[3].x = rect->MinX + BTN_ROUND;	ptList[3].y = rect->MinY;
	ptList[4].x = rect->MaxX - BTN_ROUND;	ptList[4].y = rect->MinY;
	ptList[5].x = rect->MaxX;				ptList[5].y = rect->MinY + BTN_ROUND;
	ptList[6].x = rect->MaxX;				ptList[6].y = rect->MaxY - BTN_ROUND;
	ptList[7].x = rect->MaxX - BTN_ROUND;	ptList[7].y = rect->MaxY;
	ptList[8].x = rect->MinX + BTN_ROUND;	ptList[8].y = rect->MaxY;
	if (fill) {
		SetAPen(rPort, _tbPenDark);
		FillPoly(rPort, 9, ptList);
	}
	penBlack = _tbPenBlack;
	penWhite = (!_tbNoShadows) ? _tbPenWhite : _tbPenBlack;
	if (state == BTN_OFF)
		SetAPen(rPort, _tbPenLight);
	else
		SetAPen(rPort, (state == BTN_IN) ? penBlack : penWhite);
	FramePoly(rPort, 6, &ptList[0]);
	if (state != BTN_OFF)
		SetAPen(rPort, (state == BTN_IN) ? penWhite : penBlack);
	FramePoly(rPort, 4, &ptList[5]);
	SetAPen(rPort, oldPen);
}

/*
 *	Draw push button in normal or selected state
 */

static void DrawPushButton(ImagePtr image, TextPtr text, TextChar keyEquiv,
						   BOOL selected)
{
	register WORD		i, planeSize;
	WORD				width, height, depth, len, top, left, keyOffset, boxType;
	register PLANEPTR	imageData;
	TextFontPtr			font;
	Rectangle			rect;
	RastPort			rPort;
	BitMap				bitMap;

	width	= image->Width;
	height	= image->Height;
	depth	= image->Depth;
/*
	Initialize bitMap and rPort for drawing
*/
	InitBitMap(&bitMap, depth, width, height);
	InitRastPort(&rPort);
	rPort.BitMap = &bitMap;
	imageData = (PLANEPTR) image->ImageData;
	planeSize = RASSIZE((LONG) width, (LONG) height);
	for (i = 0; i < depth; i++)
		bitMap.Planes[i] = imageData + i*planeSize;
/*
	Draw image
*/
	SetRast(&rPort, _tbPenLight);
	SetRect(&rect, 0, 0, width - 1, height - 1);
	boxType = (selected) ? BTN_IN : BTN_OUT;
	DrawButtonBox(&rPort, &rect, boxType, selected && _tbNoShadows);
	if (!_tbNoShadows) {
		InsetRect(&rect, 1, 0);
		DrawButtonBox(&rPort, &rect, boxType, FALSE);
		InsetRect(&rect, 0, 1);
		DrawButtonBox(&rPort, &rect, boxType, selected);
	}
/*
	Draw text
*/
	if ((font = OpenFont(&_tbTextAttr)) != NULL) {
		SetFont(&rPort, font);
		len = strlen(text);
		top  = (height - _tbYSize + 1)/2 + font->tf_Baseline;
		left = (width - TextLength(&rPort, text, len))/2;
		if (left < 2) {
			rPort.TxSpacing = -1;
			left += len/2;
		}
		SetAPen(&rPort, (selected) ? _tbPenWhite : _tbPenBlack);
		SetDrMd(&rPort, JAM1);
		Move(&rPort, left, top);
		Text(&rPort, text, len);
/*
	Underline key equivalent
*/
		for (i = 0; i < len; i++) {
			if (toUpper[text[i]] == toUpper[keyEquiv])
				break;
		}
		if (i < len) {
			keyOffset = TextLength(&rPort, text, i) +
						(TextLength(&rPort, text + i, 1) - TextLength(&rPort, "_", 1))/2;
			Move(&rPort, left + keyOffset, top + 1);
			Text(&rPort, "_", 1);
		}
		WaitBlit();
		CloseFont(font);
	}
}

/*
 *	Make push button gadget
 */

static BOOL MakePushButton(GadgetPtr gadget, TextPtr text)
{
	WORD		width, height, depth;
	TextChar	keyEquiv;
	BOOL		success;

	success = FALSE;
	width  = gadget->Width;
	height = gadget->Height;
	depth = _tbScreen->RastPort.BitMap->Depth;
/*
	Get images
*/
	if ((gadget->GadgetRender = NewImage(width, height, depth)) == NULL ||
		(gadget->SelectRender = NewImage(width, height, depth)) == NULL)
		goto Exit;
/*
	Draw normal and selected images
*/
	keyEquiv = ((GadgetXPtr) gadget)->KeyEquiv;
	DrawPushButton(gadget->GadgetRender, text, keyEquiv, FALSE);
	DrawPushButton(gadget->SelectRender, text, keyEquiv, TRUE);
/*
	Set gadget data
*/
	gadget->Flags		|= GFLG_GADGHIMAGE | GFLG_GADGIMAGE;
	gadget->Activation	|= GACT_IMMEDIATE | GACT_RELVERIFY;
	gadget->GadgetType	|= GTYP_BOOLGADGET;
/*
	All done
*/
	success = TRUE;
Exit:
	return (success);
}

/*
 *	Dispose of push button data
 */

static void DisposePushButton(GadgetPtr gadget)
{
	FreeImage(gadget->GadgetRender);
	FreeImage(gadget->SelectRender);
	gadget->GadgetRender = gadget->SelectRender = NULL;
}

/*
 *	Draw check box state into image
 */

static void DrawCheckBox(ImagePtr image, WORD state)
{
	register WORD		i, planeSize, width, height, depth;
	PenNum				pen;
	register PLANEPTR	imageData;
	RastPort			rPort;
	BitMap				bitMap;
	Rectangle			rect;

	width	= image->Width;
	height	= image->Height;
	depth	= image->Depth;
/*
	Initialize bitMap and rPort for drawing
*/
	InitBitMap(&bitMap, depth, width, height);
	InitRastPort(&rPort);
	rPort.BitMap = &bitMap;
	imageData = (PLANEPTR) image->ImageData;
	planeSize = RASSIZE((LONG) width, (LONG) height);
	for (i = 0; i < depth; i++)
		bitMap.Planes[i] = imageData + i*planeSize;
/*
	Draw into rPort
*/
	switch (state) {
	case STATE_OFF:
	case STATE_OFFSEL:
		pen =_tbPenLight;
		break;
	case STATE_ON:
	case STATE_ONSEL:
		pen = _tbPenRed;
		break;
	}
	SetRast(&rPort, pen);
	SetRect(&rect, 0, 0, width - 1, height - 1);
	switch (state) {
	case STATE_OFF:
	case STATE_ON:
		DrawShadowBox(&rPort, &rect, 0, state == STATE_OFF);
		break;
	case STATE_OFFSEL:
	case STATE_ONSEL:
		DrawShadowBox(&rPort, &rect, 0, FALSE);
		DrawShadowBox(&rPort, &rect, 1, FALSE);
		break;
	}
}

/*
 *	Make check box gadget
 */

static BOOL MakeCheckBox(GadgetPtr gadget, TextPtr text)
{
	return (MakeSpecialBtn(gadget, text, FALSE));
}

/*
 *	Dipose of check box data
 */

static void DisposeCheckBox(GadgetPtr gadget)
{
	DisposeSpecialBtn(gadget);
}

/*
 *	Draw radio button state into image
 */

static void DrawRadioButton(ImagePtr image, WORD state)
{
	register WORD		i, planeSize, width, height, depth;
	register PLANEPTR	imageData;
	PenNum				penBlack, penWhite;
	RastPort			rPort;
	BitMap				bitMap;
	Point				ptList[4];

	width	= image->Width;
	height	= image->Height;
	depth	= image->Depth;
/*
	Initialize bitMap and rPort for drawing
*/
	InitBitMap(&bitMap, depth, width, height);
	InitRastPort(&rPort);
	rPort.BitMap = &bitMap;
	imageData = (PLANEPTR) image->ImageData;
	planeSize = RASSIZE((LONG) width, (LONG) height);
	for (i = 0; i < depth; i++)
		bitMap.Planes[i] = imageData + i*planeSize;
/*
	Draw into rPort
*/
	SetRast(&rPort, _tbPenLight);
	ptList[0].x = width - 1;	ptList[0].y = (height - 1)/2;
	ptList[1].x = 0;			ptList[1].y = 0;
	ptList[2].x = 0;			ptList[2].y = height - 1;
	ptList[3].x = width - 1;	ptList[3].y = (height)/2;
	if (state == STATE_ON || state == STATE_ONSEL) {
		SetAPen(&rPort, _tbPenRed);
		FillPoly(&rPort, 4, ptList);
	}
	penBlack = _tbPenBlack;
	penWhite = (!_tbNoShadows) ? _tbPenWhite : _tbPenBlack;
	SetAPen(&rPort, (state == STATE_OFF) ? penBlack : penWhite);
	FramePoly(&rPort, 2, &ptList[2]);
	SetAPen(&rPort, (state == STATE_OFF) ? penWhite : penBlack);
	FramePoly(&rPort, 3, &ptList[0]);
	if (state == STATE_OFFSEL || state == STATE_ONSEL) {
		for (i = 0; i < 4; i++) {
			if (ptList[i].x == 0)
				ptList[i].x++;
			else if (ptList[i].x == width - 1)
				ptList[i].x--;
			if (ptList[i].y == 0)
				ptList[i].y++;
			else if (ptList[i].y == height - 1)
				ptList[i].y--;
		}
		SetAPen(&rPort, penWhite);
		FramePoly(&rPort, 2, &ptList[2]);
		SetAPen(&rPort, penBlack);
		FramePoly(&rPort, 3, &ptList[0]);
	}
}

/*
 *	Make radio button gadget
 */

static BOOL MakeRadioButton(GadgetPtr gadget, TextPtr text)
{
	return (MakeSpecialBtn(gadget, text, TRUE));
}

/*
 *	Dispose of radio button data
 */

static void DisposeRadioButton(GadgetPtr gadget)
{
	DisposeSpecialBtn(gadget);
}

/*
 *	Make check box or radio button gadget
 */

static BOOL MakeSpecialBtn(GadgetPtr gadget, TextPtr text, BOOL radBtn)
{
	WORD	boxWidth, boxHeight, depth;
	BOOL	success;

	success = FALSE;
	gadget->Height	 = boxHeight = _tbYSize;
	boxWidth		 = SCALE(_tbXSize, 8, 12);
	depth = _tbScreen->RastPort.BitMap->Depth;
/*
	Get font, rPort, and image structures
*/
	if ((gadget->GadgetRender = NewImage(boxWidth, boxHeight, depth)) == NULL ||
		(gadget->SelectRender = NewImage(boxWidth, boxHeight, depth)) == NULL)
		goto Exit;
/*
	Draw "off" state images
*/
	if (radBtn) {
		DrawRadioButton(gadget->GadgetRender, STATE_OFF);
		DrawRadioButton(gadget->SelectRender, STATE_OFFSEL);
	}
	else {
		DrawCheckBox(gadget->GadgetRender, STATE_OFF);
		DrawCheckBox(gadget->SelectRender, STATE_OFFSEL);
	}
/*
	Set gadget data
*/
	gadget->Flags		|= GFLG_GADGHIMAGE | GFLG_GADGIMAGE;
	gadget->Activation	|= GACT_IMMEDIATE | GACT_RELVERIFY;
	gadget->GadgetType	|= GTYP_BOOLGADGET;
	if (!SetGadgetIntuiText(gadget, text, _tbXSize*2))
		goto Exit;
	SetKeyEquivText(gadget);
/*
	Auto-calculate gadget width if width of 0 is passed
*/
	if (gadget->GadgetText->IText && gadget->Width == 0)
		gadget->Width = IntuiTextLength(gadget->GadgetText) + _tbXSize*2;
/*
	All done
*/
	success = TRUE;
Exit:
	return (success);
}

/*
 *	Dispose of check box or radio button
 */

static void DisposeSpecialBtn(GadgetPtr gadget)
{
	FreeImage(gadget->GadgetRender);
	FreeImage(gadget->SelectRender);
	gadget->GadgetRender = gadget->SelectRender = NULL;
	FreeIntuiText(gadget->GadgetText);
	gadget->GadgetText = NULL;
}

/*
 *	Draw pop-up gadget in normal or selected state
 */

static void DrawPopUp(ImagePtr image, TextPtr text, BOOL selected)
{
	register WORD		i, planeSize;
	WORD				width, height, depth, boxWidth, left, yLine;
	register PLANEPTR	imageData;
	TextFontPtr			font;
	RastPort			rPort;
	BitMap				bitMap;
	Point				ptList[4];
	Rectangle			rect;

	width	= image->Width;
	height	= image->Height;
	depth	= image->Depth;
	boxWidth = SCALE(_tbXSize, 8, 18);
	left = width - boxWidth;
/*
	Initialize bitMap and rPort for drawing
*/
	InitBitMap(&bitMap, depth, width, height);
	InitRastPort(&rPort);
	rPort.BitMap = &bitMap;
	imageData = (PLANEPTR) image->ImageData;
	planeSize = RASSIZE((LONG) width, (LONG) height);
	for (i = 0; i < depth; i++)
		bitMap.Planes[i] = imageData + i*planeSize;
/*
	Draw arrow image and shadow boxes
*/
	SetRast(&rPort, (selected) ? _tbPenDark : _tbPenLight);
	ptList[0].x = left + (boxWidth - 1)/2 + 1;
	ptList[1].x = left + boxWidth - 4;
	ptList[2].x = left + 3;
	ptList[3].x = left + (boxWidth - 1)/2;
	if (height <= 11) {				// Using 8 point or smaller font
		ptList[0].y = ptList[3].y = height - 5;
		ptList[1].y = ptList[2].y = 2;
		yLine = height - 3;
	}
	else {
		ptList[0].y = ptList[3].y = height - 6;
		ptList[1].y = ptList[2].y = 3;
		yLine = height - 4;
	}
	SetAPen(&rPort, (selected) ? _tbPenWhite : _tbPenBlack);
	FillPoly(&rPort, 4, ptList);
	Move(&rPort, left + 3, yLine);
	Draw(&rPort, left + boxWidth - 4, yLine);
	SetRect(&rect, 0, 0, left - 1, height - 1);
	DrawShadowBox(&rPort, &rect, 0, !selected);
	SetRect(&rect, left, 0, width - 1, height - 1);
	DrawShadowBox(&rPort, &rect, 0, !selected);
/*
	Draw text
*/
	if ((font = OpenFont(&_tbTextAttr)) != NULL) {
		SetFont(&rPort, font);
		SetAPen(&rPort, (selected) ? _tbPenWhite : _tbPenBlack);
		SetDrMd(&rPort, JAM1);
		Move(&rPort, 3, 2 + font->tf_Baseline);
		TextInWidth(&rPort, text, strlen(text), left - 5, FALSE);
		WaitBlit();
		CloseFont(font);
	}
}

/*
 *	Make pop-up list gadget
 */

static BOOL MakePopUp(GadgetXPtr gadget, TextPtr items[])
{
	WORD		width, height, depth, numItems;
	BOOL		success;
	ImagePtr	image;

	success = FALSE;
	width = gadget->Gadget.Width;
	gadget->Gadget.Height = height = _tbYSize;
	depth = _tbScreen->RastPort.BitMap->Depth;
/*
	Create pop-up list
*/
	for (numItems = 0; items[numItems]; numItems++) ;
	if ((gadget->Extra = CreatePopUpList(numItems, items)) == NULL)
		goto Exit;
/*
	Get images
*/
	if ((image = NewImage(width + 2, height + 3, depth)) == NULL)
		goto Exit;
	image->LeftEdge = image->TopEdge = -2;
	gadget->Gadget.GadgetRender = image;
/*
	Draw images
*/
	DrawPopUp(gadget->Gadget.GadgetRender, items[gadget->Value], FALSE);
/*
	Set gadget data
*/
	gadget->Gadget.Flags		|= GFLG_GADGHNONE | GFLG_GADGIMAGE;
	gadget->Gadget.Activation	|= GACT_IMMEDIATE;
	gadget->Gadget.GadgetType	|= GTYP_BOOLGADGET;
/*
	All done
*/
	success = TRUE;
Exit:
	return (TRUE);
}

/*
 *	Dispose of pop-up list
 */

static void DisposePopUp(GadgetPtr gadget)
{
	FreeImage(gadget->GadgetRender);
	gadget->GadgetRender = NULL;
	DisposePopUpList(((GadgetXPtr) gadget)->Extra);
	((GadgetXPtr) gadget)->Extra = NULL;
}

/*
 *	GetGadgets
 *	Process gadgets template and return pointer to gadget list
 *	Return NULL if error
 */

GadgetPtr GetGadgets(GadgetTemplate gadgTempl[])
{
	register WORD			item, type, flags, width, height, left, top;
	Ptr						gadgInfo;
	register GadgetXPtr	gadget, firstGadg, prevGadg;
	StrInfoPtr				stringInfo;
	PropInfoPtr				propInfo;
	BorderPtr				border;

/*
	Process gadget templates
*/
	firstGadg = prevGadg = NULL;
	for (item = 0; gadgTempl[item].Type != GADG_ITEM_NONE; item++) {
		if ((gadget = MemAlloc(sizeof(GadgetX), MEMF_CLEAR)) == NULL) {
			DisposeGadgets((GadgetPtr) firstGadg);
			return (NULL);
		}
		if (firstGadg == NULL)
			firstGadg = gadget;
		if (prevGadg)
			prevGadg->Gadget.NextGadget = (GadgetPtr) gadget;
/*
	Assign common parameters
*/
		gadget->Gadget.LeftEdge		= left		=
			SCALE(gadgTempl[item].LeftEdge, 8, _tbXSize) + gadgTempl[item].LeftOffset;
		gadget->Gadget.TopEdge		= top		=
			SCALE(gadgTempl[item].TopEdge, 11, _tbYSize) + gadgTempl[item].TopOffset;
		gadget->Gadget.Width		= width		=
			SCALE(gadgTempl[item].Width, 8, _tbXSize)    + gadgTempl[item].WidthOffset;
		gadget->Gadget.Height		= height	=
			SCALE(gadgTempl[item].Height, 11, _tbYSize)  + gadgTempl[item].HeightOffset;
		gadget->Type	= type = (gadgTempl[item].Type & GADG_TYPEBITS);
		flags			= (gadgTempl[item].Type & GADG_FLAGBITS);
		gadget->Number	= item;
		gadget->Value	= gadgTempl[item].Value;
		gadget->KeyEquiv= gadgTempl[item].KeyEquiv;
		gadgInfo		= gadgTempl[item].Info;
		if (gadget->Gadget.LeftEdge < 0)
			gadget->Gadget.Flags |= GFLG_RELRIGHT;
		if (gadget->Gadget.TopEdge < 0)
			gadget->Gadget.Flags |= GFLG_RELBOTTOM;
		if (gadget->Gadget.Width < 0)
			gadget->Gadget.Flags |= GFLG_RELWIDTH;
		if (gadget->Gadget.Height < 0)
			gadget->Gadget.Flags |= GFLG_RELHEIGHT;
/*
	Find and process the specified gadget type
*/
		switch (type) {
/*
	User Item
*/
		case GADG_USER_ITEM:
			if (firstGadg == gadget)
				firstGadg = gadgInfo;
			if (prevGadg)
				prevGadg->Gadget.NextGadget = gadgInfo;
			MemFree(gadget, sizeof(GadgetX));
			gadget = (GadgetXPtr) gadgInfo;
			break;
/*
	Push Button
*/
		case GADG_PUSH_BUTTON:
			if (!MakePushButton((GadgetPtr) gadget, gadgInfo)) {
				DisposeGadgets((GadgetPtr) firstGadg);
				return (NULL);
			}
			break;
/*
	Check Box
*/
		case GADG_CHECK_BOX:
			if (!MakeCheckBox((GadgetPtr) gadget, gadgInfo)) {
				DisposeGadgets((GadgetPtr) firstGadg);
				return (NULL);
			}
			break;
/*
	Radio Button
*/
		case GADG_RADIO_BUTTON:
			if (!MakeRadioButton((GadgetPtr) gadget, gadgInfo)) {
				DisposeGadgets((GadgetPtr) firstGadg);
				return (NULL);
			}
			break;
/*
	Static Text
*/
		case GADG_STAT_TEXT:
			gadget->Gadget.Flags		|= GFLG_GADGHNONE;
			gadget->Gadget.GadgetType	|= GTYP_BOOLGADGET;
			if (!SetGadgetIntuiText((GadgetPtr) gadget, (TextPtr) gadgInfo, 0)) {
				DisposeGadgets((GadgetPtr) firstGadg);
				return (NULL);
			}
			break;
/*
	Edit Text
*/
		case GADG_EDIT_TEXT:
			gadget->Gadget.Flags		|= GFLG_GADGHCOMP;
			gadget->Gadget.Activation	|= GACT_IMMEDIATE | GACT_RELVERIFY;
			gadget->Gadget.GadgetType	|= GTYP_STRGADGET;
			gadget->Gadget.Height		 = height = _tbYSize;
			if ((gadget->Gadget.GadgetRender = ShadowBoxBorder(width, height, -2, FALSE)) == NULL) {
				DisposeGadgets((GadgetPtr) firstGadg);
				return (NULL);
			}
			if (!_tbNoShadows && (border = ShadowBoxBorder(width, height, -3, TRUE)) != NULL)
				AppendBorder(gadget->Gadget.GadgetRender, border);
			if ((stringInfo = MemAlloc(sizeof(StringInfo), MEMF_CLEAR)) == NULL) {
				DisposeGadgets((GadgetPtr) firstGadg);
				return (NULL);
			}
			gadget->Gadget.SpecialInfo = stringInfo;
			stringInfo->MaxChars = GADG_MAX_STRING;
			if ((gadgInfo && strlen((BYTE *) gadgInfo) >= GADG_MAX_STRING) ||
				(stringInfo->Buffer = MemAlloc(GADG_MAX_STRING, 0)) == NULL) {
				DisposeGadgets((GadgetPtr) firstGadg);
				return (NULL);
			}
			stringInfo->UndoBuffer = textUndoBuff;
			if (gadgInfo)
				strcpy(stringInfo->Buffer, (BYTE *) gadgInfo);
			else
				stringInfo->Buffer[0] = '\0';
			if (flags & GADG_EDIT_NUMONLY)
				gadget->Gadget.Activation	|= GACT_LONGINT;
			if (flags & GADG_EDIT_RETURNCYCLE)
				gadget->Flags = GADG_EDIT_RETURNCYCLE;
			if (LibraryVersion(IntuitionBase) >= OSVERSION_2_0) {
				gadget->Gadget.Flags	|= GFLG_TABCYCLE | GFLG_STRINGEXTEND;
				stringInfo->Extension = &stringExtend;
				stringExtend.Pens[0] = _tbPenBlack;
				stringExtend.Pens[1] = _tbPenLight;
				stringExtend.ActivePens[0] = _tbPenWhite;
				stringExtend.ActivePens[1] = _tbPenBlack;
			}
			break;
/*
	Border item
*/
		case GADG_STAT_BORDER:
		case GADG_ACTIVE_BORDER:
			gadget->Gadget.Flags		|= GFLG_GADGHNONE;
			if (type == GADG_ACTIVE_BORDER)
				gadget->Gadget.Activation	|= GACT_IMMEDIATE | GACT_RELVERIFY;
			gadget->Gadget.GadgetType	|= GTYP_BOOLGADGET;
			gadget->Gadget.GadgetRender	 = gadgInfo;
			break;
/*
	Image item
*/
		case GADG_STAT_IMAGE:
		case GADG_ACTIVE_IMAGE:
			if (type == GADG_ACTIVE_IMAGE) {
				gadget->Gadget.Flags		|= GFLG_GADGHCOMP | GFLG_GADGIMAGE;
				gadget->Gadget.Activation	|= GACT_IMMEDIATE | GACT_RELVERIFY;
			}
			else
				gadget->Gadget.Flags		|= GFLG_GADGHNONE | GFLG_GADGIMAGE;
			gadget->Gadget.GadgetType		|= GTYP_BOOLGADGET;
			gadget->Gadget.GadgetRender	 = gadgInfo;
			break;
/*
	Standard border
*/
		case GADG_STAT_STDBORDER:
		case GADG_ACTIVE_STDBORDER:
			gadget->Gadget.Flags		|= GFLG_GADGHNONE;
			if (type == GADG_ACTIVE_STDBORDER)
				gadget->Gadget.Activation	|= GACT_IMMEDIATE | GACT_RELVERIFY;
			gadget->Gadget.GadgetType	|= GTYP_BOOLGADGET;
			gadget->Gadget.GadgetRender	 = GetStdBorder((WORD) gadgInfo, width, height);
			break;
/*
	Standard Image item
*/
		case GADG_STAT_STDIMAGE:
		case GADG_ACTIVE_STDIMAGE:
			if (type == GADG_ACTIVE_STDIMAGE) {
				gadget->Gadget.Flags		|= GFLG_GADGHIMAGE | GFLG_GADGIMAGE;
				gadget->Gadget.Activation	|= GACT_IMMEDIATE | GACT_RELVERIFY;
			}
			else
				gadget->Gadget.Flags		|= GFLG_GADGHNONE | GFLG_GADGIMAGE;
			gadget->Gadget.GadgetType		|= GTYP_BOOLGADGET;
			gadget->Gadget.GadgetRender		 = GetStdImage((WORD) gadgInfo, width, height, FALSE);
			if (type == GADG_ACTIVE_STDIMAGE)
				gadget->Gadget.SelectRender	 = GetStdImage((WORD) gadgInfo, width, height, TRUE);
			break;
/*
	Proportional gadgets
*/
		case GADG_PROP_VERT:
		case GADG_PROP_HORIZ:
			gadget->Gadget.Flags		|= GFLG_GADGHNONE | GFLG_GADGIMAGE;
			gadget->Gadget.Activation	|= GACT_IMMEDIATE | GACT_RELVERIFY;
			gadget->Gadget.GadgetType	|= GTYP_PROPGADGET;
			if ((gadget->Gadget.GadgetRender = MemAlloc(sizeof(Image), MEMF_CLEAR)) == NULL ||
				(propInfo = MemAlloc(sizeof(PropInfo), MEMF_CLEAR)) == NULL) {
				DisposeGadgets((GadgetPtr) firstGadg);
				return (NULL);
			}
			gadget->Gadget.SpecialInfo = propInfo;
			propInfo->Flags = (type == GADG_PROP_VERT) ? 
							  AUTOKNOB | FREEVERT : AUTOKNOB | FREEHORIZ;
			if ((flags & GADG_PROP_NOBORDER) &&
				(LibraryVersion((struct Library *) IntuitionBase) >= OSVERSION_2_0 ||
				 (_tbScreen->Flags & SCREENTYPE) != WBENCHSCREEN)) {
				propInfo->Flags |= PROPBORDERLESS;
				gadget->Gadget.LeftEdge	+= 1;
				gadget->Gadget.TopEdge	+= 1;
				gadget->Gadget.Width	-= 2;
				gadget->Gadget.Height	-= 2;
			}
			if (flags & GADG_PROP_NEWLOOK)
				propInfo->Flags |= PROPNEWLOOK;
			propInfo->HorizBody = propInfo->VertBody = MAXBODY;
			break;
/*
	Pop-up list gadgets
*/
		case GADG_POPUP:
			if (!MakePopUp(gadget, gadgInfo)) {
				DisposeGadgets((GadgetPtr) firstGadg);
				return (NULL);
			}
			break;
		}
		prevGadg = gadget;
	}
	return ((GadgetPtr) firstGadg);
}

/*
 *	DisposeGadgets
 *	Release all memory used by gadget list created by GetGadgets
 *	Ignores all system gadgets
 */

void DisposeGadgets(register GadgetPtr gadgList)
{
	register GadgetPtr	nextGadget;
	register StrInfoPtr	stringInfo;

	while (gadgList) {
		nextGadget = gadgList->NextGadget;
		if ((gadgList->GadgetType & GTYP_SYSGADGET) == 0) {
			switch (GadgetType(gadgList)) {
			case GADG_USER_ITEM:
				break;
			case GADG_PUSH_BUTTON:
				DisposePushButton(gadgList);
				break;
			case GADG_CHECK_BOX:
				DisposeCheckBox(gadgList);
				break;
			case GADG_RADIO_BUTTON:
				DisposeRadioButton(gadgList);
				break;
			case GADG_STAT_TEXT:
				FreeIntuiText(gadgList->GadgetText);
				break;
			case GADG_EDIT_TEXT:
				FreeBorder(gadgList->GadgetRender);
				if ((stringInfo = gadgList->SpecialInfo) != NULL) {
					if (stringInfo->Buffer)
						MemFree(stringInfo->Buffer, GADG_MAX_STRING);
					MemFree(stringInfo, sizeof(StringInfo));
				}
				break;
			case GADG_STAT_STDBORDER:
			case GADG_ACTIVE_STDBORDER:
				FreeBorder(gadgList->GadgetRender);
				break;
			case GADG_STAT_STDIMAGE:
			case GADG_ACTIVE_STDIMAGE:
				FreeImage(gadgList->GadgetRender);
				FreeImage(gadgList->SelectRender);
				break;
			case GADG_STAT_BORDER:
			case GADG_ACTIVE_BORDER:
			case GADG_STAT_IMAGE:
			case GADG_ACTIVE_IMAGE:
				break;
			case GADG_PROP_VERT:
			case GADG_PROP_HORIZ:
				if (gadgList->GadgetRender)
					MemFree(gadgList->GadgetRender, sizeof(Image));
				if (gadgList->SpecialInfo)
					MemFree(gadgList->SpecialInfo, sizeof(PropInfo));
				break;
			case GADG_POPUP:
				DisposePopUp(gadgList);
				break;
			}
			MemFree(gadgList, sizeof(GadgetX));
		}
		gadgList = nextGadget;
	}
}

/*
 *	GadgetItem
 *	Return a pointer to the gadget identified by its item number
 *	Ignore all system gadgets
 *	If no such gadget then return NULL
 */

GadgetPtr GadgetItem(GadgetPtr gadgList, register WORD item)
{
	register GadgetPtr	gadget;

	for (gadget = gadgList; gadget; gadget = gadget->NextGadget) {
		if ((gadget->GadgetType & GTYP_SYSGADGET) == 0 && GadgetNumber(gadget) == item)
			break;
	}
	return (gadget);
}

/*
 *	GadgetNumber
 *	Return the gadget number of this gadget
 */

WORD GadgetNumber(GadgetPtr gadget)
{
	return ((WORD) ((GadgetXPtr) gadget)->Number);
}

/*
 *	GadgetType
 *	Return the gadget type
 */

WORD GadgetType(GadgetPtr gadget)
{
	return ((WORD) ((GadgetXPtr) gadget)->Type);
}

/*
 *	GetGadgetValue
 *	Get the gadget's value
 *	Valid for gadget types of GADG_CHECK_BOX, GADG_RADIO_BUTTON, and GADG_POPUP only!
 */

WORD GetGadgetValue(register GadgetPtr gadget)
{
	if (gadget == NULL)
		return (0);
	return ((WORD) ((GadgetXPtr) gadget)->Value);
}

/*
 *	SetGadgetValue
 *	Set the gadget's value and redraw the gadget
 *	Valid for gadget types of GADG_CHECK_BOX, GADG_RADIO_BUTTON, and GADG_POPUP only!
 */

void SetGadgetValue(GadgetPtr gadget, WindowPtr window, RequestPtr request, WORD value)
{
	register WORD	gadgType;
	PopUpListPtr	popUp;

	if (gadget && ((GadgetXPtr) gadget)->Value != value) {
		gadgType = GadgetType(gadget);
		switch (gadgType) {
		case GADG_CHECK_BOX:
			if (value) {
				((GadgetXPtr) gadget)->Value = 1;
				DrawCheckBox(gadget->GadgetRender, STATE_ON);
				DrawCheckBox(gadget->SelectRender, STATE_ONSEL);
			}
			else {
				((GadgetXPtr) gadget)->Value = 0;
				DrawCheckBox(gadget->GadgetRender, STATE_OFF);
				DrawCheckBox(gadget->SelectRender, STATE_OFFSEL);
			}
			break;
		case GADG_RADIO_BUTTON:
			if (value) {
				((GadgetXPtr) gadget)->Value = 1;
				DrawRadioButton(gadget->GadgetRender, STATE_ON);
				DrawRadioButton(gadget->SelectRender, STATE_ONSEL);
			}
			else {
				((GadgetXPtr) gadget)->Value = 0;
				DrawRadioButton(gadget->GadgetRender, STATE_OFF);
				DrawRadioButton(gadget->SelectRender, STATE_OFFSEL);
			}
			break;
		case GADG_POPUP:
			popUp = ((GadgetXPtr) gadget)->Extra;
			DrawPopUp(gadget->GadgetRender, GetListItem(popUp->List, value), FALSE);
			((GadgetXPtr) gadget)->Value = value;
			break;
		}
		RefreshGList(gadget, window, request, 1);
	}
}

/*
 *	SetGadgetItemValue
 *	Set gadget value given item number
 */

void SetGadgetItemValue(GadgetPtr gadgList, WORD item, WindowPtr window,
						RequestPtr request, BOOL value)
{
	GadgetPtr	gadget;

	gadget = GadgetItem(gadgList, item);
	SetGadgetValue(gadget, window, request, value);
}

/*
 *	HiliteGadget
 *	Set the gadget's display appearance to selected or not selected
 */

void HiliteGadget(register GadgetPtr gadget, WindowPtr window, RequestPtr request,
				  BOOL hilite)
{
	register WORD	position;
	register UWORD	flags;
	BOOL			wasSelected;
	RastPtr			rPort;
	Rectangle		gadgRect;

	if (request)
		rPort = (request->ReqLayer) ? request->ReqLayer->rp : NULL;
	else
		rPort = window->RPort;
	if (rPort == NULL)
		return;
	wasSelected = (gadget->Flags & GFLG_SELECTED);
	if ((hilite && wasSelected) || (!hilite && !wasSelected))
		return;
	if ((position = RemoveGadget(window, gadget)) == -1)
		return;
	if (hilite)
		gadget->Flags |= GFLG_SELECTED;
	else
		gadget->Flags &= ~GFLG_SELECTED;
	GetGadgetRect(gadget, window, request, &gadgRect);
	flags = gadget->Flags;
	if ((flags & GFLG_GADGHIGHBITS) == GFLG_GADGHCOMP) {
		SetAPen(rPort, _tbPenLight);
		FillRect(rPort, &gadgRect);
	}
	else if ((flags & GFLG_GADGHIGHBITS) == GFLG_GADGHBOX && wasSelected) {
		SetDrMd(rPort, COMPLEMENT);
		RectFill(rPort, gadgRect.MinX - 4, gadgRect.MinY - 2,
						gadgRect.MaxX + 4, gadgRect.MaxY + 2);
	}
	(void) AddGList(window, gadget, position, 1, request);
	RefreshGList(gadget, window, request, 1);
}

/*
 *	GadgetSelected
 *	Return TRUE if gadget is selected
 */

BOOL GadgetSelected(GadgetPtr gadgList, WORD gadgNum)
{
	GadgetPtr	gadget;

	gadget = GadgetItem(gadgList, gadgNum);
	return ((gadget->Flags & GFLG_SELECTED) != 0);
}

/*
 *	Enable/disable gadget list
 *	Waits until gadget are not selected before changing
 */

void OnOffGList(register GadgetPtr gadgList, WindowPtr window, RequestPtr request,
				register WORD num, BOOL on)
{
	register WORD		i, position;
	register BOOL		selected;
	register GadgetPtr	gadget;
	RastPtr				rPort;
	Rectangle			gadgRect;

	if (request)
		rPort = (request->ReqLayer) ? request->ReqLayer->rp : NULL;
	else
		rPort = window->RPort;
	if (rPort == NULL)
		return;
/*
	Scan gadget list and wait until none are selected
*/
	do {
		selected = FALSE;
		for (gadget = gadgList, i = 0; gadget && i < num; gadget = gadget->NextGadget, i++) {
			if (gadget->Flags & GFLG_SELECTED)
				selected = TRUE;
		}
	} while (selected);
/*
	Turn on/off the gadgets
*/
	if ((position = RemoveGList(window, gadgList, num)) == -1)
		return;
	for (gadget = gadgList, i = 0; gadget && i < num; gadget = gadget->NextGadget, i++) {
		if (on)
			gadget->Flags &= ~GFLG_DISABLED;
		else
			gadget->Flags |= GFLG_DISABLED;
		GetGadgetRect(gadget, window, request, &gadgRect);
		SetAPen(rPort, _tbPenLight);
		FillRect(rPort, &gadgRect);
	}
	(void) AddGList(window, gadgList, position, num, request);
	RefreshGList(gadgList, window, request, num);
}

/*
 *	OnGList
 *	Enable the specified number of gadgets
 *	Unlike OnGadget(), this function only refreshes the specified gadgets
 */

void OnGList(GadgetPtr gadgList, WindowPtr window, RequestPtr request, WORD num)
{
	OnOffGList(gadgList, window, request, num, TRUE);
}

/*
 *	OffGList
 *	Disable the specified number of gadgets
 *	Unlike OffGadget(), this function only refreshes the specified gadgets
 */

void OffGList(GadgetPtr gadgList, WindowPtr window, RequestPtr request, WORD num)
{
	OnOffGList(gadgList, window, request, num, FALSE);
}

/*
 *	EnableGadgetItem
 *	Enable or disable the specified gadget
 *	Only refreshes if gadget state changed
 */

void EnableGadgetItem(GadgetPtr gadgList, WORD gadgNum, WindowPtr window,
					  RequestPtr request, BOOL enable)
{
	register GadgetPtr	gadget;

	gadget = GadgetItem(gadgList, gadgNum);
	if (enable) {
		if (gadget->Flags & GFLG_DISABLED)
			OnOffGList(gadget, window, request, 1, TRUE);
	}
	else {
		if ((gadget->Flags & GFLG_DISABLED) == 0)
			OnOffGList(gadget, window, request, 1, FALSE);
	}
}

/*
 *	OutlineButton
 *	Add or remove outlining of specified button
 */

void OutlineButton(GadgetPtr gadget, WindowPtr window, RequestPtr request, BOOL on)
{
	RastPtr		rPort;
	Rectangle	gadgRect;

	if (request)
		rPort = (request->ReqLayer) ? request->ReqLayer->rp : NULL;
	else
		rPort = window->RPort;
	if (rPort == NULL || GadgetType(gadget) != GADG_PUSH_BUTTON)
		return;
	GetGadgetRect(gadget, window, request, &gadgRect);
	InsetRect(&gadgRect, -2, -2);
	DrawButtonBox(rPort, &gadgRect, (on) ? BTN_IN : BTN_OFF, FALSE);
}

/*
 *	GetEditItemText
 *	Get edit text and put in specified buffer
 */

void GetEditItemText(GadgetPtr gadgList, WORD item, TextPtr buff)
{
	register GadgetPtr	gadget;
	StrInfoPtr			stringInfo;

	gadget = GadgetItem(gadgList, item);
	stringInfo = gadget->SpecialInfo;
	strcpy(buff, stringInfo->Buffer);
}

/*
 *	SetEditItemText
 *	Set specified edit text buffer to new contents
 *	If buff is NULL, clear edit text
 */

void SetEditItemText(GadgetPtr gadgList, WORD item, WindowPtr window, RequestPtr request,
					 TextPtr buff)
{
	register WORD		position;
	register GadgetPtr	gadget;
	register StrInfoPtr	stringInfo;
	BOOL				wasSelected;

	gadget = GadgetItem(gadgList, item);
	wasSelected = gadget->Flags & GFLG_SELECTED;
	position = RemoveGadget(window, gadget);
	gadget->Flags &= ~GFLG_SELECTED;			/* Need this for proper redrawing */
	stringInfo = gadget->SpecialInfo;
	if (buff)
		strcpy(stringInfo->Buffer, buff);
	else
		stringInfo->Buffer[0] = '\0';
	stringInfo->BufferPos = stringInfo->DispPos = 0;
	AddGList(window, gadget, position, 1, request);
	RefreshGList(gadget, window, request, 1);
	if (wasSelected) {
		ActivateGadget(gadget, window, request);
		Delay(5);
	}
}

/*
 *	GetGadgetItemText
 *	Get gadget text (not edit text)
 */

void GetGadgetItemText(GadgetPtr gadgList, WORD item, TextPtr text)
{
	IntuiTextPtr	intuiText;
	GadgetPtr		gadget;

	gadget = GadgetItem(gadgList, item);
	intuiText = gadget->GadgetText;
	if (intuiText && intuiText->IText)
		strcpy(text, intuiText->IText);
	else
		*text = '\0';
}

/*
 *	Erase gadget text
 */

static void EraseGadgetText(GadgetPtr gadget, WindowPtr window, RequestPtr request)
{
	register IntuiTextPtr	intuiText;

	if ((intuiText = gadget->GadgetText) != NULL) {
		while (intuiText) {
			intuiText->FrontPen = _tbPenLight;
			intuiText = intuiText->NextText;
		}
		RefreshGList(gadget, window, request, 1);
	}
}

/*
 *	SetGadgetText
 *	Set gadget text (not edit text) to new text
 */

void SetGadgetText(GadgetPtr gadget, WindowPtr window, RequestPtr request, TextPtr text)
{
	WORD			position, left;
	IntuiTextPtr	intuiText;

	if ((intuiText = gadget->GadgetText) != NULL) {
		left = intuiText->LeftEdge;
		EraseGadgetText(gadget, window, request);
	}
	else
		left = 0;
	position = RemoveGadget(window, gadget);
	FreeIntuiText(gadget->GadgetText);
	gadget->GadgetText = NULL;
	SetGadgetIntuiText(gadget, text, left);
	AddGList(window, gadget, position, 1, request);
	RefreshGList(gadget, window, request, 1);
	((GadgetXPtr) gadget)->KeyEquiv = '\0';
}


/*
 *	SetGadgetItemText
 *	Set gadget text, given item number
 */

void SetGadgetItemText(GadgetPtr gadgList, WORD item, WindowPtr window,
					   RequestPtr request, TextPtr text)
{
	SetGadgetText(GadgetItem(gadgList, item), window, request, text);
}

/*
 *	SetButtonItem
 *	Change button text & key equivalent
 */

void SetButtonItem(GadgetPtr gadgList, WORD item, WindowPtr window, RequestPtr request,
				   TextPtr text, TextChar keyEquiv)
{
	WORD		gadgType, position;
	GadgetPtr	gadget;

	gadget = GadgetItem(gadgList, item);
	gadgType = GadgetType(gadget);
/*
	First erase gadget text in check boxes and radio buttons
	Do it this way to also erase key equiv underline
*/
	if (gadgType == GADG_CHECK_BOX || gadgType == GADG_RADIO_BUTTON)
		EraseGadgetText(gadget, window, request);
/*
	Now change gadget
*/
	((GadgetXPtr) gadget)->KeyEquiv = keyEquiv;
	position = RemoveGadget(window, gadget);
	switch (gadgType) {
	case GADG_PUSH_BUTTON:
		DisposePushButton(gadget);
		MakePushButton(gadget, text);
		break;
	case GADG_CHECK_BOX:
		DisposeCheckBox(gadget);
		MakeCheckBox(gadget, text);
		if (((GadgetXPtr) gadget)->Value) {
			DrawCheckBox(gadget->GadgetRender, STATE_ON);
			DrawCheckBox(gadget->SelectRender, STATE_ONSEL);
		}
		break;
	case GADG_RADIO_BUTTON:
		DisposeRadioButton(gadget);
		MakeRadioButton(gadget, text);
		if (((GadgetXPtr) gadget)->Value) {
			DrawRadioButton(gadget->GadgetRender, STATE_ON);
			DrawRadioButton(gadget->SelectRender, STATE_ONSEL);
		}
		break;
	}
	AddGList(window, gadget, position, 1, request);
	RefreshGList(gadget, window, request, 1);
}

/*
 *	GetGadgetRect
 *	Return rectangle containing gadget
 */

void GetGadgetRect(register GadgetPtr gadget, WindowPtr window, RequestPtr request,
				   RectPtr rect)
{
	register WORD	leftEdge, topEdge, width, height;
	WORD			elemWidth, elemHeight, flags;

	if (gadget->GadgetType & GTYP_REQGADGET) {
		elemWidth  = request->Width;
		elemHeight = request->Height;
	}
	else {
		elemWidth  = window->Width;
		elemHeight = window->Height;
	}
	leftEdge = gadget->LeftEdge;
	topEdge = gadget->TopEdge;
	width = gadget->Width;
	height = gadget->Height;
	flags = gadget->Flags;
	if (flags & GFLG_RELRIGHT)
		leftEdge += elemWidth - 1;
	if (flags & GFLG_RELBOTTOM)
		topEdge += elemHeight - 1;
	if (flags & GFLG_RELWIDTH)
		width += elemWidth - 1;
	if (flags & GFLG_RELHEIGHT)
		height += elemHeight - 1;
	rect->MinX = leftEdge;
	rect->MinY = topEdge;
	rect->MaxX = leftEdge + width - 1;
	rect->MaxY = topEdge + height - 1;
}

/*
 *	TrackGadget
 *	Get messages from message port and check for gadget being selected
 *	Continuously call actionProc(window, gadgetNum) while gadget is selected
 *	Will call actionProc at least once
 *	Return when mouse button is up
 */

void TrackGadget(MsgPortPtr msgPort, register WindowPtr window,
				 register GadgetPtr gadget, void (*actionProc)(WindowPtr, WORD))
{
	register WORD			gadgetNum;
	register IntuiMsgPtr	intuiMsg;
	register ULONG			class;
	register UWORD			qualifier;
	BOOL					actionDone;
	WindowPtr				msgWindow;

	gadgetNum = GadgetNumber(gadget);
	actionDone = FALSE;
/*
	Get and process messages
	Should only get GADGETUP and possible MOUSEMOVE or INTUITICKS messages,
		anything else is invalid
*/
	for (;;) {
		while (intuiMsg = (IntuiMsgPtr) GetMsg(msgPort)) {
			class     = intuiMsg->Class;
			qualifier = (class != IDCMP_INTUITICKS) ? intuiMsg->Qualifier :
												IEQUALIFIER_LEFTBUTTON;
			msgWindow = intuiMsg->IDCMPWindow;
/*
	If message is not for us, then put it back and return
*/
			if (msgWindow != window ||
				(class != IDCMP_MOUSEMOVE && class != IDCMP_INTUITICKS) ||
				!SELECTBUTTON(qualifier)) {
				PutMsg(msgPort, (MsgPtr) intuiMsg);
				if (actionProc && !actionDone)
					(*actionProc)(window, gadgetNum);
				return;
			}
			ReplyMsg((MsgPtr) intuiMsg);
/*
	If INTUITICKS message and actionProc, then call actionProc
*/
			if (class == IDCMP_INTUITICKS && actionProc &&
				(gadget->Flags & GFLG_SELECTED)) {
				(*actionProc)(window, gadgetNum);
				actionDone = TRUE;
			}
		}
/*
	If no messages pending, then call actionProc or wait for message
	(Only call actionProc here if INTUITICKS is not enabled)
*/
		if ((window->IDCMPFlags & IDCMP_INTUITICKS) == 0 && actionProc) {
			if (gadget->Flags & GFLG_SELECTED) {
				(*actionProc)(window, gadgetNum);
				actionDone = TRUE;
			}
		}
		else
			Wait(1 << msgPort->mp_SigBit);
	}
}

/*
 *	IsEditReturnCycle
 *	Return TRUE if gadget is return-cycle text box
 */

BOOL IsEditReturnCycle(register GadgetPtr gadget)
{
	return ((gadget->GadgetType & GTYP_SYSGADGET) == 0 &&
			(gadget->GadgetType & GTYP_STRGADGET) &&
			(((GadgetXPtr) gadget)->Flags & GADG_EDIT_RETURNCYCLE));
}

/*
 *	IsGadgetKey
 *	Determine if character is key equivalent for gadget
 */

BOOL IsGadgetKey(GadgetPtr gadget, TextChar ch)
{
	WORD	type;

	type = GadgetType(gadget);
	if (type == GADG_PUSH_BUTTON	||
		type == GADG_CHECK_BOX		|| type == GADG_RADIO_BUTTON ||
		type == GADG_ACTIVE_BORDER	|| type == GADG_ACTIVE_STDBORDER ||
		type == GADG_ACTIVE_IMAGE	|| type == GADG_ACTIVE_STDIMAGE)
		return (toUpper[((GadgetXPtr) gadget)->KeyEquiv] == toUpper[ch]);
	return (FALSE);
}

/*
 *	DoPopUpGadget
 *	Handle pop-up gadget
 */

void DoPopUpGadget(GadgetPtr gadget, WindowPtr window)
{
	WORD			x, y, width, item, prevItem;
	RequestPtr		request = window->FirstRequest;
	PopUpListPtr	popUpList;
	Rectangle		rect;

/*
	Get position of pop-up list
*/
	GetGadgetRect(gadget, window, request, &rect);
	if (request)
		OffsetRect(&rect, request->LeftEdge, request->TopEdge);
	OffsetRect(&rect, window->LeftEdge, window->TopEdge);
	x = rect.MinX - 2;
	y = rect.MinY + gadget->Height + 1;
	width = gadget->Width + 2;
/*
	Handle gadget
*/
	popUpList = ((GadgetXPtr) gadget)->Extra;
	prevItem = ((GadgetXPtr) gadget)->Value;
	DrawPopUp(gadget->GadgetRender, GetListItem(popUpList->List, prevItem), TRUE);
	RefreshGList(gadget, window, request, 1);
	item = PopUpListSelect(window->WScreen, popUpList, x, y, width);
	DrawPopUp(gadget->GadgetRender, GetListItem(popUpList->List, prevItem), FALSE);
	RefreshGList(gadget, window, request, 1);
	if (item != -1)
		SetGadgetValue(gadget, window, request, item);
}
