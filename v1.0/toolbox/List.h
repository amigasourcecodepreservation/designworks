/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Amiga Toolbox routines
 *	Copyright (c) 1989 New Horizons Software
 *
 *	Simple linked list defines
 */

#ifndef TOOLBOX_LIST_H
#define TOOLBOX_LIST_H

#ifndef EXEC_TYPES_H
#include <exec/types.h>
#endif

#ifndef TYPEDEFS_H
#include "typedefs.h"
#endif

/*
 *	List entry
 */

typedef struct _ListItem {
	struct _ListItem	*Next;
	TextChar			Text[1];	/* NULL terminated text string */
} ListItem, *ListItemPtr;

/*
 *	List header
 */

typedef struct {
	ListItemPtr	First, Last;
} ListHead, *ListHeadPtr;

/*
 *	Routine prototypes
 */

ListHeadPtr	CreateList(void);
void		DisposeList(ListHeadPtr);
ListItemPtr	CreateListItem(TextPtr, WORD);
void		AddToList(ListHeadPtr, ListItemPtr, ListItemPtr);
void		RemoveFromList(ListHeadPtr, ListItemPtr, ListItemPtr);
LONG		AddListItem(ListHeadPtr, TextPtr, WORD, BOOL);
BOOL		InsertListItem(ListHeadPtr, TextPtr, WORD, LONG);
void		RemoveListItem(ListHeadPtr, LONG);
void		DisposeListItems(ListHeadPtr);
LONG		NumListItems(ListHeadPtr);
TextPtr		GetListItem(ListHeadPtr, LONG);
LONG		FindListItem(ListHeadPtr, TextPtr);

#endif
