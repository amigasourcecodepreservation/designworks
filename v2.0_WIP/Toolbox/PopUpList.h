/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Toolbox library
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	Pop-up List handler
 */

#ifndef TOOLBOX_POPUPLIST_H
#define TOOLBOX_POPUPLIST_H

#ifndef TYPEDEFS_H
#include <Typedefs.h>
#endif

#ifndef TOOLBOX_LIST_H
#include <Toolbox/List.h>
#endif

/*
 *	Pop-up list structure (private)
 */

typedef struct {
	ListHeadPtr	List;
	WindowPtr	Window;
	TextFontPtr	Font;
	void		(*DrawProc)(RastPtr, TextPtr, RectPtr);
} PopUpList, *PopUpListPtr;


/*
 *	Prototypes
 */

PopUpListPtr	CreatePopUpList(WORD, TextPtr []);
void			DisposePopUpList(PopUpListPtr);

WORD	PopUpListSelect(ScreenPtr, PopUpListPtr, WORD, WORD, WORD);

void	PopUpSetDrawProc(PopUpListPtr, void (*)(RastPtr, TextPtr, RectPtr));

#endif
