/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991-92 New Horizons Software, Inc.
 *	All rights reserved
 *
 *	EPS File routines
 */

#include <graphics/gfxmacros.h>

#include <proto/graphics.h>
#include <proto/dos.h>

#include <string.h>

#include <IFF/ILBM.h>

#include <Toolbox/Utility.h>
#include <Toolbox/DOS.h>
#include <Toolbox/Memory.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Window.h>
#include <Toolbox/Font.h>

#include "Draw.h"
#include "Proto.h"

/*
	external variables
 */

extern ScreenPtr	screen;

extern TextChar	strBuff[];

extern UWORD	ltGrayPat[], blackPat[];
extern UBYTE	_tbPenBlack;

extern TextChar	strTitle[], strCreationDate[], strCreator[];

/*
	local variables
 */

static EPSFObjPtr	growEPSFObj;
static BOOL			wasMoved;
static WORD			growHandle;

static TextChar	strTopazFont[] = "topaz.font";
static TextChar strPSAdobe[] = "%!PS-Adobe";
static TextChar strEPSFile[] = "EPSF";

static TextPtr EPSFCommands[] = {
	"%%BoundingBox:", "%%Title:", "%%CreationDate:", "%%Creator:", "%%EndComments"
};

#define NUM_COMMANDS	5

enum {
	BOUNDING_BOX,	TITLE,	CREATE_DATE, CREATOR, ENDCOMMENTS
};


/*
	local prototypes
 */

static void DisposeInfoBitMap(EPSFObjPtr);
static void CreateInfoBitMap(EPSFObjPtr, RastPtr, Fixed);
static void GetNewLine(TextPtr, ULONG, TextPtr, ULONG *, WORD *);
static void GetNextWord(TextPtr, TextPtr, ULONG *);
static void GetBoundingBox(DocDataPtr, RectPtr, BOOL);
static WORD GetCommand(TextPtr);

/*
 *	allocate memory for EPSFObj
 */

EPSFObjPtr	EPSFAllocate(void)
{
	return (MemAlloc(sizeof(EPSFObj), MEMF_CLEAR));
}

/*
 *	duplicate data for EPSFObj
 */

BOOL	EPSFDupData(EPSFObjPtr epsfObj, EPSFObjPtr newEPSFObj)
{
	RastPort	rPort;

	if ((newEPSFObj->File = MemAlloc(epsfObj->FileSize, 0)) == NULL)
		return (FALSE);
	if ((newEPSFObj->Title = MemAlloc(strlen(epsfObj->Title) + 1, 0)) == NULL)
		return (FALSE);
	if ((newEPSFObj->CreationDate = MemAlloc(strlen(epsfObj->CreationDate) + 1, 0)) == NULL)
		return (FALSE);
	if ((newEPSFObj->Creator = MemAlloc(strlen(epsfObj->Creator) + 1, 0)) == NULL)
		return (FALSE);
	if (epsfObj->InfoBitMap) {
		if ((newEPSFObj->InfoBitMap = CreateBitMap(epsfObj->InfoWidth, epsfObj->InfoHeight, 1, TRUE)) == NULL)
			return (FALSE);
		InitRastPort(&rPort);
		rPort.BitMap = newEPSFObj->InfoBitMap;
		ExtBltTemplate(epsfObj->InfoBitMap->Planes[0], 0, epsfObj->InfoBitMap->BytesPerRow,
						&rPort, 0, 0, epsfObj->InfoWidth, epsfObj->InfoHeight);
	}

	BlockMove(epsfObj->File, newEPSFObj->File, epsfObj->FileSize);
	strcpy(newEPSFObj->Title, epsfObj->Title);
	strcpy(newEPSFObj->CreationDate, epsfObj->CreationDate);
	strcpy(newEPSFObj->Creator, epsfObj->Creator);


	newEPSFObj->EPSFFlags 	= epsfObj->EPSFFlags;
	newEPSFObj->Rotate		= epsfObj->Rotate;
	newEPSFObj->BoundingBox = epsfObj->BoundingBox;
	newEPSFObj->FileSize 	= epsfObj->FileSize;
	newEPSFObj->InfoWidth	= epsfObj->InfoWidth;
	newEPSFObj->InfoHeight	= epsfObj->InfoHeight;

	return (TRUE);
}

/*
 *	dispose of memory used by EPSFObj
 */

void	EPSFDispose(EPSFObjPtr epsfObj)
{
	if (epsfObj->File)
		MemFree(epsfObj->File, epsfObj->FileSize);
	if (epsfObj->Title)
		MemFree(epsfObj->Title, strlen(epsfObj->Title) + 1);
	if (epsfObj->CreationDate)
		MemFree(epsfObj->CreationDate, strlen(epsfObj->CreationDate) + 1);
	if (epsfObj->Creator)
		MemFree(epsfObj->Creator, strlen(epsfObj->Creator) + 1);
	if (epsfObj->InfoBitMap)
		DisposeBitMap(epsfObj->InfoBitMap);
	if (epsfObj->FileName)
		MemFree(epsfObj->FileName, strlen(epsfObj->FileName) + 1);
	MemFree(epsfObj, sizeof(EPSFObj));
}

/*
 *	dispose of infoBitMap cache
 */

static void DisposeInfoBitMap(EPSFObjPtr epsfObj)
{
	if (epsfObj->InfoBitMap) {
		DisposeBitMap(epsfObj->InfoBitMap);
		epsfObj->InfoBitMap = NULL;
	}
}

/*
 *	create infoBitMap cache
 */

static void CreateInfoBitMap(EPSFObjPtr epsfObj, RastPtr rPort, Fixed scale)
{
	WORD 		fontHeight = 13;	// topaz 8 plus 5
	WORD		left, top, strWidth, width;
	RastPtr		tempPort, xfrmPort;
	RastPort	transPort;
	TextFontPtr	oldFont, newFont;
	FontNum		fontNum;
	Rectangle	dstRect, drawRect;

	drawRect.MinX = drawRect.MinY = 0;
	drawRect.MaxY = epsfObj->DocObj.Frame.MaxY - epsfObj->DocObj.Frame.MinY;
	drawRect.MaxX = epsfObj->DocObj.Frame.MaxX - epsfObj->DocObj.Frame.MinX;

	dstRect = drawRect;
	ScaleDocToWin(scale, drawRect.MaxX, drawRect.MaxY, &dstRect.MaxX, &dstRect.MaxY);
	width = drawRect.MaxX + 1;

	left = 5;
	top = 0;
	if ((fontNum = FontNumber(strTopazFont)) == -1)
		return;
	if ((tempPort = CreateRastPort(drawRect.MaxX + 1, drawRect.MaxY + 1, 1)) == NULL)
		return;
	if ((newFont = GetNumFont(fontNum, 8)) == NULL)
		goto Exit;
	oldFont = tempPort->Font;
	SetFont(tempPort, newFont);
/*
	Draw titles
*/
	SetDrMd(tempPort, JAM1);

	SetSoftStyle(tempPort, FSF_BOLD, 0xFF);
	Move(tempPort, left, top + fontHeight);		// first line
	Text(tempPort, strTitle, strlen(strTitle));
	Move(tempPort, left, top + 2*fontHeight);		// 2nd line
	Text(tempPort, strCreator, strlen(strCreator));
	Move(tempPort, left, top + 3*fontHeight);		// 3rd line
	Text(tempPort, strCreationDate, strlen(strCreationDate));
/*
	draw info
*/
	SetSoftStyle(tempPort, FS_NORMAL, 0xFF);
	if (epsfObj->Title) {
		strWidth = TextLength(tempPort, strTitle, strlen(strTitle));
		Move(tempPort, left + strWidth, top + fontHeight);		// first line
		TextInWidth(tempPort, epsfObj->Title, strlen(epsfObj->Title),
						width - strWidth, FALSE);
	}
	if (epsfObj->Creator) {
		strWidth = TextLength(tempPort, strCreator, strlen(strCreator));
		Move(tempPort, left + strWidth, top + 2*fontHeight);		// 2nd line
		TextInWidth(tempPort, epsfObj->Creator, strlen(epsfObj->Creator),
						width - strWidth, FALSE);
	}
	if (epsfObj->CreationDate) {
		strWidth = TextLength(tempPort, strCreationDate, strlen(strCreationDate));
		Move(tempPort, left + strWidth, top + 3*fontHeight);		// 3rd line
		TextInWidth(tempPort, epsfObj->CreationDate, strlen(epsfObj->CreationDate),
						width - strWidth, FALSE);
	}
/*
	transform if necessary
*/
	if (drawRect.MaxX != dstRect.MaxX || drawRect.MaxY != dstRect.MaxY ||
			epsfObj->Rotate || (epsfObj->EPSFFlags & (EPSF_FLIPHORIZ | EPSF_FLIPVERT))) {
			xfrmPort = CreateRastPort(dstRect.MaxX + 1, dstRect.MaxY + 1, 1);
			if (xfrmPort == NULL)
				goto Exit;
			TransformBitMap(tempPort, xfrmPort, &drawRect, &dstRect,
							epsfObj->Rotate,
							(epsfObj->EPSFFlags & EPSF_FLIPHORIZ),
							(epsfObj->EPSFFlags & EPSF_FLIPVERT));
	}
	else
			xfrmPort = tempPort;
/*
	save bitmap in cache
*/
	epsfObj->InfoBitMap = CreateBitMap(dstRect.MaxX + 1, dstRect.MaxY + 1, 1, TRUE);
	if (epsfObj->InfoBitMap == NULL)
		goto Exit;

	InitRastPort(&transPort);
	transPort.BitMap = epsfObj->InfoBitMap;
	epsfObj->InfoWidth = dstRect.MaxX + 1;
	epsfObj->InfoHeight = dstRect.MaxY + 1;

	ExtBltTemplate(xfrmPort->BitMap->Planes[0], 0, xfrmPort->BitMap->BytesPerRow,
					&transPort, 0, 0, epsfObj->InfoWidth, epsfObj->InfoHeight);

Exit:
	if (xfrmPort && xfrmPort != tempPort)
		DisposeRastPort(xfrmPort);

	SetFont(tempPort, oldFont);
	if (newFont)
		CloseFont(newFont);
	DisposeRastPort(tempPort);

}

/*
 *	Draw EPSF object screen representation
 */

void EPSFDrawObj(RastPtr rPort, EPSFObjPtr epsfObj, RectPtr rect, RectPtr clipRect)
{
	WORD	dstWidth, dstHeight, dx, dy;
/*
	Fill rect
*/
	PenNormal(rPort);
	FrameRectNew(rPort, rect, 1, 1);
	SetAfPt(rPort, ltGrayPat, 1);
	FillRect(rPort, rect);
	SetAfPt(rPort, blackPat, 1);
/*
	Draw info
*/
/*
	dx = epsfObj->DocObj.Frame.MaxX - epsfObj->DocObj.Frame.MinX;
	dy = epsfObj->DocObj.Frame.MaxY - epsfObj->DocObj.Frame.MinY;
	ScaleDocToWin(scale, dx, dy, &dstWidth, &dstHeight);
	dstWidth++;
	dstHeight++;

	if (epsfObj->InfoWidth != dstWidth || epsfObj->InfoHeight != dstHeight)
		DisposeInfoBitMap(epsfObj);

	if (epsfObj->InfoBitMap == NULL)
		CreateInfoBitMap(epsfObj, rPort, scale);

	if (epsfObj->InfoBitMap) {
		RGBForeColor(rPort, RGBCOLOR_BLACK);
		SetDrMd(rPort, JAM1);
		ExtBltTemplate(epsfObj->InfoBitMap->Planes[0], 0, epsfObj->InfoBitMap->BytesPerRow,
					rPort, rect->MinX, rect->MinY, epsfObj->InfoWidth, epsfObj->InfoHeight);
	}
*/
}

/*
 *	draw box designating EPSFObj bounding box
 */

void	EPSFDrawOutline(WindowPtr window, EPSFObjPtr epsfObj, WORD xOffset, WORD yOffset)
{
	RectDrawOutline(window, (RectObjPtr) epsfObj, xOffset, yOffset);
}

/*
 *	hilite box designating EPSFObj bounding box
 */

void	EPSFHilite(WindowPtr window, EPSFObjPtr epsfObj)
{
	RectHilite(window, (RectObjPtr) epsfObj);
}

/*
 *	rotate EPSFObj
 */

void	EPSFRotate(EPSFObjPtr epsfObj, WORD cx, WORD cy, WORD angle)
{
	RotateRect(&epsfObj->DocObj.Frame, cx, cy, angle);
	epsfObj->Rotate = NormalizeAngle(epsfObj->Rotate + angle);
	DisposeInfoBitMap(epsfObj);
}

/*
 *	flip EPSFObj
 */

void	EPSFFlip(EPSFObjPtr epsfObj, WORD cx, WORD cy, BOOL horiz)
{
	epsfObj->Rotate = NormalizeAngle(epsfObj->Rotate);
	if (epsfObj->Rotate % 180)
		horiz = !horiz;
	if (horiz) {
		if (epsfObj->EPSFFlags & EPSF_FLIPHORIZ)
			epsfObj->EPSFFlags &= ~EPSF_FLIPHORIZ;
		else
			epsfObj->EPSFFlags |= EPSF_FLIPHORIZ;
	}
	else {
		if (epsfObj->EPSFFlags & EPSF_FLIPVERT)
			epsfObj->EPSFFlags &= ~EPSF_FLIPVERT;
		else
			epsfObj->EPSFFlags |= EPSF_FLIPVERT;
	}
	if ((epsfObj->EPSFFlags & EPSF_FLIPHORIZ) && (epsfObj->EPSFFlags & EPSF_FLIPVERT)) {
		epsfObj->EPSFFlags &= ~(EPSF_FLIPHORIZ | EPSF_FLIPVERT);
		epsfObj->Rotate = NormalizeAngle(epsfObj->Rotate + 180);
	}
	FlipRect(&epsfObj->DocObj.Frame, cx, cy, horiz);
	DisposeInfoBitMap(epsfObj);
}

/*
 *	return TRUE if point is in EPSFObj bounding box
 */

BOOL	EPSFSelect(EPSFObjPtr epsfObj, PointPtr pt)
{
	return (TRUE);
}

/*
 *	return handle number if point is in handle else return -1
 */

WORD	EPSFHandle(EPSFObjPtr epsfObj, PointPtr pt, RectPtr handleRect)
{
	return (RectHandle((RectObjPtr) epsfObj, pt, handleRect));
}

/*
 *	scale epsfObj to given frame
 */

void	EPSFScale(EPSFObjPtr epsfObj, RectPtr frame)
{
	epsfObj->DocObj.Frame = *frame;
	DisposeInfoBitMap(epsfObj);
}

/*
 *	Track mouse and change rect shape
 */

void EPSFGrow(WindowPtr window, EPSFObjPtr epsfObj, UWORD modifier, WORD mouseX, WORD mouseY)
{
	RectGrow(window, (RectObjPtr) epsfObj, modifier, mouseX, mouseY);
	DisposeInfoBitMap(epsfObj);
}

/*
 *	send EPSFObj to PostScript output
 */

BOOL	EPSFDrawPSObj(PrintRecPtr printRec, EPSFObjPtr epsfObj,
							RectPtr dstRect, RectPtr clipRect)
{
	return (PSEPSF(printRec, epsfObj->Title, epsfObj->File, epsfObj->FileSize, epsfObj->BoundingBox,
					*dstRect, epsfObj->Rotate, epsfObj->EPSFFlags));
}

/*
 *	get full line from buffer
 */

static void GetNewLine(TextPtr buffer, ULONG size, TextPtr line, ULONG *loc, WORD *len)
{
	WORD	length;

	for (length = 0;
		 buffer[*loc + length] != '\n' && buffer[*loc + length] != CR &&
				(length + *loc) < size;
		 length++);
	BlockMove(&buffer[*loc], line, length);
	line[length] = '\0';
	*loc += (length + 1);
	*len = length;
}

/*
 *	get next word from buffer
 */

static void GetNextWord(TextPtr buffer, TextPtr word, ULONG *loc)
{
	WORD	len;

	for (len = 0;
		buffer[*loc + len] != ' ' && buffer[*loc + len] != '\t' && buffer[len + *loc];
		len++);
	BlockMove(&buffer[*loc], word, len);
	word[len] = '\0';
	*loc += (len + 1);
}

/*
 *	return TRUE if file text contains EPSF code word
 */

BOOL EPSFileFilter(TextPtr fileName)
{
	UBYTE		word[25];
	File		file;
	BOOL		success = FALSE;
	WORD		size = 0;
	TextChar	line[25];
	ULONG		lineLoc = 0;

	if ((file = Open(fileName, MODE_OLDFILE)) != NULL) {
		size = Read(file, line, 23);
		Close(file);
	}

	if (size) {
		line[size] = '\0';
		GetNextWord(line, word, &lineLoc);
		word[strlen(word) - 4] = '\0';		// strip version number
// first word must be PSAdobe
		if (CmpString(word, strPSAdobe, strlen(word), strlen(strPSAdobe), TRUE) == 0) {
			GetNextWord(line, word, &lineLoc);
			word[strlen(word) - 4] = '\0';		// strip version number
			if (CmpString(word, strEPSFile, strlen(word), strlen(strEPSFile), TRUE) == 0)
				return (TRUE);
		}
	}

	return (success);
}

static void GetBoundingBox(DocDataPtr docData, RectPtr rect, BOOL all)
{
	BOOL 		success;
	DocObjPtr 	docObj;
	WORD		maxPenX, maxPenY;
	WORD 		dx, dy, r, len;
	LineObjPtr	lineObj;

	if (all) {
		SetRect(rect, 0, 0, docData->DocWidth - 1, docData->DocHeight - 1);
		return;
	}

	maxPenX = maxPenY = 0;
	success = FALSE;
	for (docObj = FirstSelected(docData); docObj; docObj = NextSelected(docObj)) {
		if (!success)
			*rect = docObj->Frame;
		else
			UnionRect(&docObj->Frame, rect, rect);
		success = TRUE;
		switch (docObj->Type) {
			case TYPE_RECT:
				maxPenX = MAX(maxPenX, ((RectObjPtr) docObj)->PenWidth);
				maxPenY = MAX(maxPenY, ((RectObjPtr) docObj)->PenHeight);
				break;
			case TYPE_OVAL:
				maxPenX = MAX(maxPenX, ((OvalObjPtr) docObj)->PenWidth);
				maxPenY = MAX(maxPenY, ((OvalObjPtr) docObj)->PenHeight);
				break;
			case TYPE_POLY:
				maxPenX = MAX(maxPenX, ((PolyObjPtr) docObj)->PenWidth);
				maxPenY = MAX(maxPenY, ((PolyObjPtr) docObj)->PenHeight);
				break;
			case TYPE_LINE:
				lineObj = (LineObjPtr) docObj;
				maxPenX = MAX(maxPenX, lineObj->PenWidth);
				maxPenY = MAX(maxPenY, lineObj->PenHeight);
				dx = ABS(lineObj->Start.x - lineObj->End.x);
				dy = ABS(lineObj->Start.y - lineObj->End.y);
				r = LineLength(&lineObj->Start, &lineObj->End);
				len = 4 + (lineObj->PenWidth + lineObj->PenHeight)/2;
				if (lineObj->LineFlags & (LINE_ARROWSTART | LINE_ARROWEND)) {
					maxPenX += 2*(len*(dx*2 + dy)/r);
					maxPenY += 2*(len*(dy*2 - dx)/r);
				}
				break;
		}
	}
	InsetRect(rect, -maxPenX, -maxPenY);
}

/*
 *	Export picture as EPS file
 */

BOOL ExportEPSFile(DocDataPtr docData, TextPtr fileName, BOOL all)
{
	WORD		dx, dy;
	BOOL 		result;
	PrintRecord	printRec = *docData->PrintRec;
	DocObjPtr	docObj, firstObj, nextObj;
	DocLayerPtr	layer;
	Rectangle	rect, objRect;
	RastPtr		rPort = NULL;

	result = FALSE;				/* Assume error */
/*
	Open device/file for output
*/
	if ((printRec.File = Open(fileName, MODE_NEWFILE)) == NULL)
		return (FALSE);
/*
	Begin job
*/
	GetBoundingBox(docData, &rect, all);
	dx = rect.MinX;
	dy = rect.MinY;
	SetRect(&rect, 0, 0, rect.MaxX - rect.MinX, rect.MaxY - rect.MinY);
	if (!PSBeginJob(&printRec, fileName, &rect))
		goto Exit;
/*
	Print selected objects
*/
	layer = BottomLayer(docData);
	firstObj = (all) ? layer->Objects : FirstSelected(docData);
	for (docObj = firstObj; docObj; docObj = nextObj) {
		objRect = docObj->Frame;
		OffsetRect(&objRect, -dx, -dy);
		if (!DrawPSObject(&printRec, docObj, &objRect, &rect))
			break;
		nextObj = (all) ? NextObj(docObj) : NextSelected(docObj);
		if (all && nextObj == NULL) {
			layer = NextLayer(layer);
			nextObj = (layer) ? layer->Objects : NULL;
		}
	}
/*
	End job
*/
	if (PSEndJob(&printRec, TRUE))
		result = TRUE;
/*
	Clean up
*/
Exit:
	Close(printRec.File);
	if (rPort)
		DisposeRastPort(rPort);

	if (result)
		SaveIcon(fileName, ICON_EPSF);
	return (result);
}

/*
 *	return value for given command
 */

static WORD GetCommand(TextPtr word)
{
	WORD	i;

	for (i = 0; i < NUM_COMMANDS; i++)
		if (CmpString(word, EPSFCommands[i], strlen(word), strlen(EPSFCommands[i]),
				TRUE) == 0)
			return (i);

	return (i);
}

/*
 *	parse through file buffer to fill in epsfObj structure
 */

BOOL SetUpEPSFObj(EPSFObjPtr epsfObj)
{
	ULONG		loc, size, lineLoc;
	WORD		lineLen, command;
	TextChar	line[256];
	TextChar	word[30];
	BOOL		quit;

	size = epsfObj->FileSize;
	loc = 0;
	quit = FALSE;

	while (loc < size && !quit) {
		GetNewLine(epsfObj->File, size, line, &loc, &lineLen);
		lineLoc = 0;
		GetNextWord(line, word, &lineLoc);
		command = GetCommand(word);
		switch (command) {
			case BOUNDING_BOX :
				GetNextWord(line, word, &lineLoc);
				epsfObj->BoundingBox.MinX = StringToNum(word);
				GetNextWord(line, word, &lineLoc);
				epsfObj->BoundingBox.MinY = StringToNum(word);
				GetNextWord(line, word, &lineLoc);
				epsfObj->BoundingBox.MaxX = StringToNum(word);
				GetNextWord(line, word, &lineLoc);
				epsfObj->BoundingBox.MaxY = StringToNum(word);
				break;
			case TITLE:
				if ((epsfObj->Title = MemAlloc(lineLen - lineLoc + 1, MEMF_CLEAR)) == NULL)
					return (FALSE);
				strcpy(epsfObj->Title, &line[lineLoc]);
				break;
			case CREATE_DATE:
				if ((epsfObj->CreationDate = MemAlloc(lineLen - lineLoc + 1, MEMF_CLEAR)) == NULL)
					return (FALSE);
				strcpy(epsfObj->CreationDate, &line[lineLoc]);
				break;
			case CREATOR:
				if ((epsfObj->Creator = MemAlloc(lineLen - lineLoc + 1, MEMF_CLEAR)) == NULL)
					return (FALSE);
				strcpy(epsfObj->Creator, &line[lineLoc]);
				break;
			case ENDCOMMENTS:
				quit = TRUE;
				break;
		}
	}

	return (TRUE);
}

/*
 *	read EPSFile and allocate EPSFObj
 */

BOOL LoadEPSFile(WindowPtr window, TextPtr fileName, Dir dirLock, BOOL draw)
{
	BOOL					success;
	DocDataPtr				docData = GetWRefCon(window);
	register EPSFObjPtr		epsfObj;
	struct FileInfoBlock	*fib;
	Dir						lock, origDir;
	File					file;
	ULONG					size;
	WORD					len;

	success = FALSE;
	origDir = CurrentDir(dirLock);
	lock = NULL;

	if ((epsfObj = (EPSFObjPtr) NewDocObject(CurrLayer(docData), TYPE_EPSF)) == NULL)
		return (FALSE);
/*
	get file information
 */
	if ((fib = MemAlloc(sizeof(struct FileInfoBlock), MEMF_CLEAR)) == NULL)
		goto Exit;
	if ((lock = Lock(fileName, ACCESS_READ)) == NULL ||
			!Examine(lock, fib))
		goto Exit;

	size = fib->fib_Size;
/*
	get link information
*/
	epsfObj->FileDate = fib->fib_Date;
	if (NameFromLock1(dirLock, strBuff, 256)) {
		len = strlen(strBuff);
		if (strBuff[len - 1] != ':') {
			strcat(strBuff, "/");
			len++;
		}
		strcat(strBuff, fileName);
		len += strlen(fileName);
		if (epsfObj->FileName)
			MemFree(epsfObj->FileName, strlen(epsfObj->FileName) + 1);
		if ((epsfObj->FileName = MemAlloc(len + 1, MEMF_CLEAR)) != NULL)
		strcpy(epsfObj->FileName, strBuff);
	}
/*
	get file
*/
	if ((epsfObj->File = MemAlloc(size, MEMF_CLEAR)) != NULL) {
		if ((file = Open(fileName, MODE_OLDFILE)) != NULL) {
			success = (Read(file, epsfObj->File, size) == size);
			Close(file);
		}
	}
	if (success) {
		epsfObj->FileSize = size;
		success = SetUpEPSFObj(epsfObj);
		epsfObj->DocObj.Frame = epsfObj->BoundingBox;
		OffsetRect(&epsfObj->DocObj.Frame, -epsfObj->BoundingBox.MinX, -epsfObj->BoundingBox.MinY);
	}
Exit:
	if (lock)
		UnLock(lock);
	if (fib)
		MemFree(fib, sizeof(struct FileInfoBlock));
	UnLock(dirLock);
	if (origDir)
		CurrentDir(origDir);

	if (success && draw) {
		HiliteSelectOff(window);
		UnSelectAllObjects(docData);
		SelectObject((DocObjPtr) epsfObj);
		InvalObjectRect(window, (DocObjPtr) epsfObj);
		HiliteSelectOn(window);
		DocModified(docData);
	}
	if (!success) {
		DetachObject(CurrLayer(docData), (DocObjPtr) epsfObj);
		EPSFDispose(epsfObj);
	}

	return (success);
}

