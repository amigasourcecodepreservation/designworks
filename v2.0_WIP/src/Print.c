/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Printing routines
 */

#include <exec/types.h>
#include <exec/memory.h>
#include <graphics/gfxbase.h>
#include <intuition/intuition.h>
#include <devices/printer.h>
#include <devices/prtbase.h>
#include <libraries/dos.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/layers.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include <string.h>

#include <Toolbox/Graphics.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Utility.h>

#include "Draw.h"
#include "Proto.h"

/*
 *	External variables
 */

extern ScreenPtr	screen;
extern MsgPortPtr	mainMsgPort;

extern TextChar	printerName[];
extern BOOL		graphicPrinter, colorPrinter, pagePrinter;

extern TextAttr	monoAttr;

extern Options	options;

extern TextChar	strPrtSetup[], strPrtRender[], strPrtPrint[], strPrtCancel[];

extern TextChar	strBuff[];

extern DlgTemplPtr	dlgList[];

/*
 *	Local variables and definitions
 */

static Palette	prPalettes[3];		/* For three-pass full color printing */

static BOOL	printError;

#define INFO_TEXT	2

static DialogPtr	cancelDlg;

#define PRINT_OK	0
#define PRINT_ABORT	1
#define PRINT_ERROR	-1

#define DUMP_NOWAIT	0
#define DUMP_WAIT	1
#define DUMP_ERROR	-1

/*
 *	Local prototypes
 */

static void	StartPage(PrintRecPtr, WORD *, WORD *);
static void	NextPage(PrintRecPtr, WORD *, WORD *);

static BOOL	PageDialog(WORD);

static void	ScaleDocToPrint(PrintRecPtr, WORD, WORD, WORD *, WORD *);
static void	DocToPrintRect(PrintRecPtr, RectPtr, RectPtr);

static void	BuildPrintPalette(PalettePtr, WORD, WORD);
static BOOL	DrawPage(PrintRecPtr, DocDataPtr, WORD);

static WORD	DumpRPort(PrintRecPtr, BOOL);
static WORD	SendRawByte(PrintIOPtr, UBYTE);
static WORD	SendText(PrintIOPtr, TextPtr, WORD);
static WORD	SendCommand(PrintIOPtr, UWORD, UBYTE, UBYTE, UBYTE, UBYTE);
static WORD	EjectPage(PrintRecPtr);
static BOOL	FlushDump(PrintRecPtr);

static BOOL	CreatePrintRPort(PrintRecPtr, WORD, WORD);
static void	DisposePrintRPort(PrintRecPtr);

static WORD	WaitPrint(PrintIOPtr);
static WORD	DumpPage(PrintRecPtr, DocDataPtr, WORD);
static WORD	PrintGraphic(WindowPtr, DocDataPtr);

static WORD	PrintPSPage(PrintRecPtr, DocDataPtr, WORD);
static WORD	PrintPostScript(WindowPtr, DocDataPtr);

static void	SetCancelButton(BOOL);
static void	SetInfoText(TextPtr);

/*
 *	Get the starting page and copy from print record
 *	Return 0 for copy if no pages are to be printed
 */

static void StartPage(PrintRecPtr printRec, WORD *page, WORD *copy)
{
	register WORD firstPage = printRec->FirstPage;
	register WORD lastPage = printRec->LastPage;
	register UWORD flags = printRec->Flags;

	if (firstPage == lastPage)
		*page = firstPage;
	else if (flags & PRT_BACKTOFRONT)
		*page = lastPage;
	else
		*page = firstPage;
	*copy = (printRec->Quality == PRT_POSTSCRIPT && (flags & PRT_COLLATE) == 0) ?
			1 : printRec->Copies;
}

/*
 *	Get the next page and copy to print
 *	Return 0 for copy if no more pages to print
 */

static void NextPage(PrintRecPtr printRec, WORD *page, WORD *copy)
{
	register WORD firstPage = printRec->FirstPage;
	register WORD lastPage = printRec->LastPage;
	register UWORD flags = printRec->Flags;
	WORD currCopy;

	if ((flags & PRT_COLLATE) == 0) {
		(*copy)--;
		if (*copy > 0)
			return;
		*copy = (printRec->Quality == PRT_POSTSCRIPT) ? 1 : printRec->Copies;
	}
	if (flags & PRT_BACKTOFRONT)
		(*page)--;
	else
		(*page)++;
	if (*page >= firstPage && *page <= lastPage)
		return;
	if ((flags & PRT_COLLATE) && *copy > 1) {
		currCopy = *copy;
		StartPage(printRec, page, copy);
		*copy = currCopy - 1;
	}
	else
		*copy = 0;
}

/*
 *	Request the next sheet of paper
 *	Return success status
 */

static BOOL PageDialog(WORD dlgNum)
{
	WORD item;

	SetInfoText(NULL);
	SetCancelButton(FALSE);
	dlgList[dlgNum]->TopEdge = screen->BarHeight + 2;
	item = StdDialog(dlgNum);
	RefreshWindows();
	return ((item == OK_BUTTON));
}

/*
 *	Scale from document to print values
 */

static void ScaleDocToPrint(PrintRecPtr printRec, WORD docX, WORD docY, WORD *prX, WORD *prY)
{
	register LONG x, y;
	PrintIOPtr printIO = printRec->PrintIO;
	struct PrinterData *pData;
	struct PrinterExtendedData *ped;

	if (printRec->Quality != PRT_FULLRES) {
		*prX = docX;
		*prY = docY;
	}
	else {
		x = docX;		/* So negative numbers are sign extended */
		y = docY;
		pData = (struct PrinterData *) printIO->Std.io_Device;
		ped = &pData->pd_SegmentData->ps_PED;
		if (printRec->Orientation == PRT_PORTRAIT) {
			x = (x*ped->ped_XDotsInch)/printRec->xDPI;
			y = (y*ped->ped_YDotsInch)/printRec->yDPI;
		}
		else {
			x = (x*ped->ped_YDotsInch)/printRec->yDPI;
			y = (y*ped->ped_XDotsInch)/printRec->xDPI;
		}
		*prX = x;
		*prY = y;
	}
}

/*
 *	Convert document rectangle to print rectangle
 */

static void DocToPrintRect(PrintRecPtr printRec, RectPtr docRect, register RectPtr prRect)
{
	WORD maxX, maxY;

	ScaleDocToPrint(printRec, docRect->MinX, docRect->MinY, &prRect->MinX, &prRect->MinY);
	ScaleDocToPrint(printRec, docRect->MaxX + 1, docRect->MaxY + 1, &maxX, &maxY);
	prRect->MaxX = (maxX > prRect->MinX) ? maxX - 1 : prRect->MinX;
	prRect->MaxY = (maxY > prRect->MinY) ? maxY - 1 : prRect->MinY;
}

/*
 *	Build print color table
 *	12 bit-plane color table is built in three passes
 */

static void BuildPrintPalette(PalettePtr palette, WORD depth, WORD pass)
{
	WORD pen, inc, r, g, b;

	if (depth == PRT_DEPTHFULL) {
		palette->NumPens = 16;
		for (pen = 0; pen < 16; pen++) {
			r = (pass == 2) ? pen : 0;
			g = (pass == 1) ? pen : 0;
			b = (pass == 0) ? pen : 0;
			palette->ColorTable[pen] = RGBCOLOR(r, g, b);
		}
	}
	else {
		palette->NumPens = 1 << depth;
		inc = (depth == PRT_DEPTHSTANDARD) ? 15 : 5;
		pen = 0;
		for (r = 0; r < 16; r += inc) {
			for (g = 0; g < 16; g += inc) {
				for (b = 0; b < 16; b += inc) {
					palette->ColorTable[pen++] = RGBCOLOR(r, g, b);
				}
			}
		}
	}
	MakeInvColorTable(palette);
	ColorCorrectEnable(palette, FALSE);
}

/*
 *	Draw page into specified RastPort
 *	Pages are numbered down then across
 *	It is only necessary to draw in bounds of printRec left/top/width/height
 *	Return TRUE if something was drawn
 */

static BOOL DrawPage(PrintRecPtr printRec, DocDataPtr docData, WORD page)
{
	WORD i, xOffset, yOffset, pass, numPasses;
	BOOL dirty;
	DocObjPtr docObj;
	DocLayerPtr docLayer, currLayer;
	RastPtr rPort;
	BitMapPtr bitMap;
	PalettePtr palette;
	RGBColor *colorTable;
	Rectangle objRect, clipRect;
	PLANEPTR planes[12];

	if (page >= PagesAcross(docData)*PagesDown(docData))
		return (FALSE);
	xOffset = -(page/PagesDown(docData))*docData->PageWidth;
	yOffset = -(page % PagesDown(docData))*docData->PageHeight;
	ScaleDocToPrint(printRec, xOffset, yOffset, &xOffset, &yOffset);
	xOffset -= printRec->Left;
	yOffset -= printRec->Top;
	SetRect(&clipRect, 0, 0, printRec->Width - 1, printRec->Height - 1);
	rPort = printRec->RPort;
	bitMap = rPort->BitMap;
/*
	Set up for multi-pass printing
*/
	if (printRec->Depth == PRT_DEPTHFULL) {
		for (i = 0; i < PRT_DEPTHFULL; i++)
			planes[i] = bitMap->Planes[i];
		bitMap->Depth = 4;				/* Three passes of four planes each */
		numPasses = 3;
	}
	else
		numPasses = 1;
	dirty = FALSE;
	for (pass = 0; pass < numPasses; pass++) {
		SetPalette(rPort, &prPalettes[pass]);
		if (printRec->Depth == PRT_DEPTHFULL) {
			for (i = 0; i < 4; i++)
				bitMap->Planes[i] = planes[pass*4 + i];
		}
/*
	Draw page
*/
		ClearRast(rPort);
		currLayer = CurrLayer(docData);
		docLayer = (docData->Flags & DOC_HIDEBACKLAY) ? currLayer : BottomLayer(docData);
		for (;;) {
			if (docLayer == currLayer || LayerVisible(docLayer)) {
				for (docObj = BottomObject(docLayer); docObj; docObj = NextObj(docObj)) {
					DocToPrintRect(printRec, &docObj->Frame, &objRect);
					OffsetRect(&objRect, xOffset, yOffset);
/*
					if (DrawObject(rPort, docObj, &objRect, &clipRect, docData->Scale))
						dirty = TRUE;
*/
				}
			}
			if (docLayer == currLayer)
				break;
			docLayer = NextLayer(docLayer);
		}
	}
/*
	Restore bitmap data
*/
	if (printRec->Depth == PRT_DEPTHFULL) {
		bitMap->Depth = PRT_DEPTHFULL;
		for (i = 0; i < PRT_DEPTHFULL; i++)
			bitMap->Planes[i] = planes[i];
		if ((colorTable = MemAlloc((1 << PRT_DEPTHFULL)*sizeof(RGBColor), 0)) != NULL) {
			for (i = 0; i < (1 << PRT_DEPTHFULL); i++)
				colorTable[i] = i;					/* Direct-lookup table */
			LoadRGB4CM(printRec->ColorMap, colorTable, (1 << PRT_DEPTHFULL));
			MemFree(colorTable, (1 << PRT_DEPTHFULL)*sizeof(RGBColor));
		}
	}
	else {
		palette = GetPalette(rPort);
		LoadRGB4CM(printRec->ColorMap, palette->ColorTable, palette->NumPens);
	}
	return (dirty);
}

/*
 *	Begin a printer dump of the given rastPort
 *	Scale output to maximum width and given dots of _printer_ height
 *	Will NOT wait until command completion before returning
 */

static WORD DumpRPort(register PrintRecPtr printRec, BOOL dirty)
{
	WORD xDPI, yDPI, width, height, maxXDPI, maxYDPI, lines;
	register PrintIOPtr printIO = printRec->PrintIO;
	register struct PrinterData *pData;
	register struct PrinterExtendedData *ped;
	MsgPortPtr replyPort;

	pData = (struct PrinterData *) printIO->Std.io_Device;
	ped = &pData->pd_SegmentData->ps_PED;

	ScaleDocToPrint(printRec, printRec->xDPI, printRec->yDPI, &xDPI, &yDPI);
	width  = printRec->Width;
	height = printRec->Height;
	maxXDPI = ped->ped_XDotsInch;
	maxYDPI = ped->ped_YDotsInch;

	printIO->DRP.io_Command = PRD_DUMPRPORT;
	printIO->DRP.io_RastPort = printRec->RPort;
	printIO->DRP.io_ColorMap = printRec->ColorMap;
	printIO->DRP.io_Modes = 0;
	printIO->DRP.io_SrcX = 0;
	printIO->DRP.io_SrcY = 0;
	printIO->DRP.io_SrcWidth  = width;
	printIO->DRP.io_SrcHeight = height;
/*
	Set destination width/height
*/
	if (printRec->Orientation == PRT_PORTRAIT) {
		pData->pd_Preferences.PrintAspect = ASPECT_HORIZ;
		printIO->DRP.io_DestCols = ((LONG) width*maxXDPI)/xDPI;
		printIO->DRP.io_DestRows = ((LONG) height*maxYDPI)/yDPI;
	}
	else {
		pData->pd_Preferences.PrintAspect = ASPECT_VERT;
		printIO->DRP.io_DestCols = ((LONG) width*maxYDPI)/xDPI;
		printIO->DRP.io_DestRows = ((LONG) height*maxXDPI)/yDPI;
	}
/*
	Start printing
*/
	if (!dirty) {
		lines = (printIO->DRP.io_DestRows*6)/maxYDPI;
		printIO->DRP.io_DestRows -= (lines*maxYDPI)/6;
		if (lines && SendCommand(printIO, aVERP1, 0, 0, 0, 0) != 0)
			return (DUMP_ERROR);
		while (lines--) {
			if (SendText(printIO, "\n", 0) != 0)
				return (DUMP_ERROR);
		}
		if (printIO->DRP.io_DestRows <= 0)
			return (DUMP_NOWAIT);
		if (printRec->Orientation == PRT_PORTRAIT)
			printIO->DRP.io_SrcHeight = 1;
		else
			printIO->DRP.io_SrcWidth = 1;
	}
	printIO->DRP.io_Special = SPECIAL_NOFORMFEED | SPECIAL_TRUSTME;
	replyPort = ((IOReqPtr) printIO)->io_Message.mn_ReplyPort;
	SetSignal(0, (1 << replyPort->mp_SigBit));	/* Needed for old printer.device */
	SendIO((IOReqPtr) printIO);
	return (DUMP_WAIT);
}

/*
 *	Send single byte of raw data to printer
 *	Return 0 if OK
 */

static WORD SendRawByte(register PrintIOPtr printIO, UBYTE byte)
{
	printIO->Std.io_Command = PRD_RAWWRITE;
	printIO->Std.io_Length  = 1;
	printIO->Std.io_Data    = (APTR) &byte;
	return ((WORD) DoIO((IOReqPtr) printIO));
}

/*
 *	Send text to printer
 *	If len is 0 then text is NULL terminated
 *	Return 0 if OK
 */

static WORD SendText(register PrintIOPtr printIO, TextPtr s, WORD len)
{
	printIO->Std.io_Command = CMD_WRITE;
	printIO->Std.io_Length  = (len) ? len : strlen(s);
	printIO->Std.io_Data    = (APTR) s;
	return ((WORD) DoIO((IOReqPtr) printIO));
}

/*
 *	Send command to printer
 *	Return 0 if OK (ignore results of -1)
 */

static WORD SendCommand(register PrintIOPtr printIO, UWORD cmd,
						UBYTE p0, UBYTE p1, UBYTE p2, UBYTE p3)
{
	WORD result;

	printIO->Cmd.io_Command    = PRD_PRTCOMMAND;
	printIO->Cmd.io_PrtCommand = cmd;
	printIO->Cmd.io_Parm0      = p0;
	printIO->Cmd.io_Parm1      = p1;
	printIO->Cmd.io_Parm2      = p2;
	printIO->Cmd.io_Parm3      = p3;
	result = DoIO((IOReqPtr) printIO);
	if (result == -1)
		result = 0;
	return (result);
}

/*
 *	Eject page
 *	Return 0 if OK
 *	If doing graphic output then use line feeds for one inch, since some
 *		printers cannot correctly do form feeds after graphic dumps
 */

static WORD EjectPage(PrintRecPtr printRec)
{
	WORD status;
	PrintIOPtr printIO = printRec->PrintIO;

	if (pagePrinter || printRec->PaperFeed == PRT_CUTSHEET)
		status = SendText(printIO, "\f", 0);
	else if (printRec->Flags & PRT_NOGAPS)
		status = SendText(printIO, "\r", 0);		/* Print final line on page */
	else
		status = SendText(printIO, "\033[1z\n\n\n\n\n\n", 0);
	return (status);
}

/*
 *	Force completion of graphics band print
 *	(printer driver will abort without finishing band, leaving some printers in
 *		state waiting for more data)
 *	Return success status
 */

static BOOL FlushDump(register PrintRecPtr printRec)
{
	register LONG width;
	PrintIOPtr printIO = printRec->PrintIO;
	struct PrinterData *pData;
	struct PrinterExtendedData *ped;

	pData = (struct PrinterData *) printIO->Std.io_Device;
	ped = &pData->pd_SegmentData->ps_PED;
	width = ((LONG) ped->ped_MaxXDots*ped->ped_NumRows)/8;
	while (width--) {
		if (SendRawByte(printIO, '\0') != 0)
			return (FALSE);
	}
	return (TRUE);
}

/*
 *	Allocate rastport and colorMap for graphics print, and set up palette(s)
 *	Return success status
 */

static BOOL CreatePrintRPort(PrintRecPtr printRec, WORD width, WORD height)
{
	WORD depth, pass, numPasses;

	depth = printRec->Depth;
/*
	Allocate and initialize structures
	(Make sure there is at least a 16K block of contiguous chip memory left)
*/
	if ((printRec->RPort = CreateRastPort(width, height, depth)) == NULL ||
		AvailMem(MEMF_CHIP | MEMF_LARGEST) < 0x4000 ||
		(printRec->ColorMap = GetColorMap(1 << depth)) == NULL) {
		DisposePrintRPort(printRec);
		return (FALSE);
	}
/*
	Set up print palette(s) (but don't attach to rastport)
*/
	numPasses = (printRec->Depth == PRT_DEPTHFULL) ? 3 : 1;
	for (pass = 0; pass < numPasses; pass++)
		BuildPrintPalette(&prPalettes[pass], printRec->Depth, pass);
	return (TRUE);
}

/*
 *	Dispose of items allocated by CreatePrintRPort
 */

static void DisposePrintRPort(PrintRecPtr printRec)
{
	if (printRec->RPort)
		DisposeRastPort(printRec->RPort);
	if (printRec->ColorMap)
		FreeColorMap(printRec->ColorMap);
	printRec->RPort = NULL;
	printRec->ColorMap = NULL;
}

/*
 *	Wait for print completion or CANCEL button
 */

static WORD WaitPrint(PrintIOPtr printIO)
{
	BOOL abort;
	ULONG signals;
	MsgPortPtr replyPort;

	replyPort = ((IOReqPtr) printIO)->io_Message.mn_ReplyPort;
	abort = FALSE;
	for (;;) {
/*
	First make sure we have handled all intuition messages
*/
		if (CheckDialog(mainMsgPort, cancelDlg, DialogFilter) == CANCEL_BUTTON) {
			SetCancelButton(FALSE);
			SetInfoText(strPrtCancel);
			AbortIO((IOReqPtr) printIO);
			WaitIO((IOReqPtr) printIO);
			abort = TRUE;
			break;
		}
		if (CheckIO((IOReqPtr) printIO) != NULL) {
			while (GetMsg(replyPort)) ;			/* Make sure we get reply */
			break;
		}
		signals = (1L << mainMsgPort->mp_SigBit) | (1L << replyPort->mp_SigBit);
		signals = Wait(signals);
	}
	if (abort)
		return (PRINT_ABORT);
	if (printIO->DRP.io_Error != 0)
		return (PRINT_ERROR);
	return (PRINT_OK);
}

/*
 *	Draw and send graphics dump of page to printer
 */

static WORD DumpPage(PrintRecPtr printRec, DocDataPtr docData, WORD page)
{
	WORD result;
	BOOL dirty;

	SetCancelButton(FALSE);
	SetInfoText(strPrtRender);
	dirty = DrawPage(printRec, docData, page);
	SetInfoText(strPrtPrint);
	result = DumpRPort(printRec, dirty);
	if (result == DUMP_ERROR)
		return (PRINT_ERROR);
	if (result == DUMP_NOWAIT)
		result = PRINT_OK;
	else {
		SetCancelButton(TRUE);
		result = WaitPrint(printRec->PrintIO);
		if (result == PRINT_ABORT) {
			if (!FlushDump(printRec))
				return (PRINT_ERROR);
		}
	}
	return (result);
}

/*
 *	Print document in graphic mode
 */

static WORD PrintGraphic(WindowPtr window, DocDataPtr docData)
{
	WORD width, height, pageWidth, pageHeight, page, copy, result;
	WORD bandPos, bandStart, bandEnd, bandInc;
	WORD dummy;
	register PrintRecPtr printRec = docData->PrintRec;
	register PrintIOPtr printIO;

	result = PRINT_ERROR;				/* Assume error */
/*
	Open printer device for output
*/
	printRec->PrintIO = printIO = OpenPrinter();
	if (printIO == NULL ||
		!SetPrintDensity(printIO, printRec->PrintDensity, (printRec->Flags & PRT_SMOOTH)))
		goto Exit1;
/*
	Allocate graphics print structures
	Always print in 1 inch bands
*/
	pageWidth  = printRec->PageRect.MaxX - printRec->PageRect.MinX + 1;
	pageHeight = printRec->PageRect.MaxY - printRec->PageRect.MinY + 1;
	if (printRec->Orientation == PRT_PORTRAIT) {
		ScaleDocToPrint(printRec, pageWidth, printRec->yDPI, &width, &height);
		height -= height % 8;			/* Make sure patterns align */
	}
	else {
		ScaleDocToPrint(printRec, printRec->xDPI, pageHeight, &width, &height);
		width -= width % 8;			/* Make sure patterns align */
	}
	if (!CreatePrintRPort(printRec, width, height))
		goto Exit2;
/*
	Set up for band printing
*/
	bandStart = 0;
	if (printRec->Orientation == PRT_PORTRAIT) {
		ScaleDocToPrint(printRec, 0, pageHeight, &dummy, &bandEnd);
		bandInc = height;
		printRec->Left = 0;
		printRec->Width = width;
	}
	else {
		ScaleDocToPrint(printRec, pageWidth, 0, &bandEnd, &dummy);
		bandInc = width;
		printRec->Top = 0;
		printRec->Height = height;
	}
/*
	Draw the page contents and print
*/
	StartPage(printRec, &page, &copy);
	while (copy > 0) {
		if (printRec->PaperFeed == PRT_CUTSHEET && !PageDialog(DLG_NEXTPAGE)) {
			result = PRINT_ABORT;		/* Aborted */
			goto Exit3;
		}
		for (bandPos = bandStart; bandPos < bandEnd; bandPos += bandInc) {
			if (printRec->Orientation == PRT_PORTRAIT) {
				printRec->Top = bandPos;
				printRec->Height = MIN(bandInc, bandEnd - bandPos);
			}
			else {
				printRec->Left = bandPos;
				printRec->Width = MIN(bandInc, bandEnd - bandPos);
			}
			result = DumpPage(printRec, docData, page - 1);
			if (result == PRINT_ABORT)
				break;
			if (result == PRINT_ERROR)
				goto Exit3;
		}
		if (EjectPage(printRec) != 0) {
			result = PRINT_ERROR;
			goto Exit3;
		}
		if (result == PRINT_ABORT)
			goto Exit3;
		NextPage(printRec, &page, &copy);
	}
	result = PRINT_OK;
/*
	Clean up
*/
Exit3:
	DisposePrintRPort(printRec);
Exit2:
	ClosePrinter(printIO);
Exit1:
	return (result);
}

/*
 *	Print given page in PostScript
 */

static WORD PrintPSPage(PrintRecPtr printRec, DocDataPtr docData, WORD page)
{
	WORD result, xOffset, yOffset;
	DocObjPtr docObj;
	DocLayerPtr docLayer, currLayer;
	Rectangle objRect, clipRect;

/*
	Set up page
*/
	if (CheckDialog(mainMsgPort, cancelDlg, DialogFilter) == CANCEL_BUTTON) {
		result = PRINT_ABORT;
		goto Exit;
	}
	if (!PSBeginPage(printRec, page)) {
		result = PRINT_ERROR;
		goto Exit;
	}
	xOffset = -(page/PagesDown(docData))*docData->PageWidth;
	yOffset = -(page % PagesDown(docData))*docData->PageHeight;
	SetRect(&clipRect, 0, 0, docData->PageWidth - 1, docData->PageHeight - 1);
/*
	Draw page
*/
	result = PRINT_OK;
	currLayer = CurrLayer(docData);
	docLayer = (docData->Flags & DOC_HIDEBACKLAY) ? currLayer : BottomLayer(docData);
	for (;;) {
		if (docLayer == currLayer || LayerVisible(docLayer)) {
			for (docObj = BottomObject(docLayer); docObj; docObj = NextObj(docObj)) {
				objRect = docObj->Frame;
				OffsetRect(&objRect, xOffset, yOffset);
				if (CheckDialog(mainMsgPort, cancelDlg, DialogFilter) == CANCEL_BUTTON) {
					result = PRINT_ABORT;
					goto Exit;
				}
				if (!DrawPSObject(printRec, docObj, &objRect, &clipRect)) {
					result = PRINT_ERROR;
					goto Exit;
				}
			}
		}
		if (docLayer == currLayer)
			break;
		docLayer = NextLayer(docLayer);
	}
/*
	End and print page
*/
Exit:
	if (result != PRINT_ERROR) {
		if (!PSEndPage(printRec))
			result = PRINT_ERROR;
	}
	return (result);
}

/*
 *	Print the document in PostScript
 */

static WORD PrintPostScript(WindowPtr window, DocDataPtr docData)
{
	WORD page, copy;
	BOOL result;
	register PrintRecPtr printRec = docData->PrintRec;

	result = PRINT_ERROR;				/* Assume error */
/*
	Open device/file for output
*/
	if ((printRec->File = Open(PSDeviceName(), MODE_NEWFILE)) == NULL)
		goto Exit1;
/*
	Begin job
*/
	if (!PSBeginJob(printRec, docData->WindowName, NULL))
		goto Exit2;
/*
	Send the page contents
*/
	SetCancelButton(TRUE);
	SetInfoText(strPrtPrint);
	StartPage(printRec, &page, &copy);
	while (copy > 0) {
		result = PrintPSPage(printRec, docData, (WORD) (page - 1));
		if (result == PRINT_ABORT)
			break;
		if (result == PRINT_ERROR)
			goto Exit3;
		NextPage(printRec, &page, &copy);
	}
	result = PRINT_OK;
/*
	End job
*/
Exit3:
	if (!PSEndJob(printRec, FALSE))
		goto Exit2;
/*
	Clean up
*/
Exit2:
	Close(printRec->File);
Exit1:
	return (result);
}

/*
 *	Enable/disable cancel button and set pointer
 */

static void SetCancelButton(BOOL on)
{
	EnableCancelButton(cancelDlg, on);
	SetStdPointer(cancelDlg, (WORD) (on ? POINTER_ARROW : POINTER_WAIT));
}

/*
 *	Set info text in cancel dialog
 */

static void SetInfoText(TextPtr text)
{
	SetGadgetItemText(cancelDlg->FirstGadget, INFO_TEXT, cancelDlg,  NULL, text);
}

/*
 *	Print document
 *	Return FALSE if error or abort, TRUE if printed ok
 */

BOOL PrintDocument(WindowPtr window, DocDataPtr docData)
{
	WORD result;
	PrintRecPtr printRec = docData->PrintRec;

	printError = TRUE;
	if (!graphicPrinter)
		return (FALSE);
/*
	Put up cancel requester
*/
	BeginWait();
	if ((cancelDlg = GetDialog(dlgList[DLG_CANCELPRINT], screen, mainMsgPort)) == NULL) {
		EndWait();
		return (FALSE);
	}
	SetCancelButton(FALSE);
	SetInfoText(strPrtSetup);
	RefreshWindows();
/*
	Print document in desired mode
*/
	if (printRec->FirstPage < 1)
		printRec->FirstPage = 1;
	if (printRec->LastPage > PagesAcross(docData)*PagesDown(docData))
		printRec->LastPage = PagesAcross(docData)*PagesDown(docData);
	if (printRec->FirstPage > printRec->LastPage)
		result = PRINT_ERROR;
	else if (printRec->Quality == PRT_POSTSCRIPT)
		result = PrintPostScript(window, docData);
	else
		result = PrintGraphic(window, docData);
/*
	Remove requester
*/
	DisposeDialog(cancelDlg);
	EndWait();
	printError = (result == PRINT_ERROR);
	return ((BOOL) (result == PRINT_OK));
}

/*
 *	Return TRUE if last print job had a printer error
 *	(Return FALSE if normal completion or abort)
 */

BOOL PrintError()
{
	return (printError);
}
