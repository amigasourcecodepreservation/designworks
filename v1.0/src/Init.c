/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Initialization
 */

#include <exec/types.h>
#include <intuition/intuition.h>
#include <workbench/startup.h>
#include <libraries/dos.h>
#include <libraries/dosextens.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include <string.h>
#include <stdlib.h>				/* For exit() prototype */

#include <Toolbox/Graphics.h>
#include <Toolbox/Menu.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Window.h>
#include <Toolbox/Utility.h>
#include <Toolbox/StdFile.h>

#include "Draw.h"
#include "Proto.h"

/*
 *	External variables
 */

extern struct IntuitionBase	*IntuitionBase;
extern struct GfxBase		*GfxBase;
extern struct LayersBase	*LayersBase;
extern struct Library		*IconBase;
extern struct Library		*DiskfontBase;
extern struct Device		*ConsoleDevice;

extern Options	options;

extern TextChar	progPathName[];

extern BOOL	onPubScreen, fromCLI;

extern WORD	intuiVersion;

extern struct WBStartup	*WBenchMsg;

extern MenuTemplate		docWindMenus[], altWindMenus[];

extern MenuPtr		docMenuStrip;
extern ScreenPtr	screen;
extern WindowPtr	backWindow;
extern MsgPortPtr	mainMsgPort;
extern MsgPort		monitorMsgPort;

extern ColorTable		screenColors;
extern InvColorTable	scrnInvColorTable;

extern GadgetTemplate	windowGadgets[];

extern TextFontPtr	screenFont, smallFont;

extern TextPtr	initErrors[];
extern TextChar	strCancel[];

extern DlgTemplPtr	dlgList[];

/*
 *	Local variables and definitions
 */

#define MAX_FILENAME_LEN	31

static struct IOStdReq	consoleIOReq;

static TaskPtr	monitorTask;
static APTR		origWindowPtr;
static Dir		startupDir;

/*
 *	Local prototypes
 */

void	InitError(WORD);
void	BuildProgPathName(TextPtr, Dir);

BOOL	IsPubScreen(ScreenPtr);
void	WaitVisitor(void);

/*
 *	Abort initialization
 *	Display error message in workbench screen
 */

static void InitError(WORD errNum)
{
	IntuiText bodyText, negText;

	if (IntuitionBase) {
		bodyText.FrontPen	= negText.FrontPen  = AUTOFRONTPEN;
		bodyText.BackPen	= negText.BackPen   = AUTOBACKPEN;
		bodyText.DrawMode	= negText.DrawMode  = AUTODRAWMODE;
		bodyText.LeftEdge	= negText.LeftEdge  = AUTOLEFTEDGE;
		bodyText.TopEdge	= negText.TopEdge   = AUTOTOPEDGE;
		bodyText.ITextFont	= negText.ITextFont = AUTOITEXTFONT;
		bodyText.NextText	= negText.NextText  = AUTONEXTTEXT;
		bodyText.IText		= initErrors[errNum];
		negText.IText		= strCancel;
		SysBeep(5);
		DisplayBeep(NULL);
		AutoRequest(NULL, &bodyText, NULL, &negText, 0L, 0L, 250, 50);
	}
	ShutDown();
	exit(RETURN_FAIL);
}

/*
 *	Build path name to program from given parameters
 *	progPathName[] is 100 bytes in size
 *	Note: Cannot use ProgDir: in system 2.0, since this path & name is used for
 *		document icon default tools
 */

static void BuildProgPathName(TextPtr fileName, Dir dirLock)
{
	register TextPtr newName;
	register WORD nameLen, newLen;
	register Dir dir, parentDir;
	struct FileInfoBlock *fib;

	strcpy(progPathName, fileName);
	if ((fib = MemAlloc(sizeof(struct FileInfoBlock), 0)) == NULL)
		return;
	newName = fib->fib_FileName;
	dir = DupLock(dirLock);
	while (dir) {
		if (!Examine(dir, fib))
			break;
		nameLen = strlen(progPathName);
		newLen = strlen(newName);
		if (nameLen + newLen > 98)
			break;
		BlockMove(progPathName, progPathName + newLen + 1, nameLen + 1);
		BlockMove(newName, progPathName, newLen);
		parentDir = ParentDir(dir);
		progPathName[newLen] = (parentDir) ? '/' : ':';
		UnLock(dir);
		dir = parentDir;
	}
	if (dir)
		UnLock(dir);
	MemFree(fib, sizeof(struct FileInfoBlock));
}

/*
 *	Initialization routine
 *	Open necessary libraries and devices, and open background window
 */

void Init(int argc, char *argv[])
{
	WORD barHeight, numColors;
	Dir dir;
	TextPtr progName;
	ProcessPtr process;
	DialogPtr initDlg;
	MenuTemplPtr menuTempl;

	fromCLI = (argc > 0);
	process = (ProcessPtr) FindTask(NULL);
/*
	Build path name to program
	(Used to find program icon, prefs file and dictionary)
*/
	dir = DupLock(process->pr_CurrentDir);
	startupDir = CurrentDir(dir);	/* Workbench needs the original lock */
	if (argc)							/* Running under CLI */
		progName = argv[0];
	else								/* Running under Workbench */
		progName = WBenchMsg->sm_ArgList->wa_Name;
	dir = ConvertFileName(progName);
	BuildProgPathName(progName, dir);
	UnLock(dir);
/*
	Open needed libraries
*/
	IntuitionBase = (struct IntuitionBase *)
				OpenLibrary("intuition.library", OSVERSION_1_2);
	GfxBase = (struct GfxBase *)
				OpenLibrary("graphics.library", OSVERSION_1_2);
	LayersBase = (struct LayersBase *)
				OpenLibrary("layers.library", OSVERSION_1_2);
	if (IntuitionBase == NULL || GfxBase == NULL || LayersBase == NULL)
		InitError(INIT_BADSYS);
	IconBase = OpenLibrary("icon.library", OSVERSION_1_2);
	if (IconBase == NULL)
		InitError(INIT_NOICON);
	DiskfontBase = OpenLibrary("diskfont.library", OSVERSION_1_2);
	if (DiskfontBase == NULL)
		InitError(INIT_NOFONT);
	intuiVersion = LibraryVersion((struct Library *) IntuitionBase);
/*
	Open console device and get base vector
	Needed for RawKeyConvert() function call
*/
	if (OpenDevice("console.device", -1, (IOReqPtr) &consoleIOReq, 0))
		InitError(INIT_BADSYS);
	ConsoleDevice = consoleIOReq.io_Device;
/*
	Open screen (or get pointer to public screen) and set colors
*/
	if ((screen = GetScreen(argc, argv)) == NULL)
		InitError(INIT_NOSCREEN);
/*
	Adjust parameters for document windows and gadgets
*/
	barHeight = screen->BarHeight;
	windowGadgets[VERT_SCROLL].TopOffset	+= barHeight;
	windowGadgets[VERT_SCROLL].HeightOffset	-= barHeight;
	if (intuiVersion >= OSVERSION_2_0) {
		windowGadgets[VERT_SCROLL].LeftOffset	-= 1;
		windowGadgets[UP_ARROW].LeftOffset		-= 1;
		windowGadgets[DOWN_ARROW].LeftOffset	-= 1;
	}
/*
	Start the monitor task
*/
	if ((mainMsgPort = CreatePort(NULL, 0)) == NULL)
		InitError(INIT_NOMEM);
	monitorTask = CreateTask(NULL,
							 (UBYTE) (process->pr_Task.tc_Node.ln_Pri + 1),
							 (APTR) MonitorTask, 1000L);
	if (monitorTask == NULL)
		InitError(INIT_NOMEM);
/*
	Open background window and display init requester
	(Monitor task will have set up monitorMsgPort)
*/
	if ((backWindow = OpenBackWindow()) == NULL)
		InitError(INIT_NOMEM);
	SetStdPointer(backWindow, POINTER_WAIT);
	if (onPubScreen) {
		initDlg = NULL;
		if (intuiVersion >= OSVERSION_2_0)
			UnlockPubScreen(NULL, screen);	/* Recommended procedure */
	}
	else {
		initDlg = GetDialog(dlgList[DLG_INIT], screen, mainMsgPort);
		if (initDlg)
			SetStdPointer(initDlg, POINTER_WAIT);
	}
/*
	Have system requesters appear in this screen
*/
	origWindowPtr = process->pr_WindowPtr;
	process->pr_WindowPtr = (APTR) backWindow;
/*
	Set up menus
*/
	menuTempl = (screen->ViewPort.Modes & HIRES) ? docWindMenus : altWindMenus;
	if ((docMenuStrip = GetMenuStrip(menuTempl)) == NULL) {
		if (initDlg)
			DisposeDialog(initDlg);
		InitError(INIT_NOMEM);
	}
	InsertMenuStrip(backWindow, docMenuStrip);
/*
	Misc initialization
*/
	SetAllMenus();		/* Must do before BeginWait() */
	UndoOff();
	BeginWait();
	if (!InitFonts()) {
		if (initDlg)
			DisposeDialog(initDlg);
		InitError(INIT_NOMEM);
	}
	GetSysPrefs();
	InitPrintHandler();
	GetProgPrefs();		/* Do this here to minimize disk swapping */
	InitRexx();
	SetMacroMenu();
	InitIcons();
	numColors = GetColorTable(screen, &screenColors);
	MakeInvColorTable(&screenColors, numColors, &scrnInvColorTable);
	if (initDlg)
		DisposeDialog(initDlg);
	SetStdPointer(backWindow, POINTER_ARROW);
	EndWait();
}

/*
 *	Determine whether the specified screen is a public screen
 */

static BOOL IsPubScreen(ScreenPtr screen)
{
	struct PubScreenNode *psn;

	if (intuiVersion < OSVERSION_2_0)
		return (FALSE);
	psn = (struct PubScreenNode *) LockPubScreenList();
	while (psn) {
		if (psn->psn_Screen == screen)
			break;
		psn = (struct PubScreenNode *) psn->psn_Node.ln_Succ;
	}
	UnlockPubScreenList();
	return (psn != NULL);
}

/*
 *	Wait until all visitor windows are closed on screen
 */

static void WaitVisitor()
{
	register WindowPtr window;
	register MsgPtr msg;

	if (onPubScreen || screen == NULL || backWindow == NULL)
		return;
/*
	Busy loop, since we won't receive INTUITICKS if backWindow is not active
*/
	ModifyIDCMP(backWindow, REFRESHWINDOW);
	for (;;) {
		while (msg = GetMsg(mainMsgPort))
			ReplyMsg(msg);
		for (window = screen->FirstWindow; window; window = window->NextWindow) {
			if (window != backWindow)
				break;
		}
		if (window == NULL) {			/* No visitor windows found */
			if (intuiVersion < OSVERSION_2_0 ||
				!IsPubScreen(screen) ||
				(PubScreenStatus(screen, PSNF_PRIVATE) & PSNF_PRIVATE))
				break;
		}
		Delay(10L);						/* Wait 1/5 second before checking again */
	}
}

/*
 *	Shut down program
 *	Close backWindow, close libraries, and quit
 */

void ShutDown()
{
	register Dir dir;
	register ProcessPtr process;
	MsgPtr msg;

	CloseToolWindow();
	ClosePenWindow();
	CloseFillWindow();
	ShutDownIcons();
	ShutDownRexx();
	ShutDownFonts();
	if (smallFont)
		CloseFont(smallFont);
	if (screenFont)
		CloseFont(screenFont);
	if (backWindow) {
		process = (ProcessPtr) FindTask(NULL);
		process->pr_WindowPtr = origWindowPtr;
		ClearMenuStrip(backWindow);
		WaitVisitor();
		CloseWindowSafely(backWindow, mainMsgPort);
	}
	if (monitorTask) {		/* Need Forbid/Permit to work with MemMung */
		Forbid();
		DeleteTask(monitorTask);
		Permit();
	}
	if (mainMsgPort) {
		while ((msg = GetMsg(mainMsgPort)) != NULL)
			ReplyMsg(msg);
		DeletePort(mainMsgPort);
	}
	if (docMenuStrip)
		DisposeMenuStrip(docMenuStrip);
	if (!onPubScreen && screen)
		CloseScreen(screen);
	if (ConsoleDevice)
		CloseDevice((IOReqPtr) &consoleIOReq);
	if (DiskfontBase)
		CloseLibrary(DiskfontBase);
	if (IconBase)
		CloseLibrary(IconBase);
	if (LayersBase)
		CloseLibrary((struct Library *) LayersBase);
	if (GfxBase)
		CloseLibrary((struct Library *) GfxBase);
	if (IntuitionBase) {
		OpenWorkBench();
		CloseLibrary((struct Library *) IntuitionBase);
	}
	dir = CurrentDir(startupDir);		/* Keep Workbench happy */
	UnLock(dir);
}

/*
 *	Open initial files, or open new window if none given
 */

void SetUp(int argc, char *argv[])
{
	register TextPtr fileName;
	register WORD i, numFiles, len;
	register BOOL fileOpened;
	register Dir dir;
	register struct WBArg *wbArgList;

	fileOpened = FALSE;
/*
	Get number of files to open
*/
	if (argc)						/* Running under CLI */
		numFiles = argc;
	else							/* Running under Workbench */
		numFiles = WBenchMsg->sm_NumArgs;
/*
	Open files
	First check to see if this is a prefs file, if so then load prefs
*/
	for (i = 1; i < numFiles; i++) {
		if (argc) {						/* Running under CLI */
			fileName = argv[i];
			if (*fileName == '-')		/* Ignore program options */
				continue;
			SetCurrentDir(startupDir);	/* Path is relative to startupDir */
			dir = ConvertFileName(fileName);
		}
		else {							/* Running under Workbench */
			wbArgList = WBenchMsg->sm_ArgList;
			fileName = wbArgList[i].wa_Name;
			dir = DupLock(wbArgList[i].wa_Lock);
		}
		len = strlen(fileName);
		if (len == 0 || len > MAX_FILENAME_LEN)
			UnLock(dir);
		else {
			SetCurrentDir(dir);
			if (ReadProgPrefs(fileName))
				UnLock(dir);
			else if (OpenFile(fileName, dir))
				fileOpened = TRUE;
		}
	}
/*
	If no file opened, then open new window
*/
	if (!fileOpened)
		DoNew();
}
