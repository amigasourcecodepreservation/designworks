/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Toolbox library
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	AREXX support routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <string.h>

#include <proto/exec.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include <Typedefs.h>

#include <Toolbox/Dialog.h>
#include <Toolbox/StdFile.h>
#include <Toolbox/Utility.h>
#include <Toolbox/DOS.h>
#include <Toolbox/AREXX.h>

/*
 *	External variables
 */

extern TextChar	_tbStrOpenMacro[];

extern DialogTemplate	_tbMacroNameDlgTempl;

/*
 *	Local variables and definitions
 */

enum {
	MACRONAME_TEXT = 2,
	MACROARG_TEXT,
	MACROLIST_BTN
};

/*
 *	Local prototypes
 */

static BOOL	MacroFileFilter(TextPtr);
static BOOL	GetMacroNameList(ScreenPtr, MsgPortPtr, BOOL (*)(IntuiMsgPtr, WORD *), TextPtr);

/*
 *	Check to see if file is (possibly) a macro file
 *	Returns TRUE if file should be shown in SFPGetFile() list
 */

static BOOL MacroFileFilter(TextPtr fileName)
{
	WORD		len;
	BOOL		success;
	File		file;
	TextChar	buff[2];

	len = strlen(fileName);
	if (len > 2 && CmpString(fileName + len - 2, ".c", 2, 2, FALSE) == 0)
		return (FALSE);
	if ((file = Open(fileName, MODE_OLDFILE)) == NULL)
		return (FALSE);
	success = (Read(file, buff, 2) == 2 && buff[0] == '/' && buff[1] == '*');
	Close(file);
	return (success);
}

/*
 *	Get macro name from file list
 *	Put full path/filename in buffer
 *	Return FALSE if user canceled operation
 */

static BOOL GetMacroNameList(ScreenPtr screen, MsgPortPtr msgPort,
							 BOOL (*dlgFilter)(IntuiMsgPtr, WORD *),
							 TextPtr fileName)
{
	WORD		pathLen;
	BOOL		success;
	TextChar	ch;
	SFReply		sfReply;

	SFPGetFile(screen, msgPort, _tbStrOpenMacro, dlgFilter, NULL, NULL, 0, NULL,
			   MacroFileFilter, &sfReply);
	if (sfReply.Result != SFP_OK)
		return (FALSE);
/*
	Convert file name and lock into full path & file name
*/
	success = NameFromLock1(sfReply.DirLock, fileName, GADG_MAX_STRING);
	UnLock(sfReply.DirLock);
	if (success) {
		pathLen = strlen(fileName);
		if (pathLen + strlen(sfReply.Name) + 2 > GADG_MAX_STRING)
			success = FALSE;
		else {
			ch = fileName[pathLen - 1];
			if (ch != '/' && ch != ':')
				strcat(fileName, "/");
			strcat(fileName, sfReply.Name);
		}
	}
	return (success);
}

/*
 *	Get macro name
 */

BOOL GetMacroName(ScreenPtr screen, MsgPortPtr msgPort,
				  BOOL (*dlgFilter)(IntuiMsgPtr, WORD *),
				  TextPtr macroName, TextPtr arg)
{
	WORD		i, len, dlgItem;
	BOOL		done, hasColon, hasSlash;
	GadgetPtr	gadgList;
	DialogPtr	dlg;
	TextChar	fileName[GADG_MAX_STRING];

/*
	Get macro name from user
*/
	if ((dlg = GetDialog(&_tbMacroNameDlgTempl, screen, msgPort)) == NULL)
		return (FALSE);
	gadgList = dlg->FirstGadget;
	OutlineButton(GadgetItem(gadgList, 0), dlg, NULL, TRUE);
	done = FALSE;
	do {
		WaitPort(msgPort);
		dlgItem = CheckDialog(msgPort, dlg, dlgFilter);
		GetEditItemText(gadgList, MACRONAME_TEXT, macroName);
		switch (dlgItem) {
		case -1:
			EnableGadgetItem(gadgList, OK_BUTTON, dlg, NULL, (strlen(macroName) != 0));
			break;
		case OK_BUTTON:
		case CANCEL_BUTTON:
			done = TRUE;
			break;
		case MACROLIST_BTN:
			if (GetMacroNameList(screen, msgPort, dlgFilter, fileName))
				SetEditItemText(gadgList, MACRONAME_TEXT, dlg, NULL, fileName);
			break;
		}
	} while (!done);
	GetEditItemText(gadgList, MACROARG_TEXT, arg);
	DisposeDialog(dlg);
	if (dlgItem == CANCEL_BUTTON)
		return (FALSE);
/*
	Check for valid name (relative path names are not allowed)
*/
	hasColon = hasSlash = FALSE;
	len = strlen(macroName);
	for (i = 0; i < len; i++) {
		if (macroName[i] == ':')
			hasColon = TRUE;
		if (macroName[i] == '/')
			hasSlash = TRUE;
	}
	if (len == 0 || (hasSlash && !hasColon)) {
		SysBeep(5);
		return (FALSE);
	}
	return (TRUE);
}
