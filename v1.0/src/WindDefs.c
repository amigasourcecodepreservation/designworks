/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Window, screen, and window gadget definitions
 *	(except for items that must be in chip memory)
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <Toolbox/Utility.h>
#include <Toolbox/Gadget.h>

#include "Draw.h"

/*
 *	External variables
 */

extern TextChar strScreenTitle[];

/*
 *	Screen definition
 */

struct NewScreen newScreen = {
	0, 0, 640, STDSCREENHEIGHT, 3, 0, 1,
	HIRES | LACE, CUSTOMSCREEN,
	NULL, &strScreenTitle[0], NULL, NULL
};

/*
 *	Window definitions
 *	Use simple refresh on document windows for improved menu speed
 */

struct NewWindow newBackWindow = {
	0, 0, 640, 400, -1, -1,
	MOUSEBUTTONS | MENUPICK | REFRESHWINDOW | ACTIVEWINDOW | INACTIVEWINDOW |
		RAWKEY | NEWPREFS,
	SIMPLE_REFRESH | BACKDROP | BORDERLESS | ACTIVATE,
	NULL, NULL, NULL, NULL, NULL,
	0, 0, 0, 0,
	CUSTOMSCREEN
};

struct NewWindow newWindow = {
	0, 0, 640, 400, -1, -1,
	MOUSEBUTTONS | GADGETDOWN | GADGETUP | CLOSEWINDOW |
		MENUPICK | MENUVERIFY | NEWSIZE | REFRESHWINDOW |
		ACTIVEWINDOW | INACTIVEWINDOW | RAWKEY | INTUITICKS,
	WINDOWSIZING | SIZEBRIGHT | SIZEBBOTTOM | WINDOWDEPTH | WINDOWCLOSE |
		WINDOWDRAG | SIMPLE_REFRESH | ACTIVATE,
	NULL, NULL, NULL, NULL, NULL,
	200, 100, 0xFFFF, 0xFFFF,
	CUSTOMSCREEN
};

/*
 *	Gadget template for document window
 */

GadgetTemplate windowGadgets[] = {
	{ GADG_ACTIVE_STDIMAGE,
		0, 0, 1 - IMAGE_ARROW_WIDTH, 1 - 3*IMAGE_ARROW_HEIGHT,
		0, 0, IMAGE_ARROW_WIDTH, IMAGE_ARROW_HEIGHT,
		(Ptr) IMAGE_UP_ARROW },

	{ GADG_ACTIVE_STDIMAGE,
		0, 0, 1 - IMAGE_ARROW_WIDTH, 1 - 2*IMAGE_ARROW_HEIGHT,
		0, 0, IMAGE_ARROW_WIDTH, IMAGE_ARROW_HEIGHT,
		(Ptr) IMAGE_DOWN_ARROW },

	{ GADG_ACTIVE_STDIMAGE,
		0, 0, 1 - 3*IMAGE_ARROW_WIDTH, 1 - IMAGE_ARROW_HEIGHT,
		0, 0, IMAGE_ARROW_WIDTH, IMAGE_ARROW_HEIGHT,
		(Ptr) IMAGE_LEFT_ARROW },

	{ GADG_ACTIVE_STDIMAGE,
		0, 0, 1 - 2*IMAGE_ARROW_WIDTH, 1 - IMAGE_ARROW_HEIGHT,
		0, 0, IMAGE_ARROW_WIDTH, IMAGE_ARROW_HEIGHT,
		(Ptr) IMAGE_RIGHT_ARROW },

	{ GADG_PROP_VERT,
		0, 0, 1 - IMAGE_ARROW_WIDTH, 0,
		0, 0, IMAGE_ARROW_WIDTH, -3*IMAGE_ARROW_HEIGHT,
		NULL },

	{ GADG_PROP_HORIZ,
		0, 0, LYINDIC_EDGE + LYINDIC_WIDTH, 1 - IMAGE_ARROW_HEIGHT,
		0, 0, -3*IMAGE_ARROW_WIDTH - LYINDIC_EDGE - LYINDIC_WIDTH, IMAGE_ARROW_HEIGHT,
		NULL },

	{ GADG_ACTIVE_STDIMAGE,
		0, 0, LYINDIC_EDGE - 2*IMAGE_ARROW_WIDTH, 1 - IMAGE_ARROW_HEIGHT,
		0, 0, IMAGE_ARROW_WIDTH, IMAGE_ARROW_HEIGHT,
		(Ptr) IMAGE_UP_ARROW },

	{ GADG_ACTIVE_STDIMAGE,
		0, 0, LYINDIC_EDGE - IMAGE_ARROW_WIDTH, 1 - IMAGE_ARROW_HEIGHT,
		0, 0, IMAGE_ARROW_WIDTH, IMAGE_ARROW_HEIGHT,
		(Ptr) IMAGE_DOWN_ARROW },

	{ GADG_ITEM_NONE }
};
