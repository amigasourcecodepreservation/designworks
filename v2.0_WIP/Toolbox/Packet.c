/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Toolbox library
 *	Copyright (c) 1993 New Horizons Software, Inc.
 *
 *	Packet I/O routines
 */

#include <exec/types.h>
#include <exec/memory.h>

#include <string.h>

#include <proto/exec.h>

#include <Toolbox/Packet.h>

/*
 *	Local variables and definitions
 */

static LONG	packetErr;

/*
 *	Process a DOS packet
 *	Return TRUE on success, FALSE on NULL procID, no memory, or failure
 */

BOOL DoPacket(MsgPortPtr procID, LONG action, LONG args[], WORD numArgs)
{
	BOOL				success;
	register WORD		i;
	register LONG		*pktArgs;
	register StdPktPtr	packet;
	MsgPortPtr			replyPort;

	success = FALSE;
	if (procID) {
		packet = AllocMem(sizeof(StandardPacket), MEMF_PUBLIC | MEMF_CLEAR);
		if (packet) {
			replyPort = (MsgPortPtr) CreatePort(NULL, NULL);
			if (replyPort) {
				packet->sp_Msg.mn_Node.ln_Name = (BYTE *) &packet->sp_Pkt;
				packet->sp_Pkt.dp_Link = &packet->sp_Msg;
				packet->sp_Pkt.dp_Port = replyPort;
				packet->sp_Pkt.dp_Type = action;
				pktArgs = &packet->sp_Pkt.dp_Arg1;
				for (i = 0; i < numArgs; i++)
					pktArgs[i] = args[i];
				PutMsg(procID, (MsgPtr) packet);		// Send the packet
				WaitPort(replyPort);
				GetMsg(replyPort);
				success = (packet->sp_Pkt.dp_Res1 != DOSFALSE);
				if (!success)
					packetErr = packet->sp_Pkt.dp_Res2;
			}
			DeletePort(replyPort);
		}
		FreeMem(packet, sizeof(StandardPacket));
	}
	return (success);
}

/*
 *	Return error number of last bad packet
 */

LONG PacketError()
{
	return (packetErr);
}
