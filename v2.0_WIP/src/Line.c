/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Line handling routines
 */

#include <exec/types.h>
#include <graphics/gfxmacros.h>
#include <intuition/intuition.h>

#include <proto/graphics.h>

#include <Toolbox/Memory.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Window.h>

#include "Draw.h"
#include "Proto.h"

/*
 *	External variables
 */

extern Defaults	defaults;

/*
 *	Local variables and definitions
 */

#define LINE_PAT	0xFFFF		// Line pattern for object creation

static LineObjPtr	growLineObj;
static BOOL			wasMoved;
static WORD			growHandle;

/*
 *	Local prototypes
 */

static void	SetLinePoints(LineObjPtr, PointPtr, PointPtr);
static void	GetLinePoints(LineObjPtr, PointPtr, PointPtr);

static LineObjPtr	CreateLine(DocDataPtr, PointPtr, PointPtr);

static void	DrawThickLine(RastPtr, PointPtr, PointPtr, PointPtr);

static void	DrawArrow(RastPtr, LineObjPtr, RectPtr, BOOL);

static void	LineTrackCreate(WindowPtr, PointPtr, PointPtr, BOOL);
static void	LineConstrainCreate(PointPtr, PointPtr);

static void	LineTrackGrow(WindowPtr, PointPtr, PointPtr, BOOL);
static BOOL DrawPSArrow(PrintRecPtr, LineObjPtr, BOOL, RectPtr);

/*
 *	Allocate a new line object
 */

LineObjPtr LineAllocate()
{
	return ((LineObjPtr) MemAlloc(sizeof(LineObj), MEMF_CLEAR));
}

/*
 *	Dispose of line object
 */

void LineDispose(LineObjPtr lineObj)
{
	MemFree(lineObj, sizeof(LineObj));
}

/*
 *	Set line start and end points
 */

static void SetLinePoints(register LineObjPtr lineObj,
						  register PointPtr pt1, register PointPtr pt2)
{
	SetFrameRect(&lineObj->DocObj.Frame, pt1->x, pt1->y, pt2->x, pt2->y);
	lineObj->X = (pt1->x > pt2->x);
	lineObj->Y = (pt1->y > pt2->y);
}

/*
 *	Get line start and end points
 */

static void GetLinePoints(register LineObjPtr lineObj,
						  register PointPtr pt1, register PointPtr pt2)
{
	if (lineObj->X) {
		pt1->x = lineObj->DocObj.Frame.MaxX;
		pt2->x = lineObj->DocObj.Frame.MinX;
	}
	else {
		pt1->x = lineObj->DocObj.Frame.MinX;
		pt2->x = lineObj->DocObj.Frame.MaxX;
	}
	if (lineObj->Y) {
		pt1->y = lineObj->DocObj.Frame.MaxY;
		pt2->y = lineObj->DocObj.Frame.MinY;
	}
	else {
		pt1->y = lineObj->DocObj.Frame.MinY;
		pt2->y = lineObj->DocObj.Frame.MaxY;
	}
}

/*
 *	Create line with given start and end positions
 */

static LineObjPtr CreateLine(DocDataPtr docData, PointPtr pt1, PointPtr pt2)
{
	register LineObjPtr	lineObj;

	if ((lineObj = (LineObjPtr) NewDocObject(CurrLayer(docData), TYPE_LINE)) == NULL)
		return (NULL);
	SetLinePoints(lineObj, pt1, pt2);
	lineObj->DocObj.Flags	= defaults.ObjFlags;
	lineObj->LineFlags		= defaults.LineFlags;
	lineObj->PenWidth		= defaults.PenWidth;
	lineObj->PenHeight		= defaults.PenHeight;
	lineObj->PenColor		= defaults.PenColor;
	return (lineObj);
}

/*
 *	Draw line with given thickness
 */

static void DrawThickLine(RastPtr rPort, PointPtr pt1, PointPtr pt2, PointPtr pen)
{
	register WORD	i, j, w, h;

	w = pen->x;
	h = pen->y;
	for (i = 0; i < w; i++) {
		for (j = 0; j < h; j++) {
			if (i != 0 && j != 0 && i != w - 1 && j != h - 1)
				continue;
			Move(rPort, pt1->x + i - w/2, pt1->y + j - h/2);
			Draw(rPort, pt2->x + i - w/2, pt2->y + j - h/2);
		}
	}
}

/*
 *	Draw arrow at start or end
 */

static void DrawArrow(RastPtr rPort, LineObjPtr lineObj, RectPtr dstRect, BOOL atEnd)
{
	WORD	dx, dy, r, len;
	Point	pt1, pt2, ptList[3];

	GetLinePoints(lineObj, &pt1, &pt2);
	if (atEnd) {
		ptList[0] = pt2;
		dx = pt1.x - pt2.x;
		dy = pt1.y - pt2.y;
	}
	else {
		ptList[0] = pt1;
		dx = pt2.x - pt1.x;
		dy = pt2.y - pt1.y;
	}
	r = LineLength(&pt1, &pt2);
	if (r == 0)
		return;
	len = 4 + (lineObj->PenWidth + lineObj->PenHeight)/2;
	ptList[1].x = ptList[0].x + len*(dx*2 + dy)/r;
	ptList[1].y = ptList[0].y + len*(dy*2 - dx)/r;
	ptList[2].x = ptList[0].x + len*(dx*2 - dy)/r;
	ptList[2].y = ptList[0].y + len*(dy*2 + dx)/r;
	MapPt(&ptList[0], &lineObj->DocObj.Frame, dstRect);
	MapPt(&ptList[1], &lineObj->DocObj.Frame, dstRect);
	MapPt(&ptList[2], &lineObj->DocObj.Frame, dstRect);
	FillPoly(rPort, 3, ptList);
}

/*
 *	Draw line object
 */

void LineDrawObj(RastPtr rPort, LineObjPtr lineObj, RectPtr dstRect, RectPtr clipRect)
{
	Point	pen, pt1, pt2;

	if (HasPen(lineObj)) {
		pen.x = lineObj->PenWidth;
		pen.y = lineObj->PenHeight;
		ScalePt(&pen, &lineObj->DocObj.Frame, dstRect);
		GetLinePoints(lineObj, &pt1, &pt2);
		MapPt(&pt1, &lineObj->DocObj.Frame, dstRect);
		MapPt(&pt2, &lineObj->DocObj.Frame, dstRect);
		PenNormal(rPort);
		RGBForeColor(rPort, lineObj->PenColor);
		DrawThickLine(rPort, &pt1, &pt2, &pen);
		if (lineObj->LineFlags & LINE_ARROWSTART)
			DrawArrow(rPort, lineObj, dstRect, FALSE);
		if (lineObj->LineFlags & LINE_ARROWEND)
			DrawArrow(rPort, lineObj, dstRect, TRUE);
	}
}

/*
 *	Draw line outline (for creation/dragging)
 *	Offset is in document coordinates
 */

void LineDrawOutline(WindowPtr window, LineObjPtr lineObj, WORD xOffset, WORD yOffset)
{
	RastPtr	rPort = window->RPort;
	Point	start, end;

	GetLinePoints(lineObj, &start, &end);
	OffsetPoint(&start, xOffset, yOffset);
	OffsetPoint(&end,   xOffset, yOffset);
	DocToWindow(window, start.x, start.y, &start.x, &start.y);
	DocToWindow(window, end.x, end.y, &end.x, &end.y);
	SetDrPt(rPort, LINE_PAT);
	SetDrawComplement(rPort);
	Move(rPort, start.x, start.y);
	Draw(rPort, end.x, end.y);
	ClearDrawComplement(rPort);
}

/*
 *	Draw selection hilighting for line
 */

void LineHilite(WindowPtr window, LineObjPtr lineObj)
{
	Point	start, end;

	GetLinePoints(lineObj, &start, &end);
	DrawHandle(window, start.x, start.y);
	DrawHandle(window, end.x, end.y);
}

/*
 *	Set line pen color
 */

void LineSetPenColor(LineObjPtr lineObj, RGBColor penColor)
{
	lineObj->PenColor = penColor;
}

/*
 *	Set line pen size
 *	If size is -1 then do not change
 */

void LineSetPenSize(LineObjPtr lineObj, WORD penWidth, WORD penHeight)
{
	if (penWidth != -1)
		lineObj->PenWidth = penWidth;
	if (penHeight != -1)
		lineObj->PenHeight = penHeight;
}

/*
 *	Set line fill pattern
 */

void LineSetFillPat(LineObjPtr lineObj, RGBPat8Ptr fillPat)
{
}

/*
 *	Get line fill pattern
 */

BOOL LineGetFillPat(LineObjPtr lineObj, RGBPat8Ptr fillPat)
{
	return (FALSE);
}

/*
 *	Rotate line by given angle
 */

void LineRotate(LineObjPtr lineObj, WORD cx, WORD cy, WORD angle)
{
	Point	pt1, pt2;

	GetLinePoints(lineObj, &pt1, &pt2);
	RotatePoint(&pt1, cx, cy, angle);
	RotatePoint(&pt2, cx, cy, angle);
	SetLinePoints(lineObj, &pt1, &pt2);
}

/*
 *	Flip line horizontally or vertically
 */

void LineFlip(LineObjPtr lineObj, WORD cx, WORD cy, BOOL horiz)
{
	Point	pt1, pt2;

	GetLinePoints(lineObj, &pt1, &pt2);
	FlipPoint(&pt1, cx, cy, horiz);
	FlipPoint(&pt2, cx, cy, horiz);
	SetLinePoints(lineObj, &pt1, &pt2);
}

/*
 *	Scale line to new frame rectangle
 */

void LineScale(LineObjPtr lineObj, RectPtr frame)
{
	lineObj->DocObj.Frame = *frame;
}

/*
 *	Create poly equivalent to given line
 *	Returns pointer to poly, or NULL if error
 */

PolyObjPtr LineConvertToPoly(LineObjPtr lineObj)
{
	PolyObjPtr	polyObj;
	Point		pt1, pt2;

	GetLinePoints(lineObj, &pt1, &pt2);
	OffsetPoint(&pt1, -lineObj->DocObj.Frame.MinX, -lineObj->DocObj.Frame.MinY);
	OffsetPoint(&pt2, -lineObj->DocObj.Frame.MinX, -lineObj->DocObj.Frame.MinY);
	if ((polyObj = (PolyObjPtr) NewDocObject(NULL, TYPE_POLY)) == NULL)
		return (NULL);
	polyObj->DocObj.Frame = lineObj->DocObj.Frame;
	if (!PolyAddPoint(polyObj, 0, 0x7FFF, pt1.x, pt1.y) ||
		!PolyAddPoint(polyObj, 0, 0x7FFF, pt2.x, pt2.y)) {
		DisposeDocObject(polyObj);
		return (NULL);
	}
	PolyAdjustFrame(polyObj);
	PolySetPenColor(polyObj, lineObj->PenColor);
	PolySetPenSize(polyObj, lineObj->PenWidth, lineObj->PenHeight);
	PolySetFillPat(polyObj, &defaults.FillPat);
	polyObj->DocObj.Flags = lineObj->DocObj.Flags;
	PolySetSmooth(polyObj, FALSE);
	PolySetClosed(polyObj, FALSE);
	return (polyObj);
}

/*
 *	Determine if point is in line
 */

BOOL LineSelect(LineObjPtr lineObj, Point *pt)
{
	Point	pt1, pt2;

	GetLinePoints(lineObj, &pt1, &pt2);
	if (!HasPen(lineObj))
		return (FALSE);
	return (PtNearLine(pt, &pt1, &pt2, lineObj->PenWidth, lineObj->PenHeight, 2));
}

/*
 *	Return handle number of given point, or -1 if not in a handle
 */

WORD LineHandle(LineObjPtr lineObj, PointPtr pt, RectPtr handleRect)
{
	WORD	handle;
	Point	pt1, pt2;

	GetLinePoints(lineObj, &pt1, &pt2);
	if (InHandle(pt, &pt1, handleRect))
		handle = 0;
	else if (InHandle(pt, &pt2, handleRect))
		handle = 1;
	else
		handle = -1;
	return (handle);
}

/*
 *	Duplicate line data to new object
 *	Return success status
 */

BOOL LineDupData(LineObjPtr lineObj, LineObjPtr newObj)
{
	newObj->LineFlags	= lineObj->LineFlags;
	newObj->PenWidth	= lineObj->PenWidth;
	newObj->PenHeight	= lineObj->PenHeight;
	newObj->PenColor	= lineObj->PenColor;
	newObj->X			= lineObj->X;
	newObj->Y			= lineObj->Y;
	return (TRUE);
}

/*
 *	Set line arrows on or off
 *	1 = on, 0 = off, -1 = don't change
 */

void LineSetArrows(LineObjPtr lineObj, WORD atStart, WORD atEnd)
{
	if (atStart == 1)
		lineObj->LineFlags |= LINE_ARROWSTART;
	else if (atStart == 0)
		lineObj->LineFlags &= ~LINE_ARROWSTART;
	if (atEnd == 1)
		lineObj->LineFlags |= LINE_ARROWEND;
	else if (atEnd == 0)
		lineObj->LineFlags &= ~LINE_ARROWEND;
}

/*
 *	Draw routine for tracking line creation
 */

static void LineTrackCreate(WindowPtr window, PointPtr pt1, PointPtr pt2, BOOL change)
{
	LineObj	lineObj;

	if (change) {
		BlockClear(&lineObj, sizeof(LineObj));
		SetLinePoints(&lineObj, pt1, pt2);
		LineDrawOutline(window, &lineObj, 0, 0);
	}
}

/*
 *	Constrain routine for line creation
 */

static void LineConstrainCreate(register PointPtr pt1, register PointPtr pt2)
{
	register WORD	width, height;

	width = ABS(pt2->x - pt1->x);
	height = ABS(pt2->y - pt1->y);
	if (width < height/2)
		width = 0;
	else if (height < width/2)
		height = 0;
	else
		height = width = MIN(width, height);
	pt2->x = (pt2->x >= pt1->x) ? pt1->x + width  : pt1->x - width;
	pt2->y = (pt2->y >= pt1->y) ? pt1->y + height : pt1->y - height;
}

/*
 *	Create line object
 */

DocObjPtr LineCreate(WindowPtr window, UWORD modifier, WORD mouseX, WORD mouseY)
{
	DocDataPtr	docData = (DocDataPtr) GetWRefCon(window);
	Point		pt1, pt2;

/*
	Track object size
*/
	WindowToDoc(window, mouseX, mouseY, &pt1.x, &pt1.y);
	SnapToGrid(docData, &pt1.x, &pt1.y);
	pt2 = pt1;
	TrackMouse(window, modifier, &pt2, LineTrackCreate, LineConstrainCreate);
/*
	Create line
*/
	return (CreateLine(docData, &pt1, &pt2));
}

/*
 *	Draw routine for tracking line grow
 */

static void LineTrackGrow(WindowPtr window, PointPtr pt1, PointPtr pt2, BOOL change)
{
	Point	ptStart, ptEnd;
	LineObj	lineObj;

	if (change && (!EqualPt(pt1, pt2) || wasMoved)) {
		GetLinePoints(growLineObj, &ptStart, &ptEnd);
		BlockClear(&lineObj, sizeof(LineObj));
		if (growHandle == 0)
			ptStart = *pt2;
		else
			ptEnd = *pt2;
		SetLinePoints(&lineObj, &ptStart, &ptEnd);
		LineDrawOutline(window, &lineObj, 0, 0);
		wasMoved = TRUE;
	}
}

/*
 *	Track mouse and change line shape
 */

void LineGrow(WindowPtr window, LineObjPtr lineObj, UWORD modifier, WORD handle)
{
	DocDataPtr	docData = (DocDataPtr) GetWRefCon(window);
	Point		pt1, pt2, ptStart, ptEnd;

	growLineObj = lineObj;
	growHandle = handle;
	GetLinePoints(lineObj, &ptStart, &ptEnd);
/*
	Track handle movement
*/
	wasMoved = FALSE;
	pt1 = (growHandle == 0) ? ptStart : ptEnd;
	pt2 = pt1;
	SnapToGrid(docData, &pt2.x, &pt2.y);
	TrackMouse(window, modifier, &pt2, LineTrackGrow, NULL);
/*
	Set new object dimensions
*/
	if (!EqualPt(&pt1, &pt2)) {
		InvalObjectRect(window, lineObj);
		HiliteSelectOff(window);
		if (growHandle == 0)
			ptStart = pt2;
		else
			ptEnd = pt2;
		SetLinePoints(lineObj, &ptStart, &ptEnd);
		HiliteSelectOn(window);
		InvalObjectRect(window, lineObj);
	}
}

/*
 *	Draw arrow at start or end
 */

static BOOL DrawPSArrow(PrintRecPtr printRec, LineObjPtr lineObj, BOOL atEnd, RectPtr rect)
{
	WORD	i, dx, dy, r, len, numPts[1];
	Point	pt1, pt2, ptList[3], *pts[1];
	RGBPat8	pattern;

	GetLinePoints(lineObj, &pt1, &pt2);
	if (atEnd) {
		ptList[0] = pt2;
		dx = pt1.x - pt2.x;
		dy = pt1.y - pt2.y;
	}
	else {
		ptList[0] = pt1;
		dx = pt2.x - pt1.x;
		dy = pt2.y - pt1.y;
	}
	r = LineLength(&pt1, &pt2);
	if (r == 0)
		return (FALSE);
	len = 4 + (lineObj->PenWidth + lineObj->PenHeight)/2;
	ptList[1].x = ptList[0].x + len*(dx*2 + dy)/r;
	ptList[1].y = ptList[0].y + len*(dy*2 - dx)/r;
	ptList[2].x = ptList[0].x + len*(dx*2 - dy)/r;
	ptList[2].y = ptList[0].y + len*(dy*2 + dx)/r;
	MapPt(&ptList[0], &lineObj->DocObj.Frame, rect);
	MapPt(&ptList[1], &lineObj->DocObj.Frame, rect);
	MapPt(&ptList[2], &lineObj->DocObj.Frame, rect);
	for (i = 0; i < 8*8; i++)
		pattern[i] = lineObj->PenColor;
	if (!PSSetFill(printRec, &pattern))
		return (FALSE);
	numPts[0] = 3;
	pts[0] = ptList;
	return (PSPoly(printRec, 1, numPts, pts, TRUE, TRUE, TRUE, FALSE));
}

/*
 *	Draw line object to PostScript file
 */

BOOL LineDrawPSObj(PrintRecPtr printRec, LineObjPtr lineObj, RectPtr rect, RectPtr clipRect)
{
	Point	pen, start, end;

	if (HasPen(lineObj)) {
		pen.x = lineObj->PenWidth;
		pen.y = lineObj->PenHeight;
		PSScalePt(&pen, &lineObj->DocObj.Frame, rect);
		if (!PSSetPen(printRec, &pen, lineObj->PenColor))
			return (FALSE);
		GetLinePoints(lineObj, &start, &end);
		start.x = (start.x == lineObj->DocObj.Frame.MinX) ? rect->MinX : rect->MaxX;
		start.y = (start.y == lineObj->DocObj.Frame.MinY) ? rect->MinY : rect->MaxY;
		end.x = (end.x == lineObj->DocObj.Frame.MinX) ? rect->MinX : rect->MaxX;
		end.y = (end.y == lineObj->DocObj.Frame.MinY) ? rect->MinY : rect->MaxY;
		if (!PSLine(printRec, &start, &end))
			return (FALSE);
		if ((lineObj->LineFlags & LINE_ARROWSTART) &&
				!DrawPSArrow(printRec, lineObj, FALSE, rect))
			return (FALSE);
		if ((lineObj->LineFlags & LINE_ARROWEND) &&
				!DrawPSArrow(printRec, lineObj, TRUE, rect))
			return (FALSE);
	}
	return (TRUE);
}

/*
 *	Create new line given REXX arguments
 */

LineObjPtr LineNewREXX(DocDataPtr docData, TextPtr args)
{
	Point	pt1, pt2;

/*
	Get start and end points
*/
	args = GetArgPoint(args, docData, &pt1);
	if (args)
		args = GetArgPoint(args, docData, &pt2);
	if (args == NULL ||
		pt1.x < 0 || pt1.y < 0 || pt1.x >= docData->DocWidth || pt1.y >= docData->DocHeight ||
		pt2.x < 0 || pt2.y < 0 || pt2.x >= docData->DocWidth || pt2.y >= docData->DocHeight)
		return (NULL);
/*
	Create line
*/
	return (CreateLine(docData, &pt1, &pt2));
}
