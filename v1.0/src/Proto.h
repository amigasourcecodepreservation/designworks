/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Global function prototypes
 */

/*
 *	Main.c
 */

void	main(int, char **);
void	DoIntuiMessage(IntuiMsgPtr);
BOOL	DoMenu(WindowPtr, UWORD, UWORD, BOOL);
BOOL	DoCursorKey(WindowPtr, UWORD, UWORD);

/*
 *	Init.c
 */

void	Init(int, char **);
void	ShutDown(void);
void	SetUp(int, char **);

/*
 *	Monitor.c
 */

void	__saveds MonitorTask(void);

/*
 *	Menu.c
 */

void	SetProjectMenu(void);
void	SetEditMenu(void);
void	SetLayoutMenu(void);
void	SetArrangeMenu(void);
void	SetPenMenu(void);
void	SetTextMenu(void);
void	SetViewMenu(void);
void	SetMacroMenu(void);
void	SetAllMenus(void);

/*
 *	Project.c
 */

BOOL	DoNew(void);
BOOL	OpenFile(TextPtr, Dir);
BOOL	DoOpen(WindowPtr, UWORD, TextPtr);
BOOL	DoClose(WindowPtr);
BOOL	DoImportPict(WindowPtr, TextPtr);
BOOL	DoExportPict(WindowPtr, TextPtr);
BOOL	DoSave(WindowPtr, UWORD);
BOOL	DoSaveAs(WindowPtr);
BOOL	DoRevert(WindowPtr);
BOOL	DoPageSetup(WindowPtr);
BOOL	DoPrint(WindowPtr, UWORD, BOOL);
BOOL	DoSavePrefs(WindowPtr);
BOOL	DoProjectMenu(WindowPtr, UWORD, UWORD, UWORD);

/*
 *	Edit.c
 */

void	ClearPaste(void);

BOOL	DoCut(WindowPtr, BOOL, BOOL);

BOOL	DoEditMenu(WindowPtr, UWORD, UWORD, UWORD);

/*
 *	Layout.c
 */

BOOL	DoLayoutMenu(WindowPtr, UWORD, UWORD, UWORD);

/*
 *	Arrange.c
 */

BOOL	DoArrangeMenu(WindowPtr, UWORD, UWORD, UWORD);

/*
 *	Pen.c
 */

void	SetPenMenuDefaults(WindowPtr);

BOOL	DoPenMenu(WindowPtr, UWORD, UWORD, UWORD);

/*
 *	TextMenu.c
 */

WORD	NumMenuFonts(void);
WORD	MenuFontNum(FontNum, FontSize);
void	GetMenuFontInfo(WORD, FontNum *, FontSize *);
void	SetMenuFont(FontNum, FontSize, BOOL);

void	SetTextMenuDefaults(WindowPtr);

BOOL	DoTextMenu(WindowPtr, UWORD, UWORD, UWORD);

/*
 *	View.c
 */

BOOL	LayerObscured(LayerPtr);

void	AddWindowItem(WindowPtr);
void	RemoveWindowItem(WindowPtr);
void	ChangeWindowItem(WindowPtr);
BOOL	DoViewMenu(WindowPtr, UWORD, UWORD);

/*
 *	Tool.c
 */

void	SetToolDefaults(void);

void	ToolToFront(void);

void	OpenToolWindow(void);
void	OpenPenWindow(void);
void	OpenFillWindow(void);

void	CloseToolWindow(void);
void	ClosePenWindow(void);
void	CloseFillWindow(void);

void	DrawToolWindow(void);
void	DrawPenWindow(void);
void	DrawFillWindow(void);

void	SetDrawTool(WORD);
void	DoToolWindow(WORD, WORD);
void	DoPenWindow(WORD, WORD);
void	DoPenNum(WindowPtr, WORD);
void	DoFillWindow(WORD, WORD);
void	DoFillNum(WindowPtr, WORD);

/*
 *	Load.c
 */

void	SetPageParams(DocDataPtr);

BOOL	NewDocument(DocDataPtr);
void	DisposeAll(DocDataPtr);

BOOL	LoadFile(WindowPtr, DocDataPtr, TextPtr, Dir);

IFFPictPtr	LoadPict(TextPtr, Dir);
void		DisposePict(IFFPictPtr);

/*
 *	Save.c
 */

BOOL	SaveDRAWFile(DocDataPtr, TextPtr);

BOOL	ExportILBMFile(DocDataPtr, TextPtr);

/*
 *	Undo.c
 */

void	SetUndoMenuItem(WORD);
BOOL	UndoOn(WindowPtr, BOOL (*)(WindowPtr, WORD));
void	UndoOff(void);
BOOL	DoUndo(WindowPtr);

/*
 *	Clipboard.c
 */

void	GetClipboard(void);
void	PutClipboard(void);

/*
 *	Options.c
 */

void	SetStdOptions(Options *);

BOOL	DoOptions(void);
BOOL	DoScreenColors(void);

/*
 *	Screen.c
 */

WORD	GetColorTable(ScreenPtr, ColorTablePtr);
void	CheckColorTable(void);

ScreenPtr	GetScreen(int, char **);

/*
 *	Window.c
 */

WindowPtr	OpenBackWindow(void);

void	GetContentRect(WindowPtr, RectPtr);

void	SetWindowClip(WindowPtr);
void	SetContentClip(WindowPtr);

BOOL	IsDocWindow(WindowPtr);

WORD	WindowNum(WindowPtr);

WindowPtr	CreateWindow(TextPtr);
void		RemoveWindow(WindowPtr);
void		AdjustWindowTitle(WindowPtr);

void	DoGoAwayWindow(WindowPtr, UWORD);
void	DoWindowActivate(WindowPtr, BOOL);
void	DoNewSize(WindowPtr);

void	UpdateWindow(WindowPtr);
void	UpdateWindows(void);
void	DoWindowRefresh(WindowPtr);
void	RefreshWindows(void);

/*
 *	Gadget.c
 */

void	SetLayerIndic(WindowPtr);

void	AdjustScrollBars(WindowPtr);
void	SetScrollOffset(WindowPtr, WORD, WORD);
void	AdjustScrollOffset(WindowPtr);
void	ScrollToOffset(WindowPtr, WORD, WORD);

void	AdjustToVertScroll(WindowPtr);
void	AdjustToHorizScroll(WindowPtr);

void	ScrollUp(WindowPtr, BOOL);
void	ScrollDown(WindowPtr, BOOL);
void	ScrollLeft(WindowPtr, BOOL);
void	ScrollRight(WindowPtr, BOOL);

void	DoGadgetDown(WindowPtr, GadgetPtr, UWORD);
void	DoGadgetUp(WindowPtr, GadgetPtr);

/*
 *	Mouse.c
 */

void	SetFrameRect(RectPtr, WORD, WORD, WORD, WORD);
BOOL	GetSelectRect(DocDataPtr, RectPtr);

void	TrackMouse(WindowPtr, UWORD, WORD *, WORD *,
				   void (*)(WindowPtr, WORD, WORD, WORD, WORD, BOOL),
				   void (*)(WORD, WORD, WORD *, WORD *));

void	ResetMultiClick(void);
void	DoMouseDown(WindowPtr, UWORD, WORD, WORD, ULONG, ULONG);

/*
 *	Hilite.c
 */

void	GetHandleRect(WindowPtr, RectPtr);
BOOL	InHandle(PointPtr, PointPtr, RectPtr);
void	DrawHandle(RastPtr, WORD, WORD);

void	HiliteSelect(WindowPtr);
void	HiliteSelectOn(WindowPtr);
void	HiliteSelectOff(WindowPtr);

BOOL	DoCursorUp(WindowPtr, UWORD);
BOOL	DoCursorDown(WindowPtr, UWORD);
BOOL	DoCursorLeft(WindowPtr, UWORD);
BOOL	DoCursorRight(WindowPtr, UWORD);

/*
 *	Display.c
 */

void	ScaleWinToDoc(Fixed, WORD, WORD, WORD *, WORD *);
void	ScaleDocToWin(Fixed, WORD, WORD, WORD *, WORD *);

void	WindowToDoc(WindowPtr, WORD, WORD, WORD *, WORD *);
void	DocToWindow(WindowPtr, WORD, WORD, WORD *, WORD *);

void	WindowToDocPoint(WindowPtr, PointPtr, PointPtr);
void	DocToWindoowPoint(WindowPtr, PointPtr, PointPtr);
void	WindowToDocRect(WindowPtr, RectPtr, RectPtr);
void	DocToWindowRect(WindowPtr, RectPtr, RectPtr);

void	DrawDocument(WindowPtr, RectPtr);

/*
 *	Ruler.c
 */

void	GridDrawLoc(DocDataPtr, WORD, WORD *, WORD *);
void	GetGridSpacing(DocDataPtr, WORD *, WORD *);
void	SnapToGrid(DocDataPtr, WORD *, WORD *);

WORD	RulerWidth(WindowPtr);
WORD	RulerHeight(WindowPtr);

void	DrawRuler(WindowPtr);

void	DoSetRuler(WindowPtr, WORD, WORD);

void	RulerIndicOn(WindowPtr);
void	RulerIndicOff(WindowPtr);
void	UpdateRulerIndic(WindowPtr);

/*
 *	Graphics.c
 */

RGBColor	MixColors2(RGBColor, RGBColor);

void		ColorCorrectEnable(BOOL);
RGBColor	ColorCorrect(RGBColor);

void	MakeInvColorTable(ColorTablePtr, WORD, InvColorTablePtr);

void	PenNormal(RastPtr);
void	RGBForeColor(RastPtr, RGBColor);
void	RGBBackColor(RastPtr, RGBColor);

WORD	NormalizeAngle(WORD);

void	OffsetPoint(PointPtr, WORD, WORD);
void	RotatePoint(PointPtr, WORD, WORD, WORD);
void	FlipPoint(PointPtr, WORD, WORD, BOOL);

WORD	LineLength(PointPtr, PointPtr);
WORD	DistanceFromLine(PointPtr, PointPtr, PointPtr);

BOOL	PtNearLine(PointPtr, PointPtr, PointPtr, WORD, WORD, WORD);
BOOL	EqualPt(PointPtr, PointPtr);

void	OffsetRect(RectPtr, WORD, WORD);
void	RotateRect(RectPtr, WORD, WORD, WORD);
void	FlipRect(RectPtr, WORD, WORD, BOOL);
void	InsetRect(RectPtr, WORD, WORD);
BOOL	SectRect(RectPtr, RectPtr, RectPtr);
void	UnionRect(RectPtr, RectPtr, RectPtr);
BOOL	PtInRect(PointPtr, RectPtr);
BOOL	EqualRect(RectPtr, RectPtr);
BOOL	EmptyRect(RectPtr);

void	FrameRect(RastPtr, RectPtr, WORD, WORD);
void	FillRect(RastPtr, RectPtr, RGBPat8Ptr);

void	FrameOval(RastPtr, RectPtr, WORD, WORD);
void	FillOval(RastPtr, RectPtr, RGBPat8Ptr);

void	OffsetPoly(WORD, PointPtr, WORD, WORD);
void	FramePoly(RastPtr, WORD, PointPtr, WORD, WORD);
void	FillPoly(RastPtr, WORD, PointPtr, RGBPat8Ptr);

void	ExtText(RastPtr, TextPtr, WORD);
void	ExtBltTemplate(PLANEPTR, WORD, WORD, RastPtr, WORD, WORD, WORD, WORD);

void	ScalePt(PointPtr, RectPtr, RectPtr);
void	MapPt(PointPtr, RectPtr, RectPtr);
void	MapRect(RectPtr, RectPtr, RectPtr);

void	ScaleBitMap(RastPtr, RastPtr, RectPtr, RectPtr);
void	TransformBitMap(RastPtr, RastPtr, RectPtr, RectPtr, WORD, BOOL, BOOL);

RastPtr	CreateRastPort(WORD, WORD, WORD);
void	DisposeRastPort(RastPtr);

BitMapPtr	CreateBitMap(WORD, WORD, WORD);
void		DisposeBitMap(BitMapPtr);

void	ClearRast(RastPtr);

/*
 *	Layer.c
 */

DocLayerPtr	NewDocLayer(DocDataPtr, TextPtr);
void		DetachLayer(DocDataPtr, DocLayerPtr);
void		DisposeDocLayer(DocLayerPtr);

DocLayerPtr	TopLayer(DocDataPtr);
DocLayerPtr	BottomLayer(DocDataPtr);
DocLayerPtr	NextLayer(DocLayerPtr);
DocLayerPtr	PrevLayer(DocLayerPtr);

DocLayerPtr	TopVisLayer(DocDataPtr);
DocLayerPtr	BottomVisLayer(DocDataPtr);
DocLayerPtr	NextVisLayer(DocLayerPtr);
DocLayerPtr	PrevVisLayer(DocLayerPtr);

void	InsertLayer(DocDataPtr, DocLayerPtr, DocLayerPtr);
void	LayerForward(DocDataPtr, DocLayerPtr);
void	LayerBackward(DocDataPtr, DocLayerPtr);

DocLayerPtr	CurrLayer(DocDataPtr);
void		SetCurrLayer(DocDataPtr, DocLayerPtr);

WORD	LayerNum(DocDataPtr, DocLayerPtr);
WORD	NumLayers(DocDataPtr);

DocLayerPtr	GetLayer(DocDataPtr, WORD);

TextPtr	GetLayerName(DocLayerPtr);
void	SetLayerName(DocLayerPtr, TextPtr);

BOOL	DoLayerUp(WindowPtr, BOOL);
BOOL	DoLayerDown(WindowPtr, BOOL);
BOOL	DoLayerNum(WindowPtr, WORD);

/*
 *	Object.c
 */

DocObjPtr	NewDocObject(DocLayerPtr, WORD);
DocObjPtr	DuplicateObject(DocLayerPtr, DocObjPtr);
void		DetachObject(DocLayerPtr, DocObjPtr);
void		DisposeDocObject(DocObjPtr);
void		DisposeAllDocObjects(DocObjPtr);

DocObjPtr	TopObject(DocLayerPtr);
DocObjPtr	BottomObject(DocLayerPtr);

void	InsertObject(DocLayerPtr, DocObjPtr, DocObjPtr);
void	InsertAllObjects(DocLayerPtr, DocObjPtr, DocObjPtr);
void	AppendObject(DocLayerPtr, DocObjPtr);
void	AppendToGroup(GroupObjPtr, DocObjPtr);

void	ObjectForward(DocLayerPtr, DocObjPtr);
void	ObjectToFront(DocLayerPtr, DocObjPtr);
void	ObjectBackward(DocLayerPtr, DocObjPtr);
void	ObjectToBack(DocLayerPtr, DocObjPtr);

DocObjPtr	FirstObject(DocDataPtr);
DocObjPtr	LastObject(DocDataPtr);

DocObjPtr	FirstSelected(DocDataPtr);
DocObjPtr	LastSelected(DocDataPtr);
DocObjPtr	NextSelected(DocObjPtr);
DocObjPtr	PrevSelected(DocObjPtr);
void		UnSelectAllObjects(DocDataPtr);

BOOL	ObjectsLocked(DocDataPtr);

void	DrawObject(RastPtr, DocObjPtr, RectPtr, RectPtr);
void	DrawObjectOutline(WindowPtr, DocObjPtr, WORD, WORD);
void	HiliteObject(WindowPtr, DocObjPtr);
void	EnableObjectPen(DocObjPtr, BOOL);
void	SetObjectPenColor(DocObjPtr, RGBColor);
BOOL	GetObjectPenSize(DocObjPtr, WORD *, WORD *);
void	SetObjectPenSize(DocObjPtr, WORD, WORD);
void	EnableObjectFill(DocObjPtr, BOOL);
void	SetObjectFillPat(DocObjPtr, RGBPat8Ptr);
void	SetObjectTextParams(DocObjPtr, FontNum, FontSize, WORD, WORD, WORD, WORD);
void	RotateObject(DocObjPtr, WORD, WORD, WORD);
void	FlipObject(DocObjPtr, WORD, WORD, BOOL);
BOOL	PointInObject(DocObjPtr, PointPtr);
BOOL	PointInHandle(DocObjPtr, PointPtr, RectPtr);

void	OffsetObject(DocObjPtr, WORD, WORD);
void	ScaleObject(DocObjPtr, RectPtr);

PolyObjPtr	ConvertObjectToPoly(DocObjPtr);

BOOL	NewREXXObject(WindowPtr, WORD, TextPtr);

void	AdjustToDocBounds(DocDataPtr, RectPtr);

void	InvalObjectRect(WindowPtr, DocObjPtr);

/*
 *	Group.c
 */

GroupObjPtr	GroupAllocate(void);
void		GroupDispose(GroupObjPtr);

void	GroupDrawObj(RastPtr, GroupObjPtr, RectPtr, RectPtr);
void	GroupDrawOutline(WindowPtr, GroupObjPtr, WORD, WORD);
void	GroupHilite(WindowPtr, GroupObjPtr);
void	GroupSetPenColor(GroupObjPtr, RGBColor);
void	GroupSetPenSize(GroupObjPtr, WORD, WORD);
void	GroupSetFillPat(GroupObjPtr, RGBPat8Ptr);
void	GroupSetTextParams(GroupObjPtr, FontNum, FontSize, WORD, WORD, WORD, WORD);
void	GroupRotate(GroupObjPtr, WORD, WORD, WORD);
void	GroupFlip(GroupObjPtr, WORD, WORD, BOOL);
void	GroupScale(GroupObjPtr, RectPtr);

BOOL	GroupSelect(GroupObjPtr, PointPtr);
WORD	GroupHandle(GroupObjPtr, PointPtr, RectPtr);

BOOL	GroupDupData(GroupObjPtr, GroupObjPtr);
void	GroupAdjustFrame(GroupObjPtr);

void	GroupGrow(WindowPtr, GroupObjPtr, UWORD, WORD, WORD);

GroupObjPtr	MakeGroup(DocDataPtr);
void		UnMakeGroup(DocDataPtr, GroupObjPtr);

/*
 *	Line.c
 */

LineObjPtr	LineAllocate(void);
void		LineDispose(LineObjPtr);

void	LineDrawObj(RastPtr, LineObjPtr, RectPtr, RectPtr);
void	LineDrawOutline(WindowPtr, LineObjPtr, WORD, WORD);
void	LineHilite(WindowPtr, LineObjPtr);
void	LineSetPenColor(LineObjPtr, RGBColor);
void	LineSetPenSize(LineObjPtr, WORD, WORD);
void	LineSetFillPat(LineObjPtr, RGBPat8Ptr);
void	LineRotate(LineObjPtr, WORD, WORD, WORD);
void	LineFlip(LineObjPtr, WORD, WORD, BOOL);
void	LineScale(LineObjPtr, RectPtr);

PolyObjPtr	LineConvertToPoly(LineObjPtr);

BOOL	LineSelect(LineObjPtr, PointPtr);
WORD	LineHandle(LineObjPtr, PointPtr, RectPtr);

BOOL	LineDupData(LineObjPtr, LineObjPtr);

void	LineSetArrows(LineObjPtr, WORD, WORD);

DocObjPtr	LineCreate(WindowPtr, UWORD, WORD, WORD);
void		LineGrow(WindowPtr, LineObjPtr, UWORD, WORD, WORD);

LineObjPtr	LineNewREXX(DocDataPtr, TextPtr);

/*
 *	Rect.c
 */

RectObjPtr	RectAllocate(void);
void		RectDispose(RectObjPtr);

void	RectDrawObj(RastPtr, RectObjPtr, RectPtr, RectPtr);
void	RectDrawOutline(WindowPtr, RectObjPtr, WORD, WORD);
void	RectHilite(WindowPtr, RectObjPtr);
void	RectSetPenColor(RectObjPtr, RGBColor);
void	RectSetPenSize(RectObjPtr, WORD, WORD);
void	RectSetFillPat(RectObjPtr, RGBPat8Ptr);
void	RectRotate(RectObjPtr, WORD, WORD, WORD);
void	RectFlip(RectObjPtr, WORD, WORD, BOOL);
void	RectScale(RectObjPtr, RectPtr);

PolyObjPtr	RectConvertToPoly(RectObjPtr);

BOOL	RectSelect(RectObjPtr, PointPtr);
WORD	RectHandle(RectObjPtr, PointPtr, RectPtr);

BOOL	RectDupData(RectObjPtr, RectObjPtr);

DocObjPtr	RectCreate(WindowPtr, UWORD, WORD, WORD);
void		RectGrow(WindowPtr, RectObjPtr, UWORD, WORD, WORD);

RectObjPtr	RectNewREXX(DocDataPtr, TextPtr);

/*
 *	Oval.c
 */

OvalObjPtr	OvalAllocate(void);
void		OvalDispose(OvalObjPtr);

void	OvalDrawObj(RastPtr, OvalObjPtr, RectPtr, RectPtr);
void	OvalDrawOutline(WindowPtr, OvalObjPtr, WORD, WORD);
void	OvalHilite(WindowPtr, OvalObjPtr);
void	OvalSetPenColor(OvalObjPtr, RGBColor);
void	OvalSetPenSize(OvalObjPtr, WORD, WORD);
void	OvalSetFillPat(OvalObjPtr, RGBPat8Ptr);
void	OvalRotate(OvalObjPtr, WORD, WORD, WORD);
void	OvalFlip(OvalObjPtr, WORD, WORD, BOOL);
void	OvalScale(OvalObjPtr, RectPtr);

PolyObjPtr	OvalConvertToPoly(OvalObjPtr);

BOOL	OvalSelect(OvalObjPtr, PointPtr);
WORD	OvalHandle(OvalObjPtr, PointPtr, RectPtr);

BOOL	OvalDupData(OvalObjPtr, OvalObjPtr);

DocObjPtr	OvalCreate(WindowPtr, UWORD, WORD, WORD);
void		OvalGrow(WindowPtr, OvalObjPtr, UWORD, WORD, WORD);

OvalObjPtr	OvalNewREXX(DocDataPtr, TextPtr);

/*
 *	Poly.c
 */

PolyObjPtr	PolyAllocate(void);
void		PolyDispose(PolyObjPtr);

void	PolyDrawObj(RastPtr, PolyObjPtr, RectPtr, RectPtr);
void	PolyDrawOutline(WindowPtr, PolyObjPtr, WORD, WORD);
void	PolyHilite(WindowPtr, PolyObjPtr);
void	PolySetPenColor(PolyObjPtr, RGBColor);
void	PolySetPenSize(PolyObjPtr, WORD, WORD);
void	PolySetFillPat(PolyObjPtr, RGBPat8Ptr);
void	PolyRotate(PolyObjPtr, WORD, WORD, WORD);
void	PolyFlip(PolyObjPtr, WORD, WORD, BOOL);
void	PolyScale(PolyObjPtr, RectPtr);

BOOL	PolySelect(PolyObjPtr, PointPtr);
WORD	PolyHandle(PolyObjPtr, PointPtr, RectPtr);
BOOL	PolyAddHandle(PolyObjPtr, PointPtr);
void	PolyRemHandle(PolyObjPtr, WORD);

BOOL	PolyDupData(PolyObjPtr, PolyObjPtr);
BOOL	PolyAddPoint(PolyObjPtr, WORD, WORD, WORD);
void	PolyRemPoint(PolyObjPtr, WORD);
void	PolyAdjustFrame(PolyObjPtr);

void	PolySetSmooth(PolyObjPtr, BOOL);
void	PolySetClosed(PolyObjPtr, BOOL);

DocObjPtr	CurveCreate(WindowPtr, UWORD, WORD, WORD);
DocObjPtr	PolyCreate(WindowPtr, UWORD, WORD, WORD);
void		PolyGrow(WindowPtr, PolyObjPtr, UWORD, WORD, WORD);

PolyObjPtr	PolyNewREXX(DocDataPtr, TextPtr);

/*
 *	Text.c
 */

TextObjPtr	TextAllocate(void);
void		TextDispose(TextObjPtr);

void	TextDrawObj(RastPtr, TextObjPtr, RectPtr, RectPtr);
void	TextDrawOutline(WindowPtr, TextObjPtr, WORD, WORD);
void	TextHilite(WindowPtr, TextObjPtr);
void	TextSetPenColor(TextObjPtr, RGBColor);
void	TextSetPenSize(TextObjPtr, WORD, WORD);
void	TextSetFillPat(TextObjPtr, RGBPat8Ptr);
WORD	TextNewStyle(WORD, WORD);
void	TextSetTextParams(TextObjPtr, FontNum, FontSize, WORD, WORD, WORD, WORD);
void	TextRotate(TextObjPtr, WORD, WORD, WORD);
void	TextFlip(TextObjPtr, WORD, WORD, BOOL);
void	TextScale(TextObjPtr, RectPtr);

BOOL	TextSelect(TextObjPtr, PointPtr);
WORD	TextHandle(TextObjPtr, PointPtr, RectPtr);

BOOL	TextDupData(TextObjPtr, TextObjPtr);

void	TextAdjustFrame(TextObjPtr);

DocObjPtr	TextCreate(WindowPtr, UWORD, WORD, WORD);
void		TextGrow(WindowPtr, TextObjPtr, UWORD, WORD, WORD);

BOOL	TextInEdit(WindowPtr);

BOOL	TextAddChars(TextObjPtr, TextPtr, WORD);
void	TextInsert(WindowPtr, TextChar);
void	TextUpdate(void);
void	TextDelete(WindowPtr, TextChar, UWORD);
void	TextPurge(DocDataPtr);

void	TextDrawCursor(WindowPtr);
void	TextBlinkCursor(WindowPtr);
void	TextCursorUp(WindowPtr, UWORD);
void	TextCursorDown(WindowPtr, UWORD);
void	TextCursorLeft(WindowPtr, UWORD);
void	TextCursorRight(WindowPtr, UWORD);

BOOL	TextMouseClick(WindowPtr, UWORD, WORD, WORD);

TextObjPtr	TextNewREXX(DocDataPtr, TextPtr);

/*
 *	BitMap.c
 */

BMapObjPtr	BMapAllocate(void);
void		BMapDispose(BMapObjPtr);

void	BMapDrawObj(RastPtr, BMapObjPtr, RectPtr, RectPtr);
void	BMapDrawOutline(WindowPtr, BMapObjPtr, WORD, WORD);
void	BMapHilite(WindowPtr, BMapObjPtr);
void	BMapSetPenColor(BMapObjPtr, RGBColor);
void	BMapSetPenSize(BMapObjPtr, WORD, WORD);
void	BMapSetFillPat(BMapObjPtr, RGBPat8Ptr);
void	BMapRotate(BMapObjPtr, WORD, WORD, WORD);
void	BMapFlip(BMapObjPtr, WORD, WORD, BOOL);
void	BMapScale(BMapObjPtr, RectPtr);

BOOL	BMapSelect(BMapObjPtr, PointPtr);
WORD	BMapHandle(BMapObjPtr, PointPtr, RectPtr);

BOOL	BMapDupData(BMapObjPtr, BMapObjPtr);

void	BMapGrow(WindowPtr, BMapObjPtr, UWORD, WORD, WORD);

BOOL	BMapConvert(WindowPtr, IFFPictPtr);

/*
 *	Icon.c
 */

void	InitIcons(void);
void	ShutDownIcons(void);
void	DoAppMsg(struct AppMessage *);
void	SaveIcon(TextPtr, WORD);

/*
 *	Prefs.c
 */

void	SetPathName(TextPtr, TextPtr, BOOL);

BOOL	ReadProgPrefs(TextPtr);
void	GetProgPrefs(void);
BOOL	SaveProgPrefs(WindowPtr);

/*
 *	PrHandler.c
 */

PrintIOPtr	OpenPrinter(void);
void		ClosePrinter(PrintIOPtr);

BOOL	GetSysPrefs(void);
void	GetPrinterType(void);
BOOL	SetPrintDensity(PrintIOPtr, UBYTE, BOOL);
BOOL	GetPrintDensity(WORD, WORD *, WORD *);

void	InitPrintHandler(void);
void	PrValidate(PrintRecPtr);
void	PrintDefault(PrintRecPtr);
BOOL	PageSetupDialog(PrintRecPtr);
BOOL	PrintDialog(PrintRecPtr);
BOOL	SetPrintOption(PrintRecPtr, TextPtr, WORD);

/*
 *	Print.c
 */

BOOL	PrintDocument(WindowPtr, DocDataPtr);
BOOL	PrintError(void);

/*
 *	REXX.c
 */

void	InitRexx(void);
void	ShutDownRexx(void);
void	DoRexxMsg(struct RexxMsg *);
void	DoMacro(TextPtr);
BOOL	DoMacroMenu(WindowPtr, UWORD, UWORD);
void	DoAutoExec(void);

/*
 *	Font.c
 */

BOOL	AddFontEntry(FontNum, FontNum);
BOOL	GetFontEntry(FontNum, FontNum *);
BOOL	FontTableItem(UWORD, FontNum *);
void	DisposeFontTable(void);

FontNum		NumFonts(void);
FontNum		FontNumber(TextPtr);
void		GetFontName(FontNum, TextPtr);
TextFontPtr	GetNumFont(FontNum, FontSize);

WORD		NumFontAttrs(void);
TextAttrPtr	FontAttrTable(void);
FontSize	FontScaleSize(FontNum, FontSize);

BOOL	InitFonts(void);
void	ShutDownFonts(void);

/*
 *	Pattern.c
 */

BOOL		AddPatEntry(RGBPat8Ptr);
FillPatNum	GetPatEntry(RGBPat8Ptr);
RGBPat8Ptr	PatTableItem(FillPatNum);
void		DisposePatTable(void);

/*
 *	Error.c
 */

void	FixTitle(void);
void	Error(WORD);
void	DOSError(WORD, LONG);
void	InfoDialog(TextPtr, WORD);
void	DoHelp(void);
void	CheckMemory(void);

/*
 *	BuffIO.c
 */

void	ClearBuff(void);
WORD	GetByte(File);
WORD	GetNibble(File);
WORD	GetWord(File);

/*
 *	Misc.c
 */

void	SetPointerShape(void);

WORD	StdDialog(WORD);
void	OutlineOKButton(WindowPtr);
void	EnableCancelButton(WindowPtr, BOOL);
void	DrawArrowBorder(WindowPtr, WORD);
BOOL	DialogFilter(IntuiMsgPtr, WORD *);
void	BeginWait(void);
void	EndWait(void);

void	SetBusyPointer(WindowPtr);
void	NextBusyPointer(void);

void	ErrBeep(void);
void	StdBeep(void);

BOOL	CheckNumber(TextPtr);

BOOL	DevMounted(TextPtr);
BOOL	DirAssigned(TextPtr);

void	DotToText(WORD, WORD, TextPtr, WORD);
void	DecipointToText(WORD, TextPtr, WORD);
WORD	TextToDecipoint(TextPtr);
WORD	GetFracValue(GadgetPtr, WORD);
TextPtr	GetArgPoint(TextPtr, DocDataPtr, PointPtr);
