/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	PostScript printing routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>
#include <libraries/dos.h>

#include <proto/exec.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include <stdio.h>
#include <string.h>

#include <Toolbox/Font.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Utility.h>
#include <Toolbox/DateTime.h>

#include "Draw.h"
#include "Proto.h"

/*
 *	External variables
 */

extern PostScriptOptions	postScriptOptions;

extern ScreenPtr	screen;
extern MsgPortPtr	mainMsgPort;

extern UBYTE	fileBuff[];

extern DlgTemplPtr	dlgList[];

extern TextChar		strBuff[];

/*
 *	Local variables and definitions
 */

static TextChar	strPrologName[] = "PostScript Prep";

static TextChar	strParName[] = "PAR:";
static TextChar	strSerName[] = "SER:";

static TextChar	strTimesRoman[]			= "Times-Roman";
static TextChar	strTimesBold[]			= "Times-Bold";
static TextChar	strTimesItalic[]		= "Times-Italic";
static TextChar	strTimesBoldItalic[]	= "Times-BoldItalic";

static TextChar	psTextBuff[256];

static FontNum	prevFontNum = -1;
static WORD		prevStyleBits;
static FontSize prevFontSize = -1;

enum {
	PARALLEL_RADBTN = 2,
	SERIAL_RADBTN,
	CUSTOMPORT_RADBTN,
	CUSTOMPORT_TEXT,
	GRAYSCALE_RADBTN,
	COLOR_RADBTN,
	SCREENTYPE_POPUP,
	FREQ_TEXT,
	ANGLE_TEXT,
	ANGLETYPE_POPUP,
	FASTPICTS_BOX,
	EOF_BOX
};

static DialogPtr	psDlg;

/*
 *	Metric file format
 */

typedef struct {
	TextChar	PSFontName[66];
	ULONG		PropWidthTable;
	ULONG		KerningTable;
	ULONG		TrackingTable;
	WORD		TrackCount;
	UBYTE		Proportional;
	UBYTE		AmigaEncoding;
/*
	WORD		BBoxMinX, BBoxMinY, BBoxMaxX, BBoxMaxY;
	WORD		UnderlinePosition, UnderlineWeight;
	WORD		CapHeight, XHeight;
	WORD		dAscenderHeight, pDescenderHeight;
	UWORD		Style;
*/
} FontMetric;

/*
 *	Local prototypes
 */

static void	AdjustCoords(PrintRecPtr, PointPtr);
static WORD	GrayValue(RGBColor);

static BOOL	SendText(File, TextPtr);

static BOOL	PostScriptDialogFilter(IntuiMsgPtr, WORD *);

/*
 *	Scale point for PostScript output
 *	If point x or y is zero, then it is left at zero
 */

void PSScalePt(PointPtr pt, RectPtr srcRect, RectPtr dstRect)
{
	WORD x, y;

	x = pt->x;		y = pt->y;
	ScalePt(pt, srcRect, dstRect);
	if (x == 0)
		pt->x = 0;
	if (y == 0)
		pt->y = 0;
}

/*
 *	Return pointer to PostScript output device name
 */

TextPtr PSDeviceName()
{
	register TextPtr devName;

	switch (postScriptOptions.Device) {
	case PS_SERIAL:
		devName = strSerName;
		break;
	case PS_PARALLEL:
		devName = strParName;
		break;
	default:
		devName = postScriptOptions.CustomName;
		break;
	}
	return (devName);
}

/*
 *	Adjust point coordinates from document to PostScript user space
 */

static void AdjustCoords(PrintRecPtr printRec, PointPtr pt)
{
	pt->x = pt->x + printRec->PageRect.MinX - printRec->PaperRect.MinX;
	pt->y = printRec->PaperRect.MaxY - printRec->PageRect.MinY - pt->y;
}

/*
 *	Return gray value (0 to 255) for given color values (0 to 15)
 */

static WORD GrayValue(register RGBColor color)
{
	return (30*RED(color) + 59*GREEN(color) + 11*BLUE(color))*0x100L/1501L;
}

/*
 *	Write text string to file
 */

static BOOL SendText(File file, TextPtr text)
{
	register WORD	len;

	len = strlen(text);
	return ((BOOL) (Write(file, text, len) == len));
}

/*
 *	Send prologue and set up printer
 */

BOOL PSBeginJob(PrintRecPtr printRec, TextPtr jobTitle, RectPtr bounds)
{
	WORD				buffSize, paperType;
	BOOL				success;
	File				prologFile;
	File				file = printRec->File;
	Point				pt1, pt2;
	struct DateStamp	dateStamp;

	prologFile = NULL;
	success = FALSE;
	prevFontNum = -1;
	prevFontSize = -1;
/*
	Send header
*/
	if ((bounds && !SendText(file, "%!PS-Adobe-3.0 EPSF-3.0\n")) ||
		(bounds == NULL && !SendText(file, "%!PS-Adobe-3.0\n")))
		goto Exit;
	if (bounds) {
		pt1.x = bounds->MinX;
		pt1.y = bounds->MaxY;
		pt2.x = bounds->MaxX;
		pt2.y = bounds->MinY;
		AdjustCoords(printRec, &pt1);
		AdjustCoords(printRec, &pt2);
		sprintf(psTextBuff, "%%%%BoundingBox: %d %d %d %d\n", pt1.x, pt1.y, pt2.x, pt2.y);
	}
	else
		sprintf(psTextBuff, "%%%%BoundingBox: %d %d %d %d\n",
			0, 0, printRec->PaperWidth/10, printRec->PaperHeight/10);
	if (!SendText(file, psTextBuff))
		goto Exit;
	if (!SendText(file, "%%Creator: DesignWorks\n"))
		goto Exit;
	sprintf(psTextBuff, "%%%%Title: %s\n", jobTitle);
	if (!SendText(file, psTextBuff))
		goto Exit;
	DateStamp(&dateStamp);
	DateString(&dateStamp, DATE_LONG, strBuff);
	sprintf(psTextBuff, "%%%%CreationDate: %s, ", strBuff);
	if (!SendText(file, psTextBuff))
		goto Exit;
	TimeString(&dateStamp, FALSE, TRUE, strBuff);
	sprintf(psTextBuff, "%s\n",strBuff);
	if (!SendText(file, psTextBuff))
		goto Exit;
	if (!SendText(file, "%%For: Amiga\n"))
		goto Exit;
	if (bounds == NULL) {
		sprintf(psTextBuff, "%%%%Orientation: %s\n",
				(printRec->Orientation == PRT_PORTRAIT) ? "Portrait" : "Landscape");
		if (!SendText(file, psTextBuff))
			goto Exit;
		sprintf(psTextBuff, "%%%%Pages: %d\n", printRec->LastPage - printRec->FirstPage + 1);
		if (!SendText(file, psTextBuff))
			goto Exit;
		sprintf(psTextBuff, "%%%%PageOrder: %s\n",
				(printRec->Flags & PRT_BACKTOFRONT) ? "Descend" : "Ascend");
		if (!SendText(file, psTextBuff))
			goto Exit;
		if (!SendText(file, "%%DocumentData: Clean7Bit\n"))
			goto Exit;
	}
	if (!SendText(file, "%%EndComments\n"))
		goto Exit;
	if (!SendText(file, "/NHDict 150 dict def NHDict begin\n"))
		goto Exit;
/*
	Send prologue
*/
	if (!SendText(file, "%%BeginProlog\n"))
		goto Exit;
	SetPathName(psTextBuff, strPrologName, TRUE);
	if ((prologFile = Open(psTextBuff, MODE_OLDFILE)) == NULL)
		goto Exit;
	while ((buffSize = Read(prologFile, fileBuff, FILEBUFF_SIZE)) > 0) {
		if (Write(file, fileBuff, buffSize) != buffSize)
			goto Exit;
	}
	Close(prologFile);
	prologFile = NULL;
/*
	Set document defaults only if not an EPS File
*/
	if (bounds == NULL) {
		if (!SendText(file, "%%BeginSetup\n"))
			goto Exit;
		if (!SendText(file, "statusdict /waittimeout known {statusdict /waittimeout 0 put} if\n"))
			goto Exit;
		sprintf(psTextBuff, "/#copies %d def\n",
				(printRec->Flags & PRT_COLLATE) ? 1 : printRec->Copies);
		if (!SendText(file, psTextBuff))
			goto Exit;
		psTextBuff[0] = '\0';
		switch (printRec->PageSize) {
		case PRT_USLEGAL:
			paperType = 2;
			break;
		case PRT_A4LETTER:
			paperType = 3;
			break;
		default:
			paperType = 1;
			break;
		}
		sprintf(psTextBuff, "%d PaperSize\n", paperType);
		if (!SendText(file, psTextBuff))
			goto Exit;
		sprintf(psTextBuff, "%s ManualFeed\n",
				(printRec->PaperFeed == PRT_CUTSHEET) ? "true" : "false");
		if (!SendText(file, psTextBuff))
			goto Exit;
		if (postScriptOptions.ScreenType != PS_SCRNDEFAULT) {
			sprintf(psTextBuff, "%d %d %d %d %d %d SetScreen\n",
					postScriptOptions.ScreenFreq,
					postScriptOptions.ScreenAngles[0], postScriptOptions.ScreenAngles[1],
					postScriptOptions.ScreenAngles[2], postScriptOptions.ScreenAngles[3],
					postScriptOptions.ScreenType - PS_SCRNDOT);
			if (!SendText(file, psTextBuff))
				goto Exit;
		}
		if (!SendText(file, "%%EndSetup\n"))
			goto Exit;
	}
/*
	Clean up
*/
	success = TRUE;
Exit:
	if (prologFile)
		Close(prologFile);
	return (success);
}

/*
 *	Send trailer and end job
 */

BOOL PSEndJob(PrintRecPtr printRec, BOOL epsFile)
{
	BOOL	success;
	File	file = printRec->File;

	success = FALSE;
	if (!SendText(file, "%%Trailer\nend\n%%EOF\n") ||
		((!postScriptOptions.NoEOFMark && !epsFile) && !SendText(file, "\004")))
		goto Exit;
	success = TRUE;
Exit:
	return (success);
}

/*
 *	Begin page
 */

BOOL PSBeginPage(PrintRecPtr printRec, WORD page)
{
	BOOL	success;
	File	file = printRec->File;

	success = FALSE;
	prevFontNum = -1;
	prevFontSize = -1;
	sprintf(psTextBuff, "%%%%Page: %d %d\n", page + 1, page + 1);
	if (!SendText(file, psTextBuff))
		goto Exit;
/*
	Page setup (goes here, since showpage clears these settings)
*/
	if (!SendText(file, "%%BeginPageSetup\n") ||
		!SendText(file, "/pageSave save def\n"))
		goto Exit;
	if (printRec->Orientation == PRT_LANDSCAPE) {
		sprintf(psTextBuff, "%d 0 translate -90 rotate\n", printRec->PaperHeight/10);
		if (!SendText(file, psTextBuff))
			goto Exit;
	}
/*
	sprintf(psTextBuff, "72 %d div 72 %d div scale\n", printRec->xDPI, printRec->yDPI);
	if (!SendText(file, psTextBuff))
		goto Exit;
*/
	if (!SendText(file, "2 setlinecap 0 setlinejoin\n") ||
		!SendText(file, "%%EndPageSetup\n"))
		goto Exit;
	success = TRUE;
Exit:
	return (success);
}

/*
 *	End and print page
 */

BOOL PSEndPage(PrintRecPtr printRec)
{
	BOOL	success;
	File	file = printRec->File;

	success = FALSE;
	if (!SendText(file, "pageSave restore showpage\n"))
		goto Exit;
	success = TRUE;
Exit:
	return (success);
}

/*
 *	Set pen size and color
 */

BOOL PSSetPen(PrintRecPtr printRec, PointPtr pen, RGBColor color)
{
	if (postScriptOptions.Colors == PS_COLORFULL)
		sprintf(psTextBuff, "%d %d %d %d %d true SetPen\n", pen->x, pen->y,
				RED(color), GREEN(color), BLUE(color));
	else
		sprintf(psTextBuff, "%d %d %d false SetPen\n", pen->x, pen->y,
				GrayValue(color) >> 1);
	return (SendText(printRec->File, psTextBuff));
}

/*
 *	Set fill color
 */

BOOL PSSetFill(PrintRecPtr printRec, RGBPat8Ptr pat)
{
	register WORD	i, color, red, green, blue;

/*
	Average the pattern colors
*/
	red = green = blue = 0;
	for (i = 0; i < 64; i++) {
		color = (*pat)[i];
		red   += RED(color);
		green += GREEN(color);
		blue  += BLUE(color);
	}
	red   /= 64;
	green /= 64;
	blue  /= 64;
/*
	Set fill color
*/
	if (postScriptOptions.Colors == PS_COLORFULL)
		sprintf(psTextBuff, "%d %d %d true SetFill\n", red, green, blue);
	else
		sprintf(psTextBuff, "%d false SetFill\n",
				GrayValue(RGBCOLOR(red, green, blue)) >> 1);
	return (SendText(printRec->File, psTextBuff));
}

/*
 *	Line
 */

BOOL PSLine(PrintRecPtr printRec, PointPtr startPt, PointPtr endPt)
{
	Point	pt1, pt2;

	pt1 = *startPt;
	pt2 = *endPt;
	AdjustCoords(printRec, &pt1);
	AdjustCoords(printRec, &pt2);
	sprintf(psTextBuff, "%d %d %d %d Line\n", pt1.x, pt1.y, pt2.x, pt2.y);
	return (SendText(printRec->File, psTextBuff));
}

/*
 *	Rect
 */

BOOL PSRect(PrintRecPtr printRec, RectPtr rect, BOOL doPen, BOOL doFill)
{
	Point	pt1, pt2;

	pt1.x = rect->MinX;
	pt1.y = rect->MinY;
	pt2.x = rect->MaxX;
	pt2.y = rect->MaxY;
	AdjustCoords(printRec, &pt1);
	AdjustCoords(printRec, &pt2);
	sprintf(psTextBuff, "%d %d %d %d %s %s Rect\n", pt1.x, pt1.y, pt2.x, pt2.y,
			(doPen)  ? "true" : "false", (doFill) ? "true" : "false");
	return (SendText(printRec->File, psTextBuff));
}

/*
 *	Oval
 */

BOOL PSOval(PrintRecPtr printRec, RectPtr rect, BOOL doPen, BOOL doFill)
{
	Point	pt1, pt2;

	pt1.x = rect->MinX;
	pt1.y = rect->MinY;
	pt2.x = rect->MaxX;
	pt2.y = rect->MaxY;
	AdjustCoords(printRec, &pt1);
	AdjustCoords(printRec, &pt2);
	sprintf(psTextBuff, "%d %d %d %d %s %s Oval\n", pt1.x, pt1.y, pt2.x, pt2.y,
			(doPen)  ? "true" : "false", (doFill) ? "true" : "false");
	return (SendText(printRec->File, psTextBuff));
}

/*
 *	Polygon
 */

BOOL PSPoly(PrintRecPtr printRec, WORD numPaths, WORD numPts[], PointPtr pts[],
			BOOL doPen, BOOL doFill, BOOL closed, BOOL smoothed)
{
	WORD	i, j;
	Point	pt, *points;

	sprintf(psTextBuff, "%d %s %s %s %s Poly\n", numPaths,
			(doPen)  ? "true" : "false", (doFill)   ? "true" : "false",
			(closed) ? "true" : "false", (smoothed) ? "true" : "false");
	if (!SendText(printRec->File, psTextBuff))
		return (FALSE);

	for (j = 0; j < numPaths; j++) {
		points = pts[j];
		sprintf(psTextBuff, "%04X\n", numPts[j]);
		if (!SendText(printRec->File, psTextBuff))
			return (FALSE);
		for (i = 0; i < numPts[j]; i++) {
			pt = points[i];
			AdjustCoords(printRec, &pt);
			sprintf(psTextBuff, "%04X %04X\n", pt.x, pt.y);
			if (!SendText(printRec->File, psTextBuff))
				return (FALSE);
		}
	}
	return (TRUE);
}

/*
 *	BMap
 */

BOOL PSBMap(PrintRecPtr printRec, RectPtr rect, WORD width, WORD height,
			RGBColorPtr data)
{
	register WORD	x, y, len;
	RGBColor		color;
	Point			pt1, pt2;
/*
	Send BMap/Image operator
*/
	pt1.x = rect->MinX;
	pt1.y = rect->MinY;
	pt2.x = rect->MaxX;
	pt2.y = rect->MaxY;
	AdjustCoords(printRec, &pt1);
	AdjustCoords(printRec, &pt2);
	sprintf(psTextBuff, "%d %d %d %d %d %d %s %s\n", pt1.x, pt1.y, pt2.x, pt2.y,
			width, height,
			(postScriptOptions.Colors == PS_COLORFULL) ? "true" : "false",
			(postScriptOptions.FastPicts) ? "Image" : "BMap");
	if (!SendText(printRec->File, psTextBuff))
		return (FALSE);
/*
	Send data
*/
	len = 0;
	for (y = 0; y < height ; y++) {
		for (x = 0; x < width; x++) {
			if (len >= 80) {
				strcpy(psTextBuff + len, "\n");
				if (!SendText(printRec->File, psTextBuff))
					return (FALSE);
				len = 0;
			}
			color = data[y*width + x];
			if (postScriptOptions.FastPicts && color == RGBCOLOR_TRANSPARENT)
				color = RGBCOLOR_WHITE;
			switch (postScriptOptions.Colors) {
			case PS_COLORFULL:
				if (postScriptOptions.FastPicts) {
					sprintf(psTextBuff + len, "%03X", color);
					len += 3;
				}
				else {
					sprintf(psTextBuff + len, "%04X", color);
					len += 4;
				}
				break;
			case PS_GRAYSCALE:
			case PS_BLACKWHITE:
			default:
				if (postScriptOptions.FastPicts) {
					sprintf(psTextBuff + len, "%01X", (GrayValue(color) >> 4) & 0x0F);
					len += 1;
				}
				else {
					sprintf(psTextBuff + len, "%02X",
							(color != RGB_TRANSPARENT) ?
							(GrayValue(color) >> 1) & 0x7F : 0x80);
					len += 2;
				}
				break;
			}
		}
		if (postScriptOptions.FastPicts && (width & 1)) {
			strcpy(psTextBuff + len, "0");		// Pad each scan line to byte boundary
			len += 1;
		}
	}
	if (len != 0) {
		strcpy(psTextBuff + len, "\n");
		if (!SendText(printRec->File, psTextBuff))
			return (FALSE);
	}
	return (TRUE);
}

/*
 *	Select font
 */

BOOL PSSelectFont(PrintRecPtr printRec, FontNum fontNum, FontSize fontSize, WORD style)
{
	TextPtr		fontName;
	WORD		i, psStyleBits;
	BOOL		amigaEncoding;
	LONG		offset, offsets[4];
	File		metricFile;
	FontMetric	fontMetric;

/*
	Skip if this is already selected as the current font
*/
	psStyleBits = (style & (FSF_BOLD | FSF_ITALIC));
	if (prevFontNum != -1 && fontNum == prevFontNum && psStyleBits == prevStyleBits
		&& prevFontSize != -1 && fontSize == prevFontSize)
		return (TRUE);
	prevFontNum = fontNum;
	prevFontSize = fontSize;
	prevStyleBits = psStyleBits;
/*
	Get PostScript font name from metric file
*/
	fontName = NULL;
	amigaEncoding = TRUE;
	strcpy(psTextBuff, "fonts:");
	GetFontName(fontNum, psTextBuff + strlen(psTextBuff));
	strcat(psTextBuff, ".metric");
	if ((metricFile = Open(psTextBuff, MODE_OLDFILE)) != NULL &&
		Read(metricFile, offsets, 4*sizeof(LONG)) == 4*sizeof(LONG)) {
		switch (psStyleBits) {
		case FS_NORMAL:
			offset = offsets[0];
			break;
		case FSF_BOLD:
			offset = offsets[1];
			break;
		case FSF_ITALIC:
			offset = offsets[2];
			break;
		default:				// FSF_BOLD and FSF_ITALIC
			offset = offsets[3];
			break;
		}
		if (offset == 0) {		// Requested style does not exist
			for (i = 0; i < 4; i++) {
				if ((offset = offsets[i]) != 0)	// Find first installed font
					break;
			}
		}
		if (offset != 0 && Seek(metricFile, offset, OFFSET_BEGINNING) != -1 &&
			Read(metricFile, &fontMetric, sizeof(FontMetric)) == sizeof(FontMetric)) {
			fontName = fontMetric.PSFontName;
			amigaEncoding = fontMetric.AmigaEncoding;
		}
		Close(metricFile);
	}
/*
	No metric file, so set to default font
*/
	if (fontName == NULL) {
		switch (psStyleBits) {
		case FS_NORMAL:
			fontName = strTimesRoman;
			break;
		case FSF_BOLD:
			fontName = strTimesBold;
			break;
		case FSF_ITALIC:
			fontName = strTimesItalic;
			break;
		default:				// FSF_BOLD and FSF_ITALIC
			fontName = strTimesBoldItalic;
			break;
		}
	}
/*
	Select the font
*/
	sprintf(psTextBuff, "%d (%s) %s SetFont\n", fontSize, fontName,
			(amigaEncoding) ? "true" : "false");
	return (SendText(printRec->File, psTextBuff));
}

/*
 *	Text
 */

BOOL PSText(PrintRecPtr printRec, WORD x, WORD y, WORD strWidth, WORD rectWidth,
			WORD rectHeight, WORD rotate, UWORD flags, WORD style, TextPtr text,
			WORD len)
{
	register WORD		srcIndx, dstIndx;
	register TextChar	ch;
	Point				pt;

	if (len == 0)
		return (TRUE);
	pt.x = x;
	pt.y = y;
	AdjustCoords(printRec, &pt);
	sprintf(psTextBuff, "%d %d %d %d %d %d %s %s %s\n", pt.x, pt.y, strWidth,
			rectWidth, rectHeight, rotate,
			(style & FSF_UNDERLINED) ? "true" : "false",
			(flags & TEXT_FLIPHORIZ) ? "true" : "false",
			(flags & TEXT_FLIPVERT)  ? "true" : "false");
	if (!SendText(printRec->File, psTextBuff))
		return (FALSE);
/*
	Send text string in blocks of 200 or fewer characters
*/
	srcIndx = 0;
	while (srcIndx < len) {
		if (srcIndx == 0) {
			psTextBuff[0] = '(';
			dstIndx = 1;
		}
		else
			dstIndx = 0;
		do {
			ch = text[srcIndx++];
			if (ch > 0x7F) {
				psTextBuff[dstIndx++] = '\\';
				psTextBuff[dstIndx++] = ((ch >> 6) & 7) + '0';
				psTextBuff[dstIndx++] = ((ch >> 3) & 7) + '0';
				psTextBuff[dstIndx++] = ((ch     ) & 7) + '0';
			}
			else if (ch == '(' || ch == ')' || ch == '\\') {
				psTextBuff[dstIndx++] = '\\';
				psTextBuff[dstIndx++] = ch;
			}
			else
				psTextBuff[dstIndx++] = ch;
		} while (srcIndx < len && dstIndx < 200);
		strcpy(psTextBuff + dstIndx, (srcIndx < len) ? "\\\n" : ")\n");
		if (!SendText(printRec->File, psTextBuff))
			return (FALSE);
	}
	return (SendText(printRec->File, "DWText\n"));
}

/*
 *	EPSF
 */

BOOL PSEPSF(PrintRecPtr printRec, TextPtr name, UBYTE *buffer, ULONG size, Rectangle bounds, Rectangle frame,
					WORD rotate, UWORD flags)
{
	WORD	width, height, origWidth, origHeight;
	Point	pt;

	pt.x = frame.MinX;
	pt.y = frame.MaxY;
	AdjustCoords(printRec, &pt);
	width = frame.MaxX - frame.MinX + 1;
	height = frame.MaxY - frame.MinY + 1;
	origWidth = bounds.MaxX - bounds.MinX + 1;
	origHeight = bounds.MaxY - bounds.MinY + 1;
	switch (rotate) {
		case 0 :
			if (flags & EPSF_FLIPHORIZ)
				pt.x += width;
			if (flags & EPSF_FLIPVERT)
				pt.y += height;
			break;
		case 90 :
			if ((flags & EPSF_FLIPHORIZ) == 0)		// vertical or no flip
				pt.y += width;
			if (flags & EPSF_FLIPVERT)
				pt.x += height;
			break;
		case 180 :
			if ((flags & EPSF_FLIPVERT) == 0) 		// horizontal or no flip
				pt.y += height;
			if ((flags & EPSF_FLIPHORIZ) == 0) 		// vertical or no flip
				pt.x += width;
			break;
		case 270 :
			if ((flags & EPSF_FLIPVERT) == 0)		// horizontal or no flip
				pt.x += height;
			if (flags & EPSF_FLIPHORIZ)
				pt.y += width;
			break;
	}

	sprintf(psTextBuff, "%d %d %d %d %d %d %d %d %d %s %s BeginEPSF\n",
				bounds.MinX, bounds.MinY, pt.x, pt.y, origWidth, origHeight,
				width, height, rotate,
			(flags & EPSF_FLIPHORIZ) ? "true" : "false",
			(flags & EPSF_FLIPVERT) ? "true" : "false");
	if (!SendText(printRec->File, psTextBuff))
		return (FALSE);
	sprintf(psTextBuff, "%%BeginDocument: %s\n", name);
	if (!SendText(printRec->File, psTextBuff))
		return (FALSE);
/*
	Write buffer
*/
	if (Write(printRec->File, buffer, size) != size)
		return (FALSE);
	sprintf(psTextBuff, "\n%%EndDocument\n",name);
	if (!SendText(printRec->File, psTextBuff))
		return (FALSE);
/*
	End EPSF
*/
	if (!SendText(printRec->File, "EndEPSF\n"))
		return (FALSE);

	return (TRUE);
}

/*
 *	Set PostScript default options
 */

void PostScriptDefault()
{
	BlockClear(&postScriptOptions, sizeof(PostScriptOptions));
	postScriptOptions.Device = PS_PARALLEL;
	postScriptOptions.Colors = PS_GRAYSCALE;
	postScriptOptions.FastPicts = TRUE;
	postScriptOptions.NoEOFMark = FALSE;
	postScriptOptions.ScreenType = PS_SCRNDEFAULT;
	postScriptOptions.ScreenFreq = 60;
	postScriptOptions.ScreenAngles[0] = 45;		// Black
	postScriptOptions.ScreenAngles[1] = 15;		// Cyan
	postScriptOptions.ScreenAngles[2] = 75;		// Magenta
	postScriptOptions.ScreenAngles[3] = 0;		// Yellow
}

/*
 *	Set PostScript Options dialog items
 */

static void SetPSDlgItems(PostScriptOptions *psOpts)
{
	BOOL		defaultScrn;
	GadgetPtr	gadgList;

	gadgList = psDlg->FirstGadget;
	defaultScrn = (psOpts->ScreenType == PS_SCRNDEFAULT);

	EnableGadgetItem(gadgList, FREQ_TEXT, psDlg, NULL, !defaultScrn);
	EnableGadgetItem(gadgList, ANGLE_TEXT, psDlg, NULL, !defaultScrn);
	EnableGadgetItem(gadgList, ANGLETYPE_POPUP, psDlg, NULL, !defaultScrn);

	SetGadgetItemValue(gadgList, PARALLEL_RADBTN, psDlg, NULL, psOpts->Device == PS_PARALLEL);
	SetGadgetItemValue(gadgList, SERIAL_RADBTN, psDlg, NULL, psOpts->Device == PS_SERIAL);
	SetGadgetItemValue(gadgList, CUSTOMPORT_RADBTN, psDlg, NULL, psOpts->Device == PS_CUSTOM);

	SetGadgetItemValue(gadgList, GRAYSCALE_RADBTN, psDlg, NULL, psOpts->Colors != PS_COLORFULL);
	SetGadgetItemValue(gadgList, COLOR_RADBTN, psDlg, NULL, psOpts->Colors == PS_COLORFULL);

	SetGadgetItemValue(gadgList, FASTPICTS_BOX, psDlg, NULL, psOpts->FastPicts);
	SetGadgetItemValue(gadgList, EOF_BOX, psDlg, NULL, !psOpts->NoEOFMark);
}

/*
 *	PostScript options dialog filter
 */

static BOOL PostScriptDialogFilter(register IntuiMsgPtr intuiMsg, WORD *item)
{
	register WORD itemHit;
	register ULONG class;

	class = intuiMsg->Class;
	if (intuiMsg->IDCMPWindow == psDlg && (class == GADGETDOWN || class == GADGETUP)) {
		itemHit = GadgetNumber((GadgetPtr) intuiMsg->IAddress);
		if (class == GADGETDOWN && itemHit == CUSTOMPORT_TEXT) {
			ReplyMsg((MsgPtr) intuiMsg);
			*item = CUSTOMPORT_RADBTN;
			return (TRUE);
		}
	}
	return (DialogFilter(intuiMsg, item));
}

/*
 *	PostScript options dialog
 */

void PostScriptOptsDialog()
{
	WORD				item, angleType;
	BOOL				done;
	GadgetPtr			gadgList;
	PostScriptOptions	psOpts;

	psOpts = postScriptOptions;
	angleType = 0;
/*
	Get dialog and set initial values
*/
	dlgList[DLG_POSTSCRIPT]->Gadgets[SCREENTYPE_POPUP].Value = psOpts.ScreenType;
	AutoActivateEnable(FALSE);
	psDlg = GetDialog(dlgList[DLG_POSTSCRIPT], screen, mainMsgPort);
	AutoActivateEnable(TRUE);
	if (psDlg == NULL) {
		Error(ERR_NO_MEM);
		return;
	}
	gadgList = psDlg->FirstGadget;
	OutlineOKButton(psDlg);
	SetPSDlgItems(&psOpts);
	SetEditItemText(gadgList, CUSTOMPORT_TEXT, psDlg, NULL, psOpts.CustomName);
	NumToString(psOpts.ScreenFreq, strBuff);
	SetEditItemText(gadgList, FREQ_TEXT, psDlg, NULL, strBuff);
	NumToString(psOpts.ScreenAngles[angleType], strBuff);
	SetEditItemText(gadgList, ANGLE_TEXT, psDlg, NULL, strBuff);
/*
	Handle dialog
*/
	done = FALSE;
	do {
		item = ModalDialog(mainMsgPort, psDlg, PostScriptDialogFilter);
		switch (item) {
		case OK_BUTTON:
		case CANCEL_BUTTON:
			done = TRUE;
			break;
		case SERIAL_RADBTN:
			psOpts.Device = PS_SERIAL;
			break;
		case PARALLEL_RADBTN:
			psOpts.Device = PS_PARALLEL;
			break;
		case CUSTOMPORT_RADBTN:
			psOpts.Device = PS_CUSTOM;
			break;
		case GRAYSCALE_RADBTN:
			psOpts.Colors = PS_GRAYSCALE;
			break;
		case COLOR_RADBTN:
			psOpts.Colors = PS_COLORFULL;
			break;
		case SCREENTYPE_POPUP:
			psOpts.ScreenType = GetGadgetValue(GadgetItem(gadgList, SCREENTYPE_POPUP));
			break;
		case ANGLETYPE_POPUP:
			GetEditItemText(gadgList, ANGLE_TEXT, strBuff);
			psOpts.ScreenAngles[angleType] = StringToNum(strBuff);
			angleType = GetGadgetValue(GadgetItem(gadgList, ANGLETYPE_POPUP));
			NumToString(psOpts.ScreenAngles[angleType], strBuff);
			SetEditItemText(gadgList, ANGLE_TEXT, psDlg, NULL, strBuff);
			break;
		case FASTPICTS_BOX:
			psOpts.FastPicts = !psOpts.FastPicts;
			break;
		case EOF_BOX:
			psOpts.NoEOFMark = !psOpts.NoEOFMark;
			break;
		}
		if (!done && item != -1) {
			SetPSDlgItems(&psOpts);
			if (item == CUSTOMPORT_RADBTN)
				ActivateGadget(GadgetItem(gadgList, CUSTOMPORT_TEXT), psDlg, NULL);
		}
	} while (!done);
	GetEditItemText(gadgList, CUSTOMPORT_TEXT, psOpts.CustomName);
	GetEditItemText(gadgList, FREQ_TEXT, strBuff);
	psOpts.ScreenFreq = StringToNum(strBuff);
	GetEditItemText(gadgList, ANGLE_TEXT, strBuff);
	psOpts.ScreenAngles[angleType] = StringToNum(strBuff);
	DisposeDialog(psDlg);
/*
	Set values
*/
	if (item == OK_BUTTON)
		postScriptOptions = psOpts;
}
