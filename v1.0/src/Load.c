/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Load routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <proto/graphics.h>f
#include <proto/dos.h>

#include <string.h>

#include <Toolbox/Memory.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Utility.h>
#include <Toolbox/StdFile.h>

#include <IFF/GIO.h>
#include <IFF/ILBM.h>
#include <IFF/Packer.h>

#include "Draw.h"
#include "Proto.h"

/*
 *	External variables
 */

extern TextChar	fileBuff[];

extern LONG	iffError;

extern Defaults	defaults;

/*
 *	Local variables and definitions
 */

#define ID_BINARY	(0x000003F3L)

#define FILETYPE_DRAW	0
#define FILETYPE_BAD	1

typedef struct {
	ClientFrame	ClientFrame;
	DocDataPtr	DocData;
	DocLayerPtr	DocLayer;		/* Layer being added to */
	GroupObjPtr	Group;			/* Group being added to */
	FillPatNum	NumFillPats;
	RGBPat8Ptr	FillPats;		/* Array of fill patterns */
} DRAWFrame;

#define GetCAMG(context, vpMode)	\
	IFFReadBytes(context, (BYTE *) vpMode, sizeof(LONG))

typedef struct {
	ClientFrame		ClientFrame;
	UBYTE			FoundBMHD;
	BitMapHeader	BMHdr;
	IFFPictPtr		IFFPict;
} ILBMFrame;

/*
 *	Local prototypes
 */

LONG	UnPackWordRow(WORD *, WORD *, LONG, LONG);

void	FixPoint2Point(FixPtPtr, Point *);
void	FixRect2Rect(FixRectPtr, RectPtr);

IFFP	GetFONT(GroupContext *);
IFFP	GetPATS(GroupContext *, DRAWFrame *);
IFFP	GetPREC(GroupContext *, DocDataPtr);
IFFP	GetDOC(GroupContext *, DocDataPtr);
IFFP	GetLAYR(GroupContext *, DRAWFrame *);
IFFP	GetGRP(GroupContext *, DRAWFrame *);
IFFP	GetEGRP(GroupContext *, DRAWFrame *);
IFFP	GetLINE(GroupContext *, DRAWFrame *);
IFFP	GetRECT(GroupContext *, DRAWFrame *);
IFFP	GetOVAL(GroupContext *, DRAWFrame *);
IFFP	GetPOLY(GroupContext *, DRAWFrame *);
IFFP	GetSTXT(GroupContext *, DRAWFrame *);
IFFP	GetBMAP(GroupContext *, DRAWFrame *);

IFFP	GetFormDRAW(GroupContext *);
BOOL	LoadDRAWFile(DocDataPtr, File);

IFFP	GetFormILBM(GroupContext *);
BOOL	LoadILBMFile(IFFPictPtr, File);

/*
 *	Set page parameters from current print record
 */

void SetPageParams(DocDataPtr docData)
{
	PrintRecPtr printRec = docData->PrintRec;

	PrValidate(printRec);
	docData->xDPI = printRec->xDPI;
	docData->yDPI = printRec->yDPI;
	docData->PageWidth = printRec->PageRect.MaxX - printRec->PageRect.MinX + 1;
	docData->PageHeight = printRec->PageRect.MaxY - printRec->PageRect.MinY + 1;
}

/*
 *	Initialize new document
 */

BOOL NewDocument(DocDataPtr docData)
{
/*
	Allocate new layers
*/
	DisposeAll(docData);
	if (NewDocLayer(docData, NULL) == NULL) {
		DisposeAll(docData);
		return (FALSE);
	}
	SetCurrLayer(docData, TopLayer(docData));
/*
	Setup print record and misc stuff
*/
	BlockMove(&defaults.PrintRec, docData->PrintRec, sizeof(PrintRecord));
	SetPageParams(docData);
	docData->DocWidth = docData->PageWidth;
	docData->DocHeight = docData->PageHeight;
	docData->Flags = defaults.DocFlags & ~DOC_MODIFIED;
	docData->GridUnitsIndex = defaults.GridUnitsIndex;
	docData->TopOffset = docData->LeftOffset = 0;
	docData->Scale = SCALE_FULL;
	return (TRUE);
}

/*
 *	Dispose of all objects in document
 */

void DisposeAll(DocDataPtr docData)
{
	DocLayerPtr docLayer;

	while ((docLayer = BottomLayer(docData)) != NULL) {
		DetachLayer(docData, docLayer);
		DisposeDocLayer(docLayer);
	}
}

/*
 *	Unpack one row of words, updating the source and destination pointers
 *		until it produces dstWords words
 *	Return remaining number of unused source words or -1 if error
 */

static LONG UnPackWordRow(register WORD *src, register WORD *dst,
						  register LONG srcWords, register LONG dstWords)
{
	register WORD num, value;

	while (dstWords > 0) {
		num = *src++;
		srcWords--;
		if (srcWords < 0)
			break;
		if (num >= 0) {
			num++;
			srcWords -= num;
			dstWords -= num;
			if (srcWords < 0 || dstWords < 0)
				break;
			while (num--)
				*dst++ = *src++;
		}
		else {
			num = -num + 1;
			value = *src++;
			srcWords -= 1;
			dstWords -= num;
			if (srcWords < 0 || dstWords < 0)
				break;
			while (num--)
				*dst++ = value;
		}
	}
	return ((srcWords >= 0 && dstWords == 0) ? srcWords : -1);
}

/*
 *	Convert fixed point to point
 */

static void FixPoint2Point(FixPtPtr fixPt, Point *pt)
{
	pt->x = Fix2Long(fixPt->X);
	pt->y = Fix2Long(fixPt->Y);
}

/*
 *	Convert fixed rect to rect
 */

static void FixRect2Rect(FixRectPtr fixRect, RectPtr rect)
{
	rect->MinX = Fix2Long(fixRect->MinX);
	rect->MinY = Fix2Long(fixRect->MinY);
	rect->MaxX = Fix2Long(fixRect->MaxX);
	rect->MaxY = Fix2Long(fixRect->MaxY);
}

/*
 *	Get FONT chunk
 *	Add font number to translation table
 */

static IFFP GetFONT(GroupContext *context)
{
	IFFP iffp;
	FontNum oldFontNum, newFontNum;
	register FontID *fontID;
	UBYTE buff[sizeof(FontID) + 100];

	if (context->ckHdr.ckSize > sizeof(FontID) + 100)
		return (BAD_FORM);
	if ((iffp = IFFReadBytes(context, buff, context->ckHdr.ckSize)) != IFF_OKAY)
		return (iffp);
	fontID = (FontID *) buff;
	oldFontNum = fontID->FontNum;
	newFontNum = FontNumber(fontID->Name);
	(void) AddFontEntry(oldFontNum, newFontNum);
	return (IFF_OKAY);
}

/*
 *	Get PATS chunk
 */

static IFFP GetPATS(GroupContext *context, DRAWFrame *drawFrame)
{
	FillPatNum numPats;

	numPats = context->ckHdr.ckSize/sizeof(RGBPat8);
	if (numPats == 0)
		return (IFF_OKAY);
	if ((drawFrame->FillPats = MemAlloc(numPats*sizeof(RGBPat8), 0)) == NULL)
		return (CLIENT_ERROR);
	drawFrame->NumFillPats = numPats;
	return (IFFReadBytes(context, drawFrame->FillPats, numPats*sizeof(RGBPat8)));
}

/*
 *	Get PREC chunk
 */

static IFFP GetPREC(GroupContext *context, DocDataPtr docData)
{
	IFFP iffp;
	PrintRecInfo printRecInfo;

	if (context->ckHdr.ckSize != sizeof(PrintRecInfo))
		return (BAD_FORM);
	iffp = IFFReadBytes(context, &printRecInfo, sizeof(PrintRecInfo));
	if (iffp != IFF_OKAY)
		return (iffp);
	BlockMove(&printRecInfo.PrintRec, docData->PrintRec, sizeof(PrintRecord));
	SetPageParams(docData);
	return (IFF_OKAY);
}

/*
 *	Get DOC chunk
 */

static IFFP GetDOC(GroupContext *context, DocDataPtr docData)
{
	IFFP iffp;
	DocInfo docInfo;

	if (context->ckHdr.ckSize != sizeof(DocInfo))
		return (BAD_FORM);
	iffp = IFFReadBytes(context, &docInfo, sizeof(DocInfo));
	if (iffp != IFF_OKAY)
		return (iffp);
	if (docInfo.Width <= 0 || docInfo.Width > MAX_DOCWIDTH ||
		docInfo.Height <= 0 || docInfo.Height > MAX_DOCHEIGHT)
		return (BAD_FORM);
	docData->DocWidth		= docInfo.Width;
	docData->DocHeight		= docInfo.Height;
	docData->RulerOffset	= docInfo.RulerOffset;
	return (IFF_OKAY);
}

/*
 *	Get LAYR chunk
 */

static IFFP GetLAYR(GroupContext *context, DRAWFrame *drawFrame)
{
	IFFP iffp;
	TextPtr name;
	DocLayerPtr docLayer;
	DocLayerInfo *docLayerInfo;
	UBYTE buff[sizeof(DocLayerInfo) + 100];

	if (drawFrame->Group != NULL ||
		context->ckHdr.ckSize > sizeof(DocLayerInfo) + 100)
		return (BAD_FORM);
	iffp = IFFReadBytes(context, buff, context->ckHdr.ckSize);
	if (iffp != IFF_OKAY)
		return (iffp);
	docLayerInfo = (DocLayerInfo *) buff;
	name = docLayerInfo->Name;
	if (*name == '\0')
		name = NULL;
	if ((docLayer = NewDocLayer(drawFrame->DocData, name)) == NULL)
		return (CLIENT_ERROR);
	docLayer->Flags		= docLayerInfo->Flags;
	drawFrame->DocLayer	= docLayer;
	return (IFF_OKAY);
}

/*
 *	Get GRP chunk
 */

static IFFP GetGRP(GroupContext *context, DRAWFrame *drawFrame)
{
	IFFP iffp;
	DocLayerPtr docLayer;
	GroupObjPtr groupObj;
	GroupInfo groupInfo;

/*
	Create new group object
*/
	if (context->ckHdr.ckSize < sizeof(GroupInfo))
		return (BAD_FORM);
	iffp = IFFReadBytes(context, &groupInfo, sizeof(GroupInfo));
	if (iffp != IFF_OKAY)
		return (iffp);
	docLayer = (drawFrame->Group) ? NULL : drawFrame->DocLayer;
	if ((groupObj = (GroupObjPtr) NewDocObject(docLayer, TYPE_GROUP)) == NULL)
		return (CLIENT_ERROR);
	if (drawFrame->Group)
		AppendToGroup(drawFrame->Group, (DocObjPtr) groupObj);
/*
	Set group object data
*/
	groupObj->DocObj.Flags = groupInfo.Flags;
/*
	Begin adding to this group
*/
	drawFrame->Group = groupObj;
	return (IFF_OKAY);
}

/*
 *	Get EGRP chunk
 */

static IFFP GetEGRP(GroupContext *context, DRAWFrame *drawFrame)
{
	if (drawFrame->Group == NULL || context->ckHdr.ckSize != 0)
		return (BAD_FORM);
	GroupAdjustFrame(drawFrame->Group);
	drawFrame->Group = drawFrame->Group->DocObj.Group;
}

/*
 *	Get LINE chunk
 */

static IFFP GetLINE(GroupContext *context, DRAWFrame *drawFrame)
{
	IFFP iffp;
	DocLayerPtr docLayer;
	LineObjPtr lineObj;
	LineInfo lineInfo;
	Point pt, start, end;

/*
	Create new line object
*/
	if (context->ckHdr.ckSize != sizeof(LineInfo))
		return (BAD_FORM);
	iffp = IFFReadBytes(context, &lineInfo, sizeof(LineInfo));
	if (iffp != IFF_OKAY)
		return (iffp);
	docLayer = (drawFrame->Group) ? NULL : drawFrame->DocLayer;
	if ((lineObj = (LineObjPtr) NewDocObject(docLayer, TYPE_LINE)) == NULL)
		return (CLIENT_ERROR);
	if (drawFrame->Group)
		AppendToGroup(drawFrame->Group, (DocObjPtr) lineObj);
/*
	Set line object data
*/
	lineObj->DocObj.Flags	= lineInfo.Flags;
	lineObj->LineFlags		= lineInfo.LineFlags;
	lineObj->PenColor		= lineInfo.PenColor;
	FixPoint2Point(&lineInfo.PenSize, &pt);
	lineObj->PenWidth		= pt.x;
	lineObj->PenHeight		= pt.y;
	FixPoint2Point(&lineInfo.Start, &start);
	FixPoint2Point(&lineInfo.End, &end);
	SetFrameRect(&lineObj->DocObj.Frame, start.x, start.y, end.x, end.y);
	OffsetPoint(&start, (WORD) -lineObj->DocObj.Frame.MinX, (WORD) -lineObj->DocObj.Frame.MinY);
	OffsetPoint(&end, (WORD) -lineObj->DocObj.Frame.MinX, (WORD) -lineObj->DocObj.Frame.MinY);
	lineObj->Start	= start;
	lineObj->End	= end;
	return (IFF_OKAY);
}

/*
 *	Get RECT chunk
 */

static IFFP GetRECT(GroupContext *context, DRAWFrame *drawFrame)
{
	IFFP iffp;
	RGBPat8Ptr fillPat;
	DocLayerPtr docLayer;
	RectObjPtr rectObj;
	RectInfo rectInfo;
	Point pt;

/*
	Create new rect object
*/
	if (context->ckHdr.ckSize != sizeof(RectInfo))
		return (BAD_FORM);
	iffp = IFFReadBytes(context, &rectInfo, sizeof(RectInfo));
	if (iffp != IFF_OKAY)
		return (iffp);
	docLayer = (drawFrame->Group) ? NULL : drawFrame->DocLayer;
	if ((rectObj = (RectObjPtr) NewDocObject(docLayer, TYPE_RECT)) == NULL)
		return (CLIENT_ERROR);
	if (drawFrame->Group)
		AppendToGroup(drawFrame->Group, (DocObjPtr) rectObj);
/*
	Set rect object data
*/
	rectObj->DocObj.Flags	= rectInfo.Flags;
	rectObj->PenColor		= rectInfo.PenColor;
	FixPoint2Point(&rectInfo.PenSize, &pt);
	rectObj->PenWidth		= pt.x;
	rectObj->PenHeight		= pt.y;
	if (rectInfo.FillPatNum < drawFrame->NumFillPats)
		fillPat = &drawFrame->FillPats[rectInfo.FillPatNum];
	else
		fillPat = &defaults.FillPat;
	CopyRGBPat8(fillPat, rectObj->FillPat);
	FixRect2Rect(&rectInfo.Frame, &rectObj->DocObj.Frame);
	return (IFF_OKAY);
}

/*
 *	Get OVAL chunk
 */

static IFFP GetOVAL(GroupContext *context, DRAWFrame *drawFrame)
{
	IFFP iffp;
	RGBPat8Ptr fillPat;
	DocLayerPtr docLayer;
	OvalObjPtr ovalObj;
	OvalInfo ovalInfo;
	Point pt;

/*
	Create new oval object
*/
	if (context->ckHdr.ckSize != sizeof(OvalInfo))
		return (BAD_FORM);
	iffp = IFFReadBytes(context, &ovalInfo, sizeof(OvalInfo));
	if (iffp != IFF_OKAY)
		return (iffp);
	docLayer = (drawFrame->Group) ? NULL : drawFrame->DocLayer;
	if ((ovalObj = (OvalObjPtr) NewDocObject(docLayer, TYPE_OVAL)) == NULL)
		return (CLIENT_ERROR);
	if (drawFrame->Group)
		AppendToGroup(drawFrame->Group, (DocObjPtr) ovalObj);
/*
	Set oval object data
*/
	ovalObj->DocObj.Flags	= ovalInfo.Flags;
	ovalObj->PenColor		= ovalInfo.PenColor;
	FixPoint2Point(&ovalInfo.PenSize, &pt);
	ovalObj->PenWidth		= pt.x;
	ovalObj->PenHeight		= pt.y;
	if (ovalInfo.FillPatNum < drawFrame->NumFillPats)
		fillPat = &drawFrame->FillPats[ovalInfo.FillPatNum];
	else
		fillPat = &defaults.FillPat;
	CopyRGBPat8(fillPat, ovalObj->FillPat);
	FixRect2Rect(&ovalInfo.Frame, &ovalObj->DocObj.Frame);
	return (IFF_OKAY);
}

/*
 *	Get POLY chunk
 */

static IFFP GetPOLY(GroupContext *context, DRAWFrame *drawFrame)
{
	IFFP iffp;
	WORD numPoints;
	RGBPat8Ptr fillPat;
	DocLayerPtr docLayer;
	PolyObjPtr polyObj;
	PolyInfo polyInfo;
	PolyPath polyPath;
	Point pt;
	FixPoint fixPt;
	Rectangle frame, rect;

/*
	Create new poly object
*/
	iffp = IFFReadBytes(context, &polyInfo, sizeof(PolyInfo));
	if (iffp != IFF_OKAY)
		return (iffp);
	docLayer = (drawFrame->Group) ? NULL : drawFrame->DocLayer;
	if ((polyObj = (PolyObjPtr) NewDocObject(docLayer, TYPE_POLY)) == NULL)
		return (CLIENT_ERROR);
	if (drawFrame->Group)
		AppendToGroup(drawFrame->Group, (DocObjPtr) polyObj);
/*
	Set poly object data
*/
	polyObj->DocObj.Flags	= polyInfo.Flags;
	polyObj->PolyFlags		= polyInfo.PolyFlags;
	polyObj->PenColor		= polyInfo.PenColor;
	FixPoint2Point(&polyInfo.PenSize, &pt);
	polyObj->PenWidth		= pt.x;
	polyObj->PenHeight		= pt.y;
	if (polyInfo.FillPatNum < drawFrame->NumFillPats)
		fillPat = &drawFrame->FillPats[polyInfo.FillPatNum];
	else
		fillPat = &defaults.FillPat;
	CopyRGBPat8(fillPat, polyObj->FillPat);
	FixRect2Rect(&polyInfo.Frame, &polyObj->DocObj.Frame);
	frame = polyObj->DocObj.Frame;
	OffsetRect(&frame, (WORD) -frame.MinX, (WORD) -frame.MinY);
/*
	Get poly points
*/
	iffp = IFFReadBytes(context, &polyPath, sizeof(PolyPath));
	if (iffp != IFF_OKAY)
		return (iffp);
	numPoints = polyPath.NumPoints;
	if (numPoints < 0)
		return (BAD_FORM);
	SetRect(&rect, 0, 0, 0x7FFF, 0x7FFF);
	while (numPoints--) {
		iffp = IFFReadBytes(context, &fixPt, sizeof(FixPoint));
		if (iffp != IFF_OKAY)
			return (iffp);
		FixPoint2Point(&fixPt, &pt);
		MapPt(&pt, &rect, &frame);
		PolyAddPoint(polyObj, 0x7FFF, pt.x, pt.y);
	}
	PolyAdjustFrame(polyObj);
	return (IFF_OKAY);
}

/*
 *	Get STXT chunk
 */

static IFFP GetSTXT(GroupContext *context, DRAWFrame *drawFrame)
{
	IFFP iffp;
	TextPtr text;
	WORD textLen, cx, cy;
	RGBPat8Ptr fillPat;
	DocLayerPtr docLayer;
	TextObjPtr textObj;
	TextInfo textInfo;

/*
	Create new text object
*/
	textLen = context->ckHdr.ckSize - sizeof(TextInfo);
	if (textLen < 0)
		return (BAD_FORM);
	iffp = IFFReadBytes(context, &textInfo, sizeof(TextInfo));
	if (iffp != IFF_OKAY)
		return (iffp);
	docLayer = (drawFrame->Group) ? NULL : drawFrame->DocLayer;
	if ((textObj = (TextObjPtr) NewDocObject(docLayer, TYPE_TEXT)) == NULL ||
		(text = MemAlloc(textLen, 0)) == NULL)
		return (CLIENT_ERROR);
	textObj->Text = text;
	textObj->TextLen = textLen;
	iffp = IFFReadBytes(context, text, textLen);
	if (iffp != IFF_OKAY)
		return (iffp);
	if (drawFrame->Group)
		AppendToGroup(drawFrame->Group, (DocObjPtr) textObj);
/*
	Set text object data
*/
	textObj->DocObj.Flags	= textInfo.Flags;
	textObj->TextFlags		= textInfo.TextFlags;
	textObj->PenColor		= textInfo.PenColor;
	if (textInfo.FillPatNum < drawFrame->NumFillPats)
		fillPat = &drawFrame->FillPats[textInfo.FillPatNum];
	else
		fillPat = &defaults.FillPat;
	CopyRGBPat8(fillPat, textObj->FillPat);
	FixRect2Rect(&textInfo.Frame, &textObj->DocObj.Frame);
	textObj->Rotate			= NormalizeAngle(Fix2Long(textInfo.Rotate));
	(void) GetFontEntry(textInfo.FontNum, &textObj->FontNum);
	textObj->FontSize		= textInfo.FontSize;
	textObj->Style			= textInfo.Style;
	textObj->MiscStyle		= textInfo.MiscStyle;
	textObj->Justify		= textInfo.Justify;
	textObj->Spacing		= textInfo.Spacing;
	cx = (textObj->DocObj.Frame.MaxX + textObj->DocObj.Frame.MinX + 1)/2;
	cy = (textObj->DocObj.Frame.MaxY + textObj->DocObj.Frame.MinY + 1)/2;
	RotateRect(&textObj->DocObj.Frame, cx, cy, textObj->Rotate);
	TextAdjustFrame(textObj);
	return (IFF_OKAY);
}

/*
 *	Get BMAP chunk
 */

static IFFP GetBMAP(GroupContext *context, DRAWFrame *drawFrame)
{
	IFFP iffp;
	WORD width, height, row;
	LONG dataSize, buffWords, buffNum;
	WORD *cmpData;
	DocLayerPtr docLayer;
	BMapObjPtr bMapObj;
	BitMapInfo bitMapInfo;

/*
	Create new bitmap object
*/
	iffp = IFFReadBytes(context, &bitMapInfo, sizeof(BitMapInfo));
	if (iffp != IFF_OKAY)
		return (iffp);
	width = bitMapInfo.Width;
	height = bitMapInfo.Height;
	dataSize = (LONG) width*height*sizeof(RGBColor);
	docLayer = (drawFrame->Group) ? NULL : drawFrame->DocLayer;
	if ((bMapObj = (BMapObjPtr) NewDocObject(docLayer, TYPE_BMAP)) == NULL ||
		(bMapObj->Data = MemAlloc(dataSize, 0)) == NULL)
		return (CLIENT_ERROR);
	bMapObj->Width = width;
	bMapObj->Height = height;
	if (bitMapInfo.Compression == BMAP_CMPNONE)
		iffp = IFFReadBytes(context, bMapObj->Data, dataSize);
	else if (bitMapInfo.Compression == BMAP_CMPWORDRUN) {
		buffWords = (context->ckHdr.ckSize - sizeof(BitMapInfo))/sizeof(WORD);
		if ((cmpData = MemAlloc(buffWords*sizeof(WORD), 0)) == NULL)
			return (CLIENT_ERROR);
		iffp = IFFReadBytes(context, cmpData, buffWords*sizeof(WORD));
		if (iffp == IFF_OKAY) {
			buffNum = buffWords;
			for (row = 0; row < height; row++) {
				buffNum = UnPackWordRow(cmpData + buffWords - buffNum,
										bMapObj->Data + row*width, buffNum, width);
				if (buffNum < 0) {
					iffp = BAD_FORM;
					break;
				}
			}
		}
		MemFree(cmpData, buffWords*sizeof(WORD));
	}
	else
		return (BAD_FORM);
	if (iffp != IFF_OKAY)
		return (iffp);
	if (drawFrame->Group)
		AppendToGroup(drawFrame->Group, (DocObjPtr) bMapObj);
/*
	Set bitmap data
*/
	bMapObj->DocObj.Flags = bitMapInfo.Flags;
	FixRect2Rect(&bitMapInfo.Frame, &bMapObj->DocObj.Frame);
	return (IFF_OKAY);
}

/*
 *	Get contents of IFF DRAW form
 */

static IFFP GetFormDRAW(GroupContext *parent)
{
	register IFFP iffp;
	BOOL gotPREC, gotDOC, gotLAYR;
	GroupContext formContext;
	DRAWFrame drawFrame;

	if (parent->subtype != ID_DRAW)
		return (IFF_OKAY);
	drawFrame = *((DRAWFrame *) parent->clientFrame);
	iffp = OpenRGroup(parent, &formContext);
	if (iffp != IFF_OKAY)
		return (iffp);
	DisposeFontTable();			/* Just to be sure it are gone */
	gotPREC = gotDOC = gotLAYR = FALSE;
/*
	Get and handle chunks
*/
	do {
		NextBusyPointer();
		switch (iffp = GetFChunkHdr(&formContext)) {
		case ID_FONT:
			iffp = (gotPREC) ? BAD_FORM : GetFONT(&formContext);
			break;
		case ID_PATS:
			iffp = (gotPREC) ? BAD_FORM : GetPATS(&formContext, &drawFrame);
			break;
		case ID_PREC:
			iffp = (gotPREC) ? BAD_FORM : GetPREC(&formContext, drawFrame.DocData);
			gotPREC = TRUE;
			break;
		case ID_DOC:
			iffp = (!gotPREC || gotDOC) ? BAD_FORM : GetDOC(&formContext, drawFrame.DocData);
			gotDOC = TRUE;
			break;
		case ID_LAYR:
			iffp = (!gotDOC) ? BAD_FORM : GetLAYR(&formContext, &drawFrame);
			gotLAYR = TRUE;
			break;
		case ID_GRP:
			iffp = (!gotLAYR) ? BAD_FORM : GetGRP(&formContext, &drawFrame);
			break;
		case ID_EGRP:
			iffp = (drawFrame.Group == NULL) ? BAD_FORM : GetEGRP(&formContext, &drawFrame);
			break;
		case ID_LINE:
			iffp = (!gotLAYR) ? BAD_FORM : GetLINE(&formContext, &drawFrame);
			break;
		case ID_RECT:
			iffp = (!gotLAYR) ? BAD_FORM : GetRECT(&formContext, &drawFrame);
			break;
		case ID_OVAL:
			iffp = (!gotLAYR) ? BAD_FORM : GetOVAL(&formContext, &drawFrame);
			break;
		case ID_POLY:
			iffp = (!gotLAYR) ? BAD_FORM : GetPOLY(&formContext, &drawFrame);
			break;
		case ID_STXT:
			iffp = (!gotLAYR) ? BAD_FORM : GetSTXT(&formContext, &drawFrame);
			break;
		case ID_BMAP:
			iffp = (!gotLAYR) ? BAD_FORM : GetBMAP(&formContext, &drawFrame);
			break;
/*
	End of file
*/
		case END_MARK:
			iffp = IFF_DONE;
			break;
		}
	} while (iffp >= IFF_OKAY);
	DisposeFontTable();
	if (drawFrame.NumFillPats)
		MemFree(drawFrame.FillPats, drawFrame.NumFillPats*sizeof(RGBPat8));
	if (iffp != IFF_DONE)
		return (iffp);
	CloseRGroup(&formContext);
	return (iffp);
}

/*
 *	Load DRAW file
 *	Return success status
 *	Very simple IFF reader, ignores LISTs, CATs, and PROPs
 */

static BOOL LoadDRAWFile(DocDataPtr docData, File file)
{
	DRAWFrame drawFrame;

/*
	Initialize drawFrame and read file
*/
	drawFrame.ClientFrame.getList = drawFrame.ClientFrame.getProp =
		drawFrame.ClientFrame.getCat = SkipGroup;
	drawFrame.ClientFrame.getForm = GetFormDRAW;
	drawFrame.DocData = docData;
	drawFrame.DocLayer = NULL;
	drawFrame.Group = NULL;
	drawFrame.NumFillPats = 0;
	drawFrame.FillPats = NULL;
	DisposeAll(docData);
	iffError = ReadIFF(file, (ClientFrame *) &drawFrame);
	return ((BOOL) (iffError == IFF_DONE));
}

/*
 *	Load file
 *	Determine file type and load
 *	Return success status
 */

BOOL LoadFile(WindowPtr window, DocDataPtr docData, TextPtr fileName, Dir dir)
{
	register WORD fileType;
	register BOOL success;
	register File file;
	GroupHeader grpHdr;

	SetStdPointer(window, POINTER_WAIT);
	if (!NewDocument(docData)) {
		iffError = CLIENT_ERROR;
		return (FALSE);
	}
	DrawRuler(window);
/*
	Open the file and determine what type of file it is
*/
	SetCurrentDir(dir);
	if ((file = GOpen(fileName, MODE_OLDFILE)) == NULL) {
		iffError = DOS_ERROR;
		return (FALSE);
	}
	if (GRead(file, &grpHdr, sizeof(GroupHeader)) == sizeof(GroupHeader) &&
		grpHdr.ckID == FORM && grpHdr.grpSubID == ID_DRAW)
		fileType = FILETYPE_DRAW;
	else if (grpHdr.ckID == ID_BINARY)
		fileType = FILETYPE_BAD;
	else
		fileType = FILETYPE_BAD;	/* May add other file types later */
/*
	Load the document
*/
	BeginWait();
	if (fileType == FILETYPE_BAD) {
		iffError = BAD_IFF;
		success = FALSE;
	}
	else if (GSeek(file, 0, OFFSET_BEGINNING) < 0) {
		iffError = DOS_ERROR;
		success = FALSE;
	}
	else if (fileType == FILETYPE_DRAW) {
		SetBusyPointer(window);
		success = LoadDRAWFile(docData, file);
		docData->DirLock = dir;
	}
	else {
		iffError = BAD_IFF;
		success = FALSE;
	}
	GClose(file);
	if (success) {
		strcpy(docData->FileName, fileName);
		SetCurrLayer(docData, TopLayer(docData));
	}
	else {
		DisposeAll(docData);
		if (dir)
			UnLock(dir);
		docData->DirLock = NULL;	/* In case this was Revert operation */
	}
	EndWait();
	return (success);
}

/*
 *	Get contents of IFF ILBM form
 */

static IFFP GetFormILBM(GroupContext *parent)
{
	register IFFP iffp;
	WORD depth;
	LONG vpMode;
	IFFPictPtr iffPict;
	GroupContext formContext;
	ILBMFrame ilbmFrame;

	if (parent->subtype != ID_ILBM)
		return (IFF_OKAY);
	ilbmFrame = *((ILBMFrame *) parent->clientFrame);
	iffPict = ilbmFrame.IFFPict;
	iffp = OpenRGroup(parent, &formContext);
	if (iffp != IFF_OKAY)
		return (iffp);
/*
	Get and handle chunks
*/
	do {
		switch (iffp = GetFChunkHdr(&formContext)) {
		case ID_BMHD:
			ilbmFrame.FoundBMHD = TRUE;
			iffp = GetBMHD(&formContext, &ilbmFrame.BMHdr);
			iffPict->Width = ilbmFrame.BMHdr.w;
			iffPict->Height = ilbmFrame.BMHdr.h;
			iffPict->TranspColor = (ilbmFrame.BMHdr.masking == mskHasTransparentColor) ?
								   ilbmFrame.BMHdr.transparentColor : -1;
			break;
		case ID_CMAP:
			iffPict->NumColors = 256;		/* Have room for this many */
			iffp = GetCMAP(&formContext, &iffPict->ColorTable[0], &iffPict->NumColors);
			break;
		case ID_CAMG:
			iffp = GetCAMG(&formContext, &vpMode);
			if (iffp == IFF_OKAY)
				iffPict->IsHAM = (vpMode & HAM);
			break;
		case ID_BODY:
			if (!ilbmFrame.FoundBMHD) {
				iffp = BAD_FORM;
				break;
			}
			if (iffPict->BitMap) {
				if (iffPict->Mask)
					FreeRaster(iffPict->Mask, iffPict->BitMap->BytesPerRow*8,
							   iffPict->BitMap->Rows);
				DisposeBitMap(iffPict->BitMap);
			}
			iffPict->BitMap = NULL;
			iffPict->Mask = NULL;
			depth = MIN(ilbmFrame.BMHdr.nPlanes, 8);
			iffPict->BitMap = CreateBitMap(ilbmFrame.BMHdr.w, ilbmFrame.BMHdr.h, depth);
			if (iffPict->BitMap && ilbmFrame.BMHdr.masking == mskHasMask)
				iffPict->Mask = AllocRaster(ilbmFrame.BMHdr.w, ilbmFrame.BMHdr.h);
			if (iffPict->BitMap == NULL ||
				(ilbmFrame.BMHdr.masking == mskHasMask && iffPict->Mask == NULL)) {
				iffp = CLIENT_ERROR;
				break;
			}
			iffp = GetBODY(&formContext, iffPict->BitMap, iffPict->Mask,
						   &ilbmFrame.BMHdr, fileBuff, FILEBUFF_SIZE);
			break;
/*
	End of file
*/
		case END_MARK:
			iffp = IFF_DONE;
			break;
		}
	} while (iffp >= IFF_OKAY);
	if (iffp != IFF_DONE)
		return (iffp);
	CloseRGroup(&formContext);
	return (iffp);
}

/*
 *	Load ILBM file
 *	Return success status
 *	Very simple IFF reader, ignores LISTs, CATs, and PROPs
 */

static BOOL LoadILBMFile(IFFPictPtr iffPict, File file)
{
	ILBMFrame ilbmFrame;

/*
	Initialize ilbmFrame and read file
*/
	ilbmFrame.ClientFrame.getList = ilbmFrame.ClientFrame.getProp =
		ilbmFrame.ClientFrame.getCat = SkipGroup;
	ilbmFrame.ClientFrame.getForm = GetFormILBM;
	ilbmFrame.FoundBMHD	= FALSE;
	ilbmFrame.IFFPict	= iffPict;
	iffError = ReadIFF(file, (ClientFrame *) &ilbmFrame);
	return ((BOOL) (iffError == IFF_DONE));
}

/*
 *	Load IFF picture
 *	Return pointer to picture data or NULL
 */

IFFPictPtr LoadPict(TextPtr fileName, Dir dir)
{
	register BOOL success;
	register File file;
	IFFPictPtr iffPict;
	GroupHeader grpHdr;

	if ((iffPict = MemAlloc(sizeof(IFFPict), MEMF_CLEAR)) == NULL) {
		iffError = CLIENT_ERROR;
		return (NULL);
	}
/*
	Open the file and determine what type of file it is
*/
	SetCurrentDir(dir);
	UnLock(dir);
	if ((file = GOpen(fileName, MODE_OLDFILE)) == NULL) {
		iffError = DOS_ERROR;
		return (NULL);
	}
	if (GRead(file, &grpHdr, sizeof(GroupHeader)) < sizeof(GroupHeader) ||
		grpHdr.ckID != FORM || grpHdr.grpSubID != ID_ILBM) {
		GClose(file);
		iffError = BAD_IFF;
		return (NULL);
	}
	if (GSeek(file, 0, OFFSET_BEGINNING) < 0) {
		GClose(file);
		iffError = DOS_ERROR;
		return (NULL);
	}
/*
	Load the document
*/
	BeginWait();
	success = LoadILBMFile(iffPict, file);
	GClose(file);
	if (!success) {
		DisposePict(iffPict);
		iffPict = NULL;
	}
	EndWait();
	return (iffPict);
}

/*
 *	Dispose of picture data created by LoadPict()
 */

void DisposePict(IFFPictPtr iffPict)
{
	if (iffPict) {
		if (iffPict->BitMap) {
			if (iffPict->Mask)
				FreeRaster(iffPict->Mask, iffPict->BitMap->BytesPerRow*8,
						   iffPict->BitMap->Rows);
			DisposeBitMap(iffPict->BitMap);
		}
		MemFree(iffPict, sizeof(IFFPict));
	}
}
