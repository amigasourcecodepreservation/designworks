/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Toolbox
 *	Copyright (c) 1993 New Horizons Software, Inc.
 *
 *	Serialization routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>
#include <dos/dos.h>

#include <proto/exec.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include <string.h>

#include <Typedefs.h>

#include <Toolbox/Border.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Utility.h>

/*
 *	External variables
 */

extern TextChar	_tbStrPersonalize[];

extern DialogTemplate	_tbSerialDlgTempl;

/*
 *	Local variables and definitions
 */

enum {
	NAME_TEXT = 2,
	COMPANY_TEXT,
	SERIAL_TEXT,
	PERSONALIZE_STATTEXT
};

typedef struct BuffIO {
	File	File;
	UBYTE	*Buffer;
	WORD	Index;
	WORD	Size;
} BuffIO, *BuffIOPtr;

#define ID_LEN		5
#define NAME_LEN	34
#define SERIAL_LEN	9

#define ID_VALID	0x69

typedef struct SerialInfo {
	TextChar	ID[ID_LEN + 1];
	TextChar	Name[NAME_LEN + 1];
	TextChar	Company[NAME_LEN + 1];
	TextChar	SerialNum[SERIAL_LEN + 1];
	BYTE		Valid;
} SerialInfo;

static SerialInfo	serialInfo = {
	"$DAT:"
};

/*
 *	Local prototypes
 */

static void	InitBuffer(BuffIOPtr, File, UBYTE *, WORD);
static WORD	GetByte(BuffIOPtr);

static void	EncodeText(TextPtr, WORD);
static void	DecodeText(TextPtr, WORD);
static void	EncodeSerialInfo(void);
static void	DecodeSerialInfo(void);

static BOOL	ValidSerialNum(TextPtr, WORD);

/*
 *	Buffered I/O
 *	Returns -1 if no more bytes
 */

static void InitBuffer(BuffIOPtr buffIO, File file, UBYTE *buff, WORD size)
{
	buffIO->File = file;
	buffIO->Buffer = buff;
	buffIO->Index = buffIO->Size = size;
}

static WORD GetByte(BuffIOPtr buffIO)
{
	if (buffIO->Index >= buffIO->Size) {
		if (buffIO->Size == 0 ||
			(buffIO->Size = Read(buffIO->File, buffIO->Buffer, buffIO->Size)) <= 0)
			return (-1);
		buffIO->Index = 0;
	}
	return ((WORD) buffIO->Buffer[buffIO->Index++]);
}

/*
 *	Encode and decode text
 */

#define	SEED		0xC6

#define	RAND(byte)	(((byte << 1) ^ 0x95) & 0xFF)

static void EncodeText(register TextPtr text, register WORD len)
{
	register UBYTE	rand = SEED;
	register WORD	i;

	for (i = 0; i < len; i++) {
		text[i] = rand = text[i] ^ rand;
		rand = RAND(rand) ^ i;
	}
}

static void DecodeText(register TextPtr text, register WORD len)
{
	register UBYTE		rand = SEED;
	register TextChar	ch;
	register WORD		i;

	for (i = 0; i < len; i++) {
		ch = text[i];
		text[i] = ch ^ rand;
		rand = RAND(ch) ^ i;
	}
}

static void EncodeSerialInfo()
{
	EncodeText(serialInfo.Name, NAME_LEN);
	EncodeText(serialInfo.Company, NAME_LEN);
	EncodeText(serialInfo.SerialNum, SERIAL_LEN);
}

static void DecodeSerialInfo()
{
	DecodeText(serialInfo.Name, NAME_LEN);
	DecodeText(serialInfo.Company, NAME_LEN);
	DecodeText(serialInfo.SerialNum, SERIAL_LEN);
}

/*
 *	Check for valid serial number
 *	Valid numbers are of the form nnn.nnnnn where n is a numeric digit
 */

static BOOL ValidSerialNum(register TextPtr serial, WORD version)
{
	register WORD	i;

	if (strlen(serial) != SERIAL_LEN)
		return (FALSE);
/*
	Check version number
*/
	for (i = 0; i < 3; i++) {
		if (serial[i] - '0' != ((version >> (2 - i)*4) & 0x0F))
			return (FALSE);
	}
	if (serial[3] != '.')
		return (FALSE);
/*
	Check serial number
*/
	for (i = 4; i < SERIAL_LEN; i++) {
		if (serial[i] < '0' || serial[i] > '9')
			return (FALSE);
	}
	return (TRUE);
}

/*
 *	Return serialization info
 *	If info not set, then get it from user and save in executable
 *	Return TRUE if OK, FALSE if user cancelled info
 */

BOOL GetSerialInfo(TextPtr *name, TextPtr *company, TextPtr *serial,
				   ScreenPtr screen, MsgPortPtr msgPort,
				   TextPtr progName, WORD version, TextPtr progPath)
{
#ifdef DEBUG
	*name = "Beta Test Version";
	*company = "Not For Distribution";
	*serial = "Not Serialized";
	return (TRUE);
#else
	BYTE		tempByte;
	WORD		i, item, byte;
	LONG		offset;
	BOOL		done, success;
	GadgetPtr	gadgList;
	DialogPtr	serialDlg;
	File		file;
	BuffIO		buffIO;
	TextChar	idBuff[ID_LEN + 1], strBuff[512];

	*name = serialInfo.Name;
	*company = serialInfo.Company;
	*serial = serialInfo.SerialNum;
/*
	If valid serial number, return info
*/
	if (serialInfo.Valid == ID_VALID) {
		DecodeSerialInfo();
		if (ValidSerialNum(serialInfo.SerialNum, version))
			return (TRUE);
	}
/*
	Otherwise, get info from user
	Only enable OK button if valid serial number is entered
*/
	strcpy(strBuff, _tbStrPersonalize);
	strcat(strBuff, progName);
	_tbSerialDlgTempl.Gadgets[PERSONALIZE_STATTEXT].Info = strBuff;
	if ((serialDlg = GetDialog(&_tbSerialDlgTempl, screen, msgPort)) == NULL)
		return (FALSE);
	gadgList = serialDlg->FirstGadget;
	EnableGadgetItem(gadgList, OK_BUTTON, serialDlg, NULL, FALSE);
	OutlineButton(GadgetItem(gadgList, OK_BUTTON), serialDlg, NULL, TRUE);
	done = FALSE;
	do {
		WaitPort(msgPort);
		item = CheckDialog(msgPort, serialDlg, NULL);
		GetEditItemText(gadgList, SERIAL_TEXT, strBuff);
		EnableGadgetItem(gadgList, OK_BUTTON, serialDlg, NULL,
						 ValidSerialNum(strBuff, version));
		switch (item) {
		case OK_BUTTON:
			if (ValidSerialNum(strBuff, version)) {
				strcpy(serialInfo.SerialNum, strBuff);
				done = TRUE;
			}
			break;
		case CANCEL_BUTTON:
			done = TRUE;
			break;
		}
	} while (!done);
	GetEditItemText(gadgList, NAME_TEXT, strBuff);
	strncpy(serialInfo.Name, strBuff, NAME_LEN);
	GetEditItemText(gadgList, COMPANY_TEXT, strBuff);
	strncpy(serialInfo.Company, strBuff, NAME_LEN);
/*
	Write info to disk
*/
	success = (item == OK_BUTTON);
	file = NULL;
	if (success) {
		EnableGadgetItem(gadgList, OK_BUTTON, serialDlg, NULL, FALSE);
		EnableGadgetItem(gadgList, CANCEL_BUTTON, serialDlg, NULL, FALSE);
		EnableGadgetItem(gadgList, NAME_TEXT, serialDlg, NULL, FALSE);
		EnableGadgetItem(gadgList, COMPANY_TEXT, serialDlg, NULL, FALSE);
		EnableGadgetItem(gadgList, SERIAL_TEXT, serialDlg, NULL, FALSE);
		SetStdPointer(serialDlg, POINTER_WAIT);
		EncodeSerialInfo();						// Write encoded version to disk
		serialInfo.Valid = ID_VALID;
		if ((file = Open(progPath, MODE_OLDFILE)) == NULL) {
			success = FALSE;
			goto Exit;
		}
		InitBuffer(&buffIO, file, strBuff, sizeof(strBuff));
		for (i = 0; i < ID_LEN + 1; i++) {
			if ((byte = GetByte(&buffIO)) == -1) {
				success = FALSE;
				goto Exit;
			}
			idBuff[i] = byte;
		}
		offset = 0;
		while (idBuff[ID_LEN] != '\0' || strcmp(idBuff, serialInfo.ID) != 0) {
			for (i = 1; i < ID_LEN + 1; i++)
				idBuff[i - 1] = idBuff[i];
			if ((byte = GetByte(&buffIO)) == -1) {
				success = FALSE;
				break;
			}
			idBuff[ID_LEN] = byte;
			offset++;
		}
/*
	Note: Due to bug in AmigaDOS 2.0 and greater, if a write is made to a locked
	disk and user removes write-protect tab when requested, write will be made to
	beginning of file, not to Seek'ed position.  Therefore, write to first byte
	in file (which will always be 0) to force system requester if disk is locked.
*/
		if (success) {
			tempByte = 0;
			if (Seek(file, 0, OFFSET_BEGINNING) == -1 ||
				Write(file, &tempByte, 1) != 1)
				success = FALSE;
			else if (Seek(file, offset, OFFSET_BEGINNING) == -1 ||
				Write(file, &serialInfo, sizeof(SerialInfo)) != sizeof(SerialInfo))
				success = FALSE;
		}
		DecodeSerialInfo();
	}
/*
	All done
*/
Exit:
	if (file)
		Close(file);
	DisposeDialog(serialDlg);
	return (success);
#endif
}
