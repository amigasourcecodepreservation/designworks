/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Toolbox library
 *	Copyright (c) 1989 New Horizons Software, Inc.
 *
 *	Fixed point math definitions
 */

#ifndef EXEC_TYPES_H
#include <exec/types.h>
#endif

#ifndef TYPEDEFS_H
#include "typedefs.h"
#endif

/*
 *	Definitions
 */

#define DECIMAL_CHAR	'.'

typedef LONG	Fixed;		/* Integer portion in upper word */

#define FIXED_UNITY	0x00010000L

/*
 *	Prototypes
 */

#define Long2Fix(num)	((Fixed) (((LONG)  (num)) << 16))
#define Fix2Long(num)	((LONG)  (((Fixed) (num)) >> 16))
#define FixRound(num)	((WORD) ((((Fixed) (num)) + 0x8000L) >> 16))

Fixed	FixRatio(WORD, WORD);
Fixed	Ascii2Fix(TextPtr);
void	Fix2Ascii(Fixed, TextPtr, WORD);
Fixed	FixMul(Fixed, Fixed);
Fixed	FixDiv(Fixed, Fixed);
