/* 
 *	GIO.C  Generic I/O Speed Up Package		1/23/86
 *	See GIOCall.C for an example of usage.
 *	Read not speeded-up yet.  Only one Write file buffered at a time.
 *	Note: The speed-up provided is ONLY significant for code such as IFF
 *	which does numerous small Writes and Seeks.
 *
 *	By Jerry Morrison and Steve Shaw, Electronic Arts.
 *	Modified by James Bayless, New Horizons Software
 *  	This software is in the public domain.
 *
 *	This version for the Commodore-Amiga computer.
 */

#include <IFF/GIO.h>		/* See comments here for explanation.*/

#include <proto/dos.h>

#include <string.h>

#if GIO_ACTIVE

static BPTR wFile;				/* Inited to NULL */
static BYTE *wBuffer;			/* Inited to NULL */
static LONG wNBytes;			/* Buffer size in bytes, Inited to 0 */
static LONG wIndex;				/* Index of next available byte, Inited to 0 */
static LONG wWaterline;			/* Count of # bytes to be written, Inited to 0
								 * Different than wIndex because of GSeek */

/*
 *	GOpen
 */

LONG GOpen(char *filename, LONG openmode)
{
	return (Open(filename, openmode));
}

/*
 *	GClose
 */

LONG GClose(BPTR file)
{
	LONG signal;

	signal = (file == wFile) ? GWriteUndeclare(file) : 0;
	Close(file);				/* Call Close even if trouble with write */
	return (signal);
}

/*
 *	GRead
 */

LONG GRead(BPTR file, void *buffer, LONG nBytes)
{
	LONG signal;

/*
	We don't yet read directly from the buffer, so flush it to disk and
	let the DOS fetch it back
*/
	signal = (file == wFile) ? GWriteFlush(file) : 0;
	if (signal >= 0)
		signal = Read(file, buffer, nBytes);
	return (signal);
}

/*
 *	GWriteFlush
 */

LONG GWriteFlush(BPTR file)
{
	LONG gWrite;

	gWrite = (wFile != NULL && wBuffer != NULL && wIndex > 0) ?
			 Write(wFile, wBuffer, wWaterline) : 0;
	wWaterline = wIndex = 0;		/* No matter what, make sure this happens */
	return (gWrite);
}

/*
 *	GWriteDeclare
 */

LONG GWriteDeclare(BPTR file, BYTE *buffer, LONG nBytes)
{
	LONG gWrite;

	gWrite = GWriteFlush(wFile);		/* Finish any existing usage */
	if (file == NULL || (file == wFile && buffer == NULL) || nBytes <= 3) {
		wFile = NULL;
		wBuffer = NULL;
		wNBytes = 0;
	}
	else {
		wFile = file;
		wBuffer = buffer;
		wNBytes = nBytes;
	}
	return (gWrite);
}

/*
 *	GWrite
 */

LONG GWrite(BPTR file, void *buffer, LONG nBytes)
{
	LONG gWrite = 0;

	if (file == wFile && wBuffer != NULL) {
		if (wNBytes >= wIndex + nBytes) {
/*
	Append to wBuffer
*/
			movmem(buffer, wBuffer + wIndex, nBytes);
			wIndex += nBytes;
			if (wIndex > wWaterline)
				wWaterline = wIndex;
			nBytes = 0;				/* Indicate data has been swallowed */
		}
		else {
/*
	We are about to overwrite any
	data above wIndex, up to at least the buffer end
*/
			wWaterline = wIndex;
			gWrite = GWriteFlush(file);		/* Write data out in proper order */
		}
	}
	if (nBytes > 0 && gWrite >= 0)
		gWrite += Write(file, buffer, nBytes);
	return (gWrite);
}

/*
 *	GSeek
 */

LONG GSeek(BPTR file, LONG position, LONG mode)
{
	LONG gSeek = -2;
	LONG newWIndex = wIndex + position;

	if (file == wFile && wBuffer != NULL) {
		if (mode == OFFSET_CURRENT &&
			newWIndex >= 0 && newWIndex <= wWaterline) {
			gSeek = wIndex;		/* Okay; return *OLD* position */
			wIndex = newWIndex;
		}
		else {
/*
	We don't even try to optimize the other cases
*/
			gSeek = GWriteFlush(file);
			if (gSeek >= 0)
				gSeek = -2;		/* OK so far */
		}
	}
	if (gSeek == -2)
		gSeek = Seek(file, position, mode);
	return (gSeek);
}

#else

void GIODummy() { }		/* To keep the compiler happy */

#endif
