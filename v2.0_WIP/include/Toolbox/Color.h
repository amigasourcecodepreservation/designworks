/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Toolbox library
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	Color definitions
 */

#ifndef TOOLBOX_COLOR_H
#define TOOLBOX_COLOR_H

#ifndef TYPEDEFS_H
#include <Typedefs.h>
#endif

typedef UWORD		RGBColor;
typedef RGBColor	*RGBColorPtr;

#define RED(rgb)	(((rgb) >> 8) & 0x0F)
#define GREEN(rgb)	(((rgb) >> 4) & 0x0F)
#define BLUE(rgb)	((rgb) & 0x0F)

#define RGBCOLOR(r,g,b)	(((r) << 8) + ((g) << 4) + (b))

#define RGBDARKEN(rgb)	RGBCOLOR(RED(rgb) >> 1, GREEN(rgb) >> 1, BLUE(rgb) >> 1)

#define RGBCOLOR_BLACK		0x000
#define RGBCOLOR_WHITE		0xFFF
#define RGBCOLOR_RED		0xF00
#define RGBCOLOR_YELLOW		0xFF0
#define RGBCOLOR_GREEN		0x0F0
#define RGBCOLOR_CYAN		0x0FF
#define RGBCOLOR_BLUE		0x00F
#define RGBCOLOR_MAGENTA	0xF0F

#define RGBCOLOR_TRANSPARENT	0xF000

typedef RGBColor	ColorTable[];
typedef ColorTable	*ColorTablePtr;

typedef UWORD		CMYKColor;

#define CMYK_CYAN(cmyk)		(((cmyk) >>  8) & 0x0F)
#define CMYK_MAGENTA(cmyk)	(((cmyk) >>  4) & 0x0F)
#define CMYK_YELLOW(cmyk)	((cmyk) & 0x0F)
#define CMYK_BLACK(cmyk)	(((cmyk) >> 12) & 0x0F)

#define CMYKCOLOR(c,m,y,k)	(((c) << 8) + ((m) << 4) + (y) + ((k) << 12))

typedef UBYTE	PenNum, *PenNumPtr;

#endif
