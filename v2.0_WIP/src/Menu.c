/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Menu routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <proto/intuition.h>

#include <Toolbox/Utility.h>
#include <Toolbox/Menu.h>
#include <Toolbox/Window.h>

#include "Draw.h"
#include "Proto.h"

/*
 *	External variables
 */


extern struct HotLinksBase  *HotLinksBase;

extern WindowPtr	backWindow, toolWindow, penWindow, fillWindow;
extern WindowPtr	windowList[];
extern WORD			numWindows;

extern DocLayer	pasteLayer;

extern Defaults	defaults;

extern UBYTE	penSizes[];

extern struct RexxLib	*RexxSysBase;

/*
 *	Local variables and definitions
 */

/*
 *	Local prototypes
 */

static void	OnOffMenu(WindowPtr, LONG, BOOL);
static void	AddSubMenu(WindowPtr, UWORD, MenuItemPtr);

/*
 *	Turn on or off the specified menu or menu item
 */

static void OnOffMenu(WindowPtr window, LONG menuNum, BOOL on)
{
	if (on)
		OnMenu(window, menuNum);
	else
		OffMenu(window, menuNum);
}

/*
 *	Add submenu if not already set
 */

static void AddSubMenu(WindowPtr window, UWORD item, MenuItemPtr subItems)
{
	MenuItemPtr menuItem;

	menuItem = ItemAddress(window->MenuStrip, (LONG) item);
	if (menuItem->SubItem != subItems)
		InsertSubMenu(window, item, subItems);
}

/*
 *	Adjust Project menu
 */

void SetProjectMenu()
{
	BOOL on;
	WindowPtr window;
	register DocDataPtr docData;

	window = ActiveWindow();
	docData = (DocDataPtr) GetWRefCon(window);

	on = (numWindows < MAX_WINDOWS);
	OnOffMenu(backWindow, MENUITEM(PROJECT_MENU, NEW_ITEM, NOSUB), on);
	OnOffMenu(backWindow, MENUITEM(PROJECT_MENU, OPEN_ITEM, NOSUB), on);

	on = IsDocWindow(window);
	OnOffMenu(backWindow, MENUITEM(PROJECT_MENU, IMPORT_ITEM, NOSUB), on);
	OnOffMenu(backWindow, MENUITEM(PROJECT_MENU, EXPORT_ITEM, PICT_SUBITEM),
			  (on && FirstSelected(docData) != NULL));
	OnOffMenu(backWindow, MENUITEM(PROJECT_MENU, EXPORT_ITEM, NOSUB), on);
	OnOffMenu(backWindow, MENUITEM(PROJECT_MENU, CLOSE_ITEM, NOSUB), on);
	OnOffMenu(backWindow, MENUITEM(PROJECT_MENU, SAVE_ITEM, NOSUB), on);
	OnOffMenu(backWindow, MENUITEM(PROJECT_MENU, SAVEAS_ITEM, NOSUB), on);
	OnOffMenu(backWindow, MENUITEM(PROJECT_MENU, REVERT_ITEM, NOSUB),
			  (on && docData->DirLock));
	OnOffMenu(backWindow, MENUITEM(PROJECT_MENU, PAGESETUP_ITEM, NOSUB), on);
	OnOffMenu(backWindow, MENUITEM(PROJECT_MENU, PRINTONE_ITEM, NOSUB), on);
	OnOffMenu(backWindow, MENUITEM(PROJECT_MENU, PRINT_ITEM, NOSUB), on);
	OnOffMenu(backWindow, MENUITEM(PROJECT_MENU, DEFAULTS_ITEM, NOSUB), on);
}

/*
 *	Adjust Edit menu
 */

void SetEditMenu()
{
	BOOL 		on, convert, close, open, smooth, unsmooth,
				unmerge, merge, polySel;
	DocObjPtr 	docObj, firstSelObj;
	TextObjPtr 	textObj;
	DocDataPtr 	docData;
	WindowPtr 	window;

	window = ActiveWindow();
	if (!IsDocWindow(window)) {
		OffMenu(backWindow, MENUITEM(EDIT_MENU, CUT_ITEM, NOSUB));
		OffMenu(backWindow, MENUITEM(EDIT_MENU, COPY_ITEM, NOSUB));
		OffMenu(backWindow, MENUITEM(EDIT_MENU, PASTE_ITEM, NOSUB));
		OffMenu(backWindow, MENUITEM(EDIT_MENU, ERASE_ITEM, NOSUB));
		OffMenu(backWindow, MENUITEM(EDIT_MENU, HOTLINKS_ITEM, PUBLISH_SUBITEM));
		OffMenu(backWindow, MENUITEM(EDIT_MENU, HOTLINKS_ITEM, UPDATE_SUBITEM));
		OffMenu(backWindow, MENUITEM(EDIT_MENU, HOTLINKS_ITEM, INFO_SUBITEM));
		OffMenu(backWindow, MENUITEM(EDIT_MENU, HOTLINKS_ITEM, BRKLINK_SUBITEM));
		OffMenu(backWindow, MENUITEM(EDIT_MENU, DUPLICATE_ITEM, NOSUB));
		OffMenu(backWindow, MENUITEM(EDIT_MENU, ROTATE_ITEM, NOSUB));
		OffMenu(backWindow, MENUITEM(EDIT_MENU, FLIP_ITEM, NOSUB));
		OffMenu(backWindow, MENUITEM(EDIT_MENU, SCALE_ITEM, NOSUB));
		OffMenu(backWindow, MENUITEM(EDIT_MENU, CONVERTTOPOLY_ITEM, NOSUB));
		OffMenu(backWindow, MENUITEM(EDIT_MENU, POLYGON_ITEM, NOSUB));
		OffMenu(backWindow, MENUITEM(EDIT_MENU, SELECTALL_ITEM, NOSUB));
		return;
	}
	OnMenu(backWindow, MENUITEM(EDIT_MENU, NOITEM, NOSUB));
	OnMenu(backWindow, MENUITEM(EDIT_MENU, SELECTALL_ITEM, NOSUB));
	docData = (DocDataPtr) GetWRefCon(window);
	firstSelObj = FirstSelected(docData);

	if (TextInEdit(window)) {
		textObj = (TextObjPtr) firstSelObj;
		on = (textObj->SelStart < textObj->SelEnd);
	}
	else
		on = (firstSelObj != NULL);
	OnOffMenu(backWindow, MENUITEM(EDIT_MENU, CUT_ITEM, NOSUB), on);
	OnOffMenu(backWindow, MENUITEM(EDIT_MENU, COPY_ITEM, NOSUB), on);
	OnOffMenu(backWindow, MENUITEM(EDIT_MENU, ERASE_ITEM, NOSUB), on);

	OnOffMenu(backWindow,MENUITEM(EDIT_MENU, HOTLINKS_ITEM, INFO_SUBITEM),
				(BOOL)(docData->PubBlock != NULL));
	OnOffMenu(backWindow,MENUITEM(EDIT_MENU, HOTLINKS_ITEM, BRKLINK_SUBITEM),
				(BOOL)(docData->PubBlock != NULL));

	OnOffMenu(backWindow, MENUITEM(EDIT_MENU, HOTLINKS_ITEM, NOSUB),
			(BOOL)(HotLinksBase != NULL));
	OnOffMenu(backWindow,MENUITEM(EDIT_MENU, HOTLINKS_ITEM, PUBLISH_SUBITEM),
					(docData->PubBlock == NULL));
	OnOffMenu(backWindow,MENUITEM(EDIT_MENU, HOTLINKS_ITEM, SUBSCRIBE_SUBITEM), TRUE);
	OnOffMenu(backWindow, MENUITEM(EDIT_MENU, HOTLINKS_ITEM, UPDATE_SUBITEM),
				(BOOL)(docData->PubBlock != NULL));
	OnOffMenu(backWindow,MENUITEM(EDIT_MENU, HOTLINKS_ITEM, INFO_SUBITEM),
				(BOOL)(docData->PubBlock != NULL));
	OnOffMenu(backWindow,MENUITEM(EDIT_MENU, HOTLINKS_ITEM, BRKLINK_SUBITEM),
				(BOOL)(docData->PubBlock != NULL));

	on = (firstSelObj != NULL);
	OnOffMenu(backWindow, MENUITEM(EDIT_MENU, DUPLICATE_ITEM, NOSUB), on);
	OnOffMenu(backWindow, MENUITEM(EDIT_MENU, ROTATE_ITEM, NOSUB), on);
	OnOffMenu(backWindow, MENUITEM(EDIT_MENU, FLIP_ITEM, NOSUB), on);
	OnOffMenu(backWindow, MENUITEM(EDIT_MENU, SCALE_ITEM, NOSUB), on);

	on = (pasteLayer.Objects != NULL);
	OnOffMenu(backWindow, MENUITEM(EDIT_MENU, PASTE_ITEM, NOSUB), TRUE);

	on = convert = close = open = smooth = unsmooth = unmerge = merge = polySel = FALSE;
	for (docObj = firstSelObj; docObj; docObj = NextSelected(docObj)) {
		switch (docObj->Type) {
		case TYPE_LINE:
		case TYPE_RECT:
		case TYPE_OVAL:
			convert = TRUE;
			break;
		case TYPE_POLY:
			on = TRUE;
			if (PolyClosed((PolyObjPtr) docObj))
				open = TRUE;
			else
				close = TRUE;
			if (PolySmoothed((PolyObjPtr) docObj))
				unsmooth = TRUE;
			else
				smooth = TRUE;
			if (PolyMerged((PolyObjPtr) docObj))
				unmerge = TRUE;
			if (polySel)
				merge = TRUE;
			else
				polySel = TRUE;
			break;
		}
	}
	OnOffMenu(backWindow, MENUITEM(EDIT_MENU, CONVERTTOPOLY_ITEM, NOSUB), convert);
	OnOffMenu(backWindow, MENUITEM(EDIT_MENU, POLYGON_ITEM, NOSUB), on);
	if (on) {
		OnOffMenu(backWindow, MENUITEM(EDIT_MENU, POLYGON_ITEM, CLOSEPOLY_SUBITEM), close);
		OnOffMenu(backWindow, MENUITEM(EDIT_MENU, POLYGON_ITEM, OPENPOLY_SUBITEM), open);
		OnOffMenu(backWindow, MENUITEM(EDIT_MENU, POLYGON_ITEM, SMOOTH_SUBITEM), smooth);
		OnOffMenu(backWindow, MENUITEM(EDIT_MENU, POLYGON_ITEM, UNSMOOTH_SUBITEM), unsmooth);
		OnOffMenu(backWindow, MENUITEM(EDIT_MENU, POLYGON_ITEM, MERGE_SUBITEM), merge);
		OnOffMenu(backWindow, MENUITEM(EDIT_MENU, POLYGON_ITEM, UNMERGE_SUBITEM), unmerge);
	}
}

/*
 *	Adjust Layout menu
 */

void SetLayoutMenu()
{
	BOOL on;
	Fixed scale;
	WindowPtr window;
	DocDataPtr docData;

	window = ActiveWindow();
	on = IsDocWindow(window);
	OnOffMenu(backWindow, MENUITEM(LAYOUT_MENU, NORMALSIZE_ITEM, NOSUB), on);
	OnOffMenu(backWindow, MENUITEM(LAYOUT_MENU, ENLARGE_ITEM, NOSUB), on);
	OnOffMenu(backWindow, MENUITEM(LAYOUT_MENU, REDUCE_ITEM, NOSUB), on);
	OnOffMenu(backWindow, MENUITEM(LAYOUT_MENU, FITTOWINDOW_ITEM, NOSUB), on);
	OnOffMenu(backWindow, MENUITEM(LAYOUT_MENU, GRIDSNAP_ITEM, NOSUB), on);
	OnOffMenu(backWindow, MENUITEM(LAYOUT_MENU, GRIDSIZE_ITEM, NOSUB), on);
	OnOffMenu(backWindow, MENUITEM(LAYOUT_MENU, LAYERS_ITEM, NOSUB), on);
	OnOffMenu(backWindow, MENUITEM(LAYOUT_MENU, DRAWINGSIZE_ITEM, NOSUB), on);
	if (!on)
		return;

	docData = (DocDataPtr) GetWRefCon(window);
	scale = docData->Scale;

	CheckMenu(backWindow, MENUITEM(LAYOUT_MENU, NORMALSIZE_ITEM, NOSUB),
			  (scale == SCALE_FULL));
	OnOffMenu(backWindow, MENUITEM(LAYOUT_MENU, ENLARGE_ITEM, NOSUB),
			  (scale < SCALE_MAX));
	OnOffMenu(backWindow, MENUITEM(LAYOUT_MENU, REDUCE_ITEM, NOSUB),
			  (scale > SCALE_MIN));

	CheckMenu(backWindow, MENUITEM(LAYOUT_MENU, GRIDSNAP_ITEM, NOSUB),
			  (docData->Flags & DOC_GRIDSNAP));
}

/*
 *	Set Arrange menu
 */

void SetArrangeMenu()
{
	BOOL multiSel, group, locked, unlocked;
	WindowPtr window;
	DocObjPtr docObj, firstSelObj;
	DocDataPtr docData;

	window = ActiveWindow();
	docData = (DocDataPtr) GetWRefCon(window);
	if (!IsDocWindow(window) || (firstSelObj = FirstSelected(docData)) == NULL) {
		OffMenu(backWindow, MENUITEM(ARRANGE_MENU, NOITEM, NOSUB));
		return;
	}
	OnMenu(backWindow, MENUITEM(ARRANGE_MENU, NOITEM, NOSUB));
	multiSel = group = locked = unlocked = FALSE;
	for (docObj = firstSelObj; docObj; docObj = NextSelected(docObj)) {
		if (docObj != firstSelObj)
			multiSel = TRUE;
		if (docObj->Type == TYPE_GROUP)
			group = TRUE;
		if (ObjectLocked(docObj))
			locked = TRUE;
		else
			unlocked = TRUE;
	}
	OnOffMenu(backWindow, MENUITEM(ARRANGE_MENU, ALIGNOBJECTS_ITEM, NOSUB), multiSel);
	OnOffMenu(backWindow, MENUITEM(ARRANGE_MENU, GROUP_ITEM, NOSUB), multiSel);
	OnOffMenu(backWindow, MENUITEM(ARRANGE_MENU, UNGROUP_ITEM, NOSUB), group);
	OnOffMenu(backWindow, MENUITEM(ARRANGE_MENU, LOCK_ITEM, NOSUB), unlocked);
	OnOffMenu(backWindow, MENUITEM(ARRANGE_MENU, UNLOCK_ITEM, NOSUB), locked);
}

/*
 *	Set Pen menu
 */

void SetPenMenu()
{
	WORD i;
	WindowPtr window;
	DocDataPtr docData;

	window = ActiveWindow();
	docData = (DocDataPtr) GetWRefCon(window);
	if (!IsDocWindow(window)) {
		OffMenu(backWindow, MENUITEM(PEN_MENU, NOITEM, NOSUB));
		return;
	}
	OnMenu(backWindow, MENUITEM(PEN_MENU, NOITEM, NOSUB));
	for (i = 0; i < NUM_PENSIZES; i++) {
		CheckMenu(backWindow, (WORD) MENUITEM(PEN_MENU, WIDTH_ITEM, i),
				  (defaults.PenWidth == penSizes[i]));
		CheckMenu(backWindow, (WORD) MENUITEM(PEN_MENU, HEIGHT_ITEM, i),
				  (defaults.PenHeight == penSizes[i]));
	}
	CheckMenu(backWindow, MENUITEM(PEN_MENU, NOARROWS_ITEM, NOSUB),
			  ((defaults.LineFlags & (LINE_ARROWSTART | LINE_ARROWEND)) == 0));
	CheckMenu(backWindow, MENUITEM(PEN_MENU, ARROWSTART_ITEM, NOSUB),
			  (defaults.LineFlags & LINE_ARROWSTART));
	CheckMenu(backWindow, MENUITEM(PEN_MENU, ARROWEND_ITEM, NOSUB),
			  (defaults.LineFlags & LINE_ARROWEND));
}

/*
 *	Set Text menu
 */

void SetTextMenu()
{
	WORD i, menuFontNum;
	WindowPtr window;
	DocDataPtr docData;

	window = ActiveWindow();
	docData = (DocDataPtr) GetWRefCon(window);
	if (!IsDocWindow(window)) {
		OffMenu(backWindow, MENUITEM(TEXT_MENU, NOITEM, NOSUB));
		return;
	}
	OnMenu(backWindow, MENUITEM(TEXT_MENU, NOITEM, NOSUB));
	menuFontNum = MenuFontNum(defaults.FontNum, defaults.FontSize);
	for (i = 0; i < NumMenuFonts(); i++)
		CheckMenu(backWindow, MENUITEM(TEXT_MENU, FONT_ITEM, FONT_SUBITEM + i),
				  (i == menuFontNum));
	CheckMenu(backWindow, MENUITEM(TEXT_MENU, PLAIN_ITEM, NOSUB),
			  (defaults.Style == FS_NORMAL && defaults.MiscStyle == 0));
	CheckMenu(backWindow, MENUITEM(TEXT_MENU, BOLD_ITEM, NOSUB),
			  (defaults.Style & FSF_BOLD));
	CheckMenu(backWindow, MENUITEM(TEXT_MENU, ITALIC_ITEM, NOSUB),
			  (defaults.Style & FSF_ITALIC));
	CheckMenu(backWindow, MENUITEM(TEXT_MENU, UNDERLINE_ITEM, NOSUB),
			  (defaults.Style & FSF_UNDERLINED));
	CheckMenu(backWindow, MENUITEM(TEXT_MENU, CONDENSED_ITEM, NOSUB),
			  (defaults.MiscStyle & MISCSTYLE_CONDENSED));
	CheckMenu(backWindow, MENUITEM(TEXT_MENU, EXPANDED_ITEM, NOSUB),
			  (defaults.MiscStyle & MISCSTYLE_EXPANDED));
	CheckMenu(backWindow, MENUITEM(TEXT_MENU, LEFTALIGN_ITEM, NOSUB),
			  (defaults.Justify == JUSTIFY_LEFT));
	CheckMenu(backWindow, MENUITEM(TEXT_MENU, CENTERALIGN_ITEM, NOSUB),
			  (defaults.Justify == JUSTIFY_CENTER));
	CheckMenu(backWindow, MENUITEM(TEXT_MENU, RIGHTALIGN_ITEM, NOSUB),
			  (defaults.Justify == JUSTIFY_RIGHT));
	CheckMenu(backWindow, MENUITEM(TEXT_MENU, SINGLESPACE_ITEM, NOSUB),
			  (defaults.Spacing == SPACE_SINGLE));
	CheckMenu(backWindow, MENUITEM(TEXT_MENU, ONEANDHALFSPACE_ITEM, NOSUB),
			  (defaults.Spacing == SPACE_1_HALF));
	CheckMenu(backWindow, MENUITEM(TEXT_MENU, DOUBLESPACE_ITEM, NOSUB),
			  (defaults.Spacing == SPACE_DOUBLE));
}

/*
 *	Adjust View menu
 */

void SetViewMenu()
{
	register WORD i;
	BOOL on;
	WindowPtr window;
	register DocDataPtr docData;

	window = ActiveWindow();
	docData = (DocDataPtr) GetWRefCon(window);

	on = IsDocWindow(window);
	OnOffMenu(backWindow, MENUITEM(VIEW_MENU, SHOWBACKLAYERS_ITEM, NOSUB), on);
	OnOffMenu(backWindow, MENUITEM(VIEW_MENU, SHOWGRID_ITEM, NOSUB), on);
	OnOffMenu(backWindow, MENUITEM(VIEW_MENU, SHOWPAGE_ITEM, NOSUB), on);
	OnOffMenu(backWindow, MENUITEM(VIEW_MENU, SHOWRULER_ITEM, NOSUB), on);
	CheckMenu(backWindow, MENUITEM(VIEW_MENU, SHOWTOOLBOX_ITEM, NOSUB),
			  (toolWindow != NULL));
	CheckMenu(backWindow, MENUITEM(VIEW_MENU, SHOWPENPALETTE_ITEM, NOSUB),
			  (penWindow != NULL));
	CheckMenu(backWindow, MENUITEM(VIEW_MENU, SHOWFILLPALETTE_ITEM, NOSUB),
			  (fillWindow != NULL));
	if (on) {
		CheckMenu(backWindow, MENUITEM(VIEW_MENU, SHOWBACKLAYERS_ITEM, NOSUB),
				  ((docData->Flags & DOC_HIDEBACKLAY) == 0));
		CheckMenu(backWindow, MENUITEM(VIEW_MENU, SHOWRULER_ITEM, NOSUB),
				  (docData->Flags & DOC_SHOWRULER));
		CheckMenu(backWindow, MENUITEM(VIEW_MENU, SHOWGRID_ITEM, NOSUB),
				  (docData->Flags & DOC_SHOWGRID));
		CheckMenu(backWindow, MENUITEM(VIEW_MENU, SHOWPAGE_ITEM, NOSUB),
				  (docData->Flags & DOC_SHOWPAGE));
	}
	OnOffMenu(backWindow, MENUITEM(VIEW_MENU, DISPLAY_ITEM, A1600GX_SUBITEM),
			  Ameristar1600GX_Init());
	OnOffMenu(backWindow, MENUITEM(VIEW_MENU, DISPLAY_ITEM, GDA1_SUBITEM),
			  GfxBaseGDA1_Init());
	for (i = 0; i < numWindows; i++)
		CheckMenu(backWindow, (UWORD) MENUITEM(VIEW_MENU, WINDOW_ITEM + i, NOSUB),
				  (window == windowList[i]));
}

/*
 *	Adjust Macro menu
 *	Only needs to be called once at program start-up
 */

void SetMacroMenu()
{
	OnOffMenu(backWindow, MENUITEM(MACRO_MENU, NOITEM, NOSUB), (BOOL) RexxSysBase);
}

/*
 *	Set up all menus except Macro menu
 */

void SetAllMenus()
{
	SetProjectMenu();
	SetEditMenu();
	SetLayoutMenu();
	SetArrangeMenu();
	SetPenMenu();
	SetTextMenu();
	SetViewMenu();
}
