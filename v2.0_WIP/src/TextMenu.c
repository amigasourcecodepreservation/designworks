/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Text menu routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>

#include <string.h>

#include <Toolbox/Globals.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Font.h>
#include <Toolbox/Border.h>
#include <Toolbox/Menu.h>
#include <Toolbox/ScrollList.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Window.h>
#include <Toolbox/SelectFont.h>
#include <Toolbox/Utility.h>

#include "Draw.h"
#include "Proto.h"

/*
 *	External variables
 */

extern struct Library	*DiskfontBase;

extern MsgPortPtr	mainMsgPort;
extern ScreenPtr	screen;
extern WindowPtr	backWindow;

extern Defaults	defaults;

/*
 *	Local variables and definitions
 */

static WORD		numMenuFonts;
static FontNum	menuFontNums[MAX_MENU_FONTS];
static FontSize	menuFontSizes[MAX_MENU_FONTS];

/*
 *	Local prototypes
 */

static BOOL	DoFontRequest(FontNum *, FontSize *);

static BOOL	DoFontMenu(WindowPtr, UWORD);
static BOOL DoFontSizeMenu(WindowPtr, UWORD);

static BOOL	DoTextParams(WindowPtr, FontNum, FontSize, WORD, WORD, WORD, WORD);

/*
 *	Return number of menu fonts
 */

WORD NumMenuFonts()
{
	return (numMenuFonts);
}

/*
 *	Return item number in font submenu, or -1 if not a menu font
 */

WORD MenuFontNum(FontNum fontNum, FontSize fontSize)
{
	register WORD i;

	for (i = 0; i < numMenuFonts; i++) {
		if (fontNum == menuFontNums[i] && fontSize == menuFontSizes[i])
			return (i);
	}
	return (-1);
}

/*
 *	Get menu font info for given entry
 */

void GetMenuFontInfo(WORD num, FontNum *fontNum, FontSize *fontSize)
{
	if (num >= numMenuFonts)
		return;
	*fontNum = menuFontNums[num];
	*fontSize = menuFontSizes[num];
}

/*
 *	Determine if this is a menu font
 */

BOOL IsMenuFont(FontNum fontNum, FontSize fontSize)
{
	return (MenuFontNum(fontNum, fontSize) != -1);
}

/*
 *	Add or remove font from menu font list
 */

void SetMenuFont(FontNum fontNum, FontSize fontSize, BOOL add)
{
	WORD i, entry, len1, len2, cmp;
	FontSize size1, size2;
	MenuItemPtr menuItem;
	TextChar name1[50], name2[50];

	if (fontNum == -1 || fontSize <= 0 || fontSize > FONT_MAXSIZE)
		return;
/*
	Add new menu item, sorted into list
*/
	if (add) {
		if (MenuFontNum(fontNum, fontSize) != -1 || numMenuFonts >= MAX_MENU_FONTS)
			return;
		GetFontName(fontNum, name1);
		size1 = fontSize;
		len1 = strlen(name1);
		for (i = 0; i < numMenuFonts; i++) {
			GetFontName(menuFontNums[i], name2);
			size2 = menuFontSizes[i];
			len2 = strlen(name2);
			cmp = CmpString(name1, name2, len1, len2, FALSE);
			if (cmp < 0 || (cmp == 0 && size1 < size2))
				break;
		}
		entry = i;
		for (i = numMenuFonts; i > entry; i--) {
			menuFontNums[i]  = menuFontNums[i - 1];
			menuFontSizes[i] = menuFontSizes[i - 1];
		}
		menuFontNums[entry] = fontNum;
		menuFontSizes[entry] = fontSize;
		name1[0] = toUpper[name1[0]];
		name1[len1] = '-';
		NumToString((LONG) size1, name1 + len1 + 1);
		InsertMenuItem(backWindow, MENUITEM(TEXT_MENU, FONT_ITEM, entry + FONT_SUBITEM),
					   name1);
		numMenuFonts++;
	}
/*
	Remove menu item
*/
	else {
		if ((entry = MenuFontNum(fontNum, fontSize)) == -1 || numMenuFonts <= 0)
			return;
		for (i = entry; i < numMenuFonts - 1; i++) {
			menuFontNums[i]  = menuFontNums[i + 1];
			menuFontSizes[i] = menuFontSizes[i + 1];
		}
		DeleteMenuItem(backWindow, MENUITEM(TEXT_MENU, FONT_ITEM, entry + FONT_SUBITEM));
		numMenuFonts--;
	}
/*
	Adjust mutual exclude (and set checkable flag)
*/
	for (i = 0; i < numMenuFonts; i++) {
		menuItem = ItemAddress(backWindow->MenuStrip, MENUITEM(TEXT_MENU, FONT_ITEM, i + FONT_SUBITEM));
		menuItem->Flags |= CHECKIT;
		menuItem->MutualExclude = ~(1 << i) << FONT_SUBITEM;
	}
}

/*
 *	Handle font submenu
 */

static BOOL DoFontMenu(WindowPtr window, UWORD sub)
{
	FontNum		fontNum;
	FontSize	fontSize;
	BOOL		success;

	if (sub != OTHERFONT_SUBITEM &&
		(sub < FONT_SUBITEM || sub >= FONT_SUBITEM + numMenuFonts))
		return (FALSE);
/*
	Get new font from user, and make sure font is loadable
*/
	if (sub == OTHERFONT_SUBITEM) {
		fontNum = defaults.FontNum;
		fontSize = defaults.FontSize;
		BeginWait();
		success = SelectFont(screen, mainMsgPort, DialogFilter, IsMenuFont, SetMenuFont,
							 &fontNum, &fontSize);
		EndWait();
		if (!success)
			return (FALSE);
	}
	else {
		fontNum = menuFontNums[sub - FONT_SUBITEM];
		fontSize = menuFontSizes[sub - FONT_SUBITEM];
	}
	if (fontNum < 0 || fontNum > NumFonts() || fontSize <= 0 || fontSize > FONT_MAXSIZE) {
		Error(ERR_NO_FONT);		/* Shouldn't happen */
		return (FALSE);
	}
/*
	Change font
*/
	success = DoTextParams(window, fontNum, fontSize, -1, -1, -1, -1);
	return (success);
}

/*
 *	Handle font size submenu
 */

static BOOL DoFontSizeMenu(WindowPtr window, UWORD sub)
{
	FontNum		fontNum;
	FontSize	fontSize;
	BOOL		success;

	if (sub != OTHERSIZE_SUBITEM &&
		(sub < SIZE_SUBITEM || sub >= SIZE_SUBITEM + numMenuFonts))
		return (FALSE);
/*
	Get new font from user, and make sure font is loadable
*/
	if (sub == OTHERSIZE_SUBITEM) {
		fontNum = defaults.FontNum;
		fontSize = defaults.FontSize;
		BeginWait();
		success = SelectFont(screen, mainMsgPort, DialogFilter, IsMenuFont, SetMenuFont,
							 &fontNum, &fontSize);
		EndWait();
		if (!success)
			return (FALSE);
	}
	else {
		fontNum = menuFontNums[sub - SIZE_SUBITEM];
		fontSize = menuFontSizes[sub - SIZE_SUBITEM];
	}
	if (fontNum < 0 || fontNum > NumFonts() || fontSize <= 0 || fontSize > FONT_MAXSIZE) {
		Error(ERR_NO_FONT);		/* Shouldn't happen */
		return (FALSE);
	}
/*
	Change font
*/
	success = DoTextParams(window, fontNum, fontSize, -1, -1, -1, -1);
	return (success);
}

/*
 *	Set defaults for text menu from current selection
 */

void SetTextMenuDefaults(WindowPtr window)
{
	DocObjPtr docObj;
	TextObjPtr textObj;
	DocDataPtr docData = GetWRefCon(window);

	for (docObj = FirstSelected(docData); docObj; docObj = NextSelected(docObj)) {
		if (docObj->Type == TYPE_TEXT) {
			textObj = (TextObjPtr) docObj;
			defaults.FontNum	= textObj->FontNum;
			defaults.FontSize	= textObj->FontSize;
			defaults.Style		= textObj->Style;
			defaults.MiscStyle	= textObj->MiscStyle;
			defaults.Justify	= textObj->Justify;
			defaults.Spacing	= textObj->Spacing;
			break;
		}
	}
	SetTextMenu();
}

/*
 *	Set text parameters
 *	If parameter is -1 then do not change,
 */

static BOOL DoTextParams(WindowPtr window, FontNum fontNum, FontSize fontSize,
						 WORD style, WORD miscStyle, WORD justify, WORD spacing)
{
	BOOL changed;
	DocObjPtr docObj;
	DocDataPtr docData = GetWRefCon(window);

/*
	Set defaults for new objects
*/
	if (fontNum != -1)
		defaults.FontNum = fontNum;
	if (fontSize != -1)
		defaults.FontSize = fontSize;
	if (style != -1)
		defaults.Style = TextNewStyle(defaults.Style, style);
	if (miscStyle != -1)
		defaults.MiscStyle = TextNewMiscStyle(defaults.MiscStyle, miscStyle);
	if (justify != -1)
		defaults.Justify = justify;
	if (spacing != -1)
		defaults.Spacing = spacing;
	SetTextMenu();
/*
	Modify selected objects
*/
	if (ObjectsLocked(docData)) {
		Error(ERR_OBJ_LOCKED);
		return (FALSE);
	}
	HiliteSelectOff(window);
	for (docObj = FirstSelected(docData); docObj; docObj = NextSelected(docObj)) {
		if (docObj->Type == TYPE_TEXT || docObj->Type == TYPE_GROUP) {
			InvalObjectRect(window, docObj);	/* Obj rect size may change */
			SetObjectTextParams(docObj, fontNum, fontSize, style, miscStyle, justify,
								spacing);
			InvalObjectRect(window, docObj);
			changed = TRUE;
		}
	}
	HiliteSelectOn(window);
	if (changed) {
		SetTextMenuDefaults(window);
		DocModified(docData);
	}
	return (TRUE);
}

/*
 *	Handle Text menu
 */

BOOL DoTextMenu(WindowPtr window, UWORD item, UWORD sub, UWORD modifier)
{
	BOOL success;

	if (!IsDocWindow(window))
		return (FALSE);
	UndoOff();
	success = FALSE;
	switch (item) {
	case FONT_ITEM:
		success = DoFontMenu(window, sub);
		break;
	case FONTSIZE_ITEM:
		success = DoFontSizeMenu(window, sub);
		break;
	case PLAIN_ITEM:
		success = DoTextParams(window, -1, -1, FS_NORMAL, 0, -1, -1);
		break;
	case BOLD_ITEM:
		success = DoTextParams(window, -1, -1, FSF_BOLD, -1, -1, -1);
		break;
	case ITALIC_ITEM:
		success = DoTextParams(window, -1, -1, FSF_ITALIC, -1, -1, -1);
		break;
	case UNDERLINE_ITEM:
		success = DoTextParams(window, -1, -1, FSF_UNDERLINED, -1, -1, -1);
		break;
	case CONDENSED_ITEM:
		success = DoTextParams(window, -1, -1, -1, MISCSTYLE_CONDENSED, -1, -1);
		break;
	case EXPANDED_ITEM:
		success = DoTextParams(window, -1, -1, -1, MISCSTYLE_EXPANDED, -1, -1);
		break;
	case LEFTALIGN_ITEM:
		success = DoTextParams(window, -1, -1, -1, -1, JUSTIFY_LEFT, -1);
		break;
	case CENTERALIGN_ITEM:
		success = DoTextParams(window, -1, -1, -1, -1, JUSTIFY_CENTER, -1);
		break;
	case RIGHTALIGN_ITEM:
		success = DoTextParams(window, -1, -1, -1, -1, JUSTIFY_RIGHT, -1);
		break;
	case SINGLESPACE_ITEM:
		success = DoTextParams(window, -1, -1, -1, -1, -1, SPACE_SINGLE);
		break;
	case ONEANDHALFSPACE_ITEM:
		success = DoTextParams(window, -1, -1, -1, -1, -1, SPACE_1_HALF);
		break;
	case DOUBLESPACE_ITEM:
		success = DoTextParams(window, -1, -1, -1, -1, -1, SPACE_DOUBLE);
		break;
	}
	return (success);
}
