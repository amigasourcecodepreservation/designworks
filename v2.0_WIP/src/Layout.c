/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Layout menu routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>
#include <proto/dos.h>				/* For Delay() prototype */

#include <string.h>

#include <Toolbox/Globals.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Border.h>
#include <Toolbox/ScrollList.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Window.h>
#include <Toolbox/Utility.h>
#include <Toolbox/ColorPick.h>
#include <Toolbox/LocalData.h>

#include "Draw.h"
#include "Proto.h"

/*
 *	External variables
 */

extern MsgPortPtr	mainMsgPort;
extern ScreenPtr	screen;

extern Defaults		defaults;
extern Options		options;
extern PenColorPtr 	penColors;

extern UBYTE	gridSixteenthUnits[], gridMMUnits[];

extern Palette	screenPalette;

extern TextChar	strInch[], strCM[], strMM[], strPenColor[], strLayer[];
extern TextChar	strBuff[];

extern DlgTemplPtr	dlgList[];

extern UBYTE	_tbPenBlack;

/*
 *	Local variables and definitions
 */

enum {
	GRIDUP_ARROW = 2,
	GRIDDOWN_ARROW,
	GRID_TEXT
};

enum {
	DONE_BUTTON,
	NEW_BUTTON,
	EDIT_BUTTON,
	REMOVE_BUTTON,
	COLOR_BOX,
	COLOR_UP,
	COLOR_DOWN,
	COLOR_SCROLL
};

enum {
	CHANGE_BUTTON	= 2,
	PENNAME_TEXT,
	COLOR_RECT
};

enum {
	FILLNAME_TEXT = 2,
	EDITPAT_USERITEM,
	SAMPLEPAT_USERITEM,
	CLEAR_BTN,
	REVERT_BTN,
	COLOR_POPUP
};

enum {
	NEW_BTN = 0,
	MOVEUP_ARROW,
	MOVEDOWN_ARROW,
	RENAME_BTN,
	DELETE_BTN,
	SHOW_BTN,
	HIDE_BTN,
	DONE_BTN,
	LAYERS_LIST,
	LAYERS_UP,
	LAYERS_DOWN,
	LAYERS_SCROLL,
	NAME_TEXT
};

enum {
	DRAWSIZE_USERITEM = 2,
	WIDTH_TEXT,
	HEIGHT_TEXT,
	PAGES_TEXT
};

static DialogPtr	gridSizeDlg, penColorsDlg, editColorDlg, fillPatsDlg, editFillDlg,
					layersDlg, drawSizeDlg;

static ScrollListPtr	layersScrollList;

static ScrollListPtr	scrollList;

/*
 *	Local prototypes
 */

static Fixed	MinScale(WindowPtr);

static void	SetDocScale(WindowPtr, Fixed);

static BOOL	DoNormalSize(WindowPtr);
static BOOL	DoEnlarge(WindowPtr);
static BOOL	DoReduce(WindowPtr);
static BOOL	DoFitToWindow(WindowPtr);

static BOOL	DoGridSnap(WindowPtr);
static void	ShowGridSize(DialogPtr, WORD);
static BOOL	GridSizeDialogFilter(IntuiMsgPtr, WORD *);
static BOOL	DoGridSize(WindowPtr);

static void SetColorButtons(DialogPtr, WORD);
static void FillColorSample(RGBColor);
static BOOL DoEditColor(TextPtr, RGBColorPtr);
static BOOL	ColorsDialogFilter(IntuiMsgPtr, WORD *);
static BOOL	DoPenColors(void);

static void	ShowEditPat(DialogPtr, RGBPat8Ptr);
static void	DrawEditPixel(DialogPtr, RGBPat8Ptr, WORD, WORD);
static BOOL BuildColorList(TextPtr []);
static RGBColor	GetSelColor(void);
static BOOL	EditPatsDialogFilter(IntuiMsgPtr, WORD *);
static BOOL DoEditPattern(TextPtr, RGBPat8Ptr);
static BOOL	DoFillPatterns(void);

static void	ShowLayerNames(DocDataPtr, DialogPtr, ScrollListPtr, DocLayerPtr);
static void	SetLayerButtons(DialogPtr, DocLayerPtr);
static BOOL	LayersDialogFilter(IntuiMsgPtr, WORD *);

static void	MinDrawSize(DocDataPtr, WORD *, WORD *);
static void	ShowDrawSize(DialogPtr, DocDataPtr, WORD, WORD);
static BOOL	DrawSizeDialogFilter(IntuiMsgPtr, WORD *);
static BOOL	DoDrawingSize(WindowPtr);

/*
 *	Determine minimum scale value for given document size
 *	Minimum value is value that will have entire document shown in window
 *	Will not return a value greater than SCALE_FULL
 */

static Fixed MinScale(WindowPtr window)
{
	Fixed		scale, xScale, yScale;
	WORD		width, height;
	DocDataPtr	docData = GetWRefCon(window);
	DocData		tempDocData;
	Rectangle	rect;

	GetContentRect(window, &rect);
	tempDocData = *docData;
	tempDocData.Scale = SCALE_FULL;
	SetWRefCon(window, &tempDocData);
	ScaleDocToWin(window, docData->DocWidth, docData->DocHeight, &width, &height);
	SetWRefCon(window, docData);
	xScale = FixRatio(rect.MaxX - rect.MinX, width);
	yScale = FixRatio(rect.MaxY - rect.MinY, height);
	for (scale = SCALE_FULL; scale > SCALE_MIN; scale >>= 1) {
		if (scale <= xScale && scale <= yScale)
			break;
	}
	return (scale);
}

/*
 *	Set document scale to given value
 */

static void SetDocScale(WindowPtr window, Fixed scale)
{
	WORD leftOffset, topOffset;
	DocDataPtr docData = GetWRefCon(window);
	Point center;
	Rectangle rect;

/*
	Find coordinates of center of selected objects, or of window
*/
	if (!GetSelectRect(docData, &rect)) {
		if (docData->Scale <= MinScale(window))
			SetRect(&rect, 0, 0, docData->DocWidth - 1, docData->DocHeight - 1);
		else {
			GetContentRect(window, &rect);
			WindowToDocRect(window, &rect, &rect);
		}
	}
	center.x = (rect.MaxX + rect.MinX + 1)/2;
	center.y = (rect.MaxY + rect.MinY + 1)/2;
/*
	Adjust scale
*/
	if (scale > SCALE_MAX)
		scale = SCALE_MAX;
	else if (scale < SCALE_MIN)
		scale = SCALE_MIN;
	if (scale == docData->Scale)
		return;
	docData->Scale = scale;
/*
	Set scroll offset that will keep window center in center
*/
	GetContentRect(window, &rect);
	WindowToDocRect(window, &rect, &rect);
	leftOffset = center.x - (rect.MaxX - rect.MinX + 1)/2;
	topOffset  = center.y - (rect.MaxY - rect.MinY + 1)/2;
	SetScrollOffset(window, leftOffset, topOffset);
	GetWindowRect(window, &rect);
	InvalRect(window, &rect);
	SetLayoutMenu();
}

/*
 *	Enlarge to normal size
 */

static BOOL DoNormalSize(WindowPtr window)
{
	SetDocScale(window, SCALE_FULL);
	return (TRUE);
}

/*
 *	Enlarge display
 */

static BOOL DoEnlarge(WindowPtr window)
{
	DocDataPtr docData = GetWRefCon(window);

	SetDocScale(window, docData->Scale << 1);
	return (TRUE);
}

/*
 *	Reduce display
 */

static BOOL DoReduce(WindowPtr window)
{
	DocDataPtr docData = GetWRefCon(window);

	SetDocScale(window, docData->Scale >> 1);
	return (TRUE);
}

/*
 *	Reduce to fit in window
 */

static BOOL DoFitToWindow(WindowPtr window)
{
	SetDocScale(window, MinScale(window));
	return (TRUE);
}

/*
 *	Turn grid snap on/off
 */

static BOOL DoGridSnap(WindowPtr window)
{
	DocDataPtr docData = GetWRefCon(window);

	if (docData->Flags & DOC_GRIDSNAP)
		docData->Flags &= ~DOC_GRIDSNAP;
	else
		docData->Flags |= DOC_GRIDSNAP;
	SetLayoutMenu();
	return (TRUE);
}

/*
 *	Show grid size in dialog, and enable/disable up & down arrows
 */

static void ShowGridSize(DialogPtr dlg, WORD index)
{
	WORD numer, denom;
	GadgetPtr gadget, gadgList;
	TextChar denomText[5];

	if (options.MeasureUnit == MEASURE_CM) {
		NumToString(gridMMUnits[index], strBuff);
		strcat(strBuff, strMM);
	}
	else {
		numer = gridSixteenthUnits[index];
		denom = 16;
		while ((numer & 1) == 0 && (denom & 1) == 0) {
			numer >>= 1;
			denom >>= 1;
		}
		NumToString(numer, strBuff);
		if (denom > 1) {
			strcat(strBuff, "/");
			NumToString(denom, denomText);
			strcat(strBuff, denomText);
		}
		strcat(strBuff, strInch);
	}
	gadgList = dlg->FirstGadget;
	SetGadgetItemText(gadgList, GRID_TEXT, dlg, NULL, strBuff);
		gadget = GadgetItem(gadgList, GRIDDOWN_ARROW);
	if (index <= 0)
		OffGList(gadget, dlg, NULL, 1);
	else
		OnGList(gadget, dlg, NULL, 1);
		gadget = GadgetItem(gadgList, GRIDUP_ARROW);
	if (index >= NUM_GRIDINDEX - 1)
		OffGList(gadget, dlg, NULL, 1);
	else
		OnGList(gadget, dlg, NULL, 1);
}

/*
 *	Grid size dialog filter
 */

static BOOL GridSizeDialogFilter(register IntuiMsgPtr intuiMsg, WORD *item)
{
	register WORD itemHit;
	register ULONG class;

	class = intuiMsg->Class;
	if (intuiMsg->IDCMPWindow == gridSizeDlg && (class == GADGETDOWN || class == GADGETUP)) {
		itemHit = GadgetNumber((GadgetPtr) intuiMsg->IAddress);
		if (itemHit == GRIDUP_ARROW || itemHit == GRIDDOWN_ARROW) {
			ReplyMsg((MsgPtr) intuiMsg);
			*item = (class == GADGETDOWN) ? itemHit : -1;
			return (TRUE);
		}
	}
	return (DialogFilter(intuiMsg, item));
}

/*
 *	Change grid size
 */

static BOOL DoGridSize(WindowPtr window)
{
	WORD item, index;
	BOOL done;
	DocDataPtr docData = GetWRefCon(window);

	index = docData->GridUnitsIndex;
	if (index < 0)
		index = 0;
	else if (index >= NUM_GRIDINDEX)
		index = NUM_GRIDINDEX - 1;
/*
	Bring up dialog
*/
	BeginWait();
	if ((gridSizeDlg = GetDialog(dlgList[DLG_GRIDSIZE], screen, mainMsgPort)) == NULL) {
		EndWait();
		Error(ERR_NO_MEM);
		return (FALSE);
	}
	OutlineOKButton(gridSizeDlg);
	ShowGridSize(gridSizeDlg, index);
/*
	Handle dialog
*/
	done = FALSE;
	do {
		item = ModalDialog(mainMsgPort, gridSizeDlg, GridSizeDialogFilter);
		switch (item) {
		case OK_BUTTON:
		case CANCEL_BUTTON:
			done = TRUE;
			break;
		case GRIDUP_ARROW:
			if (index < NUM_GRIDINDEX - 1)
				index++;
			ShowGridSize(gridSizeDlg, index);
			break;
		case GRIDDOWN_ARROW:
			if (index > 0)
				index--;
			ShowGridSize(gridSizeDlg, index);
			break;
		}
	} while (!done);
	DisposeDialog(gridSizeDlg);
	EndWait();
	if (item == OK_BUTTON)
		docData->GridUnitsIndex = index;
	return (item == OK_BUTTON);
}

/*
 *	Set buttons for pen or fill pattern dialog
 */

static void SetColorButtons(DialogPtr dlg, WORD maxNum)
{
	WORD	numItems = SLNumItems(scrollList);

	EnableGadgetItem(dlg->FirstGadget, NEW_BUTTON, dlg, NULL, numItems < maxNum);
	EnableGadgetItem(dlg->FirstGadget, REMOVE_BUTTON, dlg, NULL, numItems);
	EnableGadgetItem(dlg->FirstGadget, EDIT_BUTTON, dlg,
						NULL, (SLNextSelect(scrollList, -1) != -1));
}

/*
 *	fill color box with color
 */

static void FillColorSample(RGBColor color)
{
	Rectangle 	rect;
	RastPtr		rPort = editColorDlg->RPort;

	GetGadgetRect(GadgetItem(editColorDlg->FirstGadget, COLOR_RECT), editColorDlg, NULL, &rect);
	DrawShadowBox(rPort, &rect, 0, FALSE);
	InsetRect(&rect, 1, 1);
	SetAPen(rPort, _tbPenDark);
	FrameRect(rPort, &rect);
	InsetRect(&rect, 1, 1);
	RGBForeColor(rPort, color);
	FillRect(rPort, &rect);
}

/*
 *	open edit color dialog
 */

static BOOL DoEditColor(TextPtr name, RGBColorPtr oldColor)
{
	BOOL		done;
	WORD		item;
	RGBColor	color;
	GadgetPtr	gadgList;

	color = *oldColor;

	BeginWait();
	if ((editColorDlg = GetDialog(dlgList[DLG_EDITPEN], screen, mainMsgPort)) == NULL) {
		EndWait();
		Error(ERR_NO_MEM);
		return (FALSE);
	}
	gadgList = editColorDlg->FirstGadget;
	SetPalette(editColorDlg->RPort, &screenPalette);
	OutlineOKButton(editColorDlg);
	Delay(5);				/* Wait for gadget to be activated completely */
	if (name[0] != '\0')
		SetEditItemText(gadgList, PENNAME_TEXT, editColorDlg, NULL, name);
	FillColorSample(color);

	done = FALSE;
	while (!done) {
		WaitPort(mainMsgPort);
		item = CheckDialog(mainMsgPort, editColorDlg, DialogFilter);
		switch (item) {
			case -1:
				GetEditItemText(gadgList, PENNAME_TEXT, strBuff);
				EnableGadgetItem(gadgList,OK_BUTTON, editColorDlg, NULL,
							   (strBuff[0] != '\0'));
				break;
			case OK_BUTTON:
			case CANCEL_BUTTON:
				done = TRUE;
				break;
			case CHANGE_BUTTON:
				if (GetColor(screen, mainMsgPort, DialogFilter, strPenColor, ColorCorrectHook,
							 color, &color, -1)) {
					FillColorSample(color);
				}
				break;
		}
	}
	GetEditItemText(gadgList, PENNAME_TEXT, strBuff);
	DisposeDialog(editColorDlg);
	EndWait();

	if (item == CANCEL_BUTTON)
		return (FALSE);

	*oldColor = color;
	strcpy(name, strBuff);

	return (TRUE);
}

/*
 *	Pen Color dialog filter
 */

static BOOL ColorsDialogFilter(register IntuiMsgPtr intuiMsg, WORD *item)
{
	WORD			gadgNum;
	ULONG			class = intuiMsg->Class;
	UWORD			code = intuiMsg->Code;

	if (intuiMsg->IDCMPWindow == penColorsDlg || intuiMsg->IDCMPWindow == fillPatsDlg) {
		switch (class) {
/*
	Handle gadget down of up/down scroll arrows
*/
		case GADGETDOWN:
		case GADGETUP:
			gadgNum = GadgetNumber((GadgetPtr) intuiMsg->IAddress);
			if (gadgNum == COLOR_BOX || gadgNum == COLOR_SCROLL ||
				gadgNum == COLOR_UP || gadgNum == COLOR_DOWN) {
				SLGadgetMessage(scrollList, mainMsgPort, intuiMsg);
				*item = gadgNum;
				return (TRUE);
			}
			break;
/*
	Handle keyboard events
*/
		case RAWKEY:
			if (code == CURSORUP || code == CURSORDOWN) {
				ReplyMsg((MsgPtr) intuiMsg);
				SLCursorKey(scrollList, code);
				return (TRUE);
			}
			break;
		}
	}
	return (DialogFilter(intuiMsg, item));
}

/*
 *	Adjust pen colors
 */

static BOOL DoPenColors()
{
	WORD 			len, item, selColor, i;
	BOOL 			done;
	GadgetPtr 		gadgList;
	RastPtr 		rPort;
	PenColorPtr		newPen;
	TextChar		name[150];
	RGBColor		color;
	TextPtr			newName;
/*
	Bring up dialog
*/
	if ((scrollList = NewScrollList(SL_SINGLESELECT)) == NULL) {
		Error(ERR_NO_MEM);
		return (FALSE);
	}

	BeginWait();
	if ((penColorsDlg = GetDialog(dlgList[DLG_PENCOLORS], screen, mainMsgPort)) == NULL) {
		EndWait();
		DisposeScrollList(scrollList);
		Error(ERR_NO_MEM);
		return (FALSE);
	}
	OutlineOKButton(penColorsDlg);
	SetColorButtons(penColorsDlg, MAX_NUM_PENS);
	rPort = penColorsDlg->RPort;
	gadgList = penColorsDlg->FirstGadget;
	SetPalette(rPort, &screenPalette);
/*
	build scrollList
*/
	InitScrollList(scrollList, GadgetItem(gadgList, COLOR_BOX), penColorsDlg, NULL);
	SLDrawBorder(scrollList);
	SLSetDrawProc(scrollList, PenDrawProc);
	SLDoDraw(scrollList, FALSE);
	i = 0;
	for (newPen = FirstPenColor(); newPen; newPen = newPen->Next) {
		len = GetPenScrollListItem(strBuff, newPen->Name, i + 3);
		SLAddItem(scrollList, strBuff, len, SLNumItems(scrollList));
		i++;
	}
	SLDoDraw(scrollList, TRUE);
/*
	Handle dialog
*/
	done = FALSE;
	do {
		item = ModalDialog(mainMsgPort, penColorsDlg, ColorsDialogFilter);
		switch (item) {
		case DONE_BUTTON:
			done = TRUE;
			break;
		case NEW_BUTTON:
			name[0] = '\0';
			color = RGBCOLOR_WHITE;
			if (DoEditColor(name, &color)) {
				AddPenColor(name, color);
				len = GetPenScrollListItem(strBuff, name, SLNumItems(scrollList) + 3);
				SLAddItem(scrollList, strBuff, len, SLNumItems(scrollList));
				SLDrawList(scrollList);
			}
			break;
		case COLOR_BOX:
			if (!SLIsDoubleClick(scrollList))
				break;				// fall through
		case EDIT_BUTTON:
			if ((selColor = SLNextSelect(scrollList, -1)) == -1)
				break;
			GetPenName(selColor + 3, name);
			color = GetPenColor(selColor + 3);
			if (DoEditColor(name, &color)) {
				ChangePenWindow(selColor + 3, name);
				len = GetPenScrollListItem(strBuff, name, selColor);
				SLChangeItem(scrollList, selColor, strBuff, len);
				newPen = FirstPenColor();
				for (selColor; selColor; selColor--)
					newPen = newPen->Next;
				newPen->Color = color;
				if ((newName = MemAlloc(strlen(name) + 1, MEMF_CLEAR)) == NULL) {
					Error(ERR_NO_MEM);
					break;
				}
				MemFree(newPen->Name, strlen(newPen->Name) + 1);
				strcpy(newName, name);
				newPen->Name = newName;
			}
			SLDrawList(scrollList);
			break;
		case REMOVE_BUTTON:
			if ((selColor = SLNextSelect(scrollList, -1)) == -1)
				break;
			RemovePenColor(selColor);
			SLRemoveItem(scrollList, selColor);
			SLDrawList(scrollList);
			break;
		}
		if (!done)
			SetColorButtons(penColorsDlg, MAX_NUM_PENS);
	} while (!done);
	DisposeDialog(penColorsDlg);
	EndWait();
/*
	Change pen colors
*/
	DisposeScrollList(scrollList);

	return (TRUE);
}

/*
 *	Draw pattern in edit and sample boxes
 */

static void ShowEditPat(DialogPtr dlg, RGBPat8Ptr pat)
{
	WORD x, y, width, height;
	GadgetPtr gadget;
	RastPtr rPort = dlg->RPort;
	Rectangle rect;

/*
	Fill in sample box
*/
	gadget = GadgetItem(dlg->FirstGadget, SAMPLEPAT_USERITEM);
	GetGadgetRect(gadget, dlg, NULL, &rect);
	FillRectNew(rPort, &rect, pat);
/*
	Fill in edit box
*/
	gadget = GadgetItem(dlg->FirstGadget, EDITPAT_USERITEM);
	width = gadget->Width;
	height = gadget->Height;
	for (y = 0; y < 8; y++) {
		rect.MinY = (y*height)/8 + gadget->TopEdge;
		rect.MaxY = ((y + 1)*height)/8 + gadget->TopEdge - 1;
		for (x = 0; x < 8; x++) {
			rect.MinX = (x*width)/8 + gadget->LeftEdge;
			rect.MaxX = ((x + 1)*width)/8 + gadget->LeftEdge - 1;
			RGBForeColor(rPort, (*pat)[y*8 + x]);
			FillRect(rPort, &rect);
		}
	}
}

/*
 *	Draw single pixel of edit pattern and entire sample box
 */

static void DrawEditPixel(DialogPtr dlg, RGBPat8Ptr pat, WORD x, WORD y)
{
	GadgetPtr gadget;
	RastPtr rPort = dlg->RPort;
	Rectangle rect;

/*
	Draw pixel in edit box
*/
	gadget = GadgetItem(dlg->FirstGadget, EDITPAT_USERITEM);
	rect.MinX = (x*gadget->Width)/8 + gadget->LeftEdge;
	rect.MaxX = ((x + 1)*gadget->Width)/8 + gadget->LeftEdge - 1;
	rect.MinY = (y*gadget->Height)/8 + gadget->TopEdge;
	rect.MaxY = ((y + 1)*gadget->Height)/8 + gadget->TopEdge - 1;
	RGBForeColor(rPort, (*pat)[y*8 + x]);
	FillRect(rPort, &rect);
/*
	Fill in sample box
*/
	gadget = GadgetItem(dlg->FirstGadget, SAMPLEPAT_USERITEM);
	GetGadgetRect(gadget, dlg, NULL, &rect);
	FillRectNew(rPort, &rect, pat);
}

/*
 *	build list of color names
 */

static BOOL BuildColorList(TextPtr list[])
{
	register WORD	i, numPens;
	PenColorPtr		pen;

/*
	only skip none
*/
	numPens = NumPens() + 2;
	pen = penColors->Next;
	for (i = 0; i < numPens; i++) {
		if ((list[i] = MemAlloc(strlen(pen->Name) + 1, MEMF_CLEAR)) == NULL)
			return (FALSE);
		strcpy(list[i], pen->Name);
		pen = pen->Next;
	}
	list[i] = NULL;

	return (TRUE);
}

/*
 *	return RGBValue of currently selected pen
 */

static RGBColor	GetSelColor()
{
	WORD	value;

	value = GetGadgetValue(GadgetItem(editFillDlg->FirstGadget, COLOR_POPUP));

	return (GetPenColor(value + 1));
}

/*
 *	Fill patterns dialog filter
 */

static BOOL EditPatsDialogFilter(IntuiMsgPtr intuiMsg, WORD *item)
{
	register WORD	itemHit;
	register ULONG	class;

	class = intuiMsg->Class;
	if (intuiMsg->IDCMPWindow == editFillDlg &&
		(class == GADGETDOWN || class == GADGETUP)) {
		itemHit = GadgetNumber((GadgetPtr) intuiMsg->IAddress);
		if (itemHit == EDITPAT_USERITEM) {
			ReplyMsg((MsgPtr) intuiMsg);
			*item = (class == GADGETDOWN) ? itemHit : -1;
			return (TRUE);
		}
	}
	return (DialogFilter(intuiMsg, item));
}

/*
 *	open edit color dialog
 */

static BOOL DoEditPattern(TextPtr name, RGBPat8Ptr oldPattern)
{
	BOOL		done;
	WORD		i, x, y, item, prevX, prevY;
	RGBPat8		pattern;
	GadgetPtr	gadgList, editGadg, sampleGadg;
	Rectangle	rect;
	TextPtr		colorList[MAX_NUM_PENS];
	RastPtr		rPort;

	CopyRGBPat8(oldPattern, &pattern);

	if (!BuildColorList(colorList)) {
		Error(ERR_NO_MEM);
		return (FALSE);
	}
	dlgList[DLG_EDITFILL]->Gadgets[COLOR_POPUP].Info = colorList;

	BeginWait();
	if ((editFillDlg = GetDialog(dlgList[DLG_EDITFILL], screen, mainMsgPort)) == NULL) {
		EndWait();
		Error(ERR_NO_MEM);
		return (FALSE);
	}
	rPort = editFillDlg->RPort;
	gadgList = editFillDlg->FirstGadget;
	editGadg = GadgetItem(gadgList, EDITPAT_USERITEM);
	editGadg->Activation |= FOLLOWMOUSE;
	sampleGadg = GadgetItem(gadgList, SAMPLEPAT_USERITEM);

	SetPalette(rPort, &screenPalette);
	OutlineOKButton(editFillDlg);
	Delay(5);				/* Wait for gadget to be activated completely */
	if (name[0] != '\0')
		SetEditItemText(gadgList, FILLNAME_TEXT, editFillDlg, NULL, name);
/*
	Draw borders and contents of user items
*/
	GetGadgetRect(editGadg, editFillDlg, NULL, &rect);
	DrawShadowBox(rPort, &rect, -1, TRUE);
	GetGadgetRect(sampleGadg, editFillDlg, NULL, &rect);
	DrawShadowBox(rPort, &rect, -1, FALSE);
	ShowEditPat(editFillDlg, oldPattern);

	done = FALSE;
	while (!done) {
		WaitPort(mainMsgPort);
		item = CheckDialog(mainMsgPort, editFillDlg, EditPatsDialogFilter);
		switch (item) {
			case -1:
				GetEditItemText(gadgList, FILLNAME_TEXT, strBuff);
				EnableGadgetItem(gadgList,OK_BUTTON, editFillDlg, NULL,
							   (strBuff[0] != '\0'));
				break;
			case OK_BUTTON:
			case CANCEL_BUTTON:
				done = TRUE;
				break;
			case EDITPAT_USERITEM:
				prevX = prevY = -1;
				do {
					x = (editFillDlg->MouseX - editGadg->LeftEdge)*8/editGadg->Width;
					y = (editFillDlg->MouseY - editGadg->TopEdge)*8/editGadg->Height;
					if (x < 0 || x > 7 || y < 0 || y > 7)
						continue;
					if (x != prevX || y != prevY) {
						pattern[y*8 + x] = GetSelColor();
						DrawEditPixel(editFillDlg, &pattern, x, y);
						prevX = x;
						prevY = y;
					}
				} while (WaitMouseUp(mainMsgPort, editFillDlg));
				break;
			case CLEAR_BTN:
				for (i = 0; i < 64; i++)
					pattern[i] = GetSelColor();
				ShowEditPat(editFillDlg, &pattern);
				break;
			case REVERT_BTN:
				CopyRGBPat8(oldPattern, &pattern);
				ShowEditPat(editFillDlg, &pattern);
				break;
		}
	}
	GetEditItemText(gadgList, FILLNAME_TEXT, strBuff);
	DisposeDialog(editFillDlg);
	EndWait();

	if (item == CANCEL_BUTTON)
		return (FALSE);

	CopyRGBPat8(&pattern, oldPattern);
	strcpy(name, strBuff);

	return (TRUE);
}

/*
 *	Adjust fill patterns
 */

static BOOL DoFillPatterns()
{
	WORD 			len, item, selPat, i;
	BOOL 			done;
	GadgetPtr 		gadgList;
	RastPtr 		rPort;
	FillPtnPtr		fill;
	TextChar		name[150];
	RGBPat8			pattern;
	TextPtr			newName;
/*
	Bring up dialog
*/
	if ((scrollList = NewScrollList(SL_SINGLESELECT)) == NULL) {
		Error(ERR_NO_MEM);
		return (FALSE);
	}

	BeginWait();
	if ((fillPatsDlg = GetDialog(dlgList[DLG_FILLPATTERNS], screen, mainMsgPort)) == NULL) {
		EndWait();
		DisposeScrollList(scrollList);
		Error(ERR_NO_MEM);
		return (FALSE);
	}
	OutlineOKButton(fillPatsDlg);
	SetColorButtons(fillPatsDlg, MAX_NUM_PATS);
	rPort = fillPatsDlg->RPort;
	gadgList = fillPatsDlg->FirstGadget;
	SetPalette(rPort, &screenPalette);
/*
	build scrollList
*/
	InitScrollList(scrollList, GadgetItem(gadgList, COLOR_BOX), fillPatsDlg, NULL);
	SLDrawBorder(scrollList);
	SLSetDrawProc(scrollList, FillDrawProc);
	SLDoDraw(scrollList, FALSE);
	i = 0;
	for (fill = FirstFillPattern(); fill; fill = fill->Next) {
		len = GetPenScrollListItem(strBuff, fill->Name, i + 1);
		SLAddItem(scrollList, strBuff, len, SLNumItems(scrollList));
		i++;
	}
	SLDoDraw(scrollList, TRUE);
/*
	Handle dialog
*/
	done = FALSE;
	do {
		item = ModalDialog(mainMsgPort, fillPatsDlg, ColorsDialogFilter);
		switch (item) {
		case DONE_BUTTON:
			done = TRUE;
			break;
		case NEW_BUTTON:
			name[0] = '\0';
			GetFillPattern(1, &pattern);
			if (DoEditPattern(name, &pattern)) {
				AddFillPattern(name, pattern, TRUE);
				len = GetFillScrollListItem(strBuff, name, SLNumItems(scrollList) + 1);
				SLAddItem(scrollList, strBuff, len, SLNumItems(scrollList));
				SLDrawList(scrollList);
			}
			break;
		case COLOR_BOX:
			if (!SLIsDoubleClick(scrollList))
				break;				// fall through
		case EDIT_BUTTON:
			if ((selPat = SLNextSelect(scrollList, -1)) == -1)
				break;
			GetFillName(selPat + 1, name);
			GetFillPattern(selPat + 1, &pattern);
			if (DoEditPattern(name, &pattern)) {
				ChangeFillWindow(selPat + 1, name);
				len = GetFillScrollListItem(strBuff, name, selPat);
				SLChangeItem(scrollList, selPat, strBuff, len);
				fill = FirstFillPattern();
				for (selPat; selPat; selPat--)
					fill = fill->Next;
				CopyRGBPat8(&pattern, &fill->Pattern);
				if ((newName = MemAlloc(strlen(name) + 1, MEMF_CLEAR)) == NULL) {
					Error(ERR_NO_MEM);
					break;
				}
				MemFree(fill->Name, strlen(fill->Name) + 1);
				strcpy(newName, name);
				fill->Name = newName;
			}
			SLDrawList(scrollList);
			break;
		case REMOVE_BUTTON:
			if ((selPat = SLNextSelect(scrollList, -1)) == -1)
				break;
			RemoveFillPattern(selPat);
			SLRemoveItem(scrollList, selPat);
			SLDrawList(scrollList);
			break;
		}
		if (!done)
			SetColorButtons(fillPatsDlg, MAX_NUM_PATS);
	} while (!done);
	DisposeDialog(fillPatsDlg);
	EndWait();
/*
	Change pen colors
*/
	DisposeScrollList(scrollList);

	return (TRUE);
}

/*
 *	Show names of document layers in scroll box
 */

static void ShowLayerNames(DocDataPtr docData, DialogPtr dlg, ScrollListPtr scrollList,
						   DocLayerPtr selLayer)
{
	WORD i, numLayers;
	TextPtr name;
	DocLayerPtr docLayer;
	TextChar text[20];

	SLDoDraw(scrollList, FALSE);
	SLRemoveAll(scrollList);
	numLayers = NumLayers(docData);
	i = 0;
	for (docLayer = TopLayer(docData); docLayer; docLayer = PrevLayer(docLayer)) {
		if ((name = GetLayerName(docLayer)) == NULL) {
			strcpy(text, "(");
			strcat(text, strLayer);
			NumToString(numLayers - i, text + strlen(text));
			strcat(text, ")");
			name = text;
		}
		SLAddItem(scrollList, name, strlen(name), i);
		if ((docLayer->Flags & LAYER_VISIBLE) == 0)
			SLSetItemStyle(scrollList, i, FSF_ITALIC);
		i++;
	}
	i = numLayers - LayerNum(docData, selLayer) - 1;
	SLSelectItem(scrollList, i, TRUE);
	SLAutoScroll(scrollList, i);
	SLDoDraw(scrollList, TRUE);
}

/*
 *	Set layers dialog buttons
 */

static void SetLayersButtons(DialogPtr dlg, DocLayerPtr selLayer)
{
	BOOL hasPrev, hasNext, visible;
	GadgetPtr gadgList = dlg->FirstGadget;

	hasPrev = (selLayer && PrevLayer(selLayer) != NULL);
	hasNext = (selLayer && NextLayer(selLayer) != NULL);
	visible = (selLayer && LayerVisible(selLayer));
	EnableGadgetItem(gadgList, DELETE_BTN, dlg, NULL, (hasPrev || hasNext));
	EnableGadgetItem(gadgList, MOVEUP_ARROW, dlg, NULL, hasNext);
	EnableGadgetItem(gadgList, MOVEDOWN_ARROW, dlg, NULL, hasPrev);
	EnableGadgetItem(gadgList, SHOW_BTN, dlg, NULL, !visible);
	EnableGadgetItem(gadgList, HIDE_BTN, dlg, NULL, visible);
}

/*
 *	Layers dialog filter
 */

static BOOL LayersDialogFilter(register IntuiMsgPtr intuiMsg, WORD *item)
{
	WORD gadgNum, selItem;
	ULONG class = intuiMsg->Class;
	UWORD code = intuiMsg->Code;

	switch (class) {
/*
	Handle gadget down of up/down scroll arrows and move arrows
*/
	case GADGETDOWN:
	case GADGETUP:
		gadgNum = GadgetNumber((GadgetPtr) intuiMsg->IAddress);
		if (gadgNum == LAYERS_LIST || gadgNum == LAYERS_SCROLL ||
			gadgNum == LAYERS_UP || gadgNum == LAYERS_DOWN) {
			SLGadgetMessage(layersScrollList, mainMsgPort, intuiMsg);
			*item = gadgNum;
			return (TRUE);
		}
		if (gadgNum == MOVEUP_ARROW || gadgNum == MOVEDOWN_ARROW) {
			ReplyMsg((MsgPtr) intuiMsg);
			*item = (class == GADGETDOWN) ? gadgNum : -1;
			return (TRUE);
		}
		break;
/*
	Handle keyboard events
*/
	case RAWKEY:
		if (code == CURSORUP || code == CURSORDOWN) {
			ReplyMsg((MsgPtr) intuiMsg);
			selItem = SLNextSelect(layersScrollList, -1);
			if (selItem == -1)
				selItem = 0;
			else if (code == CURSORUP)
				selItem--;
			else
				selItem++;
			SLSelectItem(layersScrollList, selItem, TRUE);
			return (TRUE);
		}
		break;
	}
	return (DialogFilter(intuiMsg, item));
}

/*
 *	Modify document layers
 */

BOOL DoLayers(WindowPtr window)
{
	WORD item, selLayerNum;
	BOOL done, success;
	GadgetPtr gadgList, gadget;
	DocLayerPtr selLayer, docLayer;
	DocDataPtr docData = GetWRefCon(window);
	Rectangle rect;
	TextChar nameText[GADG_MAX_STRING];

	success = FALSE;
	layersScrollList = NULL;
	layersDlg = NULL;
/*
	Bring layers dialog
*/
	BeginWait();
	if ((layersScrollList = NewScrollList(SL_SINGLESELECT)) == NULL ||
		(layersDlg = GetDialog(dlgList[DLG_LAYERS], screen, mainMsgPort)) == NULL) {
		EndWait();
		Error(ERR_NO_MEM);
		goto Exit;
	}
	gadgList = layersDlg->FirstGadget;
	InitScrollList(layersScrollList, GadgetItem(gadgList, LAYERS_LIST), layersDlg, NULL);
	OutlineOKButton(layersDlg);
	SLDrawBorder(layersScrollList);
	selLayer = CurrLayer(docData);
	ShowLayerNames(docData, layersDlg, layersScrollList, selLayer);
	SetLayersButtons(layersDlg, selLayer);
	SetEditItemText(gadgList, NAME_TEXT, layersDlg, NULL, GetLayerName(selLayer));
/*
	Handle dialog
*/
	done = FALSE;
	do {
		item = ModalDialog(mainMsgPort, layersDlg, LayersDialogFilter);
		selLayerNum = NumLayers(docData) - SLNextSelect(layersScrollList, -1) - 1;
		selLayer = GetLayer(docData, selLayerNum);
		GetEditItemText(gadgList, NAME_TEXT, nameText);
		switch (item) {
		case LAYERS_LIST:
			SetEditItemText(gadgList, NAME_TEXT, layersDlg, NULL, GetLayerName(selLayer));
			if (!SLIsDoubleClick(layersScrollList))
				break;
			item = DONE_BTN;				/* Fall through */
		case DONE_BTN:
			if (LayerVisible(selLayer))
				docLayer = selLayer;
			else {
				docLayer = NextVisLayer(selLayer);
				if (docLayer == NULL)
					docLayer = PrevVisLayer(selLayer);
			}
			if (docLayer == NULL) {
				Error(ERR_NO_VIS_LAYER);
				break;
			}
			SetCurrLayer(docData, docLayer);
			SetLayerIndic(window, FALSE);
			AdjustWindowTitle(window);
			GetContentRect(window, &rect);
			InvalRect(window, &rect);
			done = TRUE;
			break;
		case NEW_BTN:
			docLayer = NewDocLayer(docData, (nameText[0]) ? nameText : NULL);
			if (docLayer) {
				ShowLayerNames(docData, layersDlg, layersScrollList, docLayer);
				DocModified(docData);
			}
			break;
		case RENAME_BTN:
			SetLayerName(selLayer, (nameText[0]) ? nameText : NULL);
			ShowLayerNames(docData, layersDlg, layersScrollList, selLayer);
			DocModified(docData);
			break;
		case DELETE_BTN:
			if (NumLayers(docData) <= 1) {
				Error(ERR_LAST_LAYER);
				break;
			}
			if (BottomObject(selLayer) != NULL &&
				StdDialog(DLG_DELETELAYER) != OK_BUTTON)
				break;
			docLayer = NextLayer(selLayer);
			if (docLayer == NULL)
				docLayer = PrevLayer(selLayer);
			DetachLayer(docData, selLayer);
			DisposeDocLayer(selLayer);
			ShowLayerNames(docData, layersDlg, layersScrollList, docLayer);
			DocModified(docData);
			break;
		case HIDE_BTN:
		case SHOW_BTN:
			if (item == HIDE_BTN)
				HideLayer(selLayer);
			else
				ShowLayer(selLayer);
			ShowLayerNames(docData, layersDlg, layersScrollList, selLayer);
			DocModified(docData);
			break;
		case MOVEUP_ARROW:
		case MOVEDOWN_ARROW:
			if (item == MOVEUP_ARROW)
				LayerForward(docData, selLayer);
			else
				LayerBackward(docData, selLayer);
			ShowLayerNames(docData, layersDlg, layersScrollList, selLayer);
			DocModified(docData);
			break;
		}
		if (!done && item != -1) {
			gadget = GadgetItem(gadgList, NAME_TEXT);
			ActivateGadget(gadget, layersDlg, NULL);
			selLayerNum = NumLayers(docData) - SLNextSelect(layersScrollList, -1) - 1;
			selLayer = GetLayer(docData, selLayerNum);
			SetLayersButtons(layersDlg, selLayer);
		}
	} while (!done);
	DisposeDialog(layersDlg);
	EndWait();
	success = TRUE;
/*
	Clean up
*/
Exit:
	if (layersScrollList)
		DisposeScrollList(layersScrollList);
	return (success);
}

/*
 *	Find minimum drawing size that will contain all objects
 */

static void MinDrawSize(DocDataPtr docData, WORD *minWidth, WORD *minHeight)
{
	DocObjPtr docObj;
	DocLayerPtr docLayer;

	*minWidth = docData->PageWidth;
	*minHeight = docData->PageHeight;
	for (docLayer = BottomLayer(docData); docLayer; docLayer = NextLayer(docLayer)) {
		for (docObj = BottomObject(docLayer); docObj; docObj = NextObj(docObj)) {
			if (*minWidth <= docObj->Frame.MaxX)
				*minWidth = docObj->Frame.MaxX + 1;
			if (*minHeight <= docObj->Frame.MaxY)
				*minHeight = docObj->Frame.MaxY + 1;
		}
	}
	if (*minWidth > MAX_DOCWIDTH)
		*minWidth = MAX_DOCWIDTH;
	if (*minHeight > MAX_DOCHEIGHT)
		*minHeight = MAX_DOCHEIGHT;
}

/*
 *	Show current drawing size in draw size dialog
 */

static void ShowDrawSize(DialogPtr dlg, DocDataPtr docData, WORD docWidth, WORD docHeight)
{
	WORD x, y, width, height, docX, docY;
	WORD page, pages, pagesAcross, pagesDown;
	RastPtr rPort = dlg->RPort;
	GadgetPtr gadget, gadgList;
	Rectangle rect;

	gadgList = dlg->FirstGadget;
	gadget = GadgetItem(gadgList, DRAWSIZE_USERITEM);
	GetGadgetRect(gadget, dlg, NULL, &rect);
	width = rect.MaxX - rect.MinX + 1;
	height = rect.MaxY - rect.MinY + 1;
	DrawShadowBox(rPort, &rect, -1, TRUE);
/*
	First fill in pages used
	Do it this way to avoid flickering
*/
	docX = rect.MinX + ((LONG) docWidth*width + MAX_DOCWIDTH/2)/MAX_DOCWIDTH - 1;
	docY = rect.MinY + ((LONG) docHeight*height + MAX_DOCHEIGHT/2)/MAX_DOCHEIGHT - 1;
	SetDrMd(rPort, JAM1);
	SetAPen(rPort, _tbPenWhite);
	RectFill(rPort, rect.MinX, rect.MinY, docX, docY);
	SetAPen(rPort, _tbPenDark);
	if (docX < rect.MaxX)
		RectFill(rPort, docX + 1, rect.MinY, rect.MaxX, rect.MaxY);
	if (docY < rect.MaxY)
		RectFill(rPort, rect.MinX, docY + 1, rect.MaxX, rect.MaxY);
/*
	Now draw lines between pages
*/
	SetAPen(rPort, _tbPenBlack);
	pages = docWidth/docData->PageWidth;
	for (page = 0; page <= pages; page++) {
		x = ((LONG) page*docData->PageWidth*width + MAX_DOCWIDTH/2)/MAX_DOCWIDTH;
		if (x >= width)
			x = width - 1;
		Move(rPort, rect.MinX + x, rect.MinY);
		Draw(rPort, rect.MinX + x, docY);
	}
	pages = docHeight/docData->PageHeight;
	for (page = 0; page <= pages; page++) {
		y = ((LONG) page*docData->PageHeight*height + MAX_DOCHEIGHT/2)/MAX_DOCHEIGHT;
		if (y >= height)
			y = height - 1;
		Move(rPort, rect.MinX, rect.MinY + y);
		Draw(rPort, docX, rect.MinY + y);
	}
/*
	Show dimensions
*/
	pagesAcross = (docWidth - 1)/docData->PageWidth + 1;
	pagesDown = (docHeight - 1)/docData->PageHeight + 1;
	DotToText(docWidth, docData->xDPI, strBuff, 2);
	strcat(strBuff, (options.MeasureUnit == MEASURE_CM) ? strCM : strInch);
	SetGadgetItemText(gadgList, WIDTH_TEXT, dlg, NULL, strBuff);
	DotToText(docHeight, docData->yDPI, strBuff, 2);
	strcat(strBuff, (options.MeasureUnit == MEASURE_CM) ? strCM : strInch);
	SetGadgetItemText(gadgList, HEIGHT_TEXT, dlg, NULL, strBuff);
	NumToString(pagesAcross*pagesDown, strBuff);
	SetGadgetItemText(gadgList, PAGES_TEXT, dlg, NULL, strBuff);
}

/*
 *	Drawing size dialog filter
 */

static BOOL DrawSizeDialogFilter(IntuiMsgPtr intuiMsg, WORD *item)
{
	register WORD itemHit;
	register ULONG class;

	class = intuiMsg->Class;
	if (intuiMsg->IDCMPWindow == drawSizeDlg && (class == GADGETDOWN || class == GADGETUP)) {
		itemHit = GadgetNumber((GadgetPtr) intuiMsg->IAddress);
		if (itemHit == DRAWSIZE_USERITEM) {
			ReplyMsg((MsgPtr) intuiMsg);
			*item = (class == GADGETDOWN) ? itemHit : -1;
			return (TRUE);
		}
	}
	return (DialogFilter(intuiMsg, item));
}

/*
 *	Set drawing size
 */

static BOOL DoDrawingSize(WindowPtr window)
{
	WORD item;
	WORD pageWidth, pageHeight, minWidth, minHeight, docWidth, docHeight, pages;
	WORD prevWidth, prevHeight;
	LONG x, y;						/* To force LONG calculations */
	GadgetPtr sizeGadg;
	DocDataPtr docData = GetWRefCon(window);
	Rectangle rect;

	pageWidth = docData->PageWidth;
	pageHeight = docData->PageHeight;
	docWidth = docData->DocWidth;
	docHeight = docData->DocHeight;
	MinDrawSize(docData, &minWidth, &minHeight);
	if (docWidth < minWidth)
		docWidth = minWidth;
	if (docHeight < minHeight)
		docHeight = minHeight;
/*
	Set up drawing size dialog
*/
	BeginWait();
	if ((drawSizeDlg = GetDialog(dlgList[DLG_DRAWSIZE], screen, mainMsgPort)) == NULL) {
		EndWait();
		Error(ERR_NO_MEM);
		return (FALSE);
	}
	OutlineOKButton(drawSizeDlg);
	ShowDrawSize(drawSizeDlg, docData, docWidth, docHeight);
	sizeGadg = GadgetItem(drawSizeDlg->FirstGadget, DRAWSIZE_USERITEM);
/*
	Handle dialog
*/
	for (;;) {
		item = ModalDialog(mainMsgPort, drawSizeDlg, DrawSizeDialogFilter);
		if (item == OK_BUTTON || item == CANCEL_BUTTON)
			break;
		if (item == DRAWSIZE_USERITEM) {
			do {
				prevWidth  = docWidth;
				prevHeight = docHeight;
				x = drawSizeDlg->MouseX - sizeGadg->LeftEdge;
				y = drawSizeDlg->MouseY - sizeGadg->TopEdge;
				docWidth = (x*MAX_DOCWIDTH)/sizeGadg->Width;
				if (docWidth < minWidth)
					docWidth = minWidth;
				pages = (docWidth + pageWidth - 1)/pageWidth;
				docWidth = pages*pageWidth;
				if (docWidth > MAX_DOCWIDTH)
					docWidth = MAX_DOCWIDTH;
				docHeight = (y*MAX_DOCHEIGHT)/sizeGadg->Height;
				if (docHeight < minHeight)
					docHeight = minHeight;
				pages = (docHeight + pageHeight - 1)/pageHeight;
				docHeight = pages*pageHeight;
				if (docHeight > MAX_DOCHEIGHT)
					docHeight = MAX_DOCHEIGHT;
				if (docWidth != prevWidth || docHeight != prevHeight)
					ShowDrawSize(drawSizeDlg, docData, docWidth, docHeight);
			} while (WaitMouseUp(mainMsgPort, drawSizeDlg));
		}
	}
	DisposeDialog(drawSizeDlg);
	EndWait();
	if (item == CANCEL_BUTTON)
		return (FALSE);
/*
	Set result
*/
	docData->DocWidth = docWidth;
	docData->DocHeight = docHeight;
	AdjustScrollOffset(window);
	AdjustScrollBars(window);
	GetContentRect(window, &rect);
	InvalRect(window, &rect);
	DocModified(docData);
	return (TRUE);
}

/*
 *	Handle Layout menu
 */

BOOL DoLayoutMenu(WindowPtr window, UWORD item, UWORD sub, UWORD modifier)
{
	BOOL success;

	if (!IsDocWindow(window) && item != PENCOLORS_ITEM &&
		item != FILLPATTERNS_ITEM && item != PREFERENCES_ITEM &&
		item != SCREENCOLORS_ITEM)
		return (FALSE);
	UndoOff();
	success = FALSE;
	switch (item) {
	case NORMALSIZE_ITEM:
		success = DoNormalSize(window);
		break;
	case ENLARGE_ITEM:
		success = DoEnlarge(window);
		break;
	case REDUCE_ITEM:
		success = DoReduce(window);
		break;
	case FITTOWINDOW_ITEM:
		success = DoFitToWindow(window);
		break;
	case GRIDSNAP_ITEM:
		success = DoGridSnap(window);
		break;
	case GRIDSIZE_ITEM:
		success = DoGridSize(window);
		break;
	case PENCOLORS_ITEM:
		success = DoPenColors();
		break;
	case FILLPATTERNS_ITEM:
		success = DoFillPatterns();
		break;
	case LAYERS_ITEM:
		success = DoLayers(window);
		break;
	case DRAWINGSIZE_ITEM:
		success = DoDrawingSize(window);
		break;
	case PREFERENCES_ITEM:
		success = DoPreferences();
		break;
	case SCREENCOLORS_ITEM:
		success = DoScreenColors();
		break;
	}
	return (success);
}
