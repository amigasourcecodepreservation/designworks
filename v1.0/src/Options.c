/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Preferences and screen colors dialog
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>

#include <Toolbox/Graphics.h>
#include <Toolbox/Window.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Utility.h>
#include <Toolbox/Color.h>

#include "Draw.h"
#include "Proto.h"

/*
 *	External variables
 */

extern MsgPortPtr	mainMsgPort;
extern ScreenPtr	screen;

extern WORD			numWindows;
extern WindowPtr	windowList[];

extern Options	options;

extern UBYTE	whiteColor, blackColor, lightColor, darkColor;

extern ColorTable		screenColors;
extern InvColorTable	scrnInvColorTable;

extern TextChar	strScreenColor[];

extern DlgTemplPtr	dlgList[];

/*
 *	Local variables and definitions
 */

enum {
	PREFRESET_BTN = 2,
	INCH_RADBTN,
	CM_RADBTN,
	FLASH_RADBTN,
	SOUND_RADBTN,
	BOTH_RADBTN,
	POLYCLOSE_BOX,
	DRAGOUTLINES_BOX,
	SHOWPICTS_BOX,
	SAVEICONS_BOX,
	COLORCORRECT_BOX
};

enum {
	COLORRESET_BTN = 2,
	SCREENCOLORS_USERITEM,
	CHANGEPEN_BTN
};

static DialogPtr	colorsDlg;

/*
 *	Local prototypes
 */

void	SetOptionsButtons(DialogPtr, Options *);

void	GetNumAcrossDown(WORD, WORD *, WORD *);
void	DrawColorItem(DialogPtr, WORD, BOOL);
BOOL	ScreenColorsDialogFilter(IntuiMsgPtr, WORD *);

/*
 *	Set standard options
 */

void SetStdOptions(Options *opts)
{
	BlockClear(opts, sizeof(Options));
	opts->MeasureUnit	= DEFAULT_MEASURE;
	opts->BlinkPeriod	= BLINK_FAST;
	opts->BeepFlash		= TRUE;
	opts->BeepSound		= TRUE;
	opts->SaveIcons		= TRUE;
	opts->DragOutlines	= TRUE;
	opts->PolyAutoClose	= FALSE;
	opts->ShowPictures	= TRUE;
	opts->ColorCorrect	= TRUE;
	opts->PrtCyan		= PRT_RGBCOLOR_CYAN;
	opts->PrtMagenta	= PRT_RGBCOLOR_MAGENTA;
	opts->PrtYellow		= PRT_RGBCOLOR_YELLOW;
}

/*
 *	Set options dialog buttons
 */

static void SetOptionsButtons(DialogPtr dlg, Options *opts)
{
	GadgetPtr gadgList = dlg->FirstGadget;

	SetGadgetItemValue(gadgList, INCH_RADBTN, dlg, NULL, (opts->MeasureUnit == MEASURE_INCH));
	SetGadgetItemValue(gadgList, CM_RADBTN, dlg, NULL, (opts->MeasureUnit == MEASURE_CM));
	SetGadgetItemValue(gadgList, FLASH_RADBTN, dlg, NULL, opts->BeepFlash && !opts->BeepSound);
	SetGadgetItemValue(gadgList, SOUND_RADBTN, dlg, NULL, !opts->BeepFlash && opts->BeepSound);
	SetGadgetItemValue(gadgList, BOTH_RADBTN, dlg, NULL, opts->BeepFlash && opts->BeepSound);
	SetGadgetItemValue(gadgList, POLYCLOSE_BOX, dlg, NULL, opts->PolyAutoClose);
	SetGadgetItemValue(gadgList, DRAGOUTLINES_BOX, dlg, NULL, opts->DragOutlines);
	SetGadgetItemValue(gadgList, SHOWPICTS_BOX, dlg, NULL, opts->ShowPictures);
	SetGadgetItemValue(gadgList, SAVEICONS_BOX, dlg, NULL, opts->SaveIcons);
	SetGadgetItemValue(gadgList, COLORCORRECT_BOX, dlg, NULL, opts->ColorCorrect);
}

/*
 *	Adjust user options
 */

BOOL DoOptions()
{
	WORD i, item;
	BOOL newMeasure, newColor, newPict;
	DialogPtr dlg;
	Rectangle rect;
	Options opts;

	UndoOff();
	opts = options;
	if (!opts.BeepSound)
		opts.BeepFlash = TRUE;
/*
	Get request
*/
	BeginWait();
	if ((dlg = GetDialog(dlgList[DLG_OPTIONS], screen, mainMsgPort)) == NULL) {
		EndWait();
		Error(ERR_NO_MEM);
		return (FALSE);
	}
	OutlineOKButton(dlg);
	SetOptionsButtons(dlg, &opts);
/*
	Handle requester
*/
	for (;;) {
		item = ModalDialog(mainMsgPort, dlg, DialogFilter);
		if (item == OK_BUTTON || item == CANCEL_BUTTON)
			break;
		switch (item) {
		case PREFRESET_BTN:
			SetStdOptions(&opts);
			break;
		case INCH_RADBTN:
		case CM_RADBTN:
			opts.MeasureUnit = (item == INCH_RADBTN) ? MEASURE_INCH : MEASURE_CM;
			break;
		case FLASH_RADBTN:
			opts.BeepFlash = TRUE;
			opts.BeepSound = FALSE;
			break;
		case SOUND_RADBTN:
			opts.BeepFlash = FALSE;
			opts.BeepSound = TRUE;
			break;
		case BOTH_RADBTN:
			opts.BeepFlash = opts.BeepSound = TRUE;
			break;
		case POLYCLOSE_BOX:
			opts.PolyAutoClose = !opts.PolyAutoClose;
			break;
		case DRAGOUTLINES_BOX:
			opts.DragOutlines = !opts.DragOutlines;
			break;
		case SHOWPICTS_BOX:
			opts.ShowPictures = !opts.ShowPictures;
			break;
		case SAVEICONS_BOX:
			opts.SaveIcons = !opts.SaveIcons;
			break;
		case COLORCORRECT_BOX:
			opts.ColorCorrect = !opts.ColorCorrect;
			break;
		}
		SetOptionsButtons(dlg, &opts);
	}
	DisposeDialog(dlg);
	EndWait();
	if (item == CANCEL_BUTTON)
		return (FALSE);
/*
	Set new values
*/
	newMeasure = (opts.MeasureUnit != options.MeasureUnit);
	newColor = (opts.ColorCorrect != options.ColorCorrect);
	newPict = (opts.ShowPictures != options.ShowPictures);
	options = opts;
	ColorCorrectEnable(options.ColorCorrect);
	if (newMeasure || newColor || newPict) {
		for (i = 0; i < numWindows; i++) {
			GetWindowRect(windowList[i], &rect);
			InvalRect(windowList[i], &rect);
		}
	}
	if (newColor) {
		DrawToolWindow();
		DrawPenWindow();
		DrawFillWindow();
	}
	return (TRUE);
}

/*
 *	Get number of colors across and down in dialog
 */

static void GetNumAcrossDown(WORD numColors, WORD *numAcross, WORD *numDown)
{
	if (numColors > 64)
		*numDown = 8;
	else if (numColors > 16)
		*numDown = 4;
	else if (numColors > 4)
		*numDown = 2;
	else
		*numDown = 1;
	*numAcross = numColors/(*numDown);
}

/*
 *	Draw specified color item in dialog, hiliting if seleted
 */

static void DrawColorItem(DialogPtr dlg, WORD penNum, BOOL hilite)
{
	WORD x, y, width, height;
	WORD numColors, numAcross, numDown;
	RastPtr rPort;
	GadgetPtr gadget;
	Rectangle rect;

	numColors = 1 << screen->BitMap.Depth;
	GetNumAcrossDown(numColors, &numAcross, &numDown);
	rPort = dlg->RPort;
	gadget = GadgetItem(dlg->FirstGadget, SCREENCOLORS_USERITEM);
	width = gadget->Width;
	height = gadget->Height;
	x = gadget->LeftEdge + (penNum % numAcross)*width/numAcross;
	y = gadget->TopEdge + (penNum/numAcross)*height/numDown;
	SetRect(&rect,  x, y, x + width/numAcross - 1, y + height/numDown - 1);
	if (hilite) {
		SetAPen(rPort, blackColor);
		FrameRect(rPort, &rect, 1, 1);
		InsetRect(&rect, 1, 1);
		FrameRect(rPort, &rect, 1, 1);
	}
	else {
		SetAPen(rPort, whiteColor);
		FrameRect(rPort, &rect, 1, 1);
	}
	InsetRect(&rect, 1, 1);
	SetAPen(rPort, penNum);
	FillRect(rPort, &rect, NULL);
}

/*
 *	Pen colors dialog filter
 */

static BOOL ScreenColorsDialogFilter(register IntuiMsgPtr intuiMsg, WORD *item)
{
	register WORD itemHit;
	register ULONG class;

	class = intuiMsg->Class;
	if (intuiMsg->IDCMPWindow == colorsDlg && (class == GADGETDOWN || class == GADGETUP)) {
		itemHit = GadgetNumber((GadgetPtr) intuiMsg->IAddress);
		if (itemHit == SCREENCOLORS_USERITEM) {
			ReplyMsg((MsgPtr) intuiMsg);
			*item = (class == GADGETDOWN) ? itemHit : -1;
			return (TRUE);
		}
	}
	return (DialogFilter(intuiMsg, item));
}

/*
 *	Adjust screen colors
 */

BOOL DoScreenColors()
{
	WORD i, item, selColor, numColors;
	WORD x, y, numAcross, numDown;
	BOOL done, doubleClick;
	ULONG secs, micros, prevSecs, prevMicros;
	GadgetPtr colorsGadg;
	RastPtr rPort;
	BorderPtr border;
	RGBColor newColor, prevColors[256];

	numColors = 1 << screen->BitMap.Depth;
	for (i = 0; i < numColors; i++)
		prevColors[i] = screenColors[i];
/*
	Bring up dialog
*/
	BeginWait();
	if ((colorsDlg = GetDialog(dlgList[DLG_SCREENCOLORS], screen, mainMsgPort)) == NULL) {
		EndWait();
		Error(ERR_NO_MEM);
		return (FALSE);
	}
	OutlineOKButton(colorsDlg);
	rPort = colorsDlg->RPort;
	rPort->RP_User = (APTR *) &scrnInvColorTable;
	colorsGadg = GadgetItem(colorsDlg->FirstGadget, SCREENCOLORS_USERITEM);
	colorsGadg->Activation |= GADGIMMEDIATE;
	selColor = 0;
/*
	Draw dark border around pens and draw pen colors
*/
	if ((border = BoxBorder(colorsGadg->Width, colorsGadg->Height, darkColor, -1)) != NULL) {
		DrawBorder(rPort, border, colorsGadg->LeftEdge, colorsGadg->TopEdge);
		FreeBorder(border);
	}
	for (i = 0; i < numColors; i++)
		DrawColorItem(colorsDlg, i, (i == selColor));
	GetNumAcrossDown(numColors, &numAcross, &numDown);
/*
	Handle dialog
*/
	prevSecs = prevMicros = 0;
	done = FALSE;
	do {
		item = ModalDialog(mainMsgPort, colorsDlg, ScreenColorsDialogFilter);
		CurrentTime(&secs, &micros);
		switch (item) {
		case OK_BUTTON:
		case CANCEL_BUTTON:
			done = TRUE;
			break;
		case COLORRESET_BTN:
			LoadRGB4(&screen->ViewPort, prevColors, numColors);
			UpdateWindows();	/* Since no refresh happens */
								/* UpdateWindows() calls CheckColorTable() */
			break;
		case SCREENCOLORS_USERITEM:
			x = (colorsDlg->MouseX - colorsGadg->LeftEdge)*numAcross/colorsGadg->Width;
			y = (colorsDlg->MouseY - colorsGadg->TopEdge)*numDown/colorsGadg->Height;
			if (x < 0 || x >= numAcross || y < 0 || y >= numDown)
				break;
			i = x + y*numAcross;
			if (i != selColor) {
				DrawColorItem(colorsDlg, selColor, FALSE);
				selColor = i;
				DrawColorItem(colorsDlg, selColor, TRUE);
				doubleClick = FALSE;
			}
			else
				doubleClick = DoubleClick(prevSecs, prevMicros, secs, micros);
			if (!doubleClick) {
				prevSecs = secs;
				prevMicros = micros;
				break;
			}
			item = CHANGEPEN_BTN;		/* Fall through */
		case CHANGEPEN_BTN:
			if (GetColor(screen, mainMsgPort, DialogFilter, strScreenColor, NULL,
						 screenColors[selColor], &newColor, selColor)) {
				CheckColorTable();
			}
			break;
		}
	} while (!done);
	DisposeDialog(colorsDlg);
	EndWait();
	if (item == CANCEL_BUTTON) {
		LoadRGB4(&screen->ViewPort, prevColors, numColors);
		CheckColorTable();
	}
	return ((BOOL) (item == OK_BUTTON));
}
