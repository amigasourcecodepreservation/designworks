/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Toolbox library
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	Icon definitions
 */

#ifndef TOOLBOX_ICON_H
#define TOOLBOX_ICON_H

#ifndef TYPEDEFS_H
#include <Typedefs.h>
#endif

/*
 *	Vector icon
 *	Points to vector description of icon, as well as zero or more pre-built icons
 *	When building image, if there is no exact pre-built size match, and no vector
 *		description is given, then last pre-built icon in the list will be used
 */

typedef struct {
	Ptr		DrawList;		// Currently unimplemented
	IconPtr	*Icons;			// One or more pre-built icons, last entry of NULL
} VectorIcon, *VecIconPtr;

#endif
