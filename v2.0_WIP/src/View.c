/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	View menu routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <proto/intuition.h>
#include <proto/dos.h>			/* For Delay() prototype */

#include <string.h>

#include <Toolbox/Memory.h>
#include <Toolbox/Menu.h>
#include <Toolbox/Window.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Utility.h>

#include "Draw.h"
#include "Proto.h"

/*
 *	External variables
 */

extern WindowPtr	backWindow, toolWindow, penWindow, fillWindow;
extern WindowPtr	windowList[];
extern WORD			numWindows;

extern BOOL			closeFlag;
extern WindowPtr	closeWindow;

/*
 *	Local variables and definitions
 */

#define WINDOW_TOOL	0
#define WINDOW_PEN	1
#define WINDOW_FILL	2

/*
 *	Local prototypes
 */

static void	ToggleFlag(WindowPtr, UWORD);

static void	DoShowBackLayers(WindowPtr);
static void	DoShowRuler(WindowPtr);
static void	DoShowGridLines(WindowPtr);
static void	DoShowPageBreaks(WindowPtr);

static void	DoShowExtraWindow(WORD);

/*
 *	Toggle flag in docData
 */

static void ToggleFlag(WindowPtr window, UWORD flag)
{
	register DocDataPtr docData = GetWRefCon(window);

	if (docData->Flags & flag)
		docData->Flags &= ~flag;
	else
		docData->Flags |= flag;
}

/*
 *	Show/hide back layers
 */

static void DoShowBackLayers(WindowPtr window)
{
	Rectangle rect;

	ToggleFlag(window, DOC_HIDEBACKLAY);
	GetContentRect(window, &rect);
	InvalRect(window, &rect);
}

/*
 *	Show/hide ruler
 */

static void DoShowRuler(WindowPtr window)
{
	Rectangle rect;

	ToggleFlag(window, DOC_SHOWRULER);
	DoNewSize(window);			/* Sets clip region and adjust scroll bars */
	GetWindowRect(window, &rect);
	InvalRect(window, &rect);
}

/*
 *	Show/hide grid lines
 */

static void DoShowGridLines(WindowPtr window)
{
	Rectangle rect;

	ToggleFlag(window, DOC_SHOWGRID);
	GetContentRect(window, &rect);
	InvalRect(window, &rect);
}

/*
 *	Show/hide ruler
 */

static void DoShowPageBreaks(WindowPtr window)
{
	Rectangle rect;

	ToggleFlag(window, DOC_SHOWPAGE);
	GetContentRect(window, &rect);
	InvalRect(window, &rect);
}

/*
 *	Show or hide extra window
 */

static void DoShowExtraWindow(WORD windNum)
{
	WindowPtr window;

	if (windNum == WINDOW_TOOL)
		window = toolWindow;
	else if (windNum == WINDOW_PEN)
		window = penWindow;
	else
		window = fillWindow;
	if (window == NULL) {
		if (windNum == WINDOW_TOOL)
			OpenToolWindow();
		else if (windNum == WINDOW_PEN)
			OpenPenWindow();
		else
			OpenFillWindow();
	}
	else if (LayerObscured(window->WLayer))
		WindowToFront(window);
	else {
		closeFlag = TRUE;
		closeWindow = window;
	}
}

/*
 *	Add window to window list and view menu
 */

void AddWindowItem(WindowPtr window)
{
	UWORD item;
	MenuItemPtr menuItem;
	DocDataPtr docData = GetWRefCon(window);

	item = MENUITEM(VIEW_MENU, numWindows + WINDOW_ITEM, NOSUB);
	InsertMenuItem(backWindow, item, docData->WindowName);
	menuItem = ItemAddress(backWindow->MenuStrip, item);
	menuItem->Flags |= CHECKIT;
	menuItem->MutualExclude = ~(1 << numWindows) << WINDOW_ITEM;
	windowList[numWindows++] = window;
}

/*
 *	Remove window from window list and view menu
 */

void RemoveWindowItem(WindowPtr window)
{
	WORD i;
	MenuItemPtr menuItem;

	if ((i = WindowNum(window)) == -1)
		return;
	DeleteMenuItem(backWindow, MENUITEM(VIEW_MENU, i + WINDOW_ITEM, NOSUB));
	numWindows--;
	while (i < numWindows) {
		windowList[i] = windowList[i + 1];
		i++;
	}
	for (i = 0; i < numWindows; i++) {
		menuItem = ItemAddress(backWindow->MenuStrip, MENUITEM(VIEW_MENU, i + WINDOW_ITEM, NOSUB));
		menuItem->MutualExclude = ~(1 << i) << WINDOW_ITEM;
	}
}

/*
 *	Change window name in view menu
 */

void ChangeWindowItem(WindowPtr window)
{
	WORD i;
	DocDataPtr docData = GetWRefCon(window);

	if ((i = WindowNum(window)) == -1)
		return;
	SetMenuItem(window, MENUITEM(VIEW_MENU, i + WINDOW_ITEM, NOSUB), docData->WindowName);
}

/*
 *	Handle View menu command
 */

BOOL DoViewMenu(WindowPtr window, UWORD item, UWORD sub)
{
	WORD i;
	BOOL success;

	if (!IsDocWindow(window) &&
		((item >= SHOWBACKLAYERS_ITEM && item <= SHOWPAGE_ITEM) || item == DISPLAY_ITEM))
		return (FALSE);
	success = TRUE;
	switch (item) {
	case ABOUT_ITEM:
		DoHelp();
		break;
	case SHOWBACKLAYERS_ITEM:
		DoShowBackLayers(window);
		break;
	case SHOWRULER_ITEM:
		DoShowRuler(window);
		break;
	case SHOWGRID_ITEM:
		DoShowGridLines(window);
		break;
	case SHOWPAGE_ITEM:
		DoShowPageBreaks(window);
		break;
	case SHOWTOOLBOX_ITEM:
		DoShowExtraWindow(WINDOW_TOOL);
		break;
	case SHOWPENPALETTE_ITEM:
		DoShowExtraWindow(WINDOW_PEN);
		break;
	case SHOWFILLPALETTE_ITEM:
		DoShowExtraWindow(WINDOW_FILL);
		break;
	case DISPLAY_ITEM:
		success = DoDisplayMenu(window, sub);
		break;
	default:
		i = item - WINDOW_ITEM;
		if (i >= 0 && i < numWindows) {
			if (windowList[i]->WLayer->front)
				WindowToFront(windowList[i]);
			ActivateWindow(windowList[i]);
			Delay(5);				/* Wait for window to come to front */
			ToolToFront();
		}
		break;
	}
	SetViewMenu();
	return (success);
}
