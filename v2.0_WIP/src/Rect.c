/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Rectangle handling routines
 */

#include <exec/types.h>
#include <graphics/gfxmacros.h>
#include <intuition/intuition.h>

#include <proto/graphics.h>

#include <Toolbox/Memory.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Window.h>

#include "Draw.h"
#include "Proto.h"

/*
 *	External variables
 */

extern Defaults	defaults;

/*
 *	Local variables and definitions
 */

static RectObjPtr	growObj;			// Rect or Oval
static BOOL			wasMoved;
static WORD			growHandle;

/*
 *	Local prototypes
 */

static void	SetRectFrame(RectObjPtr);

static RectObjPtr	CreateRect(DocDataPtr, PointPtr, PointPtr);

static void	RectTrackCreate(WindowPtr, PointPtr, PointPtr, BOOL);
static void	RectConstrainCreate(PointPtr, PointPtr);

static void	RectTrackGrow(WindowPtr, PointPtr, PointPtr, BOOL);

/*
 *	Set rect frame from bounds and angle
 */

static void SetRectFrame(RectObjPtr rectObj)
{
	Point	pt;

	rectObj->DocObj.Frame = rectObj->Rect;
	GetRectCenter(&rectObj->Rect, &pt);
	RotateRect(&rectObj->DocObj.Frame, pt.x, pt.y, rectObj->Rotate);
}

/*
 *	Allocate a new rect object
 */

RectObjPtr RectAllocate()
{
	return ((RectObjPtr) MemAlloc(sizeof(RectObj), MEMF_CLEAR));
}

/*
 *	Dispose of rect object
 */

void RectDispose(RectObjPtr rectObj)
{
	MemFree(rectObj, sizeof(RectObj));
}

/*
 *	Create rect with given start and end positions
 */

static RectObjPtr CreateRect(DocDataPtr docData, PointPtr pt1, PointPtr pt2)
{
	register RectObjPtr	rectObj;

	if ((rectObj = (RectObjPtr) NewDocObject(CurrLayer(docData), TYPE_RECT)) == NULL)
		return (NULL);
	SetFrameRect(&rectObj->Rect, pt1->x, pt1->y, pt2->x, pt2->y);
	SetRectFrame(rectObj);
	rectObj->DocObj.Flags	= defaults.ObjFlags;
	rectObj->PenWidth		= defaults.PenWidth;
	rectObj->PenHeight		= defaults.PenHeight;
	rectObj->PenColor		= defaults.PenColor;
	CopyRGBPat8(&defaults.FillPat, &rectObj->FillPat);
	return (rectObj);
}

/*
 *	Draw rectangle object
 */

void RectDrawObj(RastPtr rPort, RectObjPtr rectObj, RectPtr dstRect, RectPtr clipRect)
{
	PolyObjPtr	polyObj;

	if ((polyObj = RectConvertToPoly(rectObj)) != NULL) {
		PolyDrawObj(rPort, polyObj, dstRect, clipRect);
		PolyDispose(polyObj);
	}
}

/*
 *	Draw outline of rect (for creation/dragging)
 *	Offset is in document coordinates
 */

void RectDrawOutline(WindowPtr window, RectObjPtr rectObj, WORD xOffset, WORD yOffset)
{
	PolyObjPtr	polyObj;

	if ((polyObj = RectConvertToPoly(rectObj)) != NULL) {
		PolyDrawOutline(window, polyObj, xOffset, yOffset);
		PolyDispose(polyObj);
	}
}

/*
 *	Draw handle in rotated position
 */

static void DrawRotateHandle(WindowPtr window, PointPtr pt, PointPtr cenPt, WORD angle)
{
	Point	newPt;

	newPt = *pt;
	RotatePoint(&newPt, cenPt->x, cenPt->y, angle);
	DrawHandle(window, newPt.x, newPt.y);
}

/*
 *	Draw selection handles for rect
 */

void RectHilite(WindowPtr window, RectObjPtr rectObj)
{
	WORD		angle;
	Point		pt, cenPt;
	Rectangle	rect;

	rect = rectObj->Rect;
	GetRectCenter(&rect, &cenPt);
	angle = rectObj->Rotate;
	pt.x = rect.MinX;	pt.y = rect.MinY;
	DrawRotateHandle(window, &pt, &cenPt, angle);
	if (rect.MaxY != rect.MinY) {
		pt.y = rect.MaxY;
		DrawRotateHandle(window, &pt, &cenPt, angle);
	}
	if (rect.MaxX != rect.MinX) {
		pt.x = rect.MaxX;	pt.y = rect.MinY;
		DrawRotateHandle(window, &pt, &cenPt, angle);
	}
	if (rect.MaxX != rect.MinX && rect.MaxY != rect.MinY) {
		pt.y = rect.MaxY;
		DrawRotateHandle(window, &pt, &cenPt, angle);
	}
}

/*
 *	Set rect pen color
 */

void RectSetPenColor(RectObjPtr rectObj, RGBColor penColor)
{
	rectObj->PenColor = penColor;
}

/*
 *	Set rect pen size
 *	If size is -1 then do not change
 */

void RectSetPenSize(RectObjPtr rectObj, WORD penWidth, WORD penHeight)
{
	if (penWidth != -1)
		rectObj->PenWidth = penWidth;
	if (penHeight != -1)
		rectObj->PenHeight = penHeight;
}

/*
 *	Set rect fill pattern
 */

void RectSetFillPat(RectObjPtr rectObj, RGBPat8Ptr fillPat)
{
	CopyRGBPat8(fillPat, &rectObj->FillPat);
}

/*
 *	Get rect fill pattern
 */

BOOL RectGetFillPat(RectObjPtr rectObj, RGBPat8Ptr fillPat)
{
	CopyRGBPat8(&rectObj->FillPat, fillPat);
	return (TRUE);
}

/*
 *	Offset rect by given amount
 */

void RectOffset(RectObjPtr rectObj, WORD xOffset, WORD yOffset)
{
	OffsetRect(&rectObj->Rect, xOffset, yOffset);
	SetRectFrame(rectObj);
}

/*
 *	Rotate rect by given angle about center
 */

void RectRotate(RectObjPtr rectObj, WORD cx, WORD cy, WORD angle)
{
	Point	pt1, pt2;

	rectObj->Rotate += angle;
	GetRectCenter(&rectObj->Rect, &pt1);
	pt2 = pt1;
	RotatePoint(&pt2, cx, cy, angle);
	OffsetRect(&rectObj->Rect, pt2.x - pt1.x, pt2.y - pt1.y);
	SetRectFrame(rectObj);
}

/*
 *	Flip rect horizontally or vertically about center
 */

void RectFlip(RectObjPtr rectObj, WORD cx, WORD cy, BOOL horiz)
{
	FlipRect(&rectObj->Rect, cx, cy, horiz);
	rectObj->Rotate = (horiz) ? 180 - rectObj->Rotate : -rectObj->Rotate;
	SetRectFrame(rectObj);
}

/*
 *	Scale rect to new size
 */

void RectScale(RectObjPtr rectObj, RectPtr rect)
{
	rectObj->Rect = *rect;
	SetRectFrame(rectObj);
}

/*
 *	Create poly equivalent to given rect
 *	Returns pointer to poly, or NULL if error
 */

PolyObjPtr RectConvertToPoly(RectObjPtr rectObj)
{
	PolyObjPtr	polyObj;
	Point		pt;
	Rectangle	rect;

	if ((polyObj = (PolyObjPtr) NewDocObject(NULL, TYPE_POLY)) == NULL)
		return (NULL);
	rect = polyObj->DocObj.Frame = rectObj->Rect;
	OffsetRect(&rect, -rect.MinX, -rect.MinY);
	if (!PolyAddPoint(polyObj, 0, 0x7FFF, rect.MinX, rect.MinY) ||
		!PolyAddPoint(polyObj, 0, 0x7FFF, rect.MaxX, rect.MinY) ||
		!PolyAddPoint(polyObj, 0, 0x7FFF, rect.MaxX, rect.MaxY) ||
		!PolyAddPoint(polyObj, 0, 0x7FFF, rect.MinX, rect.MaxY)) {
		DisposeDocObject(polyObj);
		return (NULL);
	}
	PolyAdjustFrame(polyObj);
	if (rectObj->Rotate) {
		GetRectCenter(&rectObj->Rect, &pt);
		PolyRotate(polyObj, pt.x, pt.y, rectObj->Rotate);
	}
	PolySetPenColor(polyObj, rectObj->PenColor);
	PolySetPenSize(polyObj, rectObj->PenWidth, rectObj->PenHeight);
	PolySetFillPat(polyObj, &rectObj->FillPat);
	polyObj->DocObj.Flags = rectObj->DocObj.Flags;
	PolySetSmooth(polyObj, FALSE);
	PolySetClosed(polyObj, TRUE);
	return (polyObj);
}

/*
 *	Determine if point is in rect
 */

BOOL RectSelect(RectObjPtr rectObj, PointPtr pt)
{
	BOOL		select = FALSE;
	PolyObjPtr	polyObj;

	if ((polyObj = RectConvertToPoly(rectObj)) != NULL) {
		select = PolySelect(polyObj, pt);
		PolyDispose(polyObj);
	}
	return (select);
}

/*
 *	Return handle number of given point, or -1 if not in handle
 */

WORD RectHandle(RectObjPtr rectObj, PointPtr pt, RectPtr handleRect)
{
	Point		newPt, cenPt, handlePt;
	Rectangle	rect;

	rect = rectObj->Rect;
	GetRectCenter(&rect, &cenPt);
	newPt = *pt;
	RotatePoint(&newPt, cenPt.x, cenPt.y, -rectObj->Rotate);
	handlePt.x = rect.MinX;	handlePt.y = rect.MinY;
	if (InHandle(&newPt, &handlePt, handleRect))
		return (0);
	handlePt.x = rect.MaxX;
	if (InHandle(&newPt, &handlePt, handleRect))
		return (1);
	handlePt.x = rect.MinX;	handlePt.y = rect.MaxY;
	if (InHandle(&newPt, &handlePt, handleRect))
		return (2);
	handlePt.x = rect.MaxX;
	if (InHandle(&newPt, &handlePt, handleRect))
		return (3);
	return (-1);
}

/*
 *	Duplicate rect data to new object
 *	Return success status
 */

BOOL RectDupData(RectObjPtr rectObj, RectObjPtr newObj)
{
	newObj->Rect		= rectObj->Rect;
	newObj->Rotate		= rectObj->Rotate;
	newObj->PenWidth	= rectObj->PenWidth;
	newObj->PenHeight	= rectObj->PenHeight;
	newObj->PenColor	= rectObj->PenColor;
	CopyRGBPat8(&rectObj->FillPat, &newObj->FillPat);
	return (TRUE);
}

/*
 *	Draw routine for tracking rectangle creation
 */

static void RectTrackCreate(WindowPtr window, PointPtr pt1, PointPtr pt2, BOOL change)
{
	RectObj	rectObj;

	if (change) {
		BlockClear(&rectObj, sizeof(RectObj));
		SetFrameRect(&rectObj.Rect, pt1->x, pt1->y, pt2->x, pt2->y);
		SetRectFrame(&rectObj);
		RectDrawOutline(window, &rectObj, 0, 0);
	}
}

/*
 *	Constrain routine for rect creation
 */

static void RectConstrainCreate(PointPtr pt1, PointPtr pt2)
{
	WORD	width, height, min;

	width = ABS(pt2->x - pt1->x);
	height = ABS(pt2->y - pt1->y);
	min = MIN(width, height);
	pt2->x = (pt2->x >= pt1->x) ? pt1->x + min : pt1->x - min;
	pt2->y = (pt2->y >= pt1->y) ? pt1->y + min : pt1->y - min;
}

/*
 *	Create rectangle object
 */

DocObjPtr RectCreate(WindowPtr window, UWORD modifier, WORD mouseX, WORD mouseY)
{
	DocDataPtr	docData = GetWRefCon(window);
	Point		pt1, pt2;

/*
	Track object size
*/
	WindowToDoc(window, mouseX, mouseY, &pt1.x, &pt1.y);
	SnapToGrid(docData, &pt1.x, &pt1.y);
	pt2 = pt1;
	TrackMouse(window, modifier, &pt2, RectTrackCreate, RectConstrainCreate);
/*
	Create rect
*/
	return (CreateRect(docData, &pt1, &pt2));
}

/*
 *	Set new rect for moved handle
 */

static void GetNewRect(RectObjPtr rectObj, RectPtr rect, PointPtr pt, WORD handle)
{
	Point	ptMin, ptMax, cenPt;

	*rect = rectObj->Rect;
	ptMin.x = rect->MinX;	ptMin.y = rect->MinY;
	ptMax.x = rect->MaxX;	ptMax.y = rect->MaxY;
	GetRectCenter(rect, &cenPt);
	RotatePoint(&ptMin, cenPt.x, cenPt.y, rectObj->Rotate);
	RotatePoint(&ptMax, cenPt.x, cenPt.y, rectObj->Rotate);
	if (handle == 0 || handle == 2)
		ptMin.x = pt->x;
	else
		ptMax.x = pt->x;
	if (handle == 0 || handle == 1)
		ptMin.y = pt->y;
	else
		ptMax.y = pt->y;
	cenPt.x = (ptMin.x + ptMax.x)/2;
	cenPt.y = (ptMin.y + ptMax.y)/2;
	RotatePoint(&ptMin, cenPt.x, cenPt.y, -rectObj->Rotate);
	RotatePoint(&ptMax, cenPt.x, cenPt.y, -rectObj->Rotate);
	SetRect(rect, ptMin.x, ptMin.y, ptMax.x, ptMax.y);
}

/*
 *	Draw routine for tracking rect grow
 */

static void RectTrackGrow(WindowPtr window, PointPtr pt1, PointPtr pt2, BOOL change)
{
	RectObj		rectObj;
	Rectangle	rect;

	if (change && (!EqualPt(pt1, pt2) || wasMoved)) {
		rectObj = *growObj;
		GetNewRect(&rectObj, &rect, pt2, growHandle);
		rectObj.Rect = rect;
		SetRectFrame(&rectObj);
		DrawObjectOutline(window, &rectObj, 0, 0);
		wasMoved = TRUE;
	}
}

/*
 *	Track mouse for rectangular object and return new frame
 */

void TrackRectGrow(WindowPtr window, RectObjPtr rectObj, UWORD modifier, WORD handle,
				   RectPtr rect)
{
	DocDataPtr	docData = GetWRefCon(window);
	Point		pt1, pt2, cenPt;

	growObj = rectObj;
	growHandle = handle;
/*
	Track handle movement
*/
	pt1.x = (growHandle == 0 || growHandle == 2) ?
			rectObj->Rect.MinX : rectObj->Rect.MaxX;
	pt1.y = (growHandle == 1 || growHandle == 3) ?
			rectObj->Rect.MinY : rectObj->Rect.MaxY;
	GetRectCenter(&rectObj->Rect, &cenPt);
	RotatePoint(&pt1, cenPt.x, cenPt.y, rectObj->Rotate);
	pt2 = pt1;
	SnapToGrid(docData, &pt2.x, &pt2.y);
	wasMoved = FALSE;
	TrackMouse(window, modifier, &pt2, RectTrackGrow, NULL);
/*
	Set rect dimensions
*/
	if (!EqualPt(&pt1, &pt2))
		GetNewRect(rectObj, rect, &pt2, growHandle);
	else
		*rect = rectObj->Rect;
}

/*
 *	Track mouse and change rect shape
 */

void RectGrow(WindowPtr window, RectObjPtr rectObj, UWORD modifier, WORD handle)
{
	Rectangle	rect;

	TrackRectGrow(window, rectObj, modifier, handle, &rect);
/*
	Set new object dimensions
*/
	if (!EqualRect(&rectObj->Rect, &rect)) {
		InvalObjectRect(window, rectObj);
		HiliteSelectOff(window);
		rectObj->Rect = rect;
		SetRectFrame(rectObj);
		HiliteSelectOn(window);
		InvalObjectRect(window, rectObj);
	}
}

/*
 *	Draw rect object to PostScript file
 */

BOOL RectDrawPSObj(PrintRecPtr printRec, RectObjPtr rectObj, RectPtr rect, RectPtr clipRect)
{
	BOOL	hasPen, hasFill;
	Point	pen;

	hasPen = HasPen(rectObj);
	hasFill = HasFill(rectObj);
	if (hasPen || hasFill) {
		if (hasFill) {
			if (!PSSetFill(printRec, &rectObj->FillPat))
				return (FALSE);
		}
		if (hasPen) {
			pen.x = rectObj->PenWidth;
			pen.y = rectObj->PenHeight;
			PSScalePt(&pen, &rectObj->DocObj.Frame, rect);
			if (!PSSetPen(printRec, &pen, rectObj->PenColor))
				return (FALSE);
		}
		if (!PSRect(printRec, rect, hasPen, hasFill))
			return (FALSE);
	}
	return (TRUE);
}

/*
 *	Create new rect given REXX arguments
 */

RectObjPtr RectNewREXX(DocDataPtr docData, TextPtr args)
{
	Point	pt1, pt2;

/*
	Get start and end points
*/
	args = GetArgPoint(args, docData, &pt1);
	if (args)
		args = GetArgPoint(args, docData, &pt2);
	if (args == NULL ||
		pt1.x < 0 || pt1.y < 0 || pt1.x >= docData->DocWidth || pt1.y >= docData->DocHeight ||
		pt2.x < 0 || pt2.y < 0 || pt2.x >= docData->DocWidth || pt2.y >= docData->DocHeight)
		return (NULL);
/*
	Create rect
*/
	return (CreateRect(docData, &pt1, &pt2));
}
