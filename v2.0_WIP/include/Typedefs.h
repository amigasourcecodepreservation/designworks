/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Misc type definitions
 */

#ifndef TYPEDEFS_H
#define TYPEDEFS_H

typedef void	*Ptr, **Handle;
typedef UBYTE	TextChar, *TextPtr;

typedef LONG	File, Dir;

typedef struct tPoint		*PointPtr;
typedef struct RastPort		RastPort, *RastPtr;
typedef struct Layer		Layer, *LayerPtr;
typedef struct Layer_Info	Layer_Info, *LayerInfoPtr;
typedef struct BitMap		BitMap, *BitMapPtr;
typedef struct ColorMap		ColorMap, *ColorMapPtr;
typedef struct Region		Region, *RegionPtr;
typedef struct TextAttr		TextAttr, *TextAttrPtr;
typedef struct TextFont		TextFont, *TextFontPtr;
typedef struct Rectangle	Rectangle, *RectPtr;

typedef struct Screen		Screen,  *ScreenPtr;
typedef struct Window		Window,  *WindowPtr;
typedef struct Requester	*RequestPtr;
typedef struct Gadget		Gadget, *GadgetPtr;
typedef struct PropInfo		PropInfo, *PropInfoPtr;
typedef struct IntuiText	IntuiText, *IntuiTextPtr;
typedef struct StringInfo	StringInfo, *StrInfoPtr;
typedef struct Image		Image, *ImagePtr;
typedef struct Border		Border, *BorderPtr;
typedef struct Menu			Menu, *MenuPtr;
typedef struct MenuItem		MenuItem, *MenuItemPtr;
typedef struct IntuiMessage	IntuiMessage, *IntuiMsgPtr;

typedef struct MsgPort		MsgPort, *MsgPortPtr;
typedef struct Message		Message, *MsgPtr;
typedef struct IORequest	IORequest, *IOReqPtr;
typedef struct Node			Node, *NodePtr;
typedef struct Task			Task, *TaskPtr;
typedef struct Process		Process, *ProcessPtr;

#endif
