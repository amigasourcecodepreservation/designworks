/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991-92 New Horizons Software, Inc.
 */

#ifndef DEVICES_PRINTER_H
#include <devices/printer.h>
#endif

#ifndef TYPEDEFS_H
#include <TypeDefs.h>
#endif

#include <IFF/DRAW.h>

#ifndef HOTLINKS_HOTLINKS_H
#include <hotlinks/hotlinks.h>
#endif

#include "Print.h"

/*
 *	Useful macros
 */

#define DocModified(docData)	((docData)->Flags |= DOC_MODIFIED)

#define SelectObject(docObj)	(((DocObjPtr) (docObj))->Flags |= OBJ_SELECTED)
#define UnSelectObject(docObj)	(((DocObjPtr) (docObj))->Flags &= ~OBJ_SELECTED)
#define ObjectSelected(docObj)	(((DocObjPtr) (docObj))->Flags & OBJ_SELECTED)

#define LockObject(docObj)		(((DocObjPtr) (docObj))->Flags |= OBJ_LOCKED)
#define UnLockObject(docObj)	(((DocObjPtr) (docObj))->Flags &= ~OBJ_LOCKED)
#define ObjectLocked(docObj)	(((DocObjPtr) (docObj))->Flags & OBJ_LOCKED)

#define HasPen(docObj)			(((DocObjPtr) (docObj))->Flags & OBJ_DO_PEN)

#define HasFill(docObj)			(((DocObjPtr) (docObj))->Flags & OBJ_DO_FILL)

#define PolyMerged(polyObj)		((polyObj)->NumPaths > 1)
#define PolySmoothed(polyObj)	((polyObj)->PolyFlags & POLY_SMOOTH)
#define PolyClosed(polyObj)		((polyObj)->PolyFlags & POLY_CLOSED)

#define ShowLayer(docLayer)		((docLayer)->Flags |= LAYER_VISIBLE)
#define HideLayer(docLayer)		((docLayer)->Flags &= ~LAYER_VISIBLE)
#define LayerVisible(docLayer)	((docLayer)->Flags & LAYER_VISIBLE)

#define CopyRGBPat8(src, dst)	(BlockMove((src), (dst), sizeof(RGBPat8)))

/*
 *	Note: intuition uses the name NextObject()
 */

#define NextObj(docObj)	(((DocObjPtr) (docObj))->Next)
#define PrevObj(docObj)	(((DocObjPtr) (docObj))->Prev)

typedef struct PubBlock PubBlock,	*PubBlockPtr;
typedef struct PubRecord PubRecord,	*PubRecordPtr;

/*
 *	Default data
 */

typedef struct Defaults {
	PrintRecord	PrintRec;
	UWORD		DocFlags;
	UWORD		ObjFlags;
	UWORD		LineFlags;			// For default arrows
	FontNum		FontNum;
	FontSize	FontSize;
	UBYTE		Style, MiscStyle;
	UBYTE		Justify, Spacing;
	UBYTE		PenWidth, PenHeight;
	RGBColor	PenColor;
	RGBPat8		FillPat;
	UBYTE		GridUnitsIndex;
	TextChar	PenName[30];
	TextChar	FillName[30];
	UBYTE		pad[39];
} Defaults, *DefaultsPtr;

/*
 *	Doc objects
 *	All measurements are in screen dots, unless otherwise noted
 */

enum {
	TYPE_GROUP,	TYPE_TEXT,	TYPE_BMAP,	TYPE_LINE,
	TYPE_RECT,	TYPE_OVAL,	TYPE_POLY,	TYPE_EPSF
};

#define TYPE_MAXTYPE	TYPE_EPSF

typedef struct DocObject {
	WORD		Type;
	UWORD		Flags;

	struct DocObject	*Next, *Prev;
	struct GroupObj		*Group;		// NULL if not part of a group

	Rectangle	Frame;				// Location and size, relative to group origin
} DocObject, *DocObjPtr;

typedef struct GroupObj {
	struct DocObject	DocObj;

	struct DocObject	*Objects;
} GroupObj, *GroupObjPtr;

typedef struct LineObj {
	struct DocObject	DocObj;

	UWORD		LineFlags;
	UBYTE		PenWidth, PenHeight;
	RGBColor	PenColor;
	BYTE		X, Y;				// 0 or 1 for start point relative to frame
} LineObj, *LineObjPtr;

typedef struct RectObj {
	struct DocObject	DocObj;

	Rectangle	Rect;				// Unrotated bounding rect
	WORD		Rotate;				// Degrees clockwise around center of Rect
	UBYTE		PenWidth, PenHeight;
	RGBColor	PenColor;
	RGBPat8		FillPat;
} RectObj, *RectObjPtr, OvalObj, *OvalObjPtr;

typedef struct PolyObj {
	struct DocObject	DocObj;

	UWORD		PolyFlags;
	UBYTE		PenWidth, PenHeight;
	RGBColor	PenColor;
	RGBPat8		FillPat;
	UWORD		NumPaths;
	WORD		*NumPoints;
	PointPtr	*Points;
} PolyObj, *PolyObjPtr;

typedef struct TextObj {
	struct DocObject	DocObj;

	UWORD		TextFlags;
	WORD		Rotate;				// Degrees clockwise around center of frame
	RGBColor	PenColor;
	RGBPat8		FillPat;
	TextPtr		Text;				// Pointer to TextLen characters
	WORD		TextLen;
	WORD		SelStart, SelEnd;	// Used when text is selected
	WORD		DragStart;			// Used when selecting text
	FontNum		FontNum;
	FontSize	FontSize;
	UBYTE		Style, MiscStyle;
	UBYTE		Justify, Spacing;
	WORD		NumLines;
	WORD		*LineStarts;		// NumLines + 1 entries
									// First entry is 0, last is text length
} TextObj, *TextObjPtr;

typedef struct BMapObj {
	struct DocObject	DocObj;

	WORD				Rotate;			// Degrees clockwise around center of frame
	Rectangle			Bounds;			// Bounds of un-cropped picture
	WORD				Width, Height;
	RGBColorPtr			Data;			// Width*Height color values
	BitMapPtr			PictCache;		// Cache of screen version, or NULL
	PLANEPTR			Mask;			// Cache of mask, or NULL
	struct DateStamp	FileDate;		// Date file last modified for link
	TextPtr				FileName;		// Name of link file
} BMapObj, *BMapObjPtr;

typedef struct EPSFObj {
	struct DocObject	DocObj;

	UWORD				EPSFFlags;
	WORD				Rotate;			// Degrees clockwise around center of frame
	Rectangle			BoundingBox; 	// Original bounding box
	ULONG				FileSize;		// Size of buffer
	TextPtr				Title;			// Title of EPS File
	TextPtr				CreationDate;	// Date EPS File was created
	TextPtr				Creator;		// What application created the EPS File
	BitMapPtr			InfoBitMap;		// Bitmap containing info displayed in rect
	WORD				InfoWidth;		// Width of InfoBitMap
	WORD				InfoHeight;		// Height of InfoBitMap
	struct DateStamp	FileDate;		// Date file last modified for link
	TextPtr				FileName;		// Name of file imported from for link
	UBYTE				*File;			// Buffer containing EPS File
} EPSFObj, *EPSFObjPtr;

/*
 *	IFF picture data (used when converting iff pictures to BMapObj's)
 */

typedef struct IFFPict {
	WORD			Width, Height;
	LONG			ViewMode;			// View mode (HAM, etc.)
	WORD			TranspColor;		// Transparent color, or -1 if none
	BitMapPtr		BitMap;
	PLANEPTR		Mask;				// NULL if no mask
	WORD			NumColors;
	RGBColor		ColorTable[256];	// Up to eight planes
} IFFPict, *IFFPictPtr;

/*
 *	Layers
 */

typedef struct DocLayer {
	struct DocLayer	*Next, *Prev;

	ULONG			Flags;			// Flags from above defines

	DocObjPtr		Objects;		// Objects in layer, back-to-front order
	TextPtr			Name;			// Layer name (or NULL if none)
} DocLayer, *DocLayerPtr;

/*
 *	Pen Color structure for named colors
 */

typedef struct PenColor {
	RGBColor		Color;
	TextPtr			Name;
	struct PenColor *Next;
} PenColor, *PenColorPtr;

/*
 *	Fill Pattern structure for named colors
 */

typedef struct FillPattern {
	RGBPat8				Pattern;
	TextPtr				Name;
	struct FillPattern  *Next;
} FillPattern, *FillPtnPtr;

/*
 *	Document window data structure
 *	Pointed to by UserData field of Window structure
 */

#define DOC_MODIFIED	0x0001	// Data has been modified
#define DOC_SHOWRULER	0x0002
#define DOC_SHOWGRID	0x0004	// Show grid lines
#define DOC_SHOWPAGE	0x0008	// Show page breaks
#define DOC_GRIDSNAP	0x0010
#define DOC_RULERINDIC	0x0020	// Ruler indicators are on
#define DOC_HILITE		0x0040	// Selection hilight is on
#define DOC_DRAGGING	0x0080	// Currently dragging an object
#define DOC_HIDEBACKLAY	0x0100	// Hide back layers
#define DOC_CURSOR		0x0200	// Text cursor is on

#define SCALE_FULL	FIXED_UNITY
#define SCALE_MAX	(SCALE_FULL << 2)
#define SCALE_MIN	(SCALE_FULL >> 5)

#define PagesAcross(docData)	(((docData)->DocWidth - 1)/(docData)->PageWidth + 1)
#define PagesDown(docData)		(((docData)->DocHeight - 1)/(docData)->PageHeight + 1)

typedef struct DocData {
	ULONG		Flags;				// Flags from above defines
	DocLayerPtr	CurrLayer;			// Current layer
	DocLayerPtr	Layers;				// All layers, back-to-front order

	WindowPtr	Window;				// Owner window

	PrintRecPtr	PrintRec;

	WORD		TopOffset;			// Scroll offset from top
	WORD		LeftOffset;			// Scroll offset from left

	Fixed		Scale;				// Reduction/enlargement
	WORD		GridUnitsIndex;		// Index into grid spacing table
	Point		RulerOffset;		// Ruler zero-point offset
	Rectangle	RulerIndic;			// Location of ruler indicator
									// If not dragging, only show MinX, MinY

	WORD		xDPI, yDPI;			// x and y dots per inch
	WORD		PageWidth, PageHeight;	// In pixels
	WORD		DocWidth, DocHeight;	// In pixels

	WORD		BlinkCount;			// Countdown timer for cursor blink

	ULONG		LastSaveTime;		// System seconds of last save

	TextChar	FileName[32];		// Name of file
	Dir			DirLock;			// Lock of file directory
	TextChar	WindowName[32];		// Without layer name appended
	struct PubBlock		*PubBlock;	// hotlinks publish block
	BOOL				HasLock;	// does picture have read lock or if
									// hotlinks not present then did document have link
	WORD		EditionVersion;
	BOOL		GetUpdates;
} DocData, *DocDataPtr;
