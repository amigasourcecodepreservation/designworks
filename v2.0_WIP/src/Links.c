/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991-92 New Horizons Software, Inc.
 *	All rights reserved
 *
 *	Local link routines
 */

#include <exec/types.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/dos.h>

#include <Toolbox/Utility.h>
#include <Toolbox/ScrollList.h>
#include <Toolbox/DOS.h>
#include <Toolbox/DateTime.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/StdFile.h>
#include <Toolbox/Window.h>

#include <string.h>

#include "Draw.h"
#include "Proto.h"

/*
	external variables
*/

extern MsgPortPtr	mainMsgPort;
extern ScreenPtr	screen;

extern TextChar	strBuff[];

extern TextChar	strILBM[], strEPSF[], strBytes[];

extern DlgTemplPtr	dlgList[];

/*
	local variables
*/

static DialogPtr linkDlg;

static ScrollListPtr	linkScrollList = NULL;

enum {
	DONE_BUTTON,
	INFO_BUTTON,
	BREAK_BUTTON,
	LINK_BOX,
	LINK_UP,
	LINK_DOWN,
	LINK_SCROLL
};

enum {
	NAME_TEXT = 1,
	PATH_TEXT,
	SIZE_TEXT,
	KIND_TEXT,
	DATE_TEXT
};

/*
	structure for list of linked pictures
*/

typedef struct LinkObj {
	DocObjPtr	DocObj;
	Dir			DirLock;
	struct LinkObj	*Next, *Prev;
} LinkObj, *LinkObjPtr;

static LinkObjPtr	linkedPicts = NULL;

/*
 *	local prototypes
 */

static void	DisposeLinkedPicts(void);
static BOOL	GetLinkList(DocDataPtr, ScrollListPtr);
static void RemoveLink(ScrollListPtr, WORD);
static void DoLinkInfo(ScrollListPtr, WORD);
static void SetLinkButtons(DialogPtr, ScrollListPtr);
static void LinkDrawProc(RastPtr, TextPtr, RectPtr);
static BOOL LinkDialogFilter(IntuiMsgPtr, WORD *);

/*
 *	Dispose of memory used by list of links
 */

static void DisposeLinkedPicts()
{
	LinkObjPtr	link, nextLink;

	link = linkedPicts;
	while (link) {
		nextLink = link->Next;
		UnLock(link->DirLock);
		MemFree(link, sizeof(LinkObj));
		link = nextLink;
	}
	linkedPicts = NULL;
}

/*
 *	build list of pictures with links
 */

static BOOL GetLinkList(DocDataPtr docData, ScrollListPtr scrollList)
{
	DocLayerPtr	layer;
	DocObjPtr	docObj;
	BOOL		success;
	LinkObjPtr	link, lastLink = NULL;

	SLDoDraw(scrollList, FALSE);
	for (layer = TopLayer(docData); layer; layer = PrevLayer(layer)) {
		for (docObj = TopObject(layer); docObj; docObj = PrevObj(docObj)) {
			success = FALSE;
			if (docObj->Type == TYPE_BMAP && ((BMapObjPtr) docObj)->FileName) {
				strcpy(strBuff, ((BMapObjPtr) docObj)->FileName);
				success = TRUE;
			}
			else if (docObj->Type == TYPE_EPSF && ((EPSFObjPtr) docObj)->FileName) {
				strcpy(strBuff, ((EPSFObjPtr) docObj)->FileName);
				success = TRUE;
			}
			if (success) {
				if ((link = MemAlloc(sizeof(LinkObj), MEMF_CLEAR)) == NULL)
					return (FALSE);
				link->DirLock = ConvertFileName(strBuff);
				if (docObj->Type == TYPE_BMAP)
					strcat(strBuff, strILBM);
				else
					strcat(strBuff, strEPSF);
				strBuff[0] = toUpper[strBuff[0]];
				SLAddItem(scrollList, strBuff, strlen(strBuff), SLNumItems(scrollList));
				link->DocObj = docObj;
				if (linkedPicts) {
					link->Prev = lastLink;
					lastLink->Next = link;
				}
				else
					linkedPicts = link;
				lastLink = link;
			}
		}
	}
	SLDoDraw(scrollList, TRUE);
	return (TRUE);
}

/*
 *	static remove linked picture
 */

static void RemoveLink(ScrollListPtr scrollList, WORD fileNum)
{
	LinkObjPtr	link;
	DocObjPtr	docObj;
	WORD		i;

	link = linkedPicts;
	for (i = 0; i < fileNum; i++)
		link = link->Next;
/*
	remove from linked list
*/
	if (link->Prev)
		link->Prev->Next = link->Next;
	else
		linkedPicts = link->Next;
	if (link->Next)
		link->Next->Prev = link->Prev;
/*
	remove link from object
*/
	docObj = link->DocObj;
	if (docObj->Type == TYPE_BMAP) {
		MemFree(((BMapObjPtr) docObj)->FileName, strlen(((BMapObjPtr) docObj)->FileName) + 1);
		((BMapObjPtr) docObj)->FileName = NULL;
	}
	else {
		MemFree(((EPSFObjPtr) docObj)->FileName, strlen(((EPSFObjPtr) docObj)->FileName) + 1);
		((EPSFObjPtr) docObj)->FileName = NULL;
	}
	UnLock(link->DirLock);
	MemFree(link, sizeof(LinkObj));
/*
	remove from scrollList
*/
	SLRemoveItem(scrollList, fileNum);
	SLDrawList(scrollList);
}

/*
 *	open dialog with link information
 */

static void DoLinkInfo(ScrollListPtr scrollList, WORD fileNum)
{
	LinkObjPtr				link;
	register WORD			i, j, len, frac;
	struct FileInfoBlock	*fib;
	Dir						origDir, lock;
	TextChar				fileName[256], pathText[256], sizeText[50],
							dateText[50], timeText[10];
	BOOL					isBMAP;
	DialogPtr				dlg;

	if ((fib = MemAlloc(sizeof(struct FileInfoBlock), MEMF_CLEAR)) == NULL) {
		Error(ERR_NO_MEM);
		return;
	}
	link = linkedPicts;
	for (i = 0; i < fileNum; i++)
		link = link->Next;

	isBMAP = (link->DocObj->Type == TYPE_BMAP);
	SLGetItem(scrollList, fileNum, fileName);
	fileName[strlen(fileName) - 4] = '\0';
	NameFromLock1(link->DirLock, pathText, 256);

	origDir = CurrentDir(link->DirLock);
	if ((lock = Lock(fileName, ACCESS_READ)) == NULL ||
		!Examine(lock, fib))
		goto Exit;
/*
	get size text
*/
	NumToString(fib->fib_Size, sizeText);
	len = strlen(sizeText);
	strcpy(strBuff, sizeText);
	if ((frac = len % 3) != 0)
		BlockMove(strBuff, sizeText, frac);
	j = frac;
	for (i = frac; i < len; i += 3) {
		if (frac || i > frac) {
			sizeText[j++] = ',';
		}
		BlockMove(strBuff + i, sizeText + j, 3);
		j += 3;
	}
	sizeText[j] = '\0';
	strcat(sizeText, strBytes);
/*
	get date text
*/
	DateString(&(fib->fib_Date), DATE_ABBR, dateText);
	strcat(dateText,", ");
	TimeString(&(fib->fib_Date), FALSE, TRUE, timeText);
	strcat(dateText, timeText);

	dlgList[DLG_LINKINFO]->Gadgets[NAME_TEXT].Info = fileName;
	dlgList[DLG_LINKINFO]->Gadgets[PATH_TEXT].Info = pathText;
	dlgList[DLG_LINKINFO]->Gadgets[SIZE_TEXT].Info = sizeText;
	dlgList[DLG_LINKINFO]->Gadgets[KIND_TEXT].Info = (isBMAP) ? strILBM : strEPSF;
	dlgList[DLG_LINKINFO]->Gadgets[DATE_TEXT].Info = dateText;

	if ((dlg = GetDialog(dlgList[DLG_LINKINFO], screen, mainMsgPort)) == NULL) {
		Error(ERR_NO_MEM);
		goto Exit;
	}
	OutlineOKButton(dlg);
	(void)ModalDialog(mainMsgPort, dlg, DialogFilter);
	DisposeDialog(dlg);

Exit:
	MemFree(fib, sizeof(struct FileInfoBlock));
	if (origDir)
		CurrentDir(origDir);
	if (lock)
		UnLock(lock);
}

/*
 *	set link dialog buttons
 */

static void SetLinkButtons(DialogPtr dlg, ScrollListPtr scrollList)
{
	EnableGadgetItem(dlg->FirstGadget, BREAK_BUTTON, dlg, NULL, (SLNextSelect(scrollList, -1) != -1));
	EnableGadgetItem(dlg->FirstGadget, INFO_BUTTON, dlg, NULL, (SLNextSelect(scrollList, -1) != -1));
}

/*
 *	procedure for drawing linked picture scrollList
 */

static void LinkDrawProc(RastPtr rPort, register TextPtr text, RectPtr rect)
{
	register TextPtr	type;
	register WORD		newWidth, typeWidth;

	type = &text[strlen(text) - 4];
	typeWidth = 40;  // TextLength(rPort, type, 4);
	Move(rPort, rect->MaxX - typeWidth, rPort->cp_y);
	TextInWidth(rPort, type, 4, typeWidth, FALSE);

	newWidth = rect->MaxX - rect->MinX + 1 - typeWidth - 20;
	Move(rPort, rect->MinX + 5, rPort->cp_y);
	TextInWidth(rPort, text, strlen(text) - 4, newWidth, FALSE);
}

/*
 *	Link dialog filter
 */

static BOOL LinkDialogFilter(register IntuiMsgPtr intuiMsg, WORD *item)
{
	WORD			gadgNum;
	ULONG			class = intuiMsg->Class;
	UWORD			code = intuiMsg->Code;

	if (intuiMsg->IDCMPWindow == linkDlg) {
		switch (class) {
/*
	Handle gadget down of up/down scroll arrows
*/
		case GADGETDOWN:
		case GADGETUP:
			gadgNum = GadgetNumber((GadgetPtr) intuiMsg->IAddress);
			if (gadgNum == LINK_BOX || gadgNum == LINK_SCROLL ||
				gadgNum == LINK_UP || gadgNum == LINK_DOWN) {
				SLGadgetMessage(linkScrollList, mainMsgPort, intuiMsg);
				*item = gadgNum;
				return (TRUE);
			}
			break;
/*
	Handle keyboard events
*/
		case RAWKEY:
			if (code == CURSORUP || code == CURSORDOWN) {
				ReplyMsg((MsgPtr) intuiMsg);
				SLCursorKey(linkScrollList, code);
				return (TRUE);
			}
			break;
		}
	}
	return (DialogFilter(intuiMsg, item));
}

/*
 *	open link dialog
 */

BOOL DoLink(WindowPtr window)
{
	WORD 			fileNum, item;
	GadgetPtr		gadgList;
	DocDataPtr		docData = GetWRefCon(window);
	BOOL			success;

	if ((linkScrollList = NewScrollList(SL_SINGLESELECT)) == NULL) {
		Error(ERR_NO_MEM);
		return (FALSE);
	}
	success = FALSE;
/*
	Get dialog
*/
	BeginWait();
	if ((linkDlg = GetDialog(dlgList[DLG_LINK], screen, mainMsgPort)) == NULL) {
		EndWait();
		Error(ERR_NO_MEM);
		goto Exit;
	}
	gadgList = GadgetItem(linkDlg->FirstGadget, 0);
	InitScrollList(linkScrollList, GadgetItem(gadgList, LINK_BOX), linkDlg, NULL);
	SLSetDrawProc(linkScrollList, LinkDrawProc);
	SLDrawBorder(linkScrollList);
	OutlineOKButton(linkDlg);
	SetLinkButtons(linkDlg, linkScrollList);
	if (!GetLinkList(docData, linkScrollList))
		goto Exit;
/*
	Handle dialog
*/
	success = TRUE;
	for (;;) {
		item = ModalDialog(mainMsgPort, linkDlg, LinkDialogFilter);
		if (item == DONE_BUTTON)
			break;
		switch (item) {
			case LINK_BOX :
				if (!SLIsDoubleClick(linkScrollList))
					break;
			case BREAK_BUTTON:
				fileNum = SLNextSelect(linkScrollList, -1);
				RemoveLink(linkScrollList, fileNum);
				DocModified(docData);
				break;
			case INFO_BUTTON:
				fileNum = SLNextSelect(linkScrollList, -1);
				DoLinkInfo(linkScrollList, fileNum);
				break;
		}
		SetLinkButtons(linkDlg, linkScrollList);
	}
Exit:
	if (linkDlg)
		DisposeDialog(linkDlg);
	EndWait();
	if (linkScrollList)
		DisposeScrollList(linkScrollList);
	DisposeLinkedPicts();
	return (success);
}