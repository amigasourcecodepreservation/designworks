/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Toolbox library
 *	Copyright (c) 1990 New Horizons Software
 *
 *	Misc utility routines
 */

#ifndef TOOLBOX_UTILITY_H
#define TOOLBOX_UTILITY_H

#ifndef INTUITION_INTUITION_H
#include <intuition/intuition.h>
#endif

#ifndef LIBRARTES_DOS_H
#include <libraries/dos.h>
#endif

#ifndef TYPEDEFS_H
#include "typedefs.h"
#endif

/*
 *	System versions
 */

#define OSVERSION_1_2	33
#define OSVERSION_1_3	34
#define OSVERSION_2_0	36
#define OSVERSION_2_0_4	37

/*
 *	Pointer types
 */

#define POINTER_ARROW		0
#define POINTER_IBEAM		1
#define POINTER_CROSS		2
#define POINTER_PLUS		3
#define POINTER_WAIT		4

#define POINTER_MAX_TYPE	4

/*
 *	Image types
 */

#define IMAGE_UP_ARROW		0
#define IMAGE_DOWN_ARROW	1
#define IMAGE_LEFT_ARROW	2
#define IMAGE_RIGHT_ARROW	3
#define IMAGE_STOP_ICON		4
#define IMAGE_CAUTION_ICON	5
#define IMAGE_NOTE_ICON		6

#define IMAGE_MAX_TYPE		6

#define IMAGE_ARROW_WIDTH	16
#define IMAGE_ARROW_HEIGHT	9

/*
 *	Pointer structure definition
 */

typedef struct {
    WORD	Width, Height;
    WORD	XOffset, YOffset;
    UWORD	*PointerData;
} Pointer, *PointerPtr;

/*
 *	Date formats
 */

#define DATE_SHORT		0	/* 03/01/90 */
#define DATE_ABBR		1	/* Mar 1, 1990 */
#define DATE_LONG		2	/* March 1, 1990 */
#define DATE_ABBRDAY	3	/* Thu, Mar 1, 1990 */
#define DATE_LONGDAY	4	/* Thursday, March 1, 1990 */
#define DATE_MILITARY	5	/* 01 Mar 90 */

/*
 *	Arrays
 */

extern TextChar	toUpper[];		/* Convert character to upper case */
extern TextChar	toLower[];		/* Convert character to lower case */
extern BYTE		wordChar[];		/* 1 if char is valid word char, 0 otherwise */

/*
 *	Prototypes
 */

void			InitToolbox(BOOL, TextAttrPtr);
void			SetToolboxColors(WORD, WORD, WORD, WORD);
void			AutoActivateEnable(BOOL);

UWORD			LibraryVersion(struct Library *);
UWORD			SystemVersion(void);

void			SetStdPointer(WindowPtr, UWORD);
void			ObscurePointer(WindowPtr);

ImagePtr		GetImage(UWORD);
void			SysBeep(UWORD);
void			ConvertKeyMsg(IntuiMsgPtr);
BOOL			WaitMouseUp(MsgPortPtr, WindowPtr);
WindowPtr		ActiveWindow(void);

WORD			CmpString(TextPtr, TextPtr, WORD, WORD, BOOL);
void			NumToString(LONG, TextPtr);
LONG			StringToNum(TextPtr);
void			DateString(struct DateStamp *, WORD, TextPtr);
void			TimeString(struct DateStamp *, BOOL, BOOL, TextPtr);

BorderPtr		BoxBorder(WORD, WORD, WORD, WORD);
BorderPtr		ShadowBoxBorder(WORD, WORD, WORD, BOOL);
void			AppendBorder(BorderPtr, BorderPtr);
void			FreeBorder(BorderPtr);

IntuiTextPtr	NewIntuiText(WORD, WORD, TextPtr, WORD, WORD);
void			FreeIntuiText(IntuiTextPtr);
BOOL			ChangeIntuiText(IntuiTextPtr, TextPtr);
BOOL			SetIntuiTextFont(IntuiTextPtr, TextAttrPtr);

TextFontPtr		GetFont(TextAttrPtr);

#endif
