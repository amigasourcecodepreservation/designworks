/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	IFF Form DRAW
 *	Copyright (c) 1990 New Horizons Software, Inc.
 *
 *	Permission is hereby granted to use this file in any and all
 *	applications.  Modifying the structures or defines included
 *	in this file is not permitted without written consent of
 *	New Horizons Software, Inc.
 */

#ifndef IFF_DRAW_H
#define IFF_DRAW_H

#include <IFF/IFF.h>

#ifndef TOOLBOX_FIXMATH_H
#include <Toolbox/FixMath.h>
#endif

/*
 *	Form type and chunk ID's
 */

#define ID_DRAW		MakeID('D','R','A','W')		/* Form type */

#define ID_FONT		MakeID('F','O','N','T')		/* Chunks */
#define ID_PATS		MakeID('P','A','T','S')
#define ID_PREC		MakeID('P','R','E','C')
#define ID_DOC		MakeID('D','O','C',' ')
#define ID_LAYR		MakeID('L','A','Y','R')
#define ID_GRP		MakeID('G','R','P',' ')
#define ID_EGRP		MakeID('E','G','R','P')
#define ID_BMAP		MakeID('B','M','A','P')
#define ID_STXT		MakeID('S','T','X','T')
#define ID_LINE		MakeID('L','I','N','E')
#define ID_POLY		MakeID('P','O','L','Y')
#define ID_OVAL		MakeID('O','V','A','L')
#define ID_RECT		MakeID('R','E','C','T')

/*
 *	Type definitions
 */

typedef WORD	FontNum;
typedef WORD	FontSize;		/* Must be signed */
typedef WORD	PenPatNum;
typedef WORD	FillPatNum;
typedef WORD	ArrowNum;

typedef UWORD		RGBColor;		/* Four bits each for R-G-B */
typedef RGBColor	*RGBColorPtr;

#define RED(rgb)	(((rgb) >> 8) & 0x0F)
#define GREEN(rgb)	(((rgb) >> 4) & 0x0F)
#define BLUE(rgb)	((rgb) & 0x0F)

#define RGBCOLOR(r,g,b)	(((r) << 8) + ((g) << 4) + (b))

#define RGBCOLOR_BLACK		0x000
#define RGBCOLOR_WHITE		0xFFF
#define RGBCOLOR_RED		0xF00
#define RGBCOLOR_YELLOW		0xFF0
#define RGBCOLOR_GREEN		0x0F0
#define RGBCOLOR_CYAN		0x0FF
#define RGBCOLOR_BLUE		0x00F
#define RGBCOLOR_MAGENTA	0xF0F

#define RGB_TRANSPARENT		0xF000		/* Only for bit-maps */

typedef RGBColor	RGBPat8[8*8];
typedef RGBPat8		*RGBPat8Ptr;

typedef struct {
	Fixed	X, Y;
} FixPoint, *FixPtPtr;

typedef struct {
	Fixed	MinX, MinY, MaxX, MaxY;
} FixRect, *FixRectPtr;

/*
 *	Object flags
 */

#define OBJ_DO_PEN		0x0001
#define OBJ_DO_FILL		0x0002
#define OBJ_LOCKED		0x0100
#define OBJ_SELECTED	0x8000		/* Not saved with file */

/*
 *	Chunk structures follow
 */

/*
 *	FONT - Font name/number table
 *	There are one of these for each font family
 *	These chunks should appear at the top of the file (before document data)
 *	It is necessary to save chunks only for fonts actually used in document
 *	If no FONT chunks appear, then default font is assumed for entire document
 */

typedef struct {
	FontNum		FontNum;
	TextChar	Name[1];		/* NULL terminated font name, without ".font" */
} FontID;

/*
 *	PATS - Array of patterns used in document
 *	Pattern number is index into pattern array
 *	Number of patterns is determined by size of chunk
 */

/*
 *	PREC - Print record
 *	Page setup and print parameters (see Print.h for contents)
 */

typedef struct {
	UBYTE	PrintRec[120];
} PrintRecInfo;

/*
 *	DOC - Document information
 */

#define MAX_DOCWIDTH	8000
#define MAX_DOCHEIGHT	7200

typedef struct {
	ULONG	Flags;				/* Not currently used, should be 0 */
	WORD	Width, Height;		/* Width/height in pixels */
	Point	RulerOffset;		/* Coordinates of ruler zero point */
	WORD	pad[6];
} DocInfo;

/*
 *	LAYR - Begin new layer
 *	All objects following this chunk are added to this layer,
 *	until new LAYR or end of file
 */

#define LAYER_VISIBLE	0x01
#define LAYER_ACTIVE	0x02		/* Not currently used */

typedef struct {
	ULONG		Flags;
	ULONG		pad;
	TextChar	Name[1];			/* NULL terminated layer name */
} DocLayerInfo;

/*
 *	GRP - Begin new group
 *	All objects following this chunk are part of group, until EGRP chunk
 *	Group definitions can be nested
 */

typedef struct {
	ULONG		Flags;
	ULONG		GroupFlags;			/* Currently unused, set to 0 */
	ULONG		pad;
	TextChar	Name[1];			/* Optional NULL terminated group name */
} GroupInfo;

/*
 *	EGRP - End group definition
 *	No data, just a marker
 */

/*
 *	BMAP - Bitmap picture object
 */

#define BMAP_CMPNONE	0
#define BMAP_CMPWORDRUN	1

typedef struct {
	ULONG		Flags;
	UWORD		BitMapFlags;
	ULONG		pad;
	FixRect		Frame;				/* Unrotated, bitmap is drawn scaled to frame */
	Fixed		Rotate;				/* Degrees clockwise about center */
	UBYTE		Compression;
	UBYTE		pad1;
	UWORD		Width, Height;		/* Cannot have fractional size bitmap */
/*	RGBColor	Data[Width*Height]	 Or less, if compressed */
} BitMapInfo;

/*
 *	STXT - Simple text object
 *	Note: Text frame is the text bounding box for word wrap, horizontal
 *		  and vertical stretch is specified with XScale and YScale.
 *		  Program may adjust height of frame to enclose all text
 */

#define TEXT_FLIPHORIZ		0x01
#define TEXT_FLIPVERT		0x02

#define MISCSTYLE_SUPER		0x01		/* Superscript */
#define MISCSTYLE_SUB		0x02		/* Subscript */
#define MISCSTYLE_SHADOW	0x04

#define JUSTIFY_LEFT	0
#define JUSTIFY_CENTER	1
#define JUSTIFY_RIGHT	2
#define JUSTIFY_FULL	3

#define SPACE_SINGLE	0
#define SPACE_1_HALF	0x08	/* 1 1/2 spacing */
#define SPACE_DOUBLE	0x10

#define FIXHEIGHT_AUTO	0
#define FIXHEIGHT_6LPI	12		/* 12 points is 6 lpi */
#define FIXHEIGHT_8LPI	9		/*  9 points is 8 lpi */

typedef struct {
	ULONG		Flags;
	UWORD		TextFlags;
	ULONG		pad;
	RGBColor	PenColor;
	FillPatNum	FillPatNum;
	FixRect		Frame;				/* Unrotated and unscaled */
	Fixed		Rotate;				/* Degrees clockwise about center */
	Fixed		XScale, YScale;		/* Unused, set to FIXED_UNITY */
	FontNum		FontNum;
	FontSize	FontSize;
	UBYTE		Style, MiscStyle;
	UBYTE		Justify, Spacing;
	UBYTE		FixHeight, Leading;
/*	TextChar	Text[]				 Not NULL terminated */
} TextInfo;

/*
 *	LINE - Line object
 */

#define LINE_ARROWSTART	0x01
#define LINE_ARROWEND	0x02

typedef struct {
	ULONG		Flags;
	UWORD		LineFlags;
	ULONG		pad;
	RGBColor	PenColor;
	FixPoint	PenSize;			/* Pen width & height */
	PenPatNum	PenPatNum;			/* Currently unused, set to 0 */
	ArrowNum	ArrowNum;			/* Currently unused, set to 0 */
	FixPoint	Start, End;
} LineInfo;

/*
 *	POLY - Polygon object
 *	Points are normalized to 0 = frame min, 0x7FFFFFFF = frame max
 *	There can be multiple polygons "merged" together in one object
 */

#define POLY_CLOSED		0x01
#define POLY_SMOOTH		0x02

typedef struct {
	WORD		NumPoints;
/*	FixPoint	Points[NumPoints]	 List of points in polygon */
} PolyPath;

typedef struct {
	ULONG		Flags;
	UWORD		PolyFlags;
	ULONG		pad;
	RGBColor	PenColor;
	FixPoint	PenSize;			/* Pen width & height */
	PenPatNum	PenPatNum;			/* Currently unused, set to 0 */
	FillPatNum	FillPatNum;
	FixRect		Frame;
	WORD		NumPaths;
/*	PolyPath	Paths[NumPaths]	 Merged polygons */
} PolyInfo;

/*
 *	OVAL - Ellipse object
 */

typedef struct {
	ULONG		Flags;
	UWORD		OvalFlags;			/* Currently unused, set to 0 */
	ULONG		pad;
	RGBColor	PenColor;
	FixPoint	PenSize;			/* Pen width & height */
	PenPatNum	PenPatNum;			/* Currently unused, set to 0 */
	FillPatNum	FillPatNum;
	FixRect		Frame;				/* Unrotated */
	Fixed		Rotate;				/* Degrees clockwise about center */
} OvalInfo;

/*
 *	RECT - Rectangle object
 */

typedef struct {
	ULONG		Flags;
	UWORD		RectFlags;			/* Currently unused, set to 0 */
	ULONG		pad;
	RGBColor	PenColor;
	FixPoint	PenSize;			/* Pen width & height */
	PenPatNum	PenPatNum;			/* Currently unused, set to 0 */
	FillPatNum	FillPatNum;
	FixRect		Frame;				/* Unrotated */
	Fixed		Rotate;				/* Degrees clockwise about center */
	FixPoint	CornerRadius;		/* Currently unused, set to 0,0 */
} RectInfo;

#endif
