/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software, Inc.
 */

#include "Object.h"
#include "Version.h"

#undef NULL
#define NULL	0			/* Keep C++ happy (doesn't like 0L for NULL) */

/*
 *	Misc items
 */

#define LYINDIC_EDGE	(2*IMAGE_ARROW_WIDTH + 2)
#define LYINDIC_WIDTH	(8*8 + 2)

#define MAX_WINDOWS		10
#define MAX_DEPTH		3
#define BIT_PLANES		MAX_DEPTH
#define FILEBUFF_SIZE	512

#define MAX_FONTS		0x7FFF
#define MAX_FONTSIZE	250
#define MAX_MENU_FONTS	18
#define MAX_PENSIZE		100

#define NUM_PENS		8
#define NUM_FILLPATS	20
#define NUM_PENSIZES	6
#define NUM_STDCOLORS	16
#define NUM_GRIDINDEX	9

/*
 *	Icon numbers
 */

enum {
	ICON_DOC,	ICON_BMAP,		ICON_PREFS
};

/*
 *	Types of measurements used in requesters
 */

enum {
	MEASURE_INCH,	MEASURE_CM
};

#define DEFAULT_MEASURE	MEASURE_INCH

/*
 *	Error numbers
 */

enum {
	INIT_BADSYS,	INIT_NOICON,	INIT_NOFONT,	INIT_NOSCREEN,	INIT_NOMEM
};

enum {
	ERR_UNKNOWN,
	ERR_NO_MEM,
	ERR_OPEN,
	ERR_SAVE,
	ERR_SAVE_NO_MEM,
	ERR_PRINT,
	ERR_PAGE_SIZE,
	ERR_PAGE_NUM,			/* Invalid page number */
	ERR_BAD_COPIES,
	ERR_NO_PAGE,			/* No such page */
	ERR_NO_FONT,
	ERR_NO_REXX,
	ERR_BAD_MACRO,
	ERR_MACRO_FAIL,
	ERR_OBJ_LOCKED,
	ERR_PEN_SIZE,
	ERR_SCALE,
	ERR_BAD_PICT,
	ERR_NO_VIS_LAYER,
	ERR_LAST_LAYER
};

#define ERR_MAX_ERROR	ERR_LAST_LAYER

/*
 *	Window gadget definitions
 */

enum {
	UP_ARROW,		DOWN_ARROW,		LEFT_ARROW,		RIGHT_ARROW,
	VERT_SCROLL,	HORIZ_SCROLL,	LAYERUP_ARROW,	LAYERDOWN_ARROW
};

/*
 *	Toolbox draw tool
 */

enum {
	TOOL_SELECT,	TOOL_TEXT,	TOOL_LINE,	TOOL_HVLINE,
	TOOL_RECT,		TOOL_OVAL,	TOOL_CURVE,	TOOL_POLY
};

/*
 *	Dialog ID's
 */

enum {
	DLG_INIT,
	DLG_SAVECHANGES,	DLG_REVERT,			DLG_SAVEPREFS,
	DLG_PAGESETUP,		DLG_PRINT,			DLG_CANCELPRINT,	DLG_NEXTPAGE,
	DLG_OPTIONS,		DLG_DRAWSIZE,		DLG_ALIGN,
	DLG_FONT,
	DLG_MACRONAME,
	DLG_ERROR,			DLG_HELP,			DLG_ABOUT,			DLG_MEMORY,
	DLG_OTHERPEN,		DLG_SCALE,			DLG_PENCOLORS,		DLG_SCREENCOLORS,
	DLG_FILLPATTERNS,	DLG_LAYERS,			DLG_DELETELAYER,	DLG_GRIDSIZE
};

/*
 *	Undo ID's (Redo ID's are one higher than Undo's)
 */
/*
enum {
	UNDO_NONE			= 0,
	UNDO_TYPING			= 2,
	UNDO_DELETE			= 4,
	UNDO_CUT			= 6,
	UNDO_COPY			= 8,
	UNDO_PASTE			= 10,
	UNDO_ERASE			= 12,
};
*/
/*
 *	Undo commands passed by undo handler to undo routines
 */

enum {
	UNDOCMD_CLEAR,		/* Clear undo buffer */
	UNDOCMD_UNDO,		/* Undo the last operation */
	UNDOCMD_REDO,		/* Redo the last operation */
	UNDOCMD_INIT		/* Init menu item text */
};

/*
 *	Menu item definitions
 */

enum {						/* Menus */
	PROJECT_MENU,
	EDIT_MENU,
	LAYOUT_MENU,
	ARRANGE_MENU,
	PEN_MENU,
	TEXT_MENU,
	VIEW_MENU,
	MACRO_MENU
};

enum {						/* Project menu */
	NEW_ITEM			= 0,
	OPEN_ITEM,
	CLOSE_ITEM,
	IMPORTPICT_ITEM		= 4,
	EXPORTPICT_ITEM,
	SAVE_ITEM			= 7,
	SAVEAS_ITEM,
	REVERT_ITEM,
	PAGESETUP_ITEM		= 11,
	PRINTONE_ITEM,
	PRINT_ITEM,
	SAVEDEFAULTS_ITEM	= 15,
	QUIT_ITEM			= 17
};

enum {						/* Edit menu */
	CUT_ITEM			= 0,
	COPY_ITEM,
	PASTE_ITEM,
	ERASE_ITEM,
	DUPLICATE_ITEM		= 5,
	ROTATE_ITEM,
	FLIP_ITEM,
	SCALE_ITEM,
	CONVERTTOPOLY_ITEM	= 10,
	POLYGON_ITEM,
	SELECTALL_ITEM		= 13,
};

enum {						/* Layout menu */
	NORMALSIZE_ITEM		= 0,
	ENLARGE_ITEM,
	REDUCE_ITEM,
	FITTOWINDOW_ITEM,
	GRIDSNAP_ITEM		= 5,
	GRIDSIZE_ITEM,
	PENCOLORS_ITEM		= 8,
	FILLPATTERNS_ITEM,
	LAYERS_ITEM,
	DRAWINGSIZE_ITEM,
	PREFERENCES_ITEM	= 13,
	SCREENCOLORS_ITEM
};

enum {						/* Arrange menu */
	MOVEFORWARD_ITEM	= 0,
	MOVETOFRONT_ITEM,
	MOVEBACKWARD_ITEM,
	MOVETOBACK_ITEM,
	ALIGNTOGRID_ITEM	= 5,
	ALIGNOBJECTS_ITEM,
	GROUP_ITEM			= 8,
	UNGROUP_ITEM,
	LOCK_ITEM			= 11,
	UNLOCK_ITEM
};

enum {						/* Pen menu */
	WIDTH_ITEM			= 0,
	HEIGHT_ITEM,
	NOARROWS_ITEM		= 3,
	ARROWSTART_ITEM,
	ARROWEND_ITEM
};

enum {						/* Text menu */
	FONT_ITEM			= 0,
	PLAIN_ITEM			= 2,
	BOLD_ITEM,
	ITALIC_ITEM,
	UNDERLINE_ITEM,
	LEFTALIGN_ITEM		= 7,
	CENTERALIGN_ITEM,
	RIGHTALIGN_ITEM,
	SINGLESPACE_ITEM	= 11,
	ONEANDHALFSPACE_ITEM,
	DOUBLESPACE_ITEM
};

enum {						/* View menu */
	ABOUT_ITEM			= 0,
	SHOWBACKLAYERS_ITEM	= 2,
	SHOWRULER_ITEM,
	SHOWGRID_ITEM,
	SHOWPAGE_ITEM,
	SHOWTOOLBOX_ITEM	= 7,
	SHOWPENPALETTE_ITEM,
	SHOWFILLPALETTE_ITEM,
	WINDOW_ITEM			= 11	/* First window in list */
};

enum {						/* Macro menu */
	OTHERMACRO_ITEM		= 11
};

enum {						/* Rotate submenu */
	LEFT_SUBITEM		= 0,
	RIGHT_SUBITEM
};

enum {						/* Flip submenu */
	HORIZONTAL_SUBITEM	= 0,
	VERTICAL_SUBITEM
};

enum {						/* Polygon submenu */
	CLOSEPOLY_SUBITEM	= 0,
	OPENPOLY_SUBITEM,
	SMOOTH_SUBITEM,
	UNSMOOTH_SUBITEM
};

enum {						/* Font submenu */
	OTHERFONT_SUBITEM	= 0,
	FONT_SUBITEM		= 2	/* First font in list */
};

enum {						/* Ruler submenu */
	INCH_SUBITEM		= 0,
	CM_SUBITEM
};

/*
 *	First long word of special files identifies file type
 */

#define ID_PREFSFILE	0x06660137L

/*
 *	Misc definitions
 */

#define WKIND_DOC	0
#define WKIND_TOOL	1
#define WKIND_BACK	2

#define QUOTE			'\''
#define QUOTE_OPEN		QUOTE
#define QUOTE_CLOSE		QUOTE
#define DQUOTE			'\"'
#define DQUOTE_OPEN		DQUOTE
#define DQUOTE_CLOSE	DQUOTE

#define BS		0x08
#define TAB		'\t'
#define LF		'\n'
#define CR		0x0D
#define DEL		0x7F
#define NBSP	0xA0	/* Non-breaking space */
#define SHYPH	0xAD	/* Soft hyphen */

#define BLINK_FAST	3
#define BLINK_SLOW	6
#define BLINK_OFF	0

#define SHIFTKEYS	(IEQUALIFIER_LSHIFT | IEQUALIFIER_RSHIFT)
#define ALTKEYS		(IEQUALIFIER_LALT | IEQUALIFIER_RALT)
#define CMDKEY		AMIGARIGHT

#define MODIFIER_WORD	SHIFTKEYS
#define MODIFIER_SENT	ALTKEYS
#define MODIFIER_SEL	IEQUALIFIER_CONTROL

/*
 *	Useful macros
 */

#define IS_UPPER(ch)	((ch) != toLower[(ch)])	/* Only alphabetics can be upper */

#define MAX(a,b)	((a)>(b)?(a):(b))
#define MIN(a,b)	((a)<(b)?(a):(b))
#define ABS(x)		((x)<0?(-(x)):(x))

#define DotsToDecipoints(a,dpi)	(((LONG)(a)*720L)/(dpi))
#define DecipointsToDots(a,dpi)	(((LONG)(a)*(dpi))/720L)

#define MENUITEM(menu, item, sub)	\
	(SHIFTMENU(menu) | SHIFTITEM(item) | SHIFTSUB(sub))

/*
 *	Function key definition
 */

#define FKEY_MENU	0
#define FKEY_MACRO	1

typedef struct {
	WORD	Type;		/* Menu equiv. or macro */
	Ptr		Data;		/* Menu item number or pointer to macro name */
} FKey;

/*
 *	User options
 */

typedef struct {
	UBYTE	MeasureUnit;	/* Inches or centimeters */
	UBYTE	BlinkPeriod;	/* Insertion point blinks 5/(n+1) per sec, 0 = off */
	UBYTE	BeepFlash, BeepSound;	/* Sound and/or flash screen on errors */
	UBYTE	SaveIcons;		/* Save icons with files */
	UBYTE	DragOutlines;	/* Drag outlines of objects, or bounding rect */
	UBYTE	PolyAutoClose;	/* Automatically close new polygons */
	UBYTE	ShowPictures;	/* Draw actual bitmap pictures */
	UBYTE	ColorCorrect;	/* Apply color correction when drawing in window */
	RGBColor	PrtCyan, PrtMagenta, PrtYellow;	/* For screen color correction */
	UBYTE	pad[20];
} Options;

/*
 *	Definitions for color tables and inverse color tables
 */

typedef RGBColor	ColorTable[];
typedef ColorTable	*ColorTablePtr;

typedef UWORD			InvColorTable[4096];	/* UWORD for direct-lookup tables */
typedef InvColorTable	*InvColorTablePtr;

/*
 *	Actual colors for dot-matrix printer ribbon (to match screen colors)
 *	Screen colors for red, green, and blue are derived from these
 */

#define PRT_RGBCOLOR_CYAN		0x3AC
#define PRT_RGBCOLOR_MAGENTA	0xE07
#define PRT_RGBCOLOR_YELLOW		0xFE5
