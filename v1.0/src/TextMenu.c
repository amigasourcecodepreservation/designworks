/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Text menu routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>

#include <string.h>

#include <Toolbox/Graphics.h>
#include <Toolbox/Window.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Menu.h>
#include <Toolbox/ScrollList.h>
#include <Toolbox/Utility.h>

#include "Draw.h"
#include "Proto.h"

/*
 *	External variables
 */

extern struct Library	*DiskfontBase;

extern MsgPortPtr	mainMsgPort;
extern ScreenPtr	screen;
extern WindowPtr	backWindow;

extern Defaults	defaults;

extern UBYTE	blackColor, whiteColor, darkColor, lightColor;

extern TextChar	strSampleText[];

extern DlgTemplPtr	dlgList[];

/*
 *	Local variables and definitions
 */

enum {
	NAME_LIST = 2,
	NAME_UP,
	NAME_DOWN,
	NAME_SCROLL,
	SIZE_LIST,
	SIZE_UP,
	SIZE_DOWN,
	SIZE_SCROLL,
	LISTMENU_BOX,
	SIZE_TEXT = 13
};

#define INFO_TBMARGIN	5
#define INFO_LRMARGIN	10

static WORD		numMenuFonts;
static FontNum	menuFontNums[MAX_MENU_FONTS];
static FontSize	menuFontSizes[MAX_MENU_FONTS];

static DialogPtr	fontDlg;

static ScrollListPtr	nameScrollList, sizeScrollList;

static UWORD	currSize;
static BOOL		hasSizeText;

/*
 *	Local prototypes
 */

void	GetNameList(void);
void	GetSizeList(void);
void	SetSelSize(void);

FontNum		SelFontNum(void);
FontSize	SelSize(void);

void	ShowFontSample(void);
void	NewName(WORD);
void	NewSize(WORD);
void	SetMenuBox(BOOL);
BOOL	FontDialogFilter(IntuiMsgPtr, WORD *);
BOOL	DoFontRequest(FontNum *, FontSize *);

BOOL	DoFontMenu(WindowPtr, UWORD);

BOOL	DoTextParams(WindowPtr, FontNum, FontSize, WORD, WORD, WORD, WORD);

/*
 *	Return number of menu fonts
 */

WORD NumMenuFonts()
{
	return (numMenuFonts);
}

/*
 *	Return item number in font submenu, or -1 if not a menu font
 */

WORD MenuFontNum(FontNum fontNum, FontSize size)
{
	register WORD i;

	for (i = 0; i < numMenuFonts; i++) {
		if (fontNum == menuFontNums[i] && size == menuFontSizes[i])
			return (i);
	}
	return (-1);
}

/*
 *	Get menu font info for given entry
 */

void GetMenuFontInfo(WORD num, FontNum *fontNum, FontSize *size)
{
	if (num >= numMenuFonts)
		return;
	*fontNum = menuFontNums[num];
	*size = menuFontSizes[num];
}

/*
 *	Add or remove font from menu font list
 */

void SetMenuFont(FontNum fontNum, FontSize size, BOOL add)
{
	WORD i, entry, len1, len2, cmp;
	FontSize size1, size2;
	MenuItemPtr menuItem;
	TextChar name1[50], name2[50];

	if (fontNum == -1 || size <= 0 || size > MAX_FONTSIZE)
		return;
/*
	Add new menu item, sorted into list
*/
	if (add) {
		if (MenuFontNum(fontNum, size) != -1 || numMenuFonts >= MAX_MENU_FONTS)
			return;
		GetFontName(fontNum, name1);
		size1 = size;
		len1 = strlen(name1);
		for (i = 0; i < numMenuFonts; i++) {
			GetFontName(menuFontNums[i], name2);
			size2 = menuFontSizes[i];
			len2 = strlen(name2);
			cmp = CmpString(name1, name2, len1, len2, FALSE);
			if (cmp < 0 || (cmp == 0 && size1 < size2))
				break;
		}
		entry = i;
		for (i = numMenuFonts; i > entry; i--) {
			menuFontNums[i]  = menuFontNums[i - 1];
			menuFontSizes[i] = menuFontSizes[i - 1];
		}
		menuFontNums[entry] = fontNum;
		menuFontSizes[entry] = size;
		name1[0] = toUpper[name1[0]];
		name1[len1] = '-';
		NumToString((LONG) size1, name1 + len1 + 1);
		InsertMenuItem(backWindow, MENUITEM(TEXT_MENU, FONT_ITEM, entry + FONT_SUBITEM),
					   name1);
		numMenuFonts++;
	}
/*
	Remove menu item
*/
	else {
		if ((entry = MenuFontNum(fontNum, size)) == -1 || numMenuFonts <= 0)
			return;
		for (i = entry; i < numMenuFonts - 1; i++) {
			menuFontNums[i]  = menuFontNums[i + 1];
			menuFontSizes[i] = menuFontSizes[i + 1];
		}
		DeleteMenuItem(backWindow, MENUITEM(TEXT_MENU, FONT_ITEM, entry + FONT_SUBITEM));
		numMenuFonts--;
	}
/*
	Adjust mutual exclude (and set checkable flag)
*/
	for (i = 0; i < numMenuFonts; i++) {
		menuItem = ItemAddress(backWindow->MenuStrip, MENUITEM(TEXT_MENU, FONT_ITEM, i + FONT_SUBITEM));
		menuItem->Flags |= CHECKIT;
		menuItem->MutualExclude = ~(1 << i) << FONT_SUBITEM;
	}
}

/*
 *	Get and display the font name list
 */

static void GetNameList()
{
	register WORD i, item, numNames;
	TextPtr name;
	ListHeadPtr	nameList;
	TextChar fontName[50];

/*
	Get sorted name list with capitals as first letter of name
*/
	if ((nameList = CreateList()) == NULL)
		return;
	numNames = NumFonts();
	DisposeListItems(nameList);
	for (i = 0; i < numNames; i++) {
		GetFontName(i, fontName);
		fontName[0] = toUpper[fontName[0]];
		if ((item = AddListItem(nameList, fontName, 0, TRUE)) == -1)
			break;
	}
/*
	Now display the new list of font names
*/
	SLDoDraw(nameScrollList, FALSE);
	numNames = NumListItems(nameList);
	for (i = 0; i < numNames; i++) {
		name = GetListItem(nameList, i);
		SLAddItem(nameScrollList, name, strlen(name), i);
	}
	SLDoDraw(nameScrollList, TRUE);
	DisposeList(nameList);
}

/*
 *	Get and display the size list of the currently selected font name
 */

static void GetSizeList()
{
	WORD i, len, numSizes, textLen;
	FontNum fontNum;
	FontSize size, *sizeList, *newList;
	TextAttrPtr fontTable;
	TextPtr name;
	TextChar text[50];

	if ((i = SLNextSelect(nameScrollList, -1)) == -1)
		return;
	SLGetItem(nameScrollList, i, text);
	textLen = strlen(text);
/*
	Get sorted size list for selected font
*/
	fontTable = FontAttrTable();
	numSizes = 0;
	sizeList = NULL;
	for (fontNum = 0; fontNum < NumFontAttrs(); fontNum++) {
		name = fontTable[fontNum].ta_Name;
		len = strlen(name) - 5;
/*
	If this is the selected font name then insert size into list
*/
		if (len <= 0 || CmpString(name, text, len, textLen, FALSE) != 0)
			continue;
		if ((newList = MemAlloc((numSizes + 1)*sizeof(UWORD), 0)) == NULL)
			break;
		size = fontTable[fontNum].ta_YSize;
		for (i = 0; i < numSizes && sizeList[i] < size; i++)
			newList[i] = sizeList[i];
		if (i < numSizes && sizeList[i] == size)
			MemFree(newList, (numSizes + 1)*sizeof(UWORD));
		else {
			newList[i] = size;
			for ( ; i < numSizes; i++)
				newList[i + 1] = sizeList[i];
			if (sizeList)
				MemFree(sizeList, numSizes*sizeof(UWORD));
			sizeList = newList;
			numSizes++;
		}
	}
/*
	Now display the new list of font sizes
*/
	SLDoDraw(sizeScrollList, FALSE);
	for (i = 0; i < numSizes; i++) {
		NumToString(sizeList[i], text);
		SLAddItem(sizeScrollList, text, strlen(text), i);
	}
	SLDoDraw(sizeScrollList, TRUE);
	if (sizeList)
		MemFree(sizeList, numSizes*sizeof(UWORD));
}

/*
 *	Set new value for selected size that is closest to currSize
 */

static void SetSelSize()
{
	WORD i, numSizes;
	UWORD size, prevSize;
	TextChar sizeText[10];

	if ((numSizes = SLNumItems(sizeScrollList)) == 0)
		return;
	if (hasSizeText)
		NewSize(-1);
	for (i = 0; i < numSizes; i++) {
		SLGetItem(sizeScrollList, i, sizeText);
		size = StringToNum(sizeText);
		if (size >= currSize)
			break;
		prevSize = size;
	}
	if (i >= numSizes)
		i = numSizes - 1;
	else if (i > 0 && currSize - prevSize < size - currSize)
		i--;
	if (!hasSizeText || size == currSize) {
		SLSelectItem(sizeScrollList, i, TRUE);
		SLAutoScroll(sizeScrollList, i);
		currSize = size;
	}
}

/*
 *	Return font number of selected font
 *	Return -1 if no font is chosen
 */

static FontNum SelFontNum()
{
	WORD i;
	TextChar name[50];

	if ((i = SLNextSelect(nameScrollList, -1)) < 0)
		return (-1);
	SLGetItem(nameScrollList, i, name);
	return (FontNumber(name));
}

/*
 *	Return selecte size, or -1 if no size is chosen
 */

static FontSize SelSize()
{
	WORD i;
	TextChar sizeText[GADG_MAX_STRING];

	if (hasSizeText) {
		GetEditItemText(fontDlg->FirstGadget, SIZE_TEXT, sizeText);
		if (sizeText[0] != '\0' && !CheckNumber(sizeText))
			return (-1);
	}
	else {
		if ((i = SLNextSelect(sizeScrollList, -1)) < 0)
			return (-1);
		SLGetItem(sizeScrollList, i, sizeText);
	}
	return (StringToNum(sizeText));
}

/*
 *	Show sample of selected font
 */

static void ShowFontSample()
{
	WORD top, bottom, baseline;
	FontNum fontNum;
	FontSize size;
	TextFontPtr font, oldFont;
	RegionPtr oldRgn;
	LayerPtr layer = fontDlg->WLayer;
	RastPtr rPort = fontDlg->RPort;
	GadgetPtr gadget;
	BorderPtr border;
	Rectangle rect;

	fontNum = SelFontNum();
	if ((size = SelSize()) < 0)
		return;
	gadget = GadgetItem(fontDlg->FirstGadget, LISTMENU_BOX);
	top = gadget->TopEdge + gadget->Height + INFO_TBMARGIN;
	bottom = fontDlg->Height - INFO_TBMARGIN - 1;
/*
	First clear the display area
*/
	SetRect(&rect, INFO_LRMARGIN, top, fontDlg->Width - INFO_LRMARGIN - 1, bottom);
	if ((border = BoxBorder(rect.MaxX - rect.MinX + 1, rect.MaxY - rect.MinY + 1,
							darkColor, -1)) != NULL) {
		DrawBorder(rPort, border, rect.MinX, rect.MinY);
		FreeBorder(border);
	}
	oldRgn = GetClip(layer);
	SetRectClip(layer, &rect);
	SetRast(rPort, whiteColor);
/*
	Now get and display the font
*/
	SetStdPointer(fontDlg, POINTER_WAIT);
	if (size > 0 && size <= MAX_FONTSIZE &&
		(font = GetNumFont(fontNum, size)) != NULL) {
		baseline = font->tf_Baseline + (bottom - top + 1 - font->tf_YSize)/2;
		if (baseline < font->tf_Baseline)
			baseline = font->tf_Baseline;
		baseline += top;
		if (baseline > rect.MaxY)
			baseline = rect.MaxY;
		oldFont = rPort->Font;
		SetFont(rPort, font);
		SetAPen(rPort, blackColor);
		SetDrMd(rPort, JAM1);
		Move(rPort, INFO_LRMARGIN, baseline);
		Text(rPort, strSampleText, strlen(strSampleText));
		SetFont(rPort, oldFont);
		CloseFont(font);
	}
	SetStdPointer(fontDlg, POINTER_ARROW);
/*
	Clean up
*/
	SetClip(layer, oldRgn);
}

/*
 *	Set new selected name
 */

static void NewName(WORD nameNum)
{
	WORD numNames;

	if ((numNames = SLNumItems(nameScrollList)) == 0 ||
		nameNum < 0 || nameNum >= numNames)
		return;
	SLSelectItem(nameScrollList, nameNum, TRUE);
	SLAutoScroll(nameScrollList, nameNum);
	SLRemoveAll(sizeScrollList);
	GetSizeList();
	SetSelSize();
}

/*
 *	Set new selected size
 *	If sizeNum is -1, then check size text box
 */

static void NewSize(WORD sizeNum)
{
	WORD i, numSizes;
	FontSize size;
	BOOL match, inMenu;
	TextChar sizeText[GADG_MAX_STRING];

	if ((numSizes = SLNumItems(sizeScrollList)) == 0)
		return;
	if (hasSizeText && sizeNum == -1) {
		if ((size = SelSize()) <= 0)
			return;
		if (size != currSize) {
			for (i = 0; i < numSizes; i++) {
				SLGetItem(sizeScrollList, i, sizeText);
				match = (size == StringToNum(sizeText));
				SLSelectItem(sizeScrollList, i, match);
				if (match)
					SLAutoScroll(sizeScrollList, i);
			}
			if (size > 0 && size <= MAX_FONTSIZE)
				currSize = size;
			ShowFontSample();
			inMenu = (MenuFontNum(SelFontNum(), size) != -1);
			SetMenuBox(inMenu);
		}
	}
	else {
		if (sizeNum < 0 || sizeNum >= numSizes)
			return;
		SLSelectItem(sizeScrollList, sizeNum, TRUE);
		SLAutoScroll(sizeScrollList, sizeNum);
		SLGetItem(sizeScrollList, sizeNum, sizeText);
		currSize = StringToNum(sizeText);
		if (hasSizeText)
			SetEditItemText(fontDlg, NULL, SIZE_TEXT, sizeText);
	}
}

/*
 *	Set "in menu" check box
 */

static void SetMenuBox(BOOL inMenu)
{
	BOOL enable;
	GadgetPtr gadgList = fontDlg->FirstGadget;

	enable = (numMenuFonts < MAX_MENU_FONTS || inMenu);
	EnableGadgetItem(gadgList, LISTMENU_BOX, fontDlg, NULL, enable);
	SetGadgetItemValue(gadgList, LISTMENU_BOX, fontDlg, NULL, inMenu);
}

/*
 *	Handle DoFontRequest request intuiMsg
 */

static BOOL FontDialogFilter(register IntuiMsgPtr intuiMsg, WORD *item)
{
	WORD gadgNum, selItem;
	ULONG class = intuiMsg->Class;
	UWORD code = intuiMsg->Code;
	UWORD modifier = intuiMsg->Qualifier;
	ScrollListPtr scrollList;

	switch (class) {
/*
	Handle gadget down of up/down scroll arrows
*/
	case GADGETDOWN:
	case GADGETUP:
		gadgNum = GadgetNumber((GadgetPtr) intuiMsg->IAddress);
		if (gadgNum == NAME_LIST || gadgNum == NAME_SCROLL ||
			gadgNum == NAME_UP || gadgNum == NAME_DOWN) {
			SLGadgetMessage(nameScrollList, mainMsgPort, intuiMsg);
			*item = gadgNum;
			return (TRUE);
		}
		if (gadgNum == SIZE_LIST || gadgNum == SIZE_SCROLL ||
			gadgNum == SIZE_UP || gadgNum == SIZE_DOWN) {
			SLGadgetMessage(sizeScrollList, mainMsgPort, intuiMsg);
			*item = gadgNum;
			return (TRUE);
		}
		break;
/*
	Handle keyboard events
*/
	case RAWKEY:
		if (code == CURSORUP || code == CURSORDOWN) {
			ReplyMsg((MsgPtr) intuiMsg);
			if (modifier & SHIFTKEYS) {
				scrollList = sizeScrollList;
				*item = SIZE_LIST;
			}
			else {
				scrollList = nameScrollList;
				*item = NAME_LIST;
			}
			selItem = SLNextSelect(scrollList, -1);
			if (selItem == -1)
				selItem = 0;
			else if (code == CURSORUP)
				selItem--;
			else
				selItem++;
			SLSelectItem(scrollList, selItem, TRUE);
			return (TRUE);
		}
		break;
	}
	return (DialogFilter(intuiMsg, item));
}

/*
 *	Display requester to get desired font
 *	The requested font will not be tested for opening
 */

static BOOL DoFontRequest(FontNum *fontNum, FontSize *size)
{
	WORD i, item, len, numNames, dlgNum;
	WORD prevSize, count;
	BOOL result, done, inMenu;
	GadgetPtr gadgList, sizeTextGadg;
	TextChar name[50], text[50];

	result = FALSE;
	nameScrollList = sizeScrollList = NULL;
	fontDlg = NULL;
/*
	Bring up dialog
*/
/*
	hasSizeText = (LibraryVersion(DiskfontBase) >= OSVERSION_2_0);
*/
	hasSizeText = TRUE;
	dlgNum = DLG_FONT;
	BeginWait();
	if ((nameScrollList = NewScrollList(FALSE)) == NULL ||
		(sizeScrollList = NewScrollList(FALSE)) == NULL ||
		(fontDlg = GetDialog(dlgList[dlgNum], screen, mainMsgPort)) == NULL) {
		EndWait();
		Error(ERR_NO_MEM);
		goto Exit;
	}
	SetStdPointer(fontDlg, POINTER_WAIT);
	gadgList = fontDlg->FirstGadget;
	InitScrollList(nameScrollList, GadgetItem(gadgList, NAME_LIST), fontDlg, NULL);
	InitScrollList(sizeScrollList, GadgetItem(gadgList, SIZE_LIST), fontDlg, NULL);
	OutlineOKButton(fontDlg);
	SLDrawBorder(nameScrollList);
	SLDrawBorder(sizeScrollList);
	if (hasSizeText) {
		sizeTextGadg = GadgetItem(gadgList, SIZE_TEXT);
		sizeTextGadg->Activation |= LONGINT;
	}
	GetFontName(*fontNum, name);
	currSize = *size;
	if (hasSizeText) {
		NumToString(currSize, text);
		SetEditItemText(fontDlg, NULL, SIZE_TEXT, text);
	}
	len = strlen(name);
	GetNameList();
	numNames = SLNumItems(nameScrollList);
	for (i = 0; i < numNames; i++) {
		SLGetItem(nameScrollList, i, text);
		if (CmpString(name, text, len, strlen(text), FALSE) == 0)
			break;
	}
	NewName(i);
	inMenu = (MenuFontNum(*fontNum, *size) != -1);
	SetMenuBox(inMenu);
	ShowFontSample();
/*
	Get and process requester messages
*/
	prevSize = currSize;
	count = -1;
	done = FALSE;
	SetStdPointer(fontDlg, POINTER_ARROW);
	do {
		WaitPort(mainMsgPort);
		item = CheckDialog(mainMsgPort, fontDlg, FontDialogFilter);
		switch (item) {
		case -1:
			if (prevSize != SelSize()) {
				prevSize = SelSize();
				count = 5;				/* 1/2 second before we show sample */
			}
			else if (count > 0)
				count--;
			else if (count == 0) {
				NewSize(-1);
				count = -1;
			}
			break;
		case CANCEL_BUTTON:
			done = TRUE;
			break;
		case NAME_LIST:
			if ((i = SLNextSelect(nameScrollList, -1)) == -1)
				break;
			NewName(i);
			inMenu = (MenuFontNum(SelFontNum(), SelSize()) != -1);
			SetMenuBox(inMenu);
			ShowFontSample();
			if (SLDoubleClick(nameScrollList))
				goto Done;
			break;
		case SIZE_LIST:
			if ((i = SLNextSelect(sizeScrollList, -1)) == -1)
				break;
			NewSize(i);
			inMenu = (MenuFontNum(SelFontNum(), SelSize()) != -1);
			SetMenuBox(inMenu);
			ShowFontSample();
			if (SLDoubleClick(sizeScrollList))
				goto Done;
			break;
		case OK_BUTTON:
Done:
			if ((*fontNum = SelFontNum()) < 0 || (*size = SelSize()) <= 0)
				ErrBeep();
			else {
				result = TRUE;
				done = TRUE;
			}
			break;
		case LISTMENU_BOX:
			SetStdPointer(fontDlg, POINTER_WAIT);
			*fontNum = SelFontNum();
			*size = SelSize();
			SetMenuFont(*fontNum, *size, (BOOL) (!inMenu));
			inMenu = (MenuFontNum(*fontNum, *size) != -1);
			SetMenuBox(inMenu);
			SetStdPointer(fontDlg, POINTER_ARROW);
			break;
		}
		if (hasSizeText && !done && item != -1)
			ActivateGadget(sizeTextGadg, fontDlg, NULL);
	} while (!done);
	DisposeDialog(fontDlg);
/*
	Return with success status
*/
Exit:
	if (sizeScrollList)
		DisposeScrollList(sizeScrollList);
	if (nameScrollList)
		DisposeScrollList(nameScrollList);
	EndWait();
	return (result);
}

/*
 *	Handle font submenu
 */

static BOOL DoFontMenu(WindowPtr window, UWORD sub)
{
	FontNum fontNum;
	FontSize fontSize;
	BOOL result;

	if (sub != OTHERFONT_SUBITEM &&
		(sub < FONT_SUBITEM || sub >= FONT_SUBITEM + numMenuFonts))
		return (FALSE);
/*
	Get new font from user, and make sure font is loadable
*/
	if (sub == OTHERFONT_SUBITEM) {
		fontNum = defaults.FontNum;
		fontSize = defaults.FontSize;
		result = DoFontRequest(&fontNum, &fontSize);
		if (!result)
			return (FALSE);
	}
	else {
		fontNum = menuFontNums[sub - FONT_SUBITEM];
		fontSize = menuFontSizes[sub - FONT_SUBITEM];
	}
	if (fontNum < 0 || fontNum > NumFonts() || fontSize <= 0 || fontSize > MAX_FONTSIZE) {
		Error(ERR_NO_FONT);		/* Shouldn't happen */
		return (FALSE);
	}
/*
	Change font
*/
	result = DoTextParams(window, fontNum, fontSize, -1, -1, -1, -1);
	return (result);
}

/*
 *	Set defaults for text menu from current selection
 */

void SetTextMenuDefaults(WindowPtr window)
{
	DocObjPtr docObj;
	TextObjPtr textObj;
	DocDataPtr docData = GetWRefCon(window);

	for (docObj = FirstSelected(docData); docObj; docObj = NextSelected(docObj)) {
		if (docObj->Type == TYPE_TEXT) {
			textObj = (TextObjPtr) docObj;
			defaults.FontNum	= textObj->FontNum;
			defaults.FontSize	= textObj->FontSize;
			defaults.Style		= textObj->Style;
			defaults.MiscStyle	= textObj->MiscStyle;
			defaults.Justify	= textObj->Justify;
			defaults.Spacing	= textObj->Spacing;
			break;
		}
	}
	SetTextMenu();
}

/*
 *	Set text parameters
 *	If parameter is -1 then do not change,
 */

static BOOL DoTextParams(WindowPtr window, FontNum fontNum, FontSize fontSize,
						 WORD style, WORD miscStyle, WORD justify, WORD spacing)
{
	BOOL changed;
	DocObjPtr docObj;
	DocDataPtr docData = GetWRefCon(window);

/*
	Set defaults for new objects
*/
	if (fontNum != -1)
		defaults.FontNum = fontNum;
	if (fontSize != -1)
		defaults.FontSize = fontSize;
	if (style != -1)
		defaults.Style = TextNewStyle(defaults.Style, style);
	if (miscStyle != -1)
		defaults.MiscStyle = miscStyle;
	if (justify != -1)
		defaults.Justify = justify;
	if (spacing != -1)
		defaults.Spacing = spacing;
	SetTextMenu();
/*
	Modify selected objects
*/
	if (ObjectsLocked(docData)) {
		Error(ERR_OBJ_LOCKED);
		return (FALSE);
	}
	HiliteSelectOff(window);
	for (docObj = FirstSelected(docData); docObj; docObj = NextSelected(docObj)) {
		if (docObj->Type == TYPE_TEXT || docObj->Type == TYPE_GROUP) {
			InvalObjectRect(window, docObj);	/* Obj rect size may change */
			SetObjectTextParams(docObj, fontNum, fontSize, style, miscStyle, justify,
								spacing);
			InvalObjectRect(window, docObj);
			changed = TRUE;
		}
	}
	HiliteSelectOn(window);
	if (changed) {
		SetTextMenuDefaults(window);
		DocModified(docData);
	}
	return (TRUE);
}

/*
 *	Handle Text menu
 */

BOOL DoTextMenu(WindowPtr window, UWORD item, UWORD sub, UWORD modifier)
{
	BOOL success;

	if (!IsDocWindow(window))
		return (FALSE);
	UndoOff();
	success = FALSE;
	switch (item) {
	case FONT_ITEM:
		success = DoFontMenu(window, sub);
		break;
	case PLAIN_ITEM:
		success = DoTextParams(window, -1, -1, FS_NORMAL, 0, -1, -1);
		break;
	case BOLD_ITEM:
		success = DoTextParams(window, -1, -1, FSF_BOLD, -1, -1, -1);
		break;
	case ITALIC_ITEM:
		success = DoTextParams(window, -1, -1, FSF_ITALIC, -1, -1, -1);
		break;
	case UNDERLINE_ITEM:
		success = DoTextParams(window, -1, -1, FSF_UNDERLINED, -1, -1, -1);
		break;
	case LEFTALIGN_ITEM:
		success = DoTextParams(window, -1, -1, -1, -1, JUSTIFY_LEFT, -1);
		break;
	case CENTERALIGN_ITEM:
		success = DoTextParams(window, -1, -1, -1, -1, JUSTIFY_CENTER, -1);
		break;
	case RIGHTALIGN_ITEM:
		success = DoTextParams(window, -1, -1, -1, -1, JUSTIFY_RIGHT, -1);
		break;
	case SINGLESPACE_ITEM:
		success = DoTextParams(window, -1, -1, -1, -1, -1, SPACE_SINGLE);
		break;
	case ONEANDHALFSPACE_ITEM:
		success = DoTextParams(window, -1, -1, -1, -1, -1, SPACE_1_HALF);
		break;
	case DOUBLESPACE_ITEM:
		success = DoTextParams(window, -1, -1, -1, -1, -1, SPACE_DOUBLE);
		break;
	}
	return (success);
}
