/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Toolbox library
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Border routines
 */

#include <exec/types.h>
#include <exec/memory.h>
#include <intuition/intuition.h>

#include <TypeDefs.h>

#include <Toolbox/Globals.h>
#include <Toolbox/Memory.h>
#include <Toolbox/Border.h>

#include <proto/exec.h>
#include <proto/intuition.h>

/*
 *	NewBorder
 *	Allocate and initialize a new border structure
 *		and allocate data for points (and init to 0,0)
 */

BorderPtr NewBorder(WORD leftEdge, WORD topEdge, PenNum pen, WORD numPts)
{
	BorderPtr	border;

	if ((border = MemAlloc(sizeof(Border), MEMF_CLEAR)) == NULL ||
		(border->XY = MemAlloc(numPts*sizeof(SHORT)*2, MEMF_CLEAR)) == NULL) {
		if (border)
			MemFree(border, sizeof(Border));
		return (NULL);
	}
	border->LeftEdge = leftEdge;
	border->TopEdge  = topEdge;
	border->FrontPen = pen;
	border->DrawMode = JAM1;
	border->Count    = numPts;
	return (border);
}

/*
 *	FreeBorder
 *	Free memory allocated by NewBorder
 */

void FreeBorder(register BorderPtr border)
{
	register BorderPtr	nextBorder;

	while (border) {
		nextBorder = border->NextBorder;
		if (border->XY)
			MemFree(border->XY, 2*(border->Count)*sizeof(SHORT));
		MemFree(border, sizeof(Border));
		border = nextBorder;
	}
}

/*
 *	AppendBorder
 *	Append border to end of another border
 */

void AppendBorder(register BorderPtr border1, BorderPtr border2)
{
	while (border1->NextBorder)
		border1 = border1->NextBorder;
	border1->NextBorder = border2;
}

/*
 *	LineBorder
 *	Allocate line border with given pen
 *	Return border or NULL
 */

BorderPtr LineBorder(WORD width, WORD height, PenNum pen)
{
	register SHORT		*xy;
	register BorderPtr	border;

	if ((border = NewBorder(0, 0, pen, 2)) == NULL)
		return (NULL);
	xy = border->XY;
	if (width)
		xy[2] = width - 1;
	if (height)
		xy[3] = height - 1;
	return (border);
}

/*
 *	ShadowLineBorder
 *	Allocate shadow line border
 *	If light (background) color is the same as the white color,
 *		return black box border
 */

BorderPtr ShadowLineBorder(WORD width, WORD height)
{
	register SHORT		*xy;
	register BorderPtr	border1, border2;

	if (_tbNoShadows)
		return (LineBorder(width, height, _tbPenBlack));
	if ((border1 = LineBorder(width, height, _tbPenBlack)) == NULL ||
		(border2 = LineBorder(width, height, _tbPenWhite)) == NULL) {
		FreeBorder(border1);
		return (NULL);
	}
	xy = border2->XY;
	if (width) {
		xy[1]++;
		xy[3]++;
	}
	if (height) {
		xy[0]++;
		xy[2]++;
	}
	border1->NextBorder = border2;
	return (border1);
}

/*
 *	BoxBorder
 *	Allocate box border
 *	Set to given pen, with lines inset from width/height by dx/dy
 *	Return border or NULL
 */

BorderPtr BoxBorder(WORD width, WORD height, WORD pen, WORD inset)
{
	WORD				dx, dy;
	register SHORT		*xy;
	register BorderPtr	border;

	dx = dy = inset;
	if ((border = NewBorder(dx, dy, pen, 5)) == NULL)
		return (NULL);
	xy = border->XY;
	width  -= dx + dx;
	height -= dy + dy;
	xy[2] = xy[4] = width - 1;
	xy[5] = xy[7] = height - 1;
	return (border);
}

/*
 *	ShadowBoxBorder
 *	Create shadowed box border
 *	If out is TRUE, create a shadow for object coming out from screen
 *	If light (background) color is the same as the white color,
 *		return black box border
 */

BorderPtr ShadowBoxBorder(WORD width, WORD height, WORD inset, BOOL out)
{
	register SHORT		*xy1, *xy2;
	register BorderPtr	border1, border2;

	if (_tbNoShadows)
		return (BoxBorder(width, height, _tbPenBlack, inset));
	if ((border1 = NewBorder(0, 0, _tbPenBlack, 3)) == NULL ||
		(border2 = NewBorder(0, 0, _tbPenWhite, 3)) == NULL) {
		FreeBorder(border1);
		return (NULL);
	}
	border1->NextBorder = border2;
	if (!out) {
		border1->FrontPen = _tbPenWhite;
		border2->FrontPen = _tbPenBlack;
	}
	xy1 = border1->XY;
	xy2 = border2->XY;
	xy1[0] = 1 + inset;
	xy1[1] = xy1[3] = height - 1 - inset;
	xy1[2] = xy1[4] = width - 1 - inset;
	xy1[5] = 1 + inset;
	xy2[0] = width - 1 - inset;
	xy2[1] = xy2[3] = inset;
	xy2[2] = xy2[4] = inset;
	xy2[5] = height - 1 - inset;
	return (border1);
}

/*
 *	GetStdBorder
 *	Return standard border
 */

BorderPtr GetStdBorder(WORD id, WORD width, WORD height)
{
	BorderPtr border;

	switch (id) {
	case BORDER_LINE:
		border = LineBorder(width, height, _tbPenBlack);
		break;
	case BORDER_BOX:
		border = BoxBorder(width, height, _tbPenBlack, 0);
		break;
	case BORDER_SHADOWLINE:
		border = ShadowLineBorder(width, height);
		break;
	case BORDER_SHADOWBOX:
		border = ShadowBoxBorder(width, height, 0, FALSE);
		break;
	default:
		border = NULL;
		break;
	}
	return (border);
}

/*
 *	DrawShadowBox
 *	Draw shadow box border
 */

void DrawShadowBox(RastPtr rPort, register RectPtr rect, WORD inset, BOOL out)
{
	BorderPtr	border;

	if ((border = ShadowBoxBorder(rect->MaxX - rect->MinX + 1, rect->MaxY - rect->MinY + 1,
								  inset, out)) != NULL) {
		DrawBorder(rPort, border, rect->MinX, rect->MinY);
		FreeBorder(border);
	}
}

/*
 *	DrawStdBorder
 *	Draw standard border into rPort
 */

void DrawStdBorder(RastPtr rPort, WORD id, register RectPtr rect)
{
	BorderPtr	border;

	if ((border = GetStdBorder(id, rect->MaxX - rect->MinX + 1, rect->MaxY - rect->MinY + 1)) != NULL) {
		DrawBorder(rPort, border, rect->MinX, rect->MinY);
		FreeBorder(border);
	}
}
