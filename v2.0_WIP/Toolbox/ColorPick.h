/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Toolbox library
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Color picker definitions
 */

#ifndef TOOLBOX_COLORPICK_H
#define TOOLBOX_COLORPICK_H

#ifndef EXEC_TYPES_H
#include <exec/types.h>
#endif

#ifndef TYPEDEF_H
#include <Typedefs.h>
#endif

#ifndef TOOLBOX_COLOR_H
#include <Toolbox/Color.h>
#endif

/*
 *	Color picker routines
 */

BOOL	GetColor(ScreenPtr, MsgPortPtr, BOOL (*)(IntuiMsgPtr, WORD *), TextPtr,
				 RGBColor (*)(RGBColor), RGBColor, RGBColorPtr, WORD);

BOOL	AdjustScreenColors(ScreenPtr, MsgPortPtr, BOOL (*)(IntuiMsgPtr, WORD *));

CMYKColor	RGBtoCMYK(RGBColor);
RGBColor	CMYKtoRGB(CMYKColor);

#endif
