/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Text handling routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <proto/graphics.h>

#include <string.h>

#include <Toolbox/Memory.h>
#include <Toolbox/Window.h>
#include <Toolbox/Utility.h>

#include "Draw.h"
#include "Proto.h"

/*
 *	External variables
 */

extern MsgPortPtr	mainMsgPort;

extern Defaults	defaults;
extern WORD		drawTool;

extern Options	options;

extern BOOL	cursorRepeat;

extern BOOL	doubleClick, tripleClick;

extern TextChar	strSampleText[];

/*
 *	Local variables and definitions
 */

static TextObjPtr	growTextObj;
static BOOL			wasMoved;
static WORD			growHandle;

typedef struct {
	WORD	NumLines;
	WORD	Starts[1];		/* NumLines + 1 entries */
							/* First entry is 0, last is text length */
} LineStarts, *LineStartsPtr;

#define CHAR_BUFF_SIZE	20

static TextChar		charBuff[CHAR_BUFF_SIZE];	/* Buffer for type-ahead */
static WORD			numBuffChars;
static WindowPtr	charWindow;				/* Window for type-ahead characters */

/*
 *	Local prototypes
 */

TextObjPtr	CreateText(DocDataPtr, WORD, WORD, WORD, WORD, TextPtr);

WORD	CountSpace(TextObjPtr, WORD);

WORD	WhichLine(LineStartsPtr, WORD);
WORD	HorizOffset(TextObjPtr, RastPtr, LineStartsPtr, WORD);

LineStartsPtr	CalcLineStarts(TextObjPtr);
void			DisposeLineStarts(LineStartsPtr);

void	TextTrackCreate(WindowPtr, WORD, WORD, WORD, WORD, BOOL);

void	TextTrackGrow(WindowPtr, WORD, WORD, WORD, WORD, BOOL);

void	SetSelRange(TextObjPtr, WORD, BOOL);
void	AdjustSelRange(TextObjPtr, LineStartsPtr);
WORD	HorizPosition(TextObjPtr, RastPtr, LineStartsPtr, WORD, WORD);
WORD	NearestLoc(TextObjPtr, RastPtr, LineStartsPtr, PointPtr);
WORD	NewLineLoc(TextObjPtr, WORD, BOOL);

/*
 *	Allocate a new text object
 */

TextObjPtr TextAllocate()
{
	return ((TextObjPtr) MemAlloc(sizeof(TextObj), MEMF_CLEAR));
}

/*
 *	Dispose of text object
 */

void TextDispose(TextObjPtr textObj)
{
	if (textObj->TextLen)
		MemFree(textObj->Text, textObj->TextLen);
	MemFree(textObj, sizeof(TextObj));
}

/*
 *	Create text object with given start and end positions, and contents
 */

static TextObjPtr CreateText(DocDataPtr docData, WORD xStart, WORD yStart, WORD xEnd, WORD yEnd,
							 TextPtr text)
{
	WORD len;
	TextObjPtr textObj;

	if ((textObj = (TextObjPtr) NewDocObject(CurrLayer(docData), TYPE_TEXT)) == NULL)
		return (NULL);
	SetFrameRect(&textObj->DocObj.Frame, xStart, yStart, xEnd, yEnd);
	textObj->DocObj.Flags	= (defaults.ObjFlags & ~OBJ_DO_FILL) | OBJ_DO_PEN;
	textObj->TextFlags		= 0;
	textObj->PenColor		= (defaults.PenColor != RGBCOLOR_WHITE) ?
							  defaults.PenColor : RGBCOLOR_BLACK;
	textObj->SelStart		= textObj->SelEnd = 0;
	textObj->FontNum		= defaults.FontNum;
	textObj->FontSize		= defaults.FontSize;
	textObj->Style			= defaults.Style;
	textObj->MiscStyle		= defaults.MiscStyle;
	textObj->Justify		= defaults.Justify;
	textObj->Spacing		= defaults.Spacing;
	textObj->Rotate			= 0;
	CopyRGBPat8(&defaults.FillPat, &textObj->FillPat);
/*
	Add text to object
*/
	if (text == NULL) {
		textObj->Text		= NULL;
		textObj->TextLen	= 0;
	}
	else {
		len = strlen(text);
		if ((textObj->Text = MemAlloc(len, 0)) == NULL) {
			TextDispose(textObj);
			return (NULL);
		}
		strcpy(textObj->Text, text);
		textObj->TextLen = len;
	}
	TextAdjustFrame(textObj);
	return (textObj);
}

/*
 *	Get text rectangle from object frame
 *	Text rectangle is normalized to 0,0 at upper left
 */

static void GetTextRect(TextObjPtr textObj, RectPtr rect)
{
	*rect = textObj->DocObj.Frame;
	if (textObj->Rotate)
		RotateRect(rect, 0, 0, -textObj->Rotate);
	OffsetRect(rect, -rect->MinX, -rect->MinY);
}

/*
 *	Determine which line given location is in
 *	If loc < 0 returns 0, if loc > textLen returns numLines - 1
 */

static WORD WhichLine(LineStartsPtr lineStarts, WORD loc)
{
	register WORD i;

	if (loc < 0)
		return (0);
	for (i = 0; i < lineStarts->NumLines; i++) {
		if (loc >= lineStarts->Starts[i] && loc < lineStarts->Starts[i + 1])
			return (i);
	}
	return (lineStarts->NumLines - 1);
}

/*
 *	Return the horizontal offset of line
 *	Returns value that is relative to rPort font size
 */

static WORD HorizOffset(TextObjPtr textObj, RastPtr rPort, LineStartsPtr lineStarts,
						WORD line)
{
	WORD x, len, width;
	TextPtr text;
	Rectangle rect;

	if (textObj->FontSize == 0)
		return (0);
	GetTextRect(textObj, &rect);
	width = rect.MaxX - rect.MinX + 1;
	if (textObj->FontSize != rPort->Font->tf_YSize)
		width = ((LONG) width*rPort->Font->tf_YSize)/textObj->FontSize;
	text = textObj->Text + lineStarts->Starts[line];
	len = lineStarts->Starts[line + 1] - lineStarts->Starts[line];
	while (len > 0 && (text[len - 1] == ' ' || text[len - 1] == CR))
		len--;
	switch (textObj->Justify) {
	case JUSTIFY_LEFT:
		x = 0;
		break;
	case JUSTIFY_CENTER:
		x = (width - TextLength(rPort, text, len))/2;
		break;
	case JUSTIFY_RIGHT:
		x = width - TextLength(rPort, text, len);
		break;
	default:
		x = 0;
		break;
	}
	return (x);
}

/*
 *	Set document frame from text rectangle
 *	Preserves frame's upper left coordinate
 */

static void SetTextFrame(TextObjPtr textObj, RectPtr rect)
{
	WORD x, y;

	x = textObj->DocObj.Frame.MinX;
	y = textObj->DocObj.Frame.MinY;
	textObj->DocObj.Frame = *rect;
	if (textObj->Rotate)
		RotateRect(&textObj->DocObj.Frame, 0, 0, textObj->Rotate);
	OffsetRect(&textObj->DocObj.Frame,
			   x - textObj->DocObj.Frame.MinX, y - textObj->DocObj.Frame.MinY);
}

/*
 *	Count spaces until next non-space at given location in text object
 */

static WORD CountSpace(TextObjPtr textObj, WORD loc)
{
	register WORD num;

	for (num = 0; loc + num < textObj->TextLen; num++) {
		if (textObj->Text[loc + num] != ' ')
			break;
	}
	return (num);
}

/*
 *	Calculate line breaks for text object
 */

static LineStartsPtr CalcLineStarts(TextObjPtr textObj)
{
	WORD num, loc, len, maxLen, maxWidth, prevLen;
	UWORD fontSize;
	TextPtr text;
	LineStartsPtr lineStarts;
	TextFontPtr font;
	RastPort rPort;		/* Temp rPort for length calculations */
	Rectangle rect;
	WORD lineTable[200];

	InitRastPort(&rPort);
	fontSize = FontScaleSize(textObj->FontNum, textObj->FontSize);
	font = GetNumFont(textObj->FontNum, fontSize);
	if (font == NULL)
		return (NULL);
	SetFont(&rPort, font);
/*
	Make table of line starts
*/
	GetTextRect(textObj, &rect);
	if (fontSize != textObj->FontSize)
		rect.MaxX = ((LONG) rect.MaxX*fontSize)/textObj->FontSize;
	maxWidth = rect.MaxX + 1;
	text = textObj->Text;
	maxLen = textObj->TextLen;
	num = loc = 0;
	while (loc < maxLen) {
		len = prevLen = CountSpace(textObj, loc);
		while (loc + len < maxLen) {
			while (loc + len < maxLen && text[len] != ' ' && text[len] != CR)
				len++;
			if (TextLength(&rPort, text, len) > maxWidth)
				break;
			len += CountSpace(textObj, loc + len);
			if (loc + len < maxLen && text[len] == CR) {
				prevLen = len + 1;
				break;
			}
			prevLen = len;
		}
		if (prevLen == 0) {
			for (prevLen = 1; loc + prevLen < maxLen; prevLen++) {
				if (TextLength(&rPort, text, prevLen) > maxWidth)
				   break;
			}
			prevLen--;
			if (prevLen == 0)
				prevLen = 1;
		}
		text += prevLen;
		loc += prevLen;
		lineTable[num++] = loc;
	}
	CloseFont(font);
	if (maxLen > 0 && textObj->Text[maxLen - 1] == CR)
		lineTable[num++] = maxLen;
	if (num == 0) {				/* No text in object */
		num = 1;
		lineTable[0] = 0;
	}
/*
	Create line start structure
*/
	if ((lineStarts = MemAlloc(sizeof(LineStarts) + num*sizeof(WORD), 0)) == NULL)
		return (NULL);
	lineStarts->NumLines = num;
	lineStarts->Starts[0] = 0;
	BlockMove(lineTable, &lineStarts->Starts[1], num*sizeof(WORD));
	return(lineStarts);
}

/*
 *	Dispose of line breaks from CalcLineStarts()
 */

static void DisposeLineStarts(LineStartsPtr lineStarts)
{
	MemFree(lineStarts, sizeof(LineStarts) + (lineStarts->NumLines)*sizeof(WORD));
}

/*
 *	Draw text object
 */

void TextDrawObj(RastPtr rPort, TextObjPtr textObj, RectPtr rect, RectPtr clipRect)
{
	WORD i, x, y, len;
	WORD srcWidth, srcHeight, dstWidth, dstHeight;
	FontNum fontNum;
	UWORD size, xSize, ySize, fontSize;
	TextPtr text;
	LineStartsPtr lineStarts;
	TextFontPtr font;
	RastPtr offScrnPort, xfrmPort;
	BitMapPtr bitMap;
	Rectangle srcRect, dstRect;

	if (HasFill(textObj))
		FillRect(rPort, rect, &textObj->FillPat);
/*
	Draw text
*/
	if (HasPen(textObj) && textObj->TextLen) {
		lineStarts = NULL;
		offScrnPort = xfrmPort = NULL;
		font = NULL;
		if ((lineStarts = CalcLineStarts(textObj)) == NULL)
			goto Exit;
/*
	Try to use font that is close to needed size
*/
		fontNum = textObj->FontNum;
		fontSize = textObj->FontSize;
		GetTextRect(textObj, &srcRect);
		dstRect = *rect;
		if (textObj->Rotate)
			RotateRect(&dstRect, 0, 0, -textObj->Rotate);
		OffsetRect(&dstRect, -dstRect.MinX, -dstRect.MinY);
		srcWidth  = srcRect.MaxX + 1;
		srcHeight = srcRect.MaxY + 1;
		dstWidth  = dstRect.MaxX + 1;
		dstHeight = dstRect.MaxY + 1;
		xSize = ((LONG) fontSize*dstWidth + srcWidth/2)/srcWidth;
		ySize = ((LONG) fontSize*dstHeight + srcHeight/2)/srcHeight;
/*
	Choose smaller size over larger size to avoid dropping pixels (important for printing)
	Note: This means that Topaz-11 is not a good font to use on medium res screens
*/
		if ((size = MIN(xSize, ySize)) == 0)
			goto Exit;
		size = FontScaleSize(fontNum, size);
		if ((font = GetNumFont(fontNum, size)) == NULL)
			goto Exit;
		size = font->tf_YSize;
		if (size != fontSize) {
			srcWidth  = ((LONG) srcWidth*size + fontSize/2)/fontSize;
			srcHeight = ((LONG) srcHeight*size + fontSize/2)/fontSize;
			if (srcWidth == 0 || srcHeight == 0)
				goto Exit;
			srcRect.MaxX = srcWidth - 1;
			srcRect.MaxY = srcHeight - 1;
		}
/*
	Draw into off-screen port to create blitter template
*/
		offScrnPort = CreateRastPort(srcRect.MaxX + 1, srcRect.MaxY + 1, 1);
		if (offScrnPort == NULL)
			goto Exit;
		SetFont(offScrnPort, font);
		SetSoftStyle(offScrnPort, textObj->Style, 0xFF);
		SetAPen(offScrnPort, 1);
		SetDrMd(offScrnPort, JAM1);
		for (i = 0; i < lineStarts->NumLines; i++) {
			text = textObj->Text + lineStarts->Starts[i];
			x = HorizOffset(textObj, offScrnPort, lineStarts, i);
			y = (i*size*(textObj->Spacing + 0x10) >> 4) + font->tf_Baseline;
			len = lineStarts->Starts[i + 1] - lineStarts->Starts[i];
			while (len > 0 && (text[len - 1] == ' ' || text[len - 1] == CR))
				len--;
			Move(offScrnPort, x, y);
			ExtText(offScrnPort, text, len);
		}
/*
	Scale, rotate, and flip if necessary
*/
		if (srcRect.MaxX != dstRect.MaxX || srcRect.MaxY != dstRect.MaxY ||
			textObj->Rotate || (textObj->TextFlags & (TEXT_FLIPHORIZ | TEXT_FLIPVERT))) {
			dstRect = *rect;
			OffsetRect(&dstRect, -dstRect.MinX, -dstRect.MinY);
			xfrmPort = CreateRastPort(dstRect.MaxX + 1, dstRect.MaxY + 1, 1);
			if (xfrmPort == NULL)
				goto Exit;
			TransformBitMap(offScrnPort, xfrmPort, &srcRect, &dstRect, textObj->Rotate,
							(textObj->TextFlags & TEXT_FLIPHORIZ),
							(textObj->TextFlags & TEXT_FLIPVERT));
		}
		else
			xfrmPort = offScrnPort;
/*
	Now blit into rPort with correct color and size
*/
		RGBForeColor(rPort, textObj->PenColor);
		SetDrMd(rPort, JAM1);
		bitMap = xfrmPort->BitMap;
		ExtBltTemplate(bitMap->Planes[0], 0, bitMap->BytesPerRow, rPort,
						rect->MinX, rect->MinY, rect->MaxX - rect->MinX + 1,
						rect->MaxY - rect->MinY + 1);
Exit:
		if (xfrmPort && xfrmPort != offScrnPort)
			DisposeRastPort(xfrmPort);
		if (offScrnPort)
			DisposeRastPort(offScrnPort);
		if (font)
			CloseFont(font);
		if (lineStarts)
			DisposeLineStarts(lineStarts);
	}
}

/*
 *	Draw outline of text rect (for creation/dragging)
 *	Offset is in document coordinates
 */

void TextDrawOutline(WindowPtr window, TextObjPtr textObj, WORD xOffset, WORD yOffset)
{
	RectDrawOutline(window, (RectObjPtr) textObj, xOffset, yOffset);
}

/*
 *	Draw selection handles for text object
 */

void TextHilite(WindowPtr window, TextObjPtr textObj)
{
	RectHilite(window, (RectObjPtr) textObj);
}

/*
 *	Set text pen color
 */

void TextSetPenColor(TextObjPtr textObj, RGBColor penColor)
{
	textObj->PenColor = penColor;
}

/*
 *	Set pen size (does nothing)
 */

void TextSetPenSize(TextObjPtr textObj, WORD penWidth, WORD penHeight)
{
}

/*
 *	Set text fill pattern
 */

void TextSetFillPat(TextObjPtr textObj, RGBPat8Ptr fillPat)
{
	CopyRGBPat8(fillPat, &textObj->FillPat);
}

/*
 *	Return new style bits based upon old style and new style bit
 */

WORD TextNewStyle(WORD oldStyle, WORD newStyleBit)
{
	if (newStyleBit == FS_NORMAL)
		return (FS_NORMAL);
	if (oldStyle & newStyleBit)
		return (oldStyle & ~newStyleBit);
	return (oldStyle | newStyleBit);
}

/*
 *	Set text parameters
 *	If parameter is -1 then do not change
 */

void TextSetTextParams(register TextObjPtr textObj, FontNum fontNum, FontSize fontSize,
					   WORD style, WORD miscStyle, WORD justify, WORD spacing)
{
	if (fontNum != -1)
		textObj->FontNum = fontNum;
	if (fontSize != -1)
		textObj->FontSize = fontSize;
	if (style != -1)
		textObj->Style = TextNewStyle(textObj->Style, style);
	if (miscStyle != -1)
		textObj->MiscStyle = miscStyle;
	if (justify != -1)
		textObj->Justify = justify;
	if (spacing != -1)
		textObj->Spacing = spacing;
	TextAdjustFrame(textObj);
}

/*
 *	Rotate text by given angle about center
 */

void TextRotate(TextObjPtr textObj, WORD cx, WORD cy, WORD angle)
{
	textObj->Rotate = NormalizeAngle(textObj->Rotate + angle);
	RotateRect(&textObj->DocObj.Frame, cx, cy, angle);
}

/*
 *	Flip text horizontally or vertically about center
 */

void TextFlip(TextObjPtr textObj, WORD cx, WORD cy, BOOL horiz)
{
	textObj->Rotate = NormalizeAngle(textObj->Rotate);
	if (textObj->Rotate % 180)
		horiz = !horiz;
	if (horiz) {
		if (textObj->TextFlags & TEXT_FLIPHORIZ)
			textObj->TextFlags &= ~TEXT_FLIPHORIZ;
		else
			textObj->TextFlags |= TEXT_FLIPHORIZ;
	}
	else {
		if (textObj->TextFlags & TEXT_FLIPVERT)
			textObj->TextFlags &= ~TEXT_FLIPVERT;
		else
			textObj->TextFlags |= TEXT_FLIPVERT;
	}
	if ((textObj->TextFlags & TEXT_FLIPHORIZ) && (textObj->TextFlags & TEXT_FLIPVERT)) {
		textObj->TextFlags &= ~(TEXT_FLIPHORIZ | TEXT_FLIPVERT);
		textObj->Rotate = NormalizeAngle(textObj->Rotate + 180);
	}
	FlipRect(&textObj->DocObj.Frame, cx, cy, horiz);
}

/*
 *	Adjust text line breaks to new frame
 */

void TextScale(TextObjPtr textObj, RectPtr frame)
{
	textObj->DocObj.Frame = *frame;
	TextAdjustFrame(textObj);
}

/*
 *	Determine if point is in text
 *	Point is relative to object rectangle
 */

BOOL TextSelect(TextObjPtr textObj, PointPtr pt)
{
	return (TRUE);
}

/*
 *	Return handle number of given point, or -1 if not in handle
 *	Point is relative to object rectangle
 */

WORD TextHandle(TextObjPtr textObj, PointPtr pt, RectPtr handleRect)
{
	return (RectHandle((RectObjPtr) textObj, pt, handleRect));
}

/*
 *	Duplicate text data to new object
 *	Return success status
 */

BOOL TextDupData(TextObjPtr textObj, TextObjPtr newObj)
{
	if ((newObj->Text = MemAlloc(textObj->TextLen, 0)) == NULL)
		return (FALSE);
	BlockMove(textObj->Text, newObj->Text, textObj->TextLen);
	newObj->TextLen		= textObj->TextLen;

	newObj->TextFlags	= textObj->TextFlags;
	newObj->PenColor	= textObj->PenColor;
	newObj->SelStart	= newObj->SelEnd = 0;
	newObj->FontNum		= textObj->FontNum;
	newObj->FontSize	= textObj->FontSize;
	newObj->Style		= textObj->Style;
	newObj->MiscStyle	= textObj->MiscStyle;
	newObj->Justify		= textObj->Justify;
	newObj->Spacing		= textObj->Spacing;
	newObj->Rotate		= textObj->Rotate;
	CopyRGBPat8(&textObj->FillPat, &newObj->FillPat);
	return (TRUE);
}

/*
 *	Adjust text frame to contain all text
 */

void TextAdjustFrame(TextObjPtr textObj)
{
	WORD height;
	LineStartsPtr lineStarts;
	Rectangle rect;

	if ((lineStarts = CalcLineStarts(textObj)) != NULL) {
		height = (lineStarts->NumLines*textObj->FontSize*(textObj->Spacing + 0x10)) >> 4;
		GetTextRect(textObj, &rect);
		rect.MaxY = rect.MinY + height - 1;
		SetTextFrame(textObj, &rect);
		DisposeLineStarts(lineStarts);
	}
}

/*
 *	Draw routine for tracking text creation
 */

static void TextTrackCreate(WindowPtr window, WORD xStart, WORD yStart, WORD xEnd, WORD yEnd,
							BOOL change)
{
	RectObj rectObj;

	if (change) {
		BlockClear(&rectObj, sizeof(RectObj));
		SetFrameRect(&rectObj.DocObj.Frame, xStart, yStart, xEnd, yEnd);
		RectDrawOutline(window, &rectObj, 0, 0);
	}
}

/*
 *	Create text object
 */

DocObjPtr TextCreate(WindowPtr window, UWORD modifier, WORD mouseX, WORD mouseY)
{
	WORD xStart, yStart, xEnd, yEnd;
	WORD size, xSize, ySize, baseline;
	TextFontPtr font;
	DocDataPtr docData = GetWRefCon(window);

/*
	Track object size
*/
	WindowToDoc(window, mouseX, mouseY, &xStart, &yStart);
	SnapToGrid(docData, &xStart, &yStart);
	xEnd = xStart;
	yEnd = yStart;
	TrackMouse(window, modifier, &xEnd, &yEnd, TextTrackCreate, NULL);
/*
	Adjust for rect size (in case user just clicked the mouse)
*/
	ySize = defaults.FontSize;
	size = FontScaleSize(defaults.FontNum, ySize);
	if (size <= 0 || (font = GetNumFont(defaults.FontNum, size)) == NULL)
		return (NULL);
	xSize = font->tf_XSize;
	baseline = font->tf_Baseline;
	if (font->tf_YSize != ySize) {
		xSize = ((LONG) xSize*ySize)/font->tf_YSize;
		baseline = ((LONG) baseline*ySize)/font->tf_YSize;
	}
	CloseFont(font);
	if (xEnd < xStart + xSize && yEnd < yStart + ySize) {
		xEnd = xStart + 20*xSize;
		if (xEnd >= docData->DocWidth)
			xEnd = docData->DocWidth - 1;
		yStart -= baseline;
		if (yStart < 0)
			yStart = 0;
		defaults.Justify = JUSTIFY_LEFT;	/* Simple click gives left justify */
	}
	yEnd = yStart + ySize;
/*
	Create text object
*/
	return ((DocObjPtr) CreateText(docData, xStart, yStart, xEnd, yEnd, NULL));
}

/*
 *	Draw routine for tracking text grow
 */

static void TextTrackGrow(WindowPtr window, WORD xStart, WORD yStart, WORD xEnd, WORD yEnd,
						  BOOL change)
{
	TextObj textObj;
	Rectangle rect;

	if (change && (xStart != xEnd || yStart != yEnd || wasMoved)) {
		BlockClear(&textObj, sizeof(TextObj));
		rect = growTextObj->DocObj.Frame;
		if (growHandle == 0 || growHandle == 2)
			rect.MinX = xEnd;
		else
			rect.MaxX = xEnd;
		if (growHandle == 0 || growHandle == 1)
			rect.MinY = yEnd;
		else
			rect.MaxY = yEnd;
		SetFrameRect(&textObj.DocObj.Frame, rect.MinX, rect.MinY, rect.MaxX, rect.MaxY);
		TextDrawOutline(window, &textObj, 0, 0);
		wasMoved = TRUE;
	}
}

/*
 *	Track mouse and change text frame shape
 */

void TextGrow(WindowPtr window, TextObjPtr textObj, UWORD modifier, WORD mouseX, WORD mouseY)
{
	WORD xStart, yStart, xEnd, yEnd;
	DocDataPtr docData = GetWRefCon(window);
	Point pt;
	Rectangle rect, handleRect;

	growTextObj = textObj;
	GetHandleRect(window, &handleRect);
/*
	Track handle movement
*/
	wasMoved = FALSE;
	WindowToDoc(window, mouseX, mouseY, &xStart, &yStart);
	pt.x = xStart - textObj->DocObj.Frame.MinX;
	pt.y = yStart - textObj->DocObj.Frame.MinY;
	if ((growHandle = TextHandle(textObj, &pt, &handleRect)) == -1)
		return;
	SnapToGrid(docData, &xStart, &yStart);
	xEnd = xStart;
	yEnd = yStart;
	TrackMouse(window, modifier, &xEnd, &yEnd, TextTrackGrow, NULL);
/*
	Set new object dimensions
*/
	if (xEnd != xStart || yEnd != yStart) {
		InvalObjectRect(window, (DocObjPtr) textObj);
		HiliteSelectOff(window);
		rect = textObj->DocObj.Frame;
		if (growHandle == 0 || growHandle == 2)
			rect.MinX = xEnd;
		else
			rect.MaxX = xEnd;
		if (growHandle == 0 || growHandle == 1)
			rect.MinY = yEnd;
		else
			rect.MaxY = yEnd;
		SetFrameRect(&textObj->DocObj.Frame, rect.MinX, rect.MinY, rect.MaxX, rect.MaxY);
		TextAdjustFrame(textObj);
		InvalObjectRect(window, (DocObjPtr) textObj);
		HiliteSelectOn(window);
		InvalObjectRect(window, (DocObjPtr) textObj);
	}
}

/*
 *	Return TRUE if currently editing text
 */

BOOL TextInEdit(WindowPtr window)
{
	DocObjPtr docObj;
	DocDataPtr docData = GetWRefCon(window);

	docObj = FirstSelected(docData);
	if (docObj == NULL || drawTool != TOOL_TEXT || docObj->Type != TYPE_TEXT ||
		((TextObjPtr) docObj)->Rotate != 0 ||
		(((TextObjPtr) docObj)->TextFlags & (TEXT_FLIPHORIZ | TEXT_FLIPVERT)))
		return (FALSE);
/*
	Editing text, so lets make sure no other objects are selected
*/
	while ((docObj = NextSelected(docObj)) != NULL)
		UnSelectObject(docObj);
	return (TRUE);
}

/*
 *	Set selection range
 */

static void SetSelRange(register TextObjPtr textObj, WORD loc, BOOL selecting)
{
	if (!selecting)
		textObj->SelStart = textObj->SelEnd = textObj->DragStart = loc;
	else {
		if (loc < textObj->DragStart) {
			textObj->SelStart = loc;
			textObj->SelEnd   = textObj->DragStart;
		}
		else {
			textObj->SelStart = textObj->DragStart;
			textObj->SelEnd   = loc;
		}
	}
}

/*
 *	Adjust select range for double-click and triple-click
 */

static void AdjustSelRange(register TextObjPtr textObj, LineStartsPtr lineStarts)
{
	WORD line;
	TextPtr text;

	text = textObj->Text;
	if (doubleClick) {
		while (textObj->SelStart > 0 && wordChar[text[textObj->SelStart - 1]])
			textObj->SelStart--;
		if (textObj->SelStart < textObj->TextLen && text[textObj->SelStart] == QUOTE_OPEN)
			textObj->SelStart++;
		while (textObj->SelEnd < textObj->TextLen && wordChar[text[textObj->SelEnd]])
			textObj->SelEnd++;
		if (textObj->SelEnd && text[textObj->SelEnd - 1] == QUOTE_CLOSE)
			textObj->SelEnd--;
		while (textObj->SelEnd < textObj->TextLen && text[textObj->SelEnd] == ' ')
			textObj->SelEnd++;
	}
	else if (tripleClick) {
		line = WhichLine(lineStarts, textObj->SelStart);
		textObj->SelStart = lineStarts->Starts[line];
		line = WhichLine(lineStarts, textObj->SelEnd);
		textObj->SelEnd   = lineStarts->Starts[line + 1];
	}
}

/*
 *	Add characters to text object
 *	Pass 0 for numAdd to simply remove selected text
 */

BOOL TextAddChars(TextObjPtr textObj, TextPtr text, WORD numAdd)
{
	WORD i, oldLen, newLen, selStart, selEnd, numDel;
	TextPtr oldText, newText;

	oldText  = textObj->Text;
	oldLen   = textObj->TextLen;
	selStart = textObj->SelStart;
	selEnd   = textObj->SelEnd;
/*
	Sanity check
*/
	if (selStart < 0)
		selStart = 0;
	if (selStart > oldLen)
		selStart = oldLen;
	if (selEnd < selStart)
		selEnd = selStart;
	if (selEnd > oldLen)
		selEnd = oldLen;
	if (numAdd < 0)
		numAdd = 0;
/*
	Add or remove characters
*/
	numDel = selEnd - selStart;
	newLen = oldLen - numDel + numAdd;
	if (newLen > 0) {
		if ((newText = MemAlloc(newLen, 0)) == NULL)
			return (FALSE);
	}
	else {
		newText = NULL;
		newLen  = 0;				/* To be sure */
	}
	for (i = 0; i < selStart; i++)
		newText[i] = oldText[i];
	for (i = 0; i < numAdd; i++)
		newText[i + selStart] = text[i];
	for (i = selEnd; i < oldLen; i++)
		newText[i - numDel + numAdd] = oldText[i];
	if (oldText)
		MemFree(oldText, oldLen);
/*
	Set new data
*/
	textObj->Text     = newText;
	textObj->TextLen  = newLen;
	SetSelRange(textObj, selStart + numAdd, FALSE);
	return (TRUE);
}

/*
 *	Insert character to current text
 */

void TextInsert(WindowPtr window, TextChar ch)
{
	DocDataPtr docData = GetWRefCon(window);

	if (!TextInEdit(window) || ((ch & 0x7F) < ' ' && ch != CR)) {
		ErrBeep();
		return;
	}
/*
	If character is for a different window, or buffer is full then update
*/
	if (window != charWindow || numBuffChars >= CHAR_BUFF_SIZE)
		TextUpdate();
	charWindow = window;
/*
	Add character to buffer
*/
	charBuff[numBuffChars++] = ch;
	DocModified(docData);
}

/*
 *	Update buffered keystrokes
 */

void TextUpdate()
{
	TextObjPtr textObj;
	DocDataPtr docData;

	if (numBuffChars) {
		docData = GetWRefCon(charWindow);
		textObj = (TextObjPtr) FirstSelected(docData);
		if (textObj && textObj->DocObj.Type == TYPE_TEXT) {
			HiliteSelectOff(charWindow);			/* Turn cursor off */
			if (TextAddChars(textObj, charBuff, numBuffChars)) {
				InvalObjectRect(charWindow, (DocObjPtr) textObj);
				TextAdjustFrame(textObj);
				InvalObjectRect(charWindow, (DocObjPtr) textObj);
				UpdateWindow(charWindow);			/* For snappier response */
			}
			HiliteSelectOn(charWindow);				/* Turn cursor on */
		}
		numBuffChars = 0;
	}
}

/*
 *	Delete from current text
 *	If selection range then delete selection
 *	Otherwise, BS deletes backward, DEL deletes forward, anything else does nothing
 */

void TextDelete(WindowPtr window, TextChar ch, UWORD modifier)
{
	WORD len, selStart, selEnd;
	TextObjPtr textObj;
	DocDataPtr docData = GetWRefCon(window);

	if (!TextInEdit(window) || (modifier & MODIFIER_SEL)) {
		ErrBeep();
		return;
	}
	textObj = (TextObjPtr) FirstSelected(docData);
/*
	If no selection range, then make one
*/
	selStart = textObj->SelStart;
	selEnd   = textObj->SelEnd;
	len      = textObj->TextLen;
	if (selStart == selEnd) {
		if (ch == BS)
			selStart--;
		else if (ch == DEL)
			selEnd++;
	}
	if (selStart < 0)
		selStart = 0;
	if (selStart > len)
		selStart = len;
	if (selEnd < selStart)
		selEnd = selStart;
	if (selEnd > len)
		selEnd = len;
	if (selStart == selEnd)
		return;
/*
	Remove characters and set refresh region
*/
	HiliteSelectOff(window);			/* Turn cursor off */
	textObj->SelStart = selStart;
	textObj->SelEnd   = selEnd;
	if (TextAddChars(textObj, NULL, 0)) {
		InvalObjectRect(window, (DocObjPtr) textObj);
		TextAdjustFrame(textObj);
		InvalObjectRect(window, (DocObjPtr) textObj);
		UpdateWindow(window);			/* For snappier response */
	}
	HiliteSelectOn(window);				/* Turn cursor on */
	DocModified(docData);
}

/*
 *	Remove empty non-selected text objects
 */

void TextPurge(DocDataPtr docData)
{
	DocLayerPtr docLayer;
	DocObjPtr docObj, nextObj;

	for (docLayer = BottomLayer(docData); docLayer; docLayer = NextLayer(docLayer)) {
		docObj = BottomObject(docLayer);
		while (docObj) {
			nextObj = NextObj(docObj);
			if ((docLayer != CurrLayer(docData) || !ObjectSelected(docObj)) &&
				docObj->Type == TYPE_TEXT && !HasFill(docObj) &&
				((TextObjPtr) docObj)->TextLen == 0) {
				DetachObject(docLayer, docObj);
				DisposeDocObject(docObj);
			}
			docObj = nextObj;
		}
	}
}

/*
 *	Return the horizontal position of given line and location within line
 *	Result is relative to text obj's frame
 *	Returns value that is relative to textObj font size (unlike HorizOffset())
 */

static WORD HorizPosition(TextObjPtr textObj, RastPtr rPort, LineStartsPtr lineStarts,
						  WORD line, WORD loc)
{
	WORD x, size;

	x = HorizOffset(textObj, rPort, lineStarts, line)
		+ TextLength(rPort, textObj->Text + lineStarts->Starts[line], loc);
	size = rPort->Font->tf_YSize;
	if (x && textObj->FontSize != size)
		x = ((LONG) x*textObj->FontSize + size/2)/size;
	return (x);
}

/*
 *	Draw text cursor
 */

void TextDrawCursor(WindowPtr window)
{
	TextPtr text;
	WORD i, selStart, selEnd, height;
	WORD startLine, endLine, startLoc, endLoc;
	UWORD size, fontSize;
	TextObjPtr textObj;
	DocDataPtr docData = GetWRefCon(window);
	LineStartsPtr lineStarts;
	TextFontPtr font;
	Rectangle textRect, rect;
	RastPort rPort;

	if (!TextInEdit(window)) {
		ErrBeep();
		return;
	}
	textObj = (TextObjPtr) FirstSelected(docData);
	fontSize = textObj->FontSize;
	selStart = textObj->SelStart;
	selEnd = textObj->SelEnd;
/*
	Set up for calculations
*/
	InitRastPort(&rPort);
	lineStarts = NULL;
	font = NULL;
	size = FontScaleSize(textObj->FontNum, fontSize);
	if ((lineStarts = CalcLineStarts(textObj)) == NULL ||
		(font = GetNumFont(textObj->FontNum, size)) == NULL)
		goto Exit;
	SetFont(&rPort, font);
/*
	Find line(s) containing selection
*/
	startLine = WhichLine(lineStarts, selStart);
	endLine   = WhichLine(lineStarts, selEnd);
	startLoc = selStart - lineStarts->Starts[startLine];
	endLoc   = selEnd - lineStarts->Starts[endLine];
/*
	Hilite selection range, or draw insertion point
*/
	textRect = textObj->DocObj.Frame;
	height = (selStart == selEnd) ?
			 fontSize : (fontSize*(textObj->Spacing + 0x10) >> 4);
	for (i = startLine; i <= endLine; i++) {
		text = textObj->Text + lineStarts->Starts[i];
		if (i > startLine)
			rect.MinX = textRect.MinX;
		else
			rect.MinX = textRect.MinX
						+ HorizPosition(textObj, &rPort, lineStarts, startLine, startLoc);
		if (i < endLine)
			rect.MaxX = textRect.MaxX;
		else
			rect.MaxX = textRect.MinX
						+ HorizPosition(textObj, &rPort, lineStarts, endLine, endLoc);
		rect.MinY = textRect.MinY + (i*fontSize*(textObj->Spacing + 0x10) >> 4);
		rect.MaxY = rect.MinY + height;
		DocToWindowRect(window, &rect, &rect);
		if (rect.MaxY > rect.MinY)
			rect.MaxY--;					/* Avoid overlapping fills */
		SetDrMd(window->RPort, COMPLEMENT);
		RectFill(window->RPort, rect.MinX, rect.MinY, rect.MaxX, rect.MaxY);
	}
/*
	Done
*/
Exit:
	if (font)
		CloseFont(font);
	if (lineStarts)
		DisposeLineStarts(lineStarts);
}

/*
 *	Blink cursor if text is selected
 */

void TextBlinkCursor(WindowPtr window)
{
	TextObjPtr textObj;
	DocDataPtr docData = GetWRefCon(window);

	if (!TextInEdit(window) || options.BlinkPeriod == 0)
		return;
	textObj = (TextObjPtr) FirstSelected(docData);
	if (textObj->SelStart != textObj->SelEnd || docData->BlinkCount-- > 0)
		return;
	docData->BlinkCount = options.BlinkPeriod;
	if (AttemptLockLayerRom(window->WLayer) == FALSE)
		return;
	TextDrawCursor(window);
	UnlockLayerRom(window->WLayer);
	if (docData->Flags & DOC_HILITE)
		docData->Flags &= ~DOC_HILITE;
	else
		docData->Flags |= DOC_HILITE;
}

/*
 *	Return the text location of given point in text object
 *	(relative to object's upper left corner)
 */

static WORD NearestLoc(TextObjPtr textObj, RastPtr rPort, LineStartsPtr lineStarts,
					   PointPtr pt)
{
	WORD i, loc, len, line;
	WORD dots, prevDots;

	if (textObj->FontSize == 0)
		return (0);
	line = pt->y/(textObj->FontSize*(textObj->Spacing + 0x10) >> 4);
	if (line < 0)
		return (0);
	if (line >= lineStarts->NumLines)
		return (textObj->TextLen);
	loc = lineStarts->Starts[line];
	len = lineStarts->Starts[line + 1] - loc;
	for (i = 0; i <= len; i++) {
		dots = pt->x - HorizPosition(textObj, rPort, lineStarts, line, i);
		if (dots < 0)
			break;
		prevDots = dots;
	}
	if (i > len || (i > 0 && ABS(prevDots) < ABS(dots)))
		i--;
	if (i > 0 && i == len && line < lineStarts->NumLines - 1)
		i--;
	return (loc + i);
}

/*
 *	Find loc in text object in next or prev line at same horizontal position
 */

static WORD NewLineLoc(TextObjPtr textObj, WORD loc, BOOL up)
{
	WORD i, line, len, dots, prevDots, x;
	UWORD size;
	TextFontPtr font;
	LineStartsPtr lineStarts;
	RastPort rPort;

	InitRastPort(&rPort);
	lineStarts = NULL;
	font = NULL;
	size = FontScaleSize(textObj->FontNum, textObj->FontSize);
	if ((lineStarts = CalcLineStarts(textObj)) == NULL ||
		(font = GetNumFont(textObj->FontNum, size)) == NULL)
		goto Exit;
	SetFont(&rPort, font);
/*
	Get line and x dot position relative to textObj bounds
*/
	line = WhichLine(lineStarts, loc);
	x = HorizPosition(textObj, &rPort, lineStarts, line, loc - lineStarts->Starts[line]);
/*
	Move to desired line
*/
	if (up)
		line--;
	else
		line++;
/*
	Get location of this position
*/
	if (line >= 0 && line < lineStarts->NumLines) {
		loc = lineStarts->Starts[line];
		len = lineStarts->Starts[line + 1] - loc;
		for (i = 0; i <= len; i++) {
			dots = x - HorizPosition(textObj, &rPort, lineStarts, line, i);
			if (dots < 0)
				break;
			prevDots = dots;
		}
		if (i > len || (i > 0 && ABS(prevDots) < ABS(dots)))
			i--;
		if (i > 0 && i == len && line < lineStarts->NumLines - 1)
			i--;
		loc += i;
	}
/*
	Done
*/
Exit:
	if (font)
		CloseFont(font);
	if (lineStarts)
		DisposeLineStarts(lineStarts);
	return (loc);
}

/*
 *	Move the text cursor up
 */

void TextCursorUp(WindowPtr window, UWORD modifier)
{
	WORD loc, selStart, selEnd;
	BOOL selecting;
	TextObjPtr textObj;
	DocDataPtr docData = GetWRefCon(window);

	if (!TextInEdit(window)) {
		ErrBeep();
		return;
	}
	textObj = (TextObjPtr) FirstSelected(docData);
	selecting = (modifier & MODIFIER_SEL);
/*
	Get move location
*/
	selStart  = textObj->SelStart;
	selEnd    = textObj->SelEnd;
	if (selStart == textObj->DragStart)
		loc = selEnd;
	else {
		loc = selStart;
		textObj->DragStart = selEnd;
	}
/*
	If range selected, just go to start of range
	Otherwise move up one line
*/
	if (selStart != selEnd && !selecting)
		loc = selStart;
	else
		loc = NewLineLoc(textObj, loc, TRUE);
	HiliteSelectOff(window);		/* Turn cursor off */
	SetSelRange(textObj, loc, selecting);
	HiliteSelectOn(window);			/* Turn cursor on */
	SetEditMenu();
}

/*
 *	Move the text cursor down
 */

void TextCursorDown(WindowPtr window, UWORD modifier)
{
	WORD loc, selStart, selEnd;
	BOOL selecting;
	TextObjPtr textObj;
	DocDataPtr docData = GetWRefCon(window);

	if (!TextInEdit(window)) {
		ErrBeep();
		return;
	}
	textObj = (TextObjPtr) FirstSelected(docData);
	selecting = (modifier & MODIFIER_SEL);
/*
	Get move location
*/
	selStart  = textObj->SelStart;
	selEnd    = textObj->SelEnd;
	if (selEnd == textObj->DragStart)
		loc = selStart;
	else {
		loc = selEnd;
		textObj->DragStart = selStart;
	}
/*
	If range selected, just go to end of range
	Otherwise move down one line
*/
	if (selStart != selEnd && !selecting)
		loc = selEnd;
	else
		loc = NewLineLoc(textObj, loc, FALSE);
	HiliteSelectOff(window);		/* Turn cursor off */
	SetSelRange(textObj, loc, selecting);
	HiliteSelectOn(window);			/* Turn cursor on */
	SetEditMenu();
}

/*
 *	Move the text cursor left
 */

void TextCursorLeft(WindowPtr window, UWORD modifier)
{
	WORD loc, selStart, selEnd;
	BOOL selecting;
	TextObjPtr textObj;
	DocDataPtr docData = GetWRefCon(window);

	if (!TextInEdit(window)) {
		ErrBeep();
		return;
	}
	textObj = (TextObjPtr) FirstSelected(docData);
	selecting = (modifier & MODIFIER_SEL);
/*
	Get move location
*/
	selStart  = textObj->SelStart;
	selEnd    = textObj->SelEnd;
	if (selStart == textObj->DragStart)
		loc = selEnd;
	else {
		loc = selStart;
		textObj->DragStart = selEnd;
	}
/*
	If range selected, just go to start of range
	Otherwise move back one character
*/
	if (selStart != selEnd && !selecting)
		loc = selStart;
	else {
		loc--;
		if (loc < 0)
			loc = 0;
	}
	HiliteSelectOff(window);		/* Turn cursor off */
	SetSelRange(textObj, loc, selecting);
	HiliteSelectOn(window);			/* Turn cursor on */
	SetEditMenu();
}

/*
 *	Move the text cursor right
 */

void TextCursorRight(WindowPtr window, UWORD modifier)
{
	WORD loc, selStart, selEnd;
	BOOL selecting;
	TextObjPtr textObj;
	DocDataPtr docData = GetWRefCon(window);

	if (!TextInEdit(window)) {
		ErrBeep();
		return;
	}
	textObj = (TextObjPtr) FirstSelected(docData);
	selecting = (modifier & MODIFIER_SEL);
/*
	Get move location
*/
	selStart  = textObj->SelStart;
	selEnd    = textObj->SelEnd;
	if (selEnd == textObj->DragStart)
		loc = selStart;
	else {
		loc = selEnd;
		textObj->DragStart = selStart;
	}
/*
	If range selected, just go to end of range
	Otherwise move forward one character
*/
	if (selStart != selEnd && !selecting)
		loc = selEnd;
	else {
		loc++;
		if (loc > textObj->TextLen)
			loc = textObj->TextLen;
	}
	HiliteSelectOff(window);		/* Turn cursor off */
	SetSelRange(textObj, loc, selecting);
	HiliteSelectOn(window);			/* Turn cursor on */
	SetEditMenu();
}

/*
 *	Handle mouse click in text object
 *	Return FALSE if click was not handled (object is rotated, etc.)
 */

BOOL TextMouseClick(WindowPtr window, UWORD modifier, WORD mouseX, WORD mouseY)
{
	WORD loc, newLoc;
	UWORD size;
	BOOL doRange;
	DocObjPtr docObj;
	TextObjPtr textObj;
	TextFontPtr font;
	LineStartsPtr lineStarts;
	DocDataPtr docData = GetWRefCon(window);
	Point docPt;
	RastPort rPort;

	WindowToDoc(window, mouseX, mouseY, &docPt.x, &docPt.y);
	for (docObj = LastObject(docData); docObj; docObj = PrevObj(docObj)) {
		if (PointInObject(docObj, &docPt))
			break;
	}
	textObj = (TextObjPtr) docObj;
	if (textObj == NULL || textObj->DocObj.Type != TYPE_TEXT ||
		textObj->Rotate != 0 ||
		(textObj->TextFlags & (TEXT_FLIPHORIZ | TEXT_FLIPVERT)) != 0)
		return (FALSE);
	doRange = (ObjectSelected((DocObjPtr) textObj) && (modifier & SHIFTKEYS));
/*
	Found editable text object under mouse
*/
	HiliteSelectOff(window);
	UnSelectAllObjects(docData);
	SelectObject((DocObjPtr) textObj);
/*
	Find location in text
*/
	InitRastPort(&rPort);
	lineStarts = NULL;
	font = NULL;
	size = FontScaleSize(textObj->FontNum, textObj->FontSize);
	if ((lineStarts = CalcLineStarts(textObj)) == NULL ||
		(font = GetNumFont(textObj->FontNum, size)) == NULL) {
		SetSelRange(textObj, 0, FALSE);
		HiliteSelectOn(window);
		goto Exit;
	}
	SetFont(&rPort, font);
	OffsetPoint(&docPt, -textObj->DocObj.Frame.MinX, -textObj->DocObj.Frame.MinY);
	loc = NearestLoc(textObj, &rPort, lineStarts, &docPt);
	SetSelRange(textObj, loc, doRange);
	AdjustSelRange(textObj, lineStarts);
	HiliteSelectOn(window);
/*
	Track mouse movement and select text
*/
	while (WaitMouseUp(mainMsgPort, window)) {
		if (mouseX == window->MouseX && mouseY == window->MouseY)
			continue;
		mouseX = window->MouseX;
		mouseY = window->MouseY;
/*
	Get new position in text
*/
		WindowToDoc(window, mouseX, mouseY, &docPt.x, &docPt.y);
		OffsetPoint(&docPt, -textObj->DocObj.Frame.MinX, -textObj->DocObj.Frame.MinY);
		newLoc = NearestLoc(textObj, &rPort, lineStarts, &docPt);
		if (newLoc != loc) {
			HiliteSelectOff(window);
			SetSelRange(textObj, newLoc, TRUE);
			AdjustSelRange(textObj, lineStarts);
			HiliteSelectOn(window);
			loc = newLoc;
		}
	}
/*
	Done
*/
Exit:
	if (font)
		CloseFont(font);
	if (lineStarts)
		DisposeLineStarts(lineStarts);
	SetTextMenuDefaults(window);
	return (TRUE);
}

/*
 *	Create new textObj given REXX arguments
 */

TextObjPtr TextNewREXX(DocDataPtr docData, TextPtr args)
{
	Point pt1, pt2;

/*
	Get start and end points
*/
	args = GetArgPoint(args, docData, &pt1);
	if (args)
		args = GetArgPoint(args, docData, &pt2);
	if (args == NULL ||
		pt1.x < 0 || pt1.y < 0 || pt1.x >= docData->DocWidth || pt1.y >= docData->DocHeight ||
		pt2.x < 0 || pt2.y < 0 || pt2.x >= docData->DocWidth || pt2.y >= docData->DocHeight)
		return (NULL);
/*
	Create text object
*/
	return (CreateText(docData, pt1.x, pt1.y, pt2.x, pt2.y, args));
}
