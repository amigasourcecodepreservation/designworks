/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Selection highlighting & cursor handling routines
 */

#include <exec/types.h>
#include <graphics/gfxmacros.h>
#include <intuition/intuition.h>

#include <proto/graphics.h>

#include <Toolbox/Graphics.h>
#include <Toolbox/Window.h>
#include <Toolbox/Utility.h>

#include "Draw.h"
#include "Proto.h"

/*
 *	External variables
 */

extern Options	options;

extern BOOL	inMacro, drawOff;

/*
 *	Local variables and definitions
 */

#define HANDLE_WIDTH	5
#define HANDLE_HEIGHT	5

/*
 *	Local prototypes
 */

void	DrawHilite(WindowPtr);

BOOL	MoveObjects(WindowPtr, WORD, WORD);

/*
 *	Get handle rect in document coordinates for given window
 */

void GetHandleRect(WindowPtr window, RectPtr handleRect)
{
	WORD width, height;
	Fixed scale;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
	Rectangle rect;

	scale = docData->Scale;
	SetRect(&rect, 0, 0, HANDLE_WIDTH, HANDLE_HEIGHT);
	ScaleWinToDoc(scale, rect.MinX, rect.MinY, &handleRect->MinX, &handleRect->MinY);
	ScaleWinToDoc(scale, rect.MaxX, rect.MaxY, &handleRect->MaxX, &handleRect->MaxY);
	width = handleRect->MaxX - handleRect->MinX;
	height = handleRect->MaxY - handleRect->MinY;
	OffsetRect(handleRect, -width/2, -height/2);
}

/*
 *	Return TRUE if point is in handle at given location
 */

BOOL InHandle(PointPtr pt, PointPtr handlePt, RectPtr handleRect)
{
	Point diffPt;

	diffPt.x = pt->x - handlePt->x;
	diffPt.y = pt->y - handlePt->y;
	return (PtInRect(&diffPt, handleRect));
}

/*
 *	Draw standard grow handle
 */

void DrawHandle(RastPtr rPort, WORD x, WORD y)
{
	SetDrMd(rPort, COMPLEMENT);
	SetAfPt(rPort, NULL, 0);
	RectFill(rPort, x - HANDLE_WIDTH/2, y - HANDLE_HEIGHT/2, x + HANDLE_WIDTH/2,
			 y + HANDLE_HEIGHT/2);
}

/*
 *	Highlight selection
 */

static void DrawHilite(WindowPtr window)
{
	DocObjPtr docObj;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);

	for (docObj = FirstSelected(docData); docObj; docObj = NextSelected(docObj))
		HiliteObject(window, docObj);
}

/*
 *	Highlight the selection if highlight state is on 
 *	Used when window is redrawn
 */

void HiliteSelect(WindowPtr window)
{
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);

	if (docData->Flags & DOC_HILITE) {
		if (TextInEdit(window))
			TextDrawCursor(window);
		else
			DrawHilite(window);
	}
}

/*
 *	Turn on selection highlighting if not already on
 */

void HiliteSelectOn(WindowPtr window)
{
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);

	if (inMacro && drawOff)
		return;
	if ((docData->Flags & DOC_HILITE) == 0) {
		if (TextInEdit(window)) {
			TextDrawCursor(window);
			docData->BlinkCount = options.BlinkPeriod;
		}
		else
			DrawHilite(window);
		docData->Flags |= DOC_HILITE;
	}
}

/*
 *	Turn off selection highlighting if not already off
 */

void HiliteSelectOff(WindowPtr window)
{
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);

	if (inMacro && drawOff)
		return;
	if (docData->Flags & DOC_HILITE) {
		if (TextInEdit(window))
			TextDrawCursor(window);
		else
			DrawHilite(window);
		docData->Flags &= ~DOC_HILITE;
	}
}

/*
 *	Move selected objects in specified direction by grid amount
 */

static BOOL MoveObjects(WindowPtr window, WORD dx, WORD dy)
{
	WORD oldX, oldY, newX, newY, gridX, gridY;
	DocObjPtr docObj;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
	Rectangle rect, newRect;

/*
	Get amount to move objects
*/
	GetGridSpacing(docData, &gridX, &gridY);
	if (!GetSelectRect(docData, &rect))
		return (FALSE);
	oldX = rect.MinX;
	oldY = rect.MinY;
	SnapToGrid(docData, &oldX, &oldY);
	newX = oldX + dx*gridX;
	newY = oldY + dy*gridY;
	SnapToGrid(docData, &newX, &newY);
	newRect = rect;
	OffsetRect(&newRect, newX - oldX, newY - oldY);
	AdjustToDocBounds(docData, &newRect);
	dx = newRect.MinX - rect.MinX;
	dy = newRect.MinY - rect.MinY;
	if (dx == 0 && dy == 0)
		return (FALSE);
/*
	Move objects
*/
	HiliteSelectOff(window);
	for (docObj = FirstSelected(docData); docObj; docObj = NextSelected(docObj)) {
		InvalObjectRect(window, docObj);
		OffsetObject(docObj, dx, dy);
		InvalObjectRect(window, docObj);
	}
	HiliteSelectOn(window);
	DocModified(docData);
	return (TRUE);
}

/*
 *	Handle cursor up
 */

BOOL DoCursorUp(WindowPtr window, UWORD modifier)
{
	if (TextInEdit(window))
		TextCursorUp(window, modifier);
	else
		MoveObjects(window, 0, -1);
	return (TRUE);
}

/*
 *	Handle cursor down
 */

BOOL DoCursorDown(WindowPtr window, UWORD modifier)
{
	if (TextInEdit(window))
		TextCursorDown(window, modifier);
	else
		MoveObjects(window, 0, 1);
	return (TRUE);
}

/*
 *	Handle cursor left
 */

BOOL DoCursorLeft(WindowPtr window, UWORD modifier)
{
	if (TextInEdit(window))
		TextCursorLeft(window, modifier);
	else
		MoveObjects(window, -1, 0);
	return (TRUE);
}

/*
 *	Handle cursor right
 */

BOOL DoCursorRight(WindowPtr window, UWORD modifier)
{
	if (TextInEdit(window))
		TextCursorRight(window, modifier);
	else
		MoveObjects(window, 1, 0);
	return (TRUE);
}
