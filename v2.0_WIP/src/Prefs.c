/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Preferences routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>
#include <workbench/workbench.h>
#include <libraries/dos.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/dos.h>
#include <proto/icon.h>

#include <string.h>

#include <Toolbox/Memory.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Font.h>
#include <Toolbox/Window.h>
#include <Toolbox/Screen.h>
#include <Toolbox/Utility.h>

#include "Draw.h"
#include "Proto.h"

/*
 *	External variables
 */

extern ScreenPtr	screen;

extern Palette	screenPalette;

extern TextChar	progPathName[], strPrefsName[];

extern Defaults				defaults;
extern Options				options;
extern PostScriptOptions	postScriptOptions;

extern PenColorPtr	penColors;
extern RGBPat8	fillPats[];
extern TextChar	strBlack[];

extern BOOL	_tbOnPubScreen;

/*
 *	Local variables and definitions
 */

static TextChar topazFontName[]	= "topaz";
static TextChar	timesFontName[]	= "Times";
static TextChar helvFontName[]	= "Helvetica";
static TextChar courFontName[]	= "Courier";

/*
 *	Format of prefs file:
 *
 *		ProgPrefs struct
 *		zero or more MenuFontInfo structs
 *		zero or more Pen Colors
 *		zero or more Fill Patterns
 */

#define PREFS_VERSION	2				/* Version 1 didn't have PostScriptOptions */

typedef struct {
	ULONG				FileID;			/* Prefs file ID */
	UWORD				Version;
	TextChar			FontName[32];	/* NULL terminated, without ".font" */
	Defaults			Defaults;
	Options				Options;
	PostScriptOptions	PostScriptOptions;
	WORD				NumColors;
	RGBColor			ColorTable[256];
	WORD				NumMenuFonts, NumPens, NumFillPats;
} ProgPrefs, *ProgPrefsPtr;

typedef struct {
	TextChar	FontName[32];	/* Without trailing ".font" */
	WORD		FontSize;
} MenuFontInfo;

static ProgPrefs	progPrefs;

/*
 *	Local prototypes
 */

static void	SetStdDefaults(void);
static void	SetDefaults(void);

/*
 *	Create path name to given file at same directory as program
 *	If abbrev is TRUE, then it's OK to use short-cut in AmigaDOS 2.0
 */

void SetPathName(TextPtr pathNameBuff, TextPtr fileName, BOOL abbrev)
{
	register WORD	i;

	if (SystemVersion() >= OSVERSION_2_0_4 && abbrev)
		strcpy(pathNameBuff, "ProgDir:");
	else {
		strcpy(pathNameBuff, progPathName);
		for (i = strlen(pathNameBuff); i; i--) {
			if (pathNameBuff[i - 1] == '/' || pathNameBuff[i - 1] == ':')
				break;
		}
		pathNameBuff[i] = '\0';
	}
	strcat(pathNameBuff, fileName);
}

/*
 *	Set built-in default
 */

static void SetStdDefaults()
{
	FontNum		fontNum;
	TextAttr	defaultAttr;

	BlockClear(&defaults, sizeof(Defaults));
	defaults.DocFlags	= DOC_SHOWRULER | DOC_SHOWGRID | DOC_SHOWPAGE | DOC_GRIDSNAP;
	defaults.ObjFlags	= OBJ_DO_PEN | OBJ_DO_FILL;
	defaults.LineFlags	= 0;
	defaults.Style		= FS_NORMAL;
	defaults.MiscStyle	= 0;
	defaults.Justify	= JUSTIFY_LEFT;
	defaults.Spacing	= SPACE_SINGLE;
	defaults.PenWidth	= 1;
	defaults.PenHeight	= 1;
	defaults.PenColor	= RGBCOLOR_BLACK;
	strcpy(defaults.PenName, strBlack);
	CopyRGBPat8(&fillPats[1], &defaults.FillPat);	/* Black fill pattern */
	strcpy(defaults.FillName, strBlack);

	defaults.FontNum = -1;			/* FontNumber() returns this if font not found */
	defaultAttr.ta_Style = FS_NORMAL;
	defaultAttr.ta_Flags = (FPF_ROMFONT | FPF_DISKFONT | FPF_PROPORTIONAL);
	defaultAttr.ta_Name  = timesFontName;
	defaultAttr.ta_YSize = 12;
	if ((fontNum = FontNumber(defaultAttr.ta_Name)) == -1) {
		defaultAttr.ta_Name = helvFontName;
		if ((fontNum = FontNumber(defaultAttr.ta_Name)) == -1) {
			defaultAttr.ta_Name = topazFontName;	/* Topaz font is always available */
			defaultAttr.ta_YSize = 11;
		}
	}
	defaults.FontNum	= FontNumber(defaultAttr.ta_Name);
	defaults.FontSize	= defaultAttr.ta_YSize;

	defaults.GridUnitsIndex	= 1;			/* 1/8 inch grid */

	PrintDefault(&defaults.PrintRec);
	PostScriptDefault();
	SetStdOptions(&options);

	ColorCorrectEnable(&screenPalette, options.ColorCorrect);
	SetToolDefaults();
	SetMenuFont(defaults.FontNum, defaults.FontSize, TRUE);
	if ((fontNum = FontNumber(timesFontName)) != defaults.FontNum)
		SetMenuFont(fontNum, 12, TRUE);
	if ((fontNum = FontNumber(helvFontName)) != defaults.FontNum)
		SetMenuFont(fontNum, 12, TRUE);
	if ((fontNum = FontNumber(courFontName)) != defaults.FontNum)
		SetMenuFont(fontNum, 12, TRUE);
}

/*
 *	Set defaults from prefs structure
 */

static void SetDefaults()
{
	WORD		i, entries, numColors;
	RGBColor	colors[256];

	defaults			= progPrefs.Defaults;
	options				= progPrefs.Options;
	postScriptOptions	= progPrefs.PostScriptOptions;
	defaults.FontNum	= FontNumber(progPrefs.FontName);

	PrValidate(&defaults.PrintRec);
	ColorCorrectEnable(&screenPalette, options.ColorCorrect);
	SetToolDefaults();

	if (!_tbOnPubScreen) {
		numColors = GetColorTable(screen, &colors);	/* In case screen has more colors than prefs */
		entries = MIN(numColors, progPrefs.NumColors);
		for (i = 0; i < entries/2; i++) {
			colors[i] = progPrefs.ColorTable[i];
			colors[numColors - i - 1] = progPrefs.ColorTable[progPrefs.NumColors - i - 1];
		}
		LoadRGB4(&screen->ViewPort, colors, numColors);
		CheckColorTable();
	}
}

/*
 *	Read and set defaults from specified file
 *	Return success status
 */

BOOL ReadProgPrefs(TextPtr fileName)
{
	WORD			i;
	BOOL			success;
	File			file;
	MenuFontInfo	menuFontInfo;
	RGBColor		penColor, colors[MAX_NUM_PENS];
	TextPtr			names[MAX_NUM_PENS];
	WORD			len, numPens;
	RGBPat8			fillPat;
	TextPtr			fillName;

	success = FALSE;
	numPens = 0;
	fillName = NULL;
	BlockClear(names, MAX_NUM_PENS * sizeof(TextPtr));
/*
	Open file
*/
	if ((file = Open(fileName, MODE_OLDFILE)) == NULL)
		goto Exit2;
/*
	Read main prefs data
*/
	if (Read(file, &progPrefs, sizeof(ProgPrefs)) != sizeof(ProgPrefs) ||
		progPrefs.FileID != ID_PREFSFILE || progPrefs.Version != PREFS_VERSION)
		goto Exit1;
/*
	Read menu font list
*/
	for (i = 0; i < progPrefs.NumMenuFonts; i++) {
		if (Read(file, &menuFontInfo, sizeof(MenuFontInfo)) != sizeof(MenuFontInfo))
			goto Exit1;
		SetMenuFont(FontNumber(menuFontInfo.FontName), menuFontInfo.FontSize, TRUE);
	}
/*
	Read pen colors
*/
	DisposePenColors();
	numPens = progPrefs.NumPens;
	for (i = 0; i < numPens; i++) {
		if (Read(file, &penColor, sizeof(RGBColor)) != sizeof(RGBColor))
			goto Exit1;
		colors[i] = penColor;
		if (Read(file, &len, sizeof(WORD)) != sizeof(WORD))
			goto Exit1;
		if ((names[i] = MemAlloc(len + 1, MEMF_CLEAR)) == NULL)
			goto Exit1;
		if (Read(file, names[i], len) != len)
			goto Exit1;
	}
	InitPenColors(numPens, colors, names);
/*
	Read fill patterns
*/
	DisposeFillPatterns();
	if (!InitFillPatterns())
		goto Exit1;

	for (i = 0; i < progPrefs.NumFillPats; i++) {
		if (Read(file, &fillPat, sizeof(RGBPat8)) != sizeof(RGBPat8))
			goto Exit1;
		if (Read(file, &len, sizeof(WORD)) != sizeof(WORD))
			goto Exit1;
		if ((fillName = MemAlloc(len + 1, MEMF_CLEAR)) == NULL)
			goto Exit1;
		if (Read(file, fillName, len) != len)
			goto Exit1;
		if (!AddFillPattern(fillName, fillPat, FALSE))
			goto Exit1;
		MemFree(fillName, len + 1);
		fillName = NULL;
	}
	SetDefaults();
/*
	All done
*/
	success = TRUE;
Exit1:
	Close(file);
Exit2:
	for (i = 0; i < numPens; i++)
		if (names[i])
			MemFree(names[i], strlen(names[i] + 1));
	if (fillName)
		MemFree(fillName, len + 1);

	return (success);
}

/*
 *	Return TRUE if file is a settings file
 */

BOOL IsPrefsFile(TextPtr fileName)
{
	ULONG fileID;
	UWORD version;
	BOOL success;
	File file;

	if ((file = Open(fileName, MODE_OLDFILE)) == NULL)
		return (FALSE);
	success = FALSE;
	if (Read(file, &fileID, sizeof(ULONG)) == sizeof(ULONG) &&
		fileID == ID_PREFSFILE &&
		Read(file, &version, sizeof(UWORD)) == sizeof(UWORD) &&
		(version == PREFS_VERSION))
		success = TRUE;
	Close(file);
	return (success);
}

/*
 *	Load preference settings
 *	If no prefs file found, set to standard defaults
 */

BOOL GetProgPrefs(TextPtr fileName)
{
	TextChar	pathNameBuff[150];

	if (fileName)
		strcpy(pathNameBuff, fileName);
	else
		SetPathName(pathNameBuff, strPrefsName, TRUE);

	if (!ReadProgPrefs(pathNameBuff))
		SetStdDefaults();

	return (TRUE);
}

/*
 *	Save preferences settings from given document
 *	Return success status
 */

BOOL SaveProgPrefs(WindowPtr window, TextPtr fileName)
{
	WORD			i, fontNum, fontSize;
	BOOL			success;
	File			file;
	DocDataPtr		docData = (DocDataPtr) GetWRefCon(window);
	MenuFontInfo	menuFontInfo;
	TextChar		pathNameBuff[150];
	PenColorPtr		pen;
	FillPtnPtr		fill;
	WORD			len;

/*
	Set document defaults
*/
	success = FALSE;
	defaults.DocFlags	= docData->Flags & ~DOC_MODIFIED;
	defaults.PrintRec	= *(docData->PrintRec);
	PrValidate(&defaults.PrintRec);
	defaults.GridUnitsIndex	= docData->GridUnitsIndex;
/*
	Set prefs data
*/
	BlockClear(&progPrefs, sizeof(ProgPrefs));
	progPrefs.FileID		= ID_PREFSFILE;
	progPrefs.Version		= PREFS_VERSION;
	GetFontName(defaults.FontNum, progPrefs.FontName);
	progPrefs.Defaults			= defaults;
	progPrefs.Options			= options;
	progPrefs.PostScriptOptions	= postScriptOptions;
	progPrefs.NumMenuFonts		= NumMenuFonts();
	progPrefs.NumPens			= NumPens();
	progPrefs.NumFillPats		= NumFillPatterns();
/*
	Save color palette
*/
	if (_tbOnPubScreen)
		progPrefs.NumColors = 0;
	else
		progPrefs.NumColors = GetColorTable(screen, &progPrefs.ColorTable);
/*
	Open file
*/
	if (fileName)
		strcpy(pathNameBuff, fileName);
	else
		SetPathName(pathNameBuff, strPrefsName, TRUE);
	if ((file = Open(pathNameBuff, MODE_NEWFILE)) == NULL)
		goto Exit2;
/*
	Write main prefs data
*/
	if (Write(file, &progPrefs, sizeof(ProgPrefs)) != sizeof(ProgPrefs))
		goto Exit1;
/*
	Write menu font list
*/
	for (i = 0; i < progPrefs.NumMenuFonts; i++) {
		GetMenuFontInfo(i, &fontNum, &fontSize);
		GetFontName(fontNum, menuFontInfo.FontName);
		menuFontInfo.FontSize = fontSize;
		if (Write(file, &menuFontInfo, sizeof(MenuFontInfo)) != sizeof(MenuFontInfo))
			goto Exit1;
	}
/*
	Write pen colors
*/
	pen = FirstPenColor();			// dont need to save none color
	for (i = 0; i < progPrefs.NumPens; i++) {
		if (Write(file, &pen->Color, sizeof(RGBColor)) != sizeof(RGBColor))
			goto Exit1;
		len = strlen(pen->Name);
		if (Write(file, &len, sizeof(WORD)) != sizeof(WORD))
			goto Exit1;
		if (Write(file, pen->Name, len) != len)
			goto Exit1;
		pen = pen->Next;
	}
/*
	Write fill patterns
*/
	fill = FirstFillPattern();
	for (i = 0; i < progPrefs.NumFillPats; i++) {
		if (Write(file, &fill->Pattern, sizeof(RGBPat8)) != sizeof(RGBPat8))
			goto Exit1;
		len = strlen(fill->Name);
		if (Write(file, &len, sizeof(WORD)) != sizeof(WORD))
			goto Exit1;
		if (Write(file, fill->Name, len) != len)
			goto Exit1;
		fill = fill->Next;
	}
/*
	All done
*/
	success = TRUE;
Exit1:
	Close(file);
	if (success)
		SaveIcon(pathNameBuff, ICON_PREFS);
Exit2:
	return (success);
}
