/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Toolbox library
 *	Copyright (c) 1991 New Horizons Software
 *
 *	Misc utility routines
 */

#ifndef TOOLBOX_DATETIME_H
#define TOOLBOX_DATETIME_H

#ifndef EXEC_TYPES_H
#include <exec/types.h>
#endif

#ifndef LIBRARTES_DOS_H
#include <libraries/dos.h>
#endif

#ifndef TYPEDEFS_H
#include <Typedefs.h>
#endif

/*
 *	Date formats
 */

enum {
	DATE_SHORT = 0,		/* 3/1/90 */
	DATE_ABBR,			/* Mar 1, 1990 */
	DATE_LONG,			/* March 1, 1990 */
	DATE_ABBRDAY,		/* Thu, Mar 1, 1990 */
	DATE_LONGDAY,		/* Thursday, March 1, 1990 */
	DATE_MILITARY		/* 1 Mar 90 */
};

#define DATE_CUSTOM		-1

/*
 *	Custom date characters
 */

enum {
	DAY_NUMBER = 0x01,
	DAY_NAME,
	DAY_NAMEABBREV,
	MONTH_NUMBER,
	MONTH_NAME,
	MONTH_NAMEABBREV,
	YEAR_LONG,
	YEAR_SHORT,
	DAY_NUMZERO,
	MONTH_NUMZERO
};

/*
 *	Prototypes
 */

void	MonthName(WORD, BOOL, TextPtr);
void	DateString(struct DateStamp *, WORD, TextPtr);
void	TimeString(struct DateStamp *, BOOL, BOOL, TextPtr);

#endif
