/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Toolbox library
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	Screen routines
 */

#include <exec/types.h>
#include <graphics/displayinfo.h>
#include <intuition/intuition.h>
#include <intuition/intuitionbase.h>
#include <workbench/workbench.h>
#include <workbench/startup.h>

#include <string.h>

#include <Toolbox/Globals.h>
#include <Toolbox/ColorPick.h>
#include <Toolbox/StdFile.h>
#include <Toolbox/Utility.h>
#include <Toolbox/Screen.h>

#include <proto/graphics.h>
#include <proto/intuition.h>
#include <proto/icon.h>

/*
 *	External variables
 */

extern struct IntuitionBase	*IntuitionBase;

/*
 *	Local variables and definitions
 */

#define MIN(a, b)	(((a)<(b))?(a):(b))

static TextAttr screenAttr = {
	NULL, 11, FS_NORMAL, FPF_ROMFONT | FPF_DISKFONT | FPF_DESIGNED
};

static TextChar topazFontName[] = "topaz.font";
static TextChar	systemFontName[] = "System.font";
static TextChar	systemThinFontName[] = "SystemThin.font";

static TextFontPtr	screenFont;

static ScreenPtr	pubScreen;

/*
 *	Default color tables
 *	32-color table is color-corrected table of:
 *
 *	0x000,	0xFFF,	0xF00,	0x0FF,
 *	0x700,	0x770,	0x070,	0x077,
 *	0x07F,	0x0F7,	0x70F,	0x777,
 *	0x77F,	0x7F0,	0x7F7,	0x7FF,
 *	0xF07,	0xF70,	0xF77,	0xF7F,
 *	0xFF7,	0x222,	0x999,	0xDDD,
 *	0x007,	0x707,	0x555,	0xBBB,
 *	0x0F0,	0xF0F,	0x00F,	0xFF0
 */

static RGBColor	colorTable2[] = { 0x000, 0xFFF };

static RGBColor	colorTable4[] = { 0x000, 0xFFF, 0x555, 0xBBB };

static RGBColor	colorTable32[] = {		// Color-corrected primaries
	0x000,	0xFFF,	0xE02,	0x3AC,	0x743,	0x775,	0x464,	0x467,
	0x359,	0x398,	0x806,	0x777,	0x86A,	0x9B4,	0x9B9,	0x9CD,
	0xE05,	0xE74,	0xE77,	0xE7B,	0xFEA,	0x222,	0x999,	0xDDD,
	0x435,	0x745,	0x555,	0xBBB,	0x394,	0xE07,	0x306,	0xFE5
};

/*
 *	Local prototypes
 */

static WORD	BuildDefaultColorTable(ColorTablePtr, WORD);

static WORD	MaxScreenDepth(ULONG);

static ScreenPtr	GetPublicScreen(TextPtr);

static BOOL	MatchOptions(TextPtr, TextPtr, TextPtr);

static void	DoScreenOption(struct NewScreen *, ULONG *, TextPtr, TextPtr);
static BOOL	SetScreenData(struct NewScreen *, ULONG *, int, char **, TextPtr);
static void	SetScreenColors(ScreenPtr, ColorTablePtr, WORD);

static void	SetNewLookPens(UWORD *, ColorTablePtr, WORD, WORD);

static ScreenPtr	GetNewScreen(struct NewScreen *, ULONG, UWORD *, TextPtr);

/*
 *	Build default color table for up to 256 colors
 *	Return number of colors in table
 */

static WORD BuildDefaultColorTable(register ColorTablePtr colorTable, WORD depth)
{
	register WORD	i, j, r, g, b;
	WORD			numColors;
	ColorTablePtr	table;

/*
	For 256 color screens, build color palette
*/
	if (depth == 8) {
		i = 0;
		for (r = 0; r < 16; r+= 3) {
			for (g = 0; g < 16; g += 3) {
				for (b = 0; b < 16; b += 3)
					(*colorTable)[i++] = RGBCOLOR(r, g, b);
			}
		}
		for (j = 0; j < 16; j++) {
			if (j % 3 > 0) {
				(*colorTable)[i]	  = RGBCOLOR(j, 0, 0);
				(*colorTable)[i + 10] = RGBCOLOR(0, j, 0);
				(*colorTable)[i + 20] = RGBCOLOR(0, 0, j);
				(*colorTable)[i + 30] = RGBCOLOR(j, j, j);
				i++;
			}
		}
		numColors = 256;
	}
/*
	Otherwise, load from table
*/
	else {
		switch (depth) {
		case 1:
			table = &colorTable2;
			numColors = 2;
			break;
		case 2:
			table = &colorTable4;
			numColors = 4;
			break;
		default:
			table = &colorTable32;
			numColors = 32;
			break;
		}
		BlockMove(table, colorTable, numColors*sizeof(RGBColor));
	}
/*
	Return actual number of colors set
*/
	return (numColors);
}

/*
 *	Return the maximum screen depth for the given screen modeID
 */

static WORD MaxScreenDepth(ULONG modeID)
{
	WORD					maxDepth;
	DisplayInfoHandle		dispInfo;
	struct DimensionInfo	dimInfo;

	maxDepth = 1;
/*
	Handle old system version
*/
	if (LibraryVersion((struct Library *) IntuitionBase) < OSVERSION_2_0) {
		switch (modeID) {
		case LORES_KEY:
		case LORESLACE_KEY:
			maxDepth = 5;
			break;
		default:
			maxDepth = 4;
			break;
		}
	}
/*
	Handle new system veresion
*/
	else {
		if ((dispInfo = FindDisplayInfo(modeID)) == NULL ||
			GetDisplayInfoData(dispInfo, (BYTE *) &dimInfo, sizeof(struct DimensionInfo),
							   DTAG_DIMS, NULL) == 0)
			maxDepth = 4;
		else
			maxDepth = dimInfo.MaxDepth;
	}
	return (maxDepth);
}

/*
 *	Return pointer to specified public screen
 *	If not running under 2.0 then only NULL is valid for screen name
 *	Note: Under 2.0, this routine will lock the screen, the caller
 *		  should unlock the screen after the first window is opened on the screen
 */

static ScreenPtr GetPublicScreen(TextPtr scrnName)
{
	LONG				intuiLock;
	register ScreenPtr	screen;

	if (LibraryVersion((struct Library *) IntuitionBase) >= OSVERSION_2_0)
		screen = LockPubScreen(scrnName);
	else if (scrnName == NULL) {
		intuiLock = LockIBase(0);
		for (screen = IntuitionBase->FirstScreen; screen; screen = screen->NextScreen) {
			if ((screen->Flags & SCREENTYPE) == WBENCHSCREEN)
				break;
		}
		UnlockIBase(intuiLock);
	}
	else
		screen = NULL;
	return (screen);
}

/*
 *	Match screen options
 */

static BOOL MatchOptions(register TextPtr text, TextPtr opt1, TextPtr opt2)
{
	register WORD	len;

/*
	Get command length
*/
	for (len = 0; text[len] && text[len] != ':' && text[len] != '='; len++) ;
	if (text[len])
		len++;
/*
	Check for match
*/
	return ((BOOL) (CmpString(text, opt1, len, strlen(opt1), FALSE) == 0 ||
					CmpString(text, opt2, len, strlen(opt2), FALSE) == 0));
}

/*
 *	Set screen mode from given option
 *	If public screen requested, get lock on public screen
 */

static void DoScreenOption(struct NewScreen *newScrn, ULONG *modeID, TextPtr text,
						   TextPtr screenName)
{
	register TextPtr	param;
	WORD				intuiVersion;
	LONG				num;

	intuiVersion = LibraryVersion((struct Library *) IntuitionBase);
/*
	Get pointer to command param (if any)
*/
	for (param = text; *param && *param != ':' && *param != '='; param++) ;
	if (*param)
		param++;
/*
	Check for non-screen related params
*/
	if (MatchOptions(text, "smartwindows", "sw")) {
		_tbSmartWindows = TRUE;
		return;
	}
	if (MatchOptions(text, "dumbwindows", "dw")) {
		_tbSmartWindows = FALSE;
		return;
	}
	if (MatchOptions(text, "asl", "asl")) {
		SFPUseAslRequest(TRUE);
		return;
	}
/*
	If already have public screen, ignore all other screen commands
*/
	if (_tbOnPubScreen)
		return;
/*
	Check to open on workbench screen
*/
	if (MatchOptions(text, "workbench", "wb")) {
		pubScreen = GetPublicScreen(NULL);
		if (pubScreen != NULL)
			_tbOnPubScreen = TRUE;
		return;
	}
/*
	Check for other public screen request
*/
	if (MatchOptions(text, "screen:", "screen=")) {
		if (intuiVersion < OSVERSION_2_0)
			return;
		strcpy(screenName, param);			// Override default screen name
		pubScreen = GetPublicScreen(screenName);
		if (pubScreen != NULL)
			_tbOnPubScreen = TRUE;
		return;
	}
/*
	Check for screen depth command
*/
	if (MatchOptions(text, "colors:", "colors=") ||
		MatchOptions(text, "colours:", "colours=")) {
		num = StringToNum(param);
		if (num <= 0 || num > 256)
			return;
		newScrn->Depth = 1;
		while (num > 2) {
			newScrn->Depth += 1;
			num >>= 1;
		}
		newScrn->Type = CUSTOMSCREEN;
		return;
	}
/*
	Check for other screen settings
*/
	if (MatchOptions(text, "productivity", "pr")) {
		if (intuiVersion >= OSVERSION_2_0) {
			newScrn->ViewModes = (HIRES | LACE);
			*modeID = VGAPRODUCT_KEY;
		}
		newScrn->Type = CUSTOMSCREEN;
		return;
	}
	if (MatchOptions(text, "superhires", "sr")) {
		if (intuiVersion >= OSVERSION_2_0) {
			newScrn->ViewModes = (HIRES | LACE);
			*modeID = SUPERLACE_KEY;
		}
		newScrn->Type = CUSTOMSCREEN;
		return;
	}
	if (MatchOptions(text, "hires", "hr")) {
		newScrn->ViewModes = (HIRES | LACE);
		*modeID = HIRESLACE_KEY;
		newScrn->Type = CUSTOMSCREEN;
		return;
	}
	if (MatchOptions(text, "medres", "mr")) {
		newScrn->ViewModes = HIRES;
		*modeID = HIRES_KEY;
		newScrn->Type = CUSTOMSCREEN;
		return;
	}
	if (MatchOptions(text, "lores", "lr")) {
		newScrn->ViewModes = 0;
		*modeID = LORES_KEY;
		newScrn->Type = CUSTOMSCREEN;
		return;
	}
}

/*
 *	Set screen data
 *	If _tbOnPubScreen is set by this routine, then screen is set to public screen
 *		and newScrn settings are undefined
 */

static BOOL SetScreenData(struct NewScreen *newScrn, ULONG *modeID,
						  int argc, char *argv[], TextPtr screenName)
{
	WORD				i, maxDepth, intuiVersion;
	UWORD				viewModes;
	BOOL				highRes, interlace;
	register TextPtr	text;
	struct DiskObject	*icon;
	struct WBStartup	*wbMsg;
	ScreenPtr			screen;
	Screen				screenData;

	intuiVersion = LibraryVersion((struct Library *) IntuitionBase);
/*
	Set default mode from Workbench screen settings
*/
	_tbOnPubScreen = FALSE;
	if (intuiVersion >= OSVERSION_2_0) {
		if ((screen = LockPubScreen(NULL)) == NULL)
			return (FALSE);
		screenData = *screen;			// Copy default screen data
		UnlockPubScreen(NULL, screen);
	}
	else {
		if (!GetScreenData((BYTE *) &screenData, sizeof(Screen), WBENCHSCREEN, NULL))
			return (FALSE);
	}
	if (screenData.ViewPort.Modes & LACE) {
		newScrn->ViewModes = (HIRES | LACE);
		*modeID = HIRESLACE_KEY;
	}
	else {
		newScrn->ViewModes = HIRES;
		*modeID = HIRES_KEY;
	}
/*
	Get user parameters
*/
	if (argc) {				// Running under CLI
		for (i = 1; i < argc; i++) {
			text = argv[i];
			if (*text++ == '-')
				DoScreenOption(newScrn, modeID, text, screenName);
		}
	}
	else {					// Running under Workbench
		wbMsg = (struct WBStartup *) argv;
		if ((icon = GetDiskObject(wbMsg->sm_ArgList->wa_Name)) != NULL) {
			if (icon->do_ToolTypes) {
				for (i = 0; (text = icon->do_ToolTypes[i]) != NULL; i++)
					DoScreenOption(newScrn, modeID, text, screenName);
			}
			FreeDiskObject(icon);
		}
	}
/*
	Set screen dimensions and adjust depth to upper bound
*/
	if (!_tbOnPubScreen) {
		newScrn->Height = STDSCREENHEIGHT;
		if (intuiVersion >= OSVERSION_2_0)
			newScrn->Width = STDSCREENWIDTH;
		else if (newScrn->ViewModes & HIRES)
			newScrn->Width = screenData.Width;
		else
			newScrn->Width = screenData.Width/2;
		maxDepth = MaxScreenDepth(*modeID);
		if (newScrn->Depth > maxDepth)
			newScrn->Depth = maxDepth;
	}
/*
	Set the default screen font
*/
	viewModes = (_tbOnPubScreen) ? pubScreen->ViewPort.Modes : newScrn->ViewModes;
	highRes   = ((viewModes & HIRES) != 0);
	interlace = ((viewModes & LACE) != 0);
	screenAttr.ta_Name = (highRes) ? systemFontName : systemThinFontName;
	screenAttr.ta_YSize = (interlace) ? 11 : 8;
	if ((screenFont = GetFont(&screenAttr)) == NULL) {
		screenAttr.ta_Name = topazFontName;
		if ((screenFont = GetFont(&screenAttr)) == NULL) {
			screenAttr.ta_YSize = 8;		// Can always get this font
			screenFont = GetFont(&screenAttr);
		}
	}
	newScrn->Font = &screenAttr;
	return (TRUE);
}

/*
 *	Set screen colors
 */

static void SetScreenColors(ScreenPtr screen, ColorTablePtr colorTable, WORD numColors)
{
	register WORD	i, entries, num;
	RGBColor		colors[256];

	num = GetColorTable(screen, &colors);	// In case screen has more than numColors
	if (num == 2) {
		colors[0] = (*colorTable)[0];
		colors[1] = (*colorTable)[1];
	}
	else {
		entries = MIN(num, numColors);
		for (i = 0; i < entries/2; i++) {
			colors[i] = (*colorTable)[i];
			colors[num - i - 1] = (*colorTable)[numColors - i - 1];
		}
	}
	LoadRGB4(&screen->ViewPort, colors, num);
}

/*
 *	Set new look pens from color table
 */

static void SetNewLookPens(UWORD *newLookPens, register ColorTablePtr colorTable,
						   WORD numColors, WORD screenDepth)
{
	register WORD	i, num, numScrnColors, color;
	ColorMapPtr		colorMap;

	if (LibraryVersion((struct Library *) IntuitionBase) < OSVERSION_2_0)
		return;
/*
	Set up color map that will be used
*/
	numScrnColors = 1 << screenDepth;
	num = MIN(numColors, numScrnColors);
	if ((colorMap = GetColorMap(num)) == NULL)
		return;
	for (i = 0; i < num/2; i++) {
		color = (*colorTable)[i];
		SetRGB4CM(colorMap, i, RED(color), GREEN(color), BLUE(color));
		color = (*colorTable)[numColors - i - 1];
		SetRGB4CM(colorMap, num - i - 1, RED(color), GREEN(color), BLUE(color));
	}
/*
	Now figure out pens to use
	(Set up toolbox colors from this palette, since they are not set up yet)
*/
	SetToolboxColors(colorMap, num);
	if (numScrnColors > num) {
		if (_tbPenBlack > num/2)
			_tbPenBlack += numScrnColors - num;
		if (_tbPenWhite > num/2)
			_tbPenWhite += numScrnColors - num;
		if (_tbPenDark > num/2)
			_tbPenDark += numScrnColors - num;
		if (_tbPenLight > num/2)
			_tbPenLight += numScrnColors - num;
	}
/*
	Set new look pens
*/
	newLookPens[DETAILPEN]			= 0;
	newLookPens[BLOCKPEN]			= 1;
	newLookPens[TEXTPEN]			= _tbPenLight;
	newLookPens[SHINEPEN]			= _tbPenWhite;
	newLookPens[SHADOWPEN]			= _tbPenBlack;
	newLookPens[FILLPEN]			= _tbPenLight;
	newLookPens[FILLTEXTPEN]		= _tbPenBlack;
	newLookPens[BACKGROUNDPEN]		= _tbPenDark;
	newLookPens[HIGHLIGHTTEXTPEN]	= _tbPenWhite;
	newLookPens[BARDETAILPEN]		= _tbPenBlack;
	newLookPens[BARBLOCKPEN]		= _tbPenWhite;
	newLookPens[BARTRIMPEN]			= _tbPenDark;
	newLookPens[12]					= ~0;
	FreeColorMap(colorMap);
}

/*
 *	Open new screen from newScrn parameters
 */

static ScreenPtr GetNewScreen(struct NewScreen *newScrn, ULONG modeID,
							  UWORD *newLookPens, TextPtr screenName)
{
	ScreenPtr	screen;

	if (LibraryVersion((struct Library *) IntuitionBase) >= OSVERSION_2_0) {
		screen = LockPubScreen(screenName);
		if (screen == NULL) {
			screen = OpenScreenTags(newScrn,
									SA_Pens, newLookPens,
									SA_DisplayID, modeID,
									SA_FullPalette, TRUE,
									SA_PubName, screenName,
									SA_Interleaved, TRUE,
									TAG_END);
			if (screen)
				PubScreenStatus(screen, 0);
		}
		else {								// Public screen already exists
			UnlockPubScreen(NULL, screen);
			screen = OpenScreenTags(newScrn,
									SA_Pens, newLookPens,
									SA_DisplayID, modeID,
									SA_FullPalette, TRUE,
									TAG_END);
		}
	}
	else
		screen = OpenScreen(newScrn);
	return (screen);
}

/*
 *	Get screen settings and open screen (or get pointer to public screen)
 */

ScreenPtr GetScreen(int argc, char *argv[], struct NewScreen *newScrn,
					ColorTablePtr colorTable, WORD numColors,
					TextPtr defaultScreenName)
{
	ScreenPtr	screen;
	ULONG		modeID;
	UWORD		newLookPens[13];
	TextChar	screenName[100];
	RGBColor	defaultColorTable[256];

	strcpy(screenName, defaultScreenName);
	if (!SetScreenData(newScrn, &modeID, argc, argv, screenName))
		return (NULL);
	if (_tbOnPubScreen)
		screen = pubScreen;
	else if (newScrn->Type == WBENCHSCREEN) {
		if ((screen = GetPublicScreen(NULL)) != NULL)
			_tbOnPubScreen = TRUE;
	}
	else {
		if (colorTable == NULL) {
			numColors = BuildDefaultColorTable(&defaultColorTable, newScrn->Depth);
			colorTable = &defaultColorTable;
		}
		SetNewLookPens(newLookPens, colorTable, numColors, newScrn->Depth);
		screen = GetNewScreen(newScrn, modeID, newLookPens, screenName);
		if (screen)
			SetScreenColors(screen, colorTable, numColors);
	}
/*
	All done
*/
	return (screen);
}

/*
 *	Close screen openned by GetScreen()
 */

void DisposeScreen(ScreenPtr screen)
{
	if (screen && !_tbOnPubScreen)
		CloseScreen(screen);
	if (screenFont)
		CloseFont(screenFont);
}

/*
 *	Get screen color table
 *	Return number of colors in table
 */

WORD GetColorTable(ScreenPtr scrn, register ColorTablePtr colorTable)
{
	register WORD	i, numColors;

	numColors = 1 << scrn->RastPort.BitMap->Depth;
	for (i = 0; i < numColors; i++)
		(*colorTable)[i] = GetRGB4(scrn->ViewPort.ColorMap, i);
	return (numColors);
}

/*
 *	Determine whether the specified screen is a public screen
 */

BOOL IsPubScreen(register ScreenPtr screen)
{
	register struct PubScreenNode	*psn;

	if (LibraryVersion((struct Library *) IntuitionBase) < OSVERSION_2_0)
		return (FALSE);
	psn = (struct PubScreenNode *) LockPubScreenList();
	while (psn) {
		if (psn->psn_Screen == screen)
			break;
		psn = (struct PubScreenNode *) psn->psn_Node.ln_Succ;
	}
	UnlockPubScreenList();
	return (psn != NULL);
}
