/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Oval handling routines
 */

#include <exec/types.h>
#include <graphics/gfxmacros.h>
#include <intuition/intuition.h>

#include <proto/graphics.h>

#include <Toolbox/Memory.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Window.h>

#include "Draw.h"
#include "Proto.h"

/*
 *	External variables
 */

extern Defaults	defaults;

/*
 *	Local variables and definitions
 */

#define LINE_PAT	0xFFFF		/* Line pattern for object creation */

static OvalObjPtr	growOvalObj;
static BOOL			wasMoved;
static WORD			growHandle;

/*
 *	Local prototypes
 */

OvalObjPtr	CreateOval(DocDataPtr, WORD, WORD, WORD, WORD);

void	OvalTrackCreate(WindowPtr, WORD, WORD, WORD, WORD, BOOL);
void	OvalConstrainCreate(WORD, WORD, WORD *, WORD *);

void	OvalTrackGrow(WindowPtr, WORD, WORD, WORD, WORD, BOOL);

/*
 *	Allocate a new oval object
 */

OvalObjPtr OvalAllocate()
{
	return ((OvalObjPtr) MemAlloc(sizeof(OvalObj), MEMF_CLEAR));
}

/*
 *	Dispose of oval object
 */

void OvalDispose(OvalObjPtr ovalObj)
{
	MemFree(ovalObj, sizeof(OvalObj));
}

/*
 *	Create oval with given start and end positions
 */

static OvalObjPtr CreateOval(DocDataPtr docData, WORD xStart, WORD yStart, WORD xEnd, WORD yEnd)
{
	register OvalObjPtr ovalObj;

	if ((ovalObj = (OvalObjPtr) NewDocObject(CurrLayer(docData), TYPE_OVAL)) == NULL)
		return (NULL);
	SetFrameRect(&ovalObj->DocObj.Frame, xStart, yStart, xEnd, yEnd);
	ovalObj->DocObj.Flags	= defaults.ObjFlags;
	ovalObj->PenWidth		= defaults.PenWidth;
	ovalObj->PenHeight		= defaults.PenHeight;
	ovalObj->PenColor		= defaults.PenColor;
	CopyRGBPat8(&defaults.FillPat, &ovalObj->FillPat);
	return (ovalObj);
}

/*
 *	Draw oval object
 */

void OvalDrawObj(RastPtr rPort, OvalObjPtr ovalObj, RectPtr rect, RectPtr clipRect)
{
	Point pen;
	Rectangle drawRect;

	if (HasFill(ovalObj))
		FillOval(rPort, rect, &ovalObj->FillPat);
	if (HasPen(ovalObj)) {
		pen.x = ovalObj->PenWidth;
		pen.y = ovalObj->PenHeight;
		ScalePt(&pen, &ovalObj->DocObj.Frame, rect);
		drawRect = *rect;
		OffsetRect(&drawRect, -pen.x/2, -pen.y/2);
		PenNormal(rPort);
		RGBForeColor(rPort, ovalObj->PenColor);
		FrameOval(rPort, &drawRect, pen.x, pen.y);
	}
}

/*
 *	Draw outline of oval (for creation/dragging)
 *	Offset is in document coordinates
 */

void OvalDrawOutline(WindowPtr window, OvalObjPtr ovalObj, WORD xOffset, WORD yOffset)
{
	RastPtr rPort = window->RPort;
	Rectangle rect;

	rect = ovalObj->DocObj.Frame;
	OffsetRect(&rect, xOffset, yOffset);
	DocToWindowRect(window, &rect, &rect);
	SetDrMd(rPort, COMPLEMENT);
	SetDrPt(rPort, LINE_PAT);
	FrameOval(rPort, &rect, 1, 1);
}

/*
 *	Draw selection hilighting for oval
 */

void OvalHilite(WindowPtr window, OvalObjPtr ovalObj)
{
	RectHilite(window, (RectObjPtr) ovalObj);
}

/*
 *	Set oval pen color
 */

void OvalSetPenColor(OvalObjPtr ovalObj, RGBColor penColor)
{
	ovalObj->PenColor = penColor;
}

/*
 *	Set oval pen size
 *	If size is -1 then do not change
 */

void OvalSetPenSize(OvalObjPtr ovalObj, WORD penWidth, WORD penHeight)
{
	if (penWidth != -1)
		ovalObj->PenWidth = penWidth;
	if (penHeight != -1)
		ovalObj->PenHeight = penHeight;
}

/*
 *	Set oval fill pattern
 */

void OvalSetFillPat(OvalObjPtr ovalObj, RGBPat8Ptr fillPat)
{
	CopyRGBPat8(fillPat, &ovalObj->FillPat);
}

/*
 *	Rotate oval by given angle about center
 */

void OvalRotate(OvalObjPtr ovalObj, WORD cx, WORD cy, WORD angle)
{
	RotateRect(&ovalObj->DocObj.Frame, cx, cy, angle);
}

/*
 *	Flip oval horizontally or vertically about center
 */

void OvalFlip(OvalObjPtr ovalObj, WORD cx, WORD cy, BOOL horiz)
{
	FlipRect(&ovalObj->DocObj.Frame, cx, cy, horiz);
}

/*
 *	Scale oval to new frame
 */

void OvalScale(OvalObjPtr ovalObj, RectPtr frame)
{
	ovalObj->DocObj.Frame = *frame;
}

/*
 *	Create poly equivalent to given rect
 *	Returns pointer to poly, or NULL if error
 */

PolyObjPtr OvalConvertToPoly(OvalObjPtr ovalObj)
{
	WORD x, y, x1, x2, y1, y2;
	PolyObjPtr polyObj;

	if ((polyObj = (PolyObjPtr) NewDocObject(NULL, TYPE_POLY)) == NULL)
		return (NULL);
	polyObj->DocObj.Frame = ovalObj->DocObj.Frame;
	x = ovalObj->DocObj.Frame.MaxX - ovalObj->DocObj.Frame.MinX;
	y = ovalObj->DocObj.Frame.MaxY - ovalObj->DocObj.Frame.MinY;
	x1 = (10824L*x + 10000L)/20000L;
	x2 = ( 7654L*x + 10000L)/20000L;
	y1 = (10824L*y + 10000L)/20000L;
	y2 = ( 7654L*y + 10000L)/20000L;
	x /= 2;
	y /= 2;
	if (!PolyAddPoint(polyObj, 0x7FFF, x     , y + y1) ||
		!PolyAddPoint(polyObj, 0x7FFF, x + x2, y + y2) ||
		!PolyAddPoint(polyObj, 0x7FFF, x + x1, y     ) ||
		!PolyAddPoint(polyObj, 0x7FFF, x + x2, y - y2) ||
		!PolyAddPoint(polyObj, 0x7FFF, x     , y - y1) ||
		!PolyAddPoint(polyObj, 0x7FFF, x - x2, y - y2) ||
		!PolyAddPoint(polyObj, 0x7FFF, x - x1, y     ) ||
		!PolyAddPoint(polyObj, 0x7FFF, x - x2, y + y2)) {
		DisposeDocObject((DocObjPtr) polyObj);
		return (NULL);
	}
	PolyAdjustFrame(polyObj);
	PolySetPenColor(polyObj, ovalObj->PenColor);
	PolySetPenSize(polyObj, ovalObj->PenWidth, ovalObj->PenHeight);
	PolySetFillPat(polyObj, &ovalObj->FillPat);
	polyObj->DocObj.Flags = ovalObj->DocObj.Flags;
	PolySetSmooth(polyObj, TRUE);
	PolySetClosed(polyObj, TRUE);
	return (polyObj);
}

/*
 *	Determine if point is in oval
 *	Point is relative to object rectangle
 */

BOOL OvalSelect(OvalObjPtr ovalObj, Point *pt)
{
	LONG x, y, rx, ry, rx1, ry1;
	BOOL hasPen, hasFill, inOuter, inInner;

	hasPen = HasPen(ovalObj);
	hasFill = HasFill(ovalObj);
	if (!hasPen && !hasFill)
		return (FALSE);
	rx = (ovalObj->DocObj.Frame.MaxX - ovalObj->DocObj.Frame.MinX)/2;
	ry = (ovalObj->DocObj.Frame.MaxY - ovalObj->DocObj.Frame.MinY)/2;
	if (rx == 0 || ry == 0)
		return (TRUE);
	x = (pt->x - rx);
	y = (pt->y - ry);
	rx1 = rx + 1;
	ry1 = ry + 1;
	while (x > 0x7F || y > 0x7F || rx1 > 0x7F || ry1 > 0x7F) {
		x >>= 1;
		y >>= 1;
		rx1 >>= 1;
		ry1 >>= 1;
	}
	inOuter = (x*x*ry1*ry1 + y*y*rx1*rx1 <= rx1*rx1*ry1*ry1);
	if (!inOuter)
		return (FALSE);
	if (hasFill)
		return (TRUE);
	x = (pt->x - rx);
	y = (pt->y - ry);
	rx1 = rx - ovalObj->PenWidth - 1;
	ry1 = ry - ovalObj->PenHeight - 1;
	while (x > 0x7F || y > 0x7F || rx1 > 0x7F || ry1 > 0x7F) {
		x >>= 1;
		y >>= 1;
		rx1 >>= 1;
		ry1 >>= 1;
	}
	inInner = (x*x*ry1*ry1 + y*y*rx1*rx1 <= rx1*rx1*ry1*ry1);
	return (!inInner);
}

/*
 *	Return handle number of given point, or -1 if not in a handle
 *	Point is relative to object rectangle
 */

WORD OvalHandle(OvalObjPtr ovalObj, PointPtr pt, RectPtr handleRect)
{
	return (RectHandle((RectObjPtr) ovalObj, pt, handleRect));
}

/*
 *	Duplicate oval data to new object
 *	Return success status
 */

BOOL OvalDupData(OvalObjPtr ovalObj, OvalObjPtr newObj)
{
	return (RectDupData((RectObjPtr) ovalObj, (RectObjPtr) newObj));
}

/*
 *	Draw routine for tracking oval creation
 */

static void OvalTrackCreate(WindowPtr window, WORD xStart, WORD yStart, WORD xEnd, WORD yEnd,
					  BOOL change)
{
	OvalObj ovalObj;

	if (change) {
		BlockClear(&ovalObj, sizeof(OvalObj));
		SetFrameRect(&ovalObj.DocObj.Frame, xStart, yStart, xEnd, yEnd);
		OvalDrawOutline(window, &ovalObj, 0, 0);
	}
}

/*
 *	Constrain routine for oval creation
 */

static void OvalConstrainCreate(WORD xStart, WORD yStart, WORD *xEnd, WORD *yEnd)
{
	WORD width, height, min;

	width = ABS(*xEnd - xStart);
	height = ABS(*yEnd - yStart);
	min = MIN(width, height);
	if (*xEnd >= xStart)
		*xEnd = xStart + min;
	else
		*xEnd = xStart - min;
	if (*yEnd >= yStart)
		*yEnd = yStart + min;
	else
		*yEnd = yStart - min;
}

/*
 *	Create oval object
 */

DocObjPtr OvalCreate(WindowPtr window, UWORD modifier, WORD mouseX, WORD mouseY)
{
	WORD xStart, yStart, xEnd, yEnd;
	DocDataPtr docData = GetWRefCon(window);

/*
	Track object size
*/
	WindowToDoc(window, mouseX, mouseY, &xStart, &yStart);
	SnapToGrid(docData, &xStart, &yStart);
	xEnd = xStart;
	yEnd = yStart;
	TrackMouse(window, modifier, &xEnd, &yEnd, OvalTrackCreate, OvalConstrainCreate);
/*
	Create oval
*/
	return ((DocObjPtr) CreateOval(docData, xStart, yStart, xEnd, yEnd));
}

/*
 *	Draw routine for tracking oval grow
 */

static void OvalTrackGrow(WindowPtr window, WORD xStart, WORD yStart, WORD xEnd, WORD yEnd,
						  BOOL change)
{
	OvalObj ovalObj;
	Rectangle rect;

	if (change && (xStart != xEnd || yStart != yEnd || wasMoved)) {
		BlockClear(&ovalObj, sizeof(OvalObj));
		rect = growOvalObj->DocObj.Frame;
		if (growHandle == 0 || growHandle == 2)
			rect.MinX = xEnd;
		else
			rect.MaxX = xEnd;
		if (growHandle == 0 || growHandle == 1)
			rect.MinY = yEnd;
		else
			rect.MaxY = yEnd;
		SetFrameRect(&ovalObj.DocObj.Frame, rect.MinX, rect.MinY, rect.MaxX, rect.MaxY);
		OvalDrawOutline(window, &ovalObj, 0, 0);
		wasMoved = TRUE;
	}
}

/*
 *	Track mouse and change oval shape
 */

void OvalGrow(WindowPtr window, OvalObjPtr ovalObj, UWORD modifier, WORD mouseX, WORD mouseY)
{
	WORD xStart, yStart, xEnd, yEnd;
	DocDataPtr docData = GetWRefCon(window);
	Point pt;
	Rectangle rect, handleRect;

	growOvalObj = ovalObj;
	GetHandleRect(window, &handleRect);
/*
	Track handle movement
*/
	wasMoved = FALSE;
	WindowToDoc(window, mouseX, mouseY, &xStart, &yStart);
	pt.x = xStart - ovalObj->DocObj.Frame.MinX;
	pt.y = yStart - ovalObj->DocObj.Frame.MinY;
	if ((growHandle = OvalHandle(ovalObj, &pt, &handleRect)) == -1)
		return;
	SnapToGrid(docData, &xStart, &yStart);
	xEnd = xStart;
	yEnd = yStart;
	TrackMouse(window, modifier, &xEnd, &yEnd, OvalTrackGrow, NULL);
/*
	Set new object dimensions
*/
	if (xEnd != xStart || yEnd != yStart) {
		InvalObjectRect(window, (DocObjPtr) ovalObj);
		HiliteSelectOff(window);
		rect = ovalObj->DocObj.Frame;
		if (growHandle == 0 || growHandle == 2)
			rect.MinX = xEnd;
		else
			rect.MaxX = xEnd;
		if (growHandle == 0 || growHandle == 1)
			rect.MinY = yEnd;
		else
			rect.MaxY = yEnd;
		SetFrameRect(&ovalObj->DocObj.Frame, rect.MinX, rect.MinY, rect.MaxX, rect.MaxY);
		HiliteSelectOn(window);
		InvalObjectRect(window, (DocObjPtr) ovalObj);
	}
}

/*
 *	Create new oval given REXX arguments
 */

OvalObjPtr OvalNewREXX(DocDataPtr docData, TextPtr args)
{
	Point pt1, pt2;

/*
	Get start and end points
*/
	args = GetArgPoint(args, docData, &pt1);
	if (args)
		args = GetArgPoint(args, docData, &pt2);
	if (args == NULL ||
		pt1.x < 0 || pt1.y < 0 || pt1.x >= docData->DocWidth || pt1.y >= docData->DocHeight ||
		pt2.x < 0 || pt2.y < 0 || pt2.x >= docData->DocWidth || pt2.y >= docData->DocHeight)
		return (NULL);
/*
	Create oval
*/
	return (CreateOval(docData, pt1.x, pt1.y, pt2.x, pt2.y));
}
