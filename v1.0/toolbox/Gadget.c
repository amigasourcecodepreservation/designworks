/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Toolbox library
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Gadget routines
 */

#define INTUI_V36_NAMES_ONLY	1

#include <exec/types.h>
#include <intuition/intuition.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/layers.h>
#include <proto/intuition.h>

#include <string.h>

#include "typedefs.h"

#include "Gadget.h"
#include "Utility.h"
#include "Memory.h"

/*
 *	External variables
 */

#extern struct Library	*IntuitionBase;

extern BOOL			_tbHiRes;
extern TextAttrPtr	_tbTextAttr;

extern WORD	_tbBlackColor, _tbWhiteColor;
extern WORD	_tbDarkColor, _tbLightColor;

/*
 *	Local variables and definitions
 */

#define SELECTBUTTON(q)	\
	((q&IEQUALIFIER_LEFTBUTTON)||((q&AMIGAKEYS)&&(q&IEQUALIFIER_LALT)))

/*
 *	Gadget images
 */

static UWORD chip hrOffCheckData[] = {
	0x0000, 0x7FE0, 0x7FE0, 0x7FE0, 0x7FE0, 0x7FE0,
	0x7FE0, 0x7FE0, 0x7FE0, 0x7FE0, 0x7FE0, 0x0000,
};

static UWORD chip hrSelOffCheckData[] = {
	0x0000, 0x0000, 0x0020, 0x1FA0, 0x1FA0, 0x1FA0,
	0x1FA0, 0x1FA0, 0x1FA0, 0x0020, 0x3FE0, 0x0000,
};

static UWORD chip hrOnCheckData[] = {
	0x0000, 0x7FC0, 0x4000, 0x5F80, 0x5F80, 0x5F80,
	0x5F80, 0x5F80, 0x5F80, 0x4000, 0x0000, 0x0000,

	0x0000, 0x0000, 0x0000, 0x1F80, 0x1F80, 0x1F80,
	0x1F80, 0x1F80, 0x1F80, 0x0000, 0x0000, 0x0000,

	0x0000, 0x0000, 0x0000, 0x1F80, 0x1F80, 0x1F80,
	0x1F80, 0x1F80, 0x1F80, 0x0000, 0x0000, 0x0000,

	0x0000, 0x0000, 0x0000, 0x1F80, 0x1F80, 0x1F80,
	0x1F80, 0x1F80, 0x1F80, 0x0000, 0x0000, 0x0000,
};

static UWORD chip hrSelOnCheckData[] = {
	0x0000, 0x0000, 0x0020, 0x1FA0, 0x1FA0, 0x1FA0,
	0x1FA0, 0x1FA0, 0x1FA0, 0x0020, 0x3FE0, 0x0000,

	0x0000, 0x0000, 0x0000, 0x1F80, 0x1F80, 0x1F80,
	0x1F80, 0x1F80, 0x1F80, 0x0000, 0x0000, 0x0000,

	0x0000, 0x0000, 0x0000, 0x1F80, 0x1F80, 0x1F80,
	0x1F80, 0x1F80, 0x1F80, 0x0000, 0x0000, 0x0000,

	0x0000, 0x0000, 0x0000, 0x1F80, 0x1F80, 0x1F80,
	0x1F80, 0x1F80, 0x1F80, 0x0000, 0x0000, 0x0000,
};

static UWORD chip hrOffRadioData[] = {
	0xF0F0, 0xCF30, 0xBFD0, 0xBFD0, 0x7FE0, 0x7FE0,
	0x7FE0, 0x7FE0, 0xBFD0, 0xBFD0, 0xCF30, 0xF0F0,
};

static UWORD chip hrSelOffRadioData[] = {
	0xF0F0, 0xC030, 0x8050, 0x8F50, 0x1FA0, 0x1FA0,
	0x1FA0, 0x1FA0, 0x8F50, 0xB0D0, 0xCF30, 0xF0F0,
};

static UWORD chip hrOnRadioData[] = {
	0xF0F0, 0xCF30, 0xB0D0, 0xAF10, 0x5F80, 0x5F80,
	0x5F80, 0x5F80, 0xAF10, 0xA010, 0xC030, 0xF0F0,

	0x0000, 0x0000, 0x0000, 0x0F00, 0x1F80, 0x1F80,
	0x1F80, 0x1F80, 0x0F00, 0x0000, 0x0000, 0x0000,

	0x0000, 0x0000, 0x0000, 0x0F00, 0x1F80, 0x1F80,
	0x1F80, 0x1F80, 0x0F00, 0x0000, 0x0000, 0x0000,

	0x0000, 0x0000, 0x0000, 0x0F00, 0x1F80, 0x1F80,
	0x1F80, 0x1F80, 0x0F00, 0x0000, 0x0000, 0x0000,
};

static UWORD chip hrSelOnRadioData[] = {
	0xF0F0, 0xC030, 0x8050, 0x8F50, 0x1FA0, 0x1FA0,
	0x1FA0, 0x1FA0, 0x8F50, 0xB0D0, 0xCF30, 0xF0F0,

	0x0000, 0x0000, 0x0000, 0x0F00, 0x1F80, 0x1F80,
	0x1F80, 0x1F80, 0x0F00, 0x0000, 0x0000, 0x0000,

	0x0000, 0x0000, 0x0000, 0x0F00, 0x1F80, 0x1F80,
	0x1F80, 0x1F80, 0x0F00, 0x0000, 0x0000, 0x0000,

	0x0000, 0x0000, 0x0000, 0x0F00, 0x1F80, 0x1F80,
	0x1F80, 0x1F80, 0x0F00, 0x0000, 0x0000, 0x0000,
};

static Image hrOffCheckImage = {
	0, 0, 12, 12, 1, &hrOffCheckData[0], 1, 0, NULL
};

static Image hrSelOffCheckImage = {
	0, 0, 12, 12, 1, &hrSelOffCheckData[0], 1, 0, NULL
};

static Image hrOnCheckImage = {
	0, 0, 12, 12, 4, &hrOnCheckData[0], 0x0F, 0, NULL
};

static Image hrSelOnCheckImage = {
	0, 0, 12, 12, 4, &hrSelOnCheckData[0], 0x0F, 0, NULL
};

static Image hrOffRadioImage = {
	0, 0, 12, 12, 1, &hrOffRadioData[0], 1, 0, NULL
};

static Image hrSelOffRadioImage = {
	0, 0, 12, 12, 1, &hrSelOffRadioData[0], 1, 0, NULL
};

static Image hrOnRadioImage = {
	0, 0, 12, 12, 4, &hrOnRadioData[0], 0x0F, 0, NULL
};

static Image hrSelOnRadioImage = {
	0, 0, 12, 12, 4, &hrSelOnRadioData[0], 0x0F, 0, NULL
};

static UWORD chip mrOffCheckData[] = {
	0x0000, 0x3FC0, 0x3FC0, 0x3FC0, 0x3FC0, 0x3FC0, 0x3FC0, 0x0000,
};

static UWORD chip mrSelOffCheckData[] = {
	0x0000, 0x0000, 0x0040, 0x0F40, 0x0F40, 0x0040, 0x1FC0, 0x0000,
};

static UWORD chip mrOnCheckData[] = {
	0x0000, 0x3F80, 0x2000, 0x2F00, 0x2F00, 0x2000, 0x0000, 0x0000,

	0x0000, 0x0000, 0x0000, 0x0F00, 0x0F00, 0x0000, 0x0000, 0x0000,

	0x0000, 0x0000, 0x0000, 0x0F00, 0x0F00, 0x0000, 0x0000, 0x0000,

	0x0000, 0x0000, 0x0000, 0x0F00, 0x0F00, 0x0000, 0x0000, 0x0000,
};

static UWORD chip mrSelOnCheckData[] = {
	0x0000, 0x0000, 0x0040, 0x0F40, 0x0F40, 0x0040, 0x1FC0, 0x0000,

	0x0000, 0x0000, 0x0000, 0x0F00, 0x0F00, 0x0000, 0x0000, 0x0000,

	0x0000, 0x0000, 0x0000, 0x0F00, 0x0F00, 0x0000, 0x0000, 0x0000,

	0x0000, 0x0000, 0x0000, 0x0F00, 0x0F00, 0x0000, 0x0000, 0x0000,
};

static UWORD chip mrOffRadioData[] = {
	0xE070, 0x8F10, 0x3FC0, 0x3FC0, 0x3FC0, 0x3FC0, 0x8F10, 0xE070,
};

static UWORD chip mrSelOffRadioData[] = {
	0xE070, 0x8010, 0x00C0, 0x0F40, 0x0F40, 0x00C0, 0x8F10, 0xE070,
};

static UWORD chip mrOnRadioData[] = {
	0xE070, 0x8F10, 0x3000, 0x2F00, 0x2F00, 0x3000, 0x8010, 0xE070,

	0x0000, 0x0000, 0x0000, 0x0F00, 0x0F00, 0x0000, 0x0000, 0x0000,

	0x0000, 0x0000, 0x0000, 0x0F00, 0x0F00, 0x0000, 0x0000, 0x0000,

	0x0000, 0x0000, 0x0000, 0x0F00, 0x0F00, 0x0000, 0x0000, 0x0000,
};

static UWORD chip mrSelOnRadioData[] = {
	0xE070, 0x8010, 0x00C0, 0x0F40, 0x0F40, 0x00C0, 0x8F10, 0xE070,

	0x0000, 0x0000, 0x0000, 0x0F00, 0x0F00, 0x0000, 0x0000, 0x0000,

	0x0000, 0x0000, 0x0000, 0x0F00, 0x0F00, 0x0000, 0x0000, 0x0000,

	0x0000, 0x0000, 0x0000, 0x0F00, 0x0F00, 0x0000, 0x0000, 0x0000,
};

static Image mrOffCheckImage = {
	0, 0, 12, 8, 1, &mrOffCheckData[0], 1, 0, NULL
};

static Image mrSelOffCheckImage = {
	0, 0, 12, 8, 1, &mrSelOffCheckData[0], 1, 0, NULL
};

static Image mrOnCheckImage = {
	0, 0, 12, 8, 4, &mrOnCheckData[0], 0x0F, 0, NULL
};

static Image mrSelOnCheckImage = {
	0, 0, 12, 8, 4, &mrSelOnCheckData[0], 0x0F, 0, NULL
};

static Image mrOffRadioImage = {
	0, 0, 12, 8, 1, &mrOffRadioData[0], 1, 0, NULL
};

static Image mrSelOffRadioImage = {
	0, 0, 12, 8, 1, &mrSelOffRadioData[0], 1, 0, NULL
};

static Image mrOnRadioImage = {
	0, 0, 12, 8, 4, &mrOnRadioData[0], 0x0F, 0, NULL
};

static Image mrSelOnRadioImage = {
	0, 0, 12, 8, 4, &mrSelOnRadioData[0], 0x0F, 0, NULL
};

static ImagePtr	offCheckImage, selOffCheckImage, onCheckImage, selOnCheckImage;
static ImagePtr	offRadioImage, selOffRadioImage, onRadioImage, selOnRadioImage;

/*
 *	Local prototypes
 */

void	SetGadgetImages(void);
void	GetGadgetRect(GadgetPtr, WindowPtr, RequestPtr, RectPtr);
void	OnOffGList(GadgetPtr, WindowPtr, RequestPtr, WORD, BOOL);

/*
 *	Set local pointers to gadget images to appropriate actual images
 */

static void SetGadgetImages()
{
	if (_tbHiRes) {
		offCheckImage    = &hrOffCheckImage;
		selOffCheckImage = &hrSelOffCheckImage;
		onCheckImage     = &hrOnCheckImage;
		selOnCheckImage  = &hrSelOnCheckImage;
		offRadioImage    = &hrOffRadioImage;
		selOffRadioImage = &hrSelOffRadioImage;
		onRadioImage     = &hrOnRadioImage;
		selOnRadioImage  = &hrSelOnRadioImage;
	}
	else {
		offCheckImage    = &mrOffCheckImage;
		selOffCheckImage = &mrSelOffCheckImage;
		onCheckImage     = &mrOnCheckImage;
		selOnCheckImage  = &mrSelOnCheckImage;
		offRadioImage    = &mrOffRadioImage;
		selOffRadioImage = &mrSelOffRadioImage;
		onRadioImage     = &mrOnRadioImage;
		selOnRadioImage  = &mrSelOnRadioImage;
	}
}

/*
 *	Return rectangle containing gadget
 */

static void GetGadgetRect(register GadgetPtr gadget, WindowPtr window,
						  RequestPtr request, RectPtr rect)
{
	register WORD leftEdge, topEdge, width, height;
	WORD elemWidth, elemHeight, flags;

	if (gadget->GadgetType & GTYP_REQGADGET) {
		elemWidth  = request->Width;
		elemHeight = request->Height;
	}
	else {
		elemWidth  = window->Width;
		elemHeight = window->Height;
	}
	leftEdge = gadget->LeftEdge;
	topEdge = gadget->TopEdge;
	width = gadget->Width;
	height = gadget->Height;
	flags = gadget->Flags;
	if (flags & GFLG_RELRIGHT)
		leftEdge += elemWidth - 1;
	if (flags & GFLG_RELBOTTOM)
		topEdge += elemHeight - 1;
	if (flags & GFLG_RELWIDTH)
		width += elemWidth - 1;
	if (flags & GFLG_RELHEIGHT)
		height += elemHeight - 1;
	rect->MinX = leftEdge;
	rect->MinY = topEdge;
	rect->MaxX = leftEdge + width - 1;
	rect->MaxY = topEdge + height - 1;
}

/*
 *	GetGadgets
 *	Process gadgets template and return pointer to gadget list
 *	Return NULL if error
 *
 *	Notes:
 *
 *	The data fields in gadget template are used directly, except for
 *		titles and static text, where a copy is made
 *
 *	The buffer size for edit text items is set at 256 bytes
 *
 *	Gadget type is stored in lower byte of GadgetID field
 *	Gadget item number is stored in upper byte of GadgetID field
 */

GadgetPtr GetGadgets(GadgetTemplate gadgTempl[])
{
	register WORD item, type, width, height, left, top;
	WORD xSize, ySize;
	Ptr gadgInfo;
	register GadgetPtr gadget, firstGadget, prevGadget;
	StrInfoPtr stringInfo;
	PropInfoPtr propInfo;
	BorderPtr border;
	IntuiTextPtr intuiText;

/*
	Get font dimensions
	(Use width of "e" rather than font's XSize,
		since this works better for proportional fonts)
*/
	intuiText = NewIntuiText(0, 0, "e", FS_NORMAL, 0);
	if (intuiText == NULL)
		xSize = 8;
	else {
		xSize = IntuiTextLength(intuiText);
		FreeIntuiText(intuiText);
	}
	ySize = _tbTextAttr->ta_YSize;
/*
	Process gadget templates
*/
	SetGadgetImages();
	firstGadget = prevGadget = NULL;
	for (item = 0; gadgTempl[item].Type != GADG_ITEM_NONE; item++) {
		if ((gadget = MemAlloc(sizeof(Gadget), MEMF_CLEAR)) == NULL) {
			DisposeGadgets(firstGadget);
			return (NULL);
		}
		if (firstGadget == NULL)
			firstGadget = gadget;
		if (prevGadget)
			prevGadget->NextGadget = gadget;
/*
	Assign common parameters
*/
		gadget->LeftEdge	= left		=
			(gadgTempl[item].LeftEdge*xSize)/8 + gadgTempl[item].LeftOffset;
		gadget->TopEdge		= top		=
			(gadgTempl[item].TopEdge*ySize)/11 + gadgTempl[item].TopOffset;
		gadget->Width		= width		=
			(gadgTempl[item].Width*xSize)/8   + gadgTempl[item].WidthOffset;
		gadget->Height		= height	=
			(gadgTempl[item].Height*ySize)/11 + gadgTempl[item].HeightOffset;
		gadget->GadgetID	= (gadgTempl[item].Type & 0xFF) | (item << 8);
		gadgInfo			= gadgTempl[item].Info;
		if (gadget->LeftEdge < 0)
			gadget->Flags |= GFLG_RELRIGHT;
		if (gadget->TopEdge < 0)
			gadget->Flags |= GFLG_RELBOTTOM;
		if (gadget->Width < 0)
			gadget->Flags |= GFLG_RELWIDTH;
		if (gadget->Height < 0)
			gadget->Flags |= GFLG_RELHEIGHT;
/*
	Find and process the specified gadget type
*/
		switch (type = GadgetType(gadget)) {
/*
	User Item
*/
		case GADG_USER_ITEM:
			if (firstGadget == gadget)
				firstGadget = gadgInfo;
			if (prevGadget)
				prevGadget->NextGadget = gadgInfo;
			MemFree(gadget, sizeof(Gadget));
			gadget = (GadgetPtr) gadgInfo;
			break;
/*
	Push Button
*/
		case GADG_PUSH_BUTTON:
			gadget->Flags      |= GFLG_GADGHIMAGE;
			gadget->Activation |= GACT_IMMEDIATE | GACT_RELVERIFY;
			gadget->GadgetType |= GTYP_BOOLGADGET;
			if ((gadget->GadgetRender = (APTR) ShadowBoxBorder(width, height, 0, TRUE)) == NULL ||
				(gadget->SelectRender = (APTR) ShadowBoxBorder(width, height, 0, FALSE)) == NULL) {
				DisposeGadgets(firstGadget);
				return (NULL);
			}
			left = width/2;
			top  = (height - ySize + 1)/2;
			if ((intuiText = NewIntuiText(left, top, gadgInfo, FS_NORMAL, _tbBlackColor)) == NULL) {
				DisposeGadgets(firstGadget);
				return (NULL);
			}
			intuiText->LeftEdge -= IntuiTextLength(intuiText)/2;
			gadget->GadgetText   = intuiText;
			break;
/*
	Check Box
*/
		case GADG_CHECK_BOX:
			gadget->Flags		|= GFLG_GADGHIMAGE | GFLG_GADGIMAGE;
			gadget->Activation	|= GACT_IMMEDIATE | GACT_RELVERIFY;
			gadget->GadgetType	|= GTYP_BOOLGADGET;
			gadget->TopEdge		-= 1;
			gadget->Height		 = ySize + 1;
			if (gadgTempl[item].Type & GADG_VALUE_ON) {
				gadget->GadgetRender = (APTR) onCheckImage;
				gadget->SelectRender = (APTR) selOnCheckImage;
			}
			else {
				gadget->GadgetRender = (APTR) offCheckImage;
				gadget->SelectRender = (APTR) selOffCheckImage;
			}
			if ((intuiText = NewIntuiText(16, 1, gadgInfo, FS_NORMAL, _tbBlackColor)) == NULL) {
				DisposeGadgets(firstGadget);
				return (NULL);
			}
			gadget->GadgetText = intuiText;
			break;
/*
	Radio Button
*/
		case GADG_RADIO_BUTTON:
			gadget->Flags		|= GFLG_GADGHIMAGE | GFLG_GADGIMAGE;
			gadget->Activation	|= GACT_IMMEDIATE | GACT_RELVERIFY;
			gadget->GadgetType	|= GTYP_BOOLGADGET;
			gadget->TopEdge		-= 1;
			gadget->Height		 = ySize + 1;
			if (gadgTempl[item].Type & GADG_VALUE_ON) {
				gadget->GadgetRender = (APTR) onRadioImage;
				gadget->SelectRender = (APTR) selOnRadioImage;
			}
			else {
				gadget->GadgetRender = (APTR) offRadioImage;
				gadget->SelectRender = (APTR) selOffRadioImage;
			}
			if ((intuiText = NewIntuiText(16, 1, gadgInfo, FS_NORMAL, _tbBlackColor)) == NULL) {
				DisposeGadgets(firstGadget);
				return (NULL);
			}
			gadget->GadgetText = intuiText;
			break;
/*
	Static Text
*/
		case GADG_STAT_TEXT:
			gadget->Flags		|= GFLG_GADGHNONE;
			gadget->GadgetType	|= GTYP_BOOLGADGET;
			if ((intuiText = NewIntuiText(0, 0, gadgInfo, FS_NORMAL, _tbBlackColor)) == NULL) {
				DisposeGadgets(firstGadget);
				return (NULL);
			}
			gadget->GadgetText = intuiText;
			break;
/*
	Edit Text
	Sets gadget->Height to height of current font
*/
		case GADG_EDIT_TEXT:
			gadget->Flags		|= GFLG_GADGHCOMP;
			gadget->Activation	|= GACT_IMMEDIATE | GACT_RELVERIFY;
			gadget->GadgetType	|= GTYP_STRGADGET;
			gadget->Height		 = height = ySize;
#ifdef NEW_LOOK
			if ((border = ShadowBoxBorder(width, height, -2, FALSE)) == NULL) {
#else
			if ((border = BoxBorder(width, height, _tbBlackColor, -2)) == NULL) {
#endif
				DisposeGadgets(firstGadget);
				return (NULL);
			}
			gadget->GadgetRender = (APTR) border;
			if ((stringInfo = MemAlloc(sizeof(StringInfo), MEMF_CLEAR)) == NULL) {
				DisposeGadgets(firstGadget);
				return (NULL);
			}
			gadget->SpecialInfo = (APTR) stringInfo;
			stringInfo->MaxChars = GADG_MAX_STRING;
			if ((gadgInfo && strlen((BYTE *) gadgInfo) >= GADG_MAX_STRING) ||
				(stringInfo->Buffer = MemAlloc(GADG_MAX_STRING, 0)) == NULL ||
				(stringInfo->UndoBuffer = MemAlloc(GADG_MAX_STRING, 0)) == NULL) {
				DisposeGadgets(firstGadget);
				return (NULL);
			}
			if (gadgInfo)
				strcpy(stringInfo->Buffer, (BYTE *) gadgInfo);
			else
				stringInfo->Buffer[0] = '\0';
			break;
/*
	Border item
*/
		case GADG_STAT_BORDER:
		case GADG_ACTIVE_BORDER:
			if (type == GADG_ACTIVE_BORDER) {
				gadget->Flags		|= GFLG_GADGHCOMP;
				gadget->Activation	|= GACT_IMMEDIATE | GACT_RELVERIFY;
			}
			else
				gadget->Flags		|= GFLG_GADGHNONE;
			gadget->GadgetType		|= GTYP_BOOLGADGET;
			gadget->GadgetRender	 = gadgInfo;
			break;
/*
	Image item
*/
		case GADG_STAT_IMAGE:
		case GADG_ACTIVE_IMAGE:
			if (type == GADG_ACTIVE_IMAGE) {
				gadget->Flags		|= GFLG_GADGHCOMP | GFLG_GADGIMAGE;
				gadget->Activation	|= GACT_IMMEDIATE | GACT_RELVERIFY;
			}
			else
				gadget->Flags		|= GFLG_GADGHNONE | GFLG_GADGIMAGE;
			gadget->GadgetType		|= GTYP_BOOLGADGET;
			gadget->GadgetRender	 = gadgInfo;
			break;
/*
	Standard Image item
*/
		case GADG_STAT_STDIMAGE:
		case GADG_ACTIVE_STDIMAGE:
			if (type == GADG_ACTIVE_STDIMAGE) {
				gadget->Flags		|= GFLG_GADGHCOMP | GFLG_GADGIMAGE;
				gadget->Activation	|= GACT_IMMEDIATE | GACT_RELVERIFY;
			}
			else
				gadget->Flags		|= GFLG_GADGHNONE | GFLG_GADGIMAGE;
			gadget->GadgetType		|= GTYP_BOOLGADGET;
			gadget->GadgetRender	 = (APTR) GetImage((UWORD) gadgInfo);
			break;
/*
	Proportional gadgets
*/
		case GADG_PROP_VERT:
		case GADG_PROP_HORIZ:
			gadget->Flags		|= GFLG_GADGHNONE | GFLG_GADGIMAGE;
			gadget->Activation	|= GACT_IMMEDIATE | GACT_RELVERIFY;
			gadget->GadgetType	|= GTYP_PROPGADGET;
			if ((gadget->GadgetRender = MemAlloc(sizeof(Image), MEMF_CLEAR)) == NULL ||
				(propInfo = MemAlloc(sizeof(PropInfo), MEMF_CLEAR)) == NULL) {
				DisposeGadgets(firstGadget);
				return (NULL);
			}
			gadget->SpecialInfo = (APTR) propInfo;
			if (type == GADG_PROP_VERT)
				propInfo->Flags = AUTOKNOB | FREEVERT;
			else
				propInfo->Flags = AUTOKNOB | FREEHORIZ;
			propInfo->HorizBody = propInfo->VertBody = 0xFFFF;
			break;
		}
		prevGadget = gadget;
	}
	return (firstGadget);
}

/*
 *	DisposeGadgets
 *	Release all memory used by gadget list created by GetGadgets
 *	(Remember that fields in gadget template were used directly, and so
 *		should not be released)
 *	Ignores all system gadgets
 */

void DisposeGadgets(register GadgetPtr gadgList)
{
	register GadgetPtr nextGadget;
	register StrInfoPtr stringInfo;

	while (gadgList) {
		nextGadget = gadgList->NextGadget;
		if ((gadgList->GadgetType & GTYP_SYSGADGET) == 0) {
			switch (GadgetType(gadgList)) {
			case GADG_USER_ITEM:
				break;
			case GADG_PUSH_BUTTON:
				FreeBorder((BorderPtr) gadgList->GadgetRender);
				FreeBorder((BorderPtr) gadgList->SelectRender);
				FreeIntuiText(gadgList->GadgetText);
				break;
			case GADG_CHECK_BOX:
			case GADG_RADIO_BUTTON:
			case GADG_STAT_TEXT:
				FreeIntuiText(gadgList->GadgetText);
				break;
			case GADG_EDIT_TEXT:
				FreeBorder((BorderPtr) gadgList->GadgetRender);
				stringInfo = (StrInfoPtr) gadgList->SpecialInfo;
				if (stringInfo) {
					if (stringInfo->Buffer)
						MemFree(stringInfo->Buffer, stringInfo->MaxChars);
					if (stringInfo->UndoBuffer)
						MemFree(stringInfo->UndoBuffer, stringInfo->MaxChars);
					MemFree(stringInfo, sizeof(StringInfo));
				}
				break;
			case GADG_STAT_BORDER:
			case GADG_ACTIVE_BORDER:
			case GADG_STAT_IMAGE:
			case GADG_ACTIVE_IMAGE:
			case GADG_STAT_STDIMAGE:
			case GADG_ACTIVE_STDIMAGE:
				break;
			case GADG_PROP_VERT:
			case GADG_PROP_HORIZ:
				if (gadgList->GadgetRender)
					MemFree(gadgList->GadgetRender, sizeof(Image));
				if (gadgList->SpecialInfo)
					MemFree(gadgList->SpecialInfo, sizeof(PropInfo));
				break;
			}
			MemFree(gadgList, sizeof(Gadget));
		}
		gadgList = nextGadget;
	}
}

/*
 *	GadgetItem
 *	Return a pointer to the gadget identified by its item number
 *	Ignore all system gadgets
 *	If no such gadget then return NULL
 */

GadgetPtr GadgetItem(register GadgetPtr gadgList, register WORD item)
{
	while (gadgList) {
		if ((gadgList->GadgetType & GTYP_SYSGADGET) == 0 &&
			((gadgList->GadgetID >> 8) & 0xFF) == item)
			break;
		gadgList = gadgList->NextGadget;
	}
	return (gadgList);
}

/*
 *	GadgetNumber
 *	Return the gadget number of this gadget
 */

WORD GadgetNumber(GadgetPtr gadget)
{
	return ((gadget->GadgetID >> 8) & 0xFF);
}

/*
 *	GadgetType
 *	Return the gadget type
 */

WORD GadgetType(GadgetPtr gadget)
{
	return (gadget->GadgetID & GADG_TYPE);
}

/*
 *	GetGadgetValue
 *	Get the gadget's value
 *	Valid for gadget types of GADG_CHECK_BOX and GADG_RADIO_BUTTON only!
 */

WORD GetGadgetValue(register GadgetPtr gadget)
{
	register WORD gadgType;

	if (gadget == NULL)
		return (0);
	gadgType = GadgetType(gadget);
	if ((gadgType != GADG_CHECK_BOX && gadgType != GADG_RADIO_BUTTON) ||
		!(gadget->GadgetID & GADG_VALUE_ON))
		return (0);
	return (1);
}

/*
 *	SetGadgetValue
 *	Set the gadget's value to on or off and redraw the gadget
 *	Valid for gadget types of GADG_CHECK_BOX and GADG_RADIO_BUTTON only!
 */

void SetGadgetValue(register GadgetPtr gadget, register WindowPtr window,
					register RequestPtr request, register BOOL value)
{
	register WORD gadgType, position;

	SetGadgetImages();
	if (gadget) {
		gadgType = GadgetType(gadget);
		if (gadgType != GADG_CHECK_BOX && gadgType != GADG_RADIO_BUTTON)
			return;
		position = RemoveGadget(window, gadget);
		if (value) {
			gadget->GadgetID |= GADG_VALUE_ON;
			if (gadgType == GADG_CHECK_BOX) {
				gadget->GadgetRender = (APTR) onCheckImage;
				gadget->SelectRender = (APTR) selOnCheckImage;
			}
			else {
				gadget->GadgetRender = (APTR) onRadioImage;
				gadget->SelectRender = (APTR) selOnRadioImage;
			}
		}
		else {
			gadget->GadgetID &= ~GADG_VALUE_ON;
			if (gadgType == GADG_CHECK_BOX) {
				gadget->GadgetRender = (APTR) offCheckImage;
				gadget->SelectRender = (APTR) selOffCheckImage;
			}
			else {
				gadget->GadgetRender = (APTR) offRadioImage;
				gadget->SelectRender = (APTR) selOffRadioImage;
			}
		}
		AddGList(window, gadget, position, 1, request);
		RefreshGList(gadget, window, request, 1);
	}
}

/*
 *	Set GadgetItemValue
 *	Set gadget value given item number
 */

void SetGadgetItemValue(GadgetPtr gadgList, WORD item, WindowPtr window,
						RequestPtr request, BOOL value)
{
	GadgetPtr gadget;

	gadget = GadgetItem(gadgList, item);
	SetGadgetValue(gadget, window, request, value);
}

/*
 *	HiliteGadget
 *	Set the gadget's display appearance to selected or not selected
 */

void HiliteGadget(register GadgetPtr gadget, WindowPtr window,
				  RequestPtr request, BOOL hilite)
{
	register WORD position;
	register UWORD flags;
	BOOL wasSelected;
	RastPtr rPort;
	Rectangle gadgRect;

	wasSelected = (gadget->Flags & GFLG_SELECTED);
	if ((hilite && wasSelected) || (!hilite && !wasSelected))
		return;
	if ((position = RemoveGadget(window, gadget)) == -1)
		return;
	if (hilite)
		gadget->Flags |= GFLG_SELECTED;
	else
		gadget->Flags &= ~GFLG_SELECTED;
	GetGadgetRect(gadget, window, request, &gadgRect);
	rPort = (request) ? request->ReqLayer->rp : window->RPort;
	flags = gadget->Flags;
	if ((flags & GFLG_GADGHIGHBITS) == GFLG_GADGHCOMP) {
		SetAPen(rPort, _tbLightColor);
		SetDrMd(rPort, JAM1);
		RectFill(rPort, gadgRect.MinX, gadgRect.MinY, gadgRect.MaxX, gadgRect.MaxY);
	}
	else if ((flags & GFLG_GADGHIGHBITS) == GFLG_GADGHBOX && wasSelected) {
		SetDrMd(rPort, COMPLEMENT);
		RectFill(rPort, gadgRect.MinX - 4, gadgRect.MinY - 2,
						gadgRect.MaxX + 4, gadgRect.MaxY + 2);
	}
	(void) AddGList(window, gadget, position, 1, request);
	RefreshGList(gadget, window, request, 1);
}

/*
 *	GadgetSelected
 *	Return TRUE if gadget is selected
 */

BOOL GadgetSelected(GadgetPtr gadgList, WORD gadgNum)
{
	GadgetPtr gadget;

	gadget = GadgetItem(gadgList, gadgNum);
	if (gadget->Flags & GFLG_SELECTED)
		return (TRUE);
	return (FALSE);
}

/*
 *	Enable/disable gadget list
 *	Waits until gadget are not selected before changing
 */

static void OnOffGList(register GadgetPtr gadgList, WindowPtr window,
					   RequestPtr request, register WORD num, BOOL on)
{
	register WORD i, position;
	register BOOL selected;
	register GadgetPtr gadget;
	RastPtr rPort;
	Rectangle gadgRect;

/*
	Scan gadget list and wait until none are selected
*/
	do {
		selected = FALSE;
		for (gadget = gadgList, i = 0; gadget && i < num; gadget = gadget->NextGadget, i++) {
			if (gadget->Flags & GFLG_SELECTED)
				selected = TRUE;
		}
	} while (selected);
/*
	Turn on/off the gadgets
*/
	if ((position = RemoveGList(window, gadgList, num)) == -1)
		return;
	rPort = (request) ? request->ReqLayer->rp : window->RPort;
	for (gadget = gadgList, i = 0; gadget && i < num; gadget = gadget->NextGadget, i++) {
		if (on)
			gadget->Flags &= ~GFLG_DISABLED;
		else
			gadget->Flags |= GFLG_DISABLED;
		GetGadgetRect(gadget, window, request, &gadgRect);
		SetAPen(rPort, _tbLightColor);
		SetDrMd(rPort, JAM1);
		RectFill(rPort, gadgRect.MinX, gadgRect.MinY, gadgRect.MaxX, gadgRect.MaxY);
	}
	(void) AddGList(window, gadgList, position, num, request);
	RefreshGList(gadgList, window, request, num);
}

/*
 *	OnGList
 *	Enable the specified number of gadgets
 *	Unlike OnGadget(), this function only refreshes the specified gadgets
 */

void OnGList(GadgetPtr gadgList, WindowPtr window, RequestPtr request, WORD num)
{
	OnOffGList(gadgList, window, request, num, TRUE);
}

/*
 *	OffGList
 *	Disable the specified number of gadgets
 *	Unlike OffGadget(), this function only refreshes the specified gadgets
 */

void OffGList(GadgetPtr gadgList, WindowPtr window, RequestPtr request, WORD num)
{
	OnOffGList(gadgList, window, request, num, FALSE);
}

/*
 *	EnableGadgetItem
 *	Enable or disable the specified gadget
 *	Only refreshes if gadget state changed
 */

void EnableGadgetItem(GadgetPtr gadgList, WORD gadgNum, WindowPtr window,
					  RequestPtr request, BOOL enable)
{
	register GadgetPtr gadget;

	gadget = GadgetItem(gadgList, gadgNum);
	if (enable) {
		if (gadget->Flags & GFLG_DISABLED)
			OnOffGList(gadget, window, request, 1, TRUE);
	}
	else {
		if ((gadget->Flags & GFLG_DISABLED) == 0)
			OnOffGList(gadget, window, request, 1, FALSE);
	}
}

/*
 *	OutlineButton
 *	Add or remove outlining of specified button
 */

void OutlineButton(GadgetPtr gadget, WindowPtr window, RequestPtr request, BOOL on)
{
	BorderPtr border;
	RastPtr rPort;
	Rectangle gadgRect;
#ifndef NEW_LOOK
	WORD color;
#endif

	if (GadgetType(gadget) != GADG_PUSH_BUTTON)
		return;
#ifndef NEW_LOOK
	if (window->WScreen->BitMap.Depth == 1)
		color = (on) ? 0 : 1;
	else
		color = (on) ? ~0 : 1;
#endif
	GetGadgetRect(gadget, window, request, &gadgRect);
#ifdef NEW_LOOK
	if ((border = ShadowBoxBorder(gadgRect.MaxX - gadgRect.MinX + 1,
								  gadgRect.MaxY - gadgRect.MinY + 1,
								  -2, TRUE)) == NULL)
#else
	if ((border = BoxBorder(gadgRect.MaxX - gadgRect.MinX + 1,
							gadgRect.MaxY - gadgRect.MinY + 1,
							color, -2)) == NULL)
#endif
		return;
	rPort = (request) ? request->ReqLayer->rp : window->RPort;
	DrawBorder(rPort, border, gadgRect.MinX, gadgRect.MinY);
	FreeBorder(border);
}

/*
 *	GetEditItemText
 *	Get edit text and put in specified buffer
 */

void GetEditItemText(GadgetPtr gadgList, WORD item, TextPtr buff)
{
	register GadgetPtr gadget;

	gadget = GadgetItem(gadgList, item);
	strcpy(buff, ((StrInfoPtr) gadget->SpecialInfo)->Buffer);
}

/*
 *	SetEditItemText
 *	Set specified edit text buffer to new contents
 *	If buff is NULL, clear edit text
 */

void SetEditItemText(WindowPtr window, RequestPtr request, WORD item, TextPtr buff)
{
	register WORD position;
	register GadgetPtr gadget, gadgList;
	register StrInfoPtr stringInfo;
	BOOL wasSelected;

	gadgList = (request) ? request->ReqGadget : window->FirstGadget;
	gadget = GadgetItem(gadgList, item);
	wasSelected = gadget->Flags & GFLG_SELECTED;
	position = RemoveGadget(window, gadget);
	gadget->Flags &= ~GFLG_SELECTED;			/* Need this for proper redrawing */
	stringInfo = (StrInfoPtr) gadget->SpecialInfo;
	if (buff)
		strcpy(stringInfo->Buffer, buff);
	else
		stringInfo->Buffer[0] = '\0';
	stringInfo->BufferPos = stringInfo->DispPos = 0;
	AddGList(window, gadget, position, 1, request);
	RefreshGList(gadget, window, request, 1);
	if (wasSelected)
		ActivateGadget(gadget, window, request);
}

/*
 *	GetGadgetItemText
 *	Get gadget text (not edit text)
 */

void GetGadgetItemText(GadgetPtr gadgList, WORD item, TextPtr text)
{
	IntuiTextPtr intuiText;
	GadgetPtr gadget;

	gadget = GadgetItem(gadgList, item);
	intuiText = gadget->GadgetText;
	if (intuiText && intuiText->IText)
		strcpy(text, intuiText->IText);
	else
		*text = '\0';
}

/*
 *	SetGadgetText
 *	Set gadget text (not edit text) to new text
 */

void SetGadgetText(GadgetPtr gadget, WindowPtr window, RequestPtr request,
				   TextPtr text)
{
	WORD oldColor;
	TextPtr oldText;
	IntuiTextPtr intuiText;

	intuiText = gadget->GadgetText;
	if (intuiText) {
		while (gadget->Flags & GFLG_SELECTED) ;	/* Wait until user releases button */
		oldColor = intuiText->FrontPen;
		oldText = intuiText->IText;
		intuiText->FrontPen = _tbLightColor;
		RefreshGList(gadget, window, request, 1);
		ChangeIntuiText(intuiText, text);
		if (GadgetType(gadget) == GADG_PUSH_BUTTON && text)
			intuiText->LeftEdge = (gadget->Width - IntuiTextLength(intuiText))/2;
		intuiText->FrontPen = oldColor;
		RefreshGList(gadget, window, request, 1);
	}
}


/*
 *	SetGadgetItemText
 *	Set gadget text, given item number
 */

void SetGadgetItemText(GadgetPtr gadgList, WORD item, WindowPtr window,
					   RequestPtr request, TextPtr text)
{
	GadgetPtr gadget;

	gadget = GadgetItem(gadgList, item);
	SetGadgetText(gadget, window, request, text);
}

/*
 *	TrackGadget
 *	Get messages from message port and check for gadget being selected
 *	Continuously call actionProc(window, gadgetNum) while gadget is selected
 *	Will call actionProc at least once
 *	Return when mouse button is up
 */

void TrackGadget(MsgPortPtr msgPort, register WindowPtr window,
				 register GadgetPtr gadget, void (*actionProc)(WindowPtr, WORD))
{
	register WORD gadgetNum;
	register IntuiMsgPtr intuiMsg;
	register ULONG class;
	register UWORD qualifier;
	BOOL actionDone;
	WindowPtr msgWindow;

	gadgetNum = GadgetNumber(gadget);
	actionDone = FALSE;
/*
	Get and process messages
	Should only get GADGETUP and possible MOUSEMOVE or INTUITICKS messages,
		anything else is invalid
*/
	for (;;) {
		while (intuiMsg = (IntuiMsgPtr) GetMsg(msgPort)) {
			class     = intuiMsg->Class;
			qualifier = (class != IDCMP_INTUITICKS) ? intuiMsg->Qualifier :
												IEQUALIFIER_LEFTBUTTON;
			msgWindow = intuiMsg->IDCMPWindow;
/*
	If message is not for us, then put it back and return
*/
			if (msgWindow != window ||
				(class != IDCMP_MOUSEMOVE && class != IDCMP_INTUITICKS) ||
				!SELECTBUTTON(qualifier)) {
				PutMsg(msgPort, (MsgPtr) intuiMsg);
				if (actionProc && !actionDone)
					(*actionProc)(window, gadgetNum);
				return;
			}
			ReplyMsg((MsgPtr) intuiMsg);
/*
	If INTUITICKS message and actionProc, then call actionProc
*/
			if (class == IDCMP_INTUITICKS && actionProc &&
				(gadget->Flags & GFLG_SELECTED)) {
				(*actionProc)(window, gadgetNum);
				actionDone = TRUE;
			}
		}
/*
	If no messages pending, then call actionProc or wait for message
	(Only call actionProc here if INTUITICKS is not enabled)
*/
		if ((window->IDCMPFlags & IDCMP_INTUITICKS) == 0 && actionProc) {
			if (gadget->Flags & GFLG_SELECTED) {
				(*actionProc)(window, gadgetNum);
				actionDone = TRUE;
			}
		}
		else
			Wait(1 << msgPort->mp_SigBit);
	}
}
