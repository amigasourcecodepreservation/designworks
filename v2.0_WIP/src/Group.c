/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Group handling routines
 */

#include <exec/types.h>
#include <graphics/gfxmacros.h>
#include <intuition/intuition.h>

#include <proto/graphics.h>

#include <Toolbox/Memory.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Utility.h>
#include <Toolbox/Window.h>

#include "Draw.h"
#include "Proto.h"

/*
 *	External variables
 */

/*
 *	Local variables and definitions
 */

#define LINE_PAT	0xFFFF		// Line pattern for object creation

/*
 *	Local prototypes
 */

static void	SetGroupFrame(GroupObjPtr);

/*
 *	Allocate a new group object
 */

GroupObjPtr GroupAllocate()
{
	return ((GroupObjPtr) MemAlloc(sizeof(GroupObj), MEMF_CLEAR));
}

/*
 *	Dispose of group object
 */

void GroupDispose(GroupObjPtr groupObj)
{
	DisposeAllDocObjects(groupObj->Objects);
	MemFree(groupObj, sizeof(GroupObj));
}

/*
 *	Draw group object
 */

void GroupDrawObj(RastPtr rPort, GroupObjPtr groupObj, TransformProc xFormPt, RectPtr clipRect)
{
	DocObjPtr	docObj;
	Rectangle	oldFrame;

	for (docObj = groupObj->Objects; docObj; docObj = NextObj(docObj)) {
		oldFrame = docObj->Frame;
		OffsetRect(&docObj->Frame, groupObj->DocObj.Frame.MinX, groupObj->DocObj.Frame.MinY);
		DrawObject(rPort, docObj, xFormPt, clipRect);
		docObj->Frame = oldFrame;
	}
}

/*
 *	Draw outline of group (for dragging)
 */

void GroupDrawOutline(WindowPtr window, GroupObjPtr groupObj, WORD xOffset, WORD yOffset)
{
	RectDrawOutline(window, (RectObjPtr) groupObj, xOffset, yOffset);
}

/*
 *	Draw selection hilighting for group
 */

void GroupHilite(WindowPtr window, GroupObjPtr groupObj)
{
	RectHilite(window, (RectObjPtr) groupObj);
}

/*
 *	Set group pen color
 */

void GroupSetPenColor(GroupObjPtr groupObj, RGBColor penColor)
{
	DocObjPtr	docObj;

	for (docObj = groupObj->Objects; docObj; docObj = NextObj(docObj))
		SetObjectPenColor(docObj, penColor);
}

/*
 *	Set group pen size
 *	If size is -1 then do not change
 */

void GroupSetPenSize(GroupObjPtr groupObj, WORD penWidth, WORD penHeight)
{
	DocObjPtr	docObj;

	for (docObj = groupObj->Objects; docObj; docObj = NextObj(docObj))
		SetObjectPenSize(docObj, penWidth, penHeight);
}

/*
 *	Set group fill pattern
 */

void GroupSetFillPat(GroupObjPtr groupObj, RGBPat8Ptr fillPat)
{
	DocObjPtr	docObj;

	for (docObj = groupObj->Objects; docObj; docObj = NextObj(docObj))
		SetObjectFillPat(docObj, fillPat);
}

/*
 *	Get group fill pattern
 */

BOOL GroupGetFillPat(GroupObjPtr groupObj, RGBPat8Ptr fillPat)
{
	return (FALSE);
}

/*
 *	Set text parameters for group text
 */

void GroupSetTextParams(GroupObjPtr groupObj, FontNum fontNum, FontSize fontSize,
						WORD style, WORD miscStyle, WORD justify, WORD spacing)
{
	DocObjPtr	docObj;

	for (docObj = groupObj->Objects; docObj; docObj = NextObj(docObj))
		SetObjectTextParams(docObj, fontNum, fontSize, style, miscStyle, justify, spacing);
	GroupAdjustFrame(groupObj);
}

/*
 *	Set group frame from object frames, and offset object frames
 */

static void SetGroupFrame(GroupObjPtr groupObj)
{
	DocObjPtr	docObj;
	Rectangle	newRect, rect;

	for (docObj = groupObj->Objects; docObj; docObj = NextObj(docObj)) {
		newRect = docObj->Frame;
		if (docObj == groupObj->Objects)
			rect = newRect;
		else
			UnionRect(&newRect, &rect, &rect);
	}
	groupObj->DocObj.Frame = rect;
	for (docObj = groupObj->Objects; docObj; docObj = NextObj(docObj))
		OffsetRect(&docObj->Frame, -rect.MinX, -rect.MinY);
}

/*
 *	Rotate group by given angle about center
 */

void GroupRotate(GroupObjPtr groupObj, WORD cx, WORD cy, WORD angle)
{
	DocObjPtr	docObj;

	for (docObj = groupObj->Objects; docObj; docObj = NextObj(docObj)) {
		OffsetRect(&docObj->Frame, groupObj->DocObj.Frame.MinX, groupObj->DocObj.Frame.MinY);
		RotateObject(docObj, cx, cy, angle);
	}
	SetGroupFrame(groupObj);
}

/*
 *	Flip group horizontally or vertically about center
 */

void GroupFlip(GroupObjPtr groupObj, WORD cx, WORD cy, BOOL horiz)
{
	DocObjPtr	docObj;

	for (docObj = groupObj->Objects; docObj; docObj = NextObj(docObj)) {
		OffsetRect(&docObj->Frame, groupObj->DocObj.Frame.MinX, groupObj->DocObj.Frame.MinY);
		FlipObject(docObj, cx, cy, horiz);
	}
	SetGroupFrame(groupObj);
}

/*
 *	Scale group to new frame
 *	Scale group objects proportionally
 */

void GroupScale(GroupObjPtr groupObj, RectPtr frame)
{
	DocObjPtr	docObj;
	Rectangle	rect;

	for (docObj = groupObj->Objects; docObj; docObj = NextObj(docObj)) {
		OffsetRect(&docObj->Frame, groupObj->DocObj.Frame.MinX, groupObj->DocObj.Frame.MinY);
		rect = docObj->Frame;
		MapRect(&rect, &groupObj->DocObj.Frame, frame);
		ScaleObject(docObj, &rect);
		OffsetRect(&docObj->Frame, -frame->MinX, -frame->MinY);
	}
	groupObj->DocObj.Frame = *frame;
	GroupAdjustFrame(groupObj);
}

/*
 *	Determine if point is in group
 *	Point is relative to object rectangle
 */

BOOL GroupSelect(GroupObjPtr groupObj, Point *pt)
{
	DocObjPtr	docObj;

	for (docObj = groupObj->Objects; docObj; docObj = NextObj(docObj)) {
		if (PointInObject(docObj, pt))
			return (TRUE);
	}
	return (FALSE);
}

/*
 *	Return handle number of given point, or -1 if not in a handle
 *	Point is relative to object rectangle
 */

WORD GroupHandle(GroupObjPtr groupObj, PointPtr pt, RectPtr handleRect)
{
	return (RectHandle((RectObjPtr) groupObj, pt, handleRect));
}

/*
 *	Duplicate group data to new object
 *	Return success status
 */

BOOL GroupDupData(GroupObjPtr groupObj, GroupObjPtr newObj)
{
	DocObjPtr	docObj, newSubObj;

	for (docObj = groupObj->Objects; docObj; docObj = NextObj(docObj)) {
		if ((newSubObj = DuplicateObject(NULL, docObj)) == NULL)
			return (FALSE);
		AppendToGroup(newObj, newSubObj);
	}
	return (TRUE);
}

/*
 *	Adjust group frame
 */

void GroupAdjustFrame(GroupObjPtr groupObj)
{
	DocObjPtr	docObj;

	for (docObj = groupObj->Objects; docObj; docObj = NextObj(docObj))
		OffsetRect(&docObj->Frame, groupObj->DocObj.Frame.MinX, groupObj->DocObj.Frame.MinY);
	SetGroupFrame(groupObj);
}

/*
 *	Make a group out of currently selected objects
 *	Group is linked in at location of front-most object in group
 */

GroupObjPtr MakeGroup(DocDataPtr docData)
{
	DocObjPtr	docObj, nextObj, linkObj;
	GroupObjPtr	groupObj;

/*
	Make sure there are at least two objects selected
*/
	if ((docObj = FirstSelected(docData)) == NULL || NextSelected(docObj) == NULL)
		return (NULL);
/*
	Allocate group object
*/
	if ((groupObj = (GroupObjPtr) NewDocObject(CurrLayer(docData), TYPE_GROUP)) == NULL)
		return (NULL);
/*
	First remove all selected objects from document and add them to group list
*/
	docObj = FirstObject(docData);
	while (docObj) {
		nextObj = NextObj(docObj);
		if (ObjectSelected(docObj)) {
			linkObj = PrevObj(docObj);
			DetachObject(CurrLayer(docData), docObj);
			AppendToGroup(groupObj, docObj);
		}
		docObj = nextObj;
	}
	SetGroupFrame(groupObj);
/*
	Now link group into document at position of last grouped object
*/
	for (docObj = groupObj->Objects; docObj; docObj = NextObj(docObj)) {
		UnSelectObject(docObj);
		docObj->Group = groupObj;
	}
	DetachObject(CurrLayer(docData), groupObj);
	InsertObject(CurrLayer(docData), linkObj, groupObj);
	SelectObject(groupObj);
	return (groupObj);
}

/*
 *	Un-make specified group
 */

void UnMakeGroup(DocDataPtr docData, GroupObjPtr groupObj)
{
	DocObjPtr	docObj;

/*
	Adjust object frames and re-link into document
*/
	for (docObj = groupObj->Objects; docObj; docObj = NextObj(docObj)) {
		OffsetObject(docObj, groupObj->DocObj.Frame.MinX, groupObj->DocObj.Frame.MinY);
		SelectObject(docObj);
	}
	InsertAllObjects(CurrLayer(docData), groupObj, groupObj->Objects);
	groupObj->Objects = NULL;
/*
	Dispose of group object
*/
	DetachObject(CurrLayer(docData), groupObj);
	DisposeDocObject(groupObj);
}

/*
 *	Track mouse and change group shape
 */

void GroupGrow(WindowPtr window, GroupObjPtr groupObj, UWORD modifier, WORD handle)
{
	Rectangle	rect, frame;

	TrackRectGrow(window, groupObj, modifier, handle, &rect);
/*
	Set new object dimensions
*/
	if (!EqualRect(&groupObj->DocObj.Frame, &rect)) {
		SetStdPointer(window, POINTER_WAIT);	// Pointer will be reset by caller
		InvalObjectRect(window, groupObj);
		HiliteSelectOff(window);
		SetFrameRect(&frame, rect.MinX, rect.MinY, rect.MaxX, rect.MaxY);
		GroupScale(groupObj, &frame);
		HiliteSelectOn(window);
		InvalObjectRect(window, groupObj);
	}
}

/*
 *	Draw group object to PostScript file
 */

BOOL GroupDrawPSObj(PrintRecPtr printRec, GroupObjPtr groupObj, RectPtr rect, RectPtr clipRect)
{
	DocObjPtr docObj;
	Rectangle objRect;

	for (docObj = groupObj->Objects; docObj; docObj = NextObj(docObj)) {
		objRect = docObj->Frame;
		OffsetRect(&objRect, groupObj->DocObj.Frame.MinX, groupObj->DocObj.Frame.MinY);
		MapRect(&objRect, &groupObj->DocObj.Frame, rect);
		if (!DrawPSObject(printRec, docObj, &objRect, clipRect))
			return (FALSE);
	}
	return (TRUE);
}
