/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Toolbox
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	System beep
 */

#include <exec/types.h>
#include <devices/audio.h>

#include <Toolbox/Utility.h>

#include <proto/exec.h>

/*
 *	SysBeep
 *	Produce beep for specified ticks (1/50 of sec.)
 */

#define SOUND_FREQ	330		/* In Hertz */

static BYTE chip soundData[] = {
	0,  64,  127,  127,  127,  127,  127,  64,
	0, -64, -127, -127, -127, -127, -127, -64
};

#define LEFTCHAN_MASK	0x09
#define RIGHTCHAN_MASK	0x06

static UBYTE audChanMasks[] = { 0x03, 0x05, 0x0A, 0x0C };

void SysBeep(UWORD beepTime)
{
	WORD channels;
	MsgPortPtr replyPort;
	struct IOAudio ioa1, ioa2;

	ioa1.ioa_Request.io_Message.mn_Node.ln_Pri = 80;
	ioa1.ioa_Data = audChanMasks;
	ioa1.ioa_Length = sizeof(audChanMasks);
	if ((replyPort = CreatePort(NULL, 0)) == NULL)
		return;
	ioa1.ioa_Request.io_Message.mn_ReplyPort = replyPort;
/*
	Open device, allocate left & right channels, and set up for sound
*/
	if (OpenDevice(AUDIONAME, 0, (IOReqPtr) &ioa1, 0) == 0) {
		ioa1.ioa_Request.io_Command = CMD_WRITE;
		ioa1.ioa_Request.io_Flags = ADIOF_PERVOL;
		ioa1.ioa_Data = soundData;
		ioa1.ioa_Length = 16;
		ioa1.ioa_Period = (3579545L/(16*SOUND_FREQ));
		ioa1.ioa_Volume = 64;
		ioa1.ioa_Cycles = (SOUND_FREQ*beepTime)/50;
/*
	Do both left and right channels
*/
		ioa2 = ioa1;
		channels = (WORD) ioa1.ioa_Request.io_Unit;
		ioa1.ioa_Request.io_Unit = (struct Unit *) (channels & LEFTCHAN_MASK);
		ioa2.ioa_Request.io_Unit = (struct Unit *) (channels & RIGHTCHAN_MASK);
		BeginIO((IOReqPtr) &ioa1);
		BeginIO((IOReqPtr) &ioa2);
		WaitPort(replyPort);
		(void) GetMsg(replyPort);
		WaitPort(replyPort);
		(void) GetMsg(replyPort);
/*
	Free channels and close device
*/
		ioa1.ioa_Request.io_Unit = (struct Unit *) channels;
		CloseDevice((IOReqPtr) &ioa1);
	}
	DeletePort(replyPort);
}
