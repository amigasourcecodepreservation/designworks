/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Screen Title
 */

#ifdef DEMO
char strScreenTitle[] = " DesignWorks DEMO 1.0 - \251 1991 New Horizons Software, Inc.";
#else
char strScreenTitle[] = " DesignWorks 1.0 - \251 1991 New Horizons Software, Inc.";
#endif

/*
 *	Version string for AmigaDOS 2.0
 */

static char	version[] = "$VER: DesignWorks 1.0";
