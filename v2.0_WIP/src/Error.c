/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Error/Help report routines
 */

#include <exec/types.h>
#include <exec/memory.h>
#include <intuition/intuition.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>

#include <string.h>

#include <Toolbox/Image.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Utility.h>

#include "Draw.h"
#include "Proto.h"

/*
 *	External variables
 */

extern struct RexxLib	*RexxSysBase;

extern MsgPortPtr	mainMsgPort;
extern MsgPort		rexxMsgPort;
extern ScreenPtr	screen;
extern WindowPtr	backWindow, windowList[];
extern WORD			numWindows;

extern BOOL		titleChanged;
extern TextChar	strScreenTitle[];

extern TextPtr	strsErrors[];
extern TextChar	strDemo[], strBadFile[], strMemory[];
extern TextChar	strDOSError[], strFileNotFound[], strDiskLocked[];
extern TextChar	strFileNoDelete[], strFileNoRead[];

extern DlgTemplPtr	dlgList[];

/*
 *	Local variables
 */

#define ERROR_TEXT		1
#define ERROR_DOSNUM	2
#define ERROR_ICON		3

#define ABOUT_CHIPMEM	1
#define ABOUT_FASTMEM	2
#define ABOUT_AREXX		3

/*
 *	Return screen titles to default setting
 */

void FixTitle()
{
	register WORD num;

	SetWindowTitles(backWindow, (TextPtr) -1, strScreenTitle);
	for (num = 0; num < numWindows; num++)
		SetWindowTitles(windowList[num], (TextPtr) -1, strScreenTitle);
	titleChanged = FALSE;
}

/*
 *	Display error message dialog
 *	If no memory for dialog, display error in title bar
 */

void Error(WORD errNum)
{
	WORD num;

	if (errNum > ERR_MAX_ERROR)
		errNum = ERR_UNKNOWN;
	dlgList[DLG_ERROR]->Gadgets[ERROR_TEXT].Info = strsErrors[errNum];
/*
	Can't use StdDialog since this has different low memory handling
	(and it also calls this routine)
*/
	BeginWait();
	if (StdAlert(dlgList[DLG_ERROR], screen, mainMsgPort, DialogFilter) == -1) {
		SetWindowTitles(backWindow, (TextPtr) -1, strsErrors[errNum]);
		for (num = 0; num < numWindows; num++)
			SetWindowTitles(windowList[num], (TextPtr) -1, strsErrors[errNum]);
		ErrBeep();
		titleChanged = TRUE;
	}
	EndWait();
}

/*
 *	Display DOS error message dialog
 *	If dosError is 0, then cause was not enough memory
 */

void DOSError(WORD errNum, LONG dosError)
{
	TextPtr errText;

	switch (dosError) {
	case -2:
		errText = strDemo;
		break;
	case -1:
		errText = strBadFile;
		break;
	case 0:
	case ERROR_NO_FREE_STORE:
		errText = strsErrors[ERR_NO_MEM];
		break;
	case ERROR_OBJECT_NOT_FOUND:
	case ERROR_DIR_NOT_FOUND:
	case ERROR_DEVICE_NOT_MOUNTED:
	case ERROR_OBJECT_WRONG_TYPE:
		errText = strFileNotFound;
		break;
	case ERROR_DELETE_PROTECTED:
		errText = strFileNoDelete;
		break;
	case ERROR_DISK_WRITE_PROTECTED:
		errText = strDiskLocked;
		break;
	case ERROR_READ_PROTECTED:
		errText = strFileNoRead;
		break;
	default:
		if (dosError >= 0 && dosError <= 999) {
			NumToString(dosError, strDOSError + strlen(strDOSError) - 3);
			errText = strDOSError;
		}
		else
			errText = strsErrors[ERR_UNKNOWN];
		break;
	}
	dlgList[DLG_ERROR]->Gadgets[ERROR_DOSNUM].Info = errText;
	Error(errNum);
	dlgList[DLG_ERROR]->Gadgets[ERROR_DOSNUM].Info = NULL;
}

/*
 *	Show information requester with number parameter
 *	Replace first '#' character with actual number value
 */

void InfoDialog(TextPtr text, WORD num)
{
	register WORD i;
	TextChar buff[50];

	for (i = 0; *text && *text != '#'; i++)
		buff[i] = *text++;
	if (*text == '#') {
		NumToString(num, buff + i);
		strcat(buff, ++text);
	}
	else
		buff[i] = '\0';
	dlgList[DLG_ERROR]->Gadgets[ERROR_TEXT].Info = buff;
	dlgList[DLG_ERROR]->Gadgets[ERROR_ICON].Info = (Ptr) IMAGE_ICON_NOTE;
	(void) StdDialog(DLG_ERROR);
	dlgList[DLG_ERROR]->Gadgets[ERROR_ICON].Info = (Ptr) IMAGE_ICON_STOP;
}

/*
 *	Display help dialog
 *	If cannot show full help requester, just show memory info
 */

void DoHelp()
{
	DialogPtr dlg;
	TextChar chipMem[10], fastMem[10];

	NumToString(AvailMem(MEMF_CHIP)/1024, chipMem);
	NumToString(AvailMem(MEMF_FAST)/1024, fastMem);
	strcat(chipMem, "K");
	strcat(fastMem, "K");
	dlgList[DLG_ABOUT]->Gadgets[ABOUT_CHIPMEM].Info =
		dlgList[DLG_MEMORY]->Gadgets[ABOUT_CHIPMEM].Info = chipMem;
	dlgList[DLG_ABOUT]->Gadgets[ABOUT_FASTMEM].Info =
		dlgList[DLG_MEMORY]->Gadgets[ABOUT_FASTMEM].Info = fastMem;
	dlgList[DLG_ABOUT]->Gadgets[ABOUT_AREXX].Info =
		(RexxSysBase) ? rexxMsgPort.mp_Node.ln_Name : "----";
	BeginWait();
	if ((dlg = GetDialog(dlgList[DLG_ABOUT], screen, mainMsgPort)) == NULL &&
		(dlg = GetDialog(dlgList[DLG_MEMORY], screen, mainMsgPort)) == NULL)
		Error(ERR_NO_MEM);
	else {
		OutlineOKButton(dlg);
		(void) ModalDialog(mainMsgPort, dlg, DialogFilter);
		DisposeDialog(dlg);
	}
	EndWait();
}

/*
 *	Check remaining memory and give warning if getting low
 */

void CheckMemory()
{
	register WORD num;

	if (AvailMem(0) < 0x3FFFL) {
		SetWindowTitles(backWindow, (TextPtr) -1, strMemory);
		for (num = 0; num < numWindows; num++)
			SetWindowTitles(windowList[num], (TextPtr) -1, strMemory);
		ErrBeep();
		titleChanged = TRUE;
	}
}
