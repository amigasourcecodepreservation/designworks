#ifndef TYPEDEF_H
#define TYPEDEF_H

#ifndef DOS_DOS_H
#include <dos/dos.h>
#endif /* DOS_DOS_H */

#ifndef GRAPHICS_GFX_H
#include <graphics/gfx.h>
#endif /* GRAPHICS_GFX_H */


typedef struct Task * TaskPtr ;


typedef void * Ptr;
typedef struct RastPort * RastPtr;
typedef struct RastPort RastPort;
typedef struct Gadget *GadgetPtr;
typedef struct Gadget Gadget;
typedef struct Window Window;
typedef struct Window *WindowPtr;
typedef struct Requester *RequestPtr;
typedef TEXT TextChar;
typedef struct MsgPort *MsgPortPtr;
typedef struct MsgPort MsgPort;
typedef TEXT *TextPtr;
typedef struct Rectangle *RectPtr;
typedef struct Screen Screen;
typedef struct Screen *ScreenPtr;
typedef struct IntuiMessage *IntuiMsgPtr;
typedef BPTR Dir;
typedef BPTR File;
typedef struct ColorMap *ColorMapPtr;
typedef struct TextAttr *TextAttrPtr;
typedef struct TextFont *TextFontPtr;
typedef struct TextAttr TextAttr;
typedef struct Layer *LayerPtr;
typedef struct Region *RegionPtr;
typedef struct Rectangle Rectangle;
typedef Point *PointPtr;
/*typedef struct Point Point;*/
typedef struct BitMap *BitMapPtr;
typedef struct BitMap BitMap;
typedef struct Border *BorderPtr;
typedef struct Border Border;
typedef struct Image *ImagePtr;
typedef struct Image Image;
typedef struct PropInfo PropInfo;
typedef struct PropInfo *PropInfoPtr;
typedef struct Message *MsgPtr;
typedef struct Process *ProcessPtr;
typedef struct IntuiText *IntuiTextPtr;
typedef struct IntuiText IntuiText;
typedef struct StringInfo StringInfo;
typedef struct StringInfo *StrInfoPtr;
typedef struct Layer_Info *LayerInfoPtr;
typedef struct Menu Menu;
typedef struct Menu *MenuPtr;
typedef struct MenuItem MenuItem;
typedef struct MenuItem *MenuItemPtr;
typedef struct IORequest *IOReqPtr;
typedef struct Node *NodePtr;
#endif /* TYPEDEF_H */

