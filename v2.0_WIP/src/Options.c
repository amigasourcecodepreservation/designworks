/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Preferences and screen colors dialog
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>

#include <Toolbox/Globals.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Border.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/ColorPick.h>
#include <Toolbox/Window.h>
#include <Toolbox/Screen.h>
#include <Toolbox/LocalData.h>
#include <Toolbox/Utility.h>

#include "Draw.h"
#include "Proto.h"

/*
 *	External variables
 */

extern ScreenPtr	screen;
extern MsgPortPtr	mainMsgPort;

extern WORD			numWindows;
extern WindowPtr	windowList[];

extern Options	options;

extern Palette	screenPalette;

extern TextChar	strBuff[];

extern DlgTemplPtr	dlgList[];

extern GadgetTemplate optionsGeneralGadgets[], optionsDisplayGadgets[],
						optionsSaveGadgets[], optionsPolyGadgets[],
						optionsClipboardGadgets[], optionsHotLinksGadgets[];

/*
 *	Local variables and definitions
 */

enum {
	GENERAL_PREFS,
	DISPLAY_PREFS,
	SAVE_PREFS,
	POLY_PREFS,
	CLIPBOARD_PREFS,
	HOTLINKS_PREFS
};

#define PREFRESET_BTN	2
#define PREFMODE_POPUP	3

enum {					// general prefs
	INCH_RADBTN = 4,
	CM_RADBTN,
	FLASH_RADBTN,
	SOUND_RADBTN,
	BOTH_RADBTN
};

enum {					// display prefs
	DRAGOUTLINES_BOX = 4,
	SHOWPICTS_BOX,
	COLORCORRECT_BOX,
	CACHEPICTS_BOX
};

enum {					// save prefs
	SAVEICONS_BOX = 4,
	MAKEBACKUPS_BOX,
	AUTOSAVE_BOX,
	AUTOSAVE_TEXT
};

enum {					// polygon prefs
	POLYCLOSE_BOX = 4
};

enum {					// clipboard prefs
	FULLCLIP_BOX = 4,
	CLIPDEPTH_POPUP
};

#define HLDEPTH_POPUP 4	// hotlinks prefs

static DialogPtr optionsDlg;

static WORD currentPrefs;
static WORD	prevPrefs;
static GadgetPtr statGadgList;

/*
 *	Local prototypes
 */
static void SetGeneralOptions(Options *);
static void SetDisplayOptions(Options *);
static void SetSaveOptions(Options *);
static void SetPolyOptions(Options *);
static void SetClipboardOptions(Options *);
static void SetHotLinksOptions(Options *);

static void	SetOptionsButtons(DialogPtr, Options *);
static void SetAutoSaveText(DialogPtr, UBYTE);
static WORD SetDepthOptions(GadgetPtr);

static void DoGeneralPreferences(Options *, WORD);
static void DoDisplayPreferences(Options *, WORD);
static void DoSavePreferences(Options *, WORD);
static void DoPolyPreferences(Options *, WORD);
static void DoClipboardPreferences(Options *, WORD);
static void DoHotLinksPreferences(Options *, WORD);

static BOOL ChangeGadgets(Options *, WORD *);
static BOOL OptionsDialogFilter(IntuiMsgPtr, WORD *);

/*
 *	set general options
 */

static void SetGeneralOptions(Options *opts)
{
	opts->MeasureUnit	= MEASURE_DEFAULT;
	opts->BlinkPeriod	= BLINK_FAST;
	opts->BeepFlash		= TRUE;
	opts->BeepSound		= TRUE;
}

/*
 *	set display options
 */

static void SetDisplayOptions(Options *opts)
{
	opts->DragOutlines	= TRUE;
	opts->ShowPictures	= TRUE;
	opts->ColorCorrect	= TRUE;
	opts->PrtCyan		= PRT_RGBCOLOR_CYAN;
	opts->PrtMagenta	= PRT_RGBCOLOR_MAGENTA;
	opts->PrtYellow		= PRT_RGBCOLOR_YELLOW;
	opts->CachePictures	= TRUE;
}

/*
 *	set save options
 */

static void SetSaveOptions(Options *opts)
{
	opts->SaveIcons		= TRUE;
	opts->MakeBackups	= FALSE;
	opts->AutoSave		= FALSE;
	opts->AutoSaveTime	= 30;
}

/*
 *	set polygon options
 */

static void SetPolyOptions(Options *opts)
{
	opts->PolyAutoClose	= FALSE;
}

/*
 *	set clipboard options
 */

static void SetClipboardOptions(Options *opts)
{
	opts->FullClipboard = TRUE;
	opts->ClipboardDepth = 5;
}

/*
 *	set hotlinks options
 */

static void SetHotLinksOptions(Options *opts)
{
	opts->HotLinksDepth = 5;
}

/*
 *	Set standard options
 */

void SetStdOptions(Options *opts)
{
	SetGeneralOptions(opts);
	SetDisplayOptions(opts);
	SetSaveOptions(opts);
	SetPolyOptions(opts);
	SetClipboardOptions(opts);
	SetHotLinksOptions(opts);
}

/*
 *	Set options dialog buttons
 */

static void SetOptionsButtons(DialogPtr dlg, Options *opts)
{
	GadgetPtr gadgList = dlg->FirstGadget;

	switch (currentPrefs) {
		case GENERAL_PREFS:
			SetGadgetItemValue(gadgList, INCH_RADBTN, dlg, NULL, (opts->MeasureUnit == MEASURE_INCH));
			SetGadgetItemValue(gadgList, CM_RADBTN, dlg, NULL, (opts->MeasureUnit == MEASURE_CM));
			SetGadgetItemValue(gadgList, FLASH_RADBTN, dlg, NULL, opts->BeepFlash && !opts->BeepSound);
			SetGadgetItemValue(gadgList, SOUND_RADBTN, dlg, NULL, !opts->BeepFlash && opts->BeepSound);
			SetGadgetItemValue(gadgList, BOTH_RADBTN, dlg, NULL, opts->BeepFlash && opts->BeepSound);
			break;
		case DISPLAY_PREFS:
			SetGadgetItemValue(gadgList, DRAGOUTLINES_BOX, dlg, NULL, opts->DragOutlines);
			SetGadgetItemValue(gadgList, SHOWPICTS_BOX, dlg, NULL, opts->ShowPictures);
			SetGadgetItemValue(gadgList, COLORCORRECT_BOX, dlg, NULL, opts->ColorCorrect);
			SetGadgetItemValue(gadgList, CACHEPICTS_BOX, dlg, NULL, opts->CachePictures);
			break;
		case SAVE_PREFS:
			SetGadgetItemValue(gadgList, SAVEICONS_BOX, dlg, NULL, opts->SaveIcons);
			SetGadgetItemValue(gadgList, MAKEBACKUPS_BOX, dlg, NULL, opts->MakeBackups);
			SetGadgetItemValue(gadgList, AUTOSAVE_BOX, dlg, NULL, opts->AutoSave);
			break;
		case POLY_PREFS:
			SetGadgetItemValue(gadgList, POLYCLOSE_BOX, dlg, NULL, opts->PolyAutoClose);
			break;
		case CLIPBOARD_PREFS:
			SetGadgetItemValue(gadgList, FULLCLIP_BOX, dlg, NULL, opts->FullClipboard);
			break;
	}
}

/*
 *	Set auto-save text
 */

static void SetAutoSaveText(DialogPtr dlg, UBYTE autoSaveTime)
{
	NumToString(autoSaveTime, strBuff);
	SetEditItemText(dlg->FirstGadget, AUTOSAVE_TEXT, dlg, NULL, strBuff);
}

/*
 *	set depth based on number of colors selected
 */

static WORD SetDepthOptions(GadgetPtr gadget)
{
	WORD	value;

	value = GetGadgetValue(gadget);
	if (value == COLOR32_ITEM)
		return (5);
	else if (value == COLOR64_ITEM)
		return (6);
	else
		return (8);
}

/*
 *	adjust other preferences
 */

static void DoGeneralPreferences(Options *opts, WORD item)
{
	switch (item) {
		case PREFRESET_BTN:
			SetGeneralOptions(opts);
			break;
		case INCH_RADBTN:
			opts->MeasureUnit = MEASURE_INCH;
			break;
		case CM_RADBTN:
			opts->MeasureUnit = MEASURE_CM;
			break;
		case FLASH_RADBTN:
			opts->BeepFlash = TRUE;
			opts->BeepSound = FALSE;
			break;
		case SOUND_RADBTN:
			opts->BeepFlash = FALSE;
			opts->BeepSound = TRUE;
			break;
		case BOTH_RADBTN:
			opts->BeepFlash = opts->BeepSound = TRUE;
			break;
	}
}

/*
 *	adjust general preferences
 */

static void DoDisplayPreferences(Options *opts, WORD item)
{
	switch (item) {
		case PREFRESET_BTN:
			SetDisplayOptions(opts);
			break;
		case DRAGOUTLINES_BOX:
			opts->DragOutlines = !opts->DragOutlines;
			break;
		case SHOWPICTS_BOX:
			opts->ShowPictures = !opts->ShowPictures;
			break;
		case COLORCORRECT_BOX:
			opts->ColorCorrect = !opts->ColorCorrect;
			break;
		case CACHEPICTS_BOX:
			opts->CachePictures = !opts->CachePictures;
			break;
	}
}

/*
 *	adjust save preferences
 */

static void DoSavePreferences(Options *opts, WORD item)
{
	switch (item) {
		case PREFRESET_BTN:
			SetSaveOptions(opts);
			SetAutoSaveText(optionsDlg, opts->AutoSaveTime);
			break;
		case SAVEICONS_BOX:
			opts->SaveIcons = !opts->SaveIcons;
			break;
		case MAKEBACKUPS_BOX:
			opts->MakeBackups = !opts->MakeBackups;
			break;
		case AUTOSAVE_BOX:
			opts->AutoSave = !opts->AutoSave;
			if (opts->AutoSave)
				ActivateGadget(GadgetItem(optionsDlg->FirstGadget, AUTOSAVE_TEXT), optionsDlg, NULL);
			break;
		case AUTOSAVE_TEXT:
			opts->AutoSave = TRUE;
			break;
	}
}

/*
 *	adjust polygon preferences
 */

static void DoPolyPreferences(Options *opts, WORD item)
{
	switch (item) {
		case PREFRESET_BTN:
			SetPolyOptions(opts);
			break;
		case POLYCLOSE_BOX:
			opts->PolyAutoClose = !opts->PolyAutoClose;
			break;
	}
}

/*
 *	adjust clipboard preferences
 */

static void DoClipboardPreferences(Options *opts, WORD item)
{
	GadgetPtr	gadget;

	switch (item) {
		case PREFRESET_BTN:
			SetClipboardOptions(opts);
			gadget = GadgetItem(optionsDlg->FirstGadget, CLIPDEPTH_POPUP);
			SetGadgetValue(gadget,optionsDlg, NULL, COLOR32_ITEM);
			RefreshGList(gadget, optionsDlg, NULL, 1);
			break;
		case FULLCLIP_BOX:
			opts->FullClipboard = !opts->FullClipboard;
			break;
	}
}

/*
 *	adjust hotlinks preferences
 */

static void DoHotLinksPreferences(Options *opts, WORD item)
{
	GadgetPtr	gadget;

	if (item == PREFRESET_BTN) {
		SetHotLinksOptions(opts);
		gadget = GadgetItem(optionsDlg->FirstGadget, HLDEPTH_POPUP);
		SetGadgetValue(gadget,optionsDlg, NULL, COLOR32_ITEM);
		RefreshGList(gadget, optionsDlg, NULL, 1);
	}
}

/*
 *	change current gadgets to new gadgets
 */

static BOOL ChangeGadgets(Options *opts, WORD *prevPrefs)
{
	GadgetTemplate 	*gadgetTemplPtr;
	Rectangle		btnRect, popupRect, rect;
	LONG			autoSaveTime;

	if (*prevPrefs == SAVE_PREFS) {
		GetEditItemText(optionsDlg->FirstGadget, AUTOSAVE_TEXT, strBuff);
		autoSaveTime = StringToNum(strBuff);
		if (autoSaveTime < 0)
			autoSaveTime = 1;
		else if (autoSaveTime > 60)
			autoSaveTime = 60;
		opts->AutoSaveTime = autoSaveTime;
	}
	if (*prevPrefs == CLIPBOARD_PREFS)
		opts->ClipboardDepth = SetDepthOptions(GadgetItem(statGadgList, CLIPDEPTH_POPUP));
	if (*prevPrefs == HOTLINKS_PREFS)
		opts->HotLinksDepth = SetDepthOptions(GadgetItem(statGadgList, HLDEPTH_POPUP));

	switch (currentPrefs) {
		case GENERAL_PREFS :
			gadgetTemplPtr = &optionsGeneralGadgets[0];
			break;
		case DISPLAY_PREFS:
			gadgetTemplPtr = &optionsDisplayGadgets[0];
			break;
		case SAVE_PREFS:
			gadgetTemplPtr = &optionsSaveGadgets[0];
			break;
		case POLY_PREFS:
			gadgetTemplPtr = &optionsPolyGadgets[0];
			break;
		case CLIPBOARD_PREFS:
			if (opts->ClipboardDepth == 5)
				optionsClipboardGadgets[CLIPDEPTH_POPUP].Value = COLOR32_ITEM;
			else if (opts->ClipboardDepth == 6)
				optionsClipboardGadgets[CLIPDEPTH_POPUP].Value = COLOR64_ITEM;
			else
				optionsClipboardGadgets[CLIPDEPTH_POPUP].Value = COLOR256_ITEM;
			gadgetTemplPtr = &optionsClipboardGadgets[0];
			break;
		case HOTLINKS_PREFS:
			if (opts->HotLinksDepth == 5)
				optionsHotLinksGadgets[HLDEPTH_POPUP].Value = COLOR32_ITEM;
			else if (opts->HotLinksDepth == 6)
				optionsHotLinksGadgets[HLDEPTH_POPUP].Value = COLOR64_ITEM;
			else
				optionsHotLinksGadgets[HLDEPTH_POPUP].Value = COLOR256_ITEM;
			gadgetTemplPtr = &optionsHotLinksGadgets[0];
			break;
	}
/*
	redraw dialog
*/
	GetGadgetRect(GadgetItem(optionsDlg->FirstGadget, PREFRESET_BTN),
				optionsDlg, NULL, &btnRect);
	GetGadgetRect(GadgetItem(optionsDlg->FirstGadget, PREFMODE_POPUP),
				optionsDlg, NULL, &popupRect);
	RemoveGList(optionsDlg, optionsDlg->FirstGadget, -1);
	DisposeGadgets(statGadgList);
	statGadgList = GetGadgets(gadgetTemplPtr);
	if( statGadgList == NULL ) {
		return (FALSE);		/* Exit routine if cannot swap gadgets */
	} else {
		InsetRect(&btnRect, -1, -1);
		SetAPen(optionsDlg->RPort, _tbPenLight);
		GetWindowRect(optionsDlg, &rect);
		InsetRect(&rect, 2, 2);
		rect.MinY = popupRect.MaxY + 10;
		rect.MaxX = btnRect.MinX - 2;
		FillRect(optionsDlg->RPort, &rect);
		AddGList(optionsDlg, statGadgList, 0, -1, NULL);
		RefreshGList(GadgetItem(optionsDlg->FirstGadget, PREFMODE_POPUP + 1), optionsDlg, NULL, -1);
	}
	if (currentPrefs == SAVE_PREFS)
		SetAutoSaveText(optionsDlg, opts->AutoSaveTime);
	*prevPrefs = currentPrefs;
	SetGadgetItemValue(optionsDlg->FirstGadget, PREFMODE_POPUP, optionsDlg, NULL, currentPrefs);

	return (TRUE);
}

/*
 *	Preferences dialog filter
 */

static BOOL OptionsDialogFilter(register IntuiMsgPtr intuiMsg, WORD *item)
{
	WORD	gadgNum;

	if (intuiMsg->IDCMPWindow == optionsDlg && intuiMsg->Class == GADGETDOWN) {
		gadgNum = GadgetNumber((GadgetPtr) intuiMsg->IAddress);
		if (currentPrefs == SAVE_PREFS && gadgNum == AUTOSAVE_TEXT) {
			ReplyMsg((MsgPtr) intuiMsg);
			*item = AUTOSAVE_TEXT;
			return (TRUE);
		}
	}
	return (DialogFilter(intuiMsg, item));
}

/*
 *	Adjust user preferences
 */

BOOL DoPreferences()
{
	WORD 		i, item;
	BOOL 		newMeasure, newColorCorrect, newShowPicts;
	Rectangle 	rect;
	Options 	opts;

	prevPrefs = currentPrefs = GENERAL_PREFS;
	UndoOff();
	opts = options;
	if (!opts.BeepSound)
		opts.BeepFlash = TRUE;
/*
	Get dialog
*/
	BeginWait();
	if ((optionsDlg = GetDialog(dlgList[DLG_OPTIONS], screen, mainMsgPort)) == NULL) {
		EndWait();
		Error(ERR_NO_MEM);
		return (FALSE);
	}
	statGadgList = GadgetItem(optionsDlg->FirstGadget, 0);
	OutlineOKButton(optionsDlg);
	SetOptionsButtons(optionsDlg, &opts);
/*
	Handle dialog
*/
	for (;;) {
		item = ModalDialog(mainMsgPort, optionsDlg, OptionsDialogFilter);
		if (item == OK_BUTTON || item == CANCEL_BUTTON)
			break;
		if (item == PREFMODE_POPUP)
			currentPrefs = GetGadgetValue(GadgetItem(optionsDlg->FirstGadget, PREFMODE_POPUP));
		if (currentPrefs != prevPrefs && !ChangeGadgets(&opts, &prevPrefs)) {
			item = OK_BUTTON;
			break;
		}
		switch (currentPrefs) {
			case GENERAL_PREFS :
				DoGeneralPreferences(&opts, item);
				break;
			case DISPLAY_PREFS:
				DoDisplayPreferences(&opts, item);
				break;
			case SAVE_PREFS:
				DoSavePreferences(&opts, item);
				break;
			case POLY_PREFS:
				DoPolyPreferences(&opts, item);
				break;
			case CLIPBOARD_PREFS:
				DoClipboardPreferences(&opts, item);
				break;
			case HOTLINKS_PREFS:
				DoHotLinksPreferences(&opts, item);
				break;
		}
		SetOptionsButtons(optionsDlg, &opts);
	}
	DisposeDialog(optionsDlg);
	EndWait();
	if (item == CANCEL_BUTTON)
		return (FALSE);
/*
	Set new values
*/
	newMeasure		= (opts.MeasureUnit != options.MeasureUnit);
	newColorCorrect	= (opts.ColorCorrect != options.ColorCorrect);
	newShowPicts	= (opts.ShowPictures != options.ShowPictures);
	options = opts;
	ColorCorrectEnable(&screenPalette, options.ColorCorrect);
	if (newMeasure || newColorCorrect || newShowPicts) {
		for (i = 0; i < numWindows; i++) {
			GetWindowRect(windowList[i], &rect);
			InvalRect(windowList[i], &rect);
		}
	}
	if (newColorCorrect) {
		DrawToolWindow();
		DrawPenWindow();
		DrawFillWindow();
	}
	if (newColorCorrect || !options.CachePictures) {
		for (i = 0; i < numWindows; i++)
			UnCacheAllPicts(GetWRefCon(windowList[i]));
	}
	return (TRUE);
}

/*
 *	Adjust screen colors
 */

BOOL DoScreenColors()
{
	BOOL	success;

	BeginWait();
	success = AdjustScreenColors(screen, mainMsgPort, DialogFilter);
	EndWait();
	return (success);
}
