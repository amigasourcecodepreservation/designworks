/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Font routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>
#include <libraries/diskfont.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>
#include <proto/diskfont.h>

#include <string.h>

#include <Toolbox/Request.h>
#include <Toolbox/Utility.h>
#include <Toolbox/Memory.h>
#include <Toolbox/List.h>

#include "Draw.h"
#include "Proto.h"

/*
 *	Global variables
 */

extern TextFontPtr	screenFont, smallFont;

extern Defaults	defaults;

/*
 *	Local variables and definitions
 */

typedef struct AvailFonts		*AvailFontsPtr;
typedef struct AvailFontsHeader	*AFHeaderPtr;

static ListHeadPtr	fontNameList;	/* List of unique font names */
static WORD			numFontNames;

static TextAttr	*fontTable;			/* Table of TextAttr's for existing fonts */
static WORD		numAttrs;

typedef struct FontEntry {
	struct FontEntry	*Next;
	FontNum				OldFontNum, NewFontNum;
} FontEntry, *FontEntryPtr;

static FontEntry	*fontTranslate;		/* Pointer to first entry in table */
										/* Inited to NULL */
/*
 *	Local prototypes
 */

BOOL ValidFont(AvailFontsPtr);

/*
 *	Add font entry to font table
 *	Do not add if entry for given oldFontNum already exists
 *	Return FALSE if not enough memory
 */

BOOL AddFontEntry(FontNum oldFontNum, FontNum newFontNum)
{
	register FontEntryPtr newEntry, prevEntry;

	prevEntry = NULL;
	for (newEntry = fontTranslate; newEntry; newEntry = newEntry->Next) {
		if (newEntry->OldFontNum == oldFontNum)
			return (TRUE);
		prevEntry = newEntry;
	}
	if ((newEntry = MemAlloc(sizeof(FontEntry), 0)) == NULL)
		return (FALSE);
	newEntry->Next = NULL;
	newEntry->OldFontNum = oldFontNum;
	newEntry->NewFontNum = newFontNum;
	if (prevEntry)
		prevEntry->Next = newEntry;
	else
		fontTranslate = newEntry;
	return (TRUE);
}

/*
 *	Get new font number from old font number
 *	Return FALSE if entry not found (and set to default font)
 */

BOOL GetFontEntry(FontNum oldFontNum, FontNum *newFontNum)
{
	register FontEntryPtr fontEntry;

	for (fontEntry = fontTranslate; fontEntry; fontEntry = fontEntry->Next) {
		if (fontEntry->OldFontNum == oldFontNum) {
			*newFontNum = fontEntry->NewFontNum;
			return (TRUE);
		}
	}
	*newFontNum = defaults.FontNum;
	return (FALSE);
}

/*
 *	Get font number of given table entry number
 *	Return FALSE if no such item
 */

BOOL FontTableItem(register UWORD itemNum, FontNum *oldFontNum)
{
	UWORD i;
	register FontEntryPtr fontEntry;

	fontEntry = fontTranslate;
	i = itemNum;
	while (itemNum--) {
		if (fontEntry == NULL)
			return (FALSE);
		fontEntry = fontEntry->Next;
	}
	*oldFontNum = fontEntry->OldFontNum;
	return (TRUE);
}

/*
 *	Dispose of all table entries
 */

void DisposeFontTable()
{
	register FontEntryPtr fontEntry, nextEntry;

	for (fontEntry = fontTranslate; fontEntry; fontEntry = nextEntry) {
		nextEntry = fontEntry->Next;
		MemFree(fontEntry, sizeof(FontEntry));
	}
	fontTranslate = NULL;
}

/*
 *	Return number of font names
 */

FontNum NumFonts()
{
	return (numFontNames);
}

/*
 *	Find and return the font number for the given font
 *	fontName may or may not have trailing ".font"
 *	If font not found then return default font num
 */

FontNum FontNumber(TextPtr fontName)
{
	FontNum fontNum;
	WORD len;
	TextChar name[50];

	strcpy(name, fontName);
	len = strlen(name);
	if (len < 5 || CmpString(name + len - 5, ".font", 5, 5, FALSE) != 0)
		strcat(name, ".font");
	fontNum = FindListItem(fontNameList, name);
	if (fontNum == -1)
		fontNum = defaults.FontNum;
	return (fontNum);
}

/*
 *	Get name of specified font, without trailing ".font"
 */

void GetFontName(FontNum fontNum, TextPtr fontName)
{
	TextPtr name;

	if (fontNum >= numFontNames)
		*fontName = '\0';
	else {
		name = GetListItem(fontNameList, fontNum);
		strcpy(fontName, name);
		fontName[strlen(fontName) - 5] = '\0';
	}
}

/*
 *	Open font specified by fontNum
 */

TextFontPtr GetNumFont(FontNum fontNum, FontSize size)
{
	TextAttr textAttr;

	if (fontNum < 0 || fontNum >= numFontNames || size > MAX_FONTSIZE)
		return (NULL);
	textAttr.ta_Name  = GetListItem(fontNameList, fontNum);
	textAttr.ta_YSize = size;
	textAttr.ta_Style = FS_NORMAL;
	textAttr.ta_Flags = (FPF_ROMFONT | FPF_DISKFONT | FPF_PROPORTIONAL);
	return (GetFont(&textAttr));
}

/*
 *	Return number of entries in font attr table
 */

WORD NumFontAttrs()
{
	return (numAttrs);
}

/*
 *	Return pointer to font attr table of existing fonts
 */

TextAttrPtr FontAttrTable()
{
	return (fontTable);
}

/*
 *	Return best size to use for scaling to desired size
 *	Note:  Under AmigaDOS 2.0, scaling is built into system, so we just
 *		   return given size
 */

FontSize FontScaleSize(FontNum fontNum, register FontSize reqSize)
{
	register WORD i;
	register FontSize size, smallSize, largeSize, doubleSize;
	BOOL hasHalf, hasDouble;
	TextPtr fontName;

	if (fontNum < 0 || fontNum >= numFontNames)
		return (0);
	if (LibraryVersion(DiskfontBase) >= OSVERSION_2_0)
		return ((FontSize) MIN(reqSize, MAX_FONTSIZE));
/*
	Find next larger and next smaller sizes, and if there is a half or double size
*/
	fontName = GetListItem(fontNameList, fontNum);
	smallSize = 0;
	largeSize = 0x7FFF;
	doubleSize = reqSize << 1;
	hasHalf = hasDouble = FALSE;
	for (i = 0; i < numAttrs; i++) {
		if (fontTable[i].ta_Name == fontName) {
			size = fontTable[i].ta_YSize;
			if (size == reqSize)
				return (size);
			if (size < reqSize) {
				if (size > smallSize)
					smallSize = size;
			}
			else {
				if (size < largeSize)
					largeSize = size;
			}
			if ((size << 1) == reqSize)
				hasHalf = TRUE;
			else if (size == doubleSize)
				hasDouble = TRUE;
		}
	}
/*
	Determine which size is best for scaling
*/
	if (hasDouble)
		return (doubleSize);
	if (hasHalf)
		return (reqSize >> 1);
/*
	Choose smaller size over larger size to avoid dropping pixels
*/
	if (smallSize > 0)
		return (smallSize);
	if (largeSize < 0x7FFF)
		return (largeSize);
	return (reqSize);				/* No sizes found! */
}

/*
 *	Determine if this is a valid font for the font table
 */

static BOOL ValidFont(AvailFontsPtr af)
{
	UBYTE flags = af->af_Attr.ta_Flags;
	UWORD type = af->af_Type;

	if ((flags & (FPF_REMOVED | FPF_REVPATH))			||
		((type & AFF_MEMORY) && (flags & FPF_DISKFONT))	||
		af->af_Attr.ta_YSize > MAX_FONTSIZE)
		return (FALSE);
	return (TRUE);
}

/*
 *	Initialize font data structures
 *	Return success status
 */

BOOL InitFonts()
{
	register WORD num, nameNum;
	register FontNum fontNum;
	register LONG afBuffSize, needed;
	BOOL success;
	AFHeaderPtr afHeader;
	AvailFontsPtr availFonts;
	TextAttrPtr textAttr;

	success = FALSE;
	afHeader = NULL;
	numFontNames = numAttrs = 0;
	BeginWait();
/*
	Get list of disk fonts
*/
	afBuffSize = 0;
	needed = 5000;
	do {
		if (afBuffSize)
			FreeMem(afHeader, afBuffSize);
		afBuffSize += needed;
		if ((afHeader = AllocMem(afBuffSize, 0)) == NULL)
			goto Exit;
	} while (needed = AvailFonts((BYTE *) afHeader, afBuffSize, AFF_MEMORY | AFF_DISK));
/*
	Count the number of fonts in table
*/
	availFonts = (AvailFontsPtr) (afHeader + 1);
	for (num = 0; num < afHeader->afh_NumEntries; num++) {
		if (ValidFont(&availFonts[num]))
			numAttrs++;
	}
	if (numAttrs > MAX_FONTS)
		numAttrs = MAX_FONTS;
/*
	Allocate memory for table and save pointers to fonts
*/
	if ((fontNameList = CreateList()) == NULL ||
		(fontTable = AllocMem(numAttrs*sizeof(TextAttr), 0)) == NULL)
		goto Exit;
	availFonts = (AvailFontsPtr) (afHeader + 1);
	fontNum = 0;
	for (num = 0; num < afHeader->afh_NumEntries; num++) {
		if (!ValidFont(&availFonts[num]))
			continue;
		textAttr = &availFonts[num].af_Attr;
		if ((nameNum = FindListItem(fontNameList, textAttr->ta_Name)) == -1)
			nameNum = AddListItem(fontNameList, textAttr->ta_Name, 0, FALSE);
		if (nameNum == -1)
			goto Exit;
		fontTable[fontNum] = *textAttr;
		fontTable[fontNum].ta_Name = GetListItem(fontNameList, nameNum);
		fontNum++;
	}
	numFontNames = NumListItems(fontNameList);	/* Cache this value */
	EndWait();
	success = TRUE;
Exit:
	if (afHeader)
		FreeMem(afHeader, afBuffSize);
	return (success);
}

/*
 *	Deallocate all memory used by InitFonts
 */

void ShutDownFonts()
{
	if (fontTable)
		FreeMem(fontTable, numAttrs*sizeof(TextAttr));
	if (fontNameList)
		DisposeList(fontNameList);
}
