/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Ruler and grid routines
 */

#include <exec/types.h>
#include <graphics/gfxmacros.h>
#include <intuition/intuition.h>

#include <proto/graphics.h>
#include <proto/layers.h>

#include <string.h>

#include <Toolbox/Graphics.h>
#include <Toolbox/Window.h>
#include <Toolbox/Utility.h>

#include "Draw.h"
#include "Proto.h"

/*
 *	External variables
 */

extern UBYTE	gridSixteenthUnits[], gridMMUnits[];

extern Options	options;

extern TextFontPtr	smallFont;

extern Rectangle	dragRect;
extern BOOL			autoScroll;

/*
 *	Local definitions and variables
 */

#define RULER_HEIGHT	12
#define RULER_WIDTH		12

#define RULER_TOPEDGE	(RULER_HEIGHT - 1)
#define RULER_LEFTEDGE	(RULER_WIDTH - 1)

#define INCH_HEIGHT		10
#define HALF_HEIGHT		6
#define QUARTER_HEIGHT	3
#define EIGHTH_HEIGHT	2

#define InchToDots(i, dpi)		( (LONG) (i)*(dpi))
#define EighthToDots(i, dpi)	(((LONG) (i)*(dpi))/8L)
#define SixteenthToDots(i, dpi)	(((LONG) (i)*(dpi) + 8L)/16L)

#define DotsToInch(i, dpi)		(((LONG) (i) + (dpi)/2)/(dpi))
#define DotsToEighth(i, dpi)	(( 8L*(i) + (dpi)/2)/(dpi))
#define DotsToSixteenth(i, dpi)	((16L*(i) + (dpi)/2)/(dpi))

#define CMToDots(i, dpi)	((100L*(i)*(dpi) + 127L)/254L)
#define MMToDots(i, dpi)	(( 10L*(i)*(dpi) + 127L)/254L)

#define DotsToCM(i, dpi)	((254L*(i) + 50L*(dpi))/(100L*(dpi)))
#define DotsToMM(i, dpi)	((254L*(i) +  5L*(dpi))/( 10L*(dpi)))

/*
 *	Local prototypes
 */

WORD	Round(WORD, WORD);

void	DrawValue(RastPtr, BOOL, WORD, WORD, WORD);

void	RulerTrack(WindowPtr, WORD, WORD, WORD, WORD, BOOL);

void	DrawRulerIndic(WindowPtr);

/*
 *	Return horizontal and vertical grid draw position for given index
 *	(Index 0 is left edge of document)
 *	Value returned is in document coordinates
 *	Note: Grid draw locations are not the same as grid snap locations
 */

void GridDrawLoc(DocDataPtr docData, WORD i, WORD *x, WORD *y)
{
	WORD xDPI, yDPI;
	LONG unitsPerDiv;			/* To force LONG calculations */

	xDPI = docData->xDPI;
	yDPI = docData->yDPI;
	switch (options.MeasureUnit) {
/*
	Centimeter ruler
*/
	case MEASURE_CM:
		unitsPerDiv = 1;				/* Units are cm */
		for (;;) {
			ScaleDocToWin(docData->Scale, CMToDots(unitsPerDiv, xDPI), 0, x, y);
			if (*x >= 30)
				break;
			unitsPerDiv *= 2;
			ScaleDocToWin(docData->Scale, CMToDots(unitsPerDiv, xDPI), 0, x, y);
			if (*x >= 30)
				break;
			unitsPerDiv *= 5;
		}
		*x = CMToDots(i*unitsPerDiv, xDPI)
			 + (docData->RulerOffset.x % CMToDots(unitsPerDiv, xDPI));
		*y = CMToDots(i*unitsPerDiv, yDPI)
			 + (docData->RulerOffset.y % CMToDots(unitsPerDiv, yDPI));
		break;
/*
	Inch ruler (and default)
*/
	case MEASURE_INCH:
	default:
		unitsPerDiv = 1;				/* Units are 1/8 inch */
		for (;;) {
			ScaleDocToWin(docData->Scale, EighthToDots(unitsPerDiv, xDPI), 0, x, y);
			if (*x >= 30)
				break;
			unitsPerDiv *= 2;
		}
		*x = EighthToDots(i*unitsPerDiv, xDPI)
			 + (docData->RulerOffset.x % EighthToDots(unitsPerDiv, xDPI));
		*y = EighthToDots(i*unitsPerDiv, yDPI)
			 + (docData->RulerOffset.y % EighthToDots(unitsPerDiv, yDPI));
		break;
	}
}

/*
 *	Return (approximate) grid snap spacing
 */

void GetGridSpacing(DocDataPtr docData, WORD *x, WORD *y)
{
	WORD gridUnits, xDPI, yDPI;

	if ((docData->Flags & DOC_GRIDSNAP) == 0) {
		*x = *y = 1;
		return;
	}
	xDPI = docData->xDPI;
	yDPI = docData->yDPI;
	switch (options.MeasureUnit) {
	case MEASURE_CM:
		gridUnits = gridMMUnits[docData->GridUnitsIndex];
		*x = MMToDots(gridUnits, xDPI);
		*y = MMToDots(gridUnits, yDPI);
		break;
	case MEASURE_INCH:
	default:
		gridUnits = gridSixteenthUnits[docData->GridUnitsIndex];
		*x = SixteenthToDots(gridUnits, xDPI);
		*y = SixteenthToDots(gridUnits, yDPI);
		break;
	}
}

/*
 *	Round value to nearest multiple of grid
 */

static WORD Round(WORD value, WORD grid)
{
	value = (value + grid/2)/grid;
	value = value*grid;
	return (value);
}

/*
 *	Adjust position (in document coordinates) to grid
 */

void SnapToGrid(DocDataPtr docData, WORD *x, WORD *y)
{
	WORD xDPI, yDPI;
	LONG units, gridUnits;		/* To force LONG calculations */

	if ((docData->Flags & DOC_GRIDSNAP) == 0)
		return;
	xDPI = docData->xDPI;
	yDPI = docData->yDPI;
	*x -= docData->RulerOffset.x;
	*y -= docData->RulerOffset.y;
	switch (options.MeasureUnit) {
/*
	Centimeter ruler
*/
	case MEASURE_CM:
		gridUnits = gridMMUnits[docData->GridUnitsIndex];
		if (*x < 0)
			*x -= MMToDots(gridUnits, xDPI);
		units = Round(DotsToMM(*x, xDPI), gridUnits);
		*x = MMToDots(units, xDPI);
		if (*y < 0)
			*y -= MMToDots(gridUnits, yDPI);
		units = Round(DotsToMM(*y, yDPI), gridUnits);
		*y = MMToDots(units, yDPI);
		break;
/*
	Inch ruler (and default)
*/
	case MEASURE_INCH:
	default:
		gridUnits = gridSixteenthUnits[docData->GridUnitsIndex];
		if (*x < 0)
			*x -= SixteenthToDots(gridUnits, xDPI);
		units = Round(DotsToSixteenth(*x, xDPI), gridUnits);
		*x = SixteenthToDots(units, xDPI);
		if (*y < 0)
			*y -= SixteenthToDots(gridUnits, yDPI);
		units = Round(DotsToSixteenth(*y, yDPI), gridUnits);
		*y = SixteenthToDots(units, yDPI);
		break;
	}
	*x += docData->RulerOffset.x;
	*y += docData->RulerOffset.y;
}

/*
 *	Return width of ruler
 */

WORD RulerWidth(WindowPtr window)
{
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);

	return ((WORD) ((docData->Flags & DOC_SHOWRULER) ? RULER_WIDTH : 0));
}

/*
 *	Return height of ruler
 */

WORD RulerHeight(WindowPtr window)
{
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);

	return ((WORD) ((docData->Flags & DOC_SHOWRULER) ? RULER_HEIGHT : 0));
}

/*
 *	Display measurement value in ruler
 */

static void DrawValue(RastPtr rPort, BOOL horiz, WORD x, WORD y, WORD value)
{
	WORD i;
	TextChar numString[10];

	NumToString((LONG) value, numString);
	if (horiz) {
		Move(rPort, x + 2, y + smallFont->tf_Baseline + 1);
		Text(rPort, numString, strlen(numString));
	}
	else {
		for (i = 0; numString[i]; i++, y += smallFont->tf_YSize) {
			Move(rPort, x + 1, y + smallFont->tf_Baseline + 2);
			Text(rPort, numString + i, 1);
		}
	}
}

/*
 *	Draw rulers in window
 */

void DrawRuler(WindowPtr window)
{
	WORD x, y, units, unitsPerDiv, xDPI, yDPI;
	register RastPtr rPort = window->RPort;
	register DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
	Rectangle rect;

	if ((docData->Flags & DOC_SHOWRULER) == 0)
		return;
	xDPI = docData->xDPI;
	yDPI = docData->yDPI;
/*
	Clear the ruler area
*/
	GetWindowRect(window, &rect);
	RGBForeColor(rPort, RGBCOLOR_WHITE);
	SetDrMd(rPort, JAM1);
	SetDrPt(rPort, 0xFFFF);
	RectFill(rPort, rect.MinX, rect.MinY, rect.MaxX, rect.MinY + RULER_TOPEDGE);
	RectFill(rPort, rect.MinX, rect.MinY, rect.MinX + RULER_LEFTEDGE, rect.MaxY);
/*
	Draw ruler edges
*/
	RGBForeColor(rPort, RGBCOLOR_BLACK);
	SetFont(rPort, smallFont);
	Move(rPort, rect.MinX, rect.MinY + RULER_TOPEDGE);
	Draw(rPort, rect.MaxX, rect.MinY + RULER_TOPEDGE);
	Move(rPort, rect.MinX + RULER_LEFTEDGE, rect.MinY);
	Draw(rPort, rect.MinX + RULER_LEFTEDGE, rect.MaxY);
/*
	Draw ruler divisions
*/
	switch (options.MeasureUnit) {
/*
	Inch ruler
*/
	case MEASURE_INCH:
		unitsPerDiv = 1;				/* Units are 1/8 inch */
		for (;;) {
			ScaleDocToWin(docData->Scale, EighthToDots(unitsPerDiv, xDPI), 0, &x, &y);
			if (x >= 9)
				break;
			unitsPerDiv *= 2;
		}
		units = DotsToEighth(docData->RulerOffset.x, xDPI);
		units += unitsPerDiv - (units % unitsPerDiv);
		units = -units;
		for (;;) {
			DocToWindow(window, EighthToDots(units, xDPI) + docData->RulerOffset.x, 0,
						&x, &y);
			if (x > rect.MaxX)
				break;
			if (x >= rect.MinX - 32) {
				Move(rPort, x, rect.MinY + RULER_TOPEDGE);
				if (units % 8 == 0) {
					Draw(rPort, x, rect.MinY + (RULER_TOPEDGE - INCH_HEIGHT));
					DrawValue(rPort, TRUE, x, rect.MinY, units/8);
				}
				else if (units % 4 == 0)
					Draw(rPort, x, rect.MinY + (RULER_TOPEDGE - HALF_HEIGHT));
				else if (units % 2 == 0)
					Draw(rPort, x, rect.MinY + (RULER_TOPEDGE - QUARTER_HEIGHT));
				else
					Draw(rPort, x, rect.MinY + (RULER_TOPEDGE - EIGHTH_HEIGHT));
			}
			units += unitsPerDiv;
		}
		units = DotsToEighth(docData->RulerOffset.y, yDPI);
		units += unitsPerDiv - (units % unitsPerDiv);
		units = -units;
		for (;;) {
			DocToWindow(window, 0, EighthToDots(units, yDPI) + docData->RulerOffset.y,
						&x, &y);
			if (y > rect.MaxY)
				break;
			if (y >= rect.MinY - 32) {
				Move(rPort, rect.MinX + RULER_LEFTEDGE, y);
				if (units % 8 == 0) {
					Draw(rPort, rect.MinX + (RULER_LEFTEDGE - INCH_HEIGHT), y);
					DrawValue(rPort, FALSE, rect.MinX, y, units/8);
				}
				else if (units % 4 == 0)
					Draw(rPort, rect.MinX + (RULER_LEFTEDGE - HALF_HEIGHT), y);
				else if (units % 2 == 0)
					Draw(rPort, rect.MinX + (RULER_LEFTEDGE - QUARTER_HEIGHT), y);
				else
					Draw(rPort, rect.MinX + (RULER_LEFTEDGE - EIGHTH_HEIGHT), y);
			}
			units += unitsPerDiv;
		}
		break;
/*
	CM ruler
*/
	case MEASURE_CM:
		unitsPerDiv = 1;				/* Units are mm */
		for (;;) {
			ScaleDocToWin(docData->Scale, MMToDots(unitsPerDiv, xDPI), 0, &x, &y);
			if (x >= 5)
				break;
			unitsPerDiv *= 2;
			ScaleDocToWin(docData->Scale, MMToDots(unitsPerDiv, xDPI), 0, &x, &y);
			if (x >= 5)
				break;
			unitsPerDiv *= 5;
		}
		units = DotsToMM(docData->RulerOffset.x, xDPI);
		units += unitsPerDiv - (units % unitsPerDiv);
		units = -units;
		for (;;) {
			DocToWindow(window, MMToDots(units, xDPI) + docData->RulerOffset.x, 0,
						&x, &y);
			if (x > rect.MaxX)
				break;
			if (x >= rect.MinX - 32) {
				Move(rPort, x, rect.MinY + RULER_TOPEDGE);
				if (units % 10 == 0) {
					Draw(rPort, x, rect.MinY + (RULER_TOPEDGE - INCH_HEIGHT));
					DrawValue(rPort, TRUE, x, rect.MinY, units/10);
				}
				else if (units % 5 == 0) {
					Draw(rPort, x, rect.MinY + (RULER_TOPEDGE - HALF_HEIGHT));
				}
				else
					Draw(rPort, x, rect.MinY + (RULER_TOPEDGE - EIGHTH_HEIGHT));
			}
			units += unitsPerDiv;
		}
		units = DotsToMM(docData->RulerOffset.y, yDPI);
		units += unitsPerDiv - (units % unitsPerDiv);
		units = -units;
		for (;;) {
			DocToWindow(window, 0, MMToDots(units, yDPI) + docData->RulerOffset.y,
						&x, &y);
			if (y > rect.MaxY)
				break;
			if (y >= rect.MinY - 32) {
				Move(rPort, rect.MinX + RULER_LEFTEDGE, y);
				if (units % 10 == 0) {
					Draw(rPort, rect.MinX + (RULER_LEFTEDGE - INCH_HEIGHT), y);
					DrawValue(rPort, FALSE, rect.MinX, y, units/10);
				}
				else if (units % 5 == 0) {
					Draw(rPort, rect.MinX + (RULER_LEFTEDGE - HALF_HEIGHT), y);
				}
				else
					Draw(rPort, rect.MinX + (RULER_LEFTEDGE - EIGHTH_HEIGHT), y);
			}
			units += unitsPerDiv;
		}
		break;
	}
/*
	Clear upper left corner (in case we rendered into it)
*/
	RGBForeColor(rPort, RGBCOLOR_WHITE);
	RectFill(rPort, rect.MinX, rect.MinY, rect.MinX + RULER_LEFTEDGE - 1,
			 rect.MinY + RULER_TOPEDGE - 1);
}

/*
 *	Draw routine for tracking ruler offset
 */

static void RulerTrack(WindowPtr window, WORD xStart, WORD yStart, WORD xEnd, WORD yEnd,
					   BOOL change)
{
	WORD x, y;
	RastPtr rPort = window->RPort;
	Rectangle rect;

	if (change) {
		GetContentRect(window, &rect);
		DocToWindow(window, xEnd, yEnd, &x, &y);
		SetDrMd(rPort, COMPLEMENT);
		SetDrPt(rPort, 0xFFFF);
		Move(rPort, x, rect.MinY);
		Draw(rPort, x, rect.MaxY);
		Move(rPort, rect.MinX, y);
		Draw(rPort, rect.MaxX, y);
	}
}

/*
 *	Get new ruler offset
 */

void DoSetRuler(WindowPtr window, WORD mouseX, WORD mouseY)
{
	WORD x, y;
	Rectangle rect;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);

	x = y = 0;
	autoScroll = FALSE;
	TrackMouse(window, 0, &x, &y, RulerTrack, NULL);
	autoScroll = TRUE;
	GetContentRect(window, &rect);
	if (window->MouseX < rect.MinX && window->MouseY < rect.MinY)
		x = y = 0;
	docData->RulerOffset.x = x;
	docData->RulerOffset.y = y;
	GetWindowRect(window, &rect);
	InvalRect(window, &rect);
}

/*
 *	Draw ruler indicator
 */

static void DrawRulerIndic(WindowPtr window)
{
	WORD x, y;
	RastPtr rPort = window->RPort;
	LayerPtr layer = window->WLayer;
	RegionPtr oldClip;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
	Rectangle rect;

	if ((docData->Flags & DOC_SHOWRULER) == 0)
		return;
	oldClip = InstallClipRegion(layer, NULL);	/* Remove clipping for speed */
/*
	Set up for drawing
*/
	GetWindowRect(window, &rect);
	SetDrMd(rPort, COMPLEMENT);
/*
	Draw ruler indicator
*/
	SetDrPt(rPort, 0xAAAA);
	DocToWindow(window, docData->RulerIndic.MinX, docData->RulerIndic.MinY, &x, &y);
	if (x >= rect.MinX + RULER_LEFTEDGE + 1 && x <= rect.MaxX) {
		Move(rPort, x, rect.MinY);
		Draw(rPort, x, rect.MinY + RULER_TOPEDGE - 1);
	}
	if (y >= rect.MinY + RULER_TOPEDGE + 1 && y <= rect.MaxY) {
		Move(rPort, rect.MinX, y);
		Draw(rPort, rect.MinX + RULER_LEFTEDGE - 1, y);
	}
/*
	If dragging, draw rectangle
*/
	if (docData->Flags & DOC_DRAGGING) {
		SetDrPt(rPort, 0x5555);			/* So as not to erase other markers */
		DocToWindow(window, docData->RulerIndic.MaxX, docData->RulerIndic.MaxY, &x, &y);
		if (x >= rect.MinX + RULER_LEFTEDGE + 1 && x <= rect.MaxX) {
			Move(rPort, x, rect.MinY);
			Draw(rPort, x, rect.MinY + RULER_TOPEDGE - 1);
		}
		if (y >= rect.MinY + RULER_TOPEDGE + 1 && y <= rect.MaxY) {
			Move(rPort, rect.MinX, y);
			Draw(rPort, rect.MinX + RULER_LEFTEDGE - 1, y);
		}
	}
/*
	Restore old clip region
*/
	(void) InstallClipRegion(layer, oldClip);
}

/*
 *	Turn on ruler indicator
 */

void RulerIndicOn(WindowPtr window)
{
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);

	if ((window->Flags & WINDOWACTIVE) == 0 ||
		(docData->Flags & DOC_RULERINDIC) != 0 ||
		docData->Scale == 0)
		return;
	docData->Flags |= DOC_RULERINDIC;
	if ((docData->Flags & DOC_SHOWRULER) == 0)
		return;
	if (docData->Flags & DOC_DRAGGING)
		docData->RulerIndic = dragRect;
	else {
		WindowToDoc(window, window->MouseX, window->MouseY, &docData->RulerIndic.MinX, &docData->RulerIndic.MinY);
		SnapToGrid(docData, &docData->RulerIndic.MinX, &docData->RulerIndic.MinY);
	}
	DrawRulerIndic(window);
}

/*
 *	Turn off ruler indicator
 */

void RulerIndicOff(WindowPtr window)
{
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);

	if (docData->Flags & DOC_RULERINDIC) {
		DrawRulerIndic(window);
		docData->Flags &= ~DOC_RULERINDIC;
	}
}

/*
 *	Update ruler indicator
 */

void UpdateRulerIndic(WindowPtr window)
{
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
	Rectangle rect;

	if ((docData->Flags & DOC_RULERINDIC) == 0)
		return;
	if (docData->Flags & DOC_DRAGGING) {
		rect = dragRect;
		if (rect.MinX == docData->RulerIndic.MinX && rect.MinY == docData->RulerIndic.MinY &&
			rect.MaxX == docData->RulerIndic.MaxX && rect.MaxY == docData->RulerIndic.MaxY)
			return;
	}
	else {
		WindowToDoc(window, window->MouseX, window->MouseY, &rect.MinX, &rect.MinY);
		SnapToGrid(docData, &rect.MinX, &rect.MinY);
		if (rect.MinX == docData->RulerIndic.MinX && rect.MinY == docData->RulerIndic.MinY)
			return;
	}
	DrawRulerIndic(window);
	docData->RulerIndic = rect;
	DrawRulerIndic(window);
}
