/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1990 New Horizons Software, Inc.
 *
 *	HotLink routines
 */

#include <exec/types.h>

#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/hotlinks.h>
#include <proto/graphics.h>
#include <proto/intuition.h>

#include <hotlinks/hotlinks.h>

#include <Toolbox/Utility.h>
#include <Toolbox/Window.h>
#include <Toolbox/Screen.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Font.h>
#include <Toolbox/ColorPick.h>
#include <Toolbox/Memory.h>

#include <IFF/DRAW.h>
#include <IFF/Packer.h>
#include <IFF/ILBM.h>

#include <string.h>

#include "Draw.h"
#include "Proto.h"

/*
	external variables
*/

extern struct HotLinksBase *HotLinksBase;
extern MsgPortPtr	hotLinksMsgPort;

extern ScreenPtr	screen;

extern UBYTE	fileBuff[];

extern WORD		numWindows;

extern WindowPtr windowList[];

extern Defaults	defaults;
extern Options  options;

extern WindowPtr	cmdWindow;

extern TextChar		strProgName[], strUntitled[];

/*
	local variables
*/

typedef struct HLMsg	HLMsg, 		*HLMsgPtr;

#define ID_DSWK	MakeID('D','S','W','K')
#define XBMI	MakeID('X','B','M','I')
#define AUTH	MakeID('A','U','T','H')

#define NM		((WORD)('N')<<8 | (WORD)('M'))
#define UC		((WORD)('U')<<8 | (WORD)('C'))
#define LI		((WORD)('L')<<8 | (WORD)('I'))

#define HL_OKAY 		0
#define HL_ERR			1

static ULONG hotLinksHandle;

static LONG	 writeLen;

/*
	local prototypes
*/

static BOOL	NewHLDocument(DocDataPtr);
static void	BuildGrayScale(RGBColor *,WORD,WORD *);
static LONG GetHL_CMAP(PubBlockPtr,Color4 *, WORD *,LONG);
static LONG GetHL_BODY(PubBlockPtr,LONG,struct BitMap *,PLANEPTR,BitMapHeader *,BYTE *, LONG);
static BOOL ReadHLPicture(WindowPtr,PubBlockPtr,LONG);

static LONG PutHL_CAMG(PubBlockPtr, LONG);
static LONG PutHL_CMAP(PubBlockPtr,Color4 *, WORD);
static LONG PutHL_BODY(PubBlockPtr,struct BitMap *, PLANEPTR,BitMapHeader *, BYTE *,LONG);
static LONG PutHL_BMHD(PubBlockPtr,BitMapHeader *);

static BOOL	CreateHLLayer(DocDataPtr,DocLayerPtr,RectPtr);
static BOOL	DoHLPublish(WindowPtr);
static LONG __asm SubscribeFilter(register __a0 PubBlockPtr);
static BOOL DoHLSubscribe(WindowPtr);
static BOOL	DoHLUpdate(WindowPtr);
static WindowPtr IsDWPubBlock(PubBlockPtr,BOOL);
static BOOL DoHLInformation(WindowPtr);
static BOOL DoHLBreakLink(WindowPtr);

/*
	open hotlinks for use if not already open
*/

void InitHotLinks()
{
	if (HotLinksBase)			/* already open */
		return;

	if (HotLinksBase == NULL)
		HotLinksBase = (struct HotLinksBase *)
					OpenLibrary("hotlinks.library", 0);

	if (HotLinksBase == NULL)	/* unable to open */
		return;

	if ((hotLinksMsgPort = CreatePort(NULL, 0)) == NULL) {
		ShutDownHotLinks();
		return;
	}

	if ((hotLinksHandle = HLRegister(ID_DSWK,hotLinksMsgPort,screen)) == NULL)
		ShutDownHotLinks();

}

/*
	shut down hotlinks and close Library
*/

void ShutDownHotLinks()
{
	HLMsgPtr	msg;

	if (HotLinksBase == NULL)
		return;

    if (hotLinksHandle)
        UnRegister(hotLinksHandle);
	hotLinksHandle = 0;

	if (hotLinksMsgPort) {
		while ((msg = GetMsg(hotLinksMsgPort)) != NULL)
			ReplyMsg((MsgPtr)msg);
		DeletePort(hotLinksMsgPort);
	}

	CloseLibrary(HotLinksBase);
}

/*
	copy pubRecord
*/

void CopyPubRecord(PubRecordPtr pubRecA,PubRecordPtr pubRecB)
{
	pubRecB->ID[0] 		= pubRecA->ID[0];
	pubRecB->ID[1] 		= pubRecA->ID[1];
	pubRecB->Type  		= pubRecA->Type;
    pubRecB->Version    = pubRecA->Version;
    pubRecB->CDate      = pubRecA->CDate;
    pubRecB->CTime      = pubRecA->CTime;
	pubRecB->MDate      = pubRecA->MDate;
	pubRecB->MTime      = pubRecA->MTime;
	pubRecB->Access     = pubRecA->Access;
	pubRecB->Creator    = pubRecA->Creator;
	BlockMove(pubRecB->Name,pubRecA->Name,32);
	BlockMove(pubRecB->Desc,pubRecA->Desc,256);
	BlockMove(pubRecB->Owner,pubRecA->Owner,32);
	BlockMove(pubRecB->Group,pubRecA->Group,32);
}

/*
	allocate a hotlinks publication block or return NULL if unsuccessful
*/

PubBlockPtr AllocHLPubBlock(PubRecordPtr pubRec)
{
	PubBlockPtr pBlock;

	if (HotLinksBase == NULL)
		return (NULL);

	pBlock = AllocPBlock(hotLinksHandle);

	if (pBlock == (PubBlockPtr)NOMEMORY) {
		Error(ERR_NO_MEM);
		return (NULL);
	}
	if (pBlock == (PubBlockPtr)NOPRIV)
		return (NULL);

	if (pubRec)
		CopyPubRecord(pubRec,&pBlock->PRec);

	return (pBlock);
}

/*
	dispose of publication record
*/

void	DisposeHLPubBlock(PubBlockPtr pBlock)
{
	if (HotLinksBase == NULL || pBlock == NULL)
		return;

	Notify(pBlock,NOINFORM,HLCLASS,NULL);
	FreePBlock(pBlock);
}

BOOL LockDocument(DocDataPtr docData)
{
	PubBlockPtr pBlock = docData->PubBlock;
	LONG	temp;

	if (pBlock && !docData->HasLock) {
		if ((temp = LockPub(pBlock,LOCK_WRITE)) != NOERROR) {
			Error(ERR_WRITE_LOCK);
			return (FALSE);
		}
		docData->HasLock = TRUE;
	}
	return (TRUE);
}

BOOL UnlockDocument(DocDataPtr docData)
{
	PubBlockPtr pBlock = docData->PubBlock;

	if (pBlock && docData->HasLock) {
		if (LockPub(pBlock,LOCK_RELEASE) != NOERROR)
			return (FALSE);
		docData->HasLock = FALSE;
	}
	return (TRUE);
}

/*
	create a new doc but don't change the print rec
*/

static BOOL NewHLDocument(DocDataPtr docData)
{
	DisposeAll(docData);
	if (NewDocLayer(docData, NULL) == NULL) {
		DisposeAll(docData);
		return (FALSE);
	}
	SetCurrLayer(docData, TopLayer(docData));

	return (TRUE);
}

/*
	build a gray table
*/

static void BuildGrayScale(RGBColor *table,WORD planes,WORD *number)
{
	WORD	i,
			gray;

	*number = 1 << planes;

	for (i=0; i<*number; i++) {
		gray = (2*(15*i) + (*number-1))/2*(*number - 1);
		table[*number - 1 - i]	= RGBCOLOR(gray,gray,gray);
	}
}

/*
 *	GetCMAP
 *	pNColorRegs is passed in as a pointer to the number of ColorRegisters
 *	caller has space to hold.  GetCMAP sets to the number actually read
 */

static LONG GetHL_CMAP(PubBlockPtr pBlock,Color4 *colorMap, WORD *pNColorRegs,LONG size)
{
	register WORD nColorRegs,actualColorRegs;
	ColorRegister colorReg;

	actualColorRegs = nColorRegs = size/sizeofColorRegister;
	if (*pNColorRegs < nColorRegs)
		nColorRegs = *pNColorRegs;
	*pNColorRegs = nColorRegs;		/* Set to the number actually there */
	for (; nColorRegs > 0; --nColorRegs)  {
		if (ReadPub(pBlock, (UBYTE *)&colorReg, sizeofColorRegister) == 0)
			return (1);
		*colorMap++ = ((colorReg.red   >> 4 ) << 8) |
					  ((colorReg.green >> 4 ) << 4) |
					  ((colorReg.blue  >> 4 )	 );
	}
	if (*pNColorRegs < actualColorRegs) {
		if (SeekPub(pBlock,sizeofColorRegister*(actualColorRegs - *pNColorRegs),
				SEEK_CURRENT) < HL_OKAY)
			return (1);
	}
	return (HL_OKAY);
}

/*
 *	GetBODY
 */

static LONG GetHL_BODY(PubBlockPtr pBlock,LONG size,struct BitMap *bitmap, PLANEPTR mask,
			 BitMapHeader *bmHdr, BYTE *buffer, LONG bufsize)
{
	UBYTE srcPlaneCnt = bmHdr->nPlanes;	/* Haven't counted for mask plane yet */
	WORD srcRowBytes = RowBytes(bmHdr->w);
	LONG bufRowBytes = MaxPackedSize(srcRowBytes);
	WORD nRows, scan=0;
	register WORD iPlane, iRow, nEmpty;
	register WORD nFilled;
	BYTE *buf, *nullDest, *nullBuf, **pDest;
	PLANEPTR planes[MaxSrcPlanes];	/* Array of ptrs to planes & mask */

/*
	Complain if client asked for a conversion GetBODY doesn't handle
*/
	if (bmHdr->compression > cmpByteRun1 || srcRowBytes != bitmap->BytesPerRow ||
		bufsize < bufRowBytes*2 || srcPlaneCnt > MaxSrcPlanes)
		return (1);
	nRows = bmHdr->h;
	if (nRows > bitmap->Rows)
		nRows = bitmap->Rows;
/*
	Initialize array "planes" with bitmap ptrs; NULL in empty slots
*/
	for (iPlane = 0; iPlane < bitmap->Depth; iPlane++)
		planes[iPlane] = bitmap->Planes[iPlane];
	while (iPlane < MaxSrcPlanes)
		planes[iPlane++] = NULL;
/*
	Copy any mask plane ptr into corresponding "planes" slot
	If there are more srcPlanes than dstPlanes, there will be
		NULL plane-pointers before this
*/
	if (bmHdr->masking == mskHasMask)
			planes[srcPlaneCnt++] = mask;
/*
	Setup a sink for dummy destination of rows from unwanted planes
*/
	nullDest = buffer;
	buffer  += srcRowBytes;
	bufsize -= srcRowBytes;
/*
	Read the BODY contents into client's bitmap
	De-interleave planes and decompress rows
	MODIFIES: Last iteration modifies bufsize
*/
	buf = buffer + bufsize;				/* Buffer is currently empty */
	for (iRow = nRows; iRow > 0; iRow--)  {
		for (iPlane = 0; iPlane < srcPlaneCnt; iPlane++)  {
			NextBusyPointer();
			pDest = &planes[iPlane];
/*
	Establish a sink for any unwanted plane
*/
			if (*pDest == NULL) {
				nullBuf = nullDest;
				pDest   = &nullBuf;
			}
/*
	Read in at least enough bytes to uncompress next row
*/
			nEmpty  = buf - buffer;		/* Size of empty part of buffer */
			nFilled = bufsize - nEmpty;		/* This part has data */
			if (nFilled < bufRowBytes) {
/*
	Need to read more
	Move the existing data to the front of the buffer
	Now covers range buffer[0]..buffer[nFilled-1]
*/
				BlockMove(buf, buffer, nFilled);		/* Could be moving 0 bytes */
				if (nEmpty > (size - scan )) {
/*
	There aren't enough bytes left to fill the buffer
*/
					nEmpty = (size - scan );
					bufsize = nFilled + nEmpty;				/* heh-heh */
				}
/*
	Append new data to the existing data
*/
				if (ReadPub(pBlock, (UBYTE *)&buffer[nFilled], nEmpty) < 0)
					return (1);
				scan += nEmpty;
				buf	 = buffer;
				nFilled = bufsize;
				nEmpty  = 0;
			}
/*
	Copy uncompressed row to destination plane
*/
			if (bmHdr->compression == cmpNone) {
				if (nFilled < srcRowBytes)
					return (BAD_FORM);
				BlockMove(buf, *pDest, srcRowBytes);
				buf	+= srcRowBytes;
				*pDest += srcRowBytes;
			}
/*
	Decompress row to destination plane
*/
			else if (UnPackRow(&buf, pDest, nFilled, srcRowBytes))
				return (1);
		}
	}
	return (HL_OKAY);
}

/*
	read in a ILBM file
*/

static BOOL ReadHLPicture(WindowPtr window,PubBlockPtr pBlock,LONG length)
{
	DocDataPtr		docData = GetWRefCon(window);
	LONG			size;
	ULONG			type;
	register LONG 	iffp = HL_OKAY;
	register WORD 	depth;
	BOOL 			foundBMHD;
	LONG 			viewMode = 0;				/* Need to take address of this */
	register IFFPictPtr 		iffPict;
	BitMapHeader	bmHdr;
	WORD			ilbmType;
	UBYTE			buffer[10];
	Rectangle		rect;

	if ((iffPict = MemAlloc(sizeof(IFFPict), MEMF_CLEAR)) == NULL) {
		Error(ERR_NO_MEM);
		return (HL_ERR);
	}

	GetColorTable(screen,&iffPict->ColorTable);
	iffPict->NumColors = screen->RastPort.BitMap->Depth;

	while (length > 0 && iffp == HL_OKAY) {
		if (ReadPub(pBlock, (UBYTE *)&type, 4) != 4) {
			iffp = BAD_FORM;
			break;
		}
        if (ReadPub(pBlock, (UBYTE *)&size, 4) != 4) {
        	iffp = BAD_FORM;
        	break;
        }
        switch(type) {
        	case ID_BMHD:
        		foundBMHD = TRUE;
        		if (ReadPub(pBlock, (UBYTE *)&bmHdr, size) != size) {
        			iffp = BAD_FORM;
        			break;
        		}
        		iffPict->Width = bmHdr.w;
				iffPict->Height = bmHdr.h;
        		ilbmType = bmHdr.pad1;
        		iffPict->TranspColor = (bmHdr.masking == mskHasTransparentColor) ?
								   bmHdr.transparentColor : -1;
        		break;
	        case ID_CMAP:
	        	iffPict->NumColors = MAX_COLOR_REGS;
	        	GetHL_CMAP(pBlock,&iffPict->ColorTable[0], &iffPict->NumColors,size);
	        	break;
	        case ID_BODY:
	        	if (!foundBMHD || iffPict->NumColors == 0 || size <= 0) {
					iffp = BAD_FORM;
					break;
				}
				if (iffPict->BitMap) {
					if (iffPict->Mask)
						FreeRaster(iffPict->Mask, iffPict->BitMap->BytesPerRow*8,
								   iffPict->BitMap->Rows);
						DisposeBitMap(iffPict->BitMap);
				}
				iffPict->BitMap = NULL;
				iffPict->Mask = NULL;
				depth = MIN(bmHdr.nPlanes, MAX_DEPTH);
				if (depth > 8 && depth != 24) {
					iffp = BAD_FORM;
					break;
				}
				iffPict->BitMap = CreateBitMap(bmHdr.w, bmHdr.h, depth, FALSE);
				if (iffPict->BitMap == NULL) {
					iffp = CLIENT_ERROR;
					break;
				}
				if (bmHdr.masking == mskHasMask) {
					iffPict->Mask = AllocRaster(bmHdr.w, bmHdr.h);
					if (iffPict->Mask == NULL) {
						iffp = CLIENT_ERROR;
						break;
					}
				}
				switch (ilbmType) {
					case ILBM_GREY :
	        			BuildGrayScale(iffPict->ColorTable,bmHdr.nPlanes,&iffPict->NumColors);
					case ILBM_PAL:
						iffp = GetHL_BODY(pBlock,size,iffPict->BitMap, iffPict->Mask, &bmHdr,
							   fileBuff, FILEBUFF_SIZE);
						break;
					default :
						iffp = BAD_FORM;
						break;
				}
				if (iffp != HL_OKAY) {
					DisposePict(iffPict);
					break;
				}
			   	break;
			case ID_CAMG:
				if (ReadPub(pBlock, (BYTE *)&viewMode, size) != size)
        			iffp = BAD_FORM;
				else
					iffPict->ViewMode = viewMode;
				break;
			case XBMI:
				if (ReadPub(pBlock, buffer, size) != size) {
        			iffp = BAD_FORM;
        			break;
        		}
        		ilbmType = ((WORD)buffer[0] << 8 | buffer[1]);
				break;
	        default:
	        	SeekPub(pBlock, size, SEEK_CURRENT);
	        	break;
        }
        if (size%2) {
        	SeekPub(pBlock,1,SEEK_CURRENT);
        	size++;
        }
        length -= (size+8);
    }

    if (iffp == HL_OKAY) {
    	if (iffPict->BitMap) {
			depth = iffPict->BitMap->Depth;
			if (iffPict->NumColors == (1 << depth)/2 && (iffPict->ViewMode & HAM) == 0)
				iffPict->ViewMode |= EXTRA_HALFBRITE;
		}
		if (!NewHLDocument(docData)) {
			Error (ERR_NO_MEM);
			DisposePict(iffPict);
			return (FALSE);
		}
		GetContentRect(window,&rect);
		InvalRect(window,&rect);
		iffp = BMapConvert(window,iffPict, NULL, NULL, TRUE) ? HL_OKAY : HL_ERR;
	}
	else
		DisposePict(iffPict);

	return (iffp == HL_OKAY);
}

/*
	write a chunk header to pubBlock
*/

static LONG WriteChunk(PubBlockPtr pBlock,LONG id,LONG size)
{
	LONG error;

	error = WritePub(pBlock,(UBYTE *)&id,4);
	if (error == NOERROR)
		error = WritePub(pBlock,(UBYTE *)&size,4);

	writeLen += 8;

	return (error);
}

/*
 *	Put view mode chunk
 */

static LONG PutHL_CAMG(PubBlockPtr pBlock, LONG viewMode)
{

	if (WriteChunk(pBlock, ID_CAMG, sizeof(LONG)))
		return (WritePub(pBlock, (UBYTE *)&viewMode, 4));

	writeLen += 4;

	return (-1);
}

/*
 *	PutCMAP
 */

static LONG PutHL_CMAP(PubBlockPtr pBlock,register Color4 *colorMap, WORD depth)
{
	register LONG iffp;
	register LONG nColorRegs;
	ColorRegister colorReg;

	if (depth > MaxAmDepth)
		depth = MaxAmDepth;
	nColorRegs = 1 << depth;

	iffp = WriteChunk(pBlock,ID_CMAP, nColorRegs*sizeofColorRegister);
	while (iffp == NOERROR && nColorRegs--) {
		colorReg.red   = (*colorMap >> 4) & 0xF0;
		colorReg.green = (*colorMap	 ) & 0xF0;
		colorReg.blue  = (*colorMap << 4) & 0xF0;
		if ((iffp = WritePub(pBlock, (UBYTE *)&colorReg, sizeofColorRegister)) >= 0)
			iffp = NOERROR;
		writeLen += sizeofColorRegister;
		colorMap++;
		NextBusyPointer();
	}

	return (iffp);
}

/*
 *	PutBODY
 *	NOTE: This implementation could be a LOT faster if it used more of the
 *		supplied buffer
 *	It would make far fewer calls to IFFWriteBytes (and therefore to DOS Write)
 */

static LONG PutHL_BODY(PubBlockPtr pBlock,register struct BitMap *bitMap, PLANEPTR mask,
			 register BitMapHeader *bmHdr, BYTE *buffer, LONG bufSize)
{
	register LONG iffp;
	LONG rowBytes = bitMap->BytesPerRow;
	Compression compression = bmHdr->compression;
	WORD dstDepth = bmHdr->nPlanes;
	register WORD iPlane, iRow;
	register LONG packedRowBytes;
	BYTE *buf;
	PLANEPTR planes[MaxAmDepth + 1];	/* Array of ptrs to planes and mask */
	LONG	chunkSize = 0;

	if (bufSize < MaxPackedSize(rowBytes) || compression > cmpByteRun1 ||
		bitMap->Rows != bmHdr->h || rowBytes != RowBytes(bmHdr->w) ||
		dstDepth > bitMap->Depth || dstDepth > MaxAmDepth)
		return (-1);
/*
	Copy the ptrs to bit & mask planes into local array
*/
	for (iPlane = 0; iPlane < dstDepth; iPlane++)
		planes[iPlane] = bitMap->Planes[iPlane];
	if (mask)
		planes[dstDepth++] = mask;
/*
	Write out a BODY chunk header
*/
	iffp = WriteChunk(pBlock,ID_BODY, 0);
/*
	Write out the BODY contents
*/
	for (iRow = 0; iffp == NOERROR && iRow < bmHdr->h; iRow++) {
		for (iPlane = 0; iffp == NOERROR && iPlane < dstDepth; iPlane++) {
/*
	Compress and write next row
*/
			if (compression == cmpByteRun1) {
				buf = buffer;
				packedRowBytes = PackRow(&planes[iPlane], &buf, rowBytes);
				iffp = WritePub(pBlock, buffer, packedRowBytes);
				chunkSize += packedRowBytes;
			}
/*
	Write next row
*/
			else {
				iffp = WritePub(pBlock, planes[iPlane], rowBytes);
				planes[iPlane] += rowBytes;
				chunkSize += rowBytes;
			}
		}
		NextBusyPointer();
	}
/*
	Finish the chunk
*/
	if (iffp == NOERROR) {
		if (SeekPub(pBlock,-1*(chunkSize+4),SEEK_CURRENT) > 0)
			iffp  = WritePub(pBlock,(UBYTE *)&chunkSize,4);
		else
			iffp = -1;
	}
	writeLen += chunkSize;

	return (iffp);
}

static LONG PutHL_BMHD(PubBlockPtr pBlock,BitMapHeader *bmHdr)
{
	LONG	retval;

	if ((retval = WriteChunk(pBlock, ID_BMHD, sizeof(BitMapHeader))) == NOERROR) {
		retval = WritePub(pBlock, (BYTE *)bmHdr, sizeof(BitMapHeader));
	}
	writeLen += sizeof(BitMapHeader);
	return (retval);
}

/*
	create a single layer with all layers in doc combined
*/

static BOOL CreateHLLayer(DocDataPtr docData,DocLayerPtr docLayer,RectPtr docRect)
{
	DocLayerPtr 	layer;
	DocObjPtr		object;
	BOOL			firstObj = TRUE;

	for (layer = BottomLayer(docData); layer; layer = NextLayer(layer)) {
		for (object = BottomObject(layer); object; object = NextObj(object)) {
			if (DuplicateObject(docLayer,object) == NULL)
				return (FALSE);
			if (firstObj) {
				*docRect = object->Frame;
				firstObj = FALSE;
			}
			else
				UnionRect(docRect,&object->Frame,docRect);
		}
	}

	return (TRUE);
}

/*
	write a DW document to ILBM edition
*/

BOOL WriteHLPicture(PubBlockPtr pBlock,DocDataPtr docData, BOOL afterSave)
{
	WORD 			width, height,depth;
	LONG 			error,type,length = 0;
	RastPtr 		rPort;
	Rectangle 		rect;
	BitMapHeader 	bmHdr;
	PalettePtr 		palette;
	DocLayerPtr		layer;
	UBYTE			zero = 0;
	LONG			viewMode;

	rPort = NULL;
	palette = NULL;
	layer = NULL;

	if (!LockDocument(docData))
		goto Exit;

	RefreshWindows();
	error = -1;
	depth = options.HotLinksDepth;
	writeLen = 0;
	if ((layer = MemAlloc(sizeof(DocLayer),MEMF_CLEAR)) == NULL)
		return (FALSE);
/*
	Create bitmap to clip
*/
	if (!CreateHLLayer(docData,layer,&rect))
		goto Exit;

	width = RowBytes(rect.MaxX - rect.MinX + 1)*8;
	height = rect.MaxY - rect.MinY + 1;

	if (width > MAX_BLIT_SIZE || height > MAX_BLIT_SIZE ||
		(rPort = CreateRastPort(width, height, depth)) == NULL ||
		(palette = MemAlloc(sizeof(Palette), 0)) == NULL)
			goto Exit;

	BuildExportPalette(palette, depth);
	MakeInvColorTable(palette);
	ColorCorrectEnable(palette, FALSE);
	SetPalette(rPort, palette);
	DrawLayerObjects(rPort, layer);
	RGBForeColor(rPort, RGBCOLOR_WHITE);	/* To get FgPen of white */
	viewMode = (depth == 6) ? EXTRA_HALFBRITE : 0;
/*
	put the clip
*/
	error = InitBMHdr(&bmHdr, rPort->BitMap, mskHasTransparentColor,
								cmpByteRun1, rPort->FgPen, 320,200);
	if (error == HL_OKAY)
		error = NOERROR;
	bmHdr.w = width;
	bmHdr.h = height;
/*
	write edition
*/
	if (OpenPub(pBlock,OPEN_WRITE) != NOERROR)
		goto Exit;
/*
	write form chunk
*/
	type = FORM;
	if (error == NOERROR)
		error = WritePub(pBlock,(UBYTE *)&type,4);
	if (error == NOERROR)
		error = WritePub(pBlock,(UBYTE *)&writeLen,4);
	type = ILBM;
	if (error == NOERROR)
		error = WritePub(pBlock,(UBYTE *)&type,4);
	type = AUTH;
	if (error == NOERROR)
		error = WritePub(pBlock,(UBYTE *)&type,4);
	length = 12;
	if (error == NOERROR)
		error = WritePub(pBlock,(UBYTE *)&length,4);
	if (error == NOERROR)
		error = WritePub(pBlock,(UBYTE *)strProgName,strlen(strProgName));
	if (error == NOERROR)
		error = WritePub(pBlock,&zero,1);
	writeLen = 24;
/*
	Save bitmap data
*/
	if (error == NOERROR)
		error = PutHL_BMHD(pBlock,&bmHdr);
	if (error == NOERROR)
		error = PutHL_CMAP(pBlock,palette->ColorTable,
								(viewMode & EXTRA_HALFBRITE) ? (depth - 1) : depth);
	if (error == NOERROR && viewMode != 0)
		error = PutHL_CAMG(pBlock, viewMode);
	if (error == NOERROR)
		error = PutHL_BODY(pBlock,rPort->BitMap, NULL, &bmHdr, fileBuff, FILEBUFF_SIZE);

	if (error == NOERROR) {
		if (SeekPub(pBlock,4,SEEK_BEGINNING) > 0)
			error = WritePub(pBlock,(UBYTE *)&writeLen,4);
	}
/*
	Finish up
*/
	ClosePub(pBlock);

	docData->EditionVersion++;
	if (!afterSave)
		DocModified(docData);

Exit:
	UnlockDocument(docData);

	if (rPort)
		DisposeRastPort(rPort);
	if (palette)
		MemFree(palette, sizeof(Palette));
	DisposeDocLayer(layer);

	return (error == NOERROR);
}

/*
	publish a document to hotlinks
*/

static BOOL	DoHLPublish(WindowPtr window)
{
	BOOL		success = FALSE;
	PubBlockPtr	pBlock;
	DocDataPtr	docData;
	LONG		open;

	docData = (DocDataPtr)GetWRefCon(window);

	if (docData->PubBlock)
		return (FALSE);

	if ((pBlock = AllocHLPubBlock(NULL)) == NULL)
		return (FALSE);

	BlockMove(window->Title,pBlock->PRec.Name,MIN(strlen(window->Title),32));
	pBlock->PRec.Type = ILBM;
	pBlock->PRec.Access = ACC_DEFAULT;

	if ((open = PutPub(pBlock,NULL)) != NOERROR)
		goto Exit;
	RefreshWindows();

	SetStdPointer(window,POINTER_WAIT);
	success = WriteHLPicture(pBlock, docData, FALSE);
	if (!success)
		goto Exit;

	if (success && Notify(pBlock,EXINFORM,HLCLASS,pBlock) != NOERROR) {
		success = FALSE;
		goto Exit;
	}

	docData->PubBlock = pBlock;
	docData->EditionVersion = pBlock->PRec.Version;
	docData->GetUpdates = FALSE;
	if (pBlock->PRec.Name != docData->FileName) {
		strcpy(docData->FileName, pBlock->PRec.Name);
		SetWTitle(window,docData->FileName);
		ChangeWindowItem(window);
	}
	DocModified(docData);
	SetViewMenu();
Exit:

	if (!success && pBlock) {
		DisposeHLPubBlock(pBlock);
		if (open != IOERROR)
			Error(ERR_HL_WRITE);
	}
	SetPointerShape();
	return (success);
}

static LONG __asm SubscribeFilter(register __a0 PubBlockPtr pBlock)
{
	if (pBlock->PRec.Type == ILBM)
		return (ACCEPT);
	return (NOACCEPT);
}

/*
	subscribe a hotlinks document
*/

static BOOL DoHLSubscribe(WindowPtr window)
{
	BOOL			success = FALSE,
					newWindow = FALSE;
	ULONG			chunkType;
	LONG			length, open;
	WORD			len;
	PubBlockPtr		pBlock;
	register DocDataPtr docData;
	TextChar		oldTitle[100];
	WindowPtr		docWindow;

	if ((pBlock = AllocHLPubBlock(NULL)) == NULL)
		return (FALSE);

	if ((open = GetPub(pBlock,SubscribeFilter)) != NOERROR) {
		DisposeHLPubBlock(pBlock);
		return (FALSE);
	}
	RefreshWindows();
/*
	make sure there is a window
*/
	docWindow = NULL;
	if (numWindows) {
		if (!IsDocWindow(window))
			docWindow = windowList[numWindows - 1];
		else
			docWindow = window;
		docData = GetWRefCon(docWindow);
		len = strlen(strUntitled);
		GetWTitle(docWindow, oldTitle);
		if (strlen(oldTitle) > len &&
			CmpString(oldTitle, strUntitled, len, len, TRUE) == 0 &&
			docData->DirLock == NULL && (docData->Flags & DOC_MODIFIED) == 0) {
			DisposeAll(docData);
			strcpy(docData->WindowName, pBlock->PRec.Name);
			AdjustWindowTitle(docWindow);
			ChangeWindowItem(docWindow);
			ActivateWindow(docWindow);			/* In case it wasn't active */
		}
		else
			docWindow = NULL;
	}
	if (docWindow == NULL) {
		if (numWindows == MAX_WINDOWS)
			goto Exit2;
		if ((docWindow = CreateWindow(pBlock->PRec.Name)) == NULL) {
			Error(ERR_NO_MEM);
			DisposeHLPubBlock(pBlock);
			return (FALSE);
		}
		docData = GetWRefCon(docWindow);
		NewDocument(docData);
		newWindow = TRUE;
	}

	if (OpenPub(pBlock,OPEN_READ) != NOERROR)
		goto Exit2;

	if (ReadPub(pBlock, (UBYTE *)&chunkType, 4) != 4)
		goto Exit;
	if (ReadPub(pBlock, (UBYTE *)&length, 4) != 4)
		goto Exit;
	if (ReadPub(pBlock, (UBYTE *)&chunkType, 4) != 4)
		goto Exit;

	length -= 4;
    switch (chunkType) {
    	case ILBM:
   			success = ReadHLPicture(docWindow,pBlock,length);
    		break;
    	case DTXT:
    	//	Error(ERR_HL_READ);
			break;
	}

Exit:
	ClosePub(pBlock);
	if (success)
		success = (Notify(pBlock,EXINFORM,HLCLASS,pBlock) == NOERROR);
Exit2:
	if (!success) {
		Error(ERR_HL_READ);
		if (pBlock)
			DisposeHLPubBlock(pBlock);
		if (newWindow)
			RemoveWindow(docWindow);
		else if (IsDocWindow(window)) {
			docData = GetWRefCon(window);
			strcpy(docData->WindowName, oldTitle);
			AdjustWindowTitle(window);
			ChangeWindowItem(window);
		}
		return (FALSE);
	}

	docData = (DocDataPtr)GetWRefCon(docWindow);
	docData->PubBlock = pBlock;
	window = docWindow;
	docData->GetUpdates = TRUE;
	if (newWindow)
		DrawRuler(window);
	if (pBlock->PRec.Name != docData->FileName)
		strcpy(docData->FileName, pBlock->PRec.Name);
	SetViewMenu();
	DocModified(docData);

	return (TRUE);
}

/*
	update to hotlinks
*/

static BOOL DoHLUpdate(WindowPtr window)
{
	BOOL 	success = FALSE;
	DocDataPtr		docData;

	docData = (DocDataPtr)GetWRefCon(window);

	if (docData->PubBlock != NULL) {
		SetStdPointer(window,POINTER_WAIT);
		success = WriteHLPicture(docData->PubBlock,docData, FALSE);
		SetPointerShape();
	}

	if (!success)
		Error(ERR_HL_WRITE);

	return (success);
}

/*
 *	return DocObjPtr if PubBlockPtr is one used by a DesignWorks
 *	edition that is being used
 */

static WindowPtr IsDWPubBlock(PubBlockPtr pBlock,BOOL fromMsg)
{
	DocDataPtr		docData;
	WORD			i;

	for (i=0; i<numWindows; i++) {
		docData = (DocDataPtr)GetWRefCon(windowList[i]);
		if (pBlock == docData->PubBlock) {
			if (docData->GetUpdates)
				return (windowList[i]);

			if (GetInfo(pBlock) == NOERROR) {
/* break link if is DW edition and DW didn't update */
				if (docData->EditionVersion != pBlock->PRec.Version) {
					StdDialog(DLG_BREAKLINK);
					ActivateWindow(windowList[i]);
					DoHLBreakLink(windowList[i]);
				}
				else {				/* ignore update but don't let hotlinks know */
					if (OpenPub(pBlock,OPEN_READ) == NOERROR) {
						ClosePub(pBlock);
						Notify(pBlock,EXINFORM,HLCLASS,pBlock);
					}
				}
			}
		}
	}

	return (NULL);
}

/*
	update prowrite structures if linked edition has changed
*/

BOOL UpdateDWLinks(PubBlockPtr pBlock,BOOL notify)
{
	ULONG		chunkType;
	LONG		length,changed;
	WindowPtr	window;
	DocDataPtr	docData;
	BOOL		success = FALSE;

	if ((window = IsDWPubBlock(pBlock,!notify)) == NULL)	/* not a DW link or link broken */
		return (TRUE);				/* nothing to update so not unsuccessful */

	if ((changed = PubStatus(pBlock)) == NOERROR)
		return (TRUE);

	if (changed == CHANGED) {
		if (OpenPub(pBlock,OPEN_READ) != NOERROR)
			return (FALSE);

		if (ReadPub(pBlock, (UBYTE *)&chunkType, 4) != 4)
			goto Exit;
		if (ReadPub(pBlock, (UBYTE *)&length, 4) != 4)
			goto Exit;
		if (ReadPub(pBlock, (UBYTE *)&chunkType, 4) != 4)
			goto Exit;

		length -= 4;

		HiliteSelectOff(window);
		docData = (DocDataPtr)GetWRefCon(window);

		if (chunkType == ILBM) {
			docData->PubBlock = NULL;			/* make sure pBlock isn't freed */
			success = ReadHLPicture(window,pBlock,length);
			docData->PubBlock = pBlock;
		}
	}

Exit:
	ClosePub(pBlock);

	if (success && notify)
		success = (Notify(pBlock,EXINFORM,HLCLASS,pBlock) == NOERROR);

	if (!success)
		Error(ERR_HL_READ);

	return (success);
}


/*
	process hotlinks message
*/

void DoHotLinksMsg(HLMsgPtr hlMsg)
{
	PubBlockPtr pBlock;

	if ((pBlock = (PubBlockPtr)hlMsg->UserData3) != NULL)
		(void)UpdateDWLinks(pBlock,FALSE);

	ReplyMsg((MsgPtr) hlMsg);
}

/*
	return information on edition
*/

static BOOL DoHLInformation(WindowPtr window)
{
	DocDataPtr docData = GetWRefCon(window);
	PubBlockPtr	pBlock;
	LONG	error;

	pBlock = docData->PubBlock;

	if ((error = PubInfo(pBlock)) != NOERROR && error != CHANGED)
		return (FALSE);

	if (pBlock->PRec.Name != docData->FileName) {
		strcpy(docData->FileName, pBlock->PRec.Name);
		SetWTitle(window,docData->FileName);
		ChangeWindowItem(window);
		DocModified(docData);
	}
	if (error == CHANGED)
		DocModified(docData);
	SetViewMenu();

	return (TRUE);
}

/*
	break the link to edition selected
*/

static BOOL DoHLBreakLink(WindowPtr window)
{
	register DocDataPtr docData = GetWRefCon(window);

	UnlockDocument(docData);

	DisposeHLPubBlock(docData->PubBlock);
	docData->PubBlock = NULL;

	DocModified(docData);

	return (TRUE);
}

/*
	process hotlinks menu
*/

BOOL DoHotLinksMenu(WindowPtr window,UWORD item)
{
	BOOL	success;

	if (HotLinksBase == NULL)
		return (TRUE);

	if (!IsDocWindow(window) && item != SUBSCRIBE_SUBITEM)
		return (FALSE);

	switch (item) {
		case PUBLISH_SUBITEM:
			success = DoHLPublish(window);
			break;
		case SUBSCRIBE_SUBITEM:
			success = DoHLSubscribe(window);
			break;
		case UPDATE_SUBITEM:
			success = DoHLUpdate(window);
			break;
		case INFO_SUBITEM:
			success = DoHLInformation(window);
			break;
		case BRKLINK_SUBITEM:
			success = DoHLBreakLink(window);
			break;
	}

	if (success)
		SetEditMenu();

	return (success);
}