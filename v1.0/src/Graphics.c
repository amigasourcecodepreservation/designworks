/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Graphics support routines
 */

#include <exec/types.h>
#include <graphics/gfxbase.h>
#include <graphics/gfxmacros.h>
#include <graphics/scale.h>
#include <intuition/intuition.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/layers.h>

#include <Toolbox/Memory.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Utility.h>

#include "Draw.h"
#include "Proto.h"

/*
 *	External variables
 */

extern struct GfxBase	*GfxBase;

extern Options	options;

/*
 *	Local variables and definitions
 */

#define MAX_BLIT_SIZE	1008			/* General use, OS 1.3 and prior */
#define MAX_BLIT_XSIZE	976				/* BltTemplate special case */
#define MAX_BLIT_YSIZE	1023

#define CYAN(rgb)		(0x0F - RED(rgb))
#define MAGENTA(rgb)	(0x0F - GREEN(rgb))
#define YELLOW(rgb)		(0x0F - BLUE(rgb))

static BOOL	colorCorrect = TRUE;

/*
 *	Local prototypes
 */

BOOL	AdvancedGraphics(void);

UWORD	PenNumber(RastPtr, RGBColor);
UWORD	*GetDisplayPat(RastPtr, RGBPat8Ptr);
void	DisposeScreenPat(RastPtr, UWORD *);

void	FrameRect1(RastPtr, RectPtr);
void	FrameOval1(RastPtr, RectPtr);
void	FramePoly1(RastPtr, WORD, PointPtr);

void	ScaleBitPlane(PLANEPTR, PLANEPTR, WORD, WORD, WORD, WORD, WORD, WORD);

/*
 *	Return TRUE if advanced graphics (big blits) are available
 */

static BOOL AdvancedGraphics()
{
	return ((BOOL) (LibraryVersion((struct Library *) GfxBase) >= OSVERSION_2_0 &&
					(GfxBase->ChipRevBits0 & GFXF_BIG_BLITS) != 0));
}

/*
 *	Do subtractive mix of two colors, returning result
 */

RGBColor MixColors2(register RGBColor color1, register RGBColor color2)
{
	register WORD red, green, blue;

	red		= (RED(color1)  *RED(color2)   + 7)/15;
	green	= (GREEN(color1)*GREEN(color2) + 7)/15;
	blue	= (BLUE(color1) *BLUE(color2)  + 7)/15;
	return (RGBCOLOR(red, green, blue));
}

/*
 *	Turn print->screen color mapping on or off
 */

void ColorCorrectEnable(BOOL enable)
{
	colorCorrect = enable;
}

/*
 *	Do color correction on given color
 */

RGBColor ColorCorrect(register RGBColor rgbColor)
{
	register WORD red, green, blue;
	WORD newRed, newGreen, newBlue;

	if (colorCorrect) {
		switch (rgbColor) {
		case RGBCOLOR_BLACK:
		case RGBCOLOR_WHITE:
			break;				/* No change */
		case RGBCOLOR_CYAN:
			rgbColor = options.PrtCyan;
			break;
		case RGBCOLOR_MAGENTA:
			rgbColor = options.PrtMagenta;
			break;
		case RGBCOLOR_YELLOW:
			rgbColor = options.PrtYellow;
			break;
		case RGBCOLOR_RED:
			rgbColor = MixColors2(options.PrtMagenta, options.PrtYellow);
			break;
		case RGBCOLOR_GREEN:
			rgbColor = MixColors2(options.PrtCyan, options.PrtYellow);
			break;
		case RGBCOLOR_BLUE:
			rgbColor = MixColors2(options.PrtCyan, options.PrtMagenta);
			break;
/*
	This is a simplified version of the following transformation

		cyan    = CYAN(rgbColor);
		magenta = MAGENTA(rgbColor);
		yellow  = YELLOW(rgbColor);
		red   = (0x0F - cyan*CYAN(options.PrtCyan)/0x0F)
				*(0x0F - magenta*CYAN(options.PrtMagenta)/0x0F)
				*(0x0F - yellow*CYAN(options.PrtYellow)/0x0F)
				/(0x0F*0x0F);
		green = (0x0F - cyan*MAGENTA(options.PrtCyan)/0x0F)
				 *(0x0F - magenta*MAGENTA(options.PrtMagenta)/0x0F)
				 *(0x0F - yellow*MAGENTA(options.PrtYellow)/0x0F)
				 /(0x0F*0x0F);
		blue  = (0x0F - cyan*YELLOW(options.PrtCyan)/0x0F)
				 *(0x0F - magenta*YELLOW(options.PrtMagenta)/0x0F)
				 *(0x0F - yellow*YELLOW(options.PrtYellow)/0x0F)
				 /(0x0F*0x0F);
*/
		default:
			red   = RED(rgbColor);
			green = GREEN(rgbColor);
			blue  = BLUE(rgbColor);
			newRed   = ( (red   + (RED(options.PrtCyan)   *(15 - red)   + 7)/15)
						*(green + (RED(options.PrtMagenta)*(15 - green) + 7)/15)
						*(blue  + (RED(options.PrtYellow) *(15 - blue)  + 7)/15)
						+ 112)/225;
			newGreen = ( (red   + (GREEN(options.PrtCyan)   *(15 - red)   + 7)/15)
						*(green + (GREEN(options.PrtMagenta)*(15 - green) + 7)/15)
						*(blue  + (GREEN(options.PrtYellow) *(15 - blue)  + 7)/15)
						+ 112)/225;
			newBlue  = ( (red   + (BLUE(options.PrtCyan)   *(15 - red)   + 7)/15)
						*(green + (BLUE(options.PrtMagenta)*(15 - green) + 7)/15)
						*(blue  + (BLUE(options.PrtYellow) *(15 - blue)  + 7)/15)
						+ 112)/225;
			rgbColor = RGBCOLOR(newRed, newGreen, newBlue);
			break;
		}
	}
	return (rgbColor);
}

/*
 *	Create inverse color lookup table
 *	Supports up to 256 colors in colorMap
 */

void MakeInvColorTable(register ColorTablePtr colorTable, WORD numColors,
					   InvColorTablePtr invColorTable)
{
	register WORD i, red, green, blue, redDiff, greenDiff, blueDiff;
	WORD diff, newDiff;
	RGBColor rgbColor;		/* Unsigned value */

/*
	Set inverse color table entries to closest matching color
*/
	for (rgbColor = 0; rgbColor < 0x1000; rgbColor++) {
		red   = RED(rgbColor);
		green = GREEN(rgbColor);
		blue  = BLUE(rgbColor);
		diff = 0x7FFF;
		(*invColorTable)[rgbColor] = 0;
		for (i = 0; i < numColors; i++) {
			redDiff = red - RED((*colorTable)[i]);
			if (redDiff < 0)
				redDiff = -redDiff;
			greenDiff = green - GREEN((*colorTable)[i]);
			if (greenDiff < 0)
				greenDiff = -greenDiff;
			blueDiff = blue - BLUE((*colorTable)[i]);
			if (blueDiff < 0)
				blueDiff = -blueDiff;
			newDiff = redDiff + greenDiff + blueDiff;
			if (newDiff < diff) {
				(*invColorTable)[rgbColor] = i;
				diff = newDiff;
				if (diff == 0)
					break;
			}
		}
	}
}

/*
 *	Get screen pen number for RGBColor
 *	Uses color table attached to rPort RP_User field
 *	If no color table, returns 0 for RGBCOLOR_WHITE, 1 for all other colors
 */

static UWORD PenNumber(RastPtr rPort, RGBColor rgbColor)
{
	InvColorTablePtr invColorTable;

	if ((invColorTable = (InvColorTablePtr) rPort->RP_User) == NULL)
		return ((UWORD) ((rgbColor == RGBCOLOR_WHITE) ? 0 : 1));
/*
	Map screen color and get pen number
*/
	rgbColor = ColorCorrect(rgbColor);
	return ((*invColorTable)[rgbColor & 0xFFF]);
}

/*
 *	Convert RGB pattern to Amiga display pattern
 *	Patterns are 8 by 8 pixels in size
 */

static UWORD *GetDisplayPat(RastPtr rPort, RGBPat8Ptr pat)
{
	register UBYTE *scrnPat;
	register WORD i, depth;
	register UWORD pen;
	WORD x, y, srcIndx, dstIndx;
	WORD planes[12];		/* Up to 12 bit-planes */

	depth = rPort->BitMap->Depth;
	if ((scrnPat = MemAlloc(depth*8*2, 0)) == NULL)	/* Amiga patterns are 16 bits wide */
		return (NULL);
	srcIndx = dstIndx = 0;
	for (y = 0; y < 8; y++) {
		for (x = 0; x < 8; x++) {
			pen = PenNumber(rPort, (*pat)[srcIndx++]);
			for (i = 0; i < depth; i++) {
				planes[i] = (planes[i] << 1) | (pen & 1);
				pen >>= 1;
			}
		}
		for (i = 0; i < depth; i++)
			scrnPat[dstIndx + i*16] = scrnPat[dstIndx + i*16 + 1] = planes[i];
		dstIndx += 2;
	}
	return ((UWORD *) scrnPat);
}

/*
 *	Dispose of pattern created by GetDisplayPat()
 */

static void DisposeScreenPat(RastPtr rPort, UWORD *scrnPat)
{
	MemFree(scrnPat, rPort->BitMap->Depth*8*2);
}

/*
 *	Init RastPort pen to black solid line
 */

void PenNormal(RastPtr rPort)
{
	RGBForeColor(rPort, RGBCOLOR_BLACK);
	SetDrMd(rPort, JAM1);
	SetDrPt(rPort, 0xFFFF);
}

/*
 *	Set foreground drawing color from RGB value
 */

void RGBForeColor(RastPtr rPort, RGBColor rgbColor)
{
	SetAPen(rPort, PenNumber(rPort, rgbColor));
}

/*
 *	Set background drawing color from RGB value
 */

void RGBBackColor(RastPtr rPort, RGBColor rgbColor)
{
	SetBPen(rPort, PenNumber(rPort, rgbColor));
}

/*
 *	Normalize angle to range 0..360
 */

WORD NormalizeAngle(register WORD angle)
{
	while (angle < 0)
		angle += 360;
	return ((WORD) (angle % 360));
}

/*
 *	Offset point
 */

void OffsetPoint(PointPtr pt, WORD dx, WORD dy)
{
	pt->x += dx;
	pt->y += dy;
}

/*
 *	Rotate point clockwise about given center
 *	Note: This looks backwards, but we measure y as positive downwards
 */

void RotatePoint(PointPtr pt, WORD cx, WORD cy, register WORD angle)
{
	register WORD x, y;

	angle = NormalizeAngle(angle);
	while (angle > 0) {
		x = pt->x;
		y = pt->y;
		pt->x = cx - (y - cy);
		pt->y = cy + (x - cx);
		angle -= 90;
	}
}

/*
 *	Flip point about given center
 */

void FlipPoint(PointPtr pt, WORD cx, WORD cy, BOOL horiz)
{
	if (horiz)
		pt->x = cx - (pt->x - cx);
	else
		pt->y = cy - (pt->y - cy);
}

/*
 *	Return (approximate) distance between two points
 */

WORD LineLength(PointPtr pt1, PointPtr pt2)
{
	LONG dx, dy, max, min;				/* Force LONG calculations */

	dx = ABS(pt2->x - pt1->x);
	dy = ABS(pt2->y - pt1->y);
	max = MAX(dx, dy);
	min = MIN(dx, dy);
	return ((WORD) ((max) ? (max + (min*min)/(max*2)) : 0));
}

/*
 *	Return the distance between a point and the line defined by two points
 */

WORD DistanceFromLine(PointPtr pt, PointPtr pt1, PointPtr pt2)
{
	LONG r, dist;

	r = LineLength(pt1, pt2);
	if (r == 0)
		return (LineLength(pt, pt1));
	dist = (LONG) pt2->x*pt1->y - (LONG) pt1->x*pt2->y +
		   (LONG) pt1->x* pt->y - (LONG)  pt->x*pt1->y +
		   (LONG)  pt->x*pt2->y - (LONG) pt2->x* pt->y;
	if (dist < 0)
		dist = -dist;
	dist = (dist + r/2)/r;
	return ((WORD) dist);
}

/*
 *	Determine if point is within the given slop of the specified line segment
 */

BOOL PtNearLine(PointPtr pt, PointPtr start, PointPtr end, WORD width, WORD height,
				WORD slop)
{
	LONG r, dx, dy;			/* To force LONG calculations */

/*
	First check to see if point is within line segment's boundary
*/
	dx = width/2 + slop;
	dy = height/2 + slop;
	if (pt->x < MIN(start->x, end->x) - dx || pt->x > MAX(start->x, end->x) + dx ||
		pt->y < MIN(start->y, end->y) - dy || pt->y > MAX(start->y, end->y) + dy)
		return (FALSE);
/*
	Now check to see if point is near line
*/
	r = LineLength(start, end);
	if (r > 0) {
		dx = end->x - start->x;
		dy = end->y - start->y;
		if (dx < 0)
			dx = -dx;
		if (dy < 0)
			dy = -dy;
		slop += (dx*height + dy*width + r)/(r*2);
	}
	return ((BOOL) (DistanceFromLine(pt, start, end) <= slop));
}

/*
 *	Return TRUE if two points are equal
 */

BOOL EqualPt(PointPtr pt1, PointPtr pt2)
{
	return ((BOOL) (pt1->x == pt2->x && pt1->y == pt2->y));
}

/*
 *	Offset rectangle by given x and y amounts
 *	Movement for positive values is right and down
 */

void OffsetRect(RectPtr rect, WORD dx, WORD dy)
{
	rect->MinX += dx;
	rect->MinY += dy;
	rect->MaxX += dx;
	rect->MaxY += dy;
}

/*
 *	Move rectangle edges inward by given amount
 */

void InsetRect(RectPtr rect, WORD dx, WORD dy)
{
	rect->MinX += dx;
	rect->MinY += dy;
	rect->MaxX -= dx;
	rect->MaxY -= dy;
}

/*
 *	Rotate rectangle about given center
 */

void RotateRect(RectPtr rect, WORD cx, WORD cy, WORD angle)
{
	WORD x, y;
	Point pt;

	pt.x = rect->MinX;
	pt.y = rect->MinY;
	RotatePoint(&pt, cx, cy, angle);
	rect->MinX = pt.x;
	rect->MinY = pt.y;
	pt.x = rect->MaxX;
	pt.y = rect->MaxY;
	RotatePoint(&pt, cx, cy, angle);
	rect->MaxX = pt.x;
	rect->MaxY = pt.y;
	if (rect->MinX > rect->MaxX) {
		x = rect->MinX;
		rect->MinX = rect->MaxX;
		rect->MaxX = x;
	}
	if (rect->MinY > rect->MaxY) {
		y = rect->MinY;
		rect->MinY = rect->MaxY;
		rect->MaxY = y;
	}
}

/*
 *	Flip rectangle horizontally or vertically about given center
 */

void FlipRect(RectPtr rect, WORD cx, WORD cy, BOOL horiz)
{
	WORD x, y;
	Point pt;

	pt.x = rect->MinX;
	pt.y = rect->MinY;
	FlipPoint(&pt, cx, cy, horiz);
	rect->MinX = pt.x;
	rect->MinY = pt.y;
	pt.x = rect->MaxX;
	pt.y = rect->MaxY;
	FlipPoint(&pt, cx, cy, horiz);
	rect->MaxX = pt.x;
	rect->MaxY = pt.y;
	if (rect->MinX > rect->MaxX) {
		x = rect->MinX;
		rect->MinX = rect->MaxX;
		rect->MaxX = x;
	}
	if (rect->MinY > rect->MaxY) {
		y = rect->MinY;
		rect->MinY = rect->MaxY;
		rect->MaxY = y;
	}
}

/*
 *	Return the intersection of two rectangles
 *	Returns TRUE if rectangles intersect
 *	Returns with dstRect start > end if rectangles do not intersect
 */

BOOL SectRect(RectPtr src1, RectPtr src2, RectPtr dstRect)
{
	dstRect->MinX = MAX(src1->MinX, src2->MinX);
	dstRect->MinY = MAX(src1->MinY, src2->MinY);
	dstRect->MaxX = MIN(src1->MaxX, src2->MaxX);
	dstRect->MaxY = MIN(src1->MaxY, src2->MaxY);
	return ((BOOL) !EmptyRect(dstRect));
}

/*
 *	Calculate the union of two rectangles
 *	Works properly if destRect is a source rect
 */

void UnionRect(RectPtr src1, RectPtr src2, RectPtr dstRect)
{
	dstRect->MinX = MIN(src1->MinX, src2->MinX);
	dstRect->MinY = MIN(src1->MinY, src2->MinY);
	dstRect->MaxX = MAX(src1->MaxX, src2->MaxX);
	dstRect->MaxY = MAX(src1->MaxY, src2->MaxY);
}

/*
 *	Determine if given point is in rectangle
 */

BOOL PtInRect(PointPtr pt, RectPtr rect)
{
	return ((BOOL) (pt->x >= rect->MinX && pt->x <= rect->MaxX &&
					pt->y >= rect->MinY && pt->y <= rect->MaxY));
}

/*
 *	Return TRUE if rectangles are equal
 */

BOOL EqualRect(RectPtr rect1, RectPtr rect2)
{
	return ((BOOL) (rect1->MinX == rect2->MinX && rect1->MaxX == rect2->MaxX &&
					rect1->MinY == rect2->MinY && rect1->MaxY == rect2->MaxY));
}

/*
 *	Return TRUE if rect start is greater than end
 */

BOOL EmptyRect(RectPtr rect)
{
	return ((BOOL) (rect->MinX > rect->MaxX || rect->MinY > rect->MaxY));
}

/*
 *	Draw single pixel wide rectangle
 */

static void FrameRect1(RastPtr rPort, RectPtr rect)
{
	Move(rPort, rect->MinX, rect->MinY);
	Draw(rPort, rect->MaxX, rect->MinY);
	Draw(rPort, rect->MaxX, rect->MaxY);
	Draw(rPort, rect->MinX, rect->MaxY);
	if (rect->MinY < rect->MaxY - 1)
		Draw(rPort, rect->MinX, rect->MinY + 1);
	WaitBlit();
}

/*
 *	Draw rectangle frame
 */

void FrameRect(RastPtr rPort, RectPtr rect, WORD penWidth, WORD penHeight)
{
	WORD x, y;
	Rectangle drawRect;

	for (x = 0; x < penWidth; x++) {
		for (y = 0; y < penHeight; y++) {
			if (x != 0 && y != 0 && x != penWidth - 1 && y != penHeight - 1)
				continue;
			drawRect = *rect;
			if (x != 0 || y != 0)
				OffsetRect(&drawRect, x, y);
			FrameRect1(rPort, &drawRect);
		}
	}
}

/*
 *	Fill rect with pattern
 */

void FillRect(RastPtr rPort, RectPtr rect, RGBPat8Ptr pat)
{
	BYTE oldBgPen, oldPatSize;
	WORD xStart, xEnd, yStart, yEnd, maxSize;
	UWORD *oldPat, *newPat;

	if (EmptyRect(rect))
		return;
/*
	Convert pattern
*/
	oldBgPen = rPort->BgPen;
	oldPat = rPort->AreaPtrn;
	oldPatSize = rPort->AreaPtSz;
	if (pat && (newPat = GetDisplayPat(rPort, pat)) != NULL) {
		SetAfPt(rPort, newPat, -3);
		SetAPen(rPort, 255);
		SetBPen(rPort, 0);
	}
	else {
		newPat = NULL;
		SetAfPt(rPort, NULL, 0);
		if (pat)
			RGBForeColor(rPort, RGBCOLOR_BLACK);
	}
/*
	Do fill
	If rect is larger than system can handle, do it in pieces
*/
	maxSize = (AdvancedGraphics()) ? 0x7FFF : MAX_BLIT_SIZE;
	SetDrMd(rPort, JAM2);
	BNDRYOFF(rPort);
	yStart = rect->MinY;
	do {
		yEnd = rect->MaxY;
		if (yEnd - yStart + 1 > maxSize)
			yEnd = yStart + maxSize - 1;
		xStart = rect->MinX;
		do {
			xEnd = rect->MaxX;
			if (xEnd - xStart + 1 > maxSize)
				xEnd = xStart + maxSize - 1;
			RectFill(rPort, xStart, yStart, xEnd, yEnd);
			xStart = xEnd + 1;
		} while (xStart <= rect->MaxX);
		yStart = yEnd + 1;
	} while (yStart <= rect->MaxY);
	SetBPen(rPort, oldBgPen);
	SetAfPt(rPort, oldPat, oldPatSize);
	WaitBlit();
	if (newPat)
		DisposeScreenPat(rPort, newPat);
}

/*
 *	Draw single pixel wide oval
 *	Note: DrawEllipse has width/height of (2*radius + 1)
 */

void FrameOval1(RastPtr rPort, RectPtr rect)
{
	WORD cx, cy, rx, ry;

	cx = (rect->MinX + rect->MaxX)/2;
	cy = (rect->MinY + rect->MaxY)/2;
	rx = (rect->MaxX - rect->MinX)/2;
	ry = (rect->MaxY - rect->MinY)/2;
	if (rx == 0 || ry == 0) {
		Move(rPort, cx - rx, cy - ry);
		Draw(rPort, cx + rx, cy + ry);
	}
	else
		DrawEllipse(rPort, cx, cy, rx, ry);
	WaitBlit();
}

/*
 *	Draw oval frame
 */

void FrameOval(RastPtr rPort, RectPtr rect, WORD penWidth, WORD penHeight)
{
	WORD x, y;
	Rectangle drawRect;

	for (x = 0; x < penWidth; x++) {
		for (y = 0; y < penHeight; y++) {
			if (x != 0 && y != 0 && x != penWidth - 1 && y != penHeight - 1)
				continue;
			drawRect = *rect;
			if (x != 0 || y != 0)
				OffsetRect(&drawRect, x, y);
			FrameOval1(rPort, &drawRect);
		}
	}
}

/*
 *	Fill oval with pattern
 */

void FillOval(RastPtr rPort, RectPtr rect, RGBPat8Ptr pat)
{
	WORD cx, cy, rx, ry;
	WORD width, height;
	BYTE oldBgPen, oldPatSize;
	UWORD *oldPat, *newPat;
	PLANEPTR planePtr;
	struct AreaInfo areaInfo;
	struct TmpRas tmpRas;
	UWORD areaBuff[20];		/* 40 bytes = 8 vectors (to be sure) */

	if (EmptyRect(rect))
		return;
/*
	If oval is wider than system can handle, do filled rect instead
*/
	cx = (rect->MinX + rect->MaxX)/2;
	cy = (rect->MinY + rect->MaxY)/2;
	rx = (rect->MaxX - rect->MinX)/2;
	ry = (rect->MaxY - rect->MinY)/2;
	width = rect->MaxX - rect->MinX + 1 + 16;	/* System needs extra size */
	height = rect->MaxY - rect->MinY + 1;
	if (rx == 0 || ry == 0 ||
		(!AdvancedGraphics() && (width > MAX_BLIT_SIZE || height > MAX_BLIT_SIZE))) {
		FillRect(rPort, rect, pat);
		return;
	}
/*
	Convert pattern
*/
	oldBgPen = rPort->BgPen;
	oldPat = rPort->AreaPtrn;
	oldPatSize = rPort->AreaPtSz;
	if (pat && (newPat = GetDisplayPat(rPort, pat)) != NULL) {
		SetAfPt(rPort, newPat, -3);
		SetAPen(rPort, 255);
		SetBPen(rPort, 0);
	}
	else {
		newPat = NULL;
		SetAfPt(rPort, NULL, 0);
		if (pat)
			RGBForeColor(rPort, RGBCOLOR_BLACK);
	}
/*
	Do fill
*/
	SetDrMd(rPort, JAM2);
	BNDRYOFF(rPort);
	if ((planePtr = AllocRaster(width, height)) != NULL) {
		BlockClear(areaBuff, sizeof(areaBuff));
		InitArea(&areaInfo, areaBuff, sizeof(areaBuff)/5);
		InitTmpRas(&tmpRas, planePtr, RASSIZE((LONG) width, (LONG) height));
		rPort->AreaInfo = &areaInfo;
		rPort->TmpRas = &tmpRas;
		if (AreaEllipse(rPort, cx, cy, rx, ry) == 0)
			AreaEnd(rPort);
		WaitBlit();
		FreeRaster(planePtr, width, height);
		rPort->AreaInfo = NULL;
		rPort->TmpRas = NULL;
	}
	SetBPen(rPort, oldBgPen);
	SetAfPt(rPort, oldPat, oldPatSize);
	if (newPat)
		DisposeScreenPat(rPort, newPat);
}

/*
 *	Offset list of points in polygon
 */

void OffsetPoly(WORD numPoints, PointPtr pts, WORD dx, WORD dy)
{
	while (numPoints--) {
		pts[numPoints].x += dx;
		pts[numPoints].y += dy;
	}
}

/*
 *	Draw single pixel wide poly
 */

static void FramePoly1(RastPtr rPort, WORD numPoints, PointPtr pts)
{
	Move(rPort, pts[0].x, pts[0].y);
	PolyDraw(rPort, numPoints - 1, (WORD *) &pts[1]);
	WaitBlit();
}

/*
 *	Draw polygon frame
 */

void FramePoly(RastPtr rPort, WORD numPoints, PointPtr pts, WORD penWidth, WORD penHeight)
{
	WORD x, y;

	for (x = 0; x < penWidth; x++) {
		for (y = 0; y < penHeight; y++) {
			if (x != 0 && y != 0 && x != penWidth - 1 && y != penHeight - 1)
				continue;
			if (x != 0 || y != 0)
				OffsetPoly(numPoints, pts, x, y);
			FramePoly1(rPort, numPoints, pts);
			if (x != 0 || y != 0)
				OffsetPoly(numPoints, pts, (WORD) -x, (WORD) -y);
		}
	}
}

/*
 *	Fill polygon with pattern
 */

void FillPoly(RastPtr rPort, WORD numPoints, PointPtr pts, RGBPat8Ptr pat)
{
	WORD i, width, height;
	BYTE oldBgPen, oldPatSize;
	UWORD *oldPat, *newPat;
	UWORD *areaBuff;
	PLANEPTR planePtr;
	struct AreaInfo areaInfo;
	struct TmpRas tmpRas;
	Rectangle rect;

	if (numPoints < 3)
		return;
/*
	If poly is wider than system can handle, do filled rect instead
*/
	rect.MinX = rect.MaxX = pts[0].x;
	rect.MinY = rect.MaxY = pts[0].y;
	for (i = 1; i < numPoints; i++) {
		if (pts[i].x < rect.MinX)
			rect.MinX = pts[i].x;
		if (pts[i].x > rect.MaxX)
			rect.MaxX = pts[i].x;
		if (pts[i].y < rect.MinY)
			rect.MinY = pts[i].y;
		if (pts[i].y > rect.MaxY)
			rect.MaxY = pts[i].y;
	}
	width = rect.MaxX - rect.MinX + 1 + 16;		/* System needs extra size */
	height = rect.MaxY - rect.MinY + 1;
	if (!AdvancedGraphics() && (width > MAX_BLIT_SIZE || height > MAX_BLIT_SIZE)) {
		FillRect(rPort, &rect, pat);
		return;
	}
/*
	Convert pattern
*/
	oldBgPen = rPort->BgPen;
	oldPat = rPort->AreaPtrn;
	oldPatSize = rPort->AreaPtSz;
	if (pat && (newPat = GetDisplayPat(rPort, pat)) != NULL) {
		SetAfPt(rPort, newPat, -3);
		SetAPen(rPort, 255);
		SetBPen(rPort, 0);
	}
	else {
		newPat = NULL;
		SetAfPt(rPort, NULL, 0);
		if (pat)
			RGBForeColor(rPort, RGBCOLOR_BLACK);
	}
/*
	Do fill
*/
	SetDrMd(rPort, JAM2);
	BNDRYOFF(rPort);
	if ((areaBuff = MemAlloc((numPoints + 1)*5, MEMF_CLEAR)) != NULL) {
		if ((planePtr = AllocRaster(width, height)) != NULL) {
			InitArea(&areaInfo, areaBuff, numPoints + 1);
			InitTmpRas(&tmpRas, planePtr, RASSIZE((LONG) width, (LONG) height));
			rPort->AreaInfo = &areaInfo;
			rPort->TmpRas = &tmpRas;
			AreaMove(rPort, pts[0].x, pts[0].y);
			for (i = 1; i < numPoints; i++)
				AreaDraw(rPort, pts[i].x, pts[i].y);
			AreaEnd(rPort);
			WaitBlit();
			FreeRaster(planePtr, width, height);
			rPort->AreaInfo = NULL;
			rPort->TmpRas = NULL;
		}
		MemFree(areaBuff, (numPoints + 1)*5);
	}
	SetBPen(rPort, oldBgPen);
	SetAfPt(rPort, oldPat, oldPatSize);
	if (newPat)
		DisposeScreenPat(rPort, newPat);
}

/*
 *	Replacement for Text() due to bugs in OS prior to version 2.0
 */

void ExtText(register RastPtr rPort, register TextPtr text, register WORD len)
{
	register WORD cp_x, chLen, xSize, subLen;

/*
	If new OS, let system do the work
*/
	if (AdvancedGraphics())
		Text(rPort, text, len);
/*
	Handle problem of not rendering full text with negative x offsets
*/
	else {
		cp_x = rPort->cp_x;
		if (cp_x < 0) {
			while (len) {
				chLen = TextLength(rPort, text, 1);
				if (cp_x + chLen > 0)
					break;
				cp_x += chLen;
				text++;
				len--;
			}
			Move(rPort, cp_x, rPort->cp_y);
		}
/*
	Handle problem of not drawing text wider than MAX_BLIT_SIZE pixels
*/
		if (len) {
			xSize = rPort->Font->tf_XSize;
			while (len*xSize > MAX_BLIT_SIZE) {
				subLen = MAX_BLIT_SIZE/xSize;
				Text(rPort, text, subLen);
				text += subLen;
				len -= subLen;
			}
			Text(rPort, text, len);
		}
	}
	WaitBlit();
}

/*
 *	Do BltTemplate() in pieces when too large for blitter
 */

void ExtBltTemplate(PLANEPTR source, WORD srcX, WORD srcMod, RastPtr destRPort,
					WORD destX, WORD destY, WORD sizeX, WORD sizeY)
{
	WORD x, y, width, height;
	WORD destWidth, destHeight;
	LONG offset;

	if (AdvancedGraphics())
		BltTemplate(source, srcX, srcMod, destRPort, destX, destY, sizeX, sizeY);
	else {
		destWidth = destRPort->BitMap->BytesPerRow << 3;
		destHeight = destRPort->BitMap->Rows;
		y = destY;
		do {
			height = sizeY - (y - destY);
			if (height > MAX_BLIT_YSIZE)
				height = MAX_BLIT_YSIZE;
			x = destX;
			do {
				width = sizeX - (x - destX);
				if (width > MAX_BLIT_XSIZE)
					width = MAX_BLIT_XSIZE;
				if (x < destWidth && y < destHeight &&
					x + width - 1 >= 0 && y + height - 1 >= 0) {
					offset = srcX + (x - destX) + ((LONG) (y - destY)*srcMod << 3);
					BltTemplate(source + ((offset & 0xFFFFFFF0L) >> 3),
								offset & 0x0F, srcMod, destRPort,
								x, y, width, height);
				}
				x += width;
			} while (x < destX + sizeX);
			y += height;
		} while (y < destY + sizeY);
	}
	WaitBlit();
}

/*
 *	Scale point by width/height of srcRect to dstRect
 *	Used to scale pen sizes
 *	Will not return a value less than (1,1)
 */

void ScalePt(register PointPtr pt, register RectPtr srcRect, register RectPtr dstRect)
{
	register LONG oldDX, oldDY, newDX, newDY;	/* To force LONG calculations */

	oldDX = srcRect->MaxX - srcRect->MinX + 1;
	oldDY = srcRect->MaxY - srcRect->MinY + 1;
	newDX = dstRect->MaxX - dstRect->MinX + 1;
	newDY = dstRect->MaxY - dstRect->MinY + 1;
	if (oldDX != newDX)
		pt->x = (LONG) pt->x*newDX/oldDX + (newDX/oldDX >> 1);
	if (oldDY != newDY)
		pt->y = (LONG) pt->y*newDY/oldDY + (newDY/oldDY >> 1);
	if (pt->x < 1)
		pt->x = 1;
	if (pt->y < 1)
		pt->y = 1;
}

/*
 *	Map a point in srcRect to dstRect
 *	Guarantees that points inside src will be inside dst
 */

void MapPt(register PointPtr pt, register RectPtr srcRect, register RectPtr dstRect)
{
	register LONG x, y, oldDX, oldDY, newDX, newDY;	/* To force LONG calculations */

	x = pt->x - srcRect->MinX;
	y = pt->y - srcRect->MinY;
	oldDX = srcRect->MaxX - srcRect->MinX + 1;
	oldDY = srcRect->MaxY - srcRect->MinY + 1;
	newDX = dstRect->MaxX - dstRect->MinX + 1;
	newDY = dstRect->MaxY - dstRect->MinY + 1;
	if (oldDX != newDX)
		x = x*newDX/oldDX + (newDX/oldDX >> 1);
	if (oldDY != newDY)
		y = y*newDY/oldDY + (newDY/oldDY >> 1);
	pt->x = x + dstRect->MinX;
	pt->y = y + dstRect->MinY;
}

/*
 *	Map rectangle from srcRect to dstRect
 *	Guarantees the old corners will map to new corners
 */

void MapRect(RectPtr rect, RectPtr srcRect, RectPtr dstRect)
{
	Point pt;

	pt.x = rect->MinX;
	pt.y = rect->MinY;
	MapPt(&pt, srcRect, dstRect);
	rect->MinX = pt.x;
	rect->MinY = pt.y;
	pt.x = rect->MaxX + 1;
	pt.y = rect->MaxY + 1;
	MapPt(&pt, srcRect, dstRect);
	rect->MaxX = pt.x - 1;
	rect->MaxY = pt.y - 1;
}

/*
 *	Scale a single plane of data from one rect to another
 */

static void ScaleBitPlane(PLANEPTR src, PLANEPTR dst,
						  WORD srcWidth, WORD srcHeight,
						  WORD dstWidth, WORD dstHeight,
						  WORD srcRowBytes, WORD dstRowBytes)
{
	register PLANEPTR srcPlane, dstPlane;
	register UWORD srcByte, dstByte, srcShift, dstShift;
	register WORD dstX, xAccum;
	WORD dstY, yAccum;

	yAccum = -dstHeight;
	for (dstY = 0; dstY < dstHeight; dstY++) {
		while (yAccum >= 0) {
			src += srcRowBytes;
			yAccum -= dstHeight;
		}
		yAccum += srcHeight;
		srcPlane = src;
		dstPlane = dst;
		srcShift = dstShift = 0;
		srcByte = *srcPlane;
		dstByte = 0;
		xAccum = -dstWidth;
		for (dstX = 0; dstX < dstWidth; dstX++) {
			while (xAccum >= 0) {
				srcByte <<= 1;
				if (srcShift++ == 7) {
					srcByte = *(++srcPlane);
					srcShift = 0;
				}
				xAccum -= dstWidth;
			}
			xAccum += srcWidth;
			dstByte = (dstByte | (srcByte & 0x80)) << 1;
			if (dstShift++ == 7) {
				*dstPlane++ = dstByte >> 8;
				dstShift = dstByte = 0;
			}
		}
		if (dstShift)
			*dstPlane = dstByte >> dstShift;
		dst += dstRowBytes;
	}
}

/*
 *	Scale bitmap from old size to new size
 *	Bitmap planes must be in graphics memory
 *	Assumes bit planes are initially cleared
 */

void ScaleBitMap(RastPtr srcPort, RastPtr dstPort, RectPtr srcRect, RectPtr dstRect)
{
	WORD i, srcWidth, srcHeight, dstWidth, dstHeight;
	BitMapPtr srcBitMap, dstBitMap;
	struct BitScaleArgs bitScale;

	srcWidth  = srcRect->MaxX - srcRect->MinX + 1;
	srcHeight = srcRect->MaxY - srcRect->MinY + 1;
	dstWidth  = dstRect->MaxX - dstRect->MinX + 1;
	dstHeight = dstRect->MaxY - dstRect->MinY + 1;
/*
	Use graphics library function if available
	(Includes kludge for bug in KS 2.0)
*/
	if (LibraryVersion((struct Library *) GfxBase) >= OSVERSION_2_0 &&
		srcWidth < MAX_BLIT_SIZE && dstWidth < MAX_BLIT_SIZE) {
		bitScale.bsa_SrcX		= srcRect->MinX;
		bitScale.bsa_SrcY		= srcRect->MinY;
		bitScale.bsa_SrcWidth	= bitScale.bsa_XSrcFactor = srcWidth;
		bitScale.bsa_SrcHeight	= bitScale.bsa_YSrcFactor = srcHeight;
		bitScale.bsa_DestX		= dstRect->MinX;
		bitScale.bsa_DestY		= dstRect->MinY;
		bitScale.bsa_XDestFactor	= dstWidth;
		bitScale.bsa_YDestFactor	= dstHeight;
		bitScale.bsa_SrcBitMap	= srcPort->BitMap;
		bitScale.bsa_DestBitMap	= dstPort->BitMap;
		bitScale.bsa_Flags		= 0;
		BitMapScale(&bitScale);
		WaitBlit();
	}
/*
	Else do it ourselves
*/
	else {
		WaitBlit();
		srcBitMap = srcPort->BitMap;
		dstBitMap = dstPort->BitMap;
		for (i = 0; i < srcBitMap->Depth; i++)
			ScaleBitPlane(srcBitMap->Planes[i], dstBitMap->Planes[i],
						  srcWidth, srcHeight, dstWidth, dstHeight,
						  srcBitMap->BytesPerRow, dstBitMap->BytesPerRow);
	}
}

/*
 *	Transform bitmap from one rect to another
 *	Assumes dstPort planes are cleared to 0
 */

void TransformBitMap(RastPtr srcPort, RastPtr dstPort, RectPtr srcRect, RectPtr dstRect,
					 WORD angle, BOOL flipHoriz, BOOL flipVert)
{
	WORD pen;
	LONG srcWidth, srcHeight, dstWidth, dstHeight;	/* To force LONG calculations */
	Point srcPt, dstPt;
	Rectangle rotRect;

	if (angle == 0 && !flipHoriz && !flipVert) {
		ScaleBitMap(srcPort, dstPort, srcRect, dstRect);
		return;
	}
	srcWidth  = srcRect->MaxX - srcRect->MinX + 1;
	srcHeight = srcRect->MaxY - srcRect->MinY + 1;
	rotRect = *dstRect;
	if (angle)
		RotateRect(&rotRect, 0, 0, -angle);
	dstWidth  = rotRect.MaxX - rotRect.MinX + 1;
	dstHeight = rotRect.MaxY - rotRect.MinY + 1;
	SetDrMd(dstPort, JAM1);
	for (dstPt.y = dstRect->MinY; dstPt.y <= dstRect->MaxY; dstPt.y++) {
		for (dstPt.x = dstRect->MinX; dstPt.x <= dstRect->MaxX; dstPt.x++) {
			srcPt.x = dstPt.x - dstRect->MinX;
			srcPt.y = dstPt.y - dstRect->MinY;
			if (angle) {
				RotatePoint(&srcPt, 0, 0, -angle);
				srcPt.x -= rotRect.MinX;
				srcPt.y -= rotRect.MinY;
			}
			if (dstWidth != srcWidth)
				srcPt.x = ((LONG) srcPt.x*srcWidth)/dstWidth;
			if (dstHeight != srcHeight)
				srcPt.y = ((LONG) srcPt.y*srcHeight)/dstHeight;
			srcPt.x = (flipHoriz) ? srcRect->MaxX - srcPt.x : srcRect->MinX + srcPt.x;
			srcPt.y = (flipVert)  ? srcRect->MaxY - srcPt.y : srcRect->MinY + srcPt.y;
			if (PtInRect(&srcPt, srcRect)) {
				pen = ReadPixel(srcPort, srcPt.x, srcPt.y);
				if (pen) {
					SetAPen(dstPort, pen);
					WritePixel(dstPort, dstPt.x, dstPt.y);
				}
			}
		}
	}
	WaitBlit();
}

/*
 *	Allocate and initialize off-screen drawing area
 */

RastPtr CreateRastPort(WORD width, WORD height, WORD depth)
{
	LayerPtr layer;
	LayerInfoPtr layerInfo;
	BitMapPtr bitMap;

/*
	Allocate and initialize structures
*/
	if ((bitMap = CreateBitMap(width, height, depth)) == NULL)
		goto Exit1;
	if ((layerInfo = NewLayerInfo()) == NULL)
		goto Exit2;
	if ((layer = CreateUpfrontLayer(layerInfo, bitMap, 0, 0, width - 1,
									height - 1, LAYERSIMPLE, NULL)) == NULL)
		goto Exit3;
	return (layer->rp);
/*
	Error during allocation, clean up and return NULL
*/
Exit3:
	DisposeLayerInfo(layerInfo);
Exit2:
	DisposeBitMap(bitMap);
Exit1:
	return (NULL);
}

/*
 *	Free memory allocated by CreateRastPort()
 *	Note: Caller may set BitMap to NULL before calling this routine to
 *		  retain pointer to bitmap
 */

void DisposeRastPort(RastPtr rPort)
{
	BitMapPtr bitMap;
	LayerPtr layer;
	LayerInfoPtr layerInfo;

	if (rPort) {
		bitMap = rPort->BitMap;
		layer = rPort->Layer;
		layerInfo = layer->LayerInfo;
		DeleteLayer(NULL, layer);
		DisposeLayerInfo(layerInfo);
		DisposeBitMap(bitMap);
	}
}

/*
 *	Allocate and initialize bitmap and bit planes
 */

BitMapPtr CreateBitMap(WORD width, WORD height, WORD depth)
{
	register WORD i;
	WORD bmSize;
	LONG byteCount;
	register BitMapPtr bitMap;

/*
	Allocate and initialize structures
*/
	bmSize = sizeof(BitMap) + sizeof(PLANEPTR)*(depth - 8);
	if ((bitMap = MemAlloc(bmSize, MEMF_CLEAR)) == NULL)
		goto Exit1;
	InitBitMap(bitMap, depth, width, height);
	byteCount = (LONG) bitMap->BytesPerRow*bitMap->Rows;
	for (i = 0; i < depth; i++) {
		if ((bitMap->Planes[i] = AllocMem(byteCount, MEMF_CHIP)) == NULL)
			goto Exit2;
		BltClear(bitMap->Planes[i], byteCount, 1);
	}
	return (bitMap);
/*
	Error during allocation, clean up and return NULL
*/
Exit2:
	for (i = 0; i < depth; i++) {
		if (bitMap->Planes[i])
			FreeMem(bitMap->Planes[i], byteCount);
	}
	MemFree(bitMap, bmSize);
Exit1:
	return (NULL);
}

/*
 *	Dispose of bitmap created by CreateRastPort
 */

void DisposeBitMap(register BitMapPtr bitMap)
{
	register WORD i;
	WORD bmSize;
	LONG byteCount;

	if (bitMap) {
		bmSize = sizeof(BitMap) + sizeof(PLANEPTR)*(bitMap->Depth - 8);
		byteCount = (LONG) bitMap->BytesPerRow*bitMap->Rows;
		WaitBlit();				/* Make sure we are not using this memory */
		for (i = 0; i < bitMap->Depth; i++) {
			if (bitMap->Planes[i])
				FreeMem(bitMap->Planes[i], byteCount);
		}
		MemFree(bitMap, bmSize);
	}
}

/*
 *	Clear RastPort to white
 *	Needed since SetRast() doesn't work with RastPorts wider than MAX_BLIT_SIZE pixels
 *	May not respect clipping regions in rPort
 */

void ClearRast(RastPtr rPort)
{
	register UBYTE value;
	register WORD i, depth;
	register LONG j, size;
	BitMapPtr bitMap = rPort->BitMap;

	RGBForeColor(rPort, RGBCOLOR_WHITE);
	if (AdvancedGraphics() ||
		(bitMap->BytesPerRow <= MAX_BLIT_SIZE/8 && bitMap->Rows <= MAX_BLIT_SIZE)) {
		SetRast(rPort, rPort->FgPen);
		WaitBlit();
	}
	else {
		WaitBlit();
		size = (LONG) bitMap->BytesPerRow*bitMap->Rows;
		depth = bitMap->Depth;
		for (i = 0; i < depth; i++) {
			value = ((rPort->FgPen >> i) & 1) ? 0xFF : 0;
			for (j = 0; j < size; j++)
				bitMap->Planes[i][j] = value;
		}
	}
}
