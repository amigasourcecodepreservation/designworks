/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Amiga Toolbox
 *	Copyright (c) 1990 New Horizons Software
 *
 *	Window handler
 */

#ifndef TOOLBOX_WINDOW_H
#define TOOLBOX_WINDOW_H

#ifndef INTUITION_INTUITION_H
#include <intuition/intuition.h>
#endif

#ifndef TYPEDEFS_H
#include "TypeDefs.h"
#endif

/*
 *	Window kinds
 *	Stored in lower two bits of window UserData
 */

#define WKIND_DLOG	3		/* Values 0 - 2 are for user */

/*
 *	Routines prototypes
 */

void	GetWindowRect(WindowPtr, RectPtr);
void	InvalRect(WindowPtr, RectPtr);
void	SetWTitle(WindowPtr, TextPtr);
void	GetWTitle(WindowPtr, TextPtr);
void	SetWRefCon(WindowPtr, Ptr);
Ptr		GetWRefCon(WindowPtr);
void	SetWKind(WindowPtr, WORD);
WORD	GetWKind(WindowPtr);
void	CloseWindowSafely(WindowPtr, MsgPortPtr);

#endif
