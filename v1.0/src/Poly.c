/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Polygon handling routines
 */

#include <exec/types.h>
#include <graphics/gfxmacros.h>
#include <intuition/intuition.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>

#include <Toolbox/Memory.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Utility.h>
#include <Toolbox/Window.h>

#include "Draw.h"
#include "Proto.h"

/*
 *	External variables
 */

extern MsgPortPtr	mainMsgPort;

extern Defaults	defaults;
extern Options	options;

extern BOOL	autoScroll;

/*
 *	Local variables and definitions
 */

#define LINE_PAT	0xFFFF		/* Line pattern for object creation */

static PolyObjPtr	newPolyObj, growPolyObj;

static WORD	growHandle;

#define SELECTBUTTON(q)	\
	((q&IEQUALIFIER_LEFTBUTTON)||((q&AMIGAKEYS)&&(q&IEQUALIFIER_LALT)))

typedef struct {
	Point	*Points;
	LONG	NumPoints, MaxPoints;
} PointList, *PtListPtr;

/*
 *	Local prototypes
 */

PolyObjPtr	CreatePoly(DocDataPtr);

void		CalcBezierPoints(PtListPtr, PointPtr, PointPtr, PointPtr);
PtListPtr	GetBezierPoints(PointPtr, WORD, BOOL);
void		DispozeBezierPoints(PtListPtr);

void	PolyDrawObj1(RastPtr, PolyObjPtr, RectPtr, PointPtr, RGBColor, RGBPat8Ptr);

WORD	PolyLineNumber(PolyObjPtr, PointPtr);

void	SetPolyFrame(PolyObjPtr);
void	UnSetPolyFrame(PolyObjPtr);
void	CollapsePoly(PolyObjPtr);

void	CurveTrackMouse(WindowPtr, WORD, WORD, WORD, WORD, BOOL);

void	PolyDrawLine(WindowPtr, PointPtr, PointPtr, BOOL, RGBColor);

/*
 *	Allocate a new poly object
 */

PolyObjPtr PolyAllocate()
{
	return ((PolyObjPtr) MemAlloc(sizeof(PolyObj), MEMF_CLEAR));
}

/*
 *	Dispose of poly object
 */

void PolyDispose(PolyObjPtr polyObj)
{
	if (polyObj->NumPoints)
		MemFree(polyObj->Points, polyObj->NumPoints*sizeof(Point));
	MemFree(polyObj, sizeof(PolyObj));
}

/*
 *	Create new poly
 */

static PolyObjPtr CreatePoly(DocDataPtr docData)
{
	PolyObjPtr polyObj;

	if ((polyObj = (PolyObjPtr) NewDocObject(CurrLayer(docData), TYPE_POLY)) == NULL)
		return (NULL);
	polyObj->DocObj.Flags	= defaults.ObjFlags;
	polyObj->PolyFlags		= 0;
	polyObj->PenWidth		= defaults.PenWidth;
	polyObj->PenHeight		= defaults.PenHeight;
	polyObj->Points			= NULL;
	polyObj->NumPoints		= 0;
	polyObj->PenColor		= defaults.PenColor;
	CopyRGBPat8(&defaults.FillPat, &polyObj->FillPat);
	return (polyObj);
}

/*
 *	Calculate points on second-order Bezier curve using three control points
 *	Caller must have added at least one point to start of list
 *	Uses recursion
 */

static void CalcBezierPoints(ptList, pt1, pt2, pt3)
register PtListPtr ptList;
register PointPtr pt1, pt2, pt3;
{
	register WORD dx1, dx2, dy1, dy2;
	Point midPt, endPt;

	if (ptList->NumPoints >= ptList->MaxPoints)
		return;
	dx1 = pt2->x - pt1->x;
	dy1 = pt2->y - pt1->y;
	dx2 = pt3->x - pt2->x;
	dy2 = pt3->y - pt2->y;
/*
	Two-pixel accuracy gives smoother result (and it draws faster)
*/
	if ((ABS(dx1) <= 2 && ABS(dy1) <= 2) || (ABS(dx2) <= 2 && ABS(dy2) <= 2)) {
		if (pt3->x != ptList->Points[ptList->NumPoints - 1].x ||
			pt3->y != ptList->Points[ptList->NumPoints - 1].y)
			ptList->Points[ptList->NumPoints++] = *pt3;
	}
	else {
		endPt.x = (pt1->x + (pt2->x << 1) + pt3->x + 2) >> 2;
		endPt.y = (pt1->y + (pt2->y << 1) + pt3->y + 2) >> 2;
		midPt.x = (pt1->x + pt2->x + 1) >> 1;
		midPt.y = (pt1->y + pt2->y + 1) >> 1;
		CalcBezierPoints(ptList, pt1, &midPt, &endPt);
		midPt.x = (pt2->x + pt3->x + 1) >> 1;
		midPt.y = (pt2->y + pt3->y + 1) >> 1;
		CalcBezierPoints(ptList, &endPt, &midPt, pt3);
	}
}

/*
 *	Get bezier point list for polygon points
 *	If closed, then last point will equal first and numPoints is one greater than
 *		original number in polygon (since this is how it would be drawn if
 *		it weren't smoothed)
 */

static PtListPtr GetBezierPoints(PointPtr pts, WORD numPoints, BOOL closed)
{
	register WORD i, dx, dy;
	LONG maxPts;
	Point pt1, pt2, pt3;
	Point prevPt, currPt, nextPt;
	PtListPtr ptList;

	if (numPoints < 3 || (closed && numPoints < 4) ||
		(ptList = MemAlloc(sizeof(PointList), MEMF_CLEAR)) == NULL)
		return (NULL);
	maxPts = 0;
	for (i = 0; i < numPoints - 1; i++) {
		dx = pts[i + 1].x - pts[i].x;
		dy = pts[i + 1].y - pts[i].y;
		maxPts += MAX(ABS(dx), ABS(dy))/2 + 1;
	}
	if ((ptList->Points = MemAlloc(maxPts*sizeof(Point), 0)) == NULL) {
		MemFree(ptList, sizeof(PointList));
		return (NULL);
	}
	ptList->MaxPoints = maxPts;
	if (closed) {
		currPt = pts[numPoints - 2];	/* Since last point is same as first */
		nextPt = pts[0];
		pt3.x = (currPt.x + nextPt.x)/2;
		pt3.y = (currPt.y + nextPt.y)/2;
		ptList->Points[0] = pt3;
		ptList->NumPoints = 1;
		for (i = 0; i < numPoints - 1; i++) {
			currPt = nextPt;
			nextPt = pts[i + 1];
			pt1 = pt3;
			pt2 = currPt;
			pt3.x = (currPt.x + nextPt.x)/2;
			pt3.y = (currPt.y + nextPt.y)/2;
			CalcBezierPoints(ptList, &pt1, &pt2, &pt3);
		}
	}
	else {
		currPt = pts[0];
		nextPt = pts[1];
		pt3.x = (currPt.x + nextPt.x)/2;
		pt3.y = (currPt.y + nextPt.y)/2;
		ptList->Points[0] = currPt;
		ptList->NumPoints = 1;
		for (i = 1; i < numPoints - 1; i++) {
			prevPt = currPt;
			currPt = nextPt;
			nextPt = pts[i + 1];
			if (i == 1)
				pt1 = prevPt;
			else
				pt1 = pt3;
			pt2 = currPt;
			if (i == numPoints - 2)
				pt3 = nextPt;
			else {
				pt3.x = (currPt.x + nextPt.x)/2;
				pt3.y = (currPt.y + nextPt.y)/2;
			}
			CalcBezierPoints(ptList, &pt1, &pt2, &pt3);
		}
	}
	return (ptList);
}

/*
 *	Dispose of memory allocated by GetBezierPoints()
 */

static void DisposeBezierPoints(PtListPtr ptList)
{
	if (ptList) {
		if (ptList->Points)
			MemFree(ptList->Points, ptList->MaxPoints*sizeof(Point));
		MemFree(ptList, sizeof(PointList));
	}
}

/*
 *	Draw poly object using specified line size, line color, and pattern
 */

void PolyDrawObj1(RastPtr rPort, PolyObjPtr polyObj, RectPtr rect,
				  PointPtr pen, RGBColor penColor, RGBPat8Ptr fillPat)
{
	WORD i, numPoints, drawNum;
	BOOL closed, hasFill, hasPen;
	Point offset, *newPts, *drawPts;
	PtListPtr ptList;

	hasPen  = HasPen(polyObj);
	hasFill = HasFill(polyObj);
	closed  = PolyClosed(polyObj);
	if (!hasPen && !hasFill)
		return;
	ScalePt(pen, &polyObj->DocObj.Frame, rect);
/*
	Create new point list in rPort coordinates
*/
	if ((numPoints = polyObj->NumPoints) < 2)
		return;
	if (closed)
		numPoints++;
	if ((newPts = MemAlloc(numPoints*sizeof(Point), 0)) == NULL)
		return;
	BlockMove(polyObj->Points, newPts, polyObj->NumPoints*sizeof(Point));
	if (closed)
		newPts[numPoints - 1] = polyObj->Points[0];
	for (i = 0; i < numPoints; i++) {
		OffsetPoint(&newPts[i], polyObj->DocObj.Frame.MinX, polyObj->DocObj.Frame.MinY);
		MapPt(&newPts[i], &polyObj->DocObj.Frame, rect);
	}
/*
	Get smooth point list
*/
	if (PolySmoothed(polyObj) &&
		(ptList = GetBezierPoints(newPts, numPoints, closed)) != NULL) {
		drawPts = ptList->Points;
		drawNum = ptList->NumPoints;
	}
	else {
		ptList  = NULL;
		drawPts = newPts;
		drawNum = numPoints;
	}
/*
	Fill the poly
*/
	if (hasFill) {
		if (fillPat == NULL)
			RGBForeColor(rPort, penColor);
		FillPoly(rPort, drawNum, drawPts, fillPat);
	}
/*
	Draw poly border
*/
	if (hasPen) {
		offset.x = -pen->x/2;
		offset.y = -pen->y/2;
		if (offset.x || offset.y) {
			for (i = 0; i < drawNum; i++)
				OffsetPoint(&drawPts[i], offset.x, offset.y);
		}
		PenNormal(rPort);
		RGBForeColor(rPort, penColor);
		FramePoly(rPort, drawNum, drawPts, pen->x, pen->y);
	}
/*
	Dispose of point list
*/
	if (ptList)
		DisposeBezierPoints(ptList);
	MemFree(newPts, numPoints*sizeof(Point));
}

void PolyDrawObj(RastPtr rPort, PolyObjPtr polyObj, RectPtr rect, RectPtr clipRect)
{
	Point pen;

	pen.x = polyObj->PenWidth;
	pen.y = polyObj->PenHeight;
	PolyDrawObj1(rPort, polyObj, rect, &pen, polyObj->PenColor, &polyObj->FillPat);
}

/*
 *	Draw poly outline (for dragging)
 *	Offset is in document coordinates
 */

void PolyDrawOutline(WindowPtr window, PolyObjPtr polyObj, WORD xOffset, WORD yOffset)
{
	WORD i, numPoints;
	BOOL closed;
	RastPtr rPort = window->RPort;
	PointPtr newPts;

	closed = PolyClosed(polyObj);
/*
	Create new point list in rPort coordinates
*/
	if ((numPoints = polyObj->NumPoints) < 2)
		return;
	if (closed)
		numPoints++;
	if ((newPts = MemAlloc(numPoints*sizeof(Point), 0)) == NULL)
		return;
	BlockMove(polyObj->Points, newPts, polyObj->NumPoints*sizeof(Point));
	if (closed)
		newPts[numPoints - 1] = polyObj->Points[0];
	for (i = 0; i < numPoints; i++) {
		OffsetPoint(&newPts[i], xOffset + polyObj->DocObj.Frame.MinX,
					yOffset + polyObj->DocObj.Frame.MinY);
		DocToWindow(window, newPts[i].x, newPts[i].y, &newPts[i].x, &newPts[i].y);
	}
/*
	Draw poly object
*/
	SetDrMd(rPort, COMPLEMENT);
	SetDrPt(rPort, LINE_PAT);
	FramePoly(rPort, numPoints, newPts, 1, 1);
/*
	Dispose of new point list
*/
	MemFree(newPts, numPoints*sizeof(Point));
}

/*
 *	Draw selection hilighting for poly
 */

void PolyHilite(WindowPtr window, PolyObjPtr polyObj)
{
	WORD i, numPoints;
	RastPtr rPort = window->RPort;
	Point pt, prevPt, windPt;

	numPoints = polyObj->NumPoints;
	prevPt = polyObj->Points[numPoints - 1];
	for (i = 0; i < numPoints; i++) {
		pt = polyObj->Points[i];
		if (pt.x != prevPt.x || pt.y != prevPt.y) {
			windPt = pt;
			OffsetPoint(&windPt, polyObj->DocObj.Frame.MinX, polyObj->DocObj.Frame.MinY);
			DocToWindow(window, windPt.x, windPt.y, &windPt.x, &windPt.y);
			DrawHandle(rPort, windPt.x, windPt.y);
		}
		prevPt = pt;
	}
}

/*
 *	Set poly pen color
 */

void PolySetPenColor(PolyObjPtr polyObj, RGBColor penColor)
{
	polyObj->PenColor = penColor;
}

/*
 *	Set poly pen size
 *	If size is -1 then do not change
 */

void PolySetPenSize(PolyObjPtr polyObj, WORD penWidth, WORD penHeight)
{
	if (penWidth != -1)
		polyObj->PenWidth = penWidth;
	if (penHeight != -1)
		polyObj->PenHeight = penHeight;
}

/*
 *	Set poly fill pattern
 */

void PolySetFillPat(PolyObjPtr polyObj, RGBPat8Ptr fillPat)
{
	CopyRGBPat8(fillPat, &polyObj->FillPat);
}

/*
 *	Rotate poly by given angle
 */

void PolyRotate(PolyObjPtr polyObj, WORD cx, WORD cy, WORD angle)
{
	WORD i, numPoints;

	UnSetPolyFrame(polyObj);
	numPoints = polyObj->NumPoints;
	for (i = 0; i < numPoints; i++)
		RotatePoint(&polyObj->Points[i], cx, cy, angle);
	SetPolyFrame(polyObj);
}

/*
 *	Flip poly horizontally or vertically
 */

void PolyFlip(PolyObjPtr polyObj, WORD cx, WORD cy, BOOL horiz)
{
	WORD i, numPoints;

	UnSetPolyFrame(polyObj);
	numPoints = polyObj->NumPoints;
	for (i = 0; i < numPoints; i++)
		FlipPoint(&polyObj->Points[i], cx, cy, horiz);
	SetPolyFrame(polyObj);
}

/*
 *	Scale poly to new frame
 */

void PolyScale(PolyObjPtr polyObj, RectPtr frame)
{
	WORD i;

	for (i = 0; i < polyObj->NumPoints; i++) {
		OffsetPoint(&polyObj->Points[i], polyObj->DocObj.Frame.MinX, polyObj->DocObj.Frame.MinY);
		MapPt(&polyObj->Points[i], &polyObj->DocObj.Frame, frame);
		OffsetPoint(&polyObj->Points[i], -frame->MinX, -frame->MinY);
	}
	polyObj->DocObj.Frame = *frame;
}

/*
 *	Return the line number the point is near, or -1 if not near any poly line
 *	Line number is point number of line start
 *	Only valid for non-smoothed polys
 */

static WORD PolyLineNumber(PolyObjPtr polyObj, PointPtr pt)
{
	WORD i, numPoints;

	numPoints = polyObj->NumPoints;
	for (i = 0; i < numPoints - 1; i++) {
		if (PtNearLine(pt, &polyObj->Points[i], &polyObj->Points[i + 1],
					   polyObj->PenWidth, polyObj->PenHeight, 2))
			return (i);
	}
	if (PolyClosed(polyObj)) {
		if (PtNearLine(pt, &polyObj->Points[numPoints - 1], &polyObj->Points[0],
					   polyObj->PenWidth, polyObj->PenHeight, 2))
			return (numPoints - 1);
	}
	return (-1);
}

/*
 *	Determine if point is in poly
 *	Point is relative to object rectangle
 */

BOOL PolySelect(PolyObjPtr polyObj, PointPtr pt)
{
	WORD x, y, xStart, xEnd, yStart, yEnd;
	BOOL select;
	RastPtr rPort;
	Point pen;
	Rectangle rect;

	rect = polyObj->DocObj.Frame;
	OffsetRect(&rect, -polyObj->DocObj.Frame.MinX, -polyObj->DocObj.Frame.MinY);
	if ((rPort = CreateRastPort(rect.MaxX + 1, rect.MaxY + 1, 1)) == NULL)
		return (TRUE);
	pen.x = pen.y = 1;			/* For faster drawing */
	PolyDrawObj1(rPort, polyObj, &rect, &pen, RGBCOLOR_BLACK, NULL);
	xStart = pt->x - polyObj->PenWidth/2 - 1;
	yStart = pt->y - polyObj->PenHeight/2 - 1;
	xEnd = xStart + polyObj->PenWidth + 2;
	yEnd = yStart + polyObj->PenHeight + 2;
	if (xStart < 0)
		xStart = 0;
	if (yStart < 0)
		yStart = 0;
	if (xEnd > rect.MaxX)
		xEnd = rect.MaxX;
	if (yEnd > rect.MaxY)
		yEnd = rect.MaxY;
	select = FALSE;
	for (x = xStart; x <= xEnd; x++) {
		for (y = yStart; y <= yEnd; y++) {
			if (ReadPixel(rPort, x, y) == 1) {
				select = TRUE;
				break;
			}
		}
		if (select)
			break;
	}
	DisposeRastPort(rPort);
	return (select);
}

/*
 *	Return handle number of given point, or -1 if not in a handle
 */

WORD PolyHandle(PolyObjPtr polyObj, PointPtr pt, RectPtr handleRect)
{
	WORD i;

	for (i = 0; i < polyObj->NumPoints; i++) {
		if (InHandle(pt, &polyObj->Points[i], handleRect))
			return (i);
	}
	return (-1);
}

/*
 *	Duplicate poly data to new object
 *	Return success status
 */

BOOL PolyDupData(PolyObjPtr polyObj, PolyObjPtr newObj)
{
	WORD numPoints = polyObj->NumPoints;

	newObj->PolyFlags	= polyObj->PolyFlags;
	newObj->PenWidth	= polyObj->PenWidth;
	newObj->PenHeight	= polyObj->PenHeight;
	newObj->PenColor	= polyObj->PenColor;
	CopyRGBPat8(&polyObj->FillPat, &newObj->FillPat);
	if ((newObj->Points = MemAlloc(numPoints*sizeof(Point), 0)) == NULL)
		return (FALSE);
	BlockMove(polyObj->Points, newObj->Points, numPoints*sizeof(Point));
	newObj->NumPoints = numPoints;
	return (TRUE);
}

/*
 *	Add point to poly object after specified point
 *	If after is -1, add to start of poly point list
 *	Return success status
 */

BOOL PolyAddPoint(PolyObjPtr polyObj, WORD after, WORD x, WORD y)
{
	WORD i, numPoints;
	PointPtr newPts;

	numPoints = polyObj->NumPoints;
	if (numPoints == 0x7FFF ||
		(newPts = MemAlloc((numPoints + 1)*sizeof(Point), 0)) == NULL)
		return (FALSE);
	if (after > numPoints - 1)
		after = numPoints - 1;
	for (i = 0; i <= after; i++)
		newPts[i] = polyObj->Points[i];
	newPts[i].x = x;
	newPts[i].y = y;
	while (i++ < numPoints)
		newPts[i] = polyObj->Points[i - 1];
	if (numPoints)
		MemFree(polyObj->Points, numPoints*sizeof(Point));
	polyObj->Points = newPts;
	polyObj->NumPoints = numPoints + 1;
	return (TRUE);
}

/*
 *	Remove the specified point from poly
 *	Will not reduce poly to fewer than two points
 */

void PolyRemPoint(PolyObjPtr polyObj, WORD ptNum)
{
	WORD i, numPoints;
	PointPtr newPts;

	numPoints = polyObj->NumPoints;
	if (ptNum >= numPoints || numPoints <= 2)
		return;
	if (numPoints == 1)
		newPts = NULL;
	else if (numPoints == 0 ||
		(newPts = MemAlloc((numPoints - 1)*sizeof(Point), 0)) == NULL)
		return;
	for (i = 0; i < ptNum; i++)
		newPts[i] = polyObj->Points[i];
	i++;
	while (i++ < numPoints)
		newPts[i - 2] = polyObj->Points[i - 1];
	if (numPoints)
		MemFree(polyObj->Points, numPoints*sizeof(Point));
	polyObj->Points = newPts;
	polyObj->NumPoints = numPoints - 1;
}

/*
 *	Add handle at given point if it is on a poly line
 *	Point is relative to poly frame
 *	Return TRUE if handle added
 */

BOOL PolyAddHandle(PolyObjPtr polyObj, PointPtr pt)
{
	WORD i;
	PolyObj tempPolyObj;

	if (PolySmoothed(polyObj))
		return (FALSE);
	tempPolyObj = *polyObj;
	EnableObjectPen((DocObjPtr) &tempPolyObj, TRUE);
	EnableObjectFill((DocObjPtr) &tempPolyObj, FALSE);
	if ((i = PolyLineNumber(&tempPolyObj, pt)) != -1) {
		PolyAddPoint(polyObj, i, pt->x, pt->y);
		return (TRUE);
	}
	return (FALSE);
}

/*
 *	Remove the handle at the given point
 *	Point is relative to poly frame
 */

void PolyRemHandle(PolyObjPtr polyObj, WORD handle)
{
	if (polyObj->NumPoints <= 2 || handle < 0 || handle >= polyObj->NumPoints)
		return;
	PolyRemPoint(polyObj, handle);
	PolyAdjustFrame(polyObj);
}

/*
 *	Set poly frame from point coordinates
 *	Also offset points to frame start
 */

static void SetPolyFrame(PolyObjPtr polyObj)
{
	WORD i, numPoints, x, y;
	Rectangle rect;

	if ((numPoints = polyObj->NumPoints) == 0)
		return;
	rect.MinX = rect.MaxX = polyObj->Points[0].x;
	rect.MinY = rect.MaxY = polyObj->Points[0].y;
	for (i = 1; i < numPoints; i++) {
		x = polyObj->Points[i].x;
		y = polyObj->Points[i].y;
		if (x < rect.MinX)
			rect.MinX = x;
		if (x > rect.MaxX)
			rect.MaxX = x;
		if (y < rect.MinY)
			rect.MinY = y;
		if (y > rect.MaxY)
			rect.MaxY = y;
	}
	polyObj->DocObj.Frame = rect;
	OffsetPoly(polyObj->NumPoints, polyObj->Points, -rect.MinX, -rect.MinY);
}

/*
 *	Offset all poly points by frame start
 */

static void UnSetPolyFrame(PolyObjPtr polyObj)
{
	OffsetPoly(polyObj->NumPoints, polyObj->Points,
			   polyObj->DocObj.Frame.MinX, polyObj->DocObj.Frame.MinY);
}

/*
 *	Adjust poly frame after making modifications to point list
 */

void PolyAdjustFrame(PolyObjPtr polyObj)
{
	UnSetPolyFrame(polyObj);
	SetPolyFrame(polyObj);
}

/*
 *	Set poly smoothing on or off
 */

void PolySetSmooth(PolyObjPtr polyObj, BOOL smooth)
{
	if (smooth)
		polyObj->PolyFlags |= POLY_SMOOTH;
	else
		polyObj->PolyFlags &= ~POLY_SMOOTH;
}

/*
 *	Set poly to closed on or off
 */

void PolySetClosed(PolyObjPtr polyObj, BOOL closed)
{
	if (closed)
		polyObj->PolyFlags |= POLY_CLOSED;
	else
		polyObj->PolyFlags &= ~POLY_CLOSED;
}

/*
 *	Remove redundant points on poly
 *	Only used after free-hand draw
 */

static void CollapsePoly(PolyObjPtr polyObj)
{
	WORD i;
	BOOL changed;

	do {
		changed = FALSE;
		for (i = 1; i < polyObj->NumPoints - 1; i++) {
			if (PtNearLine(&polyObj->Points[i], &polyObj->Points[i - 1], &polyObj->Points[i + 1],
						   0, 0, 1)) {
				PolyRemPoint(polyObj, i);
				changed = TRUE;
			}
		}
	} while (polyObj->NumPoints > 2 && changed);
}

/*
 *	Draw routine for tracking curve creation
 */

static void CurveTrackMouse(WindowPtr window, WORD xStart, WORD yStart, WORD xEnd, WORD yEnd,
							BOOL change)
{
	WORD x, y;
	RGBColor color;
	RastPtr rPort = window->RPort;
	Point prevPt;

	if (change) {
		prevPt = newPolyObj->Points[newPolyObj->NumPoints - 1];
		if (xEnd != prevPt.x || yEnd != prevPt.y) {
			PolyAddPoint(newPolyObj, newPolyObj->NumPoints, xEnd, yEnd);
			PenNormal(rPort);
			color = newPolyObj->PenColor;
			if (color == RGBCOLOR_WHITE)
				color = RGBCOLOR_BLACK;
			RGBForeColor(rPort, color);
			DocToWindow(window, prevPt.x, prevPt.y, &x, &y);
			Move(rPort, x, y);
			DocToWindow(window, xEnd, yEnd, &x, &y);
			Draw(rPort, x, y);
		}
	}
}

/*
 *	Create poly object from free-hand curve input
 */

DocObjPtr CurveCreate(WindowPtr window, UWORD modifier, WORD mouseX, WORD mouseY)
{
	WORD xStart, yStart, xEnd, yEnd, endPt;
	ULONG oldDocFlags;
	Point pt1, pt2;
	DocDataPtr docData = GetWRefCon(window);

/*
	Create new poly object to contain points on curve
*/
	if ((newPolyObj = CreatePoly(docData)) == NULL)
		return (NULL);
/*
	Track curve
*/
	WindowToDoc(window, mouseX, mouseY, &xStart, &yStart);
	SnapToGrid(docData, &xStart, &yStart);
	PolyAddPoint(newPolyObj, -1, xStart, yStart);
	xEnd = xStart;
	yEnd = yStart;
	ModifyIDCMP(window, window->IDCMPFlags | MOUSEMOVE);
	oldDocFlags = docData->Flags;
	docData->Flags &= ~DOC_GRIDSNAP;
	autoScroll = FALSE;
	TrackMouse(window, modifier, &xEnd, &yEnd, CurveTrackMouse, NULL);
	autoScroll = TRUE;
	docData->Flags = oldDocFlags;
	ModifyIDCMP(window, window->IDCMPFlags & ~MOUSEMOVE);
/*
	If final point is within 1 pixel of start, make it a closed poly
*/
	if ((endPt = newPolyObj->NumPoints - 1) > 1) {
		pt1 = newPolyObj->Points[0];
		pt2 = newPolyObj->Points[endPt];
		if (pt2.x <= pt1.x + 1 && pt2.x >= pt1.x - 1 &&
			pt2.y <= pt1.y + 1 && pt2.y >= pt1.y - 1) {
			PolyRemPoint(newPolyObj, endPt);
			newPolyObj->PolyFlags |= POLY_CLOSED;
		}
		else if (options.PolyAutoClose)
			newPolyObj->PolyFlags |= POLY_CLOSED;
	}
/*
	Set object data
*/
	newPolyObj->PolyFlags |= POLY_SMOOTH;
	SetPolyFrame(newPolyObj);
	InvalObjectRect(window, (DocObjPtr) newPolyObj);	/* Object rect may change */
	CollapsePoly(newPolyObj);
	PolyAdjustFrame(newPolyObj);
	return ((DocObjPtr) newPolyObj);
}

/*
 *	Draw routine for tracking poly creation
 *	Draw a solid or inverted line from start to end (in doc coordinates)
 */

static void PolyDrawLine(WindowPtr window, PointPtr start, PointPtr end,
						BOOL solid, RGBColor color)
{
	WORD x, y;
	RastPtr rPort = window->RPort;

	if (solid) {
		SetDrMd(rPort, JAM1);
		if (color == RGBCOLOR_WHITE)
			color = RGBCOLOR_BLACK;
		RGBForeColor(rPort, color);
	}
	else
		SetDrMd(rPort, COMPLEMENT);
	SetDrPt(rPort, LINE_PAT);
	DocToWindow(window, start->x, start->y, &x, &y);
	Move(rPort, x, y);
	DocToWindow(window, end->x, end->y, &x, &y);
	Draw(rPort, x, y);
}

/*
 *	Create poly object
 */

DocObjPtr PolyCreate(WindowPtr window, UWORD modifier, WORD mouseX, WORD mouseY)
{
	BOOL done, mouseDown;
	ULONG class;
	UWORD code;
	IntuiMsgPtr intuiMsg;
	Point pt1, pt2, newPt;
	DocDataPtr docData = GetWRefCon(window);

/*
	Create new poly object to contain points in poly
*/
	if ((newPolyObj = CreatePoly(docData)) == NULL)
		return (NULL);
/*
	Track poly
*/
	WindowToDoc(window, mouseX, mouseY, &pt1.x, &pt1.y);
	SnapToGrid(docData, &pt1.x, &pt1.y);
	PolyAddPoint(newPolyObj, -1, pt1.x, pt1.y);
	pt2 = pt1;
	PolyDrawLine(window, &pt1, &pt2, FALSE, 0);
	done = FALSE;
	mouseDown = TRUE;
	do {
		if (mouseDown)
			while (WaitMouseUp(mainMsgPort, window)) ;
		mouseDown = FALSE;
		while (!mouseDown && (intuiMsg = (IntuiMsgPtr) GetMsg(mainMsgPort)) != NULL) {
			class = intuiMsg->Class;
			code = intuiMsg->Code;
			modifier = intuiMsg->Qualifier;
			if (intuiMsg->IDCMPWindow != window ||
				(class != MOUSEMOVE && class != MOUSEBUTTONS &&
				 class != RAWKEY && class != INTUITICKS)) {
				PutMsg(mainMsgPort, (MsgPtr) intuiMsg);
				done = TRUE;
				break;
			}
			ReplyMsg((MsgPtr) intuiMsg);
			if (class == MOUSEBUTTONS && code == SELECTDOWN)
				mouseDown = TRUE;
			else if (class != INTUITICKS)
				mouseDown = SELECTBUTTON(modifier);
		}
		WindowToDoc(window, window->MouseX, window->MouseY, &newPt.x, &newPt.y);
		SnapToGrid(docData, &newPt.x, &newPt.y);
		if (newPt.x != pt2.x || newPt.y != pt2.y) {
			PolyDrawLine(window, &pt1, &pt2, FALSE, 0);
			pt2 = newPt;
			PolyDrawLine(window, &pt1, &pt2, FALSE, 0);
		}
		UpdateRulerIndic(window);
		if (mouseDown) {
			if (EqualPt(&pt1, &pt2))
				done = TRUE;
			else {
				PolyDrawLine(window, &pt1, &pt2, FALSE, 0);
				PolyDrawLine(window, &pt1, &pt2, TRUE, newPolyObj->PenColor);
				pt1 = pt2;
				PolyDrawLine(window, &pt1, &pt2, FALSE, 0);
				if (EqualPt(&pt2, &newPolyObj->Points[0])) {
					newPolyObj->PolyFlags |= POLY_CLOSED;
					done = TRUE;
				}
				else
					PolyAddPoint(newPolyObj, newPolyObj->NumPoints, pt2.x, pt2.y);
			}
		}
	} while (!done);
	PolyDrawLine(window, &pt1, &pt2, FALSE, 0);
/*
	Set object data
*/
Exit:
	newPolyObj->PolyFlags &= ~POLY_SMOOTH;
	if (options.PolyAutoClose)
		newPolyObj->PolyFlags |= POLY_CLOSED;
	SetPolyFrame(newPolyObj);
	return ((DocObjPtr) newPolyObj);
}

/*
 *	Draw routine for tracking poly shape change
 */

static void PolyTrackGrow(WindowPtr window, WORD xStart, WORD yStart, WORD xEnd, WORD yEnd,
						  BOOL change)
{
	WORD numPoints;
	BOOL closed;
	PolyObj polyObj;
	Point newPt, *pts, dragPts[3];

	closed = PolyClosed(growPolyObj);
	if (change) {
		polyObj = *growPolyObj;
		polyObj.PolyFlags &= ~(POLY_CLOSED | POLY_SMOOTH);
		polyObj.Points = dragPts;
		newPt.x = xEnd - growPolyObj->DocObj.Frame.MinX;
		newPt.y = yEnd - growPolyObj->DocObj.Frame.MinY;
		pts = growPolyObj->Points;
		numPoints = growPolyObj->NumPoints;
		if (growHandle == 0) {
			if (closed) {
				dragPts[0] = pts[numPoints - 1];
				dragPts[1] = newPt;
				dragPts[2] = pts[1];
				polyObj.NumPoints = 3;
			}
			else {
				dragPts[0] = newPt;
				dragPts[1] = pts[1];
				polyObj.NumPoints = 2;
			}
		}
		else if (growHandle == numPoints - 1) {
			dragPts[0] = pts[growHandle - 1];
			dragPts[1] = newPt;
			if (closed) {
				dragPts[2] = pts[0];
				polyObj.NumPoints = 3;
			}
			else
				polyObj.NumPoints = 2;
		}
		else {
			dragPts[0] = pts[growHandle - 1];
			dragPts[1] = newPt;
			dragPts[2] = pts[growHandle + 1];
			polyObj.NumPoints = 3;
		}
		PolyDrawOutline(window, &polyObj, 0, 0);
	}
}

/*
 *	Track mouse and change poly shape
 */

void PolyGrow(WindowPtr window, PolyObjPtr polyObj, UWORD modifier, WORD mouseX, WORD mouseY)
{
	WORD xStart, yStart, xEnd, yEnd;
	DocDataPtr docData = GetWRefCon(window);
	Point pt;
	Rectangle handleRect;

	growPolyObj = polyObj;
	GetHandleRect(window, &handleRect);
/*
	Track handle movement
*/
	WindowToDoc(window, mouseX, mouseY, &xStart, &yStart);
	pt.x = xStart - polyObj->DocObj.Frame.MinX;
	pt.y = yStart - polyObj->DocObj.Frame.MinY;
	if ((growHandle = PolyHandle(polyObj, &pt, &handleRect)) == -1)
		return;
	xEnd = xStart;
	yEnd = yStart;
	SnapToGrid(docData, &xEnd, &yEnd);
	TrackMouse(window, modifier, &xEnd, &yEnd, PolyTrackGrow, NULL);
/*
	Set new object dimensions
*/
	if (xEnd != xStart || yEnd != yStart) {
		InvalObjectRect(window, (DocObjPtr) polyObj);
		HiliteSelectOff(window);
		polyObj->Points[growHandle].x = xEnd - polyObj->DocObj.Frame.MinX;
		polyObj->Points[growHandle].y = yEnd - polyObj->DocObj.Frame.MinY;
		PolyAdjustFrame(polyObj);
		HiliteSelectOn(window);
		InvalObjectRect(window, (DocObjPtr) polyObj);
	}
}

/*
 *	Create new poly given REXX arguments
 */

PolyObjPtr PolyNewREXX(DocDataPtr docData, TextPtr args)
{
	PolyObjPtr polyObj;
	Point pt;

/*
	Create new poly
*/
	if ((polyObj = CreatePoly(docData)) == NULL)
		return (NULL);
/*
	Get poly points
*/
	for (;;) {
		args = GetArgPoint(args, docData, &pt);
		if (args == NULL)
			break;
		if (pt.x < 0 || pt.y < 0 || pt.x >= docData->DocWidth || pt.y >= docData->DocHeight) {
			DetachObject(CurrLayer(docData), (DocObjPtr) polyObj);
			PolyDispose(polyObj);
			return (NULL);
		}
		PolyAddPoint(polyObj, polyObj->NumPoints - 1, pt.x, pt.y);
	}
	SetPolyFrame(polyObj);
/*
	Return poly
*/
	if (polyObj->NumPoints < 2) {
		DetachObject(CurrLayer(docData), (DocObjPtr) polyObj);
		PolyDispose(polyObj);
		polyObj = NULL;
	}
	return (polyObj);
}
