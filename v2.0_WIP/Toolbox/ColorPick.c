/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Toolbox library
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Color picker routines
 */

#define INTUI_V36_NAMES_ONLY	1

#include <exec/types.h>
#include <graphics/gfxmacros.h>
#include <intuition/intuition.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>

#include <Toolbox/Globals.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Border.h>
#include <Toolbox/Image.h>
#include <Toolbox/Window.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/ColorPick.h>
#include <Toolbox/Screen.h>
#include <Toolbox/Utility.h>

/*
 *	External variables
 */

extern TextChar			_tbStrScreenColor[];

extern DialogTemplate	_tbColorPickDlgTempl, _tbScreenColorsDlgTempl;

/*
 *	Local variables and definitions
 */

enum {
	SAMPLE_USERITEM = 2,
	HUE_SLIDER,		BRIGHT_SLIDER,		SATURATE_SLIDER,
	REDARROW_UP,	REDARROW_DOWN,		RED_TEXT,
	GREENARROW_UP,	GREENARROW_DOWN,	GREEN_TEXT,
	BLUEARROW_UP,	BLUEARROW_DOWN,		BLUE_TEXT,
	PROMPT_TEXT
};

enum {
	COLORRESET_BTN = 2,
	CHANGEPEN_BTN,
	SCREENCOLORS_USERITEM
};

static DialogPtr	colorPickDlg, screenColorsDlg;
static BOOL			(*colorPickDlgFilter)(IntuiMsgPtr, WORD *);
static BOOL			(*screenColorsDlgFilter)(IntuiMsgPtr, WORD *);

#define MIN3(a,b,c)		(((a)<(b))?(((a)<(c))?(a):(c)):(((b)<(c))?(b):(c)))
#define MAX3(a,b,c)		(((a)>(b))?(((a)>(c))?(a):(c)):(((b)>(c))?(b):(c)))

/*
 *	Local prototypes
 */

static void	RGBtoHSV(UWORD, UWORD, UWORD, UWORD *, UWORD *, UWORD *);
static void	HSVtoRGB(UWORD, UWORD, UWORD, UWORD *, UWORD *, UWORD *);

static void	AdjustColorSliders(DialogPtr, WORD, WORD, WORD);
static void	GetColorSliders(DialogPtr, WORD *, WORD *, WORD *);
static void	ShowColorValues(DialogPtr, WORD, WORD, WORD);
static void	SetRGBColor(ScreenPtr, WORD, RGBColor, RGBColor (*)(RGBColor));
static BOOL	ColorPickDialogFilter(IntuiMsgPtr, WORD *);

static void	GetNumAcrossDown(WORD, WORD *, WORD *);
static void	DrawColorItem(DialogPtr, PenNum, BOOL);
static BOOL	ScreenColorsDialogFilter(IntuiMsgPtr, WORD *);

/*
 *	Convert from rgb values (in range 0-15)
 *		to hsv (h in range 0-0xFFFF; s, v in range 0-15)
 */

static void RGBtoHSV(UWORD r, UWORD g, UWORD b, UWORD *pH, UWORD *pS, UWORD *pV)
{
	register UWORD	v, s, m;
	register LONG	r1, g1, b1;
	LONG			h;

	v = MAX3(r, g, b);
	m = MIN3(r, g, b);
	s = (v - m)*15; s = (v) ? s/v : 0;
	if (s) {
		r1 = (v - r)*0x10000L; r1 /= (v - m);
		g1 = (v - g)*0x10000L; g1 /= (v - m);
		b1 = (v - b)*0x10000L; b1 /= (v - m);
		if (v == r)
			h = (m == g) ? 0x50000L + b1 : 0x10000L - g1;
		else if (v == g)
			h = (m == b) ? 0x10000L + r1 : 0x30000L - b1;
		else		/* v == b */
			h = (m == r) ? 0x30000L + g1 : 0x50000L - r1;
	}
	else
		h = 0;				/* Undefined hue */
	*pH = (h + 3)/6; *pS = s; *pV = v;
}

/*
 *	Convert hsv to rgb values
 */

static void HSVtoRGB(UWORD h, UWORD s, UWORD v, UWORD *pR, UWORD *pG, UWORD *pB)
{
	register UWORD	r, g, b;
	register LONG	p1, p2, p3;
	LONG			i, f;

	i = h*6; i /= 0x10000L;		/* Don't round */
	f = h*6 - i*0x10000L;
	p1 = v*(15 - s);						p1 = (p1 + 7)/15;
	p2 = v*(15*0x10000L - s*f);				p2 = (p2 + 15*0x8000L)/(15*0x10000L);
	p3 = v*(15*0x10000L - s*(0x10000L - f));p3 = (p3 + 15*0x8000L)/(15*0x10000L);
	switch (i) {
	case 0:
		r = v;  g = p3; b = p1;
		break;
	case 1:
		r = p2; g = v;  b = p1;
		break;
	case 2:
		r = p1; g = v;  b = p3;
		break;
	case 3:
		r = p1; g = p2; b = v;
		break;
	case 4:
		r = p3; g = p1; b = v;
		break;
	case 5:
		r = v;  g = p1; b = p2;
		break;
	}
	*pR = r; *pG = g; *pB = b;
}

/*
 *	Adjust Color and Brightness sliders to actual color
 */

static void AdjustColorSliders(DialogPtr dlg, WORD red, WORD green, WORD blue)
{
	WORD		hue, sat, value;	// Need to take address of these
	LONG		newPot, newBody;
	GadgetPtr	gadget, gadgList;
	PropInfoPtr	propInfo;

	gadgList = dlg->FirstGadget;
	RGBtoHSV(red, green, blue, &hue, &sat, &value);
/*
	Adjust color slider
*/
	gadget = GadgetItem(gadgList, HUE_SLIDER);
	propInfo = (PropInfoPtr) gadget->SpecialInfo;
	if (sat == 0) {
		newPot = 0xFFFF;
		newBody = 0xFFFF;
	}
	else {
		newPot = hue;
		newBody = 0x1111;
	}
	if (newPot != propInfo->HorizPot || newBody != propInfo->HorizBody)
		NewModifyProp(gadget, dlg, NULL, propInfo->Flags, newPot, 0, newBody, 0xFFFF, 1);
/*
	Adjust brightness slider
*/
	gadget = GadgetItem(gadgList, BRIGHT_SLIDER);
	propInfo = (PropInfoPtr) gadget->SpecialInfo;
	newPot = value*0xFFFFL;
	newPot /= 15;
	newBody = 0x1111;
	if (newPot != propInfo->HorizPot || newBody != propInfo->HorizBody)
		NewModifyProp(gadget, dlg, NULL, propInfo->Flags, newPot, 0, newBody, 0xFFFF, 1);
/*
	Adjust saturation slider
*/
	gadget = GadgetItem(gadgList, SATURATE_SLIDER);
	propInfo = (PropInfoPtr) gadget->SpecialInfo;
	newPot = sat*0xFFFFL;
	newPot /= 15;
	newBody = 0x1111;
 	if (newPot != propInfo->HorizPot || newBody != propInfo->HorizBody)
		NewModifyProp(gadget, dlg, NULL, propInfo->Flags, newPot, 0, newBody, 0xFFFF, 1);
}

/*
 *	Get color slider settings
 */

static void GetColorSliders(DialogPtr dlg, WORD *red, WORD *green, WORD *blue)
{
	WORD		hue, sat, value;
	GadgetPtr	gadget, gadgList;
	PropInfoPtr	propInfo;

	gadgList = dlg->FirstGadget;
/*
	Get new color values
*/
	gadget = GadgetItem(gadgList, HUE_SLIDER);
	propInfo = (PropInfoPtr) gadget->SpecialInfo;
	hue = propInfo->HorizPot;
	gadget = GadgetItem(gadgList, SATURATE_SLIDER);
	propInfo = (PropInfoPtr) gadget->SpecialInfo;
	sat = ((LONG) (propInfo->HorizPot)*15L + 0x7FFFL)/0xFFFFL;
	gadget = GadgetItem(gadgList, BRIGHT_SLIDER);
	propInfo = (PropInfoPtr) gadget->SpecialInfo;
	value = ((LONG) (propInfo->HorizPot)*15L + 0x7FFFL)/0xFFFFL;
	HSVtoRGB(hue, sat, value, red, green, blue);
}

/*
 *	Show new rgb values (and enable/disable arrows)
 */

static void ShowColorValues(DialogPtr dlg, WORD red, WORD green, WORD blue)
{
	register GadgetPtr	gadget, gadgList;
	TextChar			text[10];

	gadgList = dlg->FirstGadget;
	NumToString(red, text);
	gadget = GadgetItem(gadgList, RED_TEXT);
	SetGadgetText(gadget, dlg, NULL, text);
	NumToString(green, text);
	gadget = GadgetItem(gadgList, GREEN_TEXT);
	SetGadgetText(gadget, dlg, NULL, text);
	NumToString(blue, text);
	gadget = GadgetItem(gadgList, BLUE_TEXT);
	SetGadgetText(gadget, dlg, NULL, text);
	EnableGadgetItem(gadgList, REDARROW_UP, dlg, NULL, (red < 15));
	EnableGadgetItem(gadgList, REDARROW_DOWN, dlg, NULL, (red > 0));
	EnableGadgetItem(gadgList, GREENARROW_UP, dlg, NULL, (green < 15));
	EnableGadgetItem(gadgList, GREENARROW_DOWN, dlg, NULL, (green > 0));
	EnableGadgetItem(gadgList, BLUEARROW_UP, dlg, NULL, (blue < 15));
	EnableGadgetItem(gadgList, BLUEARROW_DOWN, dlg, NULL, (blue > 0));
}

/*
 *	Set specified color register to given RGB color
 *	Color given is color corrected
 */

static void SetRGBColor(ScreenPtr scrn, WORD colorNum, RGBColor rgbColor,
						RGBColor (*colorCorrect)(RGBColor))
{
	if (colorCorrect)
		rgbColor = (*colorCorrect)(rgbColor);
	SetRGB4(&scrn->ViewPort, colorNum, RED(rgbColor), GREEN(rgbColor), BLUE(rgbColor));
}

/*
 *	Color dialog filter
 */

static BOOL ColorPickDialogFilter(register IntuiMsgPtr intuiMsg, WORD *item)
{
	register WORD	itemHit;
	register ULONG	class = intuiMsg->Class;

	if (intuiMsg->IDCMPWindow == colorPickDlg &&
		(class == IDCMP_GADGETDOWN || class == IDCMP_GADGETUP)) {
		itemHit = GadgetNumber((GadgetPtr) intuiMsg->IAddress);
		if (itemHit == REDARROW_UP || itemHit == REDARROW_DOWN ||
			itemHit == GREENARROW_UP || itemHit == GREENARROW_DOWN ||
			itemHit == BLUEARROW_UP || itemHit == BLUEARROW_DOWN) {
			ReplyMsg((MsgPtr) intuiMsg);
			*item = (class == IDCMP_GADGETDOWN) ? itemHit : -1;
			return (TRUE);
		}
	}
	return ((colorPickDlgFilter) ? (*colorPickDlgFilter)(intuiMsg, item) : FALSE);
}

/*
 *	Get new color
 *	A penNum of -1 means to choose one
 *	If an actual penNum is specified, don't reset color on exit
 */

BOOL GetColor(ScreenPtr screen, MsgPortPtr msgPort,
			  BOOL (*dlgFilter)(IntuiMsgPtr, WORD *), TextPtr prompt,
			  RGBColor (*colorCorrect)(RGBColor),
			  RGBColor inColor, RGBColor *outColor, WORD penNum)
{
	WORD		item, numColors, red, green, blue;
	BOOL		done, hasPen;
	RGBColor	origColor, newColor;
	GadgetPtr	gadget, gadgList, hueGadg, briGadg, satGadg;
	RastPtr		rPort;
	Rectangle	rect;

	colorPickDlgFilter = dlgFilter;
	red = RED(inColor);
	green = GREEN(inColor);
	blue = BLUE(inColor);
	numColors = 1 << screen->RastPort.BitMap->Depth;
	if (penNum == -1) {
		hasPen = FALSE;
		penNum = (numColors >= 8) ? numColors - 3 : numColors - 1;
	}
	else
		hasPen = TRUE;
	origColor = GetRGB4(screen->ViewPort.ColorMap, penNum);
/*
	Get dialog
*/
	if ((colorPickDlg = GetDialog(&_tbColorPickDlgTempl, screen, msgPort)) == NULL)
		return (FALSE);
	gadgList = colorPickDlg->FirstGadget;
	OutlineButton(GadgetItem(gadgList, OK_BUTTON), colorPickDlg, NULL, TRUE);
	SetRGBColor(screen, penNum, inColor, colorCorrect);
/*
	Draw dialog contents
*/
	rPort = colorPickDlg->RPort;
	gadget = GadgetItem(gadgList, PROMPT_TEXT);
	SetGadgetText(gadget, colorPickDlg, NULL, prompt);
	gadget = GadgetItem(gadgList, SAMPLE_USERITEM);
	GetGadgetRect(gadget, colorPickDlg, NULL, &rect);
	InsetRect(&rect, 2, 2);
	SetAPen(rPort, penNum);
	FillRect(rPort, &rect);
	ShowColorValues(colorPickDlg, red, green, blue);
	AdjustColorSliders(colorPickDlg, red, green, blue);
	hueGadg = GadgetItem(gadgList, HUE_SLIDER);
	briGadg = GadgetItem(gadgList, BRIGHT_SLIDER);
	satGadg = GadgetItem(gadgList, SATURATE_SLIDER);
	GetGadgetRect(hueGadg, colorPickDlg, NULL, &rect);
	DrawShadowBox(rPort, &rect, -2, TRUE);
	GetGadgetRect(briGadg, colorPickDlg, NULL, &rect);
	DrawShadowBox(rPort, &rect, -2, TRUE);
	GetGadgetRect(satGadg, colorPickDlg, NULL, &rect);
	DrawShadowBox(rPort, &rect, -2, TRUE);
/*
	Handle requester
*/
	done = FALSE;
	do {
		WaitPort(msgPort);
		item = CheckDialog(msgPort, colorPickDlg, ColorPickDialogFilter);
		switch (item) {
		case OK_BUTTON:
		case CANCEL_BUTTON:
			done = TRUE;
			break;
		case HUE_SLIDER:
		case BRIGHT_SLIDER:
		case SATURATE_SLIDER:
			GetColorSliders(colorPickDlg, &red, &green, &blue);
			break;
		case REDARROW_UP:
		case REDARROW_DOWN:
			if (item == REDARROW_UP) {
				if (red < 15)
					red++;
			}
			else {
				if (red > 0)
					red--;
			}
			break;
		case GREENARROW_UP:
		case GREENARROW_DOWN:
			if (item == GREENARROW_UP) {
				if (green < 15)
					green++;
			}
			else {
				if (green > 0)
					green--;
			}
			break;
		case BLUEARROW_UP:
		case BLUEARROW_DOWN:
			if (item == BLUEARROW_UP) {
				if (blue < 15)
					blue++;
			}
			else {
				if (blue > 0)
					blue--;
			}
			break;
		}
		*outColor = RGBCOLOR(red, green, blue);
		if (item != -1) {
			SetRGBColor(screen, penNum, *outColor, colorCorrect);
			ShowColorValues(colorPickDlg, red, green, blue);
			AdjustColorSliders(colorPickDlg, red, green, blue);
		}
		else if ((hueGadg->Flags & GFLG_SELECTED) || (briGadg->Flags & GFLG_SELECTED) ||
				 (satGadg->Flags & GFLG_SELECTED)) {
			GetColorSliders(colorPickDlg, &red, &green, &blue);
			if ((newColor = RGBCOLOR(red, green, blue)) != *outColor) {
				*outColor = newColor;
				SetRGBColor(screen, penNum, *outColor, colorCorrect);
				ShowColorValues(colorPickDlg, red, green, blue);
			}
		}
	} while (!done);
	if (item == CANCEL_BUTTON || !hasPen)
		SetRGB4(&screen->ViewPort, penNum, RED(origColor), GREEN(origColor), BLUE(origColor));
	DisposeDialog(colorPickDlg);
	return (item == OK_BUTTON);
}

/*
 *	Get number of colors across and down in dialog
 */

static void GetNumAcrossDown(WORD numColors, WORD *numAcross, WORD *numDown)
{
	if (numColors == 256)
		*numDown = 16;
	else if (numColors > 64)
		*numDown = 8;
	else if (numColors > 16)
		*numDown = 4;
	else if (numColors > 4)
		*numDown = 2;
	else
		*numDown = 1;
	*numAcross = numColors/(*numDown);
}

/*
 *	Draw specified color item in dialog, hiliting if seleted
 */

static void DrawColorItem(DialogPtr dlg, PenNum penNum, BOOL hilite)
{
	WORD		x, y, width, height;
	WORD		numPens, numAcross, numDown;
	RastPtr		rPort;
	GadgetPtr	gadget;
	Rectangle	rect;

	numPens = 1 << dlg->WScreen->RastPort.BitMap->Depth;
	GetNumAcrossDown(numPens, &numAcross, &numDown);
	rPort = dlg->RPort;
	gadget = GadgetItem(dlg->FirstGadget, SCREENCOLORS_USERITEM);
	GetGadgetRect(gadget, dlg, NULL, &rect);
	width = rect.MaxX - rect.MinX + 1;
	height = rect.MaxY - rect.MinY + 1;
	x = rect.MinX + (penNum % numAcross)*width/numAcross;
	y = rect.MinY + (penNum/numAcross)*height/numDown;
	SetRect(&rect, x, y, x + width/numAcross - 1, y + height/numDown - 1);
	if (hilite) {
		DrawShadowBox(rPort, &rect, 0, FALSE);
		InsetRect(&rect, 1, 1);
		SetAPen(rPort, _tbPenDark);
		FrameRect(rPort, &rect);
	}
	else {
		SetAPen(rPort, _tbPenLight);
		FrameRect(rPort, &rect);
	}
	InsetRect(&rect, 1, 1);
	SetAPen(rPort, penNum);
	FillRect(rPort, &rect);
}

/*
 *	Screen colors dialog filter
 */

static BOOL ScreenColorsDialogFilter(register IntuiMsgPtr intuiMsg, WORD *item)
{
	register WORD	itemHit;
	register ULONG	class = intuiMsg->Class;

	if (intuiMsg->IDCMPWindow == screenColorsDlg &&
		(class == IDCMP_GADGETDOWN || class == IDCMP_GADGETUP)) {
		itemHit = GadgetNumber((GadgetPtr) intuiMsg->IAddress);
		if (itemHit == SCREENCOLORS_USERITEM) {
			ReplyMsg((MsgPtr) intuiMsg);
			*item = (class == IDCMP_GADGETDOWN) ? itemHit : -1;
			return (TRUE);
		}
	}
	return ((screenColorsDlgFilter) ? (*screenColorsDlgFilter)(intuiMsg, item) : FALSE);
}

/*
 *	Have user adjust screen colors
 */

BOOL AdjustScreenColors(ScreenPtr screen, MsgPortPtr msgPort,
						BOOL (*dlgFilter)(IntuiMsgPtr, WORD *))
{
	WORD		i, item, numPens;
	WORD		x, y, width, height, numAcross, numDown;
	BOOL		done, doubleClick;
	ULONG		secs, micros, prevSecs, prevMicros;
	PenNum		selPen;
	GadgetPtr	gadget;
	RastPtr		rPort;
	Rectangle	rect;
	RGBColor	color, newColor, prevColors[256];

	screenColorsDlgFilter = dlgFilter;
	numPens = GetColorTable(screen, &prevColors);
/*
	Bring up dialog
*/
	GetNumAcrossDown(numPens, &numAcross, &numDown);
	i = (numDown < 4) ? 20 : 15;
	height = i*numDown + 75;
	if (height < 100)
		height = 100;
	_tbScreenColorsDlgTempl.Height = height;
	if ((screenColorsDlg = GetDialog(&_tbScreenColorsDlgTempl, screen, msgPort)) == NULL)
		return (FALSE);
	OutlineButton(GadgetItem(screenColorsDlg->FirstGadget, 0), screenColorsDlg, NULL, TRUE);
	rPort = screenColorsDlg->RPort;
	selPen = 0;
/*
	Draw dark border around pens and draw pen colors
*/
	gadget = GadgetItem(screenColorsDlg->FirstGadget, SCREENCOLORS_USERITEM);
	GetGadgetRect(gadget, screenColorsDlg, NULL, &rect);
	width = rect.MaxX - rect.MinX + 1;
	height = rect.MaxY - rect.MinY + 1;
	DrawShadowBox(rPort, &rect, -1, TRUE);
	for (i = 0; i < numPens; i++)
		DrawColorItem(screenColorsDlg, i, (i == selPen));
/*
	Handle dialog
*/
	prevSecs = prevMicros = 0;
	done = FALSE;
	do {
		item = ModalDialog(msgPort, screenColorsDlg, ScreenColorsDialogFilter);
		CurrentTime(&secs, &micros);
		switch (item) {
		case OK_BUTTON:
		case CANCEL_BUTTON:
			done = TRUE;
			break;
		case COLORRESET_BTN:
			LoadRGB4(&screen->ViewPort, prevColors, numPens);
			break;
		case SCREENCOLORS_USERITEM:
			x = (screenColorsDlg->MouseX - rect.MinX)*numAcross/width;
			y = (screenColorsDlg->MouseY - rect.MinY)*numDown/height;
			if (x < 0 || x >= numAcross || y < 0 || y >= numDown)
				break;
			i = x + y*numAcross;
			if (i != selPen) {
				DrawColorItem(screenColorsDlg, selPen, FALSE);
				selPen = i;
				DrawColorItem(screenColorsDlg, selPen, TRUE);
				doubleClick = FALSE;
			}
			else
				doubleClick = DoubleClick(prevSecs, prevMicros, secs, micros);
			if (!doubleClick) {
				prevSecs = secs;
				prevMicros = micros;
				break;
			}
			item = CHANGEPEN_BTN;		/* Fall through */
		case CHANGEPEN_BTN:
			color = GetRGB4(screen->ViewPort.ColorMap, selPen);
			GetColor(screen, msgPort, dlgFilter, _tbStrScreenColor, NULL, color,
					 &newColor, selPen);
			break;
		}
	} while (!done);
	DisposeDialog(screenColorsDlg);
	if (item == CANCEL_BUTTON)
		LoadRGB4(&screen->ViewPort, prevColors, numPens);
	return ((item == OK_BUTTON));
}

/*
 *	Convert RGB color to CMYK color
 */

CMYKColor RGBtoCMYK(RGBColor rgbColor)
{
	register WORD	c, m, y, k;

	c = 0x0F - RED(rgbColor);
	m = 0x0F - GREEN(rgbColor);
	y = 0x0F - BLUE(rgbColor);
	k = MIN3(c, m, y);
	c -= k;
	m -= k;
	y -= k;
	return (CMYKCOLOR(c,m,y,k));
}

/*
 *	Convert CMYK color to RGB color
 */

RGBColor CMYKtoRGB(CMYKColor cmykColor)
{
	register WORD	c, m, y, k;

	k = CMYK_BLACK(cmykColor);
	c = CMYK_CYAN(cmykColor) + k;
	if (c > 0x0F)
		c = 0x0F;
	m = CMYK_MAGENTA(cmykColor) + k;
	if (m > 0x0F)
		m = 0x0F;
	y = CMYK_YELLOW(cmykColor) + k;
	if (y > 0x0F)
		y = 0x0F;
	return (RGBCOLOR((0x0F - c), (0x0F - m), (0x0F - y)));
/*
	register WORD	r, g, b, w;

	w = (0x0F - CMYK_BLACK(cmykColor));
	r = (0x0F - CMYK_CYAN(cmykColor))*w/0x0F;
	g = (0x0F - CMYK_MAGENTA(cmykColor))*w/0x0F;
	b = (0x0F - CMYK_YELLOW(cmykColor))*w/0x0F;
	return (RGBCOLOR(r, g, b));
*/
}
