/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Toolbox library
 *	Copyright (c) 1989 New Horizons Software, Inc.
 *
 *	Simple linked list routines
 */

#include <exec/types.h>

#include <string.h>

#include <Typedefs.h>

#include <Toolbox/Memory.h>
#include <Toolbox/List.h>
#include <Toolbox/Utility.h>

/*
 *	Create list header and return pointer to it
 */

ListHeadPtr CreateList()
{
	return (MemAlloc(sizeof(ListHead), MEMF_CLEAR));
}

/*
 *	Dispose of all list entries and of list
 */

void DisposeList(ListHeadPtr list)
{
	if (list) {
		DisposeListItems(list);
		MemFree(list, sizeof(ListHead));
	}
}

/*
 *	Create new list item
 *	If length is zero then assume text is NULL terminated string
 *	Return pointer to list item, or NULL
 */

ListItemPtr CreateListItem(TextPtr text, WORD len)
{
	register ListItemPtr	newItem;

	if (len == 0)
		len = strlen(text);
	if ((newItem = MemAlloc(sizeof(ListItem) + len, 0)) == NULL)
		return (NULL);
	memcpy(newItem->Text, text, len);
	newItem->Text[len] = '\0';
	return (newItem);
}

/*
 *	Insert list item into list after given item
 */

void AddToList(ListHeadPtr list, register ListItemPtr prevItem, register ListItemPtr newItem)
{
	if (prevItem) {
		newItem->Next = prevItem->Next;
		prevItem->Next = newItem;
	}
	else {
		newItem->Next = list->First;
		list->First = newItem;
	}
	if (newItem->Next == NULL)
		list->Last = newItem;
}

/*
 *	Remove item from list
 */

void RemoveFromList(ListHeadPtr list, register ListItemPtr prevItem, register ListItemPtr item)
{
	if (prevItem)
		prevItem->Next = item->Next;
	else
		list->First = item->Next;
	if (item->Next == NULL)
		list->Last = prevItem;
}

/*
 *	Add text item of given length to list
 *	If length is zero then assume text is NULL terminated string
 *	If sort is TRUE then sort into list, if FALSE then add to start of list
 *	Return item number, or -1 if not successful
 */

LONG AddListItem(ListHeadPtr list, TextPtr text, register WORD len, BOOL sort)
{
	register LONG			num;
	register ListItemPtr	item, prevItem;
	ListItemPtr				newItem;

	if (len == 0)
		len = strlen(text);				// Needed when sorting
	if (list == NULL || (newItem = CreateListItem(text, len)) == NULL)
		return (-1);
	prevItem = NULL;
	num = 0;
	if (sort) {
		item = list->First;
		while (item && CmpString(text, item->Text, len, strlen(item->Text), FALSE) > 0) {
			prevItem = item;
			item = item->Next;
			num++;
		}
	}
	AddToList(list, prevItem, newItem);
	return (num);
}

/*
 *	Insert list item before specified entry number
 *	Return success status
 */

BOOL InsertListItem(ListHeadPtr list, TextPtr text, WORD len, register LONG num)
{
	register ListItemPtr	item, prevItem, newItem;

	if (list == NULL || (newItem = CreateListItem(text, len)) == NULL)
		return (FALSE);
	item = list->First;
	prevItem = NULL;
	while (item && num--) {
		prevItem = item;
		item = item->Next;
	}
	AddToList(list, prevItem, newItem);
	return (TRUE);
}

/*
 *	Remove list item and free its memory (first = 0)
 */

void RemoveListItem(ListHeadPtr list, register LONG num)
{
	register ListItemPtr	item, prevItem;

	if (list) {
		item = list->First;
		prevItem = NULL;
		while (item && num--) {
			prevItem = item;
			item = item->Next;
		}
		if (item) {
			RemoveFromList(list, prevItem, item);
			MemFree(item, sizeof(ListItem) + strlen(item->Text));
		}
	}
}

/*
 *	Dispose of all list entries
 */

void DisposeListItems(ListHeadPtr list)
{
	register ListItemPtr	item, nextItem;

	if (list) {
		for (item = list->First; item; item = nextItem) {
			nextItem = item->Next;
			MemFree(item, sizeof(ListItem) + strlen(item->Text));
		}
		list->First = list->Last = NULL;
	}
}

/*
 *	Return number of entries in list
 */

LONG NumListItems(ListHeadPtr list)
{
	register LONG			num;
	register ListItemPtr	item;

	num = 0;
	if (list) {
		for (item = list->First; item; item = item->Next)
			num++;
	}
	return (num);
}

/*
 *	Return pointer to text of given item (first = 0)
 */

TextPtr GetListItem(ListHeadPtr list, register LONG num)
{
	register ListItemPtr	item;

	if (list == NULL)
		return (NULL);
	item = list->First;
	while (item && num--)
		item = item->Next;
	return ((item) ? (item->Text) : NULL);
}

/*
 *	Return item number of given text entry, or -1 if not found
 *	Match is not case sensitive
 */

LONG FindListItem(ListHeadPtr list, register TextPtr text)
{
	WORD					len;
	register LONG			num;
	register TextPtr		itemText;
	register ListItemPtr	item;

	if (list == NULL)
		return (-1);
	len = strlen(text);
	num = 0;
	for (item = list->First; item; item = item->Next) {
		itemText = item->Text;
		if (CmpString(text, itemText, len, strlen(itemText), FALSE) == 0)
			return (num);
		num++;
	}
	return (-1);
}
