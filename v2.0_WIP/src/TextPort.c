/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	TextPort routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <proto/graphics.h>

#include <Toolbox/Memory.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Font.h>

#include "Draw.h"
#include "Proto.h"

/*
 *	Local prototypes
 */

static void	Text1(RastPtr, TextPtr, WORD);

/*
 *	Set text font
 */

void TPTextFont(TextPortPtr textPort, FontNum fontNum)
{
	textPort->FontNum = fontNum;
}

/*
 *	Set text size
 */

void TPTextSize(TextPortPtr textPort, FontSize fontSize)
{
	textPort->FontSize = fontSize;
}

/*
 *	Set text style
 */

void TPTextFace(TextPortPtr textPort, WORD style, WORD miscStyle)
{
	textPort->Style = style;
	textPort->MiscStyle = miscStyle;
}

/*
 *	Determine text width
 */

WORD TPTextWidth(register TextPortPtr textPort, TextPtr text, WORD loc, WORD len)
{
	WORD				width, oldExtra;
	FontSize			fontSize;
	register RastPtr	rPort = textPort->RastPort;
	TextFontPtr			font, oldFont;

	fontSize = FontScaleSize(textPort->FontNum, textPort->FontSize);
	if ((font = GetNumFont(textPort->FontNum, fontSize)) == NULL)
		return (0);
	oldFont = rPort->Font;
	SetFont(rPort, font);
	SetSoftStyle(rPort, textPort->Style, 0xFF);
	oldExtra = rPort->TxSpacing;
	if (textPort->MiscStyle & MISCSTYLE_CONDENSED)
		rPort->TxSpacing--;
	if (textPort->MiscStyle & MISCSTYLE_EXPANDED)
		rPort->TxSpacing++;
	if (fontSize == textPort->FontSize)
		width = TextLength(rPort, text + loc, len);
	else {
		width = 0;
	}
	rPort->TxSpacing = oldExtra;
	SetFont(rPort, oldFont);
	CloseFont(font);
	return (width);
}

/*
 *	Draw text
 */

void TPDrawText(register TextPortPtr textPort, TextPtr text, WORD loc, WORD len)
{
	WORD				oldExtra;
	FontSize			fontSize;
	register RastPtr	rPort = textPort->RastPort;
	TextFontPtr			font, oldFont;

	fontSize = FontScaleSize(textPort->FontNum, textPort->FontSize);
	if ((font = GetNumFont(textPort->FontNum, fontSize)) == NULL)
		return;
	oldFont = rPort->Font;
	SetFont(rPort, font);
	SetSoftStyle(rPort, textPort->Style, 0xFF);
	oldExtra = rPort->TxSpacing;
	if (textPort->MiscStyle & MISCSTYLE_CONDENSED)
		rPort->TxSpacing--;
	if (textPort->MiscStyle & MISCSTYLE_EXPANDED)
		rPort->TxSpacing++;
	if (fontSize == textPort->FontSize)
		Text1(rPort, text + loc, len);
	else {
	}
	rPort->TxSpacing = oldExtra;
	SetFont(rPort, oldFont);
	CloseFont(font);
}

/*
 *	Get font info
 */

void TPGetFontInfo(register TextPortPtr textPort, register FontInfoPtr fontInfo)
{
	FontSize				fontSize;
	register TextFontPtr	font;

	fontInfo->Ascent = fontInfo->Descent = fontInfo->WidMax = fontInfo->Leading = 0;
	fontSize = FontScaleSize(textPort->FontNum, textPort->FontSize);
	if ((font = GetNumFont(textPort->FontNum, fontSize)) == NULL)
		return;
	if (fontSize == textPort->FontSize) {
		fontInfo->Ascent = font->tf_Baseline;
		fontInfo->Descent = font->tf_YSize - font->tf_Baseline;
		fontInfo->WidMax = font->tf_XSize;
	}
	else {
	}
	CloseFont(font);
}

/*
 *	Replacement for Text() due to bugs in OS prior to version 2.0
 */

static void Text1(register RastPtr rPort, register TextPtr text, register WORD len)
{
	register WORD	cp_x, chLen, xSize, subLen;

/*
	If new OS, let system do the work
*/
	if (AdvancedGraphics())
		Text(rPort, text, len);
/*
	Handle problem of not rendering full text with negative x offsets
*/
	else {
		cp_x = rPort->cp_x;
		if (cp_x < 0) {
			while (len) {
				chLen = TextLength(rPort, text, 1);
				if (cp_x + chLen > 0)
					break;
				cp_x += chLen;
				text++;
				len--;
			}
			Move(rPort, cp_x, rPort->cp_y);
		}
/*
	Handle problem of not drawing text wider than MAX_BLIT_SIZE pixels
*/
		if (len) {
			xSize = rPort->Font->tf_XSize;
			while (len*xSize > MAX_BLIT_SIZE) {
				subLen = MAX_BLIT_SIZE/xSize;
				Text(rPort, text, subLen);
				text += subLen;
				len -= subLen;
			}
			Text(rPort, text, len);
		}
	}
	WaitBlit();
}
