/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Edit menu routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <proto/exec.h>
#include <proto/intuition.h>
#include <proto/dos.h>				/* For Delay() prototype */

#include <string.h>

#include <Toolbox/Graphics.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Window.h>
#include <Toolbox/Utility.h>

#include "Draw.h"
#include "Proto.h"

/*
 *	External variables
 */

extern MsgPortPtr	mainMsgPort;
extern ScreenPtr	screen;

extern DocLayer	pasteLayer;

extern WORD	drawTool;

extern DlgTemplPtr	dlgList[];

extern struct ClipboardHandle *clipboard;
extern struct Library			*IFFParseBase;

extern Options options;

/*
 *	Local variables and definitions
 */

#define DUP_XOFFSET	10
#define DUP_YOFFSET	9

enum {
	WIDTH_TEXT = 2,		HEIGHT_TEXT
};

#define ANGLE_TEXT	2

static DialogPtr	scaleDlg;

/*
 *	Local prototypes
 */

static void	AdjustOffsets(WindowPtr);

static BOOL	DoPaste(WindowPtr);
static BOOL	DoDuplicate(WindowPtr);
static BOOL	DoFlip(WindowPtr, BOOL);
static BOOL	DoRotate(WindowPtr);
static void	SetScaleText(DialogPtr, WORD, WORD);
static BOOL	DoScale(WindowPtr);
static BOOL	DoConvertToPoly(WindowPtr);
static BOOL	DoPolyClose(WindowPtr, BOOL);
static BOOL	DoSmooth(WindowPtr, BOOL);
static BOOL	DoMerge(WindowPtr, BOOL);
static BOOL	DoSelectAll(WindowPtr);

/*
 *	Adjust offsets of selected objects so group is within document bounds
 */

static void AdjustOffsets(WindowPtr window)
{
	WORD		xOffset, yOffset;
	DocObjPtr	docObj;
	DocDataPtr	docData = GetWRefCon(window);
	Rectangle	rect, origRect;

	if (!GetSelectRect(docData, &rect))
		return;
	origRect = rect;
	AdjustToDocBounds(docData, &rect);
	xOffset = rect.MinX - origRect.MinX;
	yOffset = rect.MinY - origRect.MinY;
	if (xOffset != 0 || yOffset != 0) {
		for (docObj = FirstSelected(docData); docObj; docObj = NextSelected(docObj))
			OffsetObject(docObj, xOffset, yOffset);
	}
}

/*
 *	Turn undo off
 */

void UndoOff()
{
}

/*
 *	Clear paste buffer
 */

void ClearPaste()
{
	if (BottomObject(&pasteLayer)) {
		DisposeAllDocObjects(BottomObject(&pasteLayer));
		pasteLayer.Objects = NULL;
	}
}

/*
 *	Cut objects from document
 */

BOOL DoCut(WindowPtr window, BOOL doCopy, BOOL doClear)
{
	BOOL success;
	DocObjPtr docObj, newObj, nextObj;
	TextObjPtr textObj, newTextObj;
	DocDataPtr docData = GetWRefCon(window);

	if (doClear && ObjectsLocked(docData)) {
		Error(ERR_OBJ_LOCKED);
		return (FALSE);
	}
	if (doCopy)
		ClearPaste();
	success = FALSE;
	docObj = FirstSelected(docData);
/*
	Handle cut/copy/clear of selected text
*/
	if (TextInEdit(window)) {
		textObj = (TextObjPtr) docObj;
		if (doCopy) {
			if ((newTextObj = (TextObjPtr) DuplicateObject(&pasteLayer, (DocObjPtr) textObj)) == NULL) {
				Error(ERR_NO_MEM);
				success = FALSE;
				goto Exit;
			}
			UnSelectObject((DocObjPtr) newTextObj);
			newTextObj->SelStart = textObj->SelEnd;
			newTextObj->SelEnd   = textObj->TextLen;
			(void) TextAddChars(newTextObj, NULL, 0);
			newTextObj->SelStart = 0;
			newTextObj->SelEnd   = textObj->SelStart;
			(void) TextAddChars(newTextObj, NULL, 0);
			TextAdjustFrame(newTextObj);
		}
		if (doClear)
			TextDelete(window, (TextChar) 0, 0);
		success = TRUE;
	}
/*
	Handle cut/copy/clear of selected objects
*/
	else {
		HiliteSelectOff(window);
		while (docObj) {
			nextObj = NextSelected(docObj);
			if (doCopy) {
				if ((newObj = DuplicateObject(&pasteLayer, docObj)) == NULL) {
					Error(ERR_NO_MEM);
					success = FALSE;
					break;
				}
				UnSelectObject(newObj);
			}
			if (doClear) {
				InvalObjectRect(window, docObj);
				DetachObject(CurrLayer(docData), docObj);
				DisposeDocObject(docObj);
				DocModified(docData);
			}
			success = TRUE;
			docObj = nextObj;
		}
		HiliteSelectOn(window);
	}
	if (success && doCopy) {
		SetBusyPointer(window);
		PutPost(window);
		SetPointerShape();
	}
Exit:
	SetAllMenus();
	return (success);
}

/*
 *	Paste objects into document
 */

static BOOL DoPaste(WindowPtr window)
{
	WORD x, y;
	BOOL success, found;
	DocObjPtr docObj, newObj;
	TextObjPtr textObj, newTextObj;
	DocDataPtr docData = GetWRefCon(window);
	Rectangle rect, origRect, contRect;

	if (IFFParseBase == NULL && options.FullClipboard) {
		if ((IFFParseBase = (struct IFFParseBase *)
				OpenLibrary("iffparse.library",OSVERSION_2_0_4)) == NULL) {
			Error(ERR_NO_PARSE);
		}
	}
	else if (clipboard == NULL && options.FullClipboard) {
		InitClipboard();
	}
	if (clipboard)
		GetClipboard(window);

	if (BottomObject(&pasteLayer) == NULL)
		return (FALSE);
/*
	Handle text paste
*/
	if (TextInEdit(window) && (docObj = BottomObject(&pasteLayer)) != NULL &&
		docObj->Type == TYPE_TEXT && NextObj(docObj) == NULL) {
		success = FALSE;
		textObj = (TextObjPtr) FirstSelected(docData);
		newTextObj = (TextObjPtr) docObj;
		HiliteSelectOff(window);
		if (TextAddChars(textObj, newTextObj->Text, newTextObj->TextLen)) {
			success = TRUE;
			InvalObjectRect(window, (DocObjPtr) textObj);
			TextAdjustFrame(textObj);
			InvalObjectRect(window, (DocObjPtr) textObj);
		}
		HiliteSelectOn(window);
		return (success);
	}

/*
	First get containing rect of paste objects
*/
	found = FALSE;
	for (docObj = BottomObject(&pasteLayer); docObj; docObj = NextObj(docObj)) {
		if (!found)
			rect = docObj->Frame;
		else
			UnionRect(&docObj->Frame, &rect, &rect);
		found = TRUE;
	}
/*
	Center rect in window
*/
	origRect = rect;
	DocToWindowRect(window, &rect, &rect);
	GetContentRect(window, &contRect);
	x = ((contRect.MaxX - contRect.MinX) - (rect.MaxX - rect.MinX))/2 + contRect.MinX;
	y = ((contRect.MaxY - contRect.MinY) - (rect.MaxY - rect.MinY))/2 + contRect.MinY;
	OffsetRect(&rect, x - rect.MinX, y - rect.MinY);
	WindowToDocRect(window, &rect, &rect);
	AdjustToDocBounds(docData, &rect);
/*
	Paste objects and offset to new position
*/
	HiliteSelectOff(window);
	UnSelectAllObjects(docData);
	success = TRUE;
	for (docObj = BottomObject(&pasteLayer); docObj; docObj = NextObj(docObj)) {
		if ((newObj = DuplicateObject(CurrLayer(docData), docObj)) == NULL) {
			Error(ERR_NO_MEM);
			success = FALSE;
			break;
		}
		SelectObject(newObj);
		OffsetObject(newObj, rect.MinX - origRect.MinX, rect.MinY - origRect.MinY);
		InvalObjectRect(window, newObj);
	}
	HiliteSelectOn(window);
	DocModified(docData);
	SetAllMenus();
	if (drawTool == TOOL_TEXT)
		SetDrawTool(TOOL_SELECT);
	return (success);
}

/*
 *	Duplicate selected objects
 */

static BOOL DoDuplicate(WindowPtr window)
{
	WORD xOffset, yOffset;
	BOOL success;
	DocObjPtr docObj, newObj, lastObj;
	DocDataPtr docData = GetWRefCon(window);

	if ((lastObj = LastSelected(docData)) == NULL)
		return (FALSE);
	if ((docData->Flags & DOC_GRIDSNAP) == 0) {
		xOffset = DUP_XOFFSET;
		yOffset = DUP_YOFFSET;
	}
	else
		GetGridSpacing(docData, &xOffset, &yOffset);
	HiliteSelectOff(window);
	success = FALSE;
	for (docObj = FirstSelected(docData); ; docObj = NextSelected(docObj)) {
		success = TRUE;
		if ((newObj = DuplicateObject(CurrLayer(docData), docObj)) == NULL) {
			Error(ERR_NO_MEM);
			success = FALSE;
			break;
		}
		UnSelectObject(docObj);
		SelectObject(newObj);
		OffsetObject(newObj, xOffset, yOffset);
		AdjustToDocBounds(docData, &newObj->Frame);
		InvalObjectRect(window, newObj);
		if (docObj == lastObj)
			break;
	}
	HiliteSelectOn(window);
	DocModified(docData);
	return (success);
}

/*
 *	Flip objects horizontally or vertically
 */

static BOOL DoFlip(WindowPtr window, BOOL horiz)
{
	WORD cx, cy;
	DocObjPtr docObj;
	DocDataPtr docData = GetWRefCon(window);
	Rectangle selRect;

/*
	First get rectangle containing all selected objects
*/
	if (!GetSelectRect(docData, &selRect))
		return (FALSE);
	cx = (selRect.MaxX + selRect.MinX)/2;
	cy = (selRect.MaxY + selRect.MinY)/2;
/*
	Now flip about the group center
*/
	HiliteSelectOff(window);
	for (docObj = FirstSelected(docData); docObj; docObj = NextSelected(docObj)) {
		InvalObjectRect(window, docObj);
		FlipObject(docObj, cx, cy, horiz);
	}
	AdjustOffsets(window);
	for (docObj = FirstSelected(docData); docObj; docObj = NextSelected(docObj))
		InvalObjectRect(window, docObj);
	HiliteSelectOn(window);
	DocModified(docData);
	return (TRUE);
}

/*
 *	Rotate objects left or right
 */

static BOOL DoRotate(WindowPtr window)
{
	WORD		item, angle;
	BOOL		done;
	DocObjPtr	docObj;
	DocDataPtr	docData = GetWRefCon(window);
	DialogPtr	dlg;
	GadgetPtr	gadgList;
	Point		pt;
	Rectangle	selRect;
	TextChar	angleText[GADG_MAX_STRING];

/*
	Get rotation angle
*/
	BeginWait();
	if ((dlg = GetDialog(dlgList[DLG_ROTATE], screen, mainMsgPort)) == NULL) {
		EndWait();
		Error(ERR_NO_MEM);
		return (FALSE);
	}
	gadgList = dlg->FirstGadget;
	OutlineOKButton(dlg);
	done = FALSE;
	do {
		WaitPort(mainMsgPort);
		item = CheckDialog(mainMsgPort, dlg, DialogFilter);
		GetEditItemText(gadgList, ANGLE_TEXT, angleText);
		switch(item) {
		case -1:
			EnableGadgetItem(gadgList, OK_BUTTON, dlg, NULL, (strlen(angleText) != 0));
			break;
		case CANCEL_BUTTON:
			done = TRUE;
			break;
		case OK_BUTTON:
			if (strlen(angleText) == 0)
				break;
			angle = StringToNum(angleText);
			done = TRUE;
			break;
		}
	} while (!done);
	DisposeDialog(dlg);
	EndWait();
	if (item == CANCEL_BUTTON)
		return (FALSE);
/*
	If objects are locked, abort
*/
	if (ObjectsLocked(docData)) {
		Error(ERR_OBJ_LOCKED);
		return (FALSE);
	}
/*
	First get rectangle containing all selected objects
*/
	if (!GetSelectRect(docData, &selRect))
		return (FALSE);
	GetRectCenter(&selRect, &pt);
/*
	Now rotate about the group center
*/
	HiliteSelectOff(window);
	for (docObj = FirstSelected(docData); docObj; docObj = NextSelected(docObj)) {
		InvalObjectRect(window, docObj);
		RotateObject(docObj, pt.x, pt.y, angle);
	}
	AdjustOffsets(window);
	for (docObj = FirstSelected(docData); docObj; docObj = NextSelected(docObj))
		InvalObjectRect(window, docObj);
	HiliteSelectOn(window);
	DocModified(docData);
	return (TRUE);
}

/*
 *	Set scale dialog edit text
 */

static void SetScaleText(DialogPtr dlg, WORD xScale, WORD yScale)
{
	TextChar scaleText[GADG_MAX_STRING];

	NumToString(xScale, scaleText);
	SetEditItemText(dlg->FirstGadget, WIDTH_TEXT, dlg, NULL, scaleText);
	NumToString(yScale, scaleText);
	SetEditItemText(dlg->FirstGadget, HEIGHT_TEXT, dlg, NULL, scaleText);
}

/*
 *	Scale selected objects
 */

static BOOL DoScale(WindowPtr window)
{
	WORD item;
	LONG xScale, yScale, minScale, maxXScale, maxYScale;
	BOOL done;
	GadgetPtr gadget, gadgList;
	DocObjPtr docObj;
	DocDataPtr docData = GetWRefCon(window);
	Rectangle rect, newRect, frame;
	TextChar xScaleText[GADG_MAX_STRING], yScaleText[GADG_MAX_STRING];

	if (ObjectsLocked(docData)) {
		Error(ERR_OBJ_LOCKED);
		return (FALSE);
	}
	if (!GetSelectRect(docData, &rect))
		return (FALSE);
/*
	Set min and max scale amounts
	(Technically, we should scale using factor of (max - min + 1), but that
		gives results that look wrong on screen)
*/
	minScale = 10;
	if (rect.MaxX <= rect.MinX || rect.MaxY <= rect.MinY)
		return (TRUE);
	newRect.MinX = rect.MinX;
	newRect.MinY = rect.MinY;
	newRect.MaxX = docData->DocWidth - 1;
	newRect.MaxY = docData->DocHeight - 1;
	maxXScale = ((LONG) (newRect.MaxX - newRect.MinX)*100)/(rect.MaxX - rect.MinX);
	maxYScale = ((LONG) (newRect.MaxY - newRect.MinY)*100)/(rect.MaxY - rect.MinY);
/*
	Get dialog
*/
	BeginWait();
	if ((scaleDlg = GetDialog(dlgList[DLG_SCALE], screen, mainMsgPort)) == NULL) {
		EndWait();
		Error(ERR_NO_MEM);
		return (FALSE);
	}
	gadgList = scaleDlg->FirstGadget;
	OutlineOKButton(scaleDlg);
//	Delay(5);					// Wait for gadget to be activated completely
	SetScaleText(scaleDlg, 100, 100);
/*
	Handle dialog
*/
	done = FALSE;
	do {
		WaitPort(mainMsgPort);
		item = CheckDialog(mainMsgPort, scaleDlg, DialogFilter);
		GetEditItemText(gadgList, WIDTH_TEXT, xScaleText);
		GetEditItemText(gadgList, HEIGHT_TEXT, yScaleText);
		switch(item) {
		case -1:					/* INTUITICKS message */
			EnableGadgetItem(gadgList, OK_BUTTON, scaleDlg, NULL,
							 (strlen(xScaleText) != 0 && strlen(yScaleText) != 0));
			break;
		case CANCEL_BUTTON:
			done = TRUE;
			break;
		case OK_BUTTON:
			xScale = StringToNum(xScaleText);
			yScale = StringToNum(yScaleText);
			if (CheckNumber(xScaleText) && CheckNumber(yScaleText) &&
				xScale >= minScale && yScale >= minScale &&
				xScale <= maxXScale && yScale <= maxYScale) {
				done = TRUE;
				break;
			}
			Error(ERR_SCALE);
			if (xScale < minScale)
				xScale = minScale;
			else if (xScale > maxXScale)
				xScale = maxXScale;
			if (yScale < minScale)
				yScale = minScale;
			else if (yScale > maxYScale)
				yScale = maxYScale;
			SetScaleText(scaleDlg, xScale, yScale);
			gadget = GadgetItem(gadgList, WIDTH_TEXT);
			ActivateGadget(gadget, scaleDlg, NULL);
			break;
		}
	} while (!done);
	DisposeDialog(scaleDlg);
	EndWait();
	if (item == CANCEL_BUTTON)
		return (FALSE);
	if (xScale == 100 && yScale == 100)
		return (TRUE);
/*
	Scale seleted objects
*/
	newRect.MinX = rect.MinX;
	newRect.MinY = rect.MinY;
	newRect.MaxX = rect.MinX + ((LONG) (rect.MaxX - rect.MinX)*xScale + 50)/100;
	newRect.MaxY = rect.MinY + ((LONG) (rect.MaxY - rect.MinY)*yScale + 50)/100;
	for (docObj = FirstSelected(docData); docObj; docObj = NextSelected(docObj)) {
		InvalObjectRect(window, docObj);
		frame = docObj->Frame;
		MapRect(&frame, &rect, &newRect);
		ScaleObject(docObj, &frame);
		InvalObjectRect(window, docObj);
	}
	DocModified(docData);
	return (TRUE);
}

/*
 *	Select all objects in document
 */

static BOOL DoSelectAll(WindowPtr window)
{
	DocObjPtr docObj;
	TextObjPtr textObj;
	DocDataPtr docData = GetWRefCon(window);

	HiliteSelectOff(window);
	if (TextInEdit(window)) {
		textObj = (TextObjPtr) FirstSelected(docData);
		textObj->SelStart = 0;
		textObj->SelEnd   = textObj->TextLen;
	}
	else {
		for (docObj = BottomObject(CurrLayer(docData)); docObj; docObj = NextObj(docObj))
			SelectObject(docObj);
	}
	HiliteSelectOn(window);
	SetAllMenus();
	return (TRUE);
}

/*
 *	Convert selected objects to polygons
 */

static BOOL DoConvertToPoly(WindowPtr window)
{
	WORD objType;
	BOOL found;
	DocObjPtr docObj;
	PolyObjPtr polyObj;
	DocLayerPtr currLayer;
	DocDataPtr docData = GetWRefCon(window);

	if (ObjectsLocked(docData)) {
		Error(ERR_OBJ_LOCKED);
		return (FALSE);
	}
	currLayer = CurrLayer(docData);
	found = FALSE;
	HiliteSelectOff(window);
	for (docObj = FirstSelected(docData); docObj; docObj = NextSelected(docObj)) {
		objType = docObj->Type;
		if (objType != TYPE_LINE && objType != TYPE_RECT && objType != TYPE_OVAL)
			continue;
		InvalObjectRect(window, docObj);
		if ((polyObj = ConvertObjectToPoly(docObj)) == NULL) {
			Error(ERR_NO_MEM);
			break;
		}
		InsertObject(currLayer, docObj, (DocObjPtr) polyObj);
		DetachObject(currLayer, docObj);
		DisposeDocObject(docObj);
		docObj = (DocObjPtr) polyObj;
		SelectObject(docObj);
		AdjustToDocBounds(docData, &docObj->Frame);
		InvalObjectRect(window, docObj);
		found = TRUE;
	}
	HiliteSelectOn(window);
	DocModified(docData);
	SetEditMenu();
	return (found);
}

/*
 *	Close or open selected polygons
 */

static BOOL DoPolyClose(WindowPtr window, BOOL close)
{
	BOOL found;
	DocObjPtr docObj;
	DocDataPtr docData = GetWRefCon(window);

	found = FALSE;
	for (docObj = FirstSelected(docData); docObj; docObj = NextSelected(docObj)) {
		if (docObj->Type != TYPE_POLY)
			continue;
		PolySetClosed((PolyObjPtr) docObj, close);
		InvalObjectRect(window, docObj);
		found = TRUE;
	}
	DocModified(docData);
	SetEditMenu();
	return (found);
}

/*
 *	Smooth or unsmooth selected polygons
 */

static BOOL DoSmooth(WindowPtr window, BOOL smooth)
{
	BOOL found;
	DocObjPtr docObj;
	DocDataPtr docData = GetWRefCon(window);

	found = FALSE;
	for (docObj = FirstSelected(docData); docObj; docObj = NextSelected(docObj)) {
		if (docObj->Type != TYPE_POLY)
			continue;
		PolySetSmooth((PolyObjPtr) docObj, smooth);
		InvalObjectRect(window, docObj);
		found = TRUE;
	}
	DocModified(docData);
	SetEditMenu();
	return (found);
}

/*
 *	Smooth or unsmooth selected polygons
 */

static BOOL DoMerge(WindowPtr window, BOOL merge)
{
	BOOL 		found;
	DocObjPtr 	prevObj, docObj, nextObj;
	PolyObjPtr	firstObj = NULL;
	DocDataPtr 	docData = GetWRefCon(window);
	Rectangle	saveFrame;

	found = FALSE;
	for (docObj = FirstSelected(docData); docObj; docObj = nextObj) {
		nextObj = NextSelected(docObj);		// docObj may be disposed of
		prevObj = PrevSelected(docObj);
		if (docObj->Type != TYPE_POLY)
			continue;
		if (!merge) {
			if (!UnMergePoly(window, (PolyObjPtr) docObj)) {
				Error(ERR_NO_MEM);
				return (FALSE);
			}
			continue;
		}
		if (firstObj) {
			saveFrame = firstObj->DocObj.Frame;
			DetachObject(CurrLayer(docData), docObj);
			if (!MergePoly(window, firstObj, (PolyObjPtr) docObj)) {
				ErrBeep();
				firstObj->DocObj.Frame = saveFrame;
				InsertObject(CurrLayer(docData), prevObj, docObj);
				break;
			}
		}
		else
			firstObj = (PolyObjPtr) docObj;
		found = TRUE;
	}
	DocModified(docData);
	SetEditMenu();
	return (found);
}

/*
 *	Handle polygon sub menu
 */

static BOOL DoPolySubMenu(WindowPtr window, UWORD sub)
{
	BOOL	success;

	switch (sub) {
		case CLOSEPOLY_SUBITEM:
		case OPENPOLY_SUBITEM:
			success = DoPolyClose(window, (BOOL) (sub == CLOSEPOLY_SUBITEM));
			break;
		case SMOOTH_SUBITEM:
		case UNSMOOTH_SUBITEM:
			success = DoSmooth(window, (BOOL) (sub == SMOOTH_SUBITEM));
			break;
		case MERGE_SUBITEM:
		case UNMERGE_SUBITEM:
			success = DoMerge(window, (BOOL) (sub == MERGE_SUBITEM));
			break;
	}
	return (success);
}

/*
 *	Handle Edit menu
 */

BOOL DoEditMenu(WindowPtr window, UWORD item, UWORD sub, UWORD modifier)
{
	BOOL success;

	if (!IsDocWindow(window) && item != HOTLINKS_ITEM)
		return (FALSE);
	UndoOff();
	success = FALSE;
	if ((modifier & SHIFTKEYS) && (modifier & CMDKEY)) {
		if (item == POLYGON_ITEM && sub == SMOOTH_SUBITEM)
			sub = UNSMOOTH_SUBITEM;
	}
	switch (item) {
	case CUT_ITEM:
		success = DoCut(window, TRUE, TRUE);
		break;
	case COPY_ITEM:
		success = DoCut(window, TRUE, FALSE);
		break;
	case PASTE_ITEM:
		success = DoPaste(window);
		break;
	case ERASE_ITEM:
		success = DoCut(window, FALSE, TRUE);
		break;
	case HOTLINKS_ITEM:
		success = DoHotLinksMenu(window, sub);
		break;
	case DUPLICATE_ITEM:
		success = DoDuplicate(window);
		break;
	case ROTATE_ITEM:
		success = DoRotate(window);
		break;
	case FLIP_ITEM:
		success = DoFlip(window, (BOOL) (sub == HORIZONTAL_SUBITEM));
		break;
	case SCALE_ITEM:
		success = DoScale(window);
		break;
	case CONVERTTOPOLY_ITEM:
		success = DoConvertToPoly(window);
		break;
	case POLYGON_ITEM:
		success = DoPolySubMenu(window, sub);
		break;
	case SELECTALL_ITEM:
		success = DoSelectAll(window);
		break;
	}
	return (success);
}
