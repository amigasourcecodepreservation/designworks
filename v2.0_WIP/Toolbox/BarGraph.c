/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Toolbox library
 *	Copyright (c) 1992-93 New Horizons Software, Inc.
 *
 *	Bar graph routines
 */

#include <exec/types.h>
#include <graphics/gfxmacros.h>
#include <intuition/intuition.h>

#include <proto/graphics.h>

#include <string.h>

#include <Toolbox/Globals.h>
#include <Toolbox/Memory.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Border.h>
#include <Toolbox/BarGraph.h>
#include <Toolbox/Utility.h>

/*
 *	Local variables and definitions
 */

#define MIN(a, b)		(((a)<(b))?(a):(b))
#define MAX(a, b)		(((a)>(b))?(a):(b))

#define WIDTH(rect)		((rect)->MaxX - (rect)->MinX + 1)
#define HEIGHT(rect)	((rect)->MaxY - (rect)->MinY + 1)

#define IS_HORIZ(barGraph)		((barGraph)->Flags & BG_HORIZONTAL)
#define SHOW_PERCENT(barGraph)	((barGraph)->Flags & BG_SHOWPERCENT)

/*
 *	Local prototypes
 */

static WORD	NewPercent(BarGraphPtr);
static WORD	NewSize(BarGraphPtr);

static void	DrawDivisions(RastPtr, BarGraphPtr);
static void	DrawPercent(RastPtr, BarGraphPtr, WORD);
static void	UpdateBarGraph(RastPtr, BarGraphPtr, WORD);

/*
 *	NewBarGraph
 *	Allocates new BarGraph and sets it up with values passed
 *	Returns NULL if cannot allocate mem for structure
 */

BarGraphPtr NewBarGraph(RectPtr rect, ULONG maxValue, WORD numDivs, WORD flags)
{
	register BarGraphPtr	barGraph;

	if (EmptyRect(rect))
		return (NULL);
	if ((barGraph = MemAlloc(sizeof(BarGraph), MEMF_CLEAR)) != NULL) {
		barGraph->Rect			= *rect;
		barGraph->MaxValue		= MAX(1, maxValue);
		barGraph->LastPercent	= -1;
		barGraph->NumDivisions	= numDivs;
		barGraph->Flags			= flags;
		if (!IS_HORIZ(barGraph))
			barGraph->Flags &= ~BG_SHOWPERCENT;
	}
	return (barGraph);
}

/*
 *	DisposeBarGraph
 *	Frees up memory associated with bar graph
 *	This avoids flickering that would result if entire bar graph were redrawn
 */

void DisposeBarGraph(BarGraphPtr barGraph)
{
	if (barGraph)
		MemFree(barGraph, sizeof(BarGraph));
}

/*
 *	NewPercent
 *	Return percent full for bar graph
 */

static WORD NewPercent(register BarGraphPtr barGraph)
{
	return ((barGraph->Value*100L)/barGraph->MaxValue);
}

/*
 *	NewSize
 *	Return new size for bar graph
 */

static WORD NewSize(register BarGraphPtr barGraph)
{
	register LONG	size;
	Rectangle		rect;

	rect = barGraph->Rect;
	InsetRect(&rect, 1, 1);
	size = IS_HORIZ(barGraph) ? WIDTH(&rect) : HEIGHT(&rect);
	return ((barGraph->Value*size + barGraph->MaxValue/2)/barGraph->MaxValue);
}

/*
 *	DrawDivisions
 *	Draw division markers in bar graph
 */

static void DrawDivisions(RastPtr rPort, register BarGraphPtr barGraph)
{
	WORD	i, x, y, dx, dy, width, height, numDiv;

	if ((numDiv = barGraph->NumDivisions) == 0)
		return;
/*
	Set up
*/
	width = barGraph->Rect.MaxX - barGraph->Rect.MinX + 1;
	height = barGraph->Rect.MaxY - barGraph->Rect.MinY + 1;
	SetAPen(rPort, _tbPenBlack);
	SetDrMd(rPort, JAM1);
/*
	Draw divisions
*/
	for (i = 0; i <= numDiv; i++) {
		if (IS_HORIZ(barGraph)) {
			x = barGraph->Rect.MinX + (i*(width - 1))/numDiv;
			y = barGraph->Rect.MaxY + 1;
			dx = 0;
			dy = height/3;
		}
		else {
			x = barGraph->Rect.MaxX + 1;
			y = barGraph->Rect.MaxY - (i*(height - 1))/numDiv;
			dx = width/3;
			dy = 0;
		}
		if (i*2 == numDiv) {							// 50% mark
			dx = (3*dx)/4;
			dy = (3*dy)/4;
		}
		else if (i*4 == numDiv || i*4 == 3*numDiv) {	// 25% and 75% marks
			dx /= 2;
			dy /= 2;
		}
		else if (i != 0 && i != numDiv) {				// All other marks
			dx /= 4;
			dy /= 4;
		}
		Move(rPort, x, y);
		Draw(rPort, x + dx, y + dy);
	}
}

/*
 *	DrawPercent
 *	Draw percentage complete in bar graph
 */

static void DrawPercent(RastPtr rPort, register BarGraphPtr barGraph, WORD newPercent)
{
	WORD		x, y, width, height;
	TextFontPtr	font, oldFont;
	TextChar	buff[10];

	if (!SHOW_PERCENT(barGraph) || newPercent < 0 || newPercent > 100)
		return;
	NumToString(newPercent, buff);
	strcat(buff, "%");
	if ((font = GetFont(&_tbTextAttr)) != NULL) {
		oldFont = rPort->Font;
		SetFont(rPort, font);
		SetDrMd(rPort, COMPLEMENT);
		SetWrMsk(rPort, _tbPenLight ^ _tbPenBlack);
		width = barGraph->Rect.MaxX - barGraph->Rect.MinX + 1;
		height = barGraph->Rect.MaxY - barGraph->Rect.MinY + 1;
		x = (width - TextLength(rPort, buff, strlen(buff)))/2;
		y = (height - font->tf_Baseline - 1)/2 + font->tf_Baseline;
		Move(rPort, barGraph->Rect.MinX + x, barGraph->Rect.MinY + y);
		Text(rPort, buff, strlen(buff));
		SetWrMsk(rPort, 0xFF);
		SetDrMd(rPort, JAM1);
		SetFont(rPort, oldFont);
		CloseFont(font);
		barGraph->LastPercent = newPercent;
	}
}

/*
 *	UpdateBarGraph
 *	Draws only the area of barGraph that changed since last update
 *	Note: Will never be called with NULL barGraph or rPort
 */

static void UpdateBarGraph(register RastPtr rPort, register BarGraphPtr barGraph,
						   WORD newSize)
{
	register WORD	min, max;
	Rectangle		rect;

	rect = barGraph->Rect;
	InsetRect(&rect, 1, 1);
/*
	If different than last drawn size, then draw difference
*/
	if (newSize != barGraph->LastSize) {
		if (newSize > barGraph->LastSize) {
			SetAPen(rPort, _tbPenDark);
			min = barGraph->LastSize;
			max = newSize - 1;
		}
		else {
			SetAPen(rPort, _tbPenLight);
			min = newSize;
			max = barGraph->LastSize - 1;
		}
		if (IS_HORIZ(barGraph)) {
			rect.MaxX  = rect.MinX + max;
			rect.MinX += min;
		}
		else {
			rect.MinY  = rect.MaxY - max;
			rect.MaxY -= min;
		}
		FillRect(rPort, &rect);				// Checks for empty rectangle
		barGraph->LastSize = newSize;
	}
}

/*
 *	DrawBarGraph
 *	Draw the entire bar graph
 */

void DrawBarGraph(RastPtr rPort, register BarGraphPtr barGraph)
{

	if (barGraph && rPort) {
		SetAPen(rPort, _tbPenLight);
		FillRect(rPort, &barGraph->Rect);
		DrawShadowBox(rPort, &barGraph->Rect, 0, FALSE);
		if (barGraph->NumDivisions > 0)
			DrawDivisions(rPort, barGraph);
		barGraph->LastSize = 0;
		barGraph->LastPercent = -1;
		UpdateBarGraph(rPort, barGraph, NewSize(barGraph));
		DrawPercent(rPort, barGraph, NewPercent(barGraph));
	}
}

/*
 *	SetBarGraph
 *	Set bar graph to a specific value and draw it
 */

void SetBarGraph(RastPtr rPort, register BarGraphPtr barGraph, ULONG value)
{
	WORD	newSize, newPercent, lastSize, lo, mid, hi;
	BOOL	doPercent;

	if (barGraph && rPort) {
		barGraph->Value = MIN(value, barGraph->MaxValue);
		newSize = NewSize(barGraph);
		lastSize = barGraph->LastSize;
		if (SHOW_PERCENT(barGraph)) {
			newPercent = NewPercent(barGraph);
			mid = (barGraph->Rect.MaxX - barGraph->Rect.MinX + 1)/2;
			lo = mid - 2*_tbXSize;
			hi = mid + 2*_tbXSize;
			doPercent = (newPercent != barGraph->LastPercent ||
						 (newSize != lastSize &&
						  !(newSize < lo && lastSize < lo) &&
						  !(newSize > hi && lastSize > hi)));
		}
		else
			doPercent = FALSE;
		if (doPercent)
			DrawPercent(rPort, barGraph, barGraph->LastPercent);
		if (newSize != lastSize)
			UpdateBarGraph(rPort, barGraph, newSize);
		if (doPercent)
			DrawPercent(rPort, barGraph, newPercent);
	}
}

/*
 *	SetBarGraphMax
 *	Set bar graph to new max value (with same current value) and draw it
 */

void SetBarGraphMax(RastPtr rPort, BarGraphPtr barGraph, ULONG maxValue)
{
	if (barGraph && rPort) {
		DrawPercent(rPort, barGraph, barGraph->LastPercent);
		barGraph->MaxValue = MAX(1, MAX(maxValue, barGraph->Value));
		UpdateBarGraph(rPort, barGraph, NewSize(barGraph));
		DrawPercent(rPort, barGraph, NewPercent(barGraph));
	}
}
