/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Print handler routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>
#include <devices/prtbase.h>
#include <libraries/dos.h>

#include <proto/exec.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include <string.h>

#include <Toolbox/Graphics.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Utility.h>
#include <Toolbox/FixMath.h>

#include "Draw.h"
#include "Proto.h"

/*
 *	External variables
 */

extern ScreenPtr	screen;
extern MsgPortPtr	mainMsgPort;

extern Defaults	defaults;

extern BOOL	graphicPrinter, pagePrinter, colorPrinter;

extern TextChar	printerName[];
extern TextChar	strBuff[];

extern DlgTemplPtr	dlgList[];

extern BOOL	_tbHiRes;				/* Toolbox is set for hi-res */

/*
 *	Local variables and definitions
 */

enum {
	USLETTER_RADBTN = 2,
	USLEGAL_RADBTN,
	A4LETTER_RADBTN,
	WIDECARRIAGE_RADBTN,
	CUSTOM_RADBTN,
	REDUCE_TEXT,
	WIDTH_TEXT,
	HEIGHT_TEXT,
	PORTRAIT_ICON,
	LANDSCAPE_ICON,
	ADJUST_BOX,
	NOGAPS_BOX,
	PRINTERNAME1_TEXT
};

enum {
	HIGH_RADBTN = 2,
	NORMAL_RADBTN,
	ALL_RADBTN,
	FROM_RADBTN,
	AUTOMATIC_RADBTN,
	HANDFEED_RADBTN,
	COLORSFULL_RADBTN,
	COLORSEXT_RADBTN,
	COLORSSTD_RADBTN,
	COLLATE_BOX,
	COPIES_TEXT,
	FIRSTPAGE_TEXT,
	LASTPAGE_TEXT,
	DENSITYUP_ARROW,
	DENSITYDOWN_ARROW,
	DENSITY_TEXT,
	PRINTERNAME2_TEXT
};

#define MIN_PAPERWIDTH	1440	/* 2 inch min width */
#define MIN_PAPERHEIGHT	720		/* 1 inch min height (plus top/bottom margins) */

#define MIN_SCALE	25
#define MAX_SCALE	400

#define MIN_FONTNUM	0
#define MAX_FONTNUM	10

static struct Preferences	origPrefs;	/* For OpenPrinter() call */

static struct Preferences	prefs;		/* For GetSysPrefs() call */

static BOOL	havePrinterType;	/* To avoid excessive calls to GetPrinterType() */

static DialogPtr	dlg;

/*
 *	Icons for page setup dialog
 */

static UWORD chip hrPortraitIconData[] = {
	0x0000, 0x0000, 0x7FFF, 0xFFFE, 0x7FFF, 0xFFFE, 0x7C00, 0x07FE,
	0x7DFF, 0xF3FE, 0x7DFC, 0x75FE, 0x7DFB, 0xB6FE, 0x7DFB, 0xB07E,
	0x7DFB, 0xBF7E, 0x7DFD, 0x7F7E, 0x7DE3, 0x8F7E, 0x7DDF, 0xF77E,
	0x7DDF, 0xF77E, 0x7DD7, 0xD77E, 0x7DD7, 0xD77E, 0x7DD7, 0xD77E,
	0x7DD7, 0xD77E, 0x7DD7, 0xD77E, 0x4000, 0x0002, 0x57FF, 0xFFAA,
	0x57FF, 0xFFAA, 0x57FF, 0xFFAA, 0x5000, 0x002A, 0x5FFF, 0xFFEA,
	0x4000, 0x000A, 0x5FFF, 0xFFE2, 0x5FFF, 0xFFEE, 0x57FF, 0xFFEE,
	0x5FFF, 0xFFEE, 0x4000, 0x000E, 0x7FFF, 0xFFFE, 0x0000, 0x0000
};

static UWORD chip hrLandscapeIconData[] = {
	0x0000, 0x0000, 0x7FFF, 0xFFFE, 0x7FFF, 0xFFFE, 0x7C00, 0x07FE,
	0x7DFF, 0xF3FE, 0x7DFF, 0xF5FE, 0x7D00, 0xF6FE, 0x7D7F, 0x707E,
	0x7C07, 0x7F7E, 0x7DFF, 0x477E, 0x7DFF, 0xBB7E, 0x7DFF, 0xFB7E,
	0x7DFF, 0xBB7E, 0x7DFF, 0x477E, 0x7C07, 0x7F7E, 0x7D7F, 0x7F7E,
	0x7D00, 0xFF7E, 0x7DFF, 0xFF7E, 0x4000, 0x0002, 0x57FF, 0xFFAA,
	0x57FF, 0xFFAA, 0x57FF, 0xFFAA, 0x5000, 0x002A, 0x5FFF, 0xFFEA,
	0x4000, 0x000A, 0x5FFF, 0xFFE2, 0x5FFF, 0xFFEE, 0x57FF, 0xFFEE,
	0x5FFF, 0xFFEE, 0x4000, 0x000E, 0x7FFF, 0xFFFE, 0x0000, 0x0000
};

static Image hrPortraitIcon = {
	0, 0, 32, 32, 1, &hrPortraitIconData[0], 1, 0, NULL
};

static Image hrLandscapeIcon = {
	0, 0, 32, 32, 1, &hrLandscapeIconData[0], 1, 0, NULL
};

static UWORD chip mrPortraitIconData[] = {
	0x0000, 0x0000, 0x7FFF, 0xFFFE, 0x7C00, 0x03FE, 0x7DFF, 0xF9FE,
	0x7DFC, 0x3AFE, 0x7DFB, 0xD87E, 0x7DFB, 0xDF7E, 0x7DFD, 0xBF7E,
	0x7DE3, 0x8F7E, 0x7DDF, 0xF77E, 0x7DD7, 0xD77E, 0x7DD7, 0xD77E,
	0x7DD7, 0xD77E, 0x4000, 0x0002, 0x57FF, 0xFFAA, 0x5000, 0x002A,
	0x5FFF, 0xFFE2, 0x57FF, 0xFFEE, 0x5FFF, 0xFFEE, 0x4000, 0x000E,
	0x7FFF, 0xFFFE, 0x0000, 0x0000
};

static UWORD chip mrLandscapeIconData[] = {
	0x0000, 0x0000, 0x7FFF, 0xFFFE, 0x7C00, 0x03FE, 0x7DFF, 0xF9FE,
	0x7D00, 0xFAFE, 0x7D7F, 0x787E, 0x7C03, 0x477E, 0x7DFF, 0xBB7E,
	0x7DFF, 0xBB7E, 0x7C03, 0x477E, 0x7D7F, 0x7F7E, 0x7D00, 0xFF7E,
	0x7DFF, 0xFF7E, 0x4000, 0x0002, 0x57FF, 0xFFAA, 0x5000, 0x002A,
	0x5FFF, 0xFFE2, 0x57FF, 0xFFEE, 0x5FFF, 0xFFEE, 0x4000, 0x000E,
	0x7FFF, 0xFFFE, 0x0000, 0x0000
};

static Image mrPortraitIcon = {
	0, 0, 32, 22, 1, &mrPortraitIconData[0], 1, 0, NULL
};

static Image mrLandscapeIcon = {
	0, 0, 32, 22, 1, &mrLandscapeIconData[0], 1, 0, NULL
};

/*
 *	Print options for SetPrintOption()
 */

#define NUM_OPTIONS	(sizeof(printOptNames)/sizeof(TextPtr))

static TextPtr printOptNames[] = {
	"AutoFeed",		"HandFeed",
	"Collate",		"NoCollate"
};

enum {
	OPT_AUTOFEED,		OPT_HANDFEED,
	OPT_COLLATE,		OPT_NOCOLLATE
};

/*
 *	Local prototypes
 */

void	SetDPI(PrintRecPtr);
void	SetPrRects(PrintRecPtr);
void	SetPrintFlag(PrintRecPtr, UWORD, BOOL);
void	CalcWidthHeight(WORD, WORD *, WORD *);
void	ShowWidthHeight(WORD, WORD);
void	SetDensityText(WORD, TextPtr);
BOOL	PageSetupDialogFilter(IntuiMsgPtr, WORD *);
BOOL	PrintDialogFilter(IntuiMsgPtr, WORD *);

/*
 *	Open the printer for direct output
 */

PrintIOPtr OpenPrinter()
{
	register PrintIOPtr printIO;
	MsgPortPtr replyPort;
	register struct PrinterData *pData;

	if ((replyPort = CreatePort(NULL, 0)) == NULL)
		return (NULL);
	if ((printIO = (PrintIOPtr) CreateExtIO(replyPort, sizeof(PrintIO))) == NULL) {
		DeletePort(replyPort);
		return (NULL);
	}
	if (OpenDevice("printer.device", 0, (IOReqPtr) printIO, 0)) {
		DeleteExtIO((IOReqPtr) printIO);
		DeletePort(replyPort);
		return (NULL);
	}
	pData = (struct PrinterData *) printIO->Std.io_Device;

	BlockMove(&pData->pd_Preferences, &origPrefs, sizeof(struct Preferences));

	pData->pd_Preferences.PrintImage = IMAGE_POSITIVE;
	if (pData->pd_Preferences.PrintShade == SHADE_BW)
		pData->pd_Preferences.PrintShade = SHADE_GREYSCALE;
	pData->pd_Preferences.PrintLeftMargin = 1;
	pData->pd_Preferences.PrintRightMargin = 255;
	pData->pd_Preferences.PaperLength = 999;
	pData->pd_Preferences.PrintXOffset = 0;
	pData->pd_Preferences.PrintFlags &= ~(CORRECT_RGB_MASK | CENTER_IMAGE |
										  INTEGER_SCALING | DIMENSIONS_MASK |
										  DITHERING_MASK);
	pData->pd_Preferences.PrintFlags |= IGNORE_DIMENSIONS | ORDERED_DITHERING;
	return (printIO);
}

/*
 *	Close the printer device previously openned by OpenPrinter
 */

void ClosePrinter(PrintIOPtr printIO)
{
	MsgPortPtr replyPort;
	register struct PrinterData *pData;

	pData = (struct PrinterData *) printIO->Std.io_Device;

	BlockMove(&origPrefs, &pData->pd_Preferences, sizeof(struct Preferences));

	replyPort = ((IOReqPtr) printIO)->io_Message.mn_ReplyPort;
	CloseDevice((IOReqPtr) printIO);
	DeleteExtIO((IOReqPtr) printIO);
	DeletePort(replyPort);
}

/*
 *	Get new preferences values
 *	Return TRUE if preferences changed since last call
 */

BOOL GetSysPrefs()
{
	register WORD i, len;
	struct Preferences newPrefs;

/*
	Check for changed preferences
*/
	GetPrefs(&newPrefs, sizeof(struct Preferences));
	if (memcmp((BYTE *) &newPrefs, (BYTE *) &prefs, sizeof(struct Preferences)) == 0)
		return (FALSE);
	BlockMove(&newPrefs, &prefs, sizeof(struct Preferences));
/*
	Get printer name
*/
	*printerName = '\"';
	len = strlen(prefs.PrinterFilename);
	for (i = len; i; i--) {
		if (prefs.PrinterFilename[i - 1] == ':' ||
			prefs.PrinterFilename[i - 1] == '/')
			break;
	}
	len -= i;
	BlockMove(&prefs.PrinterFilename[i], printerName + 1, len);
	printerName[len + 1] = '\"';
	printerName[len + 2] = '\0';
	for (i = 1; i < len + 1; i++) {
		if (printerName[i] == '_')
			printerName[i] = ' ';
	}
	if (printerName[1] >= 'a' && printerName[1] <= 'z')
		printerName[1] -= 'a' - 'A';
	havePrinterType = FALSE;
	return (TRUE);
}

/*
 *	Get the printer type
 */

void GetPrinterType()
{
	register WORD i;
	register PrintIOPtr printIO;
	register struct PrinterData *pData;
	register struct PrinterExtendedData *ped;

	if (havePrinterType)
		return;
/*
	Set default values
*/
	graphicPrinter = colorPrinter = pagePrinter = FALSE;
/*
	Get printer values
*/
	if ((printIO = OpenPrinter()) == NULL) {
		for (i = 0; i < 10; i++) {				/* Retry counter */
			if ((printIO = OpenPrinter()) != NULL)
				break;
			Delay(10L);
		}
	}
	if (printIO == NULL)
		return;
	pData = (struct PrinterData *) printIO->Std.io_Device;
	if (pData->pd_SegmentData) {
		ped = &pData->pd_SegmentData->ps_PED;
		graphicPrinter = ((ped->ped_PrinterClass & PPCF_GFX) != 0 &&
						  ped->ped_XDotsInch != 0);
		colorPrinter = ((ped->ped_PrinterClass & PPCF_COLOR) != 0);
		pagePrinter = (ped->ped_MaxYDots != 0);
	}
	ClosePrinter(printIO);
	havePrinterType = TRUE;
}

/*
 *	Set print density to specified value
 *	Return success status
 */

BOOL SetPrintDensity(PrintIOPtr printIO, UBYTE density, BOOL smooth)
{
	struct PrinterData *pData;
	struct PrinterExtendedData *ped;

	pData = (struct PrinterData *) printIO->Std.io_Device;
	pData->pd_Preferences.PrintDensity = density;
	if (smooth)
		pData->pd_Preferences.PrintFlags |= ANTI_ALIAS;
	else
		pData->pd_Preferences.PrintFlags &= ~ANTI_ALIAS;
	if (pData->pd_Device.dd_Device.lib_Version < 35)
		return (TRUE);
	if (pData->pd_SegmentData == NULL)
		return (FALSE);
	ped = &pData->pd_SegmentData->ps_PED;
	if ((ped->ped_PrinterClass & PPCF_GFX) == 0)
		return (FALSE);
	printIO->DRP.io_Command = PRD_DUMPRPORT;
	printIO->DRP.io_RastPort = &screen->RastPort;
	printIO->DRP.io_ColorMap = screen->ViewPort.ColorMap;
	printIO->DRP.io_Modes = 0;
	printIO->DRP.io_SrcX = 0;
	printIO->DRP.io_SrcY = 0;
	printIO->DRP.io_SrcWidth = 1;
	printIO->DRP.io_SrcHeight = 1;
	printIO->DRP.io_DestCols = 1;
	printIO->DRP.io_DestRows = 1;
	printIO->DRP.io_Special = SPECIAL_NOPRINT;
	if (DoIO((IOReqPtr) printIO) != 0)
		return (FALSE);
	return (TRUE);
}

/*
 *	Get print density values for specified density number
 *	Return FALSE if unable to open printer, or not a graphics printer
 */

BOOL GetPrintDensity(WORD density, WORD *xDPI, WORD *yDPI)
{
	BOOL success;
	register PrintIOPtr printIO;
	struct PrinterData *pData;
	struct PrinterExtendedData *ped;

	*xDPI = *yDPI = 0;
	GetPrinterType();
	if (!graphicPrinter)
		return (FALSE);
	if (density < 1)
		density = 1;
	else if (density > 7)
		density = 7;
	if ((printIO = OpenPrinter()) == NULL)
		return (FALSE);
	success = FALSE;
	pData = (struct PrinterData *) printIO->Std.io_Device;
	pData->pd_Preferences.PrintAspect = ASPECT_VERT;
	if (!SetPrintDensity(printIO, (UBYTE) density, FALSE))
		goto Exit;
	ped = &pData->pd_SegmentData->ps_PED;
	*xDPI = ped->ped_XDotsInch;
	*yDPI = ped->ped_YDotsInch;
	success = TRUE;
Exit:
	ClosePrinter(printIO);
	return (success);
}

/*
 *	Initialize the print handler
 *	Checks for the presence of ProScript
 */

void InitPrintHandler()
{
}

/*
 *	Set x and y DPI values
 */

static void SetDPI(register PrintRecPtr printRec)
{
	WORD maxXDPI, maxYDPI;
	BOOL adjust;

	if (printRec->XScale < MIN_SCALE)
		printRec->XScale = MIN_SCALE;
	if (printRec->XScale > MAX_SCALE)
		printRec->XScale = MAX_SCALE;
	if (printRec->YScale < MIN_SCALE)
		printRec->YScale = MIN_SCALE;
	if (printRec->YScale > MAX_SCALE)
		printRec->YScale = MAX_SCALE;
	GetPrinterType();
	adjust = ((printRec->Flags & PRT_ASPECTADJ) || !graphicPrinter);
	GetPrintDensity((WORD) printRec->PrintDensity, &maxXDPI, &maxYDPI);
	switch (printRec->Orientation) {
	case PRT_PORTRAIT:
		if (adjust) {
			printRec->xDPI = 80;
			printRec->yDPI = 72;
		}
		else {
			printRec->xDPI = 72;
			printRec->yDPI = 72;
		}
		break;
	case PRT_LANDSCAPE:
		if (adjust) {
			printRec->xDPI = 72;
			printRec->yDPI = 60;
		}
		else {
			printRec->xDPI = 72;
			printRec->yDPI = 72;
		}
		break;
	}
	printRec->xDPI = ((LONG) printRec->xDPI*100L + printRec->XScale/2)
					 /printRec->XScale;
	printRec->yDPI = ((LONG) printRec->yDPI*100L + printRec->YScale/2)
					 /printRec->YScale;
}

/*
 *	Set paper and page rectangles according to print record settings
 */

static void SetPrRects(register PrintRecPtr printRec)
{
	register WORD xDPI, yDPI, width, height;
	WORD topMarg, botMarg, leftMarg, rightMarg;
	BOOL noGaps;

	xDPI = printRec->xDPI;
	yDPI = printRec->yDPI;
	noGaps = printRec->Flags & PRT_NOGAPS;
	switch (printRec->PageSize) {
	case PRT_USLETTER:
		printRec->PaperWidth = 6120;			/* 8.5 in */
		printRec->PaperHeight = 7920;			/* 11 in */
		break;
	case PRT_USLEGAL:
		printRec->PaperWidth = 6120;			/* 8.5 in */
		printRec->PaperHeight = 10080;			/* 14 in */
		break;
	case PRT_A4LETTER:
		printRec->PaperWidth = 5953;			/* 210 mm */
		printRec->PaperHeight = 8419;			/* 297 mm */
		break;
	case PRT_WIDECARRIAGE:
		printRec->PaperWidth = 10080;			/* 14 in */
		printRec->PaperHeight = 7920;			/* 11 in */
		break;
	}
	if (printRec->PaperWidth < MIN_PAPERWIDTH)
		printRec->PaperWidth = MIN_PAPERWIDTH;
	if (noGaps) {
		if (printRec->PaperHeight < MIN_PAPERHEIGHT)
			printRec->PaperHeight = MIN_PAPERHEIGHT;
	}
	else {
		if (printRec->PaperHeight < MIN_PAPERHEIGHT + 720)
			printRec->PaperHeight = MIN_PAPERHEIGHT + 720;
	}
	if (printRec->Orientation == PRT_PORTRAIT) {
		width = DecipointsToDots(printRec->PaperWidth, xDPI);
		height = DecipointsToDots(printRec->PaperHeight, yDPI);
		leftMarg = rightMarg = DecipointsToDots(180, xDPI);
		topMarg = botMarg = (noGaps) ? 0 : DecipointsToDots(360, yDPI);
	}
	else {
		width = DecipointsToDots(printRec->PaperHeight, xDPI);
		height = DecipointsToDots(printRec->PaperWidth, yDPI);
		leftMarg = rightMarg = (noGaps) ? 0 : DecipointsToDots(360, xDPI);
		topMarg = botMarg = DecipointsToDots(180, yDPI);
	}
	SetRect(&printRec->PaperRect, 0, 0, width - 1, height - 1);
	printRec->PageRect.MinX = printRec->PaperRect.MinX + leftMarg;
	printRec->PageRect.MaxX = printRec->PaperRect.MaxX - rightMarg;
	printRec->PageRect.MinY = printRec->PaperRect.MinY + topMarg;
	printRec->PageRect.MaxY = printRec->PaperRect.MaxY - botMarg;
}

/*
 *	Validate the print record
 */

void PrValidate(register PrintRecPtr printRec)
{
	WORD maxXDPI, maxYDPI;
	BOOL smoothAble;

	GetPrinterType();
	if (printRec->Quality != PRT_GRAPHIC && printRec->Quality != PRT_FULLRES)
		printRec->Quality = PRT_GRAPHIC;
	if (graphicPrinter)
		SetDPI(printRec);
	else {
		printRec->xDPI = 80;
		printRec->yDPI = 72;
		printRec->Orientation = PRT_PORTRAIT;
		printRec->Flags |= PRT_ASPECTADJ;
		printRec->Flags &= ~(PRT_PICTURES | PRT_FONTS);
	}
	if (printRec->Orientation != PRT_PORTRAIT && printRec->Orientation != PRT_LANDSCAPE)
		printRec->Orientation = PRT_PORTRAIT;
	if (pagePrinter)
		printRec->Flags &= ~PRT_NOGAPS;
	GetPrintDensity((WORD) printRec->PrintDensity, &maxXDPI, &maxYDPI);
	if (printRec->Quality == PRT_FULLRES)
		smoothAble = FALSE;
	else if (printRec->Orientation == PRT_PORTRAIT)
		smoothAble = (printRec->xDPI < maxXDPI && printRec->yDPI < maxYDPI);
	else
		smoothAble = (printRec->xDPI < maxYDPI && printRec->yDPI < maxXDPI);
	if (!smoothAble)
		printRec->Flags &= ~PRT_SMOOTH;
	if (printRec->FontNum < MIN_FONTNUM)
		printRec->FontNum = MIN_FONTNUM;
	else if (printRec->FontNum > MAX_FONTNUM)
		printRec->FontNum = MAX_FONTNUM;
	SetPrRects(printRec);
	printRec->Copies = 1;
	if (printRec->FirstPage < 0)
		printRec->FirstPage = 0;
	if (printRec->LastPage > 9999)
		printRec->LastPage = 9999;
	printRec->PrintPitch = PRT_PICA;
	printRec->PrintSpacing = PRT_SIXLPI;
	if (printRec->Depth != PRT_DEPTHSTANDARD && printRec->Depth != PRT_DEPTHEXTENDED &&
		printRec->Depth != PRT_DEPTHFULL)
		printRec->Depth = PRT_DEPTHSTANDARD;
}

/*
 *	Set print record to default values
 */

void PrintDefault(register PrintRecPtr printRec)
{
	WORD density;
	struct Preferences prefs;

	GetPrinterType();
	printRec->Version = PRINTHANDLER_VERSION;
	printRec->Flags = 0;
/*
	Set preferences values
*/
	GetPrefs(&prefs, sizeof(struct Preferences));
	printRec->PageSize = (prefs.PaperSize == W_TRACTOR) ?
						 PRT_WIDECARRIAGE : PRT_USLETTER;
	printRec->Orientation = (prefs.PrintAspect == ASPECT_HORIZ) ?
							PRT_PORTRAIT : PRT_LANDSCAPE;
	printRec->PaperFeed = (prefs.PaperType == FANFOLD) ?
						  PRT_CONTINUOUS : PRT_CUTSHEET;
	if (prefs.PrintPitch == PICA)
		printRec->PrintPitch = PRT_PICA;
	else if (prefs.PrintPitch == ELITE)
		printRec->PrintPitch = PRT_ELITE;
	else
		printRec->PrintPitch = PRT_CONDENSED;
	printRec->PrintSpacing = (prefs.PrintSpacing == SIX_LPI) ?
							 PRT_SIXLPI : PRT_EIGHTLPI;
	printRec->Quality = PRT_GRAPHIC;
	density = prefs.PrintDensity;
	if (density < 1)
		density = 1;
	else if (density > 7)
		density = 7;
	printRec->PrintDensity = density;
	if (prefs.PrintFlags & ANTI_ALIAS)
		printRec->Flags |= PRT_SMOOTH;
/*
	Set other defaults
*/
	printRec->XScale = printRec->YScale = 100;
	printRec->Flags |= PRT_ASPECTADJ;
	printRec->FontNum = MIN_FONTNUM;
	printRec->Copies = 1;
	printRec->FirstPage = 0;
	printRec->LastPage = 9999;
	printRec->Depth = PRT_DEPTHSTANDARD;
/*
	Validate the print record
*/
	PrValidate(printRec);
}

/*
 *	Set or clear flag in printRec
 */

static void SetPrintFlag(PrintRecPtr printRec, UWORD flagBit, BOOL setIt)
{
	if (setIt)
		printRec->Flags |= flagBit;
	else
		printRec->Flags &= ~flagBit;
}

/*
 *	Calculate new width and height for given page size
 */

static void CalcWidthHeight(register WORD pageSize, register WORD *width,
							register WORD *height)
{
	switch (pageSize) {
	case USLETTER_RADBTN:
		*width = 6120;			/* 8.5 in */
		*height = 7920;			/* 11 in */
		break;
	case USLEGAL_RADBTN:
		*width = 6120;			/* 8.5 in */
		*height = 10080;		/* 14 in */
		break;
	case A4LETTER_RADBTN:
		*width = 5953;			/* 210 mm */
		*height = 8419;			/* 297 mm */
		break;
	case WIDECARRIAGE_RADBTN:
		*width = 10080;			/* 14 in */
		*height = 7920;			/* 11 in */
		break;
	}
}

/*
 *	Show new width and height in page setup dialog
 */

static void ShowWidthHeight(register WORD width, register WORD height)
{
	DecipointToText(width, strBuff, 2);
	SetEditItemText(dlg, NULL, WIDTH_TEXT, strBuff);
	DecipointToText(height, strBuff, 2);
	SetEditItemText(dlg, NULL, HEIGHT_TEXT, strBuff);
}

/*
 *	Set print density text
 */

static void SetDensityText(WORD density, TextPtr text)
{
	WORD xDPI, yDPI;
	TextChar numBuff[10];		/* Can't use strBuff */

	if (!GetPrintDensity(density, &xDPI, &yDPI))
		xDPI = yDPI = 0;
	*text = '\0';
	if (xDPI) {
		NumToString(xDPI, numBuff);
		strcat(text, numBuff);
	}
	else
		strcat(text, "--");
	strcat(text, " � ");
	if (yDPI) {
		NumToString(yDPI, numBuff);
		strcat(text, numBuff);
	}
	else
		strcat(text, "--");
	strcat(text, " dpi (");
	NumToString(density, numBuff);
	strcat(text, numBuff);
	strcat(text, ")");
}

/*
 *	Page setup dialog filter
 */

static BOOL PageSetupDialogFilter(register IntuiMsgPtr intuiMsg, WORD *item)
{
	register WORD itemHit;
	register ULONG class;
	register UWORD qualifier;
	register GadgetPtr gadget;

	class = intuiMsg->Class;
	if (intuiMsg->IDCMPWindow == dlg && (class == GADGETDOWN || class == GADGETUP)) {
		qualifier = intuiMsg->Qualifier;
		itemHit = GadgetNumber((GadgetPtr) intuiMsg->IAddress);
		if (class == GADGETDOWN &&
			itemHit >= WIDTH_TEXT && itemHit <= HEIGHT_TEXT) {
			ReplyMsg((MsgPtr) intuiMsg);
			*item = CUSTOM_RADBTN;
			return (TRUE);
		}
		if (class == GADGETUP && !(qualifier & IEQUALIFIER_NUMERICPAD) &&
			itemHit >= WIDTH_TEXT && itemHit <= HEIGHT_TEXT) {
			if (qualifier & SHIFTKEYS)
				itemHit--;
			else
				itemHit++;
			if (itemHit < WIDTH_TEXT || itemHit > HEIGHT_TEXT)
				return (FALSE);
			ReplyMsg((MsgPtr) intuiMsg);
			gadget = GadgetItem(dlg->FirstGadget, itemHit);
			ActivateGadget(gadget, dlg, NULL);
			Delay(5);					/* Wait for gadget to become active */
			*item = -1;
			return (TRUE);
		}
		if (itemHit == PORTRAIT_ICON || itemHit == LANDSCAPE_ICON) {
			ReplyMsg((MsgPtr) intuiMsg);
			*item = (class == GADGETDOWN) ? itemHit : -1;
			return (TRUE);
		}
	}
	return (DialogFilter(intuiMsg, item));
}

/*
 *	Display and handle page setup dialog
 *	Return success status
 */

BOOL PageSetupDialog(PrintRecPtr printRec)
{
	register WORD item, offItem, onItem, strItem;
	WORD pageSize, orient, width, height, scale;
	BOOL done, adjust, noGaps;
	register GadgetPtr gadget, gadgList;
	ImagePtr portraitIcon, landscapeIcon;

	PrValidate(printRec);		/* Calls GetPrinterType() */
/*
	Get initial settings
*/
	if (printRec->PageSize == PRT_USLETTER)
		pageSize = USLETTER_RADBTN;
	else if (printRec->PageSize == PRT_USLEGAL)
		pageSize = USLEGAL_RADBTN;
	else if (printRec->PageSize == PRT_A4LETTER)
		pageSize = A4LETTER_RADBTN;
	else if (printRec->PageSize == PRT_WIDECARRIAGE)
		pageSize = WIDECARRIAGE_RADBTN;
	else
		pageSize = CUSTOM_RADBTN;
	width = printRec->PaperWidth;
	height = printRec->PaperHeight;
	scale = printRec->XScale;
	if (scale < MIN_SCALE)
		scale = MIN_SCALE;
	else if (scale > MAX_SCALE)
		scale = MAX_SCALE;
	orient = printRec->Orientation;
	adjust = (printRec->Flags & PRT_ASPECTADJ);
	noGaps = (printRec->Flags & PRT_NOGAPS);
	if (_tbHiRes) {
		portraitIcon  = &hrPortraitIcon;
		landscapeIcon = &hrLandscapeIcon;
	}
	else {
		portraitIcon  = &mrPortraitIcon;
		landscapeIcon = &mrLandscapeIcon;
	}
	dlgList[DLG_PAGESETUP]->Gadgets[PORTRAIT_ICON].Info  = portraitIcon;
	dlgList[DLG_PAGESETUP]->Gadgets[PORTRAIT_ICON].WidthOffset  = portraitIcon->Width;
	dlgList[DLG_PAGESETUP]->Gadgets[PORTRAIT_ICON].HeightOffset = portraitIcon->Height;
	dlgList[DLG_PAGESETUP]->Gadgets[LANDSCAPE_ICON].Info = landscapeIcon;
	dlgList[DLG_PAGESETUP]->Gadgets[LANDSCAPE_ICON].WidthOffset  = landscapeIcon->Width;
	dlgList[DLG_PAGESETUP]->Gadgets[LANDSCAPE_ICON].HeightOffset = landscapeIcon->Height;
	dlgList[DLG_PAGESETUP]->Gadgets[PRINTERNAME1_TEXT].Info = printerName;
/*
	Display dialog and set initial settings
*/
	BeginWait();
	if ((dlg = GetDialog(dlgList[DLG_PAGESETUP], screen, mainMsgPort)) == NULL) {
		EndWait();
		Error(ERR_NO_MEM);
		return (FALSE);
	}
	gadgList = dlg->FirstGadget;
	OutlineOKButton(dlg);
	SetGadgetItemValue(gadgList, pageSize, dlg, NULL, 1);
	SetGadgetItemValue(gadgList, ADJUST_BOX, dlg, NULL, adjust);
	EnableGadgetItem(gadgList, ADJUST_BOX, dlg, NULL, graphicPrinter);
	SetGadgetItemValue(gadgList, NOGAPS_BOX, dlg, NULL, noGaps);
	EnableGadgetItem(gadgList, NOGAPS_BOX, dlg, NULL, !pagePrinter);
	gadget = GadgetItem(gadgList, PORTRAIT_ICON);
	gadget->Activation &= ~RELVERIFY;
	if (orient == PRT_PORTRAIT)
		HiliteGadget(gadget, dlg, NULL, 1);
	gadget = GadgetItem(gadgList, LANDSCAPE_ICON);
	gadget->Activation &= ~RELVERIFY;
	if (orient == PRT_LANDSCAPE)
		HiliteGadget(gadget, dlg, NULL, 1);
	EnableGadgetItem(gadgList, LANDSCAPE_ICON, dlg, NULL, graphicPrinter);
	ShowWidthHeight(width, height);
	NumToString(scale, strBuff);
	SetEditItemText(dlg, NULL, REDUCE_TEXT, strBuff);
/*
	Handle dialog
*/
	done = FALSE;
	do {
		offItem = onItem = -1;
		strItem = REDUCE_TEXT;
		item = ModalDialog(mainMsgPort, dlg, PageSetupDialogFilter);
		switch (item) {
		case OK_BUTTON:
		case CANCEL_BUTTON:
			done = TRUE;
			break;
		case USLETTER_RADBTN:
		case USLEGAL_RADBTN:
		case A4LETTER_RADBTN:
		case WIDECARRIAGE_RADBTN:
			offItem = pageSize;
			pageSize = onItem = item;
			CalcWidthHeight(pageSize, &width, &height);
			ShowWidthHeight(width, height);
			break;
		case CUSTOM_RADBTN:
			offItem = pageSize;
			pageSize = onItem = item;
			strItem = WIDTH_TEXT;
			break;
		case NOGAPS_BOX:
			if (pagePrinter)
				break;
			noGaps = !noGaps;
			SetGadgetItemValue(gadgList, NOGAPS_BOX, dlg, NULL, noGaps);
			break;
		case PORTRAIT_ICON:
		case LANDSCAPE_ICON:
			if (!graphicPrinter)
				break;
			orient = (item == PORTRAIT_ICON) ? PRT_PORTRAIT : PRT_LANDSCAPE;
			gadget = GadgetItem(gadgList, PORTRAIT_ICON);
			HiliteGadget(gadget, dlg, NULL, (item == PORTRAIT_ICON));
			gadget = GadgetItem(gadgList, LANDSCAPE_ICON);
			HiliteGadget(gadget, dlg, NULL, (item == LANDSCAPE_ICON));
			break;
			break;
		case ADJUST_BOX:
			if (!graphicPrinter)
				break;
			adjust = !adjust;
			SetGadgetItemValue(gadgList, ADJUST_BOX, dlg, NULL, adjust);
			break;
		}
		if (!done && item != -1) {
			if (offItem != -1)
				SetGadgetItemValue(gadgList, offItem, dlg, NULL, 0);
			if (onItem != -1)
				SetGadgetItemValue(gadgList, onItem, dlg, NULL, 1);
			gadget = GadgetItem(gadgList, strItem);
			ActivateGadget(gadget, dlg, NULL);
		}
	} while (!done);
	width = GetFracValue(gadgList, WIDTH_TEXT);
	height = GetFracValue(gadgList, HEIGHT_TEXT);
	GetEditItemText(gadgList, REDUCE_TEXT, strBuff);
	scale = StringToNum(strBuff);
	DisposeDialog(dlg);
	EndWait();
	if (item == CANCEL_BUTTON)
		return (FALSE);
/*
	Save options
*/
	if (pageSize == USLETTER_RADBTN)
		printRec->PageSize = PRT_USLETTER;
	else if (pageSize == USLEGAL_RADBTN)
		printRec->PageSize = PRT_USLEGAL;
	else if (pageSize == A4LETTER_RADBTN)
		printRec->PageSize = PRT_A4LETTER;
	else if (pageSize == WIDECARRIAGE_RADBTN)
		printRec->PageSize = PRT_WIDECARRIAGE;
	else
		printRec->PageSize = PRT_CUSTOM;
	if (width < MIN_PAPERWIDTH || height < MIN_PAPERHEIGHT ||
		(!noGaps && height < MIN_PAPERHEIGHT + 720) ||
		scale < MIN_SCALE || scale > MAX_SCALE)
		Error(ERR_PAGE_SIZE);		/* Will be fixed by PrValidate */
	printRec->PaperWidth = width;
	printRec->PaperHeight = height;
	printRec->XScale = printRec->YScale = scale;
	printRec->Orientation = orient;
	SetPrintFlag(printRec, PRT_ASPECTADJ, adjust);
	SetPrintFlag(printRec, PRT_NOGAPS, noGaps);
/*
	Set paper and page rectangles
*/
	PrValidate(printRec);
	return (TRUE);
}

/*
 *	Print dialog filter
 */

static BOOL PrintDialogFilter(register IntuiMsgPtr intuiMsg, WORD *item)
{
	register WORD itemHit;
	register ULONG class;
	register UWORD qualifier;
	register GadgetPtr gadget;

	class = intuiMsg->Class;
	if (intuiMsg->IDCMPWindow == dlg && (class == GADGETDOWN || class == GADGETUP)) {
		qualifier = intuiMsg->Qualifier;
		itemHit = GadgetNumber((GadgetPtr) intuiMsg->IAddress);
		if (class == GADGETDOWN &&
			itemHit >= FIRSTPAGE_TEXT && itemHit <= LASTPAGE_TEXT) {
			ReplyMsg((MsgPtr) intuiMsg);
			*item = FROM_RADBTN;
			return (TRUE);
		}
		if (class == GADGETUP && !(qualifier & IEQUALIFIER_NUMERICPAD) &&
			itemHit >= FIRSTPAGE_TEXT && itemHit <= LASTPAGE_TEXT) {
			if (qualifier & SHIFTKEYS)
				itemHit--;
			else
				itemHit++;
			if (itemHit < FIRSTPAGE_TEXT || itemHit > LASTPAGE_TEXT)
				return (FALSE);
			ReplyMsg((MsgPtr) intuiMsg);
			gadget = GadgetItem(dlg->FirstGadget, itemHit);
			ActivateGadget(gadget, dlg, NULL);
			Delay(5);					/* Wait for gadget to become active */
			*item = -1;
			return (TRUE);
		}
		if (itemHit == DENSITYUP_ARROW || itemHit == DENSITYDOWN_ARROW) {
			ReplyMsg((MsgPtr) intuiMsg);
			*item = (class == GADGETDOWN) ? itemHit : -1;
			return (TRUE);
		}
	}
	return (DialogFilter(intuiMsg, item));
}

/*
 *	Display and handle print dialog
 *	Return success status
 */

BOOL PrintDialog(PrintRecPtr printRec)
{
	register WORD item, copies, firstPage, lastPage;
	WORD offItem, onItem, strItem, qualityItem, feedItem, colorsItem;
	WORD maxXDPI, maxYDPI;
	WORD density, maxDensity;
	WORD xDPI[7], yDPI[7];
	BOOL done, printAll, collate;
	register GadgetPtr gadget, gadgList;

	PrValidate(printRec);		/* Calls GetPrinterType() */
/*
	Get initial settings
*/
	GetPrintDensity((WORD) printRec->PrintDensity, &maxXDPI, &maxYDPI);
	printAll = TRUE;
	copies = printRec->Copies;
	firstPage = printRec->FirstPage;
	lastPage = printRec->LastPage;
	collate = (printRec->Flags & PRT_COLLATE);
	qualityItem = (printRec->Quality == PRT_FULLRES) ? HIGH_RADBTN : NORMAL_RADBTN;
	feedItem = (printRec->PaperFeed == PRT_CONTINUOUS) ?
			   AUTOMATIC_RADBTN : HANDFEED_RADBTN;
	switch (printRec->Depth) {
	case PRT_DEPTHFULL:
		colorsItem = COLORSFULL_RADBTN;
		break;
	case PRT_DEPTHEXTENDED:
		colorsItem = COLORSEXT_RADBTN;
		break;
	default:
		colorsItem= COLORSSTD_RADBTN;
		break;
	}
/*
	Get list of print densities
*/
	BeginWait();
	for (density = 1; density <= 7; density++) {
		if (!GetPrintDensity(density, &xDPI[density - 1], &yDPI[density - 1]))
			xDPI[density - 1] = yDPI[density - 1] = 0;
	}
	for (maxDensity = 7; maxDensity > 1; maxDensity--) {
		if (xDPI[maxDensity - 1] != xDPI[maxDensity - 2] ||
			yDPI[maxDensity - 1] != yDPI[maxDensity - 2])
			break;
	}
	density = printRec->PrintDensity;
	if (density < 1)
		density = 1;
	else if (density > maxDensity)
		density = maxDensity;
/*
	Get dialog and set initial values
*/
	if ((dlg = GetDialog(dlgList[DLG_PRINT], screen, mainMsgPort)) == NULL) {
		EndWait();
		Error(ERR_NO_MEM);
		return (FALSE);
	}
	OutlineOKButton(dlg);
	gadgList = dlg->FirstGadget;
	DrawArrowBorder(dlg, DENSITYUP_ARROW);
	EnableGadgetItem(gadgList, OK_BUTTON, dlg, NULL, graphicPrinter);
	SetGadgetItemText(gadgList, PRINTERNAME2_TEXT, dlg, NULL, printerName);
	SetGadgetItemValue(gadgList, qualityItem, dlg, NULL, 1);
	SetGadgetItemValue(gadgList, ALL_RADBTN, dlg, NULL, 1);
	SetGadgetItemValue(gadgList, feedItem, dlg, NULL, 1);
	SetGadgetItemValue(gadgList, colorsItem, dlg, NULL, 1);
	SetGadgetItemValue(gadgList, COLLATE_BOX, dlg, NULL, collate);
	NumToString(copies, strBuff);
	SetEditItemText(dlg, NULL, COPIES_TEXT, strBuff);
	NumToString(firstPage, strBuff);
	SetEditItemText(dlg, NULL, FIRSTPAGE_TEXT, strBuff);
	NumToString(lastPage, strBuff);
	SetEditItemText(dlg, NULL, LASTPAGE_TEXT, strBuff);
	gadget = GadgetItem(gadgList, COPIES_TEXT);
	gadget->Activation |= LONGINT;
	gadget = GadgetItem(gadgList, FIRSTPAGE_TEXT);
	gadget->Activation |= LONGINT;
	gadget = GadgetItem(gadgList, LASTPAGE_TEXT);
	gadget->Activation |= LONGINT;
	SetDensityText(density, strBuff);
	SetGadgetItemText(gadgList, DENSITY_TEXT, dlg, NULL, strBuff);
	if (density == 1)
		EnableGadgetItem(gadgList, DENSITYDOWN_ARROW, dlg, NULL, FALSE);
	if (density == maxDensity)
		EnableGadgetItem(gadgList, DENSITYUP_ARROW, dlg, NULL, FALSE);
/*
	Handle dialog
*/
	done = FALSE;
	do {
		offItem = onItem = -1;
		strItem = COPIES_TEXT;
		item = ModalDialog(mainMsgPort, dlg, PrintDialogFilter);
		switch (item) {
		case OK_BUTTON:
		case CANCEL_BUTTON:
			if (!graphicPrinter)
				item = CANCEL_BUTTON;
			done = TRUE;
			break;
		case HIGH_RADBTN:
		case NORMAL_RADBTN:
			offItem = qualityItem;
			qualityItem = onItem = item;
			break;
		case ALL_RADBTN:
			offItem = FROM_RADBTN;
			onItem = ALL_RADBTN;
			printAll = TRUE;
			break;
		case FROM_RADBTN:
			offItem = ALL_RADBTN;
			onItem = FROM_RADBTN;
			strItem = FIRSTPAGE_TEXT;
			printAll = FALSE;
			break;
		case AUTOMATIC_RADBTN:
		case HANDFEED_RADBTN:
			offItem = feedItem;
			feedItem = onItem = item;
			break;
		case COLORSFULL_RADBTN:
		case COLORSEXT_RADBTN:
		case COLORSSTD_RADBTN:
			offItem = colorsItem;
			colorsItem = onItem = item;
			break;
		case COLLATE_BOX:
			collate = !collate;
			SetGadgetItemValue(gadgList, COLLATE_BOX, dlg, NULL, collate);
			break;
		case DENSITYUP_ARROW:
		case DENSITYDOWN_ARROW:
			if (!graphicPrinter)
				break;
			if (item == DENSITYUP_ARROW) {
				if (density < maxDensity)
					density++;
			}
			else {
				if (density > 1)
					density--;
			}
			SetDensityText(density, strBuff);
			SetGadgetItemText(gadgList, DENSITY_TEXT, dlg, NULL, strBuff);
			EnableGadgetItem(gadgList, DENSITYDOWN_ARROW, dlg, NULL, (density > 1));
			EnableGadgetItem(gadgList, DENSITYUP_ARROW,   dlg, NULL, (density < maxDensity));
			break;
		}
		if (!done && item != -1) {
			if (offItem != -1)
				SetGadgetItemValue(gadgList, offItem, dlg, NULL, 0);
			if (onItem != -1)
				SetGadgetItemValue(gadgList, onItem, dlg, NULL, 1);
			gadget = GadgetItem(gadgList, strItem);
			ActivateGadget(gadget, dlg, NULL);
		}
	} while (!done);
	GetEditItemText(gadgList, COPIES_TEXT, strBuff);
	copies = CheckNumber(strBuff) ? StringToNum(strBuff) : -1;
	GetEditItemText(gadgList, FIRSTPAGE_TEXT, strBuff);
	firstPage = CheckNumber(strBuff) ? StringToNum(strBuff) : -1;
	GetEditItemText(gadgList, LASTPAGE_TEXT, strBuff);
	lastPage = CheckNumber(strBuff) ? StringToNum(strBuff) : -1;
	DisposeDialog(dlg);
	EndWait();
	if (item == CANCEL_BUTTON)
		return (FALSE);
/*
	Check values for acceptability and set return values
*/
	if (copies <= 0) {
		Error(ERR_BAD_COPIES);
		return (FALSE);
	}
	if (printAll) {
		firstPage = 0;
		lastPage = 9999;
	}
	else if (firstPage < 0 || lastPage < 0 || firstPage > lastPage) {
		Error(ERR_PAGE_NUM);
		return (FALSE);
	}
	printRec->Quality = (qualityItem == HIGH_RADBTN) ? PRT_FULLRES : PRT_GRAPHIC;
	printRec->PaperFeed = (feedItem == AUTOMATIC_RADBTN) ? PRT_CONTINUOUS : PRT_CUTSHEET;
	SetPrintFlag(printRec, PRT_SMOOTH, FALSE);
	SetPrintFlag(printRec, PRT_COLLATE, collate);
	switch (colorsItem) {
	case COLORSFULL_RADBTN:
		printRec->Depth = PRT_DEPTHFULL;
		break;
	case COLORSEXT_RADBTN:
		printRec->Depth = PRT_DEPTHEXTENDED;
		break;
	default:
		printRec->Depth = PRT_DEPTHSTANDARD;
		break;
	}
	printRec->Copies = copies;
	printRec->FirstPage = firstPage;
	printRec->LastPage = lastPage;
	printRec->PrintDensity = density;
	return (TRUE);
}

/*
 *	Set print option of given option name
 *	If printRec is NULL, set default print record
 *	Used for AREXX macros
 *	Return FALSE if not a valid option name
 */

BOOL SetPrintOption(PrintRecPtr printRec, TextPtr optName, WORD len)
{
	register WORD i;

	if (printRec == NULL)
		printRec = &defaults.PrintRec;
	for (i = 0; i < NUM_OPTIONS; i++) {
		if (CmpString(optName, printOptNames[i], len, (WORD) strlen(printOptNames[i]), FALSE)
			== 0)
			break;
	}
	if (i >= NUM_OPTIONS)
		return (FALSE);
	switch (i) {
	case OPT_AUTOFEED:
		printRec->PaperFeed = PRT_CONTINUOUS;
		break;
	case OPT_HANDFEED:
		printRec->PaperFeed = PRT_CUTSHEET;
		break;
	case OPT_COLLATE:
	case OPT_NOCOLLATE:
		SetPrintFlag(printRec, PRT_COLLATE, (i == OPT_COLLATE));
		break;
	}
	PrValidate(printRec);
	return (TRUE);
}
