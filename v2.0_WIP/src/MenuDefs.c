/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991-92 New Horizons Software
 *
 *	Menu definitions
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <Toolbox/Menu.h>
#include <Toolbox/Language.h>

#include "Draw.h"

/*
 *	External variables
 */

extern TextChar	strMacroNames1[10][32], strMacroNames2[10][32];

/*
 *	Project menu items
 */

#if (AMERICAN | BRITISH)

static MenuItemTemplate defaultsItems[] = {
	{ MENU_TEXT_ITEM, MENU_ENABLED,	  0, 0, 0, "Save", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,	  0, 0, 0, "Save As...", NULL },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED,	  0, 0, 0, "Load...", NULL },
	{ MENU_NO_ITEM }
};

static MenuItemTemplate importItems[] = {
	{ MENU_TEXT_ITEM, MENU_ENABLED,	  0, 0, 0, "Picture...", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,	  0, 0, 0, "EPSF...", NULL },
	{ MENU_NO_ITEM }
};

static MenuItemTemplate projectItems[] = {
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'N', 0, 0, "New", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'O', 0, 0, "Open...", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'W', 0, 0, "Close", NULL },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Import", importItems },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Export", importItems },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Links...", NULL },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'S', 0, 0, "Save", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Save As...", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Revert", NULL },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Page Setup...", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Print One", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Print...", NULL },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Settings", defaultsItems },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'Q', 0, 0, "Quit", NULL },
	{ MENU_NO_ITEM }
};

#endif

/*
 *	Edit menu items
 */

#if (AMERICAN | BRITISH)

static MenuItemTemplate hotLinksItems[] = {
	{ MENU_TEXT_ITEM, MENU_ENABLED, 0, 0, 0, "Publish...", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 0, 0, 0, "Subscribe...", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 0, 0, 0, "Update", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 0, 0, 0, "Information...", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 0, 0, 0, "Break Link", NULL },
	{ MENU_NO_ITEM }
};

static MenuItemTemplate flipItems[] = {
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Horizontal", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Vertical", NULL },
	{ MENU_NO_ITEM }
};
static MenuItemTemplate polyItems[] = {
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Close", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Open", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'E', 0, 0, "Smooth", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Unsmooth", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Merge", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Unmerge", NULL },
	{ MENU_NO_ITEM }
};

static MenuItemTemplate editItems[] = {
/*
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'Z', 0, 0, "Undo", NULL },
	{ MENU_TEXT_ITEM },
*/
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'X', 0, 0, "Cut", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'C', 0, 0, "Copy", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'V', 0, 0, "Paste", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Erase", NULL },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "HotLinks", hotLinksItems },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'D', 0, 0, "Duplicate", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Flip", flipItems },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Rotate...", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Scale...", NULL },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Convert to Polygon", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Polygon", polyItems },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'A', 0, 0, "Select All", NULL },
	{ MENU_NO_ITEM }
};

#endif

/*
 *	Layout menu
 */

#if (AMERICAN | BRITISH)

static MenuItemTemplate layoutItems[] = {
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKED, 0, 0, 0, "Normal Size", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 0, 0, 0, "Enlarge", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 0, 0, 0, "Reduce", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 0, 0, 0, "Fit to Window", NULL },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_TOGGLE, 0, 0, 0, "Grid Snap", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 0, 0, 0, "Grid Size...", NULL },
	{ MENU_TEXT_ITEM },
#if AMERICAN
	{ MENU_TEXT_ITEM, MENU_ENABLED, 0, 0, 0, "Pen Colors...", NULL },
#elif BRITISH
	{ MENU_TEXT_ITEM, MENU_ENABLED, 0, 0, 0, "Pen Colours...", NULL },
#endif
	{ MENU_TEXT_ITEM, MENU_ENABLED, 0, 0, 0, "Fill Patterns...", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 0, 0, 0, "Layers...", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 0, 0, 0, "Drawing Size...", NULL },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Preferences...", NULL },
#if AMERICAN
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Screen Colors...", NULL },
#elif BRITISH
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Screen Colours...", NULL },
#endif
	{ MENU_NO_ITEM }
};

#endif

/*
 *	Arrange menu
 */

#if (AMERICAN | BRITISH)

static MenuItemTemplate arrangeItems[] = {
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'F', 0, 0, "Move Forward", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Move to Front", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'J', 0, 0, "Move Backward", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Move to Back", NULL },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'K', 0, 0, "Align to Grid", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Align Objects...", NULL },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'G', 0, 0, "Group", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Ungroup", NULL },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'L', 0, 0, "Lock", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Unlock", NULL },
	{ MENU_NO_ITEM }
};

#endif

/*
 *	Pen menu
 */

#if (AMERICAN | BRITISH)

static MenuItemTemplate penSizeItems[] = {
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE, 0, 0, 0x7E, "Hairline", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKED,   0, 0, 0x7D, " 1 Point", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE, 0, 0, 0x7B, " 2 Points", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE, 0, 0, 0x77, " 4 Points", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE, 0, 0, 0x6F, " 6 Points", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE, 0, 0, 0x5F, " 8 Points", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE, 0, 0, 0x3F, "10 Points", NULL },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 0, 0, 0x3F, "Other...", NULL },
	{ MENU_NO_ITEM }
};

static MenuItemTemplate penItems[] = {
	{ MENU_TEXT_ITEM, MENU_ENABLED, 0, 0, 0, "Width", penSizeItems },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 0, 0, 0, "Height", penSizeItems },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKED,   0, 0, 0x30, "No Arrows", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE, 0, 0, 0x08, "Arrow at Start", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE, 0, 0, 0x08, "Arrow at End", NULL },
	{ MENU_NO_ITEM }
};

#endif

/*
 *	Text menu
 */

#if (AMERICAN | BRITISH)

static MenuItemTemplate fontItems[] = {
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'T', 0, 0, "Other...", NULL },
	{ MENU_TEXT_ITEM },
	{ MENU_NO_ITEM }
};

static MenuItemTemplate fontSizeItems[] = {
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'T', 0, 0, "Other...", NULL },
	{ MENU_TEXT_ITEM },
	{ MENU_NO_ITEM }
};

static MenuItemTemplate textItems[] = {
	{ MENU_TEXT_ITEM, MENU_ENABLED, 0, 0, 0, "Font", fontItems },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 0, 0, 0, "Size", fontSizeItems },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKED, 'P',              0, 0xC8,
		"Plain", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_TOGGLE,  'B',       FSF_BOLD, 0x04,
		"Bold", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_TOGGLE,  'I',     FSF_ITALIC, 0x04,
		"Italic", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_TOGGLE,  'U', FSF_UNDERLINED, 0x04,
		"Underline", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_TOGGLE, 0, 0, 0x84, "Condensed" },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_TOGGLE, 0, 0, 0x44, "Expanded" },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKED,   '[', 0, 0xC00, "Left Aligned", NULL },
#if AMERICAN
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE, '=', 0, 0x900, "Centered", NULL },
#elif BRITISH
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE, '=', 0, 0x900, "Centred", NULL },
#endif
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE, ']', 0, 0x600, "Right Aligned", NULL },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKED,   '1', 0, 0xC000, "Single Space", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE,   0, 0, 0x9000, "1-1/2 Space", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE, '2', 0, 0x6000, "Double Space", NULL },
	{ MENU_NO_ITEM }
};

#endif

/*
 *	View menu
 */

static MenuItemTemplate displayItems[] = {
	{ MENU_TEXT_ITEM, MENU_ENABLED, 0, 0, 0, "Ameristar 1600GX", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 0, 0, 0, "GfxBase GDA-1", NULL },
	{ MENU_NO_ITEM }
};

#if (AMERICAN | BRITISH)

static MenuItemTemplate viewItems[] = {
	{ MENU_TEXT_ITEM, MENU_ENABLED, 0, 0, 0, "About DesignWorks...", NULL },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_TOGGLE,   0, 0, 0, "Show Back Layers", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_TOGGLE, 'R', 0, 0, "Show Rulers", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_TOGGLE,   0, 0, 0, "Show Grid Lines", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_TOGGLE,   0, 0, 0, "Show Page Breaks", NULL },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_TOGGLE,   0, 0, 0, "Show Toolbox", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_TOGGLE,   0, 0, 0, "Show Pen Palette", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_TOGGLE,   0, 0, 0, "Show Fill Palette", NULL },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 0, 0, 0, "Display", displayItems },
	{ MENU_TEXT_ITEM },
	{ MENU_NO_ITEM }
};

#endif

/*
 *	Macro menu
 */

#if (AMERICAN | BRITISH)

static MenuItemTemplate macroItems[] = {
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, strMacroNames2[0], NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, strMacroNames2[1], NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, strMacroNames2[2], NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, strMacroNames2[3], NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, strMacroNames2[4], NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, strMacroNames2[5], NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, strMacroNames2[6], NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, strMacroNames2[7], NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, strMacroNames2[8], NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, strMacroNames2[9], NULL },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'M', 0, 0, "Other...", NULL },
	{ MENU_TEXT_ITEM },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 0, 0, 0, "Customize...", NULL },
	{ MENU_NO_ITEM }
};

#endif

/*
 *	Menu strip templates for document window
 */

#if (AMERICAN | BRITISH)

MenuTemplate docWindMenus[] = {
	{ " Project ",	projectItems },
	{ " Edit ",		editItems },
	{ " Layout ",	layoutItems },
	{ " Arrange ",	arrangeItems },
	{ " Pen ",		penItems },
	{ " Text ",		textItems },
	{ " View ",		viewItems },
	{ " Macro ",	macroItems },
	{ NULL, NULL }
};

MenuTemplate altWindMenus[] = {			/* For lo-res screens */
	{ " Proj",		projectItems },
	{ " Edit",		editItems },
	{ " Layout",	layoutItems },
	{ " Arrng",		arrangeItems },
	{ " Pen",		penItems },
	{ " Text",		textItems },
	{ " View",		viewItems },
	{ " Macro",		macroItems },
	{ NULL, NULL }
};

#endif
