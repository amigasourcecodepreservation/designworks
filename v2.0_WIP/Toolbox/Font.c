/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Toolbox
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	Font routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>
#include <libraries/diskfont.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>
#include <proto/diskfont.h>
#include <proto/dos.h>

#include <string.h>

#include <Toolbox/Memory.h>
#include <Toolbox/List.h>
#include <Toolbox/Font.h>
#include <Toolbox/Utility.h>

/*
 *	External variables
 */

extern struct Library		*DiskfontBase;

/*
 *	Local variables and definitions
 */

static ListHeadPtr	fontNameList;	// List of unique font names
static WORD			numFontNames;

static TextAttr	*fontTable;			// Table of TextAttr's for existing fonts
static WORD		numAttrs;

static FontNum	defaultFontNum;

typedef struct AvailFonts		*AvailFontsPtr;
typedef struct AvailFontsHeader	*AFHeaderPtr;

#define MIN(a,b)	((a)<(b)?(a):(b))

/*
 *	Local prototypes
 */

static FontSize FontNearestSize(FontNum, FontSize);

static BOOL	ValidFont(AvailFontsPtr);

/*
 *	Return number of font names
 */

FontNum NumFonts()
{
	return (numFontNames);
}

/*
 *	Find and return the font number for the given font
 *	fontName may or may not have trailing ".font"
 *	If font not found then return -1
 */

FontNum FontNumber(TextPtr fontName)
{
	WORD		len;
	TextChar	name[50];

	strcpy(name, fontName);
	len = strlen(name);
	if (len < 5 || CmpString(name + len - 5, ".font", 5, 5, FALSE) != 0)
		strcat(name, ".font");
	return ((FontNum) FindListItem(fontNameList, name));
}

/*
 *	Get name of specified font, without trailing ".font"
 *	If no such font, return empty string
 */

void GetFontName(FontNum fontNum, TextPtr fontName)
{
	TextPtr	name;

	if (fontNum >= numFontNames)
		*fontName = '\0';
	else {
		name = GetListItem(fontNameList, fontNum);
		strcpy(fontName, name);
		fontName[strlen(fontName) - 5] = '\0';
	}
}

/*
 *	Determine if font is a PostScript font (i.e. has a metric file)
 */

BOOL IsPostScriptFont(FontNum fontNum)
{
	LONG		lock;
	BOOL		isPS;
	TextChar	name[50];

	isPS = FALSE;
	if (fontNum < numFontNames) {
		strcpy(name, "FONTS:");
		GetFontName(fontNum, name + 6);
		strcat(name, ".metric");
		if ((lock = Lock(name, ACCESS_READ)) != NULL) {
			isPS = TRUE;
			UnLock(lock);
		}
	}
	return (isPS);
}

/*
 *	Determine if font is an outline font
 */

BOOL IsOutlineFont(FontNum fontNum)
{
	register WORD		i;
	register TextPtr	fontName;

	if (fontNum < numFontNames) {
		fontName = GetListItem(fontNameList, fontNum);
		for (i = 0; i < numAttrs; i++) {
			if (fontTable[i].ta_Name == fontName)
				return ((fontTable[i].ta_Flags & FPF_DESIGNED) == 0);
		}
	}
	return (FALSE);
}

/*
 *	Return best size to use for scaling to desired size
 *	Note:  Under AmigaDOS 2.0, scaling is built into system, so we just
 *		   return given size
 */

FontSize FontScaleSize(FontNum fontNum, register FontSize reqSize)
{
	register WORD		i;
	register FontSize	size, smallSize, largeSize, doubleSize;
	BOOL				hasHalf, hasDouble;
	TextPtr fontName;

	if (fontNum < 0 || fontNum >= numFontNames)
		return (0);
	if (DiskfontBase && LibraryVersion(DiskfontBase) >= OSVERSION_2_0)
		return ((FontSize) MIN(reqSize, FONT_MAXSIZE));
/*
	Find next larger and next smaller sizes, and if there is a half or double size
*/
	fontName = GetListItem(fontNameList, fontNum);
	smallSize = 0;
	largeSize = 0x7FFF;
	doubleSize = reqSize << 1;
	hasHalf = hasDouble = FALSE;
	for (i = 0; i < numAttrs; i++) {
		if (fontTable[i].ta_Name == fontName) {
			size = fontTable[i].ta_YSize;
			if (size == reqSize)
				return (size);
			if (size < reqSize) {
				if (size > smallSize)
					smallSize = size;
			}
			else {
				if (size < largeSize)
					largeSize = size;
			}
			if ((size << 1) == reqSize)
				hasHalf = TRUE;
			else if (size == doubleSize)
				hasDouble = TRUE;
		}
	}
/*
	Determine which size is best for scaling
*/
	if (hasDouble)
		return (doubleSize);
	if (hasHalf)
		return (reqSize >> 1);
/*
	Choose smaller size over larger size to avoid dropping pixels
*/
	if (smallSize > 0)
		return (smallSize);
	if (largeSize < 0x7FFF)
		return (largeSize);
	return (reqSize);				// No sizes found!
}

/*
 *	Return nearest size to use for desired size
 *	Note:  Under AmigaDOS 2.0, scaling is built into system, so we just
 *		   return given size
 */

static FontSize FontNearestSize(FontNum fontNum, register FontSize reqSize)
{
	register WORD		i;
	register FontSize	size, smallSize, largeSize;
	TextPtr				fontName;

	if (fontNum < 0 || fontNum >= numFontNames)
		return (reqSize);
	if (DiskfontBase && LibraryVersion(DiskfontBase) >= OSVERSION_2_0)
		return ((FontSize) MIN(reqSize, FONT_MAXSIZE));
/*
	Find next larger and next smaller sizes
*/
	fontName = GetListItem(fontNameList, fontNum);
	smallSize = 0;
	largeSize = 0x7FFF;
	for (i = 0; i < numAttrs; i++) {
		if (fontTable[i].ta_Name == fontName) {
			size = fontTable[i].ta_YSize;
			if (size == reqSize)
				return (size);
			if (size < reqSize) {
				if (size > smallSize)
					smallSize = size;
			}
			else {
				if (size < largeSize)
					largeSize = size;
			}
		}
	}
/*
	Choose smaller size over larger size
*/
	if (smallSize > 0)
		return (smallSize);
	if (largeSize < 0x7FFF)
		return (largeSize);
	return (reqSize);				// No sizes found (should never happen)
}

/*
 *	Open font specified by fontNum and fontSize
 *	Under AmigaDOS 1.3 and earlier, will change size to closest available size
 */

TextFontPtr GetNumFont(FontNum fontNum, FontSize size)
{
	TextFontPtr	font;
	TextAttr	textAttr;

	if (fontNum < 0 || fontNum >= numFontNames)
		return (NULL);
/*
	Get nearest available font size (will be requested size under Kickstart 2.0)
*/
	textAttr.ta_Name  = GetListItem(fontNameList, fontNum);
	textAttr.ta_YSize = FontNearestSize(fontNum, size);
	textAttr.ta_Style = FS_NORMAL;
	textAttr.ta_Flags = (FPF_ROMFONT | FPF_DISKFONT | FPF_PROPORTIONAL);
	font = GetFont(&textAttr);
	return (font);
}

/*
 *	Return number of entries in font attr table
 */

WORD NumFontAttrs()
{
	return (numAttrs);
}

/*
 *	Return pointer to font attr table of existing fonts
 */

TextAttrPtr FontAttrTable()
{
	return (fontTable);
}

/*
 *	Determine if this is a valid font for the font table
 */

static BOOL ValidFont(register AvailFontsPtr af)
{
	register UBYTE	flags = af->af_Attr.ta_Flags;
	register UWORD	type = af->af_Type;

	if ((flags & (FPF_REMOVED | FPF_REVPATH))			||
		((type & AFF_MEMORY) && (flags & FPF_DISKFONT))	||
		af->af_Attr.ta_YSize > FONT_MAXSIZE)
		return (FALSE);
	return (TRUE);
}

/*
 *	Initialize font data structures
 *	Return success status
 */

BOOL InitFonts()
{
	register WORD		num, nameNum;
	register FontNum	fontNum;
	register LONG		afBuffSize, needed;
	BOOL				success;
	AFHeaderPtr			afHeader;
	AvailFontsPtr		availFonts;
	TextAttrPtr			textAttr;

	success = FALSE;
	afHeader = NULL;
	numFontNames = numAttrs = 0;
/*
	Get list of disk fonts
*/
	afBuffSize = 0;
	needed = 1000*sizeof(struct AvailFonts) + sizeof(WORD);	// Room for 1000 fonts
	do {
		if (afBuffSize)
			MemFree(afHeader, afBuffSize);
		afBuffSize += needed;
		if ((afHeader = MemAlloc(afBuffSize, 0)) == NULL)
			goto Exit;
		needed = AvailFonts((BYTE *) afHeader, afBuffSize, AFF_MEMORY | AFF_DISK);
	} while (needed > 0);
/*
	Count the number of fonts in table
*/
	availFonts = (AvailFontsPtr) (afHeader + 1);
	for (num = 0; num < afHeader->afh_NumEntries; num++) {
		if (ValidFont(&availFonts[num]))
			numAttrs++;
	}
	if (numAttrs < 0)
		numAttrs = 0x7FFF;
/*
	Allocate memory for table and save pointers to fonts
*/
	if ((fontNameList = CreateList()) == NULL ||
		(fontTable = MemAlloc(numAttrs*sizeof(TextAttr), 0)) == NULL)
		goto Exit;
	fontNum = 0;
	for (num = 0; num < afHeader->afh_NumEntries; num++) {
		if (!ValidFont(&availFonts[num]))
			continue;
		textAttr = &availFonts[num].af_Attr;
		if ((nameNum = FindListItem(fontNameList, textAttr->ta_Name)) == -1)
			nameNum = AddListItem(fontNameList, textAttr->ta_Name, 0, FALSE);
		if (nameNum == -1)
			goto Exit;
		fontTable[fontNum] = *textAttr;
		fontTable[fontNum].ta_Name = GetListItem(fontNameList, nameNum);
		fontNum++;
	}
	numFontNames = NumListItems(fontNameList);		// Cache this value
	success = TRUE;
/*
	All done
*/
Exit:
	if (afHeader)
		MemFree(afHeader, afBuffSize);
	return (success);
}

/*
 *	Deallocate all memory used by InitFonts
 */

void ShutDownFonts()
{
	if (fontTable)
		MemFree(fontTable, numAttrs*sizeof(TextAttr));
	if (fontNameList)
		DisposeList(fontNameList);
}
