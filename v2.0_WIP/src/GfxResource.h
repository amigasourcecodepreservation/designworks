/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/

/* Colorgraphics card resource structure */

#include <exec/nodes.h>

#define OLDRESOURCE

#ifdef OLDRESOURCE
struct cfbResource
{
	struct Node cfbResource_Node;
	unsigned short cfbResource_Flags;
	void *cfbFrameBufferAddr;	/* changes */
	unsigned short	cfbBytesPerRow;
	unsigned short	cfbRows;
	short	DisplayBytesPerRow;
	short	DisplayRows;
	void *cfbPaletteAddr;
	void (*cfbSetRGB)();
	void (*WritePixel)();
	int  (*ReadPixel)();
	void (*HLine)();
	void (*VLine)();
	int (*ChangeMode)();
	char BitsPerPixel;
	char DisplayBufferNumber;
	short maxx;
	void *FrameBufferBaseAddr;	/* never changes */
	short scrollx;
	short scrolly;	/* unused */
	int (*cfbGetRGB)();
	int (*VBInt)();
	int	VBlankCount;
	int VBIntData;
	/* Need ownership stuff yet */
};

/* ChangeMode arguments */
#define CHANGEPIXELSIZE	1	/* a=4,8,15 */
#define CHANGERESOLUTION 2	/* x/y = 640/480,800/600,1024/800 */
#define CHANGEDISPLAYBUFF 3	/* a = 0-3 */
#define CHANGEWRITEBUFF 4	/* a = 0-3 */
#define CHANGESCROLL	5	/* x,y */

#else

/* proposed new resoource (beginning) */
struct cfbResource
{
	struct Node cfbResource_Node;
	unsigned short cfbResource_Flags;
	void	*privateExt;	/* for device itself */
	int	privateExtSize;
	struct	SignalSemaphore owner;	/* presently resource is owned */
					/* exclusively */
	void	*cfbFrameBufferAddr;	/* current frame buffer base */
	short	maxX, maxY;
	short	cfbBytesPerRow;	/* current raster bytes per row */
	short	cfbRows;	/* current raster rows */
	short	DisplayBytesPerRow;
	short	DisplayRows;
	void	(*cfbLoadRGB)();	/* (int start,int count,ulong *clrs) */
	int	(*cfbControl)();	/* set raster buffer, change display */
				/* scroll, zoom, etc. */
	void	ReadPixel();	/* very basic functions */
	void	WritePixel();	/* operate on raw frame buffer */
};
#endif

struct TMC0171
{
	unsigned char dummy1;
	unsigned char pixaddrW;   /* 1 */
	unsigned char dummy3;
	unsigned char colorValue; /* 3 */
	unsigned char dummy4;
	unsigned char pixelMask;  /* 5 */
	unsigned char dummy5;
	unsigned char pixaddrR;   /* 7 */
	unsigned char dummy6;
	unsigned char dummy7;	/* 9 */
	unsigned char dummy8;
	unsigned char dummy9;	/* B */
	unsigned char dummy10;
	unsigned char Command;	/* Bt475/477 D */
	unsigned char dummy11;
	unsigned char dummy12;	/* f */
	unsigned char dummy[0x41-16];
	unsigned char cfbControl;
};

#define CFB_HBLANK	0x4
#define CFB_VBLANK	0x8

