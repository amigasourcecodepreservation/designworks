/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Toolbox library
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Window routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>
#include <intuition/intuitionbase.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include <string.h>

#include "typedefs.h"
#include "Window.h"
#include "Memory.h"

/*
 *	GetWindowRect
 *	Get window rectangle
 */

void GetWindowRect(window, rect)
register WindowPtr window;
register RectPtr rect;
{
	rect->MinX = window->BorderLeft;
	rect->MinY = window->BorderTop;
	rect->MaxX = window->Width - window->BorderRight - 1;
	rect->MaxY = window->Height - window->BorderBottom - 1;
}

/*
 *	InvalRect
 *	Add rectangle to window damage list
 *	First clips rectangle to window display
 */

void InvalRect(WindowPtr window, RectPtr rect)
{
	Rectangle newRect, windRect;

	newRect = *rect;
	GetWindowRect(window, &windRect);
	if (newRect.MinX < windRect.MinX)
		newRect.MinX = windRect.MinX;
	if (newRect.MaxX > windRect.MaxX)
		newRect.MaxX = windRect.MaxX;
	if (newRect.MinY < windRect.MinY)
		newRect.MinY = windRect.MinY;
	if (newRect.MaxY > windRect.MaxY)
		newRect.MaxY = windRect.MaxY;
	if (newRect.MinX <= newRect.MaxX && newRect.MinY <= newRect.MaxY) {
		OrRectRegion(window->WLayer->DamageList, rect);
		window->WLayer->Flags |= LAYERREFRESH;
	}
}

/*
 *	SetWTitle
 *	Set window title to copy of given text
 */

void SetWTitle(WindowPtr window, TextPtr title)
{
	TextPtr newTitle, oldTitle;

	if (title) {
		if ((newTitle = MemAlloc(strlen(title) + 1, 0)) == NULL)
			return;
		strcpy(newTitle, title);
	}
	else
		newTitle = NULL;
	oldTitle = window->Title;
	SetWindowTitles(window, newTitle, (TextPtr) -1);
	if (oldTitle)
		MemFree(oldTitle, strlen(oldTitle) + 1);
}

/*
 *	GetWTitle
 *	Return a copy of window title
 *	Buffer must be large enough to hold title
 */

void GetWTitle(WindowPtr window, TextPtr title)
{
	if (window->Title)
		strcpy(title, window->Title);
	else
		*title = '\0';
}

/*
 *	Set window UserData
 *	Lower two bits of UserData is used for window kind
 */

void SetWRefCon(WindowPtr window, Ptr data)
{
	window->UserData = (APTR) (((ULONG) (window->UserData) & 0x03)
							   | ((ULONG) data & 0xFFFFFFFCL));
}

/*
 *	Return window UserData
 */

Ptr GetWRefCon(WindowPtr window)
{
	return ((Ptr) ((ULONG) (window->UserData) & 0xFFFFFFFCL));
}

/*
 *	Set window kind
 */

void SetWKind(WindowPtr window, WORD kind)
{
	window->UserData = (APTR) (((ULONG) (window->UserData) & 0xFFFFFFFCL)
							   | ((ULONG) kind & 0x03));
}

/*
 *	Return window kind
 */

WORD GetWKind(WindowPtr window)
{
	return ((WORD) ((ULONG) (window->UserData) & 0x03));
}

/*
 *	CloseWindowSafely
 *	Remove window messages and close window
 */

void CloseWindowSafely(WindowPtr window, MsgPortPtr msgPort)
{
	register IntuiMsgPtr msg, nextMsg;

	Forbid();
	msg = (IntuiMsgPtr) msgPort->mp_MsgList.lh_Head;
	while ((nextMsg = (IntuiMsgPtr) msg->ExecMessage.mn_Node.ln_Succ) != NULL) {
		if (msg->IDCMPWindow == window) {
			Remove((NodePtr) msg);
			ReplyMsg((MsgPtr) msg);
		}
		msg = nextMsg;
	}
	window->UserPort = NULL;
	ModifyIDCMP(window, 0);
	Permit();
	CloseWindow(window);
}
