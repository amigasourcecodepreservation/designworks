/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Toolbox library
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	Scroll List handler
 */

#ifndef TOOLBOX_SCROLLLIST_H
#define TOOLBOX_SCROLLLIST_H

#ifndef INTUITION_INTUITION_H
#include <intuition/intuition.h>
#endif

#ifndef TYPEDEFS_H
#include <Typedefs.h>
#endif

#ifndef TOOLBOX_MEMORY_H
#include <Toolbox/Memory.h>
#endif

#ifndef TOOLBOX_LIST_H
#include <Toolbox/List.h>
#endif

/*
 *	Scroll list structure
 */

typedef struct {
	GadgetPtr	ListBox;					// Border gadget for list
	GadgetPtr	UpArrow;					// Up arrow
	GadgetPtr	DownArrow;					// Down arrow
	GadgetPtr	Slider;						// Scroll box
	WindowPtr	Window;
	RequestPtr	Request;
	TextFontPtr	Font;						// Font for list elements
	ListHeadPtr	List;						// The actual list
	WORD		XOffset, YOffset, Flags;	// Private
	void		(*DrawProc)(RastPtr, TextPtr, RectPtr);
	WORD		PrevItem;
	ULONG		PrevSeconds, PrevMicros;
	BOOL		DoubleClick;
} ScrollList, *ScrollListPtr;

/*
 *	Scroll modes
 */

#define SL_NOSELECT		0x00
#define SL_SINGLESELECT	0x01
#define SL_MULTISELECT	0x02

/*
 *	Standard scroll list gadget template entries
 */

#define SL_GADG_BOX(left, top, width, num)	\
	{ GADG_ACTIVE_BORDER, left, top, 0, 0, width-ARROW_WIDTH, (num)*11, -1, num, NULL }

#define SL_GADG_UPARROW(left, top, width, num)	\
	{ GADG_ACTIVE_STDIMAGE,														\
		(left)+(width)-ARROW_WIDTH, (top)+(num)*11-2*ARROW_HEIGHT, 0, (num)+1,	\
		ARROW_WIDTH, ARROW_HEIGHT, 0, 0, 0, 0, (Ptr) IMAGE_ARROW_UP }

#define SL_GADG_DOWNARROW(left, top, width, num)	\
	{ GADG_ACTIVE_STDIMAGE,														\
		(left)+(width)-ARROW_WIDTH, (top)+(num)*11-ARROW_HEIGHT, 0, (num)+1,	\
		ARROW_WIDTH, ARROW_HEIGHT, 0, 0, 0, 0, (Ptr) IMAGE_ARROW_DOWN }

#define SL_GADG_SLIDER(left, top, width, num)	\
	{ GADG_PROP_VERT | GADG_PROP_NOBORDER,		\
		(left)+(width)-ARROW_WIDTH, top, 1, 0,	\
		ARROW_WIDTH, (num)*11-2*ARROW_HEIGHT, -2, num, NULL }

/*
 *	Prototypes
 */

ScrollListPtr	NewScrollList(WORD);

void	InitScrollList(ScrollListPtr, GadgetPtr, WindowPtr, RequestPtr);
void	DisposeScrollList(ScrollListPtr);

void	SLDoDraw(ScrollListPtr, BOOL);
void	SLDrawBorder(ScrollListPtr);
void	SLDrawItem(ScrollListPtr, WORD);
void	SLDrawList(ScrollListPtr);

void	SLAddItem(ScrollListPtr, TextPtr, WORD, WORD);
void	SLChangeItem(ScrollListPtr, WORD, TextPtr, WORD);
void	SLRemoveItem(ScrollListPtr, WORD);
void	SLRemoveAll(ScrollListPtr);

WORD	SLNumItems(ScrollListPtr);
void	SLGetItem(ScrollListPtr, WORD, TextPtr);

WORD	SLNextSelect(ScrollListPtr, WORD);
BOOL	SLIsSelected(ScrollListPtr, WORD);
void	SLSelectItem(ScrollListPtr, WORD, BOOL);
void	SLUnSelectAll(ScrollListPtr, WORD);

void	SLSetItemStyle(ScrollListPtr, WORD, WORD);
void	SLEnableItem(ScrollListPtr, WORD, BOOL);

void	SLGadgetMessage(ScrollListPtr, MsgPortPtr, IntuiMsgPtr);
void	SLCursorKey(ScrollListPtr, TextChar);

BOOL	SLIsDoubleClick(ScrollListPtr);
WORD	SLDoubleClickItem(ScrollListPtr);

void	SLAutoScroll(ScrollListPtr, WORD);
void	SLHorizScroll(ScrollListPtr, WORD);

void	SLSetSelectMode(ScrollListPtr, WORD);
void	SLSetDrawProc(ScrollListPtr, void (*)(RastPtr, TextPtr, RectPtr));

#endif
