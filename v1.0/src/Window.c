/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Window routines
 */

#include <exec/types.h>
#include <exec/memory.h>
#include <graphics/gfxmacros.h>
#include <graphics/regions.h>
#include <intuition/intuition.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/layers.h>
#include <proto/intuition.h>
#include <proto/dos.h>				/* For Delay() prototype */

#include <string.h>

#include <Toolbox/Memory.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Menu.h>
#include <Toolbox/Gadget.h>
#include <Toolbox/Window.h>
#include <Toolbox/Utility.h>

#include "Draw.h"
#include "Proto.h"

/*
 *	External variables
 */

extern WORD	intuiVersion;

extern struct NewWindow	newWindow, newBackWindow;

extern ScreenPtr	screen;
extern MsgPortPtr	mainMsgPort;
extern MsgPort		monitorMsgPort;
extern WindowPtr	backWindow, toolWindow, penWindow, fillWindow;
extern WindowPtr	windowList[];
extern WORD			numWindows;
extern MenuPtr		docMenuStrip;

extern WindowPtr	cmdWindow;		/* Last active window */

extern WindowPtr	closeWindow;
extern BOOL			closeFlag, closeAllFlag;

extern GadgetTemplate	windowGadgets[];

extern BOOL	onPubScreen, smartWindows;

extern InvColorTable	scrnInvColorTable;

extern UBYTE	blackColor, whiteColor, darkColor, lightColor;
extern UWORD	grayPat[];

extern TextChar	strPageNum[], strScreenTitle[];
extern TextChar	strBuff[];

/*
 *	Local variables and definitions
 */

typedef struct  {
	LayerPtr	Layer;
	Rectangle	Bounds;
	LONG		OffsetX, OffsetY;
} BackFillMsg, *BFMsgPtr;

static struct Hook	backFillHook, docBackFillHook;

/*
 *	Local prototypes
 */

/*#void __saveds __asm BackFill(register struct Hook *, register RastPtr, register BFMsgPtr);
#void __saveds __asm DocBackFill(register struct Hook *, register RastPtr, register BFMsgPtr);
*/
void	ClearBackWindow(RastPtr, Rectangle *);

void	RefreshWindow(WindowPtr);

/*
 *	Back fill routine for backWindow
 */

static void __saveds __asm BackFill(register __a0 struct Hook *hook,
									register __a2 RastPtr rPort,
									register __a1 BFMsgPtr bfMsg)
{
	RastPort newRPort;

	newRPort = *rPort;			/* Make a copy so we can modify it */
	newRPort.Layer = NULL;
	ClearBackWindow(&newRPort, &bfMsg->Bounds);
}

/*
 *	Back fill routine for backWindow
 */

static void __saveds __asm DocBackFill(register __a0 struct Hook *hook,
									   register __a2 RastPtr rPort,
									   register __a1 BFMsgPtr bfMsg)
{
	RastPort newRPort;

	newRPort = *rPort;			/* Make a copy so we can modify it */
	newRPort.Layer = NULL;
	newRPort.RP_User = (APTR *) &scrnInvColorTable;
	RGBForeColor(&newRPort, RGBCOLOR_WHITE);
	SetDrMd(&newRPort, JAM1);
	RectFill(&newRPort, bfMsg->Bounds.MinX, bfMsg->Bounds.MinY,
			 bfMsg->Bounds.MaxX, bfMsg->Bounds.MaxY);
}

/*
 *	Clear backWindow
 *	The scrnInvColorTable may not yet be initialized when this routine is called,
 *		so it must not use RGBForeColor() or RGBBackColor()
 */

static void ClearBackWindow(RastPtr rPort, RectPtr rect)
{
	if (screen->BitMap.Depth >= 3) {
		SetAPen(rPort, lightColor);
		SetDrMd(rPort, JAM1);
	}
	else {
		SetAPen(rPort, darkColor);
		SetBPen(rPort, whiteColor);
		SetDrMd(rPort, JAM2);
		SetAfPt(rPort, grayPat, 1);
	}
	RectFill(rPort, rect->MinX, rect->MinY, rect->MaxX, rect->MaxY);
}

/*
 *	Open backWindow
 */

WindowPtr OpenBackWindow()
{
	ULONG oldIDCMPFlags;
	Rectangle rect;

	newBackWindow.Screen = screen;
	if (onPubScreen) {
		newBackWindow.Width = newBackWindow.Height = 1;
		newBackWindow.Flags |= WFLG_NOCAREREFRESH;
	}
	else {
		newBackWindow.Width = screen->Width;
		newBackWindow.Height = screen->Height;
	}
	oldIDCMPFlags = newBackWindow.IDCMPFlags;
	newBackWindow.IDCMPFlags = 0;
	if (intuiVersion < OSVERSION_2_0)
		backWindow = OpenWindow(&newBackWindow);
	else {
		if (onPubScreen)
			newBackWindow.Type = PUBLICSCREEN;
		newBackWindow.Flags |= WFLG_NOCAREREFRESH;
		backFillHook.h_Entry = (ULONG (*)()) BackFill;
		backWindow = OpenWindowTags(&newBackWindow,
									WA_BackFill, &backFillHook,
									TAG_END);
	}
	if (backWindow == NULL)
		return (NULL);
	backWindow->UserPort = &monitorMsgPort;
	ModifyIDCMP(backWindow, oldIDCMPFlags);
	backWindow->RPort->RP_User = (APTR *) &scrnInvColorTable;
	SetWKind(backWindow, WKIND_BACK);
	if (onPubScreen) {
		ScreenToFront(screen);			/* In case it is not in front */
		WindowToBack(backWindow);
		SetWindowTitles(backWindow, (TextPtr) -1, strScreenTitle);
	}
	else if (intuiVersion < OSVERSION_2_0) {
		SetRect(&rect, 0, 0, backWindow->Width - 1, backWindow->Height - 1);
		ClearBackWindow(backWindow->RPort, &rect);
	}
	SetStdPointer(backWindow, POINTER_ARROW);
	return (backWindow);
}

/*
 *	Get content rectangle
 */

void GetContentRect(WindowPtr window, RectPtr rect)
{
	GetWindowRect(window, rect);
	rect->MinX += RulerWidth(window);
	rect->MinY += RulerHeight(window);
}

/*
 *	Install full window clip region
 */

void SetWindowClip(WindowPtr window)
{
	Rectangle rect;

	GetWindowRect(window, &rect);
	SetRectClip(window->WLayer, &rect);
}

/*
 *	Install content clip region for window
 */

void SetContentClip(WindowPtr window)
{
	Rectangle rect;

	GetContentRect(window, &rect);
	SetRectClip(window->WLayer, &rect);
}

/*
 *	Determine if window is document window
 */

BOOL IsDocWindow(WindowPtr window)
{
	return ((BOOL) (WindowNum(window) != -1));
}

/*
 *	Return number of window in windowList, or -1 if not present
 */

WORD WindowNum(window)
register WindowPtr window;
{
	register WORD i;

	for (i = 0; i < numWindows; i++) {
		if (window == windowList[i])
			return (i);
	}
	return (-1);
}

/*
 *	Create new document window with the specified title
 *	Return pointer to window if successful, NULL if not
 */

WindowPtr CreateWindow(TextPtr title)
{
	register ULONG oldIDCMPFlags;
	register WindowPtr window;
	GadgetPtr gadgList;
	DocDataPtr docData;
	WORD zoomSize[4];

/*
	Create and attach window gadgets
*/
	if ((gadgList = GetGadgets(windowGadgets)) == NULL)
		return (NULL);
	GadgetItem(gadgList, UP_ARROW)->Activation		|= GACT_RIGHTBORDER;
	GadgetItem(gadgList, DOWN_ARROW)->Activation	|= GACT_RIGHTBORDER;
	GadgetItem(gadgList, VERT_SCROLL)->Activation	|= GACT_RIGHTBORDER;
	GadgetItem(gadgList, LEFT_ARROW)->Activation	|= GACT_BOTTOMBORDER;
	GadgetItem(gadgList, RIGHT_ARROW)->Activation	|= GACT_BOTTOMBORDER;
	GadgetItem(gadgList, HORIZ_SCROLL)->Activation	|= GACT_BOTTOMBORDER;
	GadgetItem(gadgList, LAYERUP_ARROW)->Activation	|= GACT_BOTTOMBORDER;
	GadgetItem(gadgList, LAYERDOWN_ARROW)->Activation	|= GACT_BOTTOMBORDER;
	newWindow.FirstGadget = gadgList;
/*
	Open window and set message port to monitorMsgPort
*/
	newWindow.Screen = screen;
	newWindow.Type = CUSTOMSCREEN;
	newWindow.Flags = (smartWindows) ?
					  (newWindow.Flags & ~WFLG_REFRESHBITS) | WFLG_SMART_REFRESH :
					  (newWindow.Flags & ~WFLG_REFRESHBITS) | WFLG_SIMPLE_REFRESH;
	newWindow.LeftEdge = 0;
	newWindow.TopEdge = (numWindows + 1)*screen->BarHeight + 1;
	if (screen->ViewPort.Modes & LACE)
		newWindow.TopEdge++;		/* Two pixel wide line reduces flicker */
	newWindow.Width = screen->Width;
	newWindow.Height = screen->Height - newWindow.TopEdge;
	newWindow.Title = NULL;
	oldIDCMPFlags = newWindow.IDCMPFlags;
	newWindow.IDCMPFlags = 0;
	if (intuiVersion < OSVERSION_2_0)
		window = OpenWindow(&newWindow);
	else {
		zoomSize[0] = zoomSize[1] = 0;
		zoomSize[2] = screen->Width;
		zoomSize[3] = screen->Height;
		docBackFillHook.h_Entry = (ULONG (*)()) DocBackFill;
		window = OpenWindowTags(&newWindow,
								WA_Zoom, zoomSize,
								WA_BackFill, &docBackFillHook,
								TAG_END);
	}
	newWindow.IDCMPFlags = oldIDCMPFlags;	/* For next CreateWindow call */
	if (window == NULL) {
		DisposeGadgets(gadgList);
		return (NULL);
	}
	SetStdPointer(window, POINTER_ARROW);
	SetWTitle(window, title);			/* Set to copy of title */
	if (onPubScreen)
		SetWindowTitles(window, (TextPtr) -1, strScreenTitle);
	window->UserPort = &monitorMsgPort;
	ModifyIDCMP(window, oldIDCMPFlags);
/*
	Fill window with white (while menus are being created)
*/
	window->RPort->RP_User = (APTR *) &scrnInvColorTable;
	SetWindowClip(window);
	RGBBackColor(window->RPort, RGBCOLOR_WHITE);
	SetRast(window->RPort, window->RPort->BgPen);
/*
	Attach document menu strip
*/
	InsertMenuStrip(window, docMenuStrip);
/*
	Allocate new DocData record
*/
	if ((docData = MemAlloc(sizeof(DocData), MEMF_CLEAR)) == NULL) {
		RemoveWindow(window);
		return (NULL);
	}
	SetWRefCon(window, docData);
	SetWKind(window, WKIND_DOC);
	docData->Window = window;
	docData->PrintRec = MemAlloc(sizeof(PrintRecord), MEMF_CLEAR);
	if (docData->PrintRec == NULL) {
		RemoveWindow(window);
		return (NULL);
	}
	strcpy(docData->WindowName, title);
	SetLayerIndic(window);
/*
	Insert this window in the window list
*/
	AddWindowItem(window);
	ToolToFront();
	return (window);
}

/*
 *	Remove the specified document window and free all memory it allocated
 */

void RemoveWindow(WindowPtr window)
{
	GadgetPtr gadgList;
	RegionPtr clipRgn;
	DocDataPtr docData;

/*
	Shut down window messages, get pointers to structures, and close window
*/
	ClearMenuStrip(window);
	gadgList = GadgetItem(window->FirstGadget, 0);	/* First user gadget */
	clipRgn = InstallClipRegion(window->WLayer, NULL);
	docData = (DocDataPtr) GetWRefCon(window);
	SetWTitle(window, NULL);
	CloseWindowSafely(window, mainMsgPort);
/*
	Free memory used by menus, gadgets, DocData, and clipRegion
*/
	if (docData) {
		if (docData->PrintRec)
			MemFree(docData->PrintRec, sizeof(PrintRecord));
		MemFree(docData, sizeof(DocData));
	}
	if (gadgList)
		DisposeGadgets(gadgList);
	if (clipRgn)
		DisposeRegion(clipRgn);
/*
	Remove window from window list
*/
	RemoveWindowItem(window);
	if (cmdWindow == window)
		cmdWindow = backWindow;
}

/*
 *	Handle click in go-away region of window
 */

void DoGoAwayWindow(WindowPtr window, UWORD modifier)
{
	closeFlag = FALSE;
	closeWindow = NULL;
	if (IsDocWindow(window)||
		window == toolWindow || window == penWindow || window == fillWindow) {
		closeWindow = window;
		closeFlag = TRUE;
	}
/*
	If alt key down then close all windows
*/
	if (IsDocWindow(window) && (modifier & ALTKEYS)) {
		closeWindow = NULL;
		closeFlag = FALSE;
		closeAllFlag = TRUE;
	}
}

/*
 *	Set window title to concatenation of window name and layer name
 */

void AdjustWindowTitle(WindowPtr window)
{
	TextPtr layerName;
	DocDataPtr docData = GetWRefCon(window);

	if (!IsDocWindow(window))
		return;
	strcpy(strBuff, docData->WindowName);
	if ((layerName = GetLayerName(CurrLayer(docData))) != NULL) {
		strcat(strBuff, ":");
		strcat(strBuff, layerName);
	}
	SetWTitle(window, strBuff);
}

/*
 *	Handle window activate/inactivate events
 */

void DoWindowActivate(WindowPtr window, BOOL activate)
{
	WindowPtr activeWindow;

	UndoOff();
	if (IsDocWindow(window)) {
		cmdWindow = window;
		activeWindow = ActiveWindow();
		SetLayerIndic(window);
		if (activate) {
			SetPointerShape();
			RulerIndicOn(window);
			HiliteSelectOn(window);
			GetClipboard();
			SetAllMenus();
		}
		else {
			HiliteSelectOff(window);
			RulerIndicOff(window);
			SetStdPointer(window, POINTER_ARROW);
			if (!IsDocWindow(activeWindow)) {
				SetAllMenus();
				PutClipboard();
			}
		}
	}
}

/*
 *	Install clip region for window, and adjust scroll bars
 */

void DoNewSize(register WindowPtr window)
{
	SetContentClip(window);
	SetLayerIndic(window);
	AdjustScrollBars(window);
}

/*
 *	Update document window contents (refresh through damage list)
 */

void UpdateWindow(WindowPtr window)
{
	RegionPtr oldClip;
	LayerPtr layer = window->WLayer;
	Rectangle rect, bounds;

	oldClip = GetClip(layer);
	LockLayerInfo(&window->WScreen->LayerInfo);		/* Recommended procedure */
	GetContentRect(window, &rect);
	bounds = layer->DamageList->bounds;
	if (bounds.MinX < rect.MinX || bounds.MinY < rect.MinY) {
		RulerIndicOff(window);
		SetWindowClip(window);
		BeginUpdate(layer);
		DrawRuler(window);
		EndUpdate(layer, FALSE);
		RulerIndicOn(window);
	}
	SectRect(&rect, &bounds, &bounds);
	SetContentClip(window);
	BeginUpdate(layer);
	DrawDocument(window, &bounds);
	EndUpdate(layer, TRUE);
	layer->Flags &= ~LAYERREFRESH;		/* EndUpdate doesn't do this */
	SetClip(layer, oldClip);
	UnlockLayerInfo(&window->WScreen->LayerInfo);
}

/*
 *	Check to see if any document windows need updating, and update if so
 */

void UpdateWindows()
{
	register WORD i;
	register WindowPtr window, activeWindow;

	CheckColorTable();
	activeWindow = ActiveWindow();
	for (i = 0; i < numWindows; i++) {
		window = windowList[i];
		if (window->WLayer->Flags & LAYERREFRESH) {
			if (window == activeWindow)
				SetStdPointer(window, POINTER_WAIT);
			UpdateWindow(window);
			if (window == activeWindow)
				SetPointerShape();
		}
	}
}

/*
 *	Update document window contents (refresh through damage list)
 *	Similar to UpdateWindow() except it uses BeginRefresh/EndRefresh
 */

static void RefreshWindow(WindowPtr window)
{
	RegionPtr oldClip;
	LayerPtr layer = window->WLayer;
	Rectangle rect, bounds;

	SetLayerIndic(window);
	oldClip = GetClip(layer);
	LockLayerInfo(&window->WScreen->LayerInfo);		/* Recommended procedure */
	GetContentRect(window, &rect);
	bounds = layer->DamageList->bounds;
	if (bounds.MinX < rect.MinX || bounds.MinY < rect.MinY) {
		RulerIndicOff(window);
		SetWindowClip(window);
		BeginRefresh(window);
		DrawRuler(window);
		EndRefresh(window, FALSE);
		RulerIndicOn(window);
	}
	SetContentClip(window);
	BeginRefresh(window);
	DrawDocument(window, &bounds);
	EndRefresh(window, TRUE);
	SetClip(layer, oldClip);
	UnlockLayerInfo(&window->WScreen->LayerInfo);
}

/*
 *	Refresh window contents (intuition-generated event)
 */

void DoWindowRefresh(register WindowPtr window)
{
	WindowPtr activeWindow;
	Rectangle rect;

	CheckColorTable();				/* Avoid double-redraws */
	if (window == NULL ||
		(!IsDocWindow(window) && window != backWindow) ||
		(window == backWindow && onPubScreen))
		return;
/*
	Do the actual refresh
*/
	if (window == backWindow) {
		GetWindowRect(window, &rect);
		BeginRefresh(window);
		ClearBackWindow(window->RPort, &rect);
		EndRefresh(window, TRUE);
	}
	else {
		activeWindow = ActiveWindow();
		if (window == activeWindow)
			SetStdPointer(window, POINTER_WAIT);
		RefreshWindow(window);
		if (window == activeWindow)
			SetPointerShape();
	}
}

/*
 *	Check for and handle window refresh messages
 */

void RefreshWindows()
{
	WORD item;
	IntuiMsgPtr intuiMsg;

	Delay(5L);			/* Wait until requester or window is gone */
	while (intuiMsg = (IntuiMsgPtr) GetMsg(mainMsgPort)) {
		if (!DialogFilter(intuiMsg, &item))	/* Get refresh message */
			ReplyMsg((MsgPtr) intuiMsg);
	}
}
