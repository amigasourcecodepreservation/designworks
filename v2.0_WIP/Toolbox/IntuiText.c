/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Toolbox library
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	IntuiText routines
 */

#include <exec/types.h>
#include <exec/memory.h>
#include <intuition/intuition.h>

#include <string.h>

#include <proto/exec.h>
#include <proto/intuition.h>

#include <TypeDefs.h>

#include <Toolbox/Globals.h>
#include <Toolbox/Memory.h>
#include <Toolbox/IntuiText.h>

/*
 *	NewIntuiText
 *	Allocate new IntuiText and set to given parameters
 *	A copy of the text string is used
 *	Return NULL if error
 */

IntuiTextPtr NewIntuiText(WORD left, WORD top, TextPtr text, WORD style, WORD pen)
{
	TextPtr			newText;
	IntuiTextPtr	intuiText;
	TextAttrPtr		textAttr;

	if ((intuiText = MemAlloc(sizeof(IntuiText), MEMF_CLEAR)) == NULL)
		return (NULL);
	if (style != FS_NORMAL &&
		(textAttr = MemAlloc(sizeof(TextAttr), 0)) == NULL) {
		MemFree(intuiText, sizeof(IntuiText));
		return (NULL);
	}
	if (text == NULL)
		newText = NULL;
	else {
		if ((newText = MemAlloc(strlen(text) + 1, 0)) == NULL) {
			MemFree(textAttr, sizeof(TextAttr));
			MemFree(intuiText, sizeof(IntuiText));
			return (NULL);
		}
		strcpy(newText, text);
	}
	intuiText->FrontPen	= pen;
	intuiText->DrawMode	= JAM1;
	intuiText->LeftEdge	= left;
	intuiText->TopEdge	= top;
	intuiText->IText	= newText;
	if (style != FS_NORMAL) {
		*textAttr = _tbTextAttr;
		textAttr->ta_Style = style;
		intuiText->ITextFont = textAttr;
	}
	else
		intuiText->ITextFont = &_tbTextAttr;
	return (intuiText);
}

/*
 *	FreeIntuiText
 *	Free memory allocated by NewIntuiText
 */

void FreeIntuiText(IntuiTextPtr intuiText)
{
	IntuiTextPtr	nextText;

	while (intuiText) {
		nextText = intuiText->NextText;
		if (intuiText->ITextFont != &_tbTextAttr)
			MemFree(intuiText->ITextFont, sizeof(TextAttr));
		if (intuiText->IText)
			MemFree(intuiText->IText, strlen(intuiText->IText) + 1);
		MemFree(intuiText, sizeof(IntuiText));
		intuiText = nextText;
	}
}

/*
 *	AppendIntuiText
 *	Append intuiText to end of another intuitext
 */

void AppendIntuiText(IntuiTextPtr intuiText1, IntuiTextPtr intuiText2)
{
	while (intuiText1->NextText)
		intuiText1 = intuiText1->NextText;
	intuiText1->NextText = intuiText2;
}

/*
 *	ChangeIntuiText
 *	Change text used by intuiText structure
 *	A copy of the text is used
 *	Return success status
 */

BOOL ChangeIntuiText(IntuiTextPtr intuiText, TextPtr text)
{
	TextPtr	oldText, newText;

	if (intuiText == NULL)
		return (FALSE);
	if (text == NULL)
		newText = NULL;
	else {
		if ((newText = MemAlloc(strlen(text) + 1, 0)) == NULL)
			return (FALSE);
		strcpy(newText, text);
	}
	oldText = intuiText->IText;
	intuiText->IText = newText;
	if (oldText)
		MemFree(oldText, strlen(oldText) + 1);
	return (TRUE);
}

/*
 *	SetIntuiTextFont
 *	Set font and style of intuiText, uses a copy of the textAttr passed
 *	Return success status
 */

BOOL SetIntuiTextFont(IntuiTextPtr intuiText, TextAttrPtr textAttr)
{
	TextAttrPtr	newAttr;

	if (intuiText == NULL ||
		(newAttr = MemAlloc(sizeof(TextAttr), 0)) == NULL)
		return (FALSE);
	*newAttr = *textAttr;
	if (intuiText->ITextFont != &_tbTextAttr)
		MemFree(intuiText->ITextFont, sizeof(TextAttr));
	intuiText->ITextFont = newAttr;
	return (TRUE);
}
