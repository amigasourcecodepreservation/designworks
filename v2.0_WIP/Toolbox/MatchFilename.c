/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Toolbox
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	Match filename to pattern
 */

#include <exec/types.h>

#include <string.h>

#include <Typedefs.h>

#include <Toolbox/Memory.h>
#include <Toolbox/Utility.h>

#include <proto/dos.h>

/*
 *	Local variables and definitions
 */

#define MAX_LEN_FILENAME	32
#define MAX_LEN_PATTERN		50

/*
 *	External variables
 */

extern struct DosLibrary	*DOSBase;

/*
 *	Local prototypes
 */

static BOOL	DOSMatchFilename(TextPtr, TextPtr);
static BOOL SimpleMatchFilename(TextPtr, TextPtr);

/*
 *	Match file name to pattern using DOS routine
 */

static BOOL DOSMatchFilename(TextPtr pattern, TextPtr filename)
{
	TextChar	buff[MAX_LEN_PATTERN*2 + 2];

	if (ParsePatternNoCase(pattern, buff, MAX_LEN_PATTERN*2 + 2) == -1)
		return (FALSE);
	return (MatchPatternNoCase(buff, filename));
}

/*
 *	Simple match file name to pattern
 *	Handles only '?' and '#' tokens
 *	Pattern must be in upper case
 *	Recursively handles '#' token
 */

static BOOL SimpleMatchFilename(register TextPtr pattern, register TextPtr filename)
{
	register TextChar	chPat;
	register WORD		i, j, maxChars, numMove;
	WORD				patLen, nameLen;
	BOOL				match, found;

	patLen = strlen(pattern);
	nameLen = strlen(filename);
/*
	Scan for match and search for start of first '#' token
	Ignore '#' at end of line
*/
	maxChars = (patLen < nameLen) ? patLen : nameLen;
	found = FALSE;
	for (i = 0; i < maxChars; i++) {
		chPat = pattern[i];
		if (chPat == '#') {
			found = TRUE;
			break;
		}
		else if (chPat != '?' && chPat != toUpper[filename[i]])
			break;
	}
	if (found && i == patLen - 1) {
		patLen--;
		found = FALSE;
	}
/*
	Check for match when no '#' token found
*/
	if (!found)
		match = (i == maxChars && nameLen == patLen);
/*
	Handle '#' token
*/
	else {
		match = FALSE;
		chPat = pattern[i + 1];
		maxChars = nameLen - i;
		if (maxChars + patLen > MAX_LEN_PATTERN)
			maxChars = MAX_LEN_PATTERN - patLen;
		numMove = patLen - i - 1;					// Moves trailing '\0'
		BlockMove(pattern + i + 2, pattern + i, numMove);
		for (j = 0; j <= maxChars; j++) {
			if (j > 0) {
				BlockMove(pattern + i, pattern + i + 1, numMove + j - 1);
				pattern[i] = chPat;
			}
			if (SimpleMatchFilename(pattern, filename)) {
				match = TRUE;
				break;
			}
		}
		if (!match) {							// Restore old pattern for next pass
			BlockMove(pattern + i + maxChars, pattern + i + 2, numMove);
			pattern[i] = '#';
			pattern[i + 1] = chPat;
		}
	}
/*
	Return result
*/
	return (match);
}

/*
 *	Match file name to pattern
 *	Side effect: Converts pattern to all upper case
 */

BOOL MatchFilename(register TextPtr pattern, register TextPtr filename)
{
	register WORD	i, patLen;
	BOOL			match;
	TextChar		newPattern[MAX_LEN_PATTERN + 1];

	if ((patLen = strlen(pattern)) > MAX_LEN_PATTERN)
		return (FALSE);
/*
	Make pattern all upper case (also helps in DOSMatchFilename)
*/
	for (i = 0; i < patLen; i++)
		newPattern[i] = toUpper[pattern[i]];
	newPattern[patLen] = '\0';
/*
	Match file name using DOS routine, or do it ourselves
*/
	if (LibraryVersion((struct Library *) DOSBase) >= OSVERSION_2_0)
		match = DOSMatchFilename(newPattern, filename);
	else
		match = SimpleMatchFilename(newPattern, filename);
	return (match);
}
