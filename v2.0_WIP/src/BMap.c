/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Bitmap handling routines
 */

#include <exec/types.h>
#include <graphics/gfxmacros.h>
#include <intuition/intuition.h>

#include <proto/graphics.h>
#include <proto/layers.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include <Toolbox/Memory.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Window.h>
#include <Toolbox/Utility.h>
#include <Toolbox/DOS.h>

#include <string.h>

#include "Draw.h"
#include "Proto.h"

/*
 *	External variables
 */

extern ScreenPtr	screen;

extern Options	options;

extern TextChar	strBuff[];

/*
 *	Local variables and definitions
 */

typedef ULONG	RGB32Color;

#define RED_32(rgb)		(((rgb) >> 16) & 0xFF)
#define GREEN_32(rgb)	(((rgb) >> 8) & 0xFF)
#define BLUE_32(rgb)	((rgb) & 0xFF)

#define RGB32COLOR(r,g,b)	(((ULONG) (r) << 16) + ((ULONG) (g) << 8) + (ULONG) (b))

#define RGB_TO_RGB32(rgb)	RGB32COLOR((RED(rgb) << 4) + RED(rgb),		\
									   (GREEN(rgb) << 4) + GREEN(rgb),	\
									   (BLUE(rgb) << 4) + BLUE(rgb))

#define RGB32_TO_RGB(rgb)	RGBCOLOR(RED_32(rgb) >> 4, GREEN_32(rgb) >> 4, BLUE_32(rgb) >> 4)

#define RGBDARKEN(rgb)	RGBCOLOR(RED(rgb) >> 1, GREEN(rgb) >> 1, BLUE(rgb) >> 1)

static UWORD chip cropPointerData[] = {
	0x0020, 0x0000,	0x0020, 0x0000,	0x1FF8, 0x0000,	0x1020, 0x0000,
	0x1020, 0x0000,	0x1020, 0x0300, 0x1020, 0x0300,	0x1020,	0x0000,
	0x1020,	0x0000, 0x7FE0, 0x0000,	0x1000, 0x0000,	0x1000, 0x0000,
};

static Pointer cropPointer = {
	12, 12, -6, -6, &cropPointerData[0]
};

/*
 *	Local prototypes
 */

static void	DrawBMapObj(RastPtr, BMapObjPtr, RectPtr, RectPtr);

static void	BMapDrawCache(BMapObjPtr, RastPtr);

/*
 *	Allocate a new bitmap object
 */

BMapObjPtr BMapAllocate()
{
	return ((BMapObjPtr) MemAlloc(sizeof(BMapObj), MEMF_CLEAR));
}

/*
 *	Dispose of bitmap object
 */

void BMapDispose(BMapObjPtr bMapObj)
{
	LONG	dataSize;

	if (bMapObj->FileName)
		MemFree(bMapObj->FileName, strlen(bMapObj->FileName) + 1);
	if (bMapObj->Data) {
		dataSize = (LONG) bMapObj->Width*bMapObj->Height*sizeof(RGBColor);
		MemFree(bMapObj->Data, dataSize);
	}
	BMapDisposeCache(bMapObj);
	MemFree(bMapObj, sizeof(BMapObj));
}

/*
 *	Draw bitmap object
 */

static void DrawBMapObj(RastPtr rPort, BMapObjPtr bMapObj, RectPtr rect, RectPtr clipRect)
{
	register WORD	x, y, width, height, xStart;
	RGBColor		rgbColor, oldColor;
	Point			pt;
	Rectangle		drawRect, bounds;

/*
	Draw picture
	Try to minimize calls to RGBForeColor()
*/
	if (!SectRect(rect, clipRect, &drawRect))
		return;

	width = bMapObj->Width;
	height = bMapObj->Height;
	SetRect(&bounds, 0, 0, width - 1, height - 1);
	for (y = drawRect.MinY; y <= drawRect.MaxY; y++) {
		xStart = -1;
		for (x = drawRect.MinX; x <= drawRect.MaxX; x++) {
			pt.x = x;
			pt.y = y;
			MapPt(&pt, rect, &bounds);
			rgbColor = bMapObj->Data[pt.y*width + pt.x];
			if (rgbColor != RGB_TRANSPARENT) {
				if (xStart == -1) {
					oldColor = rgbColor;
					xStart = x;
				}
				else if (rgbColor != oldColor) {
					RGBForeColor(rPort, oldColor);
					if (xStart == x - 1)
						WritePixel(rPort, xStart, y);
					else {
						Move(rPort, xStart, y);
						Draw(rPort, x - 1, y);
					}
					oldColor = rgbColor;
					xStart = x;
				}
			}
			else if (xStart != -1) {
				RGBForeColor(rPort, oldColor);
				if (xStart == x - 1)
					WritePixel(rPort, xStart, y);
				else {
					Move(rPort, xStart, y);
					Draw(rPort, x - 1, y);
				}
				xStart = -1;
			}
		}
		if (xStart != -1) {
			RGBForeColor(rPort, oldColor);
			if (xStart == x - 1)
				WritePixel(rPort, xStart, y);
			else {
				Move(rPort, xStart, y);
				Draw(rPort, x - 1, y);
			}
		}
	}
}

/*
 *	Draw bitmap object or place holder
 */

void BMapDrawObj(RastPtr rPort, BMapObjPtr bMapObj, RectPtr rect, RectPtr clipRect)
{
	WORD		dstWidth, dstHeight, srcX, srcY, sizeX, sizeY;
	PLANEPTR	mask;
	BOOL		updating;
	LayerPtr	layer;
	BitMapPtr	bitMap;
	Rectangle	rect1, rect2, newClipRect, viewRect;
	RastPort	tempPort1, tempPort2;
	BitMap		tempBitMap1, tempBitMap2;

	viewRect = bMapObj->ViewRect;
	OffsetRect(&viewRect, rect->MinX - bMapObj->DocObj.Frame.MinX,
						  rect->MinY - bMapObj->DocObj.Frame.MinY);

	if (!SectRect(clipRect, &viewRect, &newClipRect))
		return;
/*
	If not showing pictures, draw place holder
*/
	if (!options.ShowPictures && rPort->BitMap == screen->RastPort.BitMap) {
		PenNormal(rPort);
		FrameRect(rPort, rect);
		Move(rPort, rect->MinX, rect->MinY);
		Draw(rPort, rect->MaxX, rect->MaxY);
		Move(rPort, rect->MaxX, rect->MinY);
		Draw(rPort, rect->MinX, rect->MaxY);
	}
/*
	If picture caching is on, draw cached piture (creating one if necessary)
	(Turn off updating while building cache to avoid blocking Intuition input)
*/
	else if (options.CachePictures && rPort->BitMap == screen->RastPort.BitMap) {
		if (bMapObj->PictCache == NULL) {
			layer = rPort->Layer;
			updating = (layer && (layer->Flags & LAYERUPDATING));
			if (updating) {
				EndUpdate(layer, FALSE);
				UnlockLayerInfo(layer->LayerInfo);
			}
			BMapCreateCache(bMapObj, rPort);
			if (updating) {
				LockLayerInfo(layer->LayerInfo);
				BeginUpdate(layer);
			}
		}
		if (bMapObj->PictCache == NULL)
			DrawBMapObj(rPort, bMapObj, rect, &newClipRect);
		else {
			dstWidth  = rect->MaxX - rect->MinX + 1;
			dstHeight = rect->MaxY - rect->MinY + 1;
			if (dstWidth == bMapObj->Width && dstHeight == bMapObj->Height) {
				bitMap = bMapObj->PictCache;
				mask   = bMapObj->Mask;
			}
			else {
				bitMap = CreateBitMap(dstWidth, dstHeight, rPort->BitMap->Depth, TRUE);
				mask   = (bMapObj->Mask) ? AllocRaster(dstWidth, dstHeight) : NULL;
				SetRect(&rect1, 0, 0, bMapObj->Width - 1, bMapObj->Height - 1);
				SetRect(&rect2, 0, 0, dstWidth - 1, dstHeight - 1);
				if (bitMap) {
					tempPort1.BitMap = bMapObj->PictCache;
					tempPort2.BitMap = bitMap;
					ScaleBitMap(&tempPort1, &tempPort2, &rect1, &rect2);
				}
				if (mask) {
					InitBitMap(&tempBitMap1, 1, bMapObj->Width, bMapObj->Height);
					InitBitMap(&tempBitMap2, 1, dstWidth, dstHeight);
					tempBitMap1.Planes[0] = bMapObj->Mask;
					tempBitMap2.Planes[0] = mask;
					tempPort1.BitMap = &tempBitMap1;
					tempPort2.BitMap = &tempBitMap2;
					ScaleBitMap(&tempPort1, &tempPort2, &rect1, &rect2);
				}
			}
			SectRect(rect, &newClipRect, &rect1);
			srcX  = rect1.MinX - rect->MinX;
			srcY  = rect1.MinY - rect->MinY;
			sizeX = rect1.MaxX - rect1.MinX + 1;
			sizeY = rect1.MaxY - rect1.MinY + 1;
			if (bitMap == NULL || (bMapObj->Mask && mask == NULL))
				DrawBMapObj(rPort, bMapObj, rect, &newClipRect);
			else if (mask)
				BltMaskBitMapRastPort(bitMap, srcX, srcY,
									  rPort, rect1.MinX, rect1.MinY,
									  sizeX, sizeY, 0xE0, mask);
			else
				BltBitMapRastPort(bitMap, srcX, srcY,
								  rPort, rect1.MinX, rect1.MinY,
								  sizeX, sizeY, 0xC0);
			WaitBlit();
			if (bitMap && bitMap != bMapObj->PictCache)
				DisposeBitMap(bitMap);
			if (mask && mask != bMapObj->Mask)
				FreeRaster(mask, dstWidth, dstHeight);
		}
	}
/*
	Draw object
*/
	else
		DrawBMapObj(rPort, bMapObj, rect, &newClipRect);
}

/*
 *	Draw outline of bitmap (for creation/dragging)
 *	Offset is in document coordinates
 */

void BMapDrawOutline(WindowPtr window, BMapObjPtr bMapObj, WORD xOffset, WORD yOffset)
{
	RastPtr rPort = window->RPort;
	Rectangle rect;

	rect = bMapObj->ViewRect;
	OffsetRect(&rect, xOffset, yOffset);
	DocToWindowRect(window, &rect, &rect);
	SetDrPt(rPort, LINE_PAT);
	SetDrawComplement(rPort);
	FrameRect(rPort, &rect);
	ClearDrawComplement(rPort);
}

/*
 *	Draw selection handles for bitmap
 */

void BMapHilite(WindowPtr window, BMapObjPtr bMapObj)
{
	RastPtr rPort = window->RPort;
	Rectangle rect;

	DocToWindowRect(window, &bMapObj->ViewRect, &rect);
	DrawHandle(rPort, rect.MinX, rect.MinY);
	if (rect.MaxY != rect.MinY)
		DrawHandle(rPort, rect.MinX, rect.MaxY);
	if (rect.MaxX != rect.MinX)
		DrawHandle(rPort, rect.MaxX, rect.MinY);
	if (rect.MaxX != rect.MinX && rect.MaxY != rect.MinY)
		DrawHandle(rPort, rect.MaxX, rect.MaxY);
}

/*
 *	Rotate bitmap by given angle about center
 */

void BMapRotate(BMapObjPtr bMapObj, WORD cx, WORD cy, WORD angle)
{
	register LONG		x, y, width, height;	// To force LONG calculations
	LONG				dataSize;
	register RGBColor	*newData, *temp;

	angle = NormalizeAngle(angle);
	if (angle % 90)				/* Only 90 degree rotations allowed */
		return;
	RotateRect(&bMapObj->DocObj.Frame, cx, cy, angle);
	RotateRect(&bMapObj->ViewRect, cx, cy, angle);
	dataSize = (LONG) bMapObj->Width*bMapObj->Height*sizeof(RGBColor);
	if ((newData = MemAlloc(dataSize, 0)) == NULL)
		return;
/*
	force building of new cache
*/
	BMapDisposeCache(bMapObj);
	for (; angle > 0; angle -= 90) {
		width = bMapObj->Width;
		height = bMapObj->Height;
		for (x = 0; x < width; x++) {
			for (y = 0; y < height; y++)
				newData[x*height + (height - 1 - y)] = bMapObj->Data[y*width + x];
		}
		bMapObj->Width = height;
		bMapObj->Height = width;
		temp = bMapObj->Data;
		bMapObj->Data = newData;
		newData = temp;
	}
	MemFree(newData, dataSize);
}

/*
 *	Flip bitmap horizontally or vertically about center
 */

void BMapFlip(BMapObjPtr bMapObj, WORD cx, WORD cy, BOOL horiz)
{
	register WORD		x, y, width, height;
	register LONG		i1, i2;
	register RGBColor	temp;

	width = bMapObj->Width;
	height = bMapObj->Height;
	if (horiz) {
		for (y = 0; y < height; y++) {
			for (x = 0; x < width/2; x++) {
				i1 = (LONG) y*width + x;
				i2 = (LONG) y*width + width - 1 - x;
				temp = bMapObj->Data[i1];
				bMapObj->Data[i1] = bMapObj->Data[i2];
				bMapObj->Data[i2] = temp;
			}
		}
	}
	else {
		for (x = 0; x < width; x++) {
			for (y = 0; y < height/2; y++) {
				i1 = y*width + x;
				i2 = (height - 1 - y)*width + x;
				temp = bMapObj->Data[i1];
				bMapObj->Data[i1] = bMapObj->Data[i2];
				bMapObj->Data[i2] = temp;
			}
		}
	}
	FlipRect(&bMapObj->DocObj.Frame, cx, cy, horiz);
	FlipRect(&bMapObj->ViewRect, cx, cy, horiz);
/*
	force building of new cache
*/
	BMapDisposeCache(bMapObj);
}

/*
 *	Scale bitmap to new frame
 */

void BMapScale(BMapObjPtr bMapObj, RectPtr frame)
{
	Rectangle	oldFrame, newFrame, oldViewRect, newViewRect;
	WORD		oldViewWidth, oldViewHeight, newWidth, newHeight, xOffset, yOffset;
	WORD		oldFrameWidth, oldFrameHeight;

	oldFrame = bMapObj->DocObj.Frame;
	bMapObj->DocObj.Frame = newFrame = *frame;
	oldViewRect = bMapObj->ViewRect;
/*
	set new viewRect
*/
	oldViewWidth = oldViewRect.MaxX - oldViewRect.MinX + 1;
	oldViewHeight = oldViewRect.MaxY - oldViewRect.MinY + 1;

	newWidth = newFrame.MaxX - newFrame.MinX + 1;
	newHeight = newFrame.MaxY - newFrame.MinY + 1;

	oldFrameWidth = oldFrame.MaxX - oldFrame.MinX + 1;
	oldFrameHeight = oldFrame.MaxY - oldFrame.MinY + 1;

	xOffset = oldFrame.MinX - oldViewRect.MinX;
	yOffset = oldFrame.MinY - oldViewRect.MinY;

	newViewRect.MinX = newFrame.MinX + xOffset * newWidth / oldFrameWidth;
	newViewRect.MinY = newFrame.MinY + yOffset * newHeight / oldFrameHeight;
	newViewRect.MaxX = newViewRect.MinX + oldViewWidth * newWidth / oldFrameWidth;
	newViewRect.MaxY = newViewRect.MinY + oldViewHeight * newHeight / oldFrameHeight;

	bMapObj->ViewRect = newViewRect;
}

/*
 *	Determine if point is in bitamp
 *	Point is relative to object rectangle
 */

BOOL BMapSelect(BMapObjPtr bMapObj, PointPtr pt)
{
	Point		dataPt;
	Rectangle	frame, rect;

	if (!options.ShowPictures)
		return (TRUE);
	frame = bMapObj->DocObj.Frame;
	OffsetRect(&frame, -frame.MinX, -frame.MinY);
	SetRect(&rect, 0, 0, bMapObj->Width - 1, bMapObj->Height - 1);
	dataPt = *pt;
	MapPt(&dataPt, &frame, &rect);		/* Map to point inside object data */
	if (PtInRect(&dataPt, &rect) &&
		bMapObj->Data[dataPt.y*bMapObj->Width + dataPt.x] != RGB_TRANSPARENT)
		return (TRUE);
	return (FALSE);
}

/*
 *	Return handle number of given point, or -1 if not in handle
 *	Point is relative to object rectangle
 */

WORD BMapHandle(BMapObjPtr bMapObj, PointPtr pt, RectPtr handleRect)
{
	Rectangle	frame;
	WORD		handle;

	frame = bMapObj->DocObj.Frame;
	bMapObj->DocObj.Frame = bMapObj->ViewRect;
	handle = RectHandle((RectObjPtr) bMapObj, pt, handleRect);
	bMapObj->DocObj.Frame = frame;

	return (handle);
}

/*
 *	Duplicate bitmap data to new object
 *	Return success status
 */

BOOL BMapDupData(BMapObjPtr bMapObj, BMapObjPtr newObj)
{
	LONG	dataSize;

	dataSize = (LONG) bMapObj->Width*bMapObj->Height*sizeof(RGBColor);
	if ((newObj->Data = MemAlloc(dataSize, MEMF_CLEAR)) == NULL)
		return (FALSE);
	newObj->Bounds	= bMapObj->Bounds;
	newObj->Width	= bMapObj->Width;
	newObj->Height	= bMapObj->Height;
	BlockMove(bMapObj->Data, newObj->Data, dataSize);
	return (TRUE);
}

/*
 *	Track mouse and change bitmap shape
 */

void BMapGrow(WindowPtr window, BMapObjPtr bMapObj, UWORD modifier, WORD mouseX, WORD mouseY)
{
	Rectangle	oldFrame, newFrame, oldBounds, newBounds;
	WORD		oldWidth, oldHeight, newWidth, newHeight, xOffset, yOffset;
	WORD		oldFrameWidth, oldFrameHeight;

/*
	Save old values
*/
	oldFrame = bMapObj->DocObj.Frame;
	oldBounds = bMapObj->Bounds;
/*
	Track mouse
*/
	TrackRect(window, (RectObjPtr) bMapObj, modifier, mouseX, mouseY, &newFrame, NULL);
/*
	If no change restore old values and return
*/
	if (newViewRect.MinX == oldViewRect.MinX && newViewRect.MinY == oldViewRect.MinY &&
		newViewRect.MaxX == oldViewRect.MaxX && newViewRect.MaxY == oldViewRect.MaxY) {
		bMapObj->DocObj.Frame = oldFrame;
		return;
	}
/*
	Invalidate for old sizes
*/
	InvalObjectRect(window, (DocObjPtr) bMapObj);
	HiliteSelectOff(window);
/*
	Set new view rectangle
*/
	SetFrameRect(&newViewRect, newViewRect.MinX, newViewRect.MinY,
								newViewRect.MaxX, newViewRect.MaxY);
	bMapObj->ViewRect = newViewRect;
/*
	Set new docFrame
*/
	oldWidth = oldViewRect.MaxX - oldViewRect.MinX + 1;
	oldHeight = oldViewRect.MaxY - oldViewRect.MinY + 1;

	newWidth = newViewRect.MaxX - newViewRect.MinX + 1;
	newHeight = newViewRect.MaxY - newViewRect.MinY + 1;

	oldFrameWidth = oldFrame.MaxX - oldFrame.MinX + 1;
	oldFrameHeight = oldFrame.MaxY - oldFrame.MinY + 1;

	xOffset = oldFrame.MinX - oldViewRect.MinX;
	yOffset = oldFrame.MinY - oldViewRect.MinY;

	newFrame.MinX = newViewRect.MinX - xOffset * newWidth / oldWidth;
	newFrame.MinY = newViewRect.MinY - yOffset * newHeight / oldHeight;
	newFrame.MaxX = newFrame.MinX + oldFrameWidth * newWidth / oldWidth;
	newFrame.MaxY = newFrame.MinY + oldFrameHeight * newHeight / oldHeight;

	bMapObj->DocObj.Frame = newFrame;
/*
	Invalidate for new sizes
*/
	HiliteSelectOn(window);
	InvalObjectRect(window, (DocObjPtr) bMapObj);	// to get new view rectangle
}

/*
 *	Draw bitmap object to PostScript file
 */

BOOL BMapDrawPSObj(PrintRecPtr printRec, BMapObjPtr bMapObj, RectPtr rect, RectPtr clipRect)
{
	return (PSBMap(printRec, rect, bMapObj->Width, bMapObj->Height, bMapObj->Data));
}

/*
 *	Convert IFF picture to bitmap object, disposing iffPict
 *	Return success status
 */

BOOL BMapConvert(WindowPtr window, IFFPictPtr iffPict, TextPtr fileName,
						Dir dirLock, BOOL draw)
{
	register WORD		i, byte, x, y, len;
	register LONG		pen, offset;		// pen must be LONG for 24-bit pictures
	WORD				red, green, blue, depth;
	LONG				dataSize;
	BOOL				isHAM, isHalfBrite;
	register BitMapPtr	bitMap;
	register BMapObjPtr	bMapObj;
	register RGB32Color	prevColor32;		// To handle HAM-8 pictures properly
	register RGBColor	*data, color;
	DocDataPtr			docData = GetWRefCon(window);
	Rectangle			frame, contRect;
	struct FileInfoBlock	*fib;
	Dir					lock;

	if (iffPict == NULL || iffPict->Width == 0 || iffPict->Height == 0)
		return (FALSE);
/*
	Allocate object and set frame (centered in window)
*/
	if ((bMapObj = (BMapObjPtr) NewDocObject(CurrLayer(docData), TYPE_BMAP)) == NULL) {
		DisposePict(iffPict);
		return (FALSE);
	}
	SetRect(&frame, 0, 0, iffPict->Width - 1, iffPict->Height - 1);
	GetContentRect(window, &contRect);
	WindowToDocRect(window, &contRect, &contRect);
	x = ((contRect.MaxX - contRect.MinX) - (frame.MaxX - frame.MinX))/2 + contRect.MinX;
	y = ((contRect.MaxY - contRect.MinY) - (frame.MaxY - frame.MinY))/2 + contRect.MinY;
	OffsetRect(&frame, x, y);
	AdjustToDocBounds(docData, &frame);
	bMapObj->DocObj.Frame = frame;
	bMapObj->ViewRect = frame;
/*
	Allocate bitmap data and copy from iffPict
*/
	dataSize = (LONG) iffPict->Width*iffPict->Height*sizeof(RGBColor);
	if ((bMapObj->Data = MemAlloc(dataSize, 0)) == NULL) {
		BMapDispose(bMapObj);
		DisposePict(iffPict);
		return (FALSE);
	}
	bMapObj->Width = iffPict->Width;
	bMapObj->Height = iffPict->Height;
	bitMap = iffPict->BitMap;
	depth = bitMap->Depth;
	data = bMapObj->Data;
	isHAM = ((iffPict->ViewMode & HAM) != 0);
	isHalfBrite = ((iffPict->ViewMode & EXTRA_HALFBRITE) != 0);
	SetBusyPointer(window);
	for (y = 0; y < bMapObj->Height; y++) {
		if (isHAM)
			prevColor32 = RGB_TO_RGB32(iffPict->ColorTable[0]);
		offset = y*bitMap->BytesPerRow;
		for (x = 0; x < bMapObj->Width; x++) {
			if (iffPict->Mask) {
				byte = iffPict->Mask[offset + (x >> 3)];
				pen = (byte >> (7 - (x & 0x07))) & 1;
			}
			else
				pen = 1;
			if (pen == 0)
				*data++ = RGB_TRANSPARENT;
			else {
				pen = 0;
				for (i = depth - 1; i >= 0; i--) {
					byte = bitMap->Planes[i][offset + (x >> 3)];
					pen = (pen << 1) | ((byte >> (7 - (x & 0x07))) & 1);
				}
				if (isHAM && depth <= 6 && (pen & 0x30))	// Convert HAM to HAM-8
					pen <<= 2;
				if (depth == 24)
					color = RGBCOLOR((pen >> 4) & 0x0F, (pen >> 12) & 0x0F, (pen >> 20) & 0x0F);
				else if (isHAM) {
					if (pen & 0xC0) {
						red		= RED_32(prevColor32);
						green	= GREEN_32(prevColor32);
						blue	= BLUE_32(prevColor32);
						if (pen < 0x80)
							blue  = (pen & 0x3F) << 2;
						else if (pen < 0xC0)
							red   = (pen & 0x3F) << 2;
						else
							green = (pen & 0x3F) << 2;
						prevColor32 = RGB32COLOR(red, green, blue);
						color = RGB32_TO_RGB(prevColor32);
					}
					else {
						color = iffPict->ColorTable[pen];
						prevColor32 = RGB_TO_RGB32(color);
					}
				}
				else if (isHalfBrite && (pen & 0x20))
					color = RGBDARKEN(iffPict->ColorTable[pen & 0x1F]);
				else
					color = iffPict->ColorTable[pen];
				*data++ = (pen != iffPict->TranspColor) ? color : RGB_TRANSPARENT;
			}
		}
		NextBusyPointer();
	}
/*
	If picture caching enabled, then cache picture
*/
	if (options.ShowPictures && options.CachePictures) {
		SetStdPointer(window, POINTER_WAIT);
		BMapCreateCache(bMapObj, window->RPort);
	}
/*
	set up link if info available
*/
	if (fileName && dirLock) {
		if ((fib = MemAlloc(sizeof(struct FileInfoBlock), MEMF_CLEAR)) != NULL) {
			SetCurrentDir(dirLock);
			if ((lock = Lock(fileName, ACCESS_READ)) != NULL) {
				if (Examine(lock, fib))
					bMapObj->FileDate = fib->fib_Date;
				if (NameFromLock1(dirLock, strBuff, 256)) {
					len = strlen(strBuff);
					if (strBuff[len - 1] != ':') {
						strcat(strBuff, "/");
						len++;
					}
					strcat(strBuff, fileName);
					len += strlen(fileName);
					if (bMapObj->FileName)
						MemFree(bMapObj->FileName, strlen(bMapObj->FileName) + 1);
					if ((bMapObj->FileName = MemAlloc(len + 1, MEMF_CLEAR)) != NULL)
						strcpy(bMapObj->FileName, strBuff);
				}
				UnLock(lock);
			}
			MemFree(fib, sizeof(struct FileInfoBlock));
		}
	}
/*
	Select new object and return
*/
	if (draw) {
		HiliteSelectOff(window);
		UnSelectAllObjects(docData);
		SelectObject((DocObjPtr) bMapObj);
		HiliteSelectOn(window);
		InvalObjectRect(window, (DocObjPtr) bMapObj);
	}
	DocModified(docData);
	DisposePict(iffPict);

	return (TRUE);
}

/*
 *	Make bitmap cache
 */

void BMapCreateCache(BMapObjPtr bMapObj, RastPtr rPort)
{
	register WORD	x, y;
	register LONG	xStart;
	WORD			width, height, depth;
//	LONG			size;
	BOOL			needMask;
//	PLANEPTR		fastPlane;
	BitMapPtr		bitMap;
	RastPort		tempPort;

	width  = bMapObj->Width;
	height = bMapObj->Height;
	depth = rPort->BitMap->Depth;
/*
	First create bitmap cache
*/
	if ((bMapObj->PictCache = CreateBitMap(width, height, depth, TRUE)) != NULL) {
		BMapDrawCache(bMapObj, rPort);
/*
	Now create mask
*/
		bMapObj->Mask = NULL;
		needMask = FALSE;
		if ((bitMap = CreateBitMap(width, height, 1, TRUE)) != NULL) {
			InitRastPort(&tempPort);
			tempPort.BitMap = bitMap;
			SetRast(&tempPort, 1);
			SetAPen(&tempPort, 0);
		}
		for (y = 0; y < height; y++) {
			xStart = (LONG) y*width;
			for (x = 0; x < width; x++) {
				if (bMapObj->Data[xStart + x] == RGB_TRANSPARENT) {
					if (bitMap)
						WritePixel(&tempPort, x, y);
					needMask = TRUE;
				}
			}
		}
		if (bitMap) {
			WaitBlit();
			if (needMask) {
				bMapObj->Mask = bitMap->Planes[0];
				bitMap->Planes[0] = NULL;
			}
			DisposeBitMap(bitMap);
		}
/*
	If a mask was needed but not obtained, discard cache
*/
		if (needMask && bMapObj->Mask == NULL)
			BMapDisposeCache(bMapObj);
/*
	Otherwise, try to copy data into fast memory
*/
/*
		else {
			bitMap = bMapObj->PictCache;
			size = (LONG) bitMap->BytesPerRow*bitMap->Rows;
			for (i = 0; i < depth; i++) {
				if ((fastPlane = MemAlloc(size, MEMF_FAST)) != NULL) {
					BlockMove(bitMap->Planes[i], fastPlane, size);
					FreeMem(bitMap->Planes[i], size);
					bitMap->Planes[i] = fastPlane;
				}
			}
			if (bMapObj->Mask && (fastPlane = MemAlloc(size, MEMF_FAST)) != NULL) {
				BlockMove(bMapObj->Mask, fastPlane, size);
				FreeMem(bMapObj->Mask, size);
				bMapObj->Mask = fastPlane;
			}
		}
*/
	}
}

/*
 *	Draw picture cache (used when palette changes)
 */

static void BMapDrawCache(BMapObjPtr bMapObj, RastPtr rPort)
{
	Rectangle	rect;
	RastPort	tempPort;

	InitRastPort(&tempPort);
	tempPort.BitMap = bMapObj->PictCache;
	SetRect(&rect, 0, 0, bMapObj->Width - 1, bMapObj->Height - 1);
	SetPalette(&tempPort, GetPalette(rPort));
	DrawBMapObj(&tempPort, bMapObj, &rect, &rect);
}

/*
 *	Dispose of bitmap cache
 */

void BMapDisposeCache(BMapObjPtr bMapObj)
{
	if (bMapObj->PictCache)
		DisposeBitMap(bMapObj->PictCache);
	if (bMapObj->Mask)
		FreeRaster(bMapObj->Mask, bMapObj->Width, bMapObj->Height);
	bMapObj->PictCache = NULL;
	bMapObj->Mask = NULL;
}

/*
 *	Set pointer to crop pointer
 */

static void SetCropPointer(WindowPtr window)
{
	register PointerPtr	pointer;

	pointer = &cropPointer;
	if (window->Pointer != pointer->PointerData) {
		SetPointer(window, pointer->PointerData,
				   pointer->Height, pointer->Width,
				   pointer->XOffset, pointer->YOffset);
	}
}

/*
 *	crop picture
 */

void DoPictureCrop(WindowPtr window, BMapObjPtr bMapObj, UWORD modifier, WORD mouseX, WORD mouseY)
{
	Rectangle	rect, viewRect, frame;

	SetCropPointer(window);

	frame = bMapObj->DocObj.Frame;
	bMapObj->DocObj.Frame = bMapObj->ViewRect;
	TrackRect(window, (RectObjPtr) bMapObj, modifier, mouseX, mouseY, &rect, &frame);
/*
	Set new view dimensions
*/
	viewRect = bMapObj->DocObj.Frame;
	if (rect.MinX != viewRect.MinX || rect.MinY != viewRect.MinY ||
		rect.MaxX != viewRect.MaxX || rect.MaxY != viewRect.MaxY) {
		InvalObjectRect(window, (DocObjPtr) bMapObj);
		HiliteSelectOff(window);
		SetFrameRect(&bMapObj->ViewRect, rect.MinX, rect.MinY, rect.MaxX, rect.MaxY);
		HiliteSelectOn(window);
		InvalObjectRect(window, (DocObjPtr) bMapObj);
	}
/*
	restore frame
*/
	bMapObj->DocObj.Frame = frame;
	SetStdPointer(window, POINTER_ARROW);
}
