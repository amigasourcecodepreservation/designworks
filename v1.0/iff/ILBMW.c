/*
 *	ILBMW.c  Support routines for reading ILBM files		1/23/86
 *	(IFF is Interchange Format File)
 *
 *	By Jerry Morrison and Steve Shaw, Electronic Arts
 *	Modified by James Bayless, New Horizons Software
 *
 *  	This software is in the public domain.
 *
 *	This version for the Commodore-Amiga computer
 */

#include <exec/types.h>

#include "ILBM.h"
#include "Packer.h"

/*
 *	InitBMHdr
 */

IFFP InitBMHdr(register BitMapHeader *bmHdr, register struct BitMap *bitMap,
			   Masking masking, Compression compression, UWORD transparentColor,
			   register WORD pageWidth, register WORD pageHeight)
{
	register WORD rowBytes = bitMap->BytesPerRow;

	bmHdr->w = rowBytes << 3;
	bmHdr->h = bitMap->Rows;
	bmHdr->x = bmHdr->y = 0;		/* Default position is (0,0) */
	bmHdr->nPlanes = bitMap->Depth;
	bmHdr->masking = masking;
	bmHdr->compression = compression;
	bmHdr->pad1 = 0;
	bmHdr->transparentColor = transparentColor;
	bmHdr->xAspect = bmHdr->yAspect = 1;
	bmHdr->pageWidth = pageWidth;
	bmHdr->pageHeight = pageHeight;
	if (pageWidth == 320) {
		if (pageHeight == 200) {
			bmHdr->xAspect = x320x200Aspect;
			bmHdr->yAspect = y320x200Aspect;
		}
		else if (pageHeight == 400) {
			bmHdr->xAspect = x320x400Aspect;
			bmHdr->yAspect = y320x400Aspect;
		}
	}
	else if (pageHeight == 640) {
		if (pageHeight == 200) {
			bmHdr->xAspect = x640x200Aspect;
			bmHdr->yAspect = y640x200Aspect;
		}
		else if (pageHeight == 400) {
			bmHdr->xAspect = x640x400Aspect;
			bmHdr->yAspect = y640x400Aspect;
		}
	}
	return (IS_ODD(rowBytes) ? CLIENT_ERROR : IFF_OKAY);
}

/*
 *	PutCMAP
 */

IFFP PutCMAP(register GroupContext *context, register Color4 *colorMap, WORD depth)
{
	register IFFP iffp;
	register LONG nColorRegs;
	ColorRegister colorReg;

	if (depth > MaxAmDepth)
		depth = MaxAmDepth;
	nColorRegs = 1 << depth;
	iffp = PutCkHdr(context, ID_CMAP, nColorRegs*sizeofColorRegister);
	while (iffp == IFF_OKAY && nColorRegs--) {
		colorReg.red   = (*colorMap >> 4) & 0xF0;
		colorReg.green = (*colorMap	 ) & 0xF0;
		colorReg.blue  = (*colorMap << 4) & 0xF0;
		iffp = IFFWriteBytes(context, &colorReg, sizeofColorRegister);
		colorMap++;
	}
	if (iffp == IFF_OKAY)
		iffp = PutCkEnd(context);
	return (iffp);
}

/*
 *	PutBODY
 *	NOTE: This implementation could be a LOT faster if it used more of the
 *		supplied buffer
 *	It would make far fewer calls to IFFWriteBytes (and therefore to DOS Write)
 */

IFFP PutBODY(GroupContext *context, register struct BitMap *bitMap, PLANEPTR mask,
			 register BitMapHeader *bmHdr, BYTE *buffer, LONG bufSize)
{
	register IFFP iffp;
	LONG rowBytes = bitMap->BytesPerRow;
	Compression compression = bmHdr->compression;
	WORD dstDepth = bmHdr->nPlanes;
	register WORD iPlane, iRow;
	register LONG packedRowBytes;
	BYTE *buf;
	PLANEPTR planes[MaxAmDepth + 1];	/* Array of ptrs to planes and mask */

	if (bufSize < MaxPackedSize(rowBytes) || compression > cmpByteRun1 ||
		bitMap->Rows != bmHdr->h || rowBytes != RowBytes(bmHdr->w) ||
		dstDepth > bitMap->Depth || dstDepth > MaxAmDepth)
		return (CLIENT_ERROR);
/*
	Copy the ptrs to bit & mask planes into local array
*/
	for (iPlane = 0; iPlane < dstDepth; iPlane++)
		planes[iPlane] = bitMap->Planes[iPlane];
	if (mask)
		planes[dstDepth++] = mask;
/*
	Write out a BODY chunk header
*/
	iffp = PutCkHdr(context, ID_BODY, szNotYetKnown);
/*
	Write out the BODY contents
*/
	for (iRow = 0; iffp == IFF_OKAY && iRow < bmHdr->h; iRow++) {
		for (iPlane = 0; iffp == IFF_OKAY && iPlane < dstDepth; iPlane++) {
/*
	Compress and write next row
*/
			if (compression == cmpByteRun1) {
				buf = buffer;
				packedRowBytes = PackRow(&planes[iPlane], &buf, rowBytes);
				iffp = IFFWriteBytes(context, buffer, packedRowBytes);
			}
/*
	Write next row
*/
			else {
				iffp = IFFWriteBytes(context, planes[iPlane], rowBytes);
				planes[iPlane] += rowBytes;
			}
		}
	}
/*
	Finish the chunk
*/
	if (iffp == IFF_OKAY)
		iffp = PutCkEnd(context);
	return (iffp);
}
