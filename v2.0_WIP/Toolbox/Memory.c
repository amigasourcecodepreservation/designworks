/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Toolbox library
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Small memory allocator
 */

#include <exec/types.h>
#include <exec/memory.h>

#include <proto/exec.h>

#include <Toolbox/Memory.h>

/*
 *	Local variables and definitions
 */

#define BLOCK_SIZE	0x2000		// Allocate in 8K chunks

#define ADJUST_SIZE(size)	(((size) + 7) & 0xFFFFFFF8L)

typedef struct FreeSect {
	struct FreeSect	*Next;		// Next free section
	LONG			Size;		// Size of this section
} FreeSect, *FreeSectPtr;

typedef struct MemBlock {
	struct MemBlock	*Next, *Prev;	// Next and Prev blocks, or NULL
	FreeSectPtr		First;			// First free section in block
	BYTE			Block[BLOCK_SIZE];
} MemBlock, *MemBlockPtr;

static MemBlockPtr firstBlock;	// Inited to NULL

/*
 *	Local routine prototypes
 */

static BYTE	*AllocSection(MemBlockPtr, ULONG, LONG);
static void	FreeSection(MemBlockPtr, BYTE *, LONG);

/*
 *	Allocate section of block
 *	Size must be multiple of eight
 *	Only MEMF_CLEAR flag is checked
 */

static BYTE *AllocSection(MemBlockPtr block, register ULONG size, LONG flags)
{
	register BYTE			*ptr;
	register FreeSectPtr	free, prevFree, nextFree;

	prevFree = NULL;
	for (free = block->First; free; free = free->Next) {
		if (free->Size >= size) {
			ptr = (BYTE *) free;
			if (free->Size == size)
				nextFree = free->Next;
			else {
				nextFree = (FreeSectPtr) (ptr + size);
				nextFree->Next = free->Next;
				nextFree->Size = free->Size - size;
			}
			if (prevFree)
				prevFree->Next = nextFree;
			else
				block->First = nextFree;
			if (flags & MEMF_CLEAR)
				BlockClear(ptr, size);
			return (ptr);
		}
		prevFree = free;
	}
	return (NULL);
}

/*
 *	Free section of memory in block
 *	Ptr must be within block, and size must be multiple of eight
 */

static void FreeSection(MemBlockPtr block, register BYTE *ptr, register LONG size)
{
	register FreeSectPtr	free, prevFree, nextFree;

	free = (FreeSectPtr) ptr;
/*
	Find previous and following sections in block
*/
	prevFree = NULL;
	for (nextFree = block->First; nextFree && nextFree < free; nextFree = nextFree->Next)
		prevFree = nextFree;
/*
	Add section to free list
*/
	free->Next = nextFree;
	free->Size = size;
	if (prevFree)
		prevFree->Next = free;
	else
		block->First = free;
/*
	Combine free section with following one if possible
*/
	if ((FreeSectPtr) (ptr + size) == nextFree) {
		free->Next = nextFree->Next;
		free->Size += nextFree->Size;
	}
/*
	Combine free section with previous one if possible
*/
	ptr = (BYTE *) prevFree;
	if (ptr && (FreeSectPtr) (ptr + prevFree->Size) == free) {
		prevFree->Next = free->Next;
		prevFree->Size += free->Size;
	}
}

/*
 *	Allocate memory, return NULL if no memory left
 *	If small allocation requested then allocate from larger blocks
 *	If any flags other than MEMF_CLEAR are set, then regular Amiga allocator is used
 */

Ptr MemAlloc(register ULONG size, LONG flags)
{
	BYTE					*ptr;
	register MemBlockPtr	block, prevBlock;
	register FreeSectPtr	free;

#ifdef DEBUG
	return (AllocMem(size, flags));
#else
	if (size == 0)
		return (NULL);
/*
	Round up to the next multiple of eight
	Use Amiga if too large or has unsupported flags
*/
	size = ADJUST_SIZE(size);
	if ((flags & ~MEMF_CLEAR) != 0 || size > BLOCK_SIZE)
		return (AllocMem(size, flags));
/*
	Find block with space
*/
	prevBlock = NULL;
	for (block = firstBlock; block; block = block->Next) {
		if ((ptr = AllocSection(block, size, flags)) != NULL)
			return (ptr);
		prevBlock = block;
	}
/*
	No block with enough space found
	Allocate and initialize new block
*/
	if ((block = AllocMem(sizeof(MemBlock), 0)) == NULL)
		return (NULL);
	block->Next = NULL;
	block->Prev = prevBlock;
	if (prevBlock == NULL)
		firstBlock = block;
	else
		prevBlock->Next = block;
	free = (FreeSectPtr) block->Block;
	free->Next = NULL;				// Mem section is entire block
	free->Size = BLOCK_SIZE;
	block->First = free;
	return ((Ptr) AllocSection(block, size, flags));
#endif
}

/*
 *	Free memory
 *	Section must have been allocated by small memory allocator
 */

void MemFree(Ptr ptr, register ULONG size)
{
	register BYTE			*p;
	register MemBlockPtr	block;

#ifdef DEBUG
	FreeMem(ptr, size);
	return;
#else
	if (size == 0)
		return;
	p = ptr;
/*
	Round size up to next multiple of eight
*/
	size = ADJUST_SIZE(size);
	if (size > BLOCK_SIZE) {
		FreeMem(p, size);
		return;
	}
/*
	Find block containing this section
*/
	for (block = firstBlock; block; block = block->Next) {
		if (p >= block->Block && p < block->Block + BLOCK_SIZE)
			break;
	}
/*
	Free section
	If this makes the entire block empty then return block to heap
*/
	if (block) {
		FreeSection(block, p, size);
		if (block->First == (FreeSectPtr) block->Block && block->First->Size == BLOCK_SIZE) {
			if (block->Prev)
				block->Prev->Next = block->Next;
			else
				firstBlock = block->Next;
			if (block->Next)
				block->Next->Prev = block->Prev;
			FreeMem(block, sizeof(MemBlock));
		}
	}
/*
	If memory block being freed is not in allocator block, then free it directly
	(It may have been allocated specifically as CHIP memory)
*/
	else
		FreeMem(p, size);
#endif
}

/*
 *	Count available memory, including free memory in heap
 */

ULONG MemAvail(LONG flags)
{
	register ULONG			avail;
	register MemBlockPtr	block;
	register FreeSectPtr	free;

	avail = 0;
	for (block = firstBlock; block; block = block->Next) {
		if (TypeOfMem(block) & flags) {
			for (free = block->First; free; free = free->Next)
				avail += free->Size;
		}
	}
	return (AvailMem(flags) + avail);
}

/*
 *	Set a block of memory to a value
 */

void BlockSet(Ptr ptr, register ULONG size, register BYTE value)
{
	register BYTE	*p;

	p = ptr;
	while (size--)
		*p++ = value;
}

/*
 *	Clear memory block to zero
 */

void BlockClear(Ptr ptr, ULONG size)
{
	BlockSet(ptr, size, 0);
}

/*
 *	Move contents of memory from one place to another
 *	Must properly handle overlapping memory blocks
 */

void BlockMove(Ptr src, Ptr dst, register ULONG size)
{
	register BYTE	*p1, *p2;

	p1 = src;	p2 = dst;
	if (p2 + size <= p1 || p2 >= p1 + size)
		CopyMem(p1, p2, size);
	else if (p2 < p1) {
		while (size--)
			*p2++ = *p1++;
	}
	else {
		p1 += size;	p2 += size;
		while (size--)
			*(--p2) = *(--p1);
	}
}

/*
 *	Swap two sections of memory
 */

void SwapMem(Ptr ptr1, Ptr ptr2, register ULONG num)
{
	register BYTE	ch, *p1, *p2;

	p1 = ptr1;	p2 = ptr2;
	while (num--) {
		ch = *p1;	*p1++ = *p2;	*p2++ = ch;
	}
}
