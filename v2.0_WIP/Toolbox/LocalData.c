/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Toolbox library
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	Localization data
 */

#include <exec/types.h>

#include <Toolbox/Language.h>
#include <Toolbox/Border.h>
#include <Toolbox/Image.h>
#include <Toolbox/Gadget.h>
#include <Toolbox/ScrollList.h>
#include <Toolbox/Request.h>
#include <Toolbox/Dialog.h>

#include <Typedefs.h>

/*
 *	Local definitions
 */

#define GT_ADJUST_UPARROW(left, top)	\
	{ GADG_ACTIVE_STDIMAGE,													\
		left, (top)+5-ARROW_HEIGHT, 0, 0, ARROW_WIDTH, ARROW_HEIGHT, 0, 0,	\
		0, 0, (Ptr) IMAGE_ARROW_UP }

#define GT_ADJUST_DOWNARROW(left, top)	\
	{ GADG_ACTIVE_STDIMAGE,										\
		left, (top)+5, 0, 0, ARROW_WIDTH, ARROW_HEIGHT, 0, 0,	\
		0, 0, (Ptr) IMAGE_ARROW_DOWN }

#define GT_ADJUST_TEXT(left, top)	\
	{ GADG_STAT_TEXT, (left)+10+ARROW_WIDTH, top, 0, 0, 0, 0, 0, 0, 0, 0, NULL }

/*
 *	Text for dialogs and requesters
 */

TextChar	_tbStrPersonalize[] = "Please personalize your copy of\n";

#if (AMERICAN | BRITISH)

#define CHR_OK		'O'
#define CHR_CANCEL	'C'
#define CHR_YES		'Y'
#define CHR_NO		'N'

TextChar	_strOK[]		= "OK";
TextChar	_strCancel[]	= "Cancel";
TextChar	_strYes[]		= "Yes";
TextChar	_strNo[]		= "No";

TextChar	_sfpSystemText[]		= "Mounted disks and assignments";
TextChar	_sfpFileExistsText[]	= "File already exists,\nOK to replace?";
TextChar	_sfpBadNameText[]		= "Improper file name.";
TextChar	_sfpNoFileText[]		= "No such file.";
TextChar	_sfpDiskLockText[]		= "Disk is locked.";

#if AMERICAN
TextChar	_tbStrScreenColor[]		= "Select a screen color";
#elif BRITISH
TextChar	_tbStrScreenColor[]		= "Select a screen colour";
#endif

TextPtr		_strInitErrors[] = {
	"Bad system state.",
	"Can't find icon.library.",
	"Can't find diskfont.library.",
	"Can't open screen.",
};

TextChar	_tbStrSampleText[]	= "The quick brown fox jumps over the lazy dog.";

TextChar	_tbStrOpenMacro[]	= "Select a macro to execute";

#elif GERMAN

#define CHR_OK		'O'
#define CHR_CANCEL	'A'
#define CHR_YES		'J'
#define CHR_NO		'N'

TextChar	_strOK[]		= "OK";
TextChar	_strCancel[]	= "Abbr.";
TextChar	_strYes[]		= "Ja";
TextChar	_strNo[]		= "Nein";

TextChar	_sfpSystemText[]		= "Laufwerke und Verzeichnisse";
TextChar	_sfpFileExistsText[]	= "Die Datel existiert bereits,\nsoll sie ersetzt werden?";
TextChar	_sfpBadNameText[]		= "Ung�ltiger Dateiname.";
TextChar	_sfpNoFileText[]		= "Keine solche Datei\nvorhanden.";
TextChar	_sfpDiskLockText[]		= "Der Datentr�ger ist\nverriegelt.";

TextChar	_tbStrScreenColor[]		= "Bildschirmfarbe w�hlen";

TextPtr		_strInitErrors[] = {
	"Falscher System-Status.",
	"icon.library kann nicht gefunden werden.",
	"diskfont.library kann nicht gefunden werden."
	"Bildschirm kann nicht ge�ffnet werden.",
};

TextChar	_tbStrSampleText[]	= "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz";

TextChar	_tbStrOpenMacro[]	= "Makro zur Ausf�hrung w�hlen";

#elif FRENCH

#define CHR_OK		'O'
#define CHR_CANCEL	'A'
#define CHR_YES		'O'
#define CHR_NO		'N'

TextChar	_strOK[]		= "OK";
TextChar	_strCancel[]	= "Annule";
TextChar	_strYes[]		= "Oui";
TextChar	_strNo[]		= "Non";

TextChar	_sfpSystemText[]		= "Disques mont�s et assignations";
TextChar	_sfpFileExistsText[]	= "Fichier existe d�j�,\nOK pour le remplacer?";
TextChar	_sfpBadNameText[]		= "Nom de fichier incorrect.";
TextChar	_sfpNoFileText[]		= "Fichier inexistant.";
TextChar	_sfpDiskLockText[]		= "Le disque est verrouill�.";

TextChar	_tbStrScreenColor[]		= "S�lectionnez une couleur �cran";

TextPtr		_strInitErrors[] = {
	"Probl�mes avec le syst�me d'exploitation.",
	"Icon.library introuvable.",
	"Diskfont.library introuvable.",
	"Impossible d'ouvrir un �cran.",
};

TextChar	_tbStrSampleText[]	= "C'est fi�re et joviale que Zazi paye et monte dans le vieux wagon b�ch� kaki.";

TextChar	_tbStrOpenMacro[]	= "S�lection d'une macro � ex�cuter";

#elif SPANISH

#define CHR_OK		'O'
#define CHR_CANCEL	'C'
#define CHR_YES		'S'
#define CHR_NO		'N'

TextChar	_strOK[]		= "OK";
TextChar	_strCancel[]	= "Cancelar";
TextChar	_strYes[]		= "S�";
TextChar	_strNo[]		= "No";

TextChar	_sfpSystemText[]		= "Discos montados y asignados";
TextChar	_sfpFileExistsText[]	= "El archivo ya existe.\n�Reemplazar?";
TextChar	_sfpBadNameText[]		= "Nombre de archivo\nincorrecto.";
TextChar	_sfpNoFileText[]		= "No existe el archivo.";
TextChar	_sfpDiskLockText[]		= "Disco protegido.";

TextChar	_tbStrScreenColor[]		= "Escoge un color de pantalla";

TextPtr		_strInitErrors[] = {
	"Sistema en mal estado.",
	"No encuentra icon.library.",
	"No encuentra diskfont.library.",
	"No puede abrir pantalla.",
};

TextChar	_tbStrSampleText[]	= "El respeto al derecho ajeno es la paz.";

TextChar	_tbStrOpenMacro[]	= "Selecciona una macro";

#elif SWEDISH

#define CHR_OK		'O'
#define CHR_CANCEL	'A'
#define CHR_YES		'J'
#define CHR_NO		'N'

TextChar	_strOK[]		= "OK";
TextChar	_strCancel[]	= "Avbryt";
TextChar	_strYes[]		= "Ja";
TextChar	_strNo[]		= "Nej";

TextChar	_sfpSystemText[]		= "Volymer och tilldelade enheter";
TextChar	_sfpFileExistsText[]	= "Filen finns redan.\nSkall den skrivas �ver?";
TextChar	_sfpBadNameText[]		= "Felaktigt filnamn.";
TextChar	_sfpNoFileText[]		= "Filen finns ej.";
TextChar	_sfpDiskLockText[]		= "Disken �r l�st.";

TextChar	_tbStrScreenColor[]		= "St�ll in sk�rmf�rg";

TextPtr		_strInitErrors[] = {
	"Fel systemversion.",
	"Kan ej hitta 'icon.library'.",
	"Kan ej hitta 'diskfont.library'.",
	"Kan ej �ppna sk�rmen.",
};

TextChar	_tbStrSampleText[]	= "P� lingonr�da tuvor upp� villande mo...";

TextChar	_tbStrOpenMacro[]	= "V�lj makro att utf�ra";

#endif

/*
 *	Open dialog
 */

#define LIST_WIDTH	(24*8)

#define OPEN_NUM	10

static GadgetTemplate getGadgets[] = {
#if (AMERICAN | BRITISH)
	{ GADG_PUSH_BUTTON,  -80, -60, 0, 0, 60, 20, 0, 0,        'O', 0, "Open" },
	{ GADG_PUSH_BUTTON,  -80, -30, 0, 0, 60, 20, 0, 0, CHR_CANCEL, 0, _strCancel },

	{ GADG_PUSH_BUTTON,  -80,  30, 0, 0, 60, 20, 0, 0, 'E', 0, "Enter" },
	{ GADG_PUSH_BUTTON,  -80,  60, 0, 0, 60, 20, 0, 0, 'B', 0, "Back" },
	{ GADG_PUSH_BUTTON,  -80,  90, 0, 0, 60, 20, 0, 0, 'D', 0, "Disks" },
#elif GERMAN
	{ GADG_PUSH_BUTTON, -100, -60, 0, 0, 80, 20, 0, 0,        'L', 0, "Laden" },
	{ GADG_PUSH_BUTTON, -100, -30, 0, 0, 80, 20, 0, 0, CHR_CANCEL, 0, _strCancel },

	{ GADG_PUSH_BUTTON, -100,  30, 0, 0, 80, 20, 0, 0, 'U', 0, "Unterv." },
	{ GADG_PUSH_BUTTON, -100,  60, 0, 0, 80, 20, 0, 0, 'M', 0, "Mutterv." },
	{ GADG_PUSH_BUTTON, -100,  90, 0, 0, 80, 20, 0, 0, 'f', 0, "Laufw." },
#elif FRENCH
	{ GADG_PUSH_BUTTON,  -90, -60, 0, 0, 70, 20, 0, 0,        'O', 0, "Ouvre" },
	{ GADG_PUSH_BUTTON,  -90, -30, 0, 0, 70, 20, 0, 0, CHR_CANCEL, 0, _strCancel },

	{ GADG_PUSH_BUTTON,  -90,  30, 0, 0, 70, 20, 0, 0, 'E', 0, "Entre" },
	{ GADG_PUSH_BUTTON,  -90,  60, 0, 0, 70, 20, 0, 0, 'R', 0, "Retour" },
	{ GADG_PUSH_BUTTON,  -90,  90, 0, 0, 70, 20, 0, 0, 'V', 0, "Volumes" },
#elif SPANISH
	{ GADG_PUSH_BUTTON,  -90, -60, 0, 0, 70, 20, 0, 0,        'A', 0, "Abrir" },
	{ GADG_PUSH_BUTTON,  -90, -30, 0, 0, 70, 20, 0, 0, CHR_CANCEL, 0, _strCancel },

	{ GADG_PUSH_BUTTON,  -90,  30, 0, 0, 70, 20, 0, 0, 'E', 0, "Entrar" },
	{ GADG_PUSH_BUTTON,  -90,  60, 0, 0, 70, 20, 0, 0, 'R', 0, "Regresar" },
	{ GADG_PUSH_BUTTON,  -90,  90, 0, 0, 70, 20, 0, 0, 'D', 0, "Discos" },
#elif SWEDISH
	{ GADG_PUSH_BUTTON,  -90, -60, 0, 0, 70, 20, 0, 0,        '�', 0, "�ppna" },
	{ GADG_PUSH_BUTTON,  -90, -30, 0, 0, 70, 20, 0, 0, CHR_CANCEL, 0, _strCancel },

	{ GADG_PUSH_BUTTON,  -90,  30, 0, 0, 70, 20, 0, 0, 'V', 0, "Visa" },
	{ GADG_PUSH_BUTTON,  -90,  60, 0, 0, 70, 20, 0, 0, 'M', 0, "Moder" },
	{ GADG_PUSH_BUTTON,  -90,  90, 0, 0, 70, 20, 0, 0, 'l', 0, "Volymer" },
#endif

	SL_GADG_BOX(20, 30, LIST_WIDTH, OPEN_NUM),
	SL_GADG_UPARROW(20, 30, LIST_WIDTH, OPEN_NUM),
	SL_GADG_DOWNARROW(20, 30, LIST_WIDTH, OPEN_NUM),
	SL_GADG_SLIDER(20, 30, LIST_WIDTH, OPEN_NUM),

	{ GADG_EDIT_TEXT, 20, -20, 0, 0, LIST_WIDTH, 11, 0, 0, 0, 0, NULL },

	{ GADG_STAT_TEXT,  65, 10, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_IMAGE, 20, 15, 0, 0, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_ITEM_NONE }
};

DialogTemplate _sfpGetFileDlgTempl = {
#if (AMERICAN | BRITISH)
	DLG_TYPE_WINDOW, 0, -1, -1, 120 + LIST_WIDTH, 180, getGadgets, NULL
#elif (FRENCH | SPANISH | SWEDISH)
	DLG_TYPE_WINDOW, 0, -1, -1, 130 + LIST_WIDTH, 180, getGadgets, NULL
#elif GERMAN
	DLG_TYPE_WINDOW, 0, -1, -1, 140 + LIST_WIDTH, 180, getGadgets, NULL
#endif
};

/*
 *	Save dialog
 */

#define SAVE_NUM	7

static GadgetTemplate putGadgets[] = {
#if (AMERICAN | BRITISH)
	{ GADG_PUSH_BUTTON,  -80, -60, 0, 0, 60, 20, 0, 0,        'S', 0, "Save" },
	{ GADG_PUSH_BUTTON,  -80, -30, 0, 0, 60, 20, 0, 0, CHR_CANCEL, 0, _strCancel },

	{ GADG_PUSH_BUTTON,  -80,  30, 0, 0, 60, 20, 0, 0, 'E', 0, "Enter" },
	{ GADG_PUSH_BUTTON,  -80,  60, 0, 0, 60, 20, 0, 0, 'B', 0, "Back" },
	{ GADG_PUSH_BUTTON,  -80,  90, 0, 0, 60, 20, 0, 0, 'D', 0, "Disks" },
#elif GERMAN
	{ GADG_PUSH_BUTTON, -100, -60, 0, 0, 80, 20, 0, 0,        'S', 0, "Speichern" },
	{ GADG_PUSH_BUTTON, -100, -30, 0, 0, 80, 20, 0, 0, CHR_CANCEL, 0, _strCancel },

	{ GADG_PUSH_BUTTON, -100,  30, 0, 0, 80, 20, 0, 0, 'U', 0, "Unterv." },
	{ GADG_PUSH_BUTTON, -100,  60, 0, 0, 80, 20, 0, 0, 'M', 0, "Mutterv." },
	{ GADG_PUSH_BUTTON, -100,  90, 0, 0, 80, 20, 0, 0, 'f', 0, "Laufw." },
#elif FRENCH
	{ GADG_PUSH_BUTTON,  -90, -60, 0, 0, 70, 20, 0, 0,        'S', 0, "Sauve" },
	{ GADG_PUSH_BUTTON,  -90, -30, 0, 0, 70, 20, 0, 0, CHR_CANCEL, 0, _strCancel },

	{ GADG_PUSH_BUTTON,  -90,  30, 0, 0, 70, 20, 0, 0, 'E', 0, "Entre" },
	{ GADG_PUSH_BUTTON,  -90,  60, 0, 0, 70, 20, 0, 0, 'R', 0, "Retour" },
	{ GADG_PUSH_BUTTON,  -90,  90, 0, 0, 70, 20, 0, 0, 'V', 0, "Volumes" },
#elif SPANISH
	{ GADG_PUSH_BUTTON,  -90, -60, 0, 0, 70, 20, 0, 0,        'G', 0, "Guardar" },
	{ GADG_PUSH_BUTTON,  -90, -30, 0, 0, 70, 20, 0, 0, CHR_CANCEL, 0, _strCancel },

	{ GADG_PUSH_BUTTON,  -90,  30, 0, 0, 70, 20, 0, 0, 'E', 0, "Entrar" },
	{ GADG_PUSH_BUTTON,  -90,  60, 0, 0, 70, 20, 0, 0, 'R', 0, "Regresar" },
	{ GADG_PUSH_BUTTON,  -90,  90, 0, 0, 70, 20, 0, 0, 'D', 0, "Discos" },
#elif SWEDISH
	{ GADG_PUSH_BUTTON,  -90, -60, 0, 0, 70, 20, 0, 0,        'S', 0, "Spara" },
	{ GADG_PUSH_BUTTON,  -90, -30, 0, 0, 70, 20, 0, 0, CHR_CANCEL, 0, _strCancel },

	{ GADG_PUSH_BUTTON,  -90,  30, 0, 0, 70, 20, 0, 0, 'V', 0, "Visa" },
	{ GADG_PUSH_BUTTON,  -90,  60, 0, 0, 70, 20, 0, 0, 'M', 0, "Moder" },
	{ GADG_PUSH_BUTTON,  -90,  90, 0, 0, 70, 20, 0, 0, 'l', 0, "Volymer" },
#endif

	SL_GADG_BOX(20, 30, LIST_WIDTH, SAVE_NUM),
	SL_GADG_UPARROW(20, 30, LIST_WIDTH, SAVE_NUM),
	SL_GADG_DOWNARROW(20, 30, LIST_WIDTH, SAVE_NUM),
	SL_GADG_SLIDER(20, 30, LIST_WIDTH, SAVE_NUM),

	{ GADG_EDIT_TEXT, 20, -40, 0, 0, LIST_WIDTH, 11, 0, 0, 0, 0, NULL },

	{ GADG_STAT_TEXT,  65,  10, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_IMAGE, 20,  15, 0, 0, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_STAT_TEXT,  20, -60, 0, 2, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_ITEM_NONE }
};

DialogTemplate _sfpPutFileDlgTempl = {
#if (AMERICAN | BRITISH)
	DLG_TYPE_WINDOW, 0, -1, -1, 120 + LIST_WIDTH, 180, putGadgets, NULL
#elif (FRENCH | SPANISH | SWEDISH)
	DLG_TYPE_WINDOW, 0, -1, -1, 130 + LIST_WIDTH, 180, putGadgets, NULL
#elif GERMAN
	DLG_TYPE_WINDOW, 0, -1, -1, 140 + LIST_WIDTH, 180, putGadgets, NULL
#endif
};

/*
 *	Misc requesters for Open and Save dialogs
 */

static GadgetTemplate req1Gadgets[] = {
	{ GADG_PUSH_BUTTON,  -80, -30, 0, 0, 60, 20, 0, 0, CHR_OK, 0, _strOK },
	{ GADG_STAT_TEXT,     55,  10, 0, 0,  0,  0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_STDIMAGE, 10,  10, 0, 0,  0,  0, 0, 0, 0, 0, (Ptr) IMAGE_ICON_STOP },
	{ GADG_ITEM_NONE }
};

RequestTemplate _sfpReq1Templ = {
	-1, -1, 280, 70, req1Gadgets
};

static GadgetTemplate req2Gadgets[] = {
	{ GADG_PUSH_BUTTON,   60, -30, 0, 0, 60, 20, 0, 0, CHR_YES, 0, _strYes },
	{ GADG_PUSH_BUTTON,  160, -30, 0, 0, 60, 20, 0, 0,  CHR_NO, 0, _strNo },
	{ GADG_STAT_TEXT,     55,  10, 0, 0,  0,  0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_STDIMAGE, 10,  10, 0, 0,  0,  0, 0, 0, 0, 0, (Ptr) IMAGE_ICON_CAUTION },
	{ GADG_ITEM_NONE }
};

RequestTemplate _sfpReq2Templ = {
	-1, -1, 280, 80, req2Gadgets
};

/*
 *	Color picker dialog
 */

static GadgetTemplate colorGadgets[] = {
#if (AMERICAN | BRITISH | GERMAN | FRENCH | SWEDISH)
	{ GADG_PUSH_BUTTON,  20, -30, 0, 0,  60, 20, 0, 0,     CHR_OK, 0, _strOK },
	{ GADG_PUSH_BUTTON, 100, -30, 0, 0,  60, 20, 0, 0, CHR_CANCEL, 0, _strCancel },
#elif SPANISH
	{ GADG_PUSH_BUTTON,  20, -30, 0, 0,  70, 20, 0, 0,     CHR_OK, 0, _strOK },
	{ GADG_PUSH_BUTTON, 110, -30, 0, 0,  70, 20, 0, 0, CHR_CANCEL, 0, _strCancel },
#endif
	{ GADG_STAT_STDBORDER,  70,  30, 0, 0,  20, 20, ARROW_WIDTH, 0, 0, 0, (Ptr) BORDER_SHADOWBOX },

	{ GADG_PROP_HORIZ | GADG_PROP_NOBORDER, 180,  45, 0, 0, 150, 15, 0, 0, 0, 0, NULL },
	{ GADG_PROP_HORIZ | GADG_PROP_NOBORDER, 180,  90, 0, 0, 150, 15, 0, 0, 0, 0, NULL },
	{ GADG_PROP_HORIZ | GADG_PROP_NOBORDER, 180, 135, 0, 0, 150, 15, 0, 0, 0, 0, NULL },

	GT_ADJUST_UPARROW(80, 70),
	GT_ADJUST_DOWNARROW(80, 70),
	GT_ADJUST_TEXT(80, 70),

	GT_ADJUST_UPARROW(80, 100),
	GT_ADJUST_DOWNARROW(80, 100),
	GT_ADJUST_TEXT(80, 100),

	GT_ADJUST_UPARROW(80, 130),
	GT_ADJUST_DOWNARROW(80, 130),
	GT_ADJUST_TEXT(80, 130),

	{ GADG_STAT_TEXT,  20,  10, 0, 0, 0, 0, 0, 0, 0, 0, NULL },

#if (AMERICAN | BRITISH)
	{ GADG_STAT_TEXT, 180,  30, 0, 0, 0, 0, 0, 0, 0, 0, "Hue" },
	{ GADG_STAT_TEXT, 180,  75, 0, 0, 0, 0, 0, 0, 0, 0, "Brightness" },
	{ GADG_STAT_TEXT, 180, 120, 0, 0, 0, 0, 0, 0, 0, 0, "Saturation" },
	{ GADG_STAT_TEXT,  20,  70, 0, 0, 0, 0, 0, 0, 0, 0, "Red:" },
	{ GADG_STAT_TEXT,  20, 100, 0, 0, 0, 0, 0, 0, 0, 0, "Green:" },
	{ GADG_STAT_TEXT,  20, 130, 0, 0, 0, 0, 0, 0, 0, 0, "Blue:" },
#elif GERMAN
	{ GADG_STAT_TEXT, 180,  30, 0, 0, 0, 0, 0, 0, 0, 0, "Farbton" },
	{ GADG_STAT_TEXT, 180,  75, 0, 0, 0, 0, 0, 0, 0, 0, "Helligkeit" },
	{ GADG_STAT_TEXT, 180, 120, 0, 0, 0, 0, 0, 0, 0, 0, "Farbs�ttigung" },
	{ GADG_STAT_TEXT,  20,  70, 0, 0, 0, 0, 0, 0, 0, 0, "Rot:" },
	{ GADG_STAT_TEXT,  20, 100, 0, 0, 0, 0, 0, 0, 0, 0, "Gr�n:" },
	{ GADG_STAT_TEXT,  20, 130, 0, 0, 0, 0, 0, 0, 0, 0, "Blau:" },
#elif FRENCH
	{ GADG_STAT_TEXT, 180,  30, 0, 0, 0, 0, 0, 0, 0, 0, "Teinte" },
	{ GADG_STAT_TEXT, 180,  75, 0, 0, 0, 0, 0, 0, 0, 0, "Luminosit�" },
	{ GADG_STAT_TEXT, 180, 120, 0, 0, 0, 0, 0, 0, 0, 0, "Saturation" },
	{ GADG_STAT_TEXT,  20,  70, 0, 0, 0, 0, 0, 0, 0, 0, "Rouge:" },
	{ GADG_STAT_TEXT,  20, 100, 0, 0, 0, 0, 0, 0, 0, 0, "Vert:" },
	{ GADG_STAT_TEXT,  20, 130, 0, 0, 0, 0, 0, 0, 0, 0, "Bleu:" },
#elif SPANISH
	{ GADG_STAT_TEXT, 180,  30, 0, 0, 0, 0, 0, 0, 0, 0, "Tinte" },
	{ GADG_STAT_TEXT, 180,  75, 0, 0, 0, 0, 0, 0, 0, 0, "Brillo" },
	{ GADG_STAT_TEXT, 180, 120, 0, 0, 0, 0, 0, 0, 0, 0, "Saturaci�n" },
	{ GADG_STAT_TEXT,  20,  70, 0, 0, 0, 0, 0, 0, 0, 0, "Rojo:" },
	{ GADG_STAT_TEXT,  20, 100, 0, 0, 0, 0, 0, 0, 0, 0, "Verde:" },
	{ GADG_STAT_TEXT,  20, 130, 0, 0, 0, 0, 0, 0, 0, 0, "Azul:" },
#elif SWEDISH
	{ GADG_STAT_TEXT, 180,  30, 0, 0, 0, 0, 0, 0, 0, 0, "Nyans" },
	{ GADG_STAT_TEXT, 180,  75, 0, 0, 0, 0, 0, 0, 0, 0, "Intensitet" },
	{ GADG_STAT_TEXT, 180, 120, 0, 0, 0, 0, 0, 0, 0, 0, "M�ttnad" },
	{ GADG_STAT_TEXT,  20,  70, 0, 0, 0, 0, 0, 0, 0, 0, "R�d:" },
	{ GADG_STAT_TEXT,  20, 100, 0, 0, 0, 0, 0, 0, 0, 0, "Gr�n:" },
	{ GADG_STAT_TEXT,  20, 130, 0, 0, 0, 0, 0, 0, 0, 0, "Bl�:" },
#endif

	{ GADG_ITEM_NONE }
};

DialogTemplate _tbColorPickDlgTempl = {
	DLG_TYPE_ALERT, 0, -1, -1, 350, 190, colorGadgets, NULL
};

/*
 *	Screen Colors dialog
 */

static GadgetTemplate screenColorsGadgets[] = {
#if (AMERICAN | BRITISH)
	{ GADG_PUSH_BUTTON,  -80,  10, 0, 0,  60, 20, 0, 0,     CHR_OK, 0, _strOK },
	{ GADG_PUSH_BUTTON,  -80,  40, 0, 0,  60, 20, 0, 0, CHR_CANCEL, 0, _strCancel },
	{ GADG_PUSH_BUTTON,  -80,  70, 0, 0,  60, 20, 0, 0, 'R', 0, "Reset" },
	{ GADG_PUSH_BUTTON,   70, -30, 0, 0,  60, 20, 0, 0, 'h', 0, "Change" },
	{ GADG_ACTIVE_BORDER, 20,  35, 0, 0, -120, -75, 0, 0, 0, 0, NULL },
#elif FRENCH
	{ GADG_PUSH_BUTTON,  -80,  10, 0, 0,  60, 20, 0, 0,     CHR_OK, 0, _strOK },
	{ GADG_PUSH_BUTTON,  -80,  40, 0, 0,  60, 20, 0, 0, CHR_CANCEL, 0, _strCancel },
	{ GADG_PUSH_BUTTON,  -80,  70, 0, 0,  60, 20, 0, 0, 'D', 0, "D�faut" },
	{ GADG_PUSH_BUTTON,   70, -30, 0, 0,  60, 20, 0, 0, 'C', 0, "Change" },
	{ GADG_ACTIVE_BORDER, 20,  35, 0, 0, -120, -75, 0, 0, 0, 0, NULL },
#elif GERMAN
	{ GADG_PUSH_BUTTON,  -90,  10, 0, 0,  70, 20, 0, 0,     CHR_OK, 0, _strOK },
	{ GADG_PUSH_BUTTON,  -90,  40, 0, 0,  70, 20, 0, 0, CHR_CANCEL, 0, _strCancel },
	{ GADG_PUSH_BUTTON,  -90,  70, 0, 0,  70, 20, 0, 0, 'Z', 0, "Zur�ck." },
	{ GADG_PUSH_BUTTON,   60, -30, 0, 0,  80, 20, 0, 0, 'W', 0, "Wechseln" },
	{ GADG_ACTIVE_BORDER, 20,  35, 0, 0, -130, -75, 0, 0, 0, 0, NULL },
#elif SPANISH
	{ GADG_PUSH_BUTTON,  -90,  10, 0, 0,  70, 20, 0, 0,     CHR_OK, 0, _strOK },
	{ GADG_PUSH_BUTTON,  -90,  40, 0, 0,  70, 20, 0, 0, CHR_CANCEL, 0, _strCancel },
	{ GADG_PUSH_BUTTON,  -90,  70, 0, 0,  70, 20, 0, 0, 'r', 0, "Original" },
	{ GADG_PUSH_BUTTON,   70, -30, 0, 0,  60, 20, 0, 0, 'a', 0, "Cambiar" },
	{ GADG_ACTIVE_BORDER, 20,  35, 0, 0, -130, -75, 0, 0, 0, 0, NULL },
#elif SWEDISH
	{ GADG_PUSH_BUTTON, -100,  10, 0, 0,  80, 20, 0, 0,     CHR_OK, 0, _strOK },
	{ GADG_PUSH_BUTTON, -100,  40, 0, 0,  80, 20, 0, 0, CHR_CANCEL, 0, _strCancel },
	{ GADG_PUSH_BUTTON, -100,  70, 0, 0,  80, 20, 0, 0, 't', 0, "�terst�ll" },
	{ GADG_PUSH_BUTTON,   70, -30, 0, 0,  60, 20, 0, 0, 'n', 0, "�ndra" },
	{ GADG_ACTIVE_BORDER, 20,  35, 0, 0, -140, -75, 0, 0, 0, 0, NULL },
#endif

	{ GADG_STAT_STDBORDER, 20,  25, 0, 0, 280 - 120, 0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
#if AMERICAN
	{ GADG_STAT_TEXT,  20, 10, 0, 0, 0, 0, 0, 0, 0, 0, "Screen Colors" },
#elif BRITISH
	{ GADG_STAT_TEXT,  20, 10, 0, 0, 0, 0, 0, 0, 0, 0, "Screen Colours" },
#elif GERMAN
	{ GADG_STAT_TEXT,  20, 10, 0, 0, 0, 0, 0, 0, 0, 0, "Bildschirmfarben" },
#elif FRENCH
	{ GADG_STAT_TEXT,  20, 10, 0, 0, 0, 0, 0, 0, 0, 0, "Couleurs �cran" },
#elif SPANISH
	{ GADG_STAT_TEXT,  20, 10, 0, 0, 0, 0, 0, 0, 0, 0, "Colores de pantalla" },
#elif SWEDISH
	{ GADG_STAT_TEXT,  20, 10, 0, 0, 0, 0, 0, 0, 0, 0, "St�ll in sk�rmf�rger" },
#endif

	{ GADG_ITEM_NONE }
};

DialogTemplate _tbScreenColorsDlgTempl = {
#if (AMERICAN | BRITISH | FRENCH)
	DLG_TYPE_ALERT, 0, -1, -1, 280, 110, screenColorsGadgets, NULL
#elif (GERMAN | SPANISH)
	DLG_TYPE_ALERT, 0, -1, -1, 290, 110, screenColorsGadgets, NULL
#elif SWEDISH
	DLG_TYPE_ALERT, 0, -1, -1, 300, 110, screenColorsGadgets, NULL
#endif
};

/*
 *	Font dialog
 */

#define FONT_WIDTH	(19*8+20)
#define SIZE_WIDTH	50
#define FONT_NUM	7
#define SIZE_NUM	(FONT_NUM - 2)

static GadgetTemplate fontGadgets[] = {
#if (AMERICAN | BRITISH)
	{ GADG_PUSH_BUTTON, -80, 30 + FONT_NUM*11 - 50, 0, FONT_NUM, 60, 20, 0, 0, 'U', 0, "Use" },
	{ GADG_PUSH_BUTTON, -80, 30 + FONT_NUM*11 - 20, 0, FONT_NUM, 60, 20, 0, 0, 'D', 0, "Done" },
	{ GADG_CHECK_BOX, 30, 30 + FONT_NUM*11 + 10, 0, FONT_NUM, 0, 0, 0, 0, 'L', 0, "List In Font Menu" },
	{ GADG_CHECK_BOX, 30, 30 + FONT_NUM*11 + 30, 0, FONT_NUM, 0, 0, 0, 0, 'S', 0, "Show Sample" },
#elif GERMAN
	{ GADG_PUSH_BUTTON, -90, 30 + FONT_NUM*11 - 50, 0, FONT_NUM, 70, 20, 0, 0, 'B', 0, "Benutzen" },
	{ GADG_PUSH_BUTTON, -90, 30 + FONT_NUM*11 - 20, 0, FONT_NUM, 70, 20, 0, 0, 'F', 0, "Fertig" },
	{ GADG_CHECK_BOX, 30, 30 + FONT_NUM*11 + 10, 0, FONT_NUM, 0, 0, 0, 0, 'M', 0, "Font in Men� auflisten" },
	{ GADG_CHECK_BOX, 30, 30 + FONT_NUM*11 + 30, 0, FONT_NUM, 0, 0, 0, 0, 'z', 0, "Beispiel zeigen" },
#elif FRENCH
	{ GADG_PUSH_BUTTON, -80, 30 + FONT_NUM*11 - 50, 0, FONT_NUM, 60, 20, 0, 0, 'O', 0, "OK" },
	{ GADG_PUSH_BUTTON, -80, 30 + FONT_NUM*11 - 20, 0, FONT_NUM, 60, 20, 0, 0, 'Q', 0, "Quitte" },
	{ GADG_CHECK_BOX, 30, 30 + FONT_NUM*11 + 10, 0, FONT_NUM, 0, 0, 0, 0, 'P', 0, "Placer dans menu Fonte" },
	{ GADG_CHECK_BOX, 30, 30 + FONT_NUM*11 + 30, 0, FONT_NUM, 0, 0, 0, 0, 'A', 0, "Afficher exemple" },
#elif SPANISH
	{ GADG_PUSH_BUTTON, -80, 30 + FONT_NUM*11 - 50, 0, FONT_NUM, 60, 20, 0, 0, 'U', 0, "Usar" },
	{ GADG_PUSH_BUTTON, -80, 30 + FONT_NUM*11 - 20, 0, FONT_NUM, 60, 20, 0, 0, 'L', 0, "Listo" },
	{ GADG_CHECK_BOX, 30, 30 + FONT_NUM*11 + 10, 0, FONT_NUM, 0, 0, 0, 0, 'A', 0, "A�adir al Menu" },
	{ GADG_CHECK_BOX, 30, 30 + FONT_NUM*11 + 30, 0, FONT_NUM, 0, 0, 0, 0, 'M', 0, "Mostrar Ejemplo" },
#elif SWEDISH
	{ GADG_PUSH_BUTTON, -80, 30 + FONT_NUM*11 - 50, 0, FONT_NUM, 60, 20, 0, 0, 'A', 0, "Anv�nd" },
	{ GADG_PUSH_BUTTON, -80, 30 + FONT_NUM*11 - 20, 0, FONT_NUM, 60, 20, 0, 0, 'K', 0, "Klar" },
	{ GADG_CHECK_BOX, 30, 30 + FONT_NUM*11 + 10, 0, FONT_NUM, 0, 0, 0, 0, 'L', 0, "L�gg till i typsnittsmenyn" },
	{ GADG_CHECK_BOX, 30, 30 + FONT_NUM*11 + 30, 0, FONT_NUM, 0, 0, 0, 0, 'V', 0, "Visa exempel" },
#endif

	SL_GADG_BOX(20, 30, FONT_WIDTH, FONT_NUM),
	SL_GADG_UPARROW(20, 30, FONT_WIDTH, FONT_NUM),
	SL_GADG_DOWNARROW(20, 30, FONT_WIDTH, FONT_NUM),
	SL_GADG_SLIDER(20, 30, FONT_WIDTH, FONT_NUM),

	SL_GADG_BOX(30 + FONT_WIDTH, 30, SIZE_WIDTH, SIZE_NUM),
	SL_GADG_UPARROW(30 + FONT_WIDTH, 30, SIZE_WIDTH, SIZE_NUM),
	SL_GADG_DOWNARROW(30 + FONT_WIDTH, 30, SIZE_WIDTH, SIZE_NUM),
	SL_GADG_SLIDER(30 + FONT_WIDTH, 30, SIZE_WIDTH, SIZE_NUM),

	{ GADG_EDIT_TEXT | GADG_EDIT_NUMONLY,
		30 + FONT_WIDTH, 30 + (FONT_NUM - 1)*11, 1, FONT_NUM,
		48, 11, 0, 0,
		0, 0, NULL },

	{ GADG_STAT_BORDER,
		10, 70 + FONT_NUM*11 + 10, 0, FONT_NUM,
		-20, 40, 0, 0, 0, 0, NULL },

#if (AMERICAN | BRITISH)
	{ GADG_STAT_TEXT, 20, 10, 0, 0, 0, 0, 0, 0, 0, 0, "Font name:" },
	{ GADG_STAT_TEXT, 30 + FONT_WIDTH, 10, 0, 0, 0, 0, 0, 0, 0, 0, "Size:" },
#elif GERMAN
	{ GADG_STAT_TEXT, 20, 10, 0, 0, 0, 0, 0, 0, 0, 0, "Font-Name:" },
	{ GADG_STAT_TEXT, 30 + FONT_WIDTH, 10, 0, 0, 0, 0, 0, 0, 0, 0, "Gr��e:" },
#elif FRENCH
	{ GADG_STAT_TEXT, 20, 10, 0, 0, 0, 0, 0, 0, 0, 0, "Nom fonte:" },
	{ GADG_STAT_TEXT, 30 + FONT_WIDTH, 10, 0, 0, 0, 0, 0, 0, 0, 0, "Corps:" },
#elif SPANISH
	{ GADG_STAT_TEXT, 20, 10, 0, 0, 0, 0, 0, 0, 0, 0, "Nombre de la letra:" },
	{ GADG_STAT_TEXT, 30 + FONT_WIDTH, 10, 0, 0, 0, 0, 0, 0, 0, 0, "Tama�o:" },
#elif SWEDISH
	{ GADG_STAT_TEXT, 20, 10, 0, 0, 0, 0, 0, 0, 0, 0, "Typsnitt:" },
	{ GADG_STAT_TEXT, 30 + FONT_WIDTH, 10, 0, 0, 0, 0, 0, 0, 0, 0, "Storlek:" },
#endif

	{ GADG_ITEM_NONE }
};

DialogTemplate _tbSelFontDlgTempl = {
#if (AMERICAN | BRITISH | FRENCH | SWEDISH | SPANISH)
	DLG_TYPE_ALERT, 0, -1, -1, 180 + FONT_WIDTH, 30 + 85 + 40 + 60, fontGadgets, NULL
#elif GERMAN
	DLG_TYPE_ALERT, 0, -1, -1, 190 + FONT_WIDTH, 30 + 85 + 40 + 60, fontGadgets, NULL
#endif
};

/*
 *	Macro name dialog
 */

static GadgetTemplate macroNameGadgets[] = {
#if (AMERICAN | BRITISH)
	{ GADG_PUSH_BUTTON,  55, -30, 0, 0,  70, 20, 0, 0,        'E', 0, "Execute" },
	{ GADG_PUSH_BUTTON, 175, -30, 0, 0,  70, 20, 0, 0, CHR_CANCEL, 0, _strCancel },
#elif FRENCH
	{ GADG_PUSH_BUTTON,  55, -30, 0, 0,  70, 20, 0, 0,        'E', 0, "Ex�cute" },
	{ GADG_PUSH_BUTTON, 175, -30, 0, 0,  70, 20, 0, 0, CHR_CANCEL, 0, _strCancel },
#elif GERMAN
	{ GADG_PUSH_BUTTON,  50, -30, 0, 0,  80, 20, 0, 0,        'u', 0, "Ausf�hren" },
	{ GADG_PUSH_BUTTON, 170, -30, 0, 0,  80, 20, 0, 0, CHR_CANCEL, 0, _strCancel },
#elif SPANISH
	{ GADG_PUSH_BUTTON,  55, -30, 0, 0,  70, 20, 0, 0,        'E', 0, "Ejecutar" },
	{ GADG_PUSH_BUTTON, 175, -30, 0, 0,  70, 20, 0, 0, CHR_CANCEL, 0, _strCancel },
#elif SWEDISH
	{ GADG_PUSH_BUTTON,  55, -30, 0, 0,  70, 20, 0, 0,        'U', 0, "Utf�r" },
	{ GADG_PUSH_BUTTON, 175, -30, 0, 0,  70, 20, 0, 0, CHR_CANCEL, 0, _strCancel },
#endif

	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 112, 10, 0, 0, 21*8, 0, 0, 0, 0, 0, NULL },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 112, 30, 0, 0, 21*8, 0, 0, 0, 0, 0, NULL },

	{ GADG_PUSH_BUTTON, 90, 10, 0, -3, 15, 11, 0, 6, '?', 0, "?" },

#if (AMERICAN | BRITISH)
	{ GADG_STAT_TEXT,  20, 10, 0, 0, 0, 0, 0, 0, 0, 0, "Macro:" },
	{ GADG_STAT_TEXT,  20, 30, 0, 0, 0, 0, 0, 0, 0, 0, "Parameter:" },
#elif GERMAN
	{ GADG_STAT_TEXT,  20, 10, 0, 0, 0, 0, 0, 0, 0, 0, "Makro:" },
	{ GADG_STAT_TEXT,  20, 30, 0, 0, 0, 0, 0, 0, 0, 0, "Parameter:" },
#elif FRENCH
	{ GADG_STAT_TEXT,  20, 10, 0, 0, 0, 0, 0, 0, 0, 0, "Macro:" },
	{ GADG_STAT_TEXT,  20, 30, 0, 0, 0, 0, 0, 0, 0, 0, "Param�tre:" },
#elif SPANISH
	{ GADG_STAT_TEXT,  20, 10, 0, 0, 0, 0, 0, 0, 0, 0, "Macro:" },
	{ GADG_STAT_TEXT,  20, 30, 0, 0, 0, 0, 0, 0, 0, 0, "Parametro:" },
#elif SWEDISH
	{ GADG_STAT_TEXT,  20, 10, 0, 0, 0, 0, 0, 0, 0, 0, "Makro:" },
	{ GADG_STAT_TEXT,  20, 30, 0, 0, 0, 0, 0, 0, 0, 0, "Parameter:" },
#endif

	{ GADG_ITEM_NONE }
};

DialogTemplate _tbMacroNameDlgTempl = {
	DLG_TYPE_ALERT, 0, -1, -1, 300, 90, macroNameGadgets, NULL
};

/*
 *	Serialization dialog
 */

static GadgetTemplate	serialGadgets[] = {
	{ GADG_PUSH_BUTTON,  -80, -30, 0, 0,   60, 20, 0, 0, 'O', 0, "OK" },
	{ GADG_PUSH_BUTTON, -160, -30, 0, 0,   60, 20, 0, 0, 'C', 0, "Cancel" },

	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE,  96,  55, 0, 0, 8*28,  0, 0, 0, 0, 0, NULL },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE,  96,  80, 0, 0, 8*28,  0, 0, 0, 0, 0, NULL },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 144, 105, 0, 0, 8*22,  0, 0, 0, 0, 0, NULL },

	{ GADG_STAT_TEXT, 20,  10, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_STDBORDER, 20, 40, 0, 0, 340-40, 0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	{ GADG_STAT_TEXT, 20,  55, 0, 0, 0, 0, 0, 0, 0, 0, "Name:" },
	{ GADG_STAT_TEXT, 20,  80, 0, 0, 0, 0, 0, 0, 0, 0, "Company:" },
	{ GADG_STAT_TEXT, 20, 105, 0, 0, 0, 0, 0, 0, 0, 0, "Serial number:" },

	{ GADG_ITEM_NONE }
};

DialogTemplate	_tbSerialDlgTempl = {
	DLG_TYPE_ALERT, 0, -1, -1, 340, 160, serialGadgets, NULL
};

/*
 *	Month and day names
 */

#if (AMERICAN | BRITISH)

TextPtr _monthNames[] = {
	"January",		"February",		"March",		"April",
	"May",			"June",			"July",			"August",
	"September",	"October",		"November",		"December"
};

TextPtr _monthNamesAbbr[] = {
	"Jan", "Feb", "Mar", "Apr", "May", "Jun",
	"Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
};

TextPtr _dayNames[] = {
	"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
};

TextPtr _dayNamesAbbr[] = {
	"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"
};

#elif GERMAN

TextPtr _monthNames[] = {
	"Januar",		"Februar",		"M�rz",			"April",
	"Mai",			"Juni",			"Juli",			"August",
	"September",	"Oktober",		"November",		"Dezember"
};

TextPtr _monthNamesAbbr[] = {
	"Jan", "Feb", "M�r", "Apr", "Mai", "Jun",
	"Jul", "Aug", "Sep", "Okt", "Nov", "Dez"
};

TextPtr _dayNames[] = {
	"Sonntag", "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag"
};

TextPtr _dayNamesAbbr[] = {
	"So", "Mo", "Di", "Mi", "Do", "Fr", "Sa"
};

#elif FRENCH

TextPtr _monthNames[] = {
	"Janvier",		"F�vrier",		"Mars",			"Avril",
	"Mai",			"Juin",			"Juillet",		"Ao�t",
	"Septembre",	"Octobre",		"Novembre",		"D�cembre"
};

TextPtr _monthNamesAbbr[] = {
	"Jan", "F�v", "Mar", "Avr", "Mai", "Jun",
	"Jul", "Ao�", "Sep", "Oct", "Nov", "D�c"
};

TextPtr _dayNames[] = {
	"Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi"
};

TextPtr _dayNamesAbbr[] = {
	"Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam"
};

#elif SPANISH

TextPtr _monthNames[] = {
	"Enero",		"Febrero",		"Marzo",		"Abril",
	"Mayo",			"Junio",		"Julio",		"Agosto",
	"Septiembre",	"Octubre",		"Noviembre",	"Diciembre"
};

TextPtr _monthNamesAbbr[] = {
	"Ene", "Feb", "Mar", "Abr", "May", "Jun",
	"Jul", "Ago", "Sep", "Oct", "Nov", "Dic"
};

TextPtr _dayNames[] = {
	"Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"
};

TextPtr _dayNamesAbbr[] = {
	"Dom", "Lun", "Mar", "Mier", "Jue", "Vie", "Sab"
};

#elif SWEDISH

TextPtr _monthNames[] = {
	"januari",		"februari",		"mars",			"april",
	"maj",			"juni",			"juli",			"augusti",
	"september",	"oktober",		"november",		"december"
};

TextPtr _monthNamesAbbr[] = {
	"jan", "feb", "mar", "apr", "maj", "jun",
	"jul", "aug", "sep", "okt", "nov", "dec"
};

TextPtr _dayNames[] = {
	"s�ndag", "m�ndag", "tisdag", "onsdag", "torsdag", "fredag", "l�rdag"
};

TextPtr _dayNamesAbbr[] = {
	"s�n", "m�n", "tis", "ons", "tor", "fre", "l�r"
};

#endif

/*
 *	Date formats, indexed by date type
 */

#if AMERICAN

TextPtr _dateFormats[] = {
	"\x04/\x01/\x08",		// MONTH_NUMBER/DAY_NUMBER/YEAR_SHORT
	"\x06 \x01, \x07",		// MONTH_NAMEABBREV DAY_NUMBER, YEAR_LONG
	"\x05 \x01, \x07",		// MONTH_NAME DAY_NUMBER, YEAR_LONG
	"\x03, \x06 \x01, \x07",	// DAY_NAMEABBREV, MONTH_NAMEABBREV DAY_NUMBER, YEAR_LONG
	"\x02, \x05 \x01, \x07",	// DAY_NAME, MONTH_NAME DAY_NUMBER, YEAR_LONG
	"\x01 \x06 \x08"		// DAY_NUMBER MONTH_NAMEABBREV YEAR_SHORT
};

#elif BRITISH

TextPtr _dateFormats[] = {
	"\x09/\x0A/\x07",		// DAY_NUMZERO/MONTH_NUMZERO/YEAR_LONG
	"\x06 \x01, \x07",		// MONTH_NAMEABBREV DAY_NUMBER, YEAR_LONG
	"\x05 \x01, \x07",		// MONTH_NAME DAY_NUMBER, YEAR_LONG
	"\x03, \x06 \x01, \x07",	// DAY_NAMEABBREV, MONTH_NAMEABBREV DAY_NUMBER, YEAR_LONG
	"\x02, \x05 \x01, \x07",	// DAY_NAME, MONTH_NAME DAY_NUMBER, YEAR_LONG
	"\x01 \x06 \x08"		// DAY_NUMBER MONTH_NAMEABBREV YEAR_SHORT
};

#elif GERMAN

TextPtr _dateFormats[] = {
	"\x01.\x0A.\x07",		// DAY_NUMBER.MONTH_NUMZERO.YEAR_LONG
	"\x01. \x06 \x07",		// DAY_NUMBER. MONTH_NAMEABBREV YEAR_LONG
	"\x01. \x05 \x07",		// DAY_NUMBER. MONTH_NAME YEAR_LONG
	"\x03, \x01. \x06 \x07",	// DAY_NAMEABBREV, DAY_NUMBER. MONTH_NAMEABBREV YEAR_LONG
	"\x02, \x01. \x05 \x07",	// DAY_NAME, DAY_NUMBER. MONTH_NAME YEAR_LONG
	"\x01 \x06 \x08"		// DAY_NUMBER MONTH_NAMEABBREV YEAR_SHORT
};

#elif FRENCH

TextPtr _dateFormats[] = {
	"\x01.\x0A.\x08",		// DAY_NUMBER.MONTH_NUMZERO.YEAR_SHORT
	"\x01 \x06 \x07",		// DAY_NUMBER MONTH_NAMEABBREV YEAR_LONG
	"\x01 \x05 \x07",		// DAY_NUMBER MONTH_NAME YEAR_LONG
	"\x03 \x01 \x06 \x07",	// DAY_NAMEABBREV DAY_NUMBER MONTH_NAMEABBREV YEAR_LONG
	"\x02 \x01 \x05 \x07",	// DAY_NAME DAY_NUMBER MONTH_NAME YEAR_LONG
	"\x01 \x06 \x08"		// DAY_NUMBER MONTH_NAMEABBREV YEAR_SHORT
};

#elif SPANISH

TextPtr _dateFormats[] = {
	"\x04/\x01/\x08",		// MONTH_NUMBER/DAY_NUMBER/YEAR_SHORT
	"\x06 \x01, \x07",		// MONTH_NAMEABBREV DAY_NUMBER, YEAR_LONG
	"\x05 \x01, \x07",		// MONTH_NAME DAY_NUMBER, YEAR_LONG
	"\x03, \x06 \x01, \x07",	// DAY_NAMEABBREV, MONTH_NAMEABBREV DAY_NUMBER, YEAR_LONG
	"\x02, \x05 \x01, \x07",	// DAY_NAME, MONTH_NAME DAY_NUMBER, YEAR_LONG
	"\x01 \x06 \x08"		// DAY_NUMBER MONTH_NAMEABBREV YEAR_SHORT
};

#elif SWEDISH

TextPtr _dateFormats[] = {
	"\x01.\x04.\x08",		// DAY_NUMBER.MONTH_NUMBER.YEAR_SHORT
	"\x01. \x06 \x07",		// DAY_NUMBER.MONTH_NAMEABBREV YEAR_LONG
	"\x01. \x05 \x07",		// DAY_NUMBER. MONTH_NAME YEAR_LONG
	"\x03, \x01. \x06 \x07",	// DAY_NAMEABBREV, DAY_NUMBER. MONTH_NAMEABBREV  YEAR_LONG
	"\x02, \x01. \x05 \x07",	// DAY_NAME, DAY_NUMBER. MONTH_NAME YEAR_LONG
	"\x01 \x06 \x08"		// DAY_NUMBER MONTH_NAMEABBREV YEAR_SHORT
};

#endif
