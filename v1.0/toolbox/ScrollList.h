/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Amiga Toolbox
 *	Copyright (c) 1990 New Horizons Software, Inc.
 *
 *	Scroll List handler
 */

#ifndef TOOLBOX_SCROLLLIST_H
#define TOOLBOX_SCROLLLIST_H

#ifndef INTUITION_INTUITION_H
#include <intuition/intuition.h>
#endif

#ifndef TYPEDEFS_H
#include "typedefs.h"
#endif

#ifndef TOOLBOX_MEMORY_H
#include "Memory.h"
#endif

#ifndef TOOLBOX_LIST_H
#include "List.h"
#endif

/*
 *	Scroll list structure
 */

typedef struct {
	GadgetPtr	ListBox;		/* Border gadget for list */
	GadgetPtr	UpArrow;		/* Up arrow */
	GadgetPtr	DownArrow;		/* Down arrow */
	GadgetPtr	Slider;			/* Scroll box */
	WindowPtr	Window;
	RequestPtr	Request;
	TextAttrPtr	Font;			/* Font for list elements */
	ListHeadPtr	List;			/* The actual list */
	WORD		Offset, Flags;	/* Private */
	WORD		PrevItem;
	ULONG		PrevSeconds, PrevMicros;
	BOOL		DoubleClick;
} ScrollList, *ScrollListPtr;

/*
 *	Prototypes
 */

ScrollListPtr	NewScrollList(BOOL);
void			InitScrollList(ScrollListPtr, GadgetPtr, WindowPtr, RequestPtr);
void			DisposeScrollList(ScrollListPtr);
void			SLDoDraw(ScrollListPtr, BOOL);
void			SLDrawBorder(ScrollListPtr);
void			SLDrawList(ScrollListPtr);
void			SLAddItem(ScrollListPtr, TextPtr, WORD, WORD);
void			SLRemoveItem(ScrollListPtr, WORD);
void			SLRemoveAll(ScrollListPtr);
WORD			SLNumItems(ScrollListPtr);
void			SLGetItem(ScrollListPtr, WORD, TextPtr);
WORD			SLNextSelect(ScrollListPtr, WORD);
BOOL			SLIsSelected(ScrollListPtr, WORD);
void			SLSetItemStyle(ScrollListPtr, WORD, WORD);
void			SLEnableItem(ScrollListPtr, WORD, BOOL);
void			SLSelectItem(ScrollListPtr, WORD, BOOL);
void			SLGadgetMessage(ScrollListPtr, MsgPortPtr, IntuiMsgPtr);
BOOL			SLDoubleClick(ScrollListPtr);
void			SLAutoScroll(ScrollListPtr, WORD);

#endif
