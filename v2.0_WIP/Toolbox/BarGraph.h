/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Toolbox
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	Bar graph routines
 */

#ifndef TOOLBOX_BARGRAPH_H
#define TOOLBOX_BARGRAPH_H

#ifndef GRAPHICS_RASTPORT_H
#include <gfx/rastport.h>
#endif

#ifndef TYPEDEFS_H
#include <TypeDefs.h>
#endif

/*
 *	Bar graph flags
 */

#define BG_HORIZONTAL	0x0001
#define BG_SHOWPERCENT	0x0002

/*
 *	Bar graph structure
 */

typedef struct {
	Rectangle	Rect;
	ULONG		Value, MaxValue;
	WORD		LastSize, LastPercent;
	WORD		NumDivisions;
	WORD		Flags;
} BarGraph, *BarGraphPtr;

/*
 *	Prototypes
 */

BarGraphPtr	NewBarGraph(RectPtr, ULONG, WORD, WORD);
void		DisposeBarGraph(BarGraphPtr);

void	DrawBarGraph(RastPtr, BarGraphPtr);
void	SetBarGraph(RastPtr, BarGraphPtr, ULONG);
void	SetBarGraphMax(RastPtr, BarGraphPtr, ULONG);

#endif
