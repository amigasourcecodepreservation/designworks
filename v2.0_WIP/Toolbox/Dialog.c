/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Toolbox library
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Dialog routines
 */

#define INTUI_V36_NAMES_ONLY	1

#include <exec/types.h>
#include <intuition/intuition.h>
#include <intuition/intuitionbase.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include <string.h>

#include <Typedefs.h>

#include <Toolbox/Globals.h>
#include <Toolbox/Memory.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Border.h>
#include <Toolbox/Request.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Window.h>
#include <Toolbox/Utility.h>

/*
 *	External variables
 */

extern struct IntuitionBase	*IntuitionBase;

/*
 *	Local variables and definitions
 */

#define TAB	0x09
#define CR	0x0D
#define ESC	0x1B

#define SHIFTKEYS	(IEQUALIFIER_LSHIFT | IEQUALIFIER_RSHIFT)
#define ALTKEYS		(IEQUALIFIER_LALT | IEQUALIFIER_RALT)
#define CMDKEY		AMIGARIGHT

#define GADG_DISABLED(gadg)	((gadg)->Flags & GFLG_DISABLED)
#define GADG_ENABLED(gadg)	(!GADG_DISABLED(gadg))

#define SCALE(x, src, dst)	(((LONG) (x)*(dst) + (src)/2)/(src))

static WORD	defaultOKBtn		= OK_BUTTON;
static WORD	defaultCancelBtn	= CANCEL_BUTTON;

/*
 *	Local routine prototypes
 */

static GadgetPtr	NextEditCycle(GadgetPtr, GadgetPtr);
static GadgetPtr	PrevEditCycle(GadgetPtr, GadgetPtr);

static WORD	HandleKey(WindowPtr, TextChar, UWORD, GadgetPtr);
static WORD	HandleDialogMsg(WindowPtr, ULONG, UWORD, UWORD, GadgetPtr);

/*
 *	Return pointer to next cycle edit box, or NULL
 *	Note: this routine does not use gadgList,
 *		but it is included to have the same calling convention as PrevEditCycle
 */

static GadgetPtr NextEditCycle(register GadgetPtr gadgList, register GadgetPtr gadget)
{
	for (gadget = gadget->NextGadget; gadget; gadget = gadget->NextGadget) {
		if (GADG_ENABLED(gadget) && IsEditReturnCycle(gadget))
		   break;
	}
	return (gadget);
}

/*
 *	Return pointer to previous cycle edit box, or NULL
 */

static GadgetPtr PrevEditCycle(register GadgetPtr gadgList, register GadgetPtr gadget)
{
	GadgetPtr editGadg;

	editGadg = NULL;
	for (; gadgList != gadget; gadgList = gadgList->NextGadget) {
		if (GADG_ENABLED(gadgList) && IsEditReturnCycle(gadgList))
			editGadg = gadgList;
	}
	return (editGadg);
}

/*
 *	Handle key down intuition messages in dialog and requester
 *	EditGadg is active edit gadget or NULL
 *	Otherwise returns -1
 */

static WORD HandleKey(WindowPtr window, register TextChar ch, UWORD modifier,
					  GadgetPtr editGadg)
{
	WORD item, btn;
	RequestPtr request;
	register GadgetPtr gadget, gadgList;

	request = window->FirstRequest;
	gadgList = (request) ? request->ReqGadget : window->FirstGadget;
	if (gadgList == NULL)
		return (-1);
/*
	If return and in return-cycle edit box,
		then activate next or prev edit cycle text box
*/
	if (ch == CR && (modifier & IEQUALIFIER_NUMERICPAD) == 0 &&
		editGadg && IsEditReturnCycle(editGadg)) {
		gadget = (modifier & SHIFTKEYS) ?
				 PrevEditCycle(gadgList, editGadg) : NextEditCycle(gadgList, editGadg);
		if (gadget) {
			ActivateGadget(gadget, window, request);
			Delay(5);
			return (-1);
		}
	}
/*
	If ESC key in dialog with close box, simulate close box
*/
	if (ch == ESC && request == NULL && (window->Flags & WFLG_CLOSEGADGET))
		return (DLG_CLOSE_BOX);
/*
	Handle keystroke
*/
	item = -1;
	switch (ch) {
/*
	If return/enter key then simulate OK button
	If ESC key then simulate Cancel button
*/
	case CR:
	case ESC:
		btn = (ch == CR) ? defaultOKBtn : defaultCancelBtn;
		gadget = GadgetItem(gadgList, btn);
		if (gadget && GadgetType(gadget) == GADG_PUSH_BUTTON &&
			DepressGadget(gadget, window, request))
			item = btn;
		break;
/*
	If tab key, activate first text box
*/
	case TAB:
		for (gadget = GadgetItem(gadgList, 0); gadget; gadget = gadget->NextGadget) {
			if ((gadget->GadgetType & GTYP_STRGADGET) && GADG_ENABLED(gadget))
				break;
		}
		if (gadget) {
			ActivateGadget(gadget, window, request);
			Delay(5);
		}
		break;
/*
	Otherwise, search for matching button name
*/
	default:
		for (gadget = gadgList; gadget; gadget = gadget->NextGadget) {
			if (IsGadgetKey(gadget, ch))
				break;
		}
		if (gadget && DepressGadget(gadget, window, request))
			item = GadgetNumber(gadget);
		break;
	}
	return (item);
}

/*
 *	Handle gadget message
 *	Called by CheckDialog and DialogSelect
 */

static TextChar	cursorKey[] = { 0x1C, 0x1D, 0x1E, 0x1F };

static WORD HandleDialogMsg(WindowPtr window, ULONG class, UWORD code, UWORD modifier,
							GadgetPtr gadget)
{
	WORD	item = -1;

	switch (class) {
	case IDCMP_CLOSEWINDOW:
		item = DLG_CLOSE_BOX;
		break;
	case IDCMP_GADGETDOWN:
		if (GadgetType(gadget) == GADG_POPUP) {
			DoPopUpGadget(gadget, window);
			item = GadgetNumber(gadget);
		}
		break;
	case IDCMP_GADGETUP:
		if (gadget == NULL)
			break;
		if (gadget->GadgetType & GTYP_STRGADGET) {
			if (LibraryVersion(IntuitionBase) < OSVERSION_2_0 || code != TAB)
				item = HandleKey(window, CR, modifier, gadget);
		}
		else
			item = GadgetNumber(gadget);
		break;
	case IDCMP_VANILLAKEY:
		if ((modifier & IEQUALIFIER_REPEAT) == 0)
			item = HandleKey(window, (TextChar) code, modifier, NULL);
		break;
	case IDCMP_RAWKEY:					// Handle cursor keys as vanilla key
		if (code >= CURSORUP && code <= CURSORLEFT &&
			(modifier & IEQUALIFIER_REPEAT) == 0)
			item = HandleKey(window, cursorKey[code - CURSORUP], modifier, NULL);
		break;
	}
	return (item);
}

/*
 *	Set default OK and Cancel buttons
 *	If -1 then don't change
 */

void SetDefaultButtons(WORD okBtn, WORD cancelBtn)
{
	if (okBtn != -1)
		defaultOKBtn = okBtn;
	if (cancelBtn != -1)
		defaultCancelBtn = cancelBtn;
}

/*
 *	GetDialog
 *	Process dialog template, and return pointer to Dialog structure
 *	Return NULL if error
 *	If screen is NULL then open on Workbench screen
 */

DialogPtr GetDialog(DlgTemplPtr dlgTempl, ScreenPtr screen, MsgPortPtr msgPort)
{
	WORD				leftEdge, topEdge, width, height, titleHeight, intuiVersion;
	WORD				zoomSize[4];
	ULONG				IDCMPFlags, intuiLock;
	DialogPtr			dlg;
	GadgetPtr			gadget, gadgList;
	WindowPtr			activeWindow;
	ScreenPtr			firstScreen;
	Rectangle			rect;
	Screen				wbScreen;
	struct NewWindow	newWind;

	activeWindow = ActiveWindow();			// To get its screen title later
	intuiVersion = LibraryVersion(IntuitionBase);
/*
	If no screen, then get workbench screen data
*/
	if (screen == NULL) {
		if (!GetScreenData((BYTE *) &wbScreen, sizeof(Screen), WBENCHSCREEN, NULL))
			return (NULL);
		screen = &wbScreen;
	}
	titleHeight = screen->WBorTop + screen->Font->ta_YSize + 1;
/*
	Initialize NewWindow struct
*/
	BlockClear(&newWind, sizeof(struct NewWindow));
	if (intuiVersion < OSVERSION_2_0 && (screen->Flags & SCREENTYPE) == WBENCHSCREEN) {
		newWind.DetailPen = 0;
		newWind.BlockPen = 1;
	}
	else {
		newWind.DetailPen = _tbPenLight;	// For correct prop gadget rendering
		newWind.BlockPen = _tbPenBlack;
	}
	width  = SCALE(dlgTempl->Width, 8, _tbXSize);
	height = SCALE(dlgTempl->Height, 11, _tbYSize);
	switch (dlgTempl->Type) {
	case DLG_TYPE_ALERT:
		newWind.IDCMPFlags = IDCMP_MOUSEBUTTONS | IDCMP_GADGETDOWN | IDCMP_GADGETUP
							 | IDCMP_RAWKEY | IDCMP_ACTIVEWINDOW | IDCMP_INACTIVEWINDOW
							 | IDCMP_INTUITICKS | IDCMP_MENUVERIFY;
		newWind.Flags = WFLG_SMART_REFRESH | WFLG_BORDERLESS | WFLG_ACTIVATE
						| WFLG_NOCAREREFRESH;
		break;
	case DLG_TYPE_WINDOW:
		width  += screen->WBorLeft + screen->WBorRight;
		height += titleHeight + screen->WBorBottom;
		newWind.IDCMPFlags = IDCMP_MOUSEBUTTONS | IDCMP_GADGETDOWN | IDCMP_GADGETUP
							 | IDCMP_RAWKEY | IDCMP_ACTIVEWINDOW | IDCMP_INACTIVEWINDOW
							 | IDCMP_INTUITICKS | IDCMP_MENUVERIFY;
		newWind.Flags = WFLG_DRAGBAR | WFLG_SMART_REFRESH | WFLG_ACTIVATE;
		if (dlgTempl->Flags & DLG_FLAG_CLOSE) {
			newWind.IDCMPFlags |= IDCMP_CLOSEWINDOW;
			newWind.Flags |= WFLG_CLOSEGADGET;
		}
		if (dlgTempl->Flags & DLG_FLAG_DEPTH)
			newWind.Flags |= WFLG_DEPTHGADGET;
		if (dlgTempl->Flags & DLG_FLAG_ZOOM)
			newWind.IDCMPFlags |= IDCMP_REFRESHWINDOW;
		else
			newWind.Flags |= WFLG_NOCAREREFRESH;
		break;
	default:
		return (NULL);
	}
	GetScreenViewRect(screen, &rect);
	if ((leftEdge = dlgTempl->LeftEdge) == -1)
		leftEdge = (rect.MaxX - rect.MinX + 1 - width)/2;
	else
		leftEdge = SCALE(leftEdge, 8, _tbXSize);
	leftEdge += rect.MinX;
	if ((topEdge = dlgTempl->TopEdge) == -1)
		topEdge = (rect.MaxY - rect.MinY + 1 - height)/3;
	else
		topEdge = SCALE(topEdge, 11, _tbYSize);
	topEdge += rect.MinY;
	if (leftEdge < 0)
		leftEdge = 0;
	if (topEdge < 0)
		topEdge = 0;
	if (leftEdge + width > screen->Width)
		width = screen->Width - leftEdge;
	if (topEdge + height > screen->Height)
		height = screen->Height - topEdge;
	newWind.Title		= (dlgTempl->Type == DLG_TYPE_WINDOW) ? dlgTempl->Title : NULL;
	newWind.LeftEdge	= leftEdge;
	newWind.TopEdge		= topEdge;
	newWind.Width		= newWind.MinWidth	= newWind.MaxWidth	= width;
	newWind.Height		= newWind.MinHeight	= newWind.MaxHeight	= height;
	newWind.MaxWidth	= width;
	newWind.MaxHeight	= height;
	if (screen != &wbScreen) {
		newWind.Screen	= screen;
		newWind.Type	= CUSTOMSCREEN;
	}
	else {
		newWind.Screen	= NULL;
		newWind.Type	= WBENCHSCREEN;
	}
/*
	Calculate zoom rect size
*/
	if (intuiVersion >= OSVERSION_3_0)
		zoomSize[0] = zoomSize[1] = -1;		// Size-only zooming
	else {
		zoomSize[0] = newWind.LeftEdge;
		zoomSize[1] = newWind.TopEdge;
	}
	zoomSize[2] = 100 + ((newWind.Title) ? strlen(newWind.Title)*_tbXSize : 0);
	zoomSize[3] = titleHeight;
/*
	Create gadgets (but don't attach to window until it is open and cleared)
*/
	if (dlgTempl->Gadgets == NULL)
		return (NULL);
	gadgList = GetGadgets(dlgTempl->Gadgets);
	if (gadgList == NULL)
		return (NULL);
	if (dlgTempl->Type == DLG_TYPE_WINDOW) {
		for (gadget = gadgList; gadget; gadget = gadget->NextGadget) {
			if ((gadget->Flags & GFLG_RELRIGHT) == 0)
				gadget->LeftEdge += screen->WBorLeft;
			if ((gadget->Flags & GFLG_RELBOTTOM) == 0)
				gadget->TopEdge  += titleHeight;
		}
	}
/*
	Open dialog window
*/
	IDCMPFlags = newWind.IDCMPFlags;
	newWind.IDCMPFlags = 0;
	if (intuiVersion >= OSVERSION_2_0) {
		dlg = (dlgTempl->Type == DLG_TYPE_WINDOW &&
			   (dlgTempl->Flags & DLG_FLAG_ZOOM) != 0) ?
			  OpenWindowTags(&newWind, WA_Zoom, zoomSize, WA_NewLookMenus, TRUE, TAG_END) :
			  OpenWindowTags(&newWind, WA_NewLookMenus, TRUE, TAG_END);
	}
	else
		dlg = OpenWindow(&newWind);
	newWind.IDCMPFlags = IDCMPFlags;
	if (dlg == NULL) {
		DisposeGadgets(gadgList);
		return (NULL);
	}
	dlg->UserPort = msgPort;
	ModifyIDCMP(dlg, IDCMPFlags);
	SetStdPointer(dlg, POINTER_ARROW);
	SetWindowTitles(dlg, (UBYTE *) -1, activeWindow->ScreenTitle);
	intuiLock = LockIBase(0);
	firstScreen = IntuitionBase->FirstScreen;
	UnlockIBase(intuiLock);
	if (dlg->WScreen != firstScreen)
		ScreenToFront(dlg->WScreen);
/*
	Clear dialog window
*/
	GetWindowRect(dlg, &rect);
	SetAPen(dlg->RPort, _tbPenLight);
	FillRect(dlg->RPort, &rect);
/*
	Draw alert dialog border
*/
	if (dlgTempl->Type == DLG_TYPE_ALERT) {
		SetRect(&rect, 0, 0, width - 1, height - 1);
		if (_tbNoShadows) {
			DrawShadowBox(dlg->RPort, &rect, 0, TRUE);
			DrawShadowBox(dlg->RPort, &rect, 2, TRUE);
		}
		else
			DrawShadowBox(dlg->RPort, &rect, 1, TRUE);
	}
/*
	Attach and draw gadgets
*/
	AddGList(dlg, gadgList, 0x7FFF, -1, NULL);
	RefreshGadgets(gadgList, dlg, NULL);
/*
	Activate first text box in dialog
*/
	if (_tbAutoActivate) {
		for (gadget = gadgList; gadget; gadget = gadget->NextGadget) {
			if (GadgetType(gadget) == GADG_EDIT_TEXT)
				break;
		}
		if (gadget)
			ActivateGadget(gadget, dlg, NULL);
	}
	SetWKind(dlg, WKIND_DIALOG);
	return (dlg);
}

/*
 *	DisposeDialog
 *	Release all memory used by dialog created with GetDialog
 */

void DisposeDialog(DialogPtr dlg)
{
	GadgetPtr	gadgList;

	if (dlg == NULL)
		return;
	gadgList = GadgetItem(dlg->FirstGadget, 0);	// First user gadget
	CloseWindowSafely(dlg, dlg->UserPort);
	if (gadgList)
		DisposeGadgets(gadgList);
}

/*
 *	GadgetMsgAvail
 *	Check to see if gadget message is present for given window
 *	Return TRUE if yes, FALSE if not
 */

BOOL GadgetMsgAvail(MsgPortPtr msgPort, WindowPtr window)
{
	register BOOL			found;
	register IntuiMsgPtr	msg, nextMsg;

	found = FALSE;
	Forbid();
	msg = (IntuiMsgPtr) msgPort->mp_MsgList.lh_Head;
	while (nextMsg = (IntuiMsgPtr) msg->ExecMessage.mn_Node.ln_Succ) {
		if ((msg->Class == IDCMP_GADGETDOWN || msg->Class == IDCMP_GADGETUP) &&
			msg->IDCMPWindow == window) {
			found = TRUE;
			break;
		}
		msg = nextMsg;
	}
	Permit();
	return (found);
}

/*
 *	Return TRUE if this is a dialog message
 *	Checks the window kind for WKIND_DIALOG
 */

BOOL IsDialogMsg(IntuiMsgPtr intuiMsg)
{
	return (GetWKind(intuiMsg->IDCMPWindow) == WKIND_DIALOG);
}

/*
 *	CheckDialog
 *	Get and handle dialog and requester events
 *	Returns item number of gadget that was selected
 *	Will return -1 if no gadget messages
 *	Note: Will not return with -1 unless ALL messages at port have been processed
 *	See ModalDialog() below for more details
 */

WORD CheckDialog(MsgPortPtr msgPort, DialogPtr dlg, BOOL (*dlgFilter)(IntuiMsgPtr, WORD *))
{
	register IntuiMsgPtr	intuiMsg;
	ULONG					class;
	UWORD					code, modifier;
	APTR					iAddress;
	WindowPtr				msgWindow;
	WORD					item;

	if (dlg->FirstRequest == NULL && dlg->WLayer->front)
		WindowToFront(dlg);				// If dialog, make sure it is in front
	while (intuiMsg = (IntuiMsgPtr) GetMsg(msgPort)) {
		if (intuiMsg->Class == IDCMP_RAWKEY)
			ConvertKeyMsg(intuiMsg);
		item = -1;
		if (dlgFilter && (*dlgFilter)(intuiMsg, &item)) {
			if (item != -1)
				return (item);
			continue;
		}
		class		= intuiMsg->Class;
		code		= intuiMsg->Code;
		modifier	= intuiMsg->Qualifier;
		iAddress	= intuiMsg->IAddress;
		msgWindow	= intuiMsg->IDCMPWindow;
		if (class == IDCMP_MENUVERIFY && code == MENUHOT && dlg->MenuStrip == NULL)
			intuiMsg->Code = MENUCANCEL;		// Need to do this before replying
		ReplyMsg((MsgPtr) intuiMsg);
		if (msgWindow != dlg)
			continue;
/*
	Handle the message
*/
		if ((item = HandleDialogMsg(dlg, class, code, modifier, iAddress)) != -1)
			return (item);
	}
	return (-1);				// No gadget message present
}

/*
 *	ModalDialog
 *	Get and handle dialog and requester events
 *	Returns item number of gadget that was selected
 *	Will not return to caller until a gadget was actually selected
 *	Ignore all messages that are not for specified window/dialog
 *	If dlgFilter is not NULL, then call dlgFilter(intuiMsg, &item) before
 *		handling event (with item preset to -1)
 *	dlgFilter returns TRUE if it handled the message, FALSE if not
 *		If TRUE, then dlgFilter must have called ReplyMsg(); item is set
 *			to item number to return (or -1)
 *		If FALSE then must have NOT called ReplyMsg()
 */

WORD ModalDialog(MsgPortPtr msgPort, DialogPtr dlg, BOOL (*dlgFilter)(IntuiMsgPtr, WORD *))
{
	register WORD	item;

	do {
		(void) WaitPort(msgPort);
	} while ((item = CheckDialog(msgPort, dlg, dlgFilter)) == -1);
	return (item);
}

/*
 *	DialogSelect
 *	Handle messages in modeless dialogs
 *	For gadget up and key down messages, sets dlg, itemHit and returns TRUE
 *	For all other messages it returns FALSE (with dlg and itemHit undefined)
 *	This routine does not reply to the message
 *	Call IsDialogMsg() to make sure only dialog messages are passed to this routine
 */

BOOL DialogSelect(register IntuiMsgPtr intuiMsg, DialogPtr *dlg, WORD *itemHit)
{
	ULONG	class;
	UWORD	code, modifier;
	APTR	iAddress;

	if (intuiMsg->Class == IDCMP_RAWKEY)
		ConvertKeyMsg(intuiMsg);
	class		= intuiMsg->Class;
	code		= intuiMsg->Code;
	modifier	= intuiMsg->Qualifier;
	iAddress	= intuiMsg->IAddress;
	*dlg		= intuiMsg->IDCMPWindow;
/*
	Handle the message
*/
	if (class == IDCMP_MENUVERIFY && code == MENUHOT && (*dlg)->MenuStrip == NULL)
		intuiMsg->Code = MENUCANCEL;
	else if ((*itemHit = HandleDialogMsg(*dlg, class, code, modifier, iAddress)) != -1)
		return (TRUE);
	return (FALSE);
}

/*
 *	StdAlert
 *	Bring up and handle dialog containing only push buttons
 *	Return number of button pressed, or -1 if no memory for dialog
 */

WORD StdAlert(DlgTemplPtr dlgTempl, ScreenPtr screen, MsgPortPtr msgPort,
			  BOOL (*dlgFilter)(IntuiMsgPtr, WORD *))
{
	WORD		item;
	DialogPtr	dlg;

	if ((dlg = GetDialog(dlgTempl, screen, msgPort)) == NULL)
		return (-1);
	OutlineButton(GadgetItem(dlg->FirstGadget, defaultOKBtn), dlg, NULL, TRUE);
	SysBeep(5);
	item = ModalDialog(msgPort, dlg, dlgFilter);
	DisposeDialog(dlg);
	return (item);
}

/*
 *	DepressGadget
 *	Briefly show gadget in depressed state if gadget is enabled
 *	If in request and is ENDGADGET, remove the request
 *	Return FALSE if gadget is not enabled, TRUE otherwise
 */

BOOL DepressGadget(GadgetPtr gadget, WindowPtr window, RequestPtr request)
{
	if (gadget == NULL || GADG_DISABLED(gadget))
		return (FALSE);
	HiliteGadget(gadget, window, request, TRUE);
	Delay(5);
	HiliteGadget(gadget, window, request, FALSE);
	if (request && (gadget->Activation & GACT_ENDGADGET))
		EndRequest(request, window);
	return (TRUE);
}
