/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Amiga Toolbox
 *	Copyright (c) 1989 New Horizons Software, Inc.
 *
 *	Misc graphics routines
 */

#include <clib/macros.h>
#include <exec/types.h>
#include <intuition/intuition.h>
#include <intuition/intuitionbase.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/layers.h>
#include <proto/intuition.h>

#include "typedefs.h"

#include "Memory.h"
#include "Graphics.h"

/*
 *	External variables
 */

extern struct IntuitionBase *IntuitionBase;

/*
 *	LocalToGlobal(window, xp, yp)
 *	Convert local window coordinates to global coordinates
 *	Global coordinates are for HIRES LACE screens
 */

void LocalToGlobal(window, xp, yp)
register WindowPtr window;
register WORD *xp, *yp;
{
	register WORD modes;
	register ScreenPtr screen;

	screen = window->WScreen;
	*xp += window->LeftEdge + screen->LeftEdge;
	*yp += window->TopEdge + screen->TopEdge;
	if (((modes = ViewPortAddress(window)->Modes) & HIRES) == 0)
		*xp <<= 1;
	if ((modes & LACE) == 0)
		*yp <<= 1;
}

/*
 *	WhichScreen(x, y)
 *	Return pointer to screen the (global) point is in, or NULL
 */

ScreenPtr WhichScreen(x, y)
register WORD x, y;
{
	register WORD x1, y1;
	register UWORD modes;
	register LONG intuiLock;
	register ScreenPtr screen;

	intuiLock = LockIBase(0);
	for (screen = IntuitionBase->FirstScreen; screen; screen = screen->NextScreen) {
		modes = screen->ViewPort.Modes;
		x1 = (modes & HIRES) ? x : x >> 1;
		y1 = (modes & LACE)  ? y : y >> 1;
		if (x1 >= screen->LeftEdge && x1 < screen->LeftEdge + screen->Width &&
			y1 >= screen->TopEdge  && y1 < screen->TopEdge + screen->Height)
			break;
	}
	UnlockIBase(intuiLock);
	return (screen);
}

/*
 *	ScrollRect(layer, dx, dy, xMin, yMin, xMax, yMax)
 *	Move the bits in the raster by (dx,dy) towards (0,0)
 *	Limit the scroll operation to the rectangle (xMin,yMin,xMax,yMax)
 *	Add the exposed area to the layer's damage list
 */

void ScrollRect(layer, dx, dy, xMin, yMin, xMax, yMax)
register LayerPtr layer;
register WORD dx, dy, xMin, yMin, xMax, yMax;
{
	register LayerPtr frontLayer;
	register WORD xOffset, yOffset;
	RegionPtr updateRgn;
	Rectangle rect;

	if ((dx == 0 && dy == 0) || (updateRgn = NewRegion()) == NULL)
		return;
/*
	Handle actually scrolling in rectangle
*/
	if (ABS(dx) <= xMax - xMin && ABS(dy) <= yMax - yMin) {
		ScrollRaster(layer->rp, dx, dy, xMin, yMin, xMax, yMax);
		if (dx) {
			if (dx > 0) {
				rect.MinX = xMax - dx + 1;
				rect.MaxX = xMax;
			}
			else {						/* dx < 0 */
				rect.MinX = xMin;
				rect.MaxX = xMin - dx - 1;
			}
			rect.MinY = yMin;
			rect.MaxY = yMax;
			(void) OrRectRegion(updateRgn, &rect);
		}
		if (dy) {
			if (dy > 0) {
				rect.MinY = yMax - dy + 1;
				rect.MaxY = yMax;
			}
			else {						/* dy < 0 */
				rect.MinY = yMin;
				rect.MaxY = yMin - dy - 1;
			}
			rect.MinX = xMin;
			rect.MaxX = xMax;
			(void) OrRectRegion(updateRgn, &rect);
		}
/*
	Add any overlapping layer's rectangles to the update region
*/
		if (layer->Flags & LAYERSIMPLE) {
			xOffset = layer->bounds.MinX + dx;
			yOffset = layer->bounds.MinY + dy;
			for (frontLayer = layer->front; frontLayer; frontLayer = frontLayer->front) {
				rect.MinX = frontLayer->bounds.MinX - xOffset;
				rect.MinY = frontLayer->bounds.MinY - yOffset;
				rect.MaxX = frontLayer->bounds.MaxX - xOffset;
				rect.MaxY = frontLayer->bounds.MaxY - yOffset;
				if (rect.MinX > xMax || rect.MinY > yMax ||
					rect.MaxX < xMin || rect.MaxY < yMin)
					continue;
				if (rect.MinX < xMin)
					rect.MinX = xMin;
				if (rect.MinY < yMin)
					rect.MinY = yMin;
				if (rect.MaxX > xMax)
					rect.MaxX = xMax;
				if (rect.MaxY > yMax)
					rect.MaxY = yMax;
				(void) OrRectRegion(updateRgn, &rect);
			}
		}
	}
/*
	Scrolling more than the entire contents of the rectangle, just update all
*/
	else {
		rect.MinX = xMin;
		rect.MinY = yMin;
		rect.MaxX = xMax;
		rect.MaxY = yMax;
		(void) OrRectRegion(updateRgn, &rect);
	}
	if (layer->ClipRegion)
		(void) AndRegionRegion(layer->ClipRegion, updateRgn);
	(void) OrRegionRegion(updateRgn, layer->DamageList);
	DisposeRegion(updateRgn);
}

/*
 *	LoadRGB4CM
 *	Load a set of color values into a color map structure
 */

void LoadRGB4CM(colorMap, colors, count)
register ColorMapPtr colorMap;
register UWORD *colors;
register WORD count;
{
	register WORD i, color;

	for (i = 0; i < count; i++) {
		color = colors[i];
		SetRGB4CM(colorMap, i, (color >> 8) & 0x0F, (color >> 4) & 0x0F,
				  color & 0x0F);
	}
}

/*
 *	Initialize rast port and attach layer
 *	Does not set bit map plane pointers
 */

RastPtr OpenPort()
{
	LayerPtr layer;
	LayerInfoPtr layerInfo;
	BitMapPtr bitMap;

/*
	Allocate and initialize structures
*/
	if ((bitMap = MemAlloc(sizeof(BitMap), MEMF_CLEAR)) != NULL) {
		if ((layerInfo = NewLayerInfo()) != NULL) {
			if ((layer = CreateUpfrontLayer(layerInfo, bitMap, 0, 0, MAX_BLIT_WIDTH,
											MAX_BLIT_WIDTH, LAYERSIMPLE, NULL)) != NULL)
				return (layer->rp);
			DisposeLayerInfo(layerInfo);
		}
		MemFree(bitMap, sizeof(BitMap));
	}
	return (NULL);
}

/*
 *	Dispose of memory allocated by OpenPort
 */

void ClosePort(RastPtr rPort)
{
	LayerPtr layer;
	LayerInfoPtr layerInfo;
	BitMapPtr bitMap;

	layer = rPort->Layer;
	layerInfo = layer->LayerInfo;
	bitMap = rPort->BitMap;
	MemFree(bitMap, sizeof(BitMap));
	DeleteLayer(NULL, layer);
	DisposeLayerInfo(layerInfo);
}

/*
 *	Set rectangle to given values
 */

void SetRect(rect, minX, minY, maxX, maxY)
register RectPtr rect;
WORD minX, minY, maxX, maxY;
{
	rect->MinX = minX;		rect->MinY = minY;
	rect->MaxX = maxX;		rect->MaxY = maxY;
}

/*
 *	Get current user clip region for layer
 */

RegionPtr GetClip(LayerPtr layer)
{
	register RegionPtr clipRgn;

	if (layer->ClipRegion == NULL || (clipRgn = NewRegion()) == NULL)
		return (NULL);
	(void) OrRegionRegion(layer->ClipRegion, clipRgn);
	return (clipRgn);
}

/*
 *	Set clip region, disposing of old clip region
 */

void SetClip(LayerPtr layer, RegionPtr clipRgn)
{
	register RegionPtr oldRgn;

	oldRgn = InstallClipRegion(layer, clipRgn);
	if (oldRgn)
		DisposeRegion(oldRgn);
}

/*
 *	Set rectangular clip region, disposing of old clip region
 */

void SetRectClip(LayerPtr layer, RectPtr rect)
{
	register RegionPtr clipRgn;

	if ((clipRgn = NewRegion()) != NULL) {
		if (OrRectRegion(clipRgn, rect))
			SetClip(layer, clipRgn);
		else
			DisposeRegion(clipRgn);
	}
}

/*
 *	Determine if layer is covered by front layers
 */

BOOL LayerObscured(LayerPtr layer)
{
	Rectangle bounds;

	bounds = layer->bounds;
	while (layer->front) {
		layer = layer->front;
		if (bounds.MinX <= layer->bounds.MaxX && bounds.MaxX >= layer->bounds.MinX &&
			bounds.MinY <= layer->bounds.MaxY && bounds.MaxY >= layer->bounds.MinY)
			return (TRUE);
	}
	return (FALSE);
}
