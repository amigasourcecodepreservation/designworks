/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Main Program
 */

#include <exec/types.h>
#include <intuition/intuition.h>
#include <workbench/workbench.h>

#include <proto/exec.h>
#include <proto/intuition.h>

#include <Rexx/rxslib.h>

#include <Toolbox/Window.h>
#include <Toolbox/Utility.h>

#include "Draw.h"
#include "Proto.h"

/*
 *	External variables
 */

extern WORD	intuiVersion;

extern struct RexxLib		*RexxSysBase;
extern struct HotLinksBase 	*HotLinksBase;
extern struct IFFParseBase  *IFFParseBase;

extern ScreenPtr	screen;
extern WindowPtr	backWindow;
extern WindowPtr	penWindow, fillWindow;
extern MsgPortPtr	mainMsgPort;
extern MsgPortPtr	appIconMsgPort;
extern MsgPort		monitorMsgPort;		/* Not a pointer! */
extern MsgPort		rexxMsgPort;
extern MsgPortPtr	hotLinksMsgPort;
extern MsgPortPtr	clipboardMsgPort;

extern WindowPtr	windowList[];
extern WORD			numWindows;

extern WindowPtr	closeWindow;
extern BOOL			closeFlag, closeAllFlag, quitFlag;

extern BOOL	inMacro, drawOff;

extern BOOL	cursorRepeat;

extern BOOL	busy, titleChanged;

extern FKey	fKeyTable1[], fKeyTable2[];

extern BOOL	_tbOnPubScreen;

/*
 *	Local variables and definitions
 */

#define RAWKEY_F1	0x50
#define RAWKEY_F10	0x59
#define RAWKEY_HELP	0x5F

static BOOL	newPrefs;		/* Inited to FALSE */

#define MOUSEBUTTON(q)	\
	((q & (IEQUALIFIER_LEFTBUTTON | IEQUALIFIER_RBUTTON)) || ((q & AMIGAKEYS) && (q & (IEQUALIFIER_LALT | IEQUALIFIER_RALT))))

/*
 *	Local prototypes
 */

static void	DoNewPrefs(void);
static void	DoFunctionKey(WindowPtr, UWORD, UWORD);
static void	DoRawKey(WindowPtr, UWORD, UWORD);
static void	DoVanillaKey(WindowPtr, UWORD, UWORD);

/*
 *	Main program
 */

void main(int argc, char *argv[])
{
	WORD					mouseX, mouseY;
	UWORD					modifier;
	ULONG					waitSigs;
	ULONG 					secs, micros, userSecs;
	BOOL					isTick;
	register IntuiMsgPtr	intuiMsg;
	struct RexxMsg			*rexxMsg;
	struct AppMessage		*appMsg;
	struct HLMsg			*hlMsg;
	WindowPtr				window;
	struct SatsifyMsg	*cbMsg;
/*
	Initialize, open document window, and start new document
*/
	Init(argc, argv);
	SetUp(argc, argv);
	OpenToolWindow();
	OpenPenWindow();
	OpenFillWindow();
	SetViewMenu();
/*
	Do auto exec macro
*/
	DoAutoExecMacro();
/*
	Get and respond to messages
*/
	quitFlag = FALSE;
	mouseX = mouseY = -1;
	CurrentTime(&userSecs, &micros);
	for (;;) {
/*
	Process intuition messages
*/
		while ((intuiMsg = (IntuiMsgPtr) GetMsg(mainMsgPort)) != NULL) {
			isTick = intuiMsg->Class == INTUITICKS;
			if (!isTick || intuiVersion >= OSVERSION_2_0)
				modifier = intuiMsg->Qualifier;
			if (intuiMsg->Class == SIZEVERIFY) {
				TextUpdate();
				ReplyMsg((MsgPtr) intuiMsg);
			}
			else
				DoIntuiMessage(intuiMsg);
			if (!isTick)
				CurrentTime(&userSecs, &micros);
		}
/*
	Process REXX messages
	Only do one Rexx message per pass through main loop, so that all windows
		are opened/closed and program is in correct state
*/
		if (RexxSysBase &&
			(rexxMsg = (struct RexxMsg *) GetMsg(&rexxMsgPort)) != NULL)
			DoRexxMsg(rexxMsg);
/*
	Process HotLinks messages
*/
		if (HotLinksBase &&
			(hlMsg = (struct HLMsg *) GetMsg(hotLinksMsgPort)) != NULL)
			DoHotLinksMsg(hlMsg);
/*
	Process clipboard messages
*/
		if (IFFParseBase &&
			(cbMsg = (struct SatisfyMsg *) GetMsg(clipboardMsgPort)) != NULL)
			DoClipboardMsg(cbMsg);
/*
	Background handling
*/
		TextUpdate();
		CheckColorTable();
		if (!inMacro || !drawOff)
			UpdateWindows();
/*
	Check for mouse move
*/
		if ((screen->MouseX != mouseX || screen->MouseY != mouseY) &&
			!closeFlag && !closeAllFlag && !quitFlag &&
			(intuiVersion < OSVERSION_2_0 || !MOUSEBUTTON(modifier))) {
			mouseX = screen->MouseX;
			mouseY = screen->MouseY;
			SetPointerShape();
			window = ActiveWindow();
			if (IsDocWindow(window))
				UpdateRulerIndic(window);
		}
/*
	Respond to NEWPREFS message (don't want to do this in requesters)
*/
		if (newPrefs) {
			DoNewPrefs();
			newPrefs = FALSE;
		}
/*
	Respond to close window message
*/
		if (closeFlag) {
			(void) DoClose(closeWindow);
			closeFlag = FALSE;
		}
		if (closeAllFlag)
			closeAllFlag = (numWindows) ?
						   DoClose(windowList[numWindows - 1]) : FALSE;
/*
	For quitting, close one window then respond to any messages
*/
		if (quitFlag) {
			if (numWindows)
				quitFlag = DoClose(windowList[numWindows - 1]);
			else if (!inMacro)		/* Wait for macro reply msg before quitting */
				break;
		}
/*
	If all windows are closed, and we are on a public screen, then quit
*/
		if (numWindows == 0 && _tbOnPubScreen)
			break;
/*
	Continue with main loop if closing all windows or quitting
*/
		if (closeAllFlag || quitFlag)
			continue;
/*
	Handle AppIcon messages
*/
		if (!inMacro && appIconMsgPort) {
			while ((appMsg = (struct AppMessage *) GetMsg(appIconMsgPort)) != NULL)
				DoAppMsg(appMsg);
		}
/*
	Check for auto-saves (only after 15 or more seconds of no user activity)
*/
		CurrentTime(&secs, &micros);
		if (secs > userSecs + 15) {
			CheckAutoSave();
			userSecs = secs;		/* In case they cancel a "Save As" requester */
		}
/*
	If wait for messages
*/
		waitSigs = 1L << mainMsgPort->mp_SigBit;
		if (RexxSysBase)
			waitSigs |= 1L << rexxMsgPort.mp_SigBit;
		if (appIconMsgPort)
			waitSigs |= 1L << appIconMsgPort->mp_SigBit;
		if (HotLinksBase)
			waitSigs |= 1 << hotLinksMsgPort->mp_SigBit;
		if (IFFParseBase)
			waitSigs |= 1 << clipboardMsgPort->mp_SigBit;
		Wait(waitSigs);
	}
/*
	Quitting
*/
	SetStdPointer(backWindow, POINTER_WAIT);
	UndoOff();			/* Make sure undo buffer is cleared */
	if (MustWriteClip())
		PutClipboard();
	ClearPaste();
	ShutDown();
}

/*
 *	Handle change in system preferences
 */

static void DoNewPrefs()
{
	WORD i;
	DocDataPtr docData;
	WindowPtr window;
	Rectangle rect;

	if (GetSysPrefs()) {
		BeginWait();
		for (i = 0; i < numWindows; i++) {
			window = windowList[i];
			docData = GetWRefCon(window);
			PrValidate(docData->PrintRec);
			GetWindowRect(window, &rect);
			InvalRect(window, &rect);
			AdjustScrollBars(window);
		}
		EndWait();
		UpdateWindows();
	}
}

/*
 *	Process intuition messages
 */

void DoIntuiMessage(register IntuiMsgPtr intuiMsg)
{
	register ULONG class;
	register UWORD code;
	register APTR  iAddress;
	register WORD mouseX, mouseY;
	ULONG seconds, micros;
	register UWORD modifier;
	register WindowPtr window;

	if (intuiMsg->Class == RAWKEY)
		ConvertKeyMsg(intuiMsg);	/* Convert some RAWKEY to VANILLAKEY */
	class = intuiMsg->Class;
	code = intuiMsg->Code;
	iAddress = intuiMsg->IAddress;
	mouseX = intuiMsg->MouseX;
	mouseY = intuiMsg->MouseY;
	seconds = intuiMsg->Seconds;
	micros = intuiMsg->Micros;
	modifier = intuiMsg->Qualifier;
	window = intuiMsg->IDCMPWindow;
	if (window == penWindow && (class == RAWKEY || class == GADGETDOWN)) {
		DoPenWindowMessage(intuiMsg);
		return;
	}
	if (window == fillWindow && (class == RAWKEY || class == GADGETDOWN)) {
		DoFillWindowMessage(intuiMsg);
		return;
	}

	ReplyMsg((MsgPtr) intuiMsg);
/*
	Ignore key up messages
*/
	if (class == RAWKEY && (code & IECODE_UP_PREFIX))
		return;
/*
	Ignore user input if processing macro
*/
	if (inMacro && class != NEWSIZE && class != REFRESHWINDOW &&
		class != ACTIVEWINDOW && class != INACTIVEWINDOW && class != NEWPREFS)
		return;
/*
	If not a key message then update typed character display
*/
	if (class != VANILLAKEY && class != INTUITICKS)
		TextUpdate();
/*
	If not a cursor key then turn off cursor repeat
*/
	if (class != INTUITICKS &&
		(class != RAWKEY || (code != CURSORUP && code != CURSORDOWN)))
		cursorRepeat = FALSE;
/*
	Process the message
*/
	switch (class) {
	case INTUITICKS:
		if (IsDocWindow(window) && !inMacro)
			TextBlinkCursor(window);
		break;
	case MOUSEBUTTONS:
		if (code == SELECTDOWN)
			DoMouseDown(window, modifier, mouseX, mouseY, seconds, micros);
		break;
	case GADGETDOWN:
		DoGadgetDown(window, (GadgetPtr) iAddress, modifier);
		break;
	case GADGETUP:
		DoGadgetUp(window, (GadgetPtr) iAddress);
		break;
	case NEWSIZE:
		DoNewSize(window);
		break;
	case REFRESHWINDOW:
		DoWindowRefresh(window);
		break;
	case ACTIVEWINDOW:
		DoWindowActivate(window, TRUE);
		break;
	case INACTIVEWINDOW:
		DoWindowActivate(window, FALSE);
		break;
	case CLOSEWINDOW:
		DoGoAwayWindow(window, modifier);
		break;
	case MENUPICK:
		(void) DoMenu(window, code, modifier, TRUE);
		break;
	case RAWKEY:
		DoRawKey(window, code, modifier);
		break;
	case VANILLAKEY:
		DoVanillaKey(window, code, modifier);
		break;
	case NEWPREFS:
		newPrefs = TRUE;
		break;
	}
}

/*
 *	Handle menu events
 */

BOOL DoMenu(WindowPtr window, UWORD menuNumber, UWORD modifier, BOOL multiSel)
{
	register UWORD menu, item, sub;
	BOOL success;

	if (titleChanged)
		FixTitle();
	success = FALSE;
	while (menuNumber != MENUNULL) {
		menu = MENUNUM(menuNumber);
		item = ITEMNUM(menuNumber);
		sub = SUBNUM(menuNumber);
		switch (menu) {
		case PROJECT_MENU:
			success = DoProjectMenu(window, item, sub, modifier);
			break;
		case EDIT_MENU:
			success = DoEditMenu(window, item, sub, modifier);
			break;
		case LAYOUT_MENU:
			success = DoLayoutMenu(window, item, sub, modifier);
			break;
		case ARRANGE_MENU:
			success = DoArrangeMenu(window, item, sub, modifier);
			break;
		case PEN_MENU:
			success = DoPenMenu(window, item, sub, modifier);
			break;
		case TEXT_MENU:
			success = DoTextMenu(window, item, sub, modifier);
			break;
		case VIEW_MENU:
			success = DoViewMenu(window, item, sub);
			break;
		case MACRO_MENU:
			success = DoMacroMenu(window, item, sub);
			break;
		}
		menuNumber = (multiSel) ?
					 ItemAddress(window->MenuStrip, menuNumber)->NextSelect :
					 MENUNULL;
	}
/*
	Set pointer appearance
*/
	if (IsDocWindow(window)) {
		if ((modifier & CMDKEY) || !multiSel)
			ObscurePointer(window);
		else
			SetPointerShape();
	}
	return (success);
}

/*
 *	Process cursor key
 */

BOOL DoCursorKey(WindowPtr window, UWORD code, UWORD modifier)
{
	BOOL success;

	if (!IsDocWindow(window))
		return (FALSE);
	UndoOff();
	ObscurePointer(window);
	success = FALSE;
	switch (code) {
	case CURSORUP:
		success = DoCursorUp(window, modifier);
		break;
	case CURSORDOWN:
		success = DoCursorDown(window, modifier);
		break;
	case CURSORLEFT:
		success = DoCursorLeft(window, modifier);
		break;
	case CURSORRIGHT:
		success = DoCursorRight(window, modifier);
		break;
	}
	return (success);
}

/*
 *	Process function key
 */

static void DoFunctionKey(WindowPtr window, UWORD code, UWORD modifier)
{
	FKey *fKeyTable;

	if (!IsDocWindow(window) && window != backWindow)
		return;
	if (window != backWindow)
		ObscurePointer(window);
	code -= RAWKEY_F1;
	fKeyTable = (modifier & SHIFTKEYS) ? fKeyTable2 : fKeyTable1;
	if (fKeyTable[code].Type == FKEY_MENU)
		(void) DoMenu(window, (UWORD) fKeyTable[code].Data, modifier & ~SHIFTKEYS, FALSE);
	else if (fKeyTable[code].Type == FKEY_MACRO) {
		if (RexxSysBase)
			DoMacro(fKeyTable[code].Data, NULL);
	}
}

/*
 *	Handle RAWKEY messages
 *	ASCII keys will have already been stripped by ConvertKeyMsg()
 */

static void DoRawKey(WindowPtr window, UWORD code, UWORD modifier)
{
	ResetMultiClick();
	if ((code & IECODE_UP_PREFIX) || (modifier & AMIGAKEYS))
		return;
	busy = TRUE;
/*
	Check for function keys
*/
	if (code >= RAWKEY_F1 && code <= RAWKEY_F10) {
		if ((modifier & IEQUALIFIER_REPEAT) == 0)
			DoFunctionKey(window, code, modifier);
	}
/*
	Check for HELP key
*/
	else if (code == RAWKEY_HELP) {
		if (modifier & SHIFTKEYS)
			DoPreferences();
		else
			DoHelp();
		SetPointerShape();
	}
/*
	Ignore all others for non-document windows
*/
	else if (IsDocWindow(window)) {
/*
	Check for cursor keys
*/
		if (code >= CURSORUP && code <= CURSORLEFT)
			(void) DoCursorKey(window, code, modifier);
/*
	Otherwise is invalid key
*/
		else
			ErrBeep();
	}
	busy = FALSE;
}

/*
 *	Handle VANILLAKEY messages
 *	These messages are synthesized from RAWKEY messages by ConvertKeyMsg()
 */

static void DoVanillaKey(WindowPtr window, UWORD code, UWORD modifier)
{
	DocDataPtr	docData = GetWRefCon(window);

	ResetMultiClick();
	if (!IsDocWindow(window))
		return;
	if (!LockDocument(docData))
		return;
/*
	Handle key
*/
	if (!TextInEdit(window)) {
		if (code == BS || code == DEL)
			DoCut(window, FALSE, TRUE);
	}
	else {
		busy = TRUE;
		ObscurePointer(window);
		if (code == BS || code == DEL)
			TextDelete(window, (TextChar) code, modifier);
		else
			TextInsert(window, (TextChar) code);
		busy = FALSE;
	}
}
