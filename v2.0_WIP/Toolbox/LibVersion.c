/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Toolbox
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	Library version routines
 */

#include <exec/types.h>
#include <exec/libraries.h>

#include <Toolbox/Utility.h>

/*
 *	LibraryVersion
 *	Return version number of library
 */

UWORD LibraryVersion(struct Library *libBase)
{
	return (libBase->lib_Version);
}

/*
 *	SystemVersion
 *	Return version number of system
 */

UWORD SystemVersion()
{
	struct Library *AbsExecBase;

	AbsExecBase = *((struct Library **) 4L);		/* Absolute address */
	return (LibraryVersion(AbsExecBase));
}
