/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991-93 New Horizons Software, Inc.
 *
 *	Dialog templates
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <Toolbox/Border.h>
#include <Toolbox/Image.h>
#include <Toolbox/PopUpList.h>
#include <Toolbox/ScrollList.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Utility.h>
#include <Toolbox/Language.h>

#include "Draw.h"

/*
 *	External variables
 */

extern TextChar	strCancel[];

extern TextChar	strMacroNames1[10][32], strMacroNames2[10][32];

/*
	Repeated and general purpose items
*/

#if (AMERICAN | BRITISH)

#define CHR_OK		'O'
#define CHR_CANCEL	'C'
#define CHR_YES		'Y'
#define CHR_NO		'N'

static TextChar	strOK[]			= "OK";
static TextChar	strYes[]		= "Yes";
static TextChar strUse[]		= "Use";
static TextChar	strNo[]			= "No";
static TextChar strDone[]		= "Done";
static TextChar	strAvailMem[]	= "Available memory:";
static TextChar	graphicsText[]	= "Graphics";
static TextChar	strExpansion[]	= "Expansion";

#elif GERMAN

#elif FRENCH

#elif SWEDISH

#endif

#define GT_ADJUST_UPARROW(left, top)	\
	{ GADG_ACTIVE_STDIMAGE,													\
		left, (top)+5-ARROW_HEIGHT, 0, 0, ARROW_WIDTH, ARROW_HEIGHT, 0, 0,	\
		0x1C, 0, (Ptr) IMAGE_ARROW_UP }

#define GT_ADJUST_DOWNARROW(left, top)	\
	{ GADG_ACTIVE_STDIMAGE,										\
		left, (top)+5, 0, 0, ARROW_WIDTH, ARROW_HEIGHT, 0, 0,	\
		0x1D, 0, (Ptr) IMAGE_ARROW_DOWN }

#define GT_ADJUST_TEXT(left, top)	\
	{ GADG_STAT_TEXT, (left)+10+ARROW_WIDTH, top, 0, 0, 0, 0, 0, 0, 0, 0, NULL }

/*
 *	Init info
 */

static GadgetTemplate initGadgets[] = {
	{ GADG_STAT_TEXT,  40, 120, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT,  40, 135, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT, 136, 150, 0, 0, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_STAT_TEXT,  20,  16, 0, 0, 0, 0, 0, 0, 0, 0, "DesignWorks   2.0" },
	{ GADG_STAT_TEXT, 108,  10, 0, 0, 0, 0, 0, 0, 0, 0, "tm" },
	{ GADG_STAT_TEXT,  20,  40, 0, 0, 0, 0, 0, 0, 0, 0,
		"Copyright \251 1991-93\nNew Horizons Software, Inc.\nAll Rights Reserved" },

	{ GADG_STAT_TEXT,  20,  90, 0, 0, 0, 0, 0, 0, 0, 0,
		"This copy of DesignWorks is\nlicensed to:" },
	{ GADG_STAT_TEXT,  40, 150, 0, 0, 0, 0, 0, 0, 0, 0, "Serial no.:" },

	{ GADG_ITEM_NONE }
};

static DialogTemplate initDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 300, 170, initGadgets, NULL
};

/*
 *	Save changes
 */

static GadgetTemplate saveGadgets[] = {
	{ GADG_PUSH_BUTTON,  30, 60, 0, 0, 60, 20, 0, 0,    CHR_YES, 0, strYes },
	{ GADG_PUSH_BUTTON, -90, 90, 0, 0, 60, 20, 0, 0, CHR_CANCEL, 0, strCancel },
	{ GADG_PUSH_BUTTON,  30, 90, 0, 0, 60, 20, 0, 0,     CHR_NO, 0, strNo },
	{ GADG_STAT_TEXT,  55, 10,  0,  0, 0, 0, 0, 0, 0, 0, "Save changes to" },
	{ GADG_STAT_TEXT,  55, 25,  0,  0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT,  55, 40,  0,  0, 0, 0, 0, 0, 0, 0, "before closing?" },
	{ GADG_STAT_STDIMAGE,  10, 10,  0,  0, 0, 0, 0, 0, 0, 0, (Ptr) IMAGE_ICON_CAUTION },
	{ GADG_ITEM_NONE }
};

static DialogTemplate saveDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 220, 120, saveGadgets, NULL
};

/*
 *	Revert
 */

static GadgetTemplate revertGadgets[] = {
	{ GADG_PUSH_BUTTON,  30, 60, 0, 0, 60, 20, 0, 0, CHR_YES, 0, strYes },
	{ GADG_PUSH_BUTTON, -90, 60, 0, 0, 60, 20, 0, 0,  CHR_NO, 0, strNo },
	{ GADG_STAT_TEXT,  55, 15,  0,  0, 0, 0, 0, 0, 0, 0,
		"Revert to last\nversion saved?" },
	{ GADG_STAT_STDIMAGE,  10, 10,  0,  0, 0, 0, 0, 0, 0, 0, (Ptr) IMAGE_ICON_CAUTION },
	{ GADG_ITEM_NONE }
};

static DialogTemplate revertDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 220, 90, revertGadgets, NULL
};

/*
 *	Page Setup
 */

static TextPtr	paperList[] = {
	"US Letter", "US Legal", "A4 Letter", "Wide Carriage", "", "Custom", NULL
};

static GadgetTemplate pageGadgets[] = {
	{ GADG_PUSH_BUTTON, -80, 10, 0, 0,  60, 20, 0, 0,     CHR_OK, 0, strOK },
	{ GADG_PUSH_BUTTON, -80, 40, 0, 0,  60, 20, 0, 0, CHR_CANCEL, 0, strCancel },

	{ GADG_POPUP,     120, 35, 0, 0, 130, 11, 0, 0, 0, 0, &paperList },

	{ GADG_EDIT_TEXT | GADG_EDIT_NUMONLY, 170, 75, 0, 0, 4*8, 11, 0, 0, 0, 0, NULL },

	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 170, 55, 0, 0, 6*8, 11, 0, 0, 0, 0, NULL },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 290, 55, 0, 0, 6*8, 11, 0, 0, 0, 0, NULL },

	{ GADG_ACTIVE_IMAGE, 25, 115, 0, 0, ICON_WIDTH, ICON_HEIGHT, 0, 0, CURSORKEY_LEFT, 0, NULL },
	{ GADG_ACTIVE_IMAGE, 70, 115, 0, 0, ICON_WIDTH, ICON_HEIGHT, 0, 0, CURSORKEY_RIGHT, 0, NULL },

	{ GADG_CHECK_BOX, 180, 115, 0, 0, 0, 0, 0, 0, 'A', 0, "Aspect Adjusted" },
	{ GADG_CHECK_BOX, 180, 130, 0, 0, 0, 0, 0, 0, 'G', 0, "No Gaps Between Pages" },

	{ GADG_STAT_TEXT, 120,  10, 0, 0, 0, 0, NULL },

	{ GADG_STAT_STDBORDER, 20, 25, 0, 0, 430 - 120, 0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	{ GADG_STAT_TEXT,  20,  10, 0, 0, 0, 0, 0, 0, 0, 0, "Page Setup" },
	{ GADG_STAT_TEXT,  20,  35, 0, 0, 0, 0, 0, 0, 0, 0, "Paper size:" },
	{ GADG_STAT_TEXT, 120,  55, 0, 0, 0, 0, 0, 0, 0, 0, "width" },
	{ GADG_STAT_TEXT, 235,  55, 0, 0, 0, 0, 0, 0, 0, 0, "height" },
	{ GADG_STAT_TEXT,  20,  75, 0, 0, 0, 0, 0, 0, 0, 0, "Reduce or enlarge:" },
	{ GADG_STAT_TEXT, 210,  75, 0, 0, 0, 0, 0, 0, 0, 0, "%" },
	{ GADG_STAT_TEXT,  20,  95, 0, 0, 0, 0, 0, 0, 0, 0, "Orientation" },
	{ GADG_STAT_TEXT, 170,  95, 0, 0, 0, 0, 0, 0, 0, 0, "Options:" },

	{ GADG_ITEM_NONE }
};

static DialogTemplate pageDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 430, 155, pageGadgets, NULL
};

/*
 *	Print
 */

static GadgetTemplate printGadgets[] = {
	{ GADG_PUSH_BUTTON,  -90,  10, 0, 0,  70, 20, 0, 0, 'P', 0, "Print" },
	{ GADG_PUSH_BUTTON,  -90,  40, 0, 0,  70, 20, 0, 0, CHR_CANCEL, 0, strCancel },
	{ GADG_PUSH_BUTTON,  -90,  70, 0, 0,  70, 20, 0, 0, 'O', 0, "Options" },

	{ GADG_RADIO_BUTTON, 120,  35, 0, 0, 0, 0, 0, 0, 's', 0, "PostScript" },
	{ GADG_RADIO_BUTTON, 230,  35, 0, 0, 0, 0, 0, 0, 'H', 0, "High" },
	{ GADG_RADIO_BUTTON, 330,  35, 0, 0, 0, 0, 0, 0, 'N', 0, "Normal" },

	{ GADG_RADIO_BUTTON, 120,  55, 0, 0, 0, 0, 0, 0, 'A', 0, "All" },
	{ GADG_RADIO_BUTTON, 230,  55, 0, 0, 0, 0, 0, 0, 'F', 0, "From" },

	{ GADG_RADIO_BUTTON, 120,  95, 0, 0, 0, 0, 0, 0, 'u', 0, "Automatic" },
	{ GADG_RADIO_BUTTON, 230,  95, 0, 0, 0, 0, 0, 0, 'e', 0, "Hand Feed" },

	{ GADG_CHECK_BOX,    150, 115, 0, 0, 0, 0, 0, 0, 'l', 0, "Collate" },

	{ GADG_EDIT_TEXT | GADG_EDIT_NUMONLY, 120, 75,  0, 0, 5*8, 11,  0, 0, 0, 0, NULL },

	{ GADG_EDIT_TEXT | GADG_EDIT_NUMONLY | GADG_EDIT_RETURNCYCLE, 290, 55,  0, 0, 4*8, 11,  0, 0, 0, 0, NULL },
	{ GADG_EDIT_TEXT | GADG_EDIT_NUMONLY | GADG_EDIT_RETURNCYCLE, 355, 55,  0, 0, 4*8, 11,  0, 0, 0, 0, NULL },

	{ GADG_STAT_TEXT, 120,  10, 0, 0, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_STAT_STDBORDER, 20, 25, 0, 0, 510 - 120, 0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	{ GADG_STAT_TEXT,  20,  10, 0, 0, 0, 0, 0, 0, 0, 0, "Print" },
	{ GADG_STAT_TEXT,  20,  35, 0, 0, 0, 0, 0, 0, 0, 0, "Quality:" },
	{ GADG_STAT_TEXT,  20,  55, 0, 0, 0, 0, 0, 0, 0, 0, "Pages:" },
	{ GADG_STAT_TEXT, 330,  55, 0, 0, 0, 0, 0, 0, 0, 0, "to" },
	{ GADG_STAT_TEXT,  20,  75, 0, 0, 0, 0, 0, 0, 0, 0, "Copies:" },
	{ GADG_STAT_TEXT,  20,  95, 0, 0, 0, 0, 0, 0, 0, 0, "Paper feed:" },
	{ GADG_STAT_TEXT,  20, 115, 0, 0, 0, 0, 0, 0, 0, 0, "Paper handling:" },

	{ GADG_ITEM_NONE }
};

static DialogTemplate printDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 510, 135, printGadgets, NULL
};

/*
 *	Print Options
 */

static GadgetTemplate printOptsGadgets[] = {
	{ GADG_PUSH_BUTTON, -80, 10, 0, 0, 60, 20, 0, 0,     CHR_OK, 0, strOK },
	{ GADG_PUSH_BUTTON, -80, 40, 0, 0, 60, 20, 0, 0, CHR_CANCEL, 0, strCancel },

	{ GADG_RADIO_BUTTON, 100, 35, 0, 0, 0, 0, 0, 0, '4', 0, "4,096" },
	{ GADG_RADIO_BUTTON, 180, 35, 0, 0, 0, 0, 0, 0, '6', 0, "64" },
	{ GADG_RADIO_BUTTON, 260, 35, 0, 0, 0, 0, 0, 0, '8', 0, "8" },

	GT_ADJUST_UPARROW(150, 60),
	GT_ADJUST_DOWNARROW(150, 60),
	GT_ADJUST_TEXT(150, 60),

	{ GADG_STAT_STDBORDER, 20, 25, 0, 0, 420 - 120, 0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	{ GADG_STAT_TEXT, 20, 10, 0, 0, 0, 0, 0, 0, 0, 0, "Print Options" },
#if AMERICAN
	{ GADG_STAT_TEXT, 20, 35, 0, 0, 0, 0, 0, 0, 0, 0, "Colors:" },
#elif BRITISH
	{ GADG_STAT_TEXT, 20, 35, 0, 0, 0, 0, 0, 0, 0, 0, "Colours:" },
#endif
	{ GADG_STAT_TEXT, 20, 60, 0, 0, 0, 0, 0, 0, 0, 0, "Print density:" },

	{ GADG_ITEM_NONE }
};

static DialogTemplate printOptsDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 420, 85, printOptsGadgets, NULL
};

/*
 *	PostScript Options
 */

static TextPtr	screenList[] = {
	"Default", "Dot", "Line", "Ellipse", "Square", "Cross", NULL
};

static TextPtr	angleList[] = {
	"black", "cyan", "magenta", "yellow", NULL
};

static GadgetTemplate postScriptGadgets[] = {
	{ GADG_PUSH_BUTTON, -80, 10, 0, 0, 60, 20, 0, 0,     CHR_OK, 0, strOK },
	{ GADG_PUSH_BUTTON, -80, 40, 0, 0, 60, 20, 0, 0, CHR_CANCEL, 0, strCancel },

	{ GADG_RADIO_BUTTON, 100, 35, 0, 0, 0, 0, 0, 0, 'P', 0, "Parallel Port" },
	{ GADG_RADIO_BUTTON, 240, 35, 0, 0, 0, 0, 0, 0, 'S', 0, "Serial Port" },
	{ GADG_RADIO_BUTTON, 100, 55, 0, 0, 0, 0, 0, 0, 'u', 0, "Custom:" },

	{ GADG_EDIT_TEXT, 180, 55, 0, 0, 19*8, 11, 0, 0, 0, 0, NULL },

#if AMERICAN
	{ GADG_RADIO_BUTTON, 100, 80, 0, 0, 0, 0, 0, 0, 'G', 0, "Gray Scale" },
	{ GADG_RADIO_BUTTON, 240, 80, 0, 0, 0, 0, 0, 0, 'l', 0, "Color" },
#elif BRITISH
	{ GADG_RADIO_BUTTON, 100, 80, 0, 0, 0, 0, 0, 0, 'G', 0, "Grey Scale" },
	{ GADG_RADIO_BUTTON, 240, 80, 0, 0, 0, 0, 0, 0, 'l', 0, "Colour" },
#endif
	{ GADG_POPUP,		 100, 105, 0, 0, 95, 11, 0, 0, 0, 0, &screenList },

	{ GADG_EDIT_TEXT | GADG_EDIT_NUMONLY, 100, 125, 0, 0, 4*8, 11, 0, 0, 0, 0, NULL },

	{ GADG_EDIT_TEXT | GADG_EDIT_NUMONLY, 240, 125, 0, 0, 4*8, 11, 0, 0, 0, 0, NULL },
	{ GADG_POPUP,		 300, 125, 0, 0, 80, 11, 0, 0, 0, 0, &angleList },

	{ GADG_CHECK_BOX,    100, 150, 0, 0, 0, 0, 0, 0, 'F', 0, "Faster Picture Printing" },
	{ GADG_CHECK_BOX,    100, 165, 0, 0, 0, 0, 0, 0, 'I', 0, "Include EOF (^D) Marker" },

	{ GADG_STAT_STDBORDER, 20, 25, 0, 0, 440 - 120, 0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	{ GADG_STAT_TEXT,  20,  10, 0, 0, 0, 0, 0, 0, 0, 0, "PostScript Options" },
	{ GADG_STAT_TEXT,  20,  35, 0, 0, 0, 0, 0, 0, 0, 0, "Send to:" },
	{ GADG_STAT_TEXT,  20,  80, 0, 0, 0, 0, 0, 0, 0, 0, "Print:" },
	{ GADG_STAT_TEXT,  20, 105, 0, 0, 0, 0, 0, 0, 0, 0, "Screen:" },
	{ GADG_STAT_TEXT,  40, 125, 0, 0, 0, 0, 0, 0, 0, 0, "freq.:" },
	{ GADG_STAT_TEXT, 140, 125, 0, 0, 0, 0, 0, 0, 0, 0, "lpi" },
	{ GADG_STAT_TEXT, 185, 125, 0, 0, 0, 0, 0, 0, 0, 0, "angle:" },
	{ GADG_STAT_TEXT, 280, 125, 0, 0, 0, 0, 0, 0, 0, 0, "�" },
	{ GADG_STAT_TEXT,  20, 150, 0, 0, 0, 0, 0, 0, 0, 0, "Options:" },

	{ GADG_ITEM_NONE }
};

static DialogTemplate postScriptDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 440, 185, postScriptGadgets, NULL
};

/*
 *	Cancel Print
 */

static GadgetTemplate cancelPrintGadgets[] = {
	{ GADG_STAT_TEXT,     55,  15, 0, 0,  0,  0, 0, 0,   0, 0, "Printing in progress." },
	{ GADG_PUSH_BUTTON,  -80, -30, 0, 0, 60, 20, 0, 0, CHR_CANCEL, 0, strCancel },
	{ GADG_STAT_TEXT,     55,  30, 0, 0,  0,  0, 0, 0,   0, 0, NULL },
	{ GADG_STAT_STDIMAGE, 10,  10, 0, 0,  0,  0, 0, 0,   0, 0, (Ptr) IMAGE_ICON_NOTE },
	{ GADG_ITEM_NONE }
};

static DialogTemplate cancelPrintDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 300, 80, cancelPrintGadgets, NULL
};

/*
 *	Next Page
 */

static GadgetTemplate nextPageGadgets[] = {
	{ GADG_PUSH_BUTTON, 290, 10, 0, 0, 60, 20, 0, 0,     CHR_OK, 0, strOK },
	{ GADG_PUSH_BUTTON, 290, 40, 0, 0, 60, 20, 0, 0, CHR_CANCEL, 0, strCancel },

	{ GADG_STAT_TEXT,    55, 15, 0, 0,  0,  0, 0, 0,   0, 0,
		"Please insert the next\nsheet of paper." },
	{ GADG_STAT_STDIMAGE, 10, 15, 0, 0, 0, 0, 0, 0, 0, 0, (Ptr) IMAGE_ICON_STOP },

	{ GADG_ITEM_NONE }
};

static DialogTemplate nextPageDialog = {
	DLG_TYPE_ALERT, 0, -1, 0, 370, 70, nextPageGadgets, NULL
};

/*
 *	Save Defaults
 */

static GadgetTemplate savePrefsGadgets[] = {
	{ GADG_PUSH_BUTTON,  60, 60, 0, 0, 60, 20, 0, 0, CHR_YES, 0, strYes },
	{ GADG_PUSH_BUTTON, 180, 60, 0, 0, 60, 20, 0, 0,  CHR_NO, 0, strNo },

#if (AMERICAN | BRITISH)
	{ GADG_STAT_TEXT,    55, 15, 0, 0, 0, 0, 0, 0, 0, 0,
		"Save current format settings\nas default values?" },
#elif GERMAN
	{ GADG_STAT_TEXT,    55, 15, 0, 0, 0, 0, 0, 0, 0, 0,
		"Save current format settings\nas default values?" },
#elif FRENCH
	{ GADG_STAT_TEXT,    55, 15, 0, 0, 0, 0, 0, 0, 0, 0,
		"Sauver les r�glages courants\ncomme format par d�faut?" },
#elif SWEDISH
	{ GADG_STAT_TEXT,    55, 15, 0, 0, 0, 0, 0, 0, 0, 0,
		"Spara nuvarande\ninst�llningar?" },
#elif SPANISH
	{ GADG_STAT_TEXT,    55, 15, 0, 0, 0, 0, 0, 0, 0, 0,
		"�Guarda los ajustes actuales como\nlos valores por omisi�n?" },
#endif

	{ GADG_STAT_STDIMAGE, 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, (Ptr) IMAGE_ICON_CAUTION },

	{ GADG_ITEM_NONE }
};

static DialogTemplate savePrefsDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 300, 90, savePrefsGadgets, NULL
};

/*
 *	Rotate Size
 */

static GadgetTemplate rotateGadgets[] = {
	{ GADG_PUSH_BUTTON,  20, 40, 0, 0, 60, 20, 0, 0, 'R', 0, "Rotate" },
	{ GADG_PUSH_BUTTON, 100, 40, 0, 0, 60, 20, 0, 0, CHR_CANCEL, 0, strCancel },

	{ GADG_EDIT_TEXT, 80, 10, 0, 0, 5*8, 0, 0, 0, 0, 0, NULL },

	{ GADG_STAT_TEXT,  20, 10, 0, 0, 0, 0, 0, 0, 0, 0, "Angle:" },
	{ GADG_STAT_TEXT, 130, 10, 0, 0, 0, 0, 0, 0, 0, 0, "�" },

	{ GADG_ITEM_NONE }
};

static DialogTemplate rotateDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 180, 70, rotateGadgets, NULL
};

/*
 *	Scale
 */

static GadgetTemplate scaleGadgets[] = {
	{ GADG_PUSH_BUTTON, -80, 10, 0, 0, 60, 20, 0, 0, 'S', 0, "Scale" },
	{ GADG_PUSH_BUTTON, -80, 40, 0, 0, 60, 20, 0, 0, CHR_CANCEL, 0, strCancel },

	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 80, 30, 0, 0, 40, 11, 0, 0, 0, 0, NULL },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 80, 50, 0, 0, 40, 11, 0, 0, 0, 0, NULL },

	{ GADG_STAT_TEXT,  20, 10, 0, 0, 0, 0, 0, 0, 0, 0, "Scale Objects" },
	{ GADG_STAT_TEXT,  20, 30, 0, 0, 0, 0, 0, 0, 0, 0, "Width" },
	{ GADG_STAT_TEXT,  20, 50, 0, 0, 0, 0, 0, 0, 0, 0, "Height" },
	{ GADG_STAT_TEXT, 130, 30, 0, 0, 0, 0, 0, 0, 0, 0, "%" },
	{ GADG_STAT_TEXT, 130, 50, 0, 0, 0, 0, 0, 0, 0, 0, "%" },
	{ GADG_ITEM_NONE }
};

static DialogTemplate scaleDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 240, 70, scaleGadgets, NULL
};

/*
 *	Preferences
 */

static TextPtr 	prefsList[] = {
	"General",	"Display",	"Save",	"Polygons",	"Clipboard",	"HotLinks",	NULL
};
static TextPtr	colorList[] = {
	"32", "64 (Half-brite)", "256", NULL
};

GadgetTemplate optionsGeneralGadgets[] = {
	{ GADG_PUSH_BUTTON, -80, 10, 0, 0, 60, 20, 0, 0,     CHR_OK, 0, strOK },
	{ GADG_PUSH_BUTTON, -80, 40, 0, 0, 60, 20, 0, 0, CHR_CANCEL, 0, strCancel },
	{ GADG_PUSH_BUTTON, -80, 70, 0, 0, 60, 20, 0, 0, 'R', 0, "Reset" },

	{ GADG_POPUP,     130, 10, 0, 0, 130, 11, 0, 0, 0, 0, &prefsList },

	{ GADG_RADIO_BUTTON,  30,  45, 0, 0, 0, 0, 0, 0, 'I', 0, "Inches" },
	{ GADG_RADIO_BUTTON,  30,  60, 0, 0, 0, 0, 0, 0, 'e', 0, "Centimeters" },

	{ GADG_RADIO_BUTTON, 220,  45, 0, 0, 0, 0, 0, 0, 'F', 0, "Flash Screen" },
	{ GADG_RADIO_BUTTON, 220,  60, 0, 0, 0, 0, 0, 0, 'S', 0, "Sound Beep" },
	{ GADG_RADIO_BUTTON, 220,  75, 0, 0, 0, 0, 0, 0, 'B', 0, "Both" },

	{ GADG_STAT_STDBORDER, 20, 25, 0, 0, 460 - 120, 0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },

	{ GADG_STAT_TEXT,  20, 10, 0, 0, 0, 0, 0, 0, 0, 0, "Preferences: " },
	{ GADG_STAT_TEXT,  20, 30, 0, 0, 0, 0, 0, 0, 0, 0, "Unit of measurement:" },
	{ GADG_STAT_TEXT, 210, 30, 0, 0, 0, 0, 0, 0, 0, 0, "Error notification:" },
	{ GADG_ITEM_NONE }
};

GadgetTemplate optionsDisplayGadgets[] = {
	{ GADG_PUSH_BUTTON, -80, 10, 0, 0, 60, 20, 0, 0,     CHR_OK, 0, strOK },
	{ GADG_PUSH_BUTTON, -80, 40, 0, 0, 60, 20, 0, 0, CHR_CANCEL, 0, strCancel },
	{ GADG_PUSH_BUTTON, -80, 70, 0, 0, 60, 20, 0, 0, 'R', 0, "Reset" },

	{ GADG_POPUP,     130, 10, 0, 0, 130, 11, 0, 0, 0, 0, &prefsList },

	{ GADG_CHECK_BOX,     30, 35, 0, 0, 0, 0, 0, 0, 'D', 0, "Drag As Outlines" },
	{ GADG_CHECK_BOX,     30, 50, 0, 0, 0, 0, 0, 0, 'P', 0, "Show Pictures" },
#if AMERICAN
	{ GADG_CHECK_BOX, 30, 65, 0, 0, 0, 0, 0, 0, 'n', 0, "Screen Color Correction" },
#elif BRITISH
	{ GADG_CHECK_BOX, 30, 65, 0, 0, 0, 0, 0, 0, 'n', 0, "Screen Colour Correction" },
#endif
	{ GADG_CHECK_BOX, 30, 80, 0, 0, 0, 0, 0, 0, 'h', 0, "Cache Pictures" },

	{ GADG_STAT_STDBORDER, 20, 25, 0, 0, 460 - 120, 0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },

	{ GADG_STAT_TEXT,  20, 10, 0, 0, 0, 0, 0, 0, 0, 0, "Preferences: " },
	{ GADG_ITEM_NONE }
};

GadgetTemplate optionsSaveGadgets[] = {
	{ GADG_PUSH_BUTTON, -80, 10, 0, 0, 60, 20, 0, 0,     CHR_OK, 0, strOK },
	{ GADG_PUSH_BUTTON, -80, 40, 0, 0, 60, 20, 0, 0, CHR_CANCEL, 0, strCancel },
	{ GADG_PUSH_BUTTON, -80, 70, 0, 0, 60, 20, 0, 0, 'R', 0, "Reset" },

	{ GADG_POPUP,     130, 10, 0, 0, 130, 11, 0, 0, 0, 0, &prefsList },

	{ GADG_CHECK_BOX,     30, 35, 0, 0, 0, 0, 0, 0, 'S', 0, "Save Icons" },
	{ GADG_CHECK_BOX,     30, 50, 0, 0, 0, 0, 0, 0, 'M', 0, "Make Backups" },
	{ GADG_CHECK_BOX,     30, 65, 0, 0, 0, 0, 0, 0, 'u', 0, "Auto-save every:" },
	{ GADG_EDIT_TEXT | GADG_EDIT_NUMONLY, 185, 65, 0, 0, 24, 11, 0, 0, 0, 0, NULL },

	{ GADG_STAT_STDBORDER, 20, 25, 0, 0, 460 - 120, 0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },

	{ GADG_STAT_TEXT,  20, 10, 0, 0, 0, 0, 0, 0, 0, 0, "Preferences: " },
	{ GADG_STAT_TEXT, 220, 65, 0, 0, 0, 0, 0, 0, 0, 0, "minutes" },
	{ GADG_ITEM_NONE }
};

GadgetTemplate optionsPolyGadgets[] = {
	{ GADG_PUSH_BUTTON, -80, 10, 0, 0, 60, 20, 0, 0,     CHR_OK, 0, strOK },
	{ GADG_PUSH_BUTTON, -80, 40, 0, 0, 60, 20, 0, 0, CHR_CANCEL, 0, strCancel },
	{ GADG_PUSH_BUTTON, -80, 70, 0, 0, 60, 20, 0, 0, 'R', 0, "Reset" },

	{ GADG_POPUP,     130, 10, 0, 0, 130, 11, 0, 0, 0, 0, &prefsList },

	{ GADG_CHECK_BOX,     30, 35, 0, 0, 0, 0, 0, 0, 'A', 0, "Auto-Close Polygons" },

	{ GADG_STAT_STDBORDER, 20, 25, 0, 0, 460 - 120, 0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },

	{ GADG_STAT_TEXT,  20, 10, 0, 0, 0, 0, 0, 0, 0, 0, "Preferences: " },
	{ GADG_ITEM_NONE }
};

GadgetTemplate optionsClipboardGadgets[] = {
	{ GADG_PUSH_BUTTON, -80, 10, 0, 0, 60, 20, 0, 0,     CHR_OK, 0, strOK },
	{ GADG_PUSH_BUTTON, -80, 40, 0, 0, 60, 20, 0, 0, CHR_CANCEL, 0, strCancel },
	{ GADG_PUSH_BUTTON, -80, 70, 0, 0, 60, 20, 0, 0, 'R', 0, "Reset" },

	{ GADG_POPUP,     130, 10, 0, 0, 130, 11, 0, 0, 0, 0, &prefsList },

	{ GADG_CHECK_BOX,  30,  35, 0, 0, 0, 0, 0, 0, 'h', 0, "Full Clipboard Support" },

	{ GADG_POPUP,     100, 55, 0, 0, 130, 11, 0, 0, 0, 0, &colorList },

	{ GADG_STAT_STDBORDER, 20, 25, 0, 0, 460 - 120, 0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },

	{ GADG_STAT_TEXT,  20, 10, 0, 0, 0, 0, 0, 0, 0, 0, "Preferences: " },
	{ GADG_STAT_TEXT,  30, 55, 0, 0, 0, 0, 0, 0, 0, 0, "Colors: " },	// clipboard
	{ GADG_ITEM_NONE }
};

GadgetTemplate optionsHotLinksGadgets[] = {
	{ GADG_PUSH_BUTTON, -80, 10, 0, 0, 60, 20, 0, 0,     CHR_OK, 0, strOK },
	{ GADG_PUSH_BUTTON, -80, 40, 0, 0, 60, 20, 0, 0, CHR_CANCEL, 0, strCancel },
	{ GADG_PUSH_BUTTON, -80, 70, 0, 0, 60, 20, 0, 0, 'R', 0, "Reset" },

	{ GADG_POPUP,     130, 10, 0, 0, 130, 11, 0, 0, 0, 0, &prefsList },

	{ GADG_POPUP,     100, 35, 0, 0, 130, 11, 0, 0, 0, 0, &colorList },

	{ GADG_STAT_STDBORDER, 20, 25, 0, 0, 460 - 120, 0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },

	{ GADG_STAT_TEXT,  20, 10, 0, 0, 0, 0, 0, 0, 0, 0, "Preferences: " },
	{ GADG_STAT_TEXT,  30, 35, 0, 0, 0, 0, 0, 0, 0, 0, "Colors: " },	// hotlinks

	{ GADG_ITEM_NONE }
};

static DialogTemplate optionsDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 460, 120, optionsGeneralGadgets, NULL
};

/*
 *	Align
 */

static GadgetTemplate alignGadgets[] = {
	{ GADG_PUSH_BUTTON, -80, 10, 0, 0, 60, 20, 0, 0, 'A', 0, "Align" },
	{ GADG_PUSH_BUTTON, -80, 40, 0, 0, 60, 20, 0, 0, CHR_CANCEL, 0, strCancel },

	{ GADG_RADIO_BUTTON,  30, 30, 0, 0, 0, 0, 0, 0, 'L', 0, "Left edges" },
#if AMERICAN
	{ GADG_RADIO_BUTTON,  30, 50, 0, 0, 0, 0, 0, 0, 'f', 0, "Left/right centers" },
#elif BRITISH
	{ GADG_RADIO_BUTTON,  30, 50, 0, 0, 0, 0, 0, 0, 'f', 0, "Left/right centres" },
#endif
	{ GADG_RADIO_BUTTON,  30, 70, 0, 0, 0, 0, 0, 0, 'R', 0, "Right edges" },
	{ GADG_RADIO_BUTTON,  30, 90, 0, 0, 0, 0, 0, 0, 'N', 0, "None" },

	{ GADG_RADIO_BUTTON, 200, 30, 0, 0, 0, 0, 0, 0, 'T', 0, "Top edges" },
#if AMERICAN
	{ GADG_RADIO_BUTTON, 200, 50, 0, 0, 0, 0, 0, 0, 'p', 0, "Top/bottom centers" },
#elif BRITISH
	{ GADG_RADIO_BUTTON, 200, 50, 0, 0, 0, 0, 0, 0, 'p', 0, "Top/bottom centres" },
#endif
	{ GADG_RADIO_BUTTON, 200, 70, 0, 0, 0, 0, 0, 0, 'B', 0, "Bottom edges" },
	{ GADG_RADIO_BUTTON, 200, 90, 0, 0, 0, 0, 0, 0, 'o', 0, "None" },

	{ GADG_STAT_TEXT, 20, 10, 0, 0, 0, 0, 0, 0, 0, 0, "Align objects:" },

	{ GADG_ITEM_NONE }
};

static DialogTemplate alignDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 450, 110, alignGadgets, NULL
};

/*
 *	Drawing size
 */

static GadgetTemplate drawSizeGadgets[] = {
	{ GADG_PUSH_BUTTON,  20, -30, 0, 0, 60, 20, 0, 0,     CHR_OK, 0, strOK },
	{ GADG_PUSH_BUTTON, 100, -30, 0, 0, 60, 20, 0, 0, CHR_CANCEL, 0, strCancel },

	{ GADG_ACTIVE_BORDER, 180, 10, 0, 0, 200, 180, 0, 0, 0, 0, NULL },

	{ GADG_STAT_TEXT, 100, 40, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT, 100, 60, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT, 100, 90, 0, 0, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_STAT_STDBORDER, 20, 25, 0, 0, 140, 0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	{ GADG_STAT_TEXT,  20, 10, 0, 0, 0, 0, 0, 0, 0, 0, "Drawing Size" },
	{ GADG_STAT_TEXT,  30, 40, 0, 0, 0, 0, 0, 0, 0, 0, "Width:" },
	{ GADG_STAT_TEXT,  30, 60, 0, 0, 0, 0, 0, 0, 0, 0, "Height:" },
	{ GADG_STAT_TEXT,  30, 90, 0, 0, 0, 0, 0, 0, 0, 0, "Pages:" },

	{ GADG_ITEM_NONE }
};

static DialogTemplate drawSizeDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 400, 200, drawSizeGadgets, NULL
};

/*
 *	Pen Size
 */

static GadgetTemplate penSizeGadgets[] = {
	{ GADG_PUSH_BUTTON,  20, 40, 0, 0, 60, 20, 0, 0,     CHR_OK, 0, strOK },
	{ GADG_PUSH_BUTTON, 100, 40, 0, 0, 60, 20, 0, 0, CHR_CANCEL, 0, strCancel },

	{ GADG_EDIT_TEXT, 120, 10, 0, 0, 40, 11, 0, 0, 0, 0, NULL },

	{ GADG_STAT_TEXT, 20, 10, 0, 0, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_ITEM_NONE }
};

static DialogTemplate penSizeDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 180, 70, penSizeGadgets, NULL
};

/*
 *	Error
 */

static GadgetTemplate errorGadgets[] = {
	{ GADG_PUSH_BUTTON, 220, 50, 0, 0, 60, 20, 0, 0, CHR_OK, 0, strOK },

	{ GADG_STAT_TEXT, 55, 15, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT, 55, 30, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_STDIMAGE, 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, (Ptr) IMAGE_ICON_STOP },

	{ GADG_ITEM_NONE }
};

static DialogTemplate errorDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 300, 80, errorGadgets, NULL
};

/*
 *	About (and memory) dialog
 */

static GadgetTemplate aboutGadgets[] = {
	{ GADG_PUSH_BUTTON, -80, 10, 0, 0, 60, 20, 0, 0, CHR_OK, 0, strOK },

	{ GADG_STAT_TEXT, 150, 155, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT, 150, 170, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT, 150, 190, 0, 0, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_STAT_TEXT,  20,  16, 0, 0, 0, 0, 0, 0, 0, 0, "DesignWorks" },
	{ GADG_STAT_TEXT, 108,  10, 0, 0, 0, 0, 0, 0, 0, 0, "tm" },
	{ GADG_STAT_TEXT,  20,  40, 0, 0, 0, 0, 0, 0, 0, 0,
		"Designed and developed\n  by James Bayless\n  and Beth Henry" },
	{ GADG_STAT_TEXT,  20,  90, 0, 0, 0, 0, 0, 0, 0, 0,
		"Copyright \251 1991-93\nNew Horizons Software, Inc.\nAll Rights Reserved" },

	{ GADG_STAT_TEXT,  20, 140, 0, 0, 0, 0, 0, 0, 0, 0, strAvailMem },
	{ GADG_STAT_TEXT,  40, 155, 0, 0, 0, 0, 0, 0, 0, 0, graphicsText },
	{ GADG_STAT_TEXT,  40, 170, 0, 0, 0, 0, 0, 0, 0, 0, strExpansion },
	{ GADG_STAT_TEXT,  20, 190, 0, 0, 0, 0, 0, 0, 0, 0, "AREXX port:" },

	{ GADG_ITEM_NONE }
};

static DialogTemplate aboutDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 260, 210, aboutGadgets, NULL
};

static GadgetTemplate memoryGadgets[] = {
	{ GADG_PUSH_BUTTON, -80, 10, 0, 0, 60, 20, 0, 0, 0, 0, strOK },

	{ GADG_STAT_TEXT, 150, 25, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT, 150, 40, 0, 0, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_STAT_TEXT,  20, 10, 0, 0, 0, 0, 0, 0, 0, 0, strAvailMem },
	{ GADG_STAT_TEXT,  40, 25, 0, 0, 0, 0, 0, 0, 0, 0, graphicsText },
	{ GADG_STAT_TEXT,  40, 40, 0, 0, 0, 0, 0, 0, 0, 0, strExpansion },

	{ GADG_ITEM_NONE }
};

static DialogTemplate memoryDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 280, 65, memoryGadgets, NULL
};

/*
 *	Pen Colors
 */

#define PEN_WIDTH	8*25
#define PEN_NUM		10

static GadgetTemplate penColorsGadgets[] = {
	{ GADG_PUSH_BUTTON, -80, 10, 0, 0,  60, 20, 0, 0, 'D', 0, strDone },
	{ GADG_PUSH_BUTTON, -80, 40, 0, 0,  60, 20, 0, 0, 'N', 0, "New" },
	{ GADG_PUSH_BUTTON, -80, 70, 0, 0,  60, 20, 0, 0, 'E', 0, "Edit" },
	{ GADG_PUSH_BUTTON, -80,100, 0, 0,  60, 20, 0, 0, 'R', 0, "Remove" },

	SL_GADG_BOX(20, 40, PEN_WIDTH, PEN_NUM),
	SL_GADG_UPARROW(20, 40, PEN_WIDTH, PEN_NUM),
	SL_GADG_DOWNARROW(20, 40, PEN_WIDTH, PEN_NUM),
	SL_GADG_SLIDER(20, 40, PEN_WIDTH, PEN_NUM),

	{ GADG_STAT_STDBORDER, 20, 25, 0, 0, PEN_WIDTH, 0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
#if AMERICAN
	{ GADG_STAT_TEXT,  20, 10, 0, 0, 0, 0, 0, 0, 0, 0, "Pen Colors" },
#elif BRITISH
	{ GADG_STAT_TEXT,  20, 10, 0, 0, 0, 0, 0, 0, 0, 0, "Pen Colours" },
#endif

	{ GADG_ITEM_NONE }
};

static DialogTemplate penColorsDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, PEN_WIDTH + 120, 170, penColorsGadgets, NULL
};

/*
 *	Edit Pen Color
 */

static GadgetTemplate editPenColorGadgets[] = {
	{ GADG_PUSH_BUTTON, -80, 10, 0, 0,  60, 20, 0, 0,  	  CHR_OK, 0, strOK },
	{ GADG_PUSH_BUTTON, -80, 40, 0, 0,  60, 20, 0, 0, CHR_CANCEL, 0, strCancel },
	{ GADG_PUSH_BUTTON, -80, 70, 0, 0,  60, 20, 0, 0, 'h', 0, "Change" },

	{ GADG_EDIT_TEXT, 80, 40, 0, 0, 120, 11, 0, 0, 0, 0, NULL },

	{ GADG_ACTIVE_BORDER, 80, 60, 0, 0, 40, 20, 0, 0, 0, 0, NULL },	// color display

	{ GADG_STAT_STDBORDER, 20, 25, 0, 0, 300 - 120, 0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },

	{ GADG_STAT_TEXT, 20, 10, 0, 0, 0, 0, 0, 0, 0, 0, "Pen Color " },
	{ GADG_STAT_TEXT, 20, 40, 0, 0, 0, 0, 0, 0, 0, 0, "Name: " },
	{ GADG_STAT_TEXT, 20, 65, 0, 0, 0, 0, 0, 0, 0, 0, "Color: " },

	{ GADG_ITEM_NONE }
};

static DialogTemplate editPenColorDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 300, 100, editPenColorGadgets, NULL
};

/*
 *	Fill Patterns (superset of pen colors dialog)
 */

#define FILL_WIDTH	PEN_WIDTH
#define FILL_NUM	PEN_NUM

static GadgetTemplate fillPatternsGadgets[] = {
	{ GADG_PUSH_BUTTON, -80, 10, 0, 0,  60, 20, 0, 0, 'D', 0, strDone },
	{ GADG_PUSH_BUTTON, -80, 40, 0, 0,  60, 20, 0, 0, 'N', 0, "New" },
	{ GADG_PUSH_BUTTON, -80, 70, 0, 0,  60, 20, 0, 0, 'E', 0, "Edit" },
	{ GADG_PUSH_BUTTON, -80,100, 0, 0,  60, 20, 0, 0, 'R', 0, "Remove" },

	SL_GADG_BOX(20, 40, FILL_WIDTH, FILL_NUM),
	SL_GADG_UPARROW(20, 40, PEN_WIDTH, FILL_NUM),
	SL_GADG_DOWNARROW(20, 40, FILL_WIDTH, FILL_NUM),
	SL_GADG_SLIDER(20, 40, FILL_WIDTH, FILL_NUM),

	{ GADG_STAT_STDBORDER, 20, 25, 0, 0, FILL_WIDTH, 0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },

	{ GADG_STAT_TEXT,  20, 10, 0, 0, 0, 0, 0, 0, 0, 0, "Fill Patterns" },

	{ GADG_ITEM_NONE }
};

static DialogTemplate fillPatternsDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, FILL_WIDTH + 120, 170, fillPatternsGadgets, NULL
};

/*
 *	Edit Pen Color
 */

static GadgetTemplate editFillPatGadgets[] = {
	{ GADG_PUSH_BUTTON, -80, 10, 0, 0,  60, 20, 0, 0,     CHR_OK, 0, strOK },
	{ GADG_PUSH_BUTTON, -80, 40, 0, 0,  60, 20, 0, 0, CHR_CANCEL, 0, strCancel },

	{ GADG_EDIT_TEXT, 80, 40, 0, 0, 120, 11, 0, 0, 0, 0, NULL },

	{ GADG_ACTIVE_BORDER,  40, 75, 0, 0, 64, 64, 0, 0, 0, 0, NULL },	// edit box
	{ GADG_ACTIVE_BORDER, 200-64, 75, 0, 0, 64, 64, 0, 0, 0, 0, NULL },	// sample box

	{ GADG_PUSH_BUTTON, 220,  75, 0, 0,  60, 20, 0, 0, 'l', 0, "Clear" },
	{ GADG_PUSH_BUTTON, 220, 105, 0, 0,  60, 20, 0, 0, 'R', 0, "Revert" },

	{ GADG_POPUP,	120, -30, 0, 0, 120, 11, 0, 0, 0, 0, NULL },	// pen color names

	{ GADG_STAT_STDBORDER, 20, 25, 0, 0, 320 - 120, 0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },
	{ GADG_STAT_STDBORDER, 20, 65, 0, 0, 320 -  40, 0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },

	{ GADG_STAT_TEXT, 20, 10, 0, 0, 0, 0, 0, 0, 0, 0, "Fill Pattern" },
	{ GADG_STAT_TEXT, 20, 40, 0, 0, 0, 0, 0, 0, 0, 0, "Name: " },
	{ GADG_STAT_TEXT, 20, -30, 0, 0, 0, 0, 0, 0, 0, 0, "Pen Color: "},

	{ GADG_ITEM_NONE }
};

static DialogTemplate editFillPatDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 320, 190, editFillPatGadgets, NULL
};

/*
 *	Layers
 */

#define LAYERS_WIDTH	(19*8+20)
#define LAYERS_NUM		7

static GadgetTemplate layersGadgets[] = {
	{ GADG_PUSH_BUTTON, 30 + LAYERS_WIDTH,  60, 0, 0,  60, 20, 0, 0, 'C', 0, "Create" },

	GT_ADJUST_UPARROW(30 + LAYERS_WIDTH, 35),
	GT_ADJUST_DOWNARROW(30 + LAYERS_WIDTH, 35),

	{ GADG_PUSH_BUTTON,  30 + LAYERS_WIDTH,  90, 0, 0,  60, 20, 0, 0, 'R', 0, "Rename" },
	{ GADG_PUSH_BUTTON,  30 + LAYERS_WIDTH, 120, 0, 0,  60, 20, 0, 0, 'e', 0, "Delete" },
	{ GADG_PUSH_BUTTON, 100 + LAYERS_WIDTH,  60, 0, 0,  60, 20, 0, 0, 'S', 0, "Show" },
	{ GADG_PUSH_BUTTON, 100 + LAYERS_WIDTH,  90, 0, 0,  60, 20, 0, 0, 'H', 0, "Hide" },
	{ GADG_PUSH_BUTTON, 100 + LAYERS_WIDTH, 120, 0, 0,  60, 20, 0, 0, 'D', 0, strDone },

	SL_GADG_BOX(20, 30, LAYERS_WIDTH, LAYERS_NUM),
	SL_GADG_UPARROW(20, 30, LAYERS_WIDTH, LAYERS_NUM),
	SL_GADG_DOWNARROW(20, 30, LAYERS_WIDTH, LAYERS_NUM),
	SL_GADG_SLIDER(20, 30, LAYERS_WIDTH, LAYERS_NUM),

	{ GADG_EDIT_TEXT,
		20, 40 + LAYERS_NUM*11, 1, LAYERS_NUM,
		LAYERS_WIDTH, 11, -2, 0,
		0, 0, NULL },

	{ GADG_STAT_TEXT, (30+LAYERS_WIDTH)+10+ARROW_WIDTH, 35, 0, 0, 0, 0, 0, 0, 0, 0, "Move" },
	{ GADG_STAT_TEXT,  20, 10, 0, 0, 0, 0, 0, 0, 0, 0, "Layers" },

	{ GADG_ITEM_NONE }
};

static DialogTemplate layersDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 180 + LAYERS_WIDTH, 150, layersGadgets, NULL
};

/*
 *	Delete Layer
 */

static GadgetTemplate deleteLayerGadgets[] = {
	{ GADG_PUSH_BUTTON,   50, 50, 0, 0, 60, 20, 0, 0, CHR_YES, 0, strYes },
	{ GADG_PUSH_BUTTON, -110, 50, 0, 0, 60, 20, 0, 0,  CHR_NO, 0, strNo },

	{ GADG_STAT_TEXT, 55, 15, 0, 0, 0, 0, 0, 0, 0, 0,
		"Permanently delete layer\nand everything on it?" },
	{ GADG_STAT_STDIMAGE, 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, (Ptr) IMAGE_ICON_CAUTION },

	{ GADG_ITEM_NONE }
};

static DialogTemplate deleteLayerDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 260, 80, deleteLayerGadgets, NULL
};

/*
 *	Grid Size
 */

static GadgetTemplate gridGadgets[] = {
	{ GADG_PUSH_BUTTON,  30, -30, 0, 0, 60, 20, 0, 0,     CHR_OK, 0, strOK },
	{ GADG_PUSH_BUTTON, -90, -30, 0, 0, 60, 20, 0, 0, CHR_CANCEL, 0, strCancel },

	GT_ADJUST_UPARROW(110, 15),
	GT_ADJUST_DOWNARROW(110, 15),
	GT_ADJUST_TEXT(110, 15),

	{ GADG_STAT_TEXT, 20, 15, 0, 0, 0, 0, 0, 0, 0, 0, "Grid size:" },
	{ GADG_ITEM_NONE }
};

static DialogTemplate gridDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 220, 70, gridGadgets, NULL
};

/*
 *	Customize macro menu
 */

static GadgetTemplate customMacroGadgets[] = {
	{ GADG_PUSH_BUTTON, 105, -30, 0, 0,  60, 20, 0, 0, 'U', 0, strUse },
	{ GADG_PUSH_BUTTON, 185, -30, 0, 0,  60, 20, 0, 0, CHR_CANCEL, 0, strCancel },
	{ GADG_PUSH_BUTTON,  25, -30, 0, 0,  60, 20, 0, 0, 'S', 0, "Save" },

	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 50,  30, 0, 0, 200, 11, 0, 0, 0, 0, strMacroNames2[0] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 50,  50, 0, 0, 200, 11, 0, 0, 0, 0, strMacroNames2[1] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 50,  70, 0, 0, 200, 11, 0, 0, 0, 0, strMacroNames2[2] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 50,  90, 0, 0, 200, 11, 0, 0, 0, 0, strMacroNames2[3] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 50, 110, 0, 0, 200, 11, 0, 0, 0, 0, strMacroNames2[4] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 50, 130, 0, 0, 200, 11, 0, 0, 0, 0, strMacroNames2[5] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 50, 150, 0, 0, 200, 11, 0, 0, 0, 0, strMacroNames2[6] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 50, 170, 0, 0, 200, 11, 0, 0, 0, 0, strMacroNames2[7] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 50, 190, 0, 0, 200, 11, 0, 0, 0, 0, strMacroNames2[8] },
	{ GADG_EDIT_TEXT | GADG_EDIT_RETURNCYCLE, 50, 210, 0, 0, 200, 11, 0, 0, 0, 0, strMacroNames2[9] },

	{ GADG_STAT_TEXT, 20,  30, 0, 0, 0, 0, 0, 0, 0, 0, "F1" },
	{ GADG_STAT_TEXT, 20,  50, 0, 0, 0, 0, 0, 0, 0, 0, "F2" },
	{ GADG_STAT_TEXT, 20,  70, 0, 0, 0, 0, 0, 0, 0, 0, "F3" },
	{ GADG_STAT_TEXT, 20,  90, 0, 0, 0, 0, 0, 0, 0, 0, "F4" },
	{ GADG_STAT_TEXT, 20, 110, 0, 0, 0, 0, 0, 0, 0, 0, "F5" },
	{ GADG_STAT_TEXT, 20, 130, 0, 0, 0, 0, 0, 0, 0, 0, "F6" },
	{ GADG_STAT_TEXT, 20, 150, 0, 0, 0, 0, 0, 0, 0, 0, "F7" },
	{ GADG_STAT_TEXT, 20, 170, 0, 0, 0, 0, 0, 0, 0, 0, "F8" },
	{ GADG_STAT_TEXT, 20, 190, 0, 0, 0, 0, 0, 0, 0, 0, "F9" },
	{ GADG_STAT_TEXT, 20, 210, 0, 0, 0, 0, 0, 0, 0, 0, "F10" },
	{ GADG_STAT_TEXT, 20,  10, 0, 0, 0, 0, 0, 0, 0, 0, "Macro names:" },

	{ GADG_ITEM_NONE }
};

static DialogTemplate customMacroDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 270, 260, customMacroGadgets, NULL
};

/*
 *	Lose links
 */

static GadgetTemplate loseLinkGadgets[] = {
	{ GADG_PUSH_BUTTON, 180, -30, 0, 0, 60, 20, 0, 0, CHR_NO, 0, strNo },
	{ GADG_PUSH_BUTTON,  60, -30, 0, 0, 60, 20, 0, 0, CHR_OK, 0, strOK },
#if (AMERICAN | BRITISH)
	{ GADG_STAT_TEXT,  20, 10, 0, 0, 0, 0, 0, 0, 0, 0,
		"Saving now will lose the link\nto the edition.\nDo you wish to continue?"},
#elif GERMAN
	{ GADG_STAT_TEXT,  20, 10, 0, 0, 0, 0, 0, 0, 0, 0,
		"Saving now will lose the link\nto the edition.\nDo you wish to continue?"},
#elif FRENCH
	{ GADG_STAT_TEXT,  20, 10, 0, 0, 0, 0, 0, 0, 0, 0,
		"La sauvegarde vous fera perdre\nl'abonnement � l'�dition.\nVoulez-vous continuer?"},
#elif SWEDISH
	{ GADG_STAT_TEXT,  20, 10, 0, 0, 0, 0, 0, 0, 0, 0,
		"Att spara nu bryter l�nken\ntill utg�van.\nVill du verkligen forts�ttta?"},
#elif SPANISH
	{ GADG_STAT_TEXT,  20, 10, 0, 0, 0, 0, 0, 0, 0, 0,
		"Guardar ahora romper� la conecci�n\ncon la edici�n.\n�Desea continuar?"},
#endif
	{ GADG_ITEM_NONE }
};

static DialogTemplate loseLinkDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 300, 85, loseLinkGadgets, NULL
};


static GadgetTemplate breakGadgets[] = {
	{ GADG_PUSH_BUTTON, 220, 50, 0, 0, 60, 20, 0, 0, CHR_OK, 0, strOK },

	{ GADG_STAT_TEXT, 55, 15, 0, 0, 0, 0, 0, 0, 0, 0,
		"An update message has been received.\nBreaking link to the edition."},
	{ GADG_STAT_STDIMAGE, 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, (Ptr) IMAGE_ICON_STOP },

	{ GADG_ITEM_NONE }
};

static DialogTemplate breakLinkDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 350, 80, breakGadgets, NULL
};

/*
 *	User requests
 */

static GadgetTemplate userReq1Gadgets[] = {
	{ GADG_PUSH_BUTTON, -80, -30, 0, 0, 60, 20, 0, 0, CHR_OK, 0, strOK },

	{ GADG_STAT_TEXT,  20, 10, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT,  20, 10, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT,  20, 10, 0, 0, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_ITEM_NONE }
};

static DialogTemplate userReq1Dialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 300, 70, userReq1Gadgets, NULL
};

static GadgetTemplate userReq2Gadgets[] = {
	{ GADG_PUSH_BUTTON,  60, -30, 0, 0, 60, 20, 0, 0,     CHR_OK, 0, strOK },
	{ GADG_PUSH_BUTTON, 180, -30, 0, 0, 60, 20, 0, 0, CHR_CANCEL, 0, strCancel },

	{ GADG_STAT_TEXT,  20, 10, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT,  20, 10, 0, 0, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_ITEM_NONE }
};

static DialogTemplate userReq2Dialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 300, 70, userReq2Gadgets, NULL
};

static GadgetTemplate userReq3Gadgets[] = {
	{ GADG_PUSH_BUTTON,  30, -30, 0, 0, 60, 20, 0, 0,    CHR_YES, 0, strYes },
	{ GADG_PUSH_BUTTON, 210, -30, 0, 0, 60, 20, 0, 0, CHR_CANCEL, 0, strCancel },
	{ GADG_PUSH_BUTTON, 120, -30, 0, 0, 60, 20, 0, 0,     CHR_NO, 0, strNo },

	{ GADG_STAT_TEXT,  20, 10, 0, 0, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_ITEM_NONE }
};

static DialogTemplate userReq3Dialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 300, 70, userReq3Gadgets, NULL
};

static GadgetTemplate userReqTextGadgets[] = {
	{ GADG_PUSH_BUTTON,  55, -30, 0, 0,  70, 20, 0, 0,     CHR_OK, 0, strOK },
	{ GADG_PUSH_BUTTON, 175, -30, 0, 0,  70, 20, 0, 0, CHR_CANCEL, 0, strCancel },

	{ GADG_EDIT_TEXT, 22, 30, 0, 0, 256, 11, 0, 0, 0, 0, NULL },

	{ GADG_STAT_TEXT, 20, 10, 0, 0, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_ITEM_NONE }
};

static DialogTemplate userReqTextDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 300, 90, userReqTextGadgets, NULL
};

/*
 *	Info
 */

static GadgetTemplate infoGadgets[] = {
	{ GADG_STAT_TEXT,     55, 20, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_STDIMAGE, 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, (Ptr) IMAGE_ICON_NOTE },
	{ GADG_ITEM_NONE }
};

static DialogTemplate infoDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 300, 55, infoGadgets, NULL
};

/*
 *	Export Picture
 */

#define EXPORT_WIDTH	(24*8)
#define EXPORT_NUM	7

static GadgetTemplate exportGadgets[] = {
	{ GADG_PUSH_BUTTON, -80, -60, 0, 0, 60, 20, 0, 0, 'x', 0, "Export" },
	{ GADG_PUSH_BUTTON, -80, -30, 0, 0, 60, 20, 0, 0, CHR_CANCEL, 0, strCancel },

	{ GADG_PUSH_BUTTON, -80,  30, 0, 0, 60, 20, 0, 0, 'E', 0, "Enter" },
	{ GADG_PUSH_BUTTON, -80,  60, 0, 0, 60, 20, 0, 0, 'B', 0, "Back" },
	{ GADG_PUSH_BUTTON, -80,  90, 0, 0, 60, 20, 0, 0, 'D', 0, "Disks" },

	SL_GADG_BOX(20, 30, EXPORT_WIDTH, EXPORT_NUM),
	SL_GADG_UPARROW(20, 30, EXPORT_WIDTH, EXPORT_NUM),
	SL_GADG_DOWNARROW(20, 30, EXPORT_WIDTH, EXPORT_NUM),
	SL_GADG_SLIDER(20, 30, EXPORT_WIDTH, EXPORT_NUM),

	{ GADG_EDIT_TEXT,  20, -40, 0, 0, EXPORT_WIDTH, 11, 0, 0, 0, 0, NULL },

	{ GADG_STAT_TEXT,  65, 10, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_IMAGE, 20, 15, 0, 0, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_STAT_TEXT,  20, -60, 0, 2, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_POPUP,      85, -20, 0, 0, EXPORT_WIDTH - 65, 11, 0, 0, 0, 0, &colorList },

	{ GADG_STAT_TEXT,  20, -20, 0, 0,   0,  0, 0, 0, 0, 0, "Colors:" },

	{ GADG_ITEM_NONE }
};

static DialogTemplate exportDialog = {
	DLG_TYPE_WINDOW, 0, -1, -1, 120 + EXPORT_WIDTH, 180, exportGadgets, NULL
};

/*
 *	Export EPSF
 */

#define EXPORT_WIDTH	(24*8)
#define EXPORT_NUM		7

static GadgetTemplate exportEPSFGadgets[] = {
#if (AMERICAN | BRITISH)
	{ GADG_PUSH_BUTTON, -80, -60, 0, 0, 60, 20, 0, 0, 'x', 0, "Export" },
	{ GADG_PUSH_BUTTON, -80, -30, 0, 0, 60, 20, 0, 0, CHR_CANCEL, 0, strCancel },

	{ GADG_PUSH_BUTTON, -80,  30, 0, 0, 60, 20, 0, 0, 'E', 0, "Enter" },
	{ GADG_PUSH_BUTTON, -80,  60, 0, 0, 60, 20, 0, 0, 'B', 0, "Back" },
	{ GADG_PUSH_BUTTON, -80,  90, 0, 0, 60, 20, 0, 0, 'D', 0, "Disks" },
#elif GERMAN
	{ GADG_PUSH_BUTTON, -80, -60, 0, 0, 60, 20, 0, 0, 'x', 0, "Export" },
	{ GADG_PUSH_BUTTON, -100, -30, 0, 0, 80, 20, 0, 0, CHR_CANCEL, 0, strCancel },

	{ GADG_PUSH_BUTTON, -100,  30, 0, 0, 80, 20, 0, 0, 'U', 0, "Unterv." },
	{ GADG_PUSH_BUTTON, -100,  60, 0, 0, 80, 20, 0, 0, 'M', 0, "Mutterv." },
	{ GADG_PUSH_BUTTON, -100,  90, 0, 0, 80, 20, 0, 0, 'f', 0, "Laufw." },
#elif FRENCH
	{ GADG_PUSH_BUTTON, -80, -60, 0, 0, 60, 20, 0, 0, 'x', 0, "Export" },
	{ GADG_PUSH_BUTTON, -90, -30, 0, 0, 70, 20, 0, 0, CHR_CANCEL, 0, strCancel },

	{ GADG_PUSH_BUTTON, -90,  30, 0, 0, 70, 20, 0, 0, 'E', 0, "Entre" },
	{ GADG_PUSH_BUTTON, -90,  60, 0, 0, 70, 20, 0, 0, 'R', 0, "Retour" },
	{ GADG_PUSH_BUTTON, -90,  90, 0, 0, 70, 20, 0, 0, 'V', 0, "Volumes" },
#elif SWEDISH
	{ GADG_PUSH_BUTTON, -80, -60, 0, 0, 60, 20, 0, 0, 'x', 0, "Export" },
	{ GADG_PUSH_BUTTON, -90, -30, 0, 0, 70, 20, 0, 0, CHR_CANCEL, 0, strCancel },

	{ GADG_PUSH_BUTTON, -90,  30, 0, 0, 70, 20, 0, 0, 'V', 0, "Visa" },
	{ GADG_PUSH_BUTTON, -90,  60, 0, 0, 70, 20, 0, 0, 'M', 0, "Moder" },
	{ GADG_PUSH_BUTTON, -90,  90, 0, 0, 70, 20, 0, 0, 'l', 0, "Volymer" },
#elif SPANISH
	{ GADG_PUSH_BUTTON, -80, -60, 0, 0, 60, 20, 0, 0, 'x', 0, "Export" },
	{ GADG_PUSH_BUTTON, -90, -30, 0, 0, 70, 20, 0, 0, CHR_CANCEL, 0, strCancel },

	{ GADG_PUSH_BUTTON, -90,  30, 0, 0, 70, 20, 0, 0, 'E', 0, "Entrar" },
	{ GADG_PUSH_BUTTON, -90,  60, 0, 0, 70, 20, 0, 0, 'R', 0, "Regresar" },
	{ GADG_PUSH_BUTTON, -90,  90, 0, 0, 70, 20, 0, 0, 'D', 0, "Discos" },
#endif
	SL_GADG_BOX(20, 30, EXPORT_WIDTH, EXPORT_NUM),
	SL_GADG_UPARROW(20, 30, EXPORT_WIDTH, EXPORT_NUM),
	SL_GADG_DOWNARROW(20, 30, EXPORT_WIDTH, EXPORT_NUM),
	SL_GADG_SLIDER(20, 30, EXPORT_WIDTH, EXPORT_NUM),

	{ GADG_EDIT_TEXT,  20, -55, 0, 0, EXPORT_WIDTH, 11, 0, 0, 0, 0, NULL },

	{ GADG_STAT_TEXT,  65, 10, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_IMAGE, 20, 15, 0, 0, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_STAT_TEXT,  20, -75, 0, 2, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_RADIO_BUTTON, 20, -35, 0, 0, 0, 0, 0, 0, 'S', 0, "Selected Objects" },
	{ GADG_RADIO_BUTTON, 20, -20, 0, 0, 0, 0, 0, 0, 'n', 0, "Entire Document" },

	{ GADG_ITEM_NONE }
};

static DialogTemplate exportEPSFDialog = {
#if (AMERICAN | BRITISH)
	DLG_TYPE_WINDOW, 0, -1, -1, 120 + EXPORT_WIDTH, 195, exportEPSFGadgets, NULL
#elif GERMAN
	DLG_TYPE_WINDOW, 0, -1, -1, 140 + EXPORT_WIDTH, 195, exportEPSFGadgets, NULL
#elif (FRENCH | SWEDISH | SPANISH)
	DLG_TYPE_WINDOW, 0, -1, -1, 130 + EXPORT_WIDTH, 195, exportEPSFGadgets, NULL
#endif
};

/*
	link dialog
 */

#define LINK_WIDTH		30*8
#define LINK_NUM		10

static GadgetTemplate linkGadgets[] = {
	{ GADG_PUSH_BUTTON, -80, 30, 0, 0, 60, 20, 0, 0, 'D', 0, "Done" },
	{ GADG_PUSH_BUTTON, -80, 60, 0, 0, 60, 20, 0, 0, 'I', 0, "Info" },
	{ GADG_PUSH_BUTTON, -80, 90, 0, 0, 60, 20, 0, 0, 'U', 0, "UnLink" },

	SL_GADG_BOX(20, 40, LINK_WIDTH, LINK_NUM),
	SL_GADG_UPARROW(20, 40, LINK_WIDTH, LINK_NUM),
	SL_GADG_DOWNARROW(20, 40, LINK_WIDTH, LINK_NUM),
	SL_GADG_SLIDER(20, 40, LINK_WIDTH, LINK_NUM),

	{ GADG_STAT_STDBORDER, 20, 25, 0, 0, LINK_WIDTH, 0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },

	{ GADG_STAT_TEXT,  20, 10, 0, 0,   0,  0, 0, 0, 0, 0, "Links" },

	{ GADG_ITEM_NONE }
};

static DialogTemplate linkDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 120 + LINK_WIDTH, 175, linkGadgets, NULL
};

/*
 *	link info dialog
 */

static GadgetTemplate linkInfoGadgets[] = {
	{ GADG_PUSH_BUTTON, -80, 10, 0, 0, 60, 20, 0, 0, CHR_OK, 0, strOK },

	{ GADG_STAT_TEXT, 110, 35, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT, 110, 50, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT,  70, 65, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT,  70, 80, 0, 0, 0, 0, 0, 0, 0, 0, NULL },
	{ GADG_STAT_TEXT,  70, 95, 0, 0, 0, 0, 0, 0, 0, 0, NULL },

	{ GADG_STAT_STDBORDER, 20, 25, 0, 0, 350 - 120, 0, 0, 0, 0, 0, (Ptr) BORDER_SHADOWLINE },

	{ GADG_STAT_TEXT,  20, 10, 0, 0,   0,  0, 0, 0, 0, 0, "Link Information" },

	{ GADG_STAT_TEXT, 20, 35, 0, 0, 0, 0, 0, 0, 0, 0, "File Name:" },
	{ GADG_STAT_TEXT, 20, 50, 0, 0, 0, 0, 0, 0, 0, 0, "Full Path:" },
	{ GADG_STAT_TEXT, 20, 65, 0, 0, 0, 0, 0, 0, 0, 0, "Size:" },
	{ GADG_STAT_TEXT, 20, 80, 0, 0, 0, 0, 0, 0, 0, 0, "Type:" },
	{ GADG_STAT_TEXT, 20, 95, 0, 0, 0, 0, 0, 0, 0, 0, "Date:" },
	{ GADG_ITEM_NONE }
};

static DialogTemplate linkInfoDialog = {
	DLG_TYPE_ALERT, 0, -1, -1, 350, 120, linkInfoGadgets, NULL
};

/*
 *	Dialog list
 */

DlgTemplPtr dlgList[] = {
	&initDialog,
	&saveDialog,
	&revertDialog,
	&savePrefsDialog,
	&pageDialog,
	&printDialog,
	&cancelPrintDialog,
	&nextPageDialog,
	&optionsDialog,
	&drawSizeDialog,
	&alignDialog,
	&errorDialog,
	&aboutDialog,
	&memoryDialog,
	&penSizeDialog,
	&scaleDialog,
	&penColorsDialog,
	&editPenColorDialog,
	&fillPatternsDialog,
	&editFillPatDialog,
	&layersDialog,
	&deleteLayerDialog,
	&gridDialog,
	&postScriptDialog,
	&customMacroDialog,
	&loseLinkDialog,
	&breakLinkDialog,
	&userReq1Dialog,
	&userReq2Dialog,
	&userReq3Dialog,
	&userReqTextDialog,
	&infoDialog,
	&exportDialog,
	&exportEPSFDialog,
	&printOptsDialog,
	&linkDialog,
	&linkInfoDialog,
	&rotateDialog
};
