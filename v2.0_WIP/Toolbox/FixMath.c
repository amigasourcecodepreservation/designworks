/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Toolbox library
 *	Copyright (c) 1989-93 New Horizons Software, Inc.
 *
 *	Fixed point math routines
 */

#include <Toolbox/Memory.h>
#include <Toolbox/LocalData.h>
#include <Toolbox/Utility.h>
#include <Toolbox/FixMath.h>

#include <string.h>

#include <Typedefs.h>

/*
 *	Local definitions
 */

#define HIWORD(num)	(((num) >> 16) & 0xFFFF)
#define LOWORD(num)	((num) & 0xFFFF)

/*
 *	Return the fixed point ratio of two word values
 */

Fixed FixRatio(WORD num1, WORD num2)
{
	return (FixDiv(num1 << 16, num2 << 16));
}

/*
 *	Convert ASCII text to fixed point number
 */

Fixed Ascii2Fix(register TextPtr s)
{
	register LONG	divisor, frac;
	register BOOL	negate;
	register Fixed	num;

	while (*s == ' ')
		s++;
	if (*s == '-') {
		negate = TRUE;
		s++;
	}
	else
		negate = FALSE;
	num = 0;
	while (*s && *s >= '0' && *s <= '9') {
		num *= 10;
		num += (*s++ & 0x0F);
	}
	num <<= 16;
	if (*s++ == DECIMAL_CHAR) {
		frac = 0;
		divisor = 1;
		while (*s >= '0' && *s <= '9' && divisor <= (9 << 16)) {
			frac *= 10;
			frac += (*s++ & 0x0F);
			divisor *= 10;
		}
		frac = (frac << 16)/divisor;
		if (*s >= '5' && *s <= '9')
			frac++;
		num += frac;
	}
	if (negate)
		num = -num;
	return (num);
}

/*
 *	Convert fixed point number to ASCII text with specified decimal places
 *	String will be zero-padded to number of places
 *	Maximum number of decimal places is four
 */

void Fix2Ascii(register Fixed num, register TextPtr s, register WORD places)
{
	register WORD	i, divisor, extra;

	if (places < 0)
		places = 0;
	else if (places > 4)
		places = 4;
	if (num < 0) {
		*s++ = '-';
		num = -num;
	}
	divisor = 1;
	for (i = 0; i < places; i++)
		divisor *= 10;
	num += (0x00008000L + divisor/2)/divisor;
	NumToString(HIWORD(num), s);
	if (places > 0) {
		s += strlen(s);
		*s++ = DECIMAL_CHAR;
		num = LOWORD(num);
		for (i = 0; i < places; i++)
			num *= 10;
		NumToString(HIWORD(num), s);
		extra = places - strlen(s);
		if (extra > 0) {
			BlockMove(s, s + extra, strlen(s) + 1);
			while (extra--)
				*s++ = '0';
		}
	}
}

/*
 *	Multiply two fixed point numbers
 */

Fixed FixMul(register Fixed num1, register Fixed num2)
{
	register LONG	result;
	register BOOL	negate;
	register ULONG	lo1, lo2, hi1, hi2;

	negate = FALSE;
	if (num1 < 0) {
		negate = !negate;
		num1 = -num1;
	}
	if (num2 < 0) {
		negate = !negate;
		num2 = -num2;
	}
	lo1 = LOWORD(num1);	lo2 = LOWORD(num2);
	hi1 = HIWORD(num1);	hi2 = HIWORD(num2);
	result = (hi1*hi2 << 16) + lo1*hi2 + lo2*hi1 + (lo1*lo2 >> 16);
	if (negate)
		result = -result;
	return ((Fixed) result);
}

/*
 *	Divide two fixed point numbers
 *	Can also divide two LONG numbers, returning a Fixed result
 */

Fixed FixDiv(register Fixed num1, register Fixed num2)
{
	register WORD	i;
	register LONG	result;
	register BOOL	negate;

	negate = FALSE;
	if (num1 < 0) {
		negate = !negate;
		num1 = -num1;
	}
	if (num2 < 0) {
		negate = !negate;
		num2 = -num2;
	}
	if (num2 == 0)
		result = (negate) ? 0x80000000L : 0x7FFFFFFFL;
	else {
		result = 0;
		for (i = 0; i < 16; i++) {
			result += num1/num2;
			num1 = (num1 % num2) << 1;
			result <<= 1;
		}
		if (negate)
			result = -result;
	}
	return ((Fixed) result);
}
