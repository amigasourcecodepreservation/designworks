/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Toolbox library
 *	Copyright (c) 1992-93 New Horizons Software, Inc.
 *
 *	Font requester routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>

#include <string.h>

#include <Typedefs.h>

#include <Toolbox/Globals.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Font.h>
#include <Toolbox/Border.h>
#include <Toolbox/ScrollList.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/SelectFont.h>
#include <Toolbox/Utility.h>

/*
 *	External variables
 */

extern TextChar	_tbStrSampleText[];

extern DialogTemplate	_tbSelFontDlgTempl;

/*
 *	Local variables and definitions
 */

#define SHIFTKEYS	(IEQUALIFIER_LSHIFT | IEQUALIFIER_RSHIFT)

enum {
	LISTMENU_BOX = 2,
	SHOWSAMPLE_BOX,
	NAME_LIST,
	NAME_UP,
	NAME_DOWN,
	NAME_SCROLL,
	SIZE_LIST,
	SIZE_UP,
	SIZE_DOWN,
	SIZE_SCROLL,
	SIZE_TEXT,
	SAMPLE_ITEM
};

static BOOL	showSample = TRUE;

static ScrollListPtr	nameScrollList, sizeScrollList;
static DialogPtr		fontDlg;
static MsgPortPtr		fontMsgPort;

static BOOL	(*fontDlgFilter)(IntuiMsgPtr, WORD *);

/*
 *	Local prototypes
 */

static void	GetNameList(ScrollListPtr);
static void	GetSizeList(ScrollListPtr);

static FontNum	SelFontNum(void);
static FontSize	SelSize(void);

static void	ShowFontSample(FontNum, FontSize);
static void	NewName(WORD);
static void	NewSize(WORD);
static BOOL	FontDialogFilter(IntuiMsgPtr, WORD *);

/*
 *	Get and display the font name list
 */

static void GetNameList(ScrollListPtr scrollList)
{
	register WORD	i, fontNum, numNames;
	WORD			style;
	TextPtr			name;
	ListHeadPtr		nameList;
	TextChar		fontName[50];

/*
	Get sorted name list with capitals as first letter of name
	(Since SLAddItem() does not sort entries)
*/
	if ((nameList = CreateList()) == NULL)
		return;
	numNames = NumFonts();
	DisposeListItems(nameList);
	for (i = 0; i < numNames; i++) {
		GetFontName(i, fontName);
		fontName[0] = toUpper[fontName[0]];
		if (AddListItem(nameList, fontName, 0, TRUE) == -1)
			break;
	}
/*
	Now display the new list of font names
*/
	SLDoDraw(scrollList, FALSE);
	numNames = NumListItems(nameList);
	for (i = 0; i < numNames; i++) {
		name = GetListItem(nameList, i);
		SLAddItem(scrollList, name, 0, i);
		fontNum = FontNumber(name);
		style = FS_NORMAL;
		if (IsPostScriptFont(fontNum))
			style |= FSF_BOLD;
		if (IsOutlineFont(fontNum))
			style |= FSF_ITALIC;
		SLSetItemStyle(scrollList, i, style);
	}
	SLDoDraw(scrollList, TRUE);
	DisposeList(nameList);
}

/*
 *	Get and display the size list of the currently selected font name
 */

static void GetSizeList(ScrollListPtr scrollList)
{
	WORD		i, len, numSizes, textLen, numAttrs;
	FontNum		fontNum;
	FontSize	size, *sizeList, *newList;
	TextAttrPtr	fontTable;
	TextPtr		name;
	TextChar	text[50];

	if ((i = SLNextSelect(nameScrollList, -1)) == -1)
		return;
	SLGetItem(nameScrollList, i, text);
	textLen = strlen(text);
/*
	Get sorted size list for selected font
*/
	fontTable = FontAttrTable();
	numAttrs = NumFontAttrs();
	numSizes = 0;
	sizeList = NULL;
	for (fontNum = 0; fontNum < numAttrs; fontNum++) {
		name = fontTable[fontNum].ta_Name;
		len = strlen(name) - 5;
/*
	If this is the selected font name then insert size into list
*/
		if (len <= 0 || CmpString(name, text, len, textLen, FALSE) != 0)
			continue;
		if ((newList = MemAlloc((numSizes + 1)*sizeof(WORD), 0)) == NULL)
			break;
		size = fontTable[fontNum].ta_YSize;
		for (i = 0; i < numSizes && sizeList[i] < size; i++)
			newList[i] = sizeList[i];
		if (i < numSizes && sizeList[i] == size)
			MemFree(newList, (numSizes + 1)*sizeof(WORD));
		else {
			newList[i] = size;
			for ( ; i < numSizes; i++)
				newList[i + 1] = sizeList[i];
			if (sizeList)
				MemFree(sizeList, numSizes*sizeof(WORD));
			sizeList = newList;
			numSizes++;
		}
	}
/*
	Now display the new list of font sizes
*/
	SLDoDraw(scrollList, FALSE);
	for (i = 0; i < numSizes; i++) {
		NumToString(sizeList[i], text);
		SLAddItem(scrollList, text, strlen(text), i);
	}
	SLDoDraw(scrollList, TRUE);
	if (sizeList)
		MemFree(sizeList, numSizes*sizeof(WORD));
}

/*
 *	Return font number of selected font
 *	Return -1 if no font is chosen
 */

static FontNum SelFontNum()
{
	WORD		i;
	TextChar	name[50];

	if ((i = SLNextSelect(nameScrollList, -1)) < 0)
		return (-1);
	SLGetItem(nameScrollList, i, name);
	return (FontNumber(name));
}

/*
 *	Return selected size
 */

static FontSize SelSize()
{
	TextChar	sizeText[GADG_MAX_STRING];

	GetEditItemText(fontDlg->FirstGadget, SIZE_TEXT, sizeText);
	return (StringToNum(sizeText));
}

/*
 *	Show sample of selected font
 */

static void ShowFontSample(FontNum fontNum, FontSize fontSize)
{
	WORD		baseline;
	TextFontPtr	font, oldFont;
	RegionPtr	oldRgn;
	LayerPtr	layer = fontDlg->WLayer;
	RastPtr		rPort = fontDlg->RPort;
	GadgetPtr	gadget;
	Rectangle	rect;

	gadget = GadgetItem(fontDlg->FirstGadget, SAMPLE_ITEM);
	GetGadgetRect(gadget, fontDlg, NULL, &rect);
/*
	First clear the display area
*/
	DrawShadowBox(rPort, &rect, -1, FALSE);
	oldRgn = GetClip(layer);
	SetRectClip(layer, &rect);
	SetRast(rPort, _tbPenLight);
/*
	Now get and display the font
*/
	if (showSample && fontSize > 0 && fontSize <= FONT_MAXSIZE) {
		SetStdPointer(fontDlg, POINTER_WAITDELAY);
		if ((font = GetNumFont(fontNum, fontSize)) != NULL) {
			baseline = font->tf_Baseline + (rect.MaxY - rect.MinY + 1 - font->tf_YSize)/2;
			if (baseline < font->tf_Baseline)
				baseline = font->tf_Baseline;
			baseline += rect.MinY;
			if (baseline > rect.MaxY)
				baseline = rect.MaxY;
			oldFont = rPort->Font;
			SetFont(rPort, font);
			SetAPen(rPort, _tbPenBlack);
			SetDrMd(rPort, JAM1);
			Move(rPort, rect.MinX, baseline);
			Text(rPort, _tbStrSampleText, strlen(_tbStrSampleText));
			SetFont(rPort, oldFont);
			CloseFont(font);
		}
		SetStdPointer(fontDlg, POINTER_ARROW);
	}
/*
	Clean up
*/
	SetClip(layer, oldRgn);
}

/*
 *	Set new selected name
 */

static void NewName(WORD nameNum)
{
	WORD	numNames;

	if ((numNames = SLNumItems(nameScrollList)) == 0 ||
		nameNum < 0 || nameNum >= numNames)
		return;
	SLSelectItem(nameScrollList, nameNum, TRUE);
	SLAutoScroll(nameScrollList, nameNum);
	SLRemoveAll(sizeScrollList);
	GetSizeList(sizeScrollList);
	NewSize(-1);		// Select size if it is in list
}

/*
 *	Set new selected size
 *	If sizeNum is -1, then check size text box
 */

static void NewSize(WORD sizeNum)
{
	WORD		i, numSizes;
	FontSize	selSize;
	BOOL		match;
	TextChar	sizeText[GADG_MAX_STRING];

	if ((numSizes = SLNumItems(sizeScrollList)) == 0)
		return;
	if (sizeNum == -1) {
		selSize = SelSize();
		for (i = 0; i < numSizes; i++) {
			SLGetItem(sizeScrollList, i, sizeText);
			match = (selSize == StringToNum(sizeText));
			SLSelectItem(sizeScrollList, i, match);
			if (match)
				SLAutoScroll(sizeScrollList, i);
		}
	}
	else {
		if (sizeNum < 0 || sizeNum >= numSizes)
			return;
		SLSelectItem(sizeScrollList, sizeNum, TRUE);
		SLAutoScroll(sizeScrollList, sizeNum);
		SLGetItem(sizeScrollList, sizeNum, sizeText);
		SetEditItemText(fontDlg->FirstGadget, SIZE_TEXT, fontDlg, NULL, sizeText);
	}
}

/*
 *	Font dialog filter
 */

static BOOL FontDialogFilter(register IntuiMsgPtr intuiMsg, WORD *item)
{
	WORD			gadgNum;
	ULONG			class = intuiMsg->Class;
	UWORD			code = intuiMsg->Code;
	UWORD			modifier = intuiMsg->Qualifier;
	ScrollListPtr	scrollList;

	if (intuiMsg->IDCMPWindow == fontDlg) {
		switch (class) {
/*
	Handle gadget down of up/down scroll arrows
*/
		case GADGETDOWN:
		case GADGETUP:
			gadgNum = GadgetNumber((GadgetPtr) intuiMsg->IAddress);
			if (gadgNum == NAME_LIST || gadgNum == NAME_SCROLL ||
				gadgNum == NAME_UP || gadgNum == NAME_DOWN) {
				SLGadgetMessage(nameScrollList, fontMsgPort, intuiMsg);
				*item = gadgNum;
				return (TRUE);
			}
			if (gadgNum == SIZE_LIST || gadgNum == SIZE_SCROLL ||
				gadgNum == SIZE_UP || gadgNum == SIZE_DOWN) {
				SLGadgetMessage(sizeScrollList, fontMsgPort, intuiMsg);
				*item = gadgNum;
				return (TRUE);
			}
			break;
/*
	Handle keyboard events
*/
		case RAWKEY:
			if (code == CURSORUP || code == CURSORDOWN) {
				ReplyMsg((MsgPtr) intuiMsg);
				if (modifier & SHIFTKEYS) {
					scrollList = sizeScrollList;
					*item = SIZE_LIST;
				}
				else {
					scrollList = nameScrollList;
					*item = NAME_LIST;
				}
				SLCursorKey(scrollList, code);
				return (TRUE);
			}
			break;
		}
	}
	return ((fontDlgFilter) ? (*fontDlgFilter)(intuiMsg, item) : FALSE);
}

/*
 *	SelectFont
 *	Display font requester and have user select desired font
 */

BOOL SelectFont(ScreenPtr screen, MsgPortPtr msgPort,
				BOOL (*dlgFilter)(IntuiMsgPtr, WORD *),
				BOOL (*isMenuFont)(FontNum, FontSize),
				void (*setMenuFont)(FontNum, FontSize, BOOL),
				FontNum *fontNum, FontSize *fontSize)
{
	WORD		i, item, len, numNames, count;
	BOOL		result, done, inMenu;
	FontNum		selFontNum;
	FontSize	selSize;
	GadgetPtr	gadgList;
	TextChar	name[50], text[50];

	result = FALSE;
	nameScrollList = sizeScrollList = NULL;
	fontDlg = NULL;
	fontDlgFilter = dlgFilter;
	fontMsgPort = msgPort;
/*
	Bring up dialog
*/
	if ((nameScrollList = NewScrollList(SL_SINGLESELECT)) == NULL ||
		(sizeScrollList = NewScrollList(SL_SINGLESELECT)) == NULL ||
		(fontDlg = GetDialog(&_tbSelFontDlgTempl, screen, msgPort)) == NULL) {
		goto Exit;
	}
	SetStdPointer(fontDlg, POINTER_WAIT);
	gadgList = fontDlg->FirstGadget;
	InitScrollList(nameScrollList, GadgetItem(gadgList, NAME_LIST), fontDlg, NULL);
	InitScrollList(sizeScrollList, GadgetItem(gadgList, SIZE_LIST), fontDlg, NULL);
	OutlineButton(GadgetItem(gadgList, OK_BUTTON), fontDlg, NULL, TRUE);
	SLDrawBorder(nameScrollList);
	SLDrawBorder(sizeScrollList);
	EnableGadgetItem(gadgList, LISTMENU_BOX, fontDlg, NULL, (isMenuFont && setMenuFont));
	GetFontName(*fontNum, name);
	NumToString(*fontSize, text);
	SetEditItemText(gadgList, SIZE_TEXT, fontDlg, NULL, text);
	len = strlen(name);
	GetNameList(nameScrollList);
	numNames = SLNumItems(nameScrollList);
	for (i = 0; i < numNames; i++) {
		SLGetItem(nameScrollList, i, text);
		if (CmpString(name, text, len, strlen(text), FALSE) == 0)
			break;
	}
	NewName(i);
	if (isMenuFont)
		SetGadgetItemValue(gadgList, LISTMENU_BOX, fontDlg, NULL,
						   (*isMenuFont)(*fontNum, *fontSize));
	SetGadgetItemValue(gadgList, SHOWSAMPLE_BOX, fontDlg, NULL, showSample);
/*
	Get and process requester messages
*/
	selFontNum = SelFontNum();
	selSize = SelSize();
	ShowFontSample(selFontNum, selSize);
	count = -1;
	done = FALSE;
	SetStdPointer(fontDlg, POINTER_ARROW);
	do {
		WaitPort(msgPort);
		item = CheckDialog(msgPort, fontDlg, FontDialogFilter);
		switch (item) {
		case -1:
			if (selSize != SelSize()) {
				selSize = SelSize();
				count = 5;				// 1/2 second before we new show sample
			}
			else if (count > 0)
				count--;
			else if (count == 0) {
				NewSize(-1);
				selSize = SelSize();	// Just in case it changed
				if (isMenuFont)
					SetGadgetItemValue(gadgList, LISTMENU_BOX, fontDlg, NULL,
									   (*isMenuFont)(selFontNum, selSize));
				ShowFontSample(selFontNum, selSize);
				count = -1;
			}
			break;
		case OK_BUTTON:
Done:
			if ((*fontNum = selFontNum) < 0 || (*fontSize = selSize) <= 0)
				SysBeep(5);
			else {
				result = TRUE;
				done = TRUE;
			}
			break;
		case CANCEL_BUTTON:
			done = TRUE;
			break;
		case NAME_LIST:
			if ((i = SLNextSelect(nameScrollList, -1)) == -1)
				break;
			NewName(i);
			selFontNum = SelFontNum();
			if (isMenuFont)
				SetGadgetItemValue(gadgList, LISTMENU_BOX, fontDlg, NULL,
								   (*isMenuFont)(selFontNum, selSize));
			ShowFontSample(selFontNum, selSize);
			if (SLIsDoubleClick(nameScrollList))
				goto Done;
			break;
		case SIZE_LIST:
			if ((i = SLNextSelect(sizeScrollList, -1)) == -1)
				break;
			NewSize(i);
			selSize = SelSize();
			if (isMenuFont)
				SetGadgetItemValue(gadgList, LISTMENU_BOX, fontDlg, NULL,
								   (*isMenuFont)(selFontNum, selSize));
			ShowFontSample(selFontNum, selSize);
			if (SLIsDoubleClick(sizeScrollList))
				goto Done;
			break;
		case LISTMENU_BOX:
			SetStdPointer(fontDlg, POINTER_WAITDELAY);
			if (isMenuFont) {
				inMenu = (*isMenuFont)(selFontNum, selSize);
				if (setMenuFont)
					(*setMenuFont)(selFontNum, selSize, !inMenu);
					SetGadgetItemValue(gadgList, LISTMENU_BOX, fontDlg, NULL,
									   (*isMenuFont)(selFontNum, selSize));
			}
			SetStdPointer(fontDlg, POINTER_ARROW);
			break;
		case SHOWSAMPLE_BOX:
			showSample = !showSample;
			SetGadgetItemValue(gadgList, SHOWSAMPLE_BOX, fontDlg, NULL, showSample);
			ShowFontSample(selFontNum, selSize);
			break;
		}
	} while (!done);
	DisposeDialog(fontDlg);
/*
	Return with success status
*/
Exit:
	if (sizeScrollList)
		DisposeScrollList(sizeScrollList);
	if (nameScrollList)
		DisposeScrollList(nameScrollList);
	return (result);
}
