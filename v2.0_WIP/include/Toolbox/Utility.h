/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Toolbox library
 *	Copyright (c) 1991 New Horizons Software
 *
 *	Misc utility routines
 */

#ifndef TOOLBOX_UTILITY_H
#define TOOLBOX_UTILITY_H

#ifndef EXEC_TYPES_H
#include <exec/types.h>
#endif

#ifndef INTUITION_INTUITION_H
#include <intuition/intuition.h>
#endif

#ifndef TYPEDEFS_H
#include <Typedefs.h>
#endif

#ifndef TOOLBOX_COLOR_H
#include <Toolbox/Color.h>
#endif

/*
 *	System versions
 */

#define OSVERSION_1_2	33
#define OSVERSION_1_3	34
#define OSVERSION_2_0	36
#define OSVERSION_2_0_4	37
#define OSVERSION_2_1	38
#define OSVERSION_3_0	39

/*
 *	Pointer types
 */

enum {
	POINTER_ARROW = 0,
	POINTER_IBEAM,
	POINTER_CROSS,
	POINTER_PLUS,
	POINTER_WAIT,
	POINTER_WAITDELAY
};

#define POINTER_MAX_TYPE	POINTER_WAITDELAY

/*
 *	Pointer structure definition
 */

typedef struct {
    WORD	Width, Height;
    WORD	XOffset, YOffset;
    UWORD	*PointerData;
} Pointer, *PointerPtr;

/*
 *	Arrays
 */

extern TextChar	toUpper[];		// Convert character to upper case
extern TextChar	toLower[];		// Convert character to lower case
extern BYTE		wordChar[];		// 1 if char is valid word char, 0 otherwise

/*
 *	Prototypes
 */

void	InitToolbox(ScreenPtr);
void	SetToolboxColors(ColorMapPtr, WORD);
void	AutoActivateEnable(BOOL);

UWORD	LibraryVersion(struct Library *);
UWORD	SystemVersion(void);

void	SetStdPointer(WindowPtr, UWORD);
void	ObscurePointer(WindowPtr);

void	ConvertKeyMsg(IntuiMsgPtr);
BOOL	WaitMouseUp(MsgPortPtr, WindowPtr);

WORD	CmpString(TextPtr, TextPtr, WORD, WORD, BOOL);
void	NumToString(LONG, TextPtr);
LONG	StringToNum(TextPtr);

TextFontPtr	GetFont(TextAttrPtr);

void	TextInWidth(RastPtr, TextPtr, WORD, WORD, BOOL);

void	GetScreenViewRect(ScreenPtr, RectPtr);

PenNum	PenNumber(RGBColor, ColorMapPtr, WORD);

void	SysBeep(UWORD);
BOOL	MatchFilename(TextPtr, TextPtr);

#endif
