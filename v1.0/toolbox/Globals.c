/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Toolbox library
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Global variables
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include "typedefs.h"

BOOL	_tbHiRes;	/* TRUE if using hi-res screen, inited to FALSE */

static TextAttr	defaultTextAttr = {
	"topaz.font", 8, FS_NORMAL, FPF_ROMFONT
};

TextAttrPtr	_tbTextAttr = &defaultTextAttr;

#define COLOR_BLUE	0
#define COLOR_WHITE	1

WORD	_tbBlackColor = COLOR_BLUE;
WORD	_tbWhiteColor = COLOR_WHITE;
WORD	_tbDarkColor = COLOR_BLUE;
WORD	_tbLightColor = COLOR_WHITE;

BOOL	_tbAutoActivate = TRUE;
