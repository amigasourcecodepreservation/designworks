/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Toolbox library
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Utility routines
 */

#define INTUI_V36_NAMES_ONLY	1

#include <exec/types.h>
#include <graphics/displayinfo.h>
#include <intuition/intuition.h>
#include <intuition/intuitionbase.h>

#include <string.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>
#include <proto/diskfont.h>
#include <proto/console.h>

#include <Typedefs.h>

#include <Toolbox/Language.h>
#include <Toolbox/Globals.h>
#include <Toolbox/Memory.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Color.h>
#include <Toolbox/ColorPick.h>
#include <Toolbox/Utility.h>

/*
 *	External variables
 */

extern struct IntuitionBase *IntuitionBase;
extern struct GfxBase		*GfxBase;

/*
 *	Local variables and definitions
 */

#define RAWKEY_F1	0x50
#define RAWKEY_F10	0x59
#define RAWKEY_HELP	0x5F

#define SELECTBUTTON(q)	\
	((q&IEQUALIFIER_LEFTBUTTON)||((q&AMIGAKEYS)&&(q&IEQUALIFIER_LALT)))

/*
 *	Pointers
 */

static UWORD chip nullPointerData[] = {
	0x0000, 0x0000, 0x0000, 0x0000
};

static UWORD chip arrowPointerData[] = {
	0x0000, 0x0000, 0x0000, 0xC000, 0x4000, 0xE000, 0x6000, 0xB000,
	0x7000, 0x9800, 0x7800, 0x8C00, 0x7C00, 0x8600, 0x7E00, 0x8300,
	0x7800, 0x8F00, 0x5800, 0xAC00, 0x0C00, 0xD600, 0x0C00, 0x1600,
	0x0000, 0x0E00, 0x0000, 0x0000
};

static UWORD chip iBeamPointerData[] = {
	0x0000, 0x0000, 0x4400, 0x8800, 0x2800, 0x5000, 0x1000, 0x2000,
	0x1000, 0x2000, 0x1000, 0x2000, 0x1000, 0x2000, 0x1000, 0x2000,
	0x1000, 0x2000, 0x1000, 0x2000, 0x2800, 0x5000, 0x4400, 0x8800,
	0x0000, 0x0000
};

static UWORD chip plusPointerData[] = {
	0x0000, 0x0000, 0x0000, 0x3E00, 0x1C00, 0x3E00, 0x1C00, 0xE780,
	0x7F00, 0xE780, 0x7F00, 0x8180, 0x7F00, 0x8180, 0x1C00, 0xE780,
	0x1C00, 0x2600, 0x0000, 0x3E00, 0x0000, 0x0000
};

static UWORD chip crossPointerData[] = {
	0x0000, 0x0000,
	0x0400, 0x0000, 0x0400, 0x0000, 0x0400, 0x0000, 0x0400, 0x0000,
	0x0000, 0x0000, 0xF1E0, 0x0400, 0x0000, 0x0000,
	0x0400, 0x0000, 0x0400, 0x0000, 0x0400, 0x0000, 0x0400, 0x0000,
	0x0000, 0x0000
};

static UWORD chip waitPointerData[] = {
	0x0000, 0x0000,
	0x0000, 0x0780, 0x0780, 0x1860, 0x1860, 0x2790, 0x37B0, 0x4FC8,
	0x2DD0, 0x5FE8, 0x5DE8, 0xBFF4, 0x5DE8, 0xBFF4, 0x5C68, 0xBFF4,
	0x5FE8, 0xBFF4, 0x2FD0, 0x5FE8, 0x37B0, 0x4FC8, 0x1860, 0x2790,
	0x0780, 0x1860, 0x0000, 0x0780,
	0x0000, 0x0000
};

static Pointer nullPointer = {
	0, 0, 0, 0, &nullPointerData[0]
};

static Pointer arrowPointer = {
	8, 12, -1, 0, &arrowPointerData[0]
};

static Pointer iBeamPointer = {
	6, 11, -4, -5, &iBeamPointerData[0]
};

static Pointer crossPointer = {
	11, 11, -6, -5, &crossPointerData[0]
};

static Pointer plusPointer = {
	9, 9, -5, -4, &plusPointerData[0]
};

static Pointer waitPointer = {
	14, 14, -7, -7, &waitPointerData[0]
};

/*
 *	Pointer array
 *	Must be in order given by pointer type definitions
 */

static Pointer *pointerArray[] = {
	&arrowPointer,
	&iBeamPointer,
	&crossPointer,
	&plusPointer,
	&waitPointer,
	&waitPointer			// Wait delay pointer for Kickstart 1.2 to 2.0
};

/*
 *	InitToolbox
 *	Initialize toolbox for specified screen
 *	Default state is med-res and Topaz-8 font
 */

void InitToolbox(register ScreenPtr screen)
{
	TextFontPtr		font;
	RastPort		rPort;
	struct DrawInfo	*drawInfo;

	_tbScreen = screen;
	_tbTextAttr = *(screen->Font);
	_tbTextAttr.ta_Style = FS_NORMAL;	// Needs to be like this
/*
	Get font dimensions
	(Use width of "e" rather than font's XSize,
		since this works better for proportional fonts)
*/
	if ((font = OpenFont(&_tbTextAttr)) == NULL) {
		_tbXSize	= 8;
		_tbBaseline	= 7;
	}
	else {
		InitRastPort(&rPort);
		SetFont(&rPort, font);
		_tbXSize	= TextLength(&rPort, "e", 1);
		_tbBaseline	= font->tf_Baseline;
		CloseFont(font);
	}
	_tbYSize = _tbTextAttr.ta_YSize;
/*
	Get "new look" pen numbers
*/
	SetToolboxColors(screen->ViewPort.ColorMap, 1 << screen->RastPort.BitMap->Depth);
/*
	Get menu text pen number
*/
	drawInfo = NULL;
	if (LibraryVersion((struct Library *) IntuitionBase) >= OSVERSION_3_0 &&
		(drawInfo = GetScreenDrawInfo(screen)) != NULL &&
		drawInfo->dri_NumPens > BARDETAILPEN)
		_tbPenMenu = drawInfo->dri_Pens[BARDETAILPEN];
	else
		_tbPenMenu = 0;
	if (drawInfo)
		FreeScreenDrawInfo(screen, drawInfo);
}

/*
 *	Set toolbox colors from color map
 */

void SetToolboxColors(ColorMapPtr colorMap, WORD numColors)
{
	RGBColor	rgbBlack, rgbWhite, rgbDark, rgbLight;

	_tbPenBlack = PenNumber(0x000, colorMap, numColors);
	_tbPenWhite = PenNumber(0xFFF, colorMap, numColors);
	rgbBlack = GetRGB4(colorMap, _tbPenBlack);
	rgbWhite = GetRGB4(colorMap, _tbPenWhite);
	rgbDark  = RGBCOLOR( 5*(  RED(rgbWhite) -   RED(rgbBlack))/15 +   RED(rgbBlack),
						 5*(GREEN(rgbWhite) - GREEN(rgbBlack))/15 + GREEN(rgbBlack),
						 6*( BLUE(rgbWhite) -  BLUE(rgbBlack))/15 +  BLUE(rgbBlack));
	rgbLight = RGBCOLOR(10*(  RED(rgbWhite) -   RED(rgbBlack))/15 +   RED(rgbBlack),
						10*(GREEN(rgbWhite) - GREEN(rgbBlack))/15 + GREEN(rgbBlack),
						11*( BLUE(rgbWhite) -  BLUE(rgbBlack))/15 +  BLUE(rgbBlack));
	_tbPenDark  = PenNumber(rgbDark, colorMap, numColors);	// Dark blue-gray
	_tbPenLight = PenNumber(rgbLight, colorMap, numColors);	// Light cyan-gray
	_tbPenRed	= PenNumber(0xF00, colorMap, numColors);	// Red
	if (_tbPenDark == _tbPenLight)
		_tbPenLight = _tbPenWhite;
	if (_tbPenRed == _tbPenBlack)
		_tbPenRed = _tbPenDark;
	_tbNoShadows = (_tbPenLight == _tbPenWhite);
}

/*
 *	Enable or disable auto-activation of text boxes in requesters and dialogs
 *	Default is enabled
 */

void AutoActivateEnable(BOOL enable)
{
	_tbAutoActivate = enable;
}

/*
 *	SetStdPointer
 *	Set pointer to one of standard types
 *	Does not change pointer if already set to requested pointer
 *	Note: Under system 2.0 & 3.0, window->Pointer is NULL for default system pointers,
 *		  There is no way to determine which of the system arrow or wait pointer is set
 *		  Also, when system wait pointer is set, a ClearPointer() must be done before
 *		  custom pointers can be set
 */

void SetStdPointer(WindowPtr window, UWORD pointerType)
{
	register WORD		intuiVersion;
	register PointerPtr	pointer;

	if (pointerType > POINTER_MAX_TYPE)
		return;
	intuiVersion = LibraryVersion((struct Library *) IntuitionBase);
	if (pointerType == POINTER_ARROW && intuiVersion >= OSVERSION_2_0)
		ClearPointer(window);
	else if ((pointerType == POINTER_WAIT || pointerType == POINTER_WAITDELAY) &&
			 intuiVersion >= OSVERSION_3_0) {
		ClearPointer(window);					// Set window->Pointer to NULL
		SetWindowPointer(window,
						 WA_BusyPointer, TRUE,
						 WA_PointerDelay, (pointerType == POINTER_WAITDELAY),
						 TAG_DONE);
	}
	else {
		pointer = pointerArray[pointerType];
		if (window->Pointer != pointer->PointerData) {
			SetPointer(window, pointer->PointerData,
					   pointer->Height, pointer->Width,
					   pointer->XOffset, pointer->YOffset);
		}
	}
}

/*
 *	ObscurePointer
 *	Hide the pointer from view
 *	(Since we compile with -b option, we must reference off a Pointer struct)
 */

void ObscurePointer(WindowPtr window)
{
	if (window->Pointer != nullPointer.PointerData)
		SetPointer(window, nullPointer.PointerData, 0, 0, 0, 0);
}

/*
 *	ConvertKeyMsg
 *	Convert RAWKEY messages for ASCII keys to VANILLAKEY messages
 *	Must be called before message is replied to,
 *		and ConsoleDevice base address must have obtained from console.device
 */

static struct InputEvent keyEvent = {
	NULL, IECLASS_RAWKEY, 0, 0, 0, (WORD) 0, (WORD) 0, (ULONG) 0, (ULONG) 0
};

void ConvertKeyMsg(register IntuiMsgPtr intuiMsg)
{
	register UWORD	code;
	register UWORD	qualifier;
	register WORD	num;
	UBYTE			key;		// Need to take its address

	code = intuiMsg->Code;
	qualifier = intuiMsg->Qualifier;
	if (intuiMsg->Class != IDCMP_RAWKEY				||
		(code & IECODE_UP_PREFIX)					||
		(qualifier & AMIGAKEYS)						||
		(code >= CURSORUP && code <= CURSORLEFT)	||
		(code >= RAWKEY_F1 && code <= RAWKEY_F10)	||
		code == RAWKEY_HELP)
		return;
	keyEvent.ie_Code = code;
	keyEvent.ie_Qualifier = qualifier;
	keyEvent.ie_position.ie_addr = *((APTR *) intuiMsg->IAddress);
	if ((num = RawKeyConvert(&keyEvent, &key, 1, NULL)) == 1) {
		intuiMsg->Code	= key;
		intuiMsg->Class	= IDCMP_VANILLAKEY;
	}
	else if (num == 0)						// Was SHIFT, ALT, etc.
		intuiMsg->Code |= IECODE_UP_PREFIX;	// Make caller ignore it
}

/*
 *	WaitMouseUp
 *	Check msgPort for mouse up event in window
 *	If message is not for specified window, or is not MOUSEMOVE, MOUSEBUTTONS,
 *	RAWKEY or INTUITICKS, put message back and return FALSE
 *	Otherwise reply to message and return status of select button
 *	Will wait until at least one message is received
 *	(Having INTUITICKS enabled allows us to auto-scroll while dragging)
 */

BOOL WaitMouseUp(MsgPortPtr msgPort, WindowPtr window)
{
	register BOOL			mouseDown;
	register ULONG			class;
	register UWORD			code, modifier;
	register IntuiMsgPtr	intuiMsg;

	mouseDown = TRUE;					// Assume mouse down (for INTUITICKS msgs)
	(void) WaitPort(msgPort);
	while (mouseDown && (intuiMsg = (IntuiMsgPtr) GetMsg(msgPort)) != NULL) {
		class = intuiMsg->Class;
		code = intuiMsg->Code;
		modifier = intuiMsg->Qualifier;
		if (intuiMsg->IDCMPWindow != window ||
			(class != IDCMP_MOUSEMOVE && class != IDCMP_MOUSEBUTTONS &&
			 class != IDCMP_RAWKEY && class != IDCMP_INTUITICKS)) {
			PutMsg(msgPort, (MsgPtr) intuiMsg);
			return (FALSE);
		}
		ReplyMsg((MsgPtr) intuiMsg);
		if (class == IDCMP_MOUSEBUTTONS && code == SELECTUP)
			mouseDown = FALSE;
		else if (class != IDCMP_INTUITICKS ||
				 LibraryVersion((struct Library *) IntuitionBase) >= OSVERSION_2_0)
			mouseDown = SELECTBUTTON(modifier);
		if (class == IDCMP_MOUSEMOVE)
			return (mouseDown);			// Return immediately if IDCMP_MOUSEMOVE
	}
	return (mouseDown);
}

/*
 *	CmpString
 *	Compare to strings, ignoring capitalization if caseSense == FALSE
 *	Return -1 if s1 < s2, 0 if s1 = s2, +1 if s1 > s2
 *	Note: Cannot put in module by itself, due to name collision in rexxglue.o
 */

WORD CmpString(register TextPtr s1, register TextPtr s2, WORD l1, WORD l2, BOOL caseSense)
{
	register TextChar	ch1, ch2;
	register WORD		len;

	len = (l1 < l2) ? l1 : l2;
	if (caseSense) {
		while (len--) {
			ch1 = *s1++;
			ch2 = *s2++;
#if SWEDISH
			if (ch1 == (UBYTE) '�')
				ch1 = (UBYTE) '�';
			else if (ch1 == (UBYTE) '�')
				ch1 = (UBYTE) '�';
			if (ch2 == (UBYTE) '�')
				ch2 = (UBYTE) '�';
			else if (ch2 == (UBYTE) '�')
				ch2 = (UBYTE) '�';
#endif
			if (ch1 != ch2) {		// Only one comparison if same
				if (ch1 < ch2)
					return (-1);
				return (1);
			}
		}
	}
	else {
		while (len--) {
			ch1 = *s1++;
			ch2 = *s2++;
			if ((ch1 >= 'A' && ch1 <= 'Z') || (ch1 >= 0xC0 && ch1 <= 0xDF))
				ch1 += 0x20;
			if ((ch2 >= 'A' && ch2 <= 'Z') || (ch2 >= 0xC0 && ch2 <= 0xDF))
				ch2 += 0x20;
#if SWEDISH
			if (ch1 == (UBYTE) '�')
				ch1 = (UBYTE) '�';
			else if (ch1 == (UBYTE) '�')
				ch1 = (UBYTE) '�';
			if (ch2 == (UBYTE) '�')
				ch2 = (UBYTE) '�';
			else if (ch2 == (UBYTE) '�')
				ch2 = (UBYTE) '�';
#endif
			if (ch1 != ch2) {		// Only one comparison if same
				if (ch1 < ch2)
					return (-1);
				return (1);
			}
		}
	}
	if (l1 < l2)
		return (-1);
	else if (l1 > l2)
		return (1);
	return (0);
}

/*
 *	NumToString
 *	Convert number to string
 *	String buffer must have enough room for string and trailing NULL
 */

void NumToString(register LONG num, register TextPtr s)
{
	register LONG	divisor;

	if (num < 0) {
		*s++ = '-';
		num = -num;
	}
	for (divisor = 1; num/divisor > 9;	divisor *= 10) ;
	while  (divisor) {
		*s++ = num/divisor + '0';
		num %= divisor;
		divisor /= 10;
	}
	*s = '\0';
}

/*
 *	StringToNum
 *	Convert string to number (long)
 *	Skips initial spaces
 */

LONG StringToNum(register TextPtr s)
{
	register TextChar	ch;
	register WORD		sign;
	register LONG		num;

	while (*s == ' ')
		s++;
	if (*s == '-') {
		sign = -1;
		s++;
	}
	else
		sign = 1;
	num = 0;
	while (((ch = *s++) >= '0' && ch <= '9') || ch == ',') {
		if (ch != ',') {
			num *= 10;
			num += ch & 0x0F;
		}
	}
	if (sign == -1)
		num = -num;
	return (num);
}

/*
 *	GetFont
 *	Get specified font, either from memory or disk
 *	Included here instead of in Font.c so that programs that only use GetFont()
 *		won't include the entire Font.c module
 */

TextFontPtr GetFont(TextAttrPtr textAttr)
{
	register TextFontPtr	font;

/*
	First try calling OpenFont() (to avoid disk access if font is already present)
*/
	font = OpenFont(textAttr);
	if (font && font->tf_YSize != textAttr->ta_YSize) {
		CloseFont(font);
		font = NULL;
	}
/*
	If failed, try OpenDiskFont()
*/
	if (font == NULL && DiskfontBase) {
		font = OpenDiskFont(textAttr);
		if (font && font->tf_YSize != textAttr->ta_YSize) {
			CloseFont(font);
			font = NULL;
		}
	}
	return (font);
}

/*
 *	Draw text in specified width
 *	If text is too long, decrease intercharacter spacing and truncate at start or end
 */

void TextInWidth(register RastPtr rPort, TextPtr text, WORD len, WORD width, BOOL atStart)
{
	register WORD	drawWidth, extra;

	rPort->TxSpacing = 0;
	drawWidth = TextLength(rPort, text, len);
	if (drawWidth > width && LibraryVersion((struct Library *) GfxBase) > OSVERSION_1_2) {
		rPort->TxSpacing = -1;
		drawWidth -= len;
	}
	if (drawWidth <= width)
		Text(rPort, text, len);
	else {
		extra = TextLength(rPort, "...", 3);
		while (len && TextLength(rPort, text, len) + extra > width) {
			if (atStart)
				text++;
			len--;
		}
		if (atStart)
			Text(rPort, "...", 3);
		Text(rPort, text, len);
		if (!atStart)
			Text(rPort, "...", 3);
	}
	WaitBlit();
	rPort->TxSpacing = 0;
}

/*
 *	Get rectange containing viewable portion of screen
 *	Is here rather than in Screen.c so that programs that use it (or use GetDialog(),
 *		which calls this) won't include all of the Screen.c code
 */

void GetScreenViewRect(register ScreenPtr screen, register RectPtr rect)
{
	ULONG	modeID;

	if (LibraryVersion((struct Library *) IntuitionBase) >= OSVERSION_2_0 &&
		(modeID = GetVPModeID(&screen->ViewPort)) != INVALID_ID &&
		QueryOverscan(modeID, rect, OSCAN_TEXT)) {
		if (screen->LeftEdge < 0)
			OffsetRect(rect, -screen->LeftEdge, 0);
		if (screen->TopEdge < 0)
			OffsetRect(rect, 0, -screen->TopEdge);
		if (rect->MaxX - rect->MinX + 1 > screen->Width)
			rect->MaxX = rect->MinX + screen->Width - 1;
		if (rect->MaxY - rect->MinY + 1 > screen->Height)
			rect->MaxY = rect->MinY + screen->Height - 1;
	}
	else
		SetRect(rect, 0, 0, screen->Width - 1, screen->Height - 1);
}

/*
 *	Return pen number of closest matching color
 *	Is here rather than in ColorPick.c so that programs that use it won't include
 *		all of the ColorPick.c code (which also will include the Screen.c code)
 */

PenNum PenNumber(RGBColor rgbColor, ColorMapPtr colorMap, WORD numPens)
{
	register WORD	red, green, blue, redDiff, greenDiff, blueDiff;
	WORD			entry, diff, newDiff, penNum;
	LONG			color;

	red   = RED(rgbColor);
	green = GREEN(rgbColor);
	blue  = BLUE(rgbColor);
	diff = 0x7FFF;
	penNum = 0;
	for (entry = 0; entry < numPens; entry++) {
		color = GetRGB4(colorMap, entry);
		if ((redDiff = red - RED(color)) < 0)
			redDiff = -redDiff;
		if ((greenDiff = green - GREEN(color)) < 0)
			greenDiff = -greenDiff;
		if ((blueDiff = blue - BLUE(color)) < 0)
			blueDiff = -blueDiff;
		if ((newDiff = redDiff + greenDiff + blueDiff) < diff) {
			penNum = entry;
			diff = newDiff;
			if (diff == 0)
				break;
		}
	}
	return ((PenNum) penNum);
}
