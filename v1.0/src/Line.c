/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Line handling routines
 */

#include <exec/types.h>
#include <graphics/gfxmacros.h>
#include <intuition/intuition.h>

#include <proto/graphics.h>

#include <Toolbox/Memory.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Window.h>

#include "Draw.h"
#include "Proto.h"


/*
 *	External variables
 */

extern Defaults	defaults;

/*
 *	Local variables and definitions
 */

#define LINE_PAT	0xFFFF		/* Line pattern for object creation */

static LineObjPtr	growLineObj;
static BOOL			wasMoved;
static WORD			growHandle;

/*
 *	Local prototypes
 */

LineObjPtr	CreateLine(DocDataPtr, WORD, WORD, WORD, WORD);

void	DrawThickLine(RastPtr, Point *, Point *, WORD, WORD);

void	DrawArrow(RastPtr, LineObjPtr, BOOL, RectPtr);

void	LineTrackCreate(WindowPtr, WORD, WORD, WORD, WORD, BOOL);
void	LineConstrainCreate(WORD, WORD, WORD *, WORD *);

void	LineTrackGrow(WindowPtr, WORD, WORD, WORD, WORD, BOOL);

/*
 *	Allocate a new line object
 */

LineObjPtr LineAllocate()
{
	return ((LineObjPtr) MemAlloc(sizeof(LineObj), MEMF_CLEAR));
}

/*
 *	Dispose of line object
 */

void LineDispose(LineObjPtr lineObj)
{
	MemFree(lineObj, sizeof(LineObj));
}

/*
 *	Create line with given start and end positions
 */

static LineObjPtr CreateLine(DocDataPtr docData, WORD xStart, WORD yStart, WORD xEnd, WORD yEnd)
{
	register LineObjPtr lineObj;

	if ((lineObj = (LineObjPtr) NewDocObject(CurrLayer(docData), TYPE_LINE)) == NULL)
		return (NULL);
	SetFrameRect(&lineObj->DocObj.Frame, xStart, yStart, xEnd, yEnd);
	lineObj->DocObj.Flags	= defaults.ObjFlags;
	lineObj->LineFlags		= defaults.LineFlags;
	lineObj->PenWidth		= defaults.PenWidth;
	lineObj->PenHeight		= defaults.PenHeight;
	lineObj->PenColor		= defaults.PenColor;
	lineObj->Start.x		= xStart - lineObj->DocObj.Frame.MinX;
	lineObj->Start.y		= yStart - lineObj->DocObj.Frame.MinY;
	lineObj->End.x			= xEnd - lineObj->DocObj.Frame.MinX;
	lineObj->End.y			= yEnd - lineObj->DocObj.Frame.MinY;
	return (lineObj);
}

/*
 *	Draw line with given thickness
 */

static void DrawThickLine(RastPtr rPort, Point *start, Point *end, WORD width, WORD height)
{
	register WORD i, j;

	for (i = 0; i < width; i++) {
		for (j = 0; j < height; j++) {
			if (i != 0 && j != 0 && i != width - 1 && j != height - 1)
				continue;
			Move(rPort, start->x + i - width/2, start->y + j - height/2);
			Draw(rPort, end->x   + i - width/2, end->y   + j - height/2);
		}
	}
}

/*
 *	Draw arrow at start or end
 */

static void DrawArrow(RastPtr rPort, LineObjPtr lineObj, BOOL atEnd, RectPtr rect)
{
	WORD dx, dy, r, len;
	Point ptList[3];

	if (atEnd) {
		ptList[0] = lineObj->End;
		dx = lineObj->Start.x - lineObj->End.x;
		dy = lineObj->Start.y - lineObj->End.y;
	}
	else {
		ptList[0] = lineObj->Start;
		dx = lineObj->End.x - lineObj->Start.x;
		dy = lineObj->End.y - lineObj->Start.y;
	}
	OffsetPoint(&ptList[0], lineObj->DocObj.Frame.MinX, lineObj->DocObj.Frame.MinY);
	r = LineLength(&lineObj->Start, &lineObj->End);
	if (r == 0)
		return;
	len = 4 + (lineObj->PenWidth + lineObj->PenHeight)/2;
	ptList[1].x = ptList[0].x + len*(dx*2 + dy)/r;
	ptList[1].y = ptList[0].y + len*(dy*2 - dx)/r;
	ptList[2].x = ptList[0].x + len*(dx*2 - dy)/r;
	ptList[2].y = ptList[0].y + len*(dy*2 + dx)/r;
	MapPt(&ptList[0], &lineObj->DocObj.Frame, rect);
	MapPt(&ptList[1], &lineObj->DocObj.Frame, rect);
	MapPt(&ptList[2], &lineObj->DocObj.Frame, rect);
	FillPoly(rPort, 3, ptList, NULL);
}

/*
 *	Draw line object
 */

void LineDrawObj(RastPtr rPort, LineObjPtr lineObj, RectPtr rect, RectPtr clipRect)
{
	Point start, end, pen;

	if (HasPen(lineObj)) {
		pen.x = lineObj->PenWidth;
		pen.y = lineObj->PenHeight;
		start = lineObj->Start;
		end = lineObj->End;
		ScalePt(&pen, &lineObj->DocObj.Frame, rect);
		OffsetPoint(&start, lineObj->DocObj.Frame.MinX, lineObj->DocObj.Frame.MinY);
		start.x = (start.x == lineObj->DocObj.Frame.MinX) ? rect->MinX : rect->MaxX;
		start.y = (start.y == lineObj->DocObj.Frame.MinY) ? rect->MinY : rect->MaxY;
		OffsetPoint(&end, lineObj->DocObj.Frame.MinX, lineObj->DocObj.Frame.MinY);
		end.x = (end.x == lineObj->DocObj.Frame.MinX) ? rect->MinX : rect->MaxX;
		end.y = (end.y == lineObj->DocObj.Frame.MinY) ? rect->MinY : rect->MaxY;
		PenNormal(rPort);
		RGBForeColor(rPort, lineObj->PenColor);
		DrawThickLine(rPort, &start, &end, pen.x, pen.y);
		if (lineObj->LineFlags & LINE_ARROWSTART)
			DrawArrow(rPort, lineObj, FALSE, rect);
		if (lineObj->LineFlags & LINE_ARROWEND)
			DrawArrow(rPort, lineObj, TRUE, rect);
	}
}

/*
 *	Draw line outline (for creation/dragging)
 *	Offset is in document coordinates
 */

void LineDrawOutline(WindowPtr window, LineObjPtr lineObj, WORD xOffset, WORD yOffset)
{
	RastPtr rPort = window->RPort;
	Point start, end;

	start = lineObj->Start;
	end = lineObj->End;
	OffsetPoint(&start, (WORD) (xOffset + lineObj->DocObj.Frame.MinX),
				(WORD) (yOffset + lineObj->DocObj.Frame.MinY));
	OffsetPoint(&end, (WORD) (xOffset + lineObj->DocObj.Frame.MinX),
				(WORD) (yOffset + lineObj->DocObj.Frame.MinY));
	DocToWindow(window, start.x, start.y, &start.x, &start.y);
	DocToWindow(window, end.x, end.y, &end.x, &end.y);
	SetDrMd(rPort, COMPLEMENT);
	SetDrPt(rPort, LINE_PAT);
	Move(rPort, start.x, start.y);
	Draw(rPort, end.x, end.y);
}

/*
 *	Draw selection hilighting for line
 */

void LineHilite(WindowPtr window, LineObjPtr lineObj)
{
	RastPtr rPort = window->RPort;
	Point start, end;

	start = lineObj->Start;
	end = lineObj->End;
	OffsetPoint(&start, lineObj->DocObj.Frame.MinX, lineObj->DocObj.Frame.MinY);
	OffsetPoint(&end, lineObj->DocObj.Frame.MinX, lineObj->DocObj.Frame.MinY);
	DocToWindow(window, start.x, start.y, &start.x, &start.y);
	DocToWindow(window, end.x, end.y, &end.x, &end.y);
	DrawHandle(rPort, start.x, start.y);
	DrawHandle(rPort, end.x, end.y);
}

/*
 *	Set line pen color
 */

void LineSetPenColor(LineObjPtr lineObj, RGBColor penColor)
{
	lineObj->PenColor = penColor;
}

/*
 *	Set line pen size
 *	If size is -1 then do not change
 */

void LineSetPenSize(LineObjPtr lineObj, WORD penWidth, WORD penHeight)
{
	if (penWidth != -1)
		lineObj->PenWidth = penWidth;
	if (penHeight != -1)
		lineObj->PenHeight = penHeight;
}

/*
 *	Set line fill pattern
 */

void LineSetFillPat(LineObjPtr lineObj, RGBPat8Ptr fillPat)
{
}

/*
 *	Rotate line by given angle
 */

void LineRotate(LineObjPtr lineObj, WORD cx, WORD cy, WORD angle)
{
	WORD xOffset, yOffset;

	xOffset = lineObj->DocObj.Frame.MinX;
	yOffset = lineObj->DocObj.Frame.MinY;
	OffsetPoint(&lineObj->Start, xOffset, yOffset);
	OffsetPoint(&lineObj->End, xOffset, yOffset);
	RotateRect(&lineObj->DocObj.Frame, cx, cy, angle);
	RotatePoint(&lineObj->Start, cx, cy, angle);
	RotatePoint(&lineObj->End, cx, cy, angle);
	xOffset = lineObj->DocObj.Frame.MinX;
	yOffset = lineObj->DocObj.Frame.MinY;
	OffsetPoint(&lineObj->Start, (WORD) -xOffset, (WORD) -yOffset);
	OffsetPoint(&lineObj->End, (WORD) -xOffset, (WORD) -yOffset);
}

/*
 *	Flip line horizontally or vertically
 */

void LineFlip(LineObjPtr lineObj, WORD cx, WORD cy, BOOL horiz)
{
	WORD xOffset, yOffset;

	xOffset = lineObj->DocObj.Frame.MinX;
	yOffset = lineObj->DocObj.Frame.MinY;
	OffsetPoint(&lineObj->Start, xOffset, yOffset);
	OffsetPoint(&lineObj->End, xOffset, yOffset);
	FlipRect(&lineObj->DocObj.Frame, cx, cy, horiz);
	FlipPoint(&lineObj->Start, cx, cy, horiz);
	FlipPoint(&lineObj->End, cx, cy, horiz);
	xOffset = lineObj->DocObj.Frame.MinX;
	yOffset = lineObj->DocObj.Frame.MinY;
	OffsetPoint(&lineObj->Start, (WORD) -xOffset, (WORD) -yOffset);
	OffsetPoint(&lineObj->End, (WORD) -xOffset, (WORD) -yOffset);
}

/*
 *	Scale line to new frame rectangle
 */

void LineScale(LineObjPtr lineObj, RectPtr frame)
{
	OffsetPoint(&lineObj->Start, lineObj->DocObj.Frame.MinX, lineObj->DocObj.Frame.MinY);
	OffsetPoint(&lineObj->End, lineObj->DocObj.Frame.MinX, lineObj->DocObj.Frame.MinY);
	MapPt(&lineObj->Start, &lineObj->DocObj.Frame, frame);
	MapPt(&lineObj->End, &lineObj->DocObj.Frame, frame);
	OffsetPoint(&lineObj->Start, (WORD) -frame->MinX, (WORD) -frame->MinY);
	OffsetPoint(&lineObj->End, (WORD) -frame->MinX, (WORD) -frame->MinY);
	lineObj->DocObj.Frame = *frame;
}

/*
 *	Create poly equivalent to given line
 *	Returns pointer to poly, or NULL if error
 */

PolyObjPtr LineConvertToPoly(LineObjPtr lineObj)
{
	PolyObjPtr polyObj;

	if ((polyObj = (PolyObjPtr) NewDocObject(NULL, TYPE_POLY)) == NULL)
		return (NULL);
	polyObj->DocObj.Frame = lineObj->DocObj.Frame;
	if (!PolyAddPoint(polyObj, 0x7FFF, lineObj->Start.x, lineObj->Start.y) ||
		!PolyAddPoint(polyObj, 0x7FFF, lineObj->End.x, lineObj->End.y)) {
		DisposeDocObject((DocObjPtr) polyObj);
		return (NULL);
	}
	PolyAdjustFrame(polyObj);
	PolySetPenColor(polyObj, lineObj->PenColor);
	PolySetPenSize(polyObj, lineObj->PenWidth, lineObj->PenHeight);
	PolySetFillPat(polyObj, &defaults.FillPat);
	polyObj->DocObj.Flags = lineObj->DocObj.Flags;
	PolySetSmooth(polyObj, FALSE);
	PolySetClosed(polyObj, FALSE);
	return (polyObj);
}

/*
 *	Determine if point is in line
 *	Point is relative to object rectangle
 */

BOOL LineSelect(LineObjPtr lineObj, Point *pt)
{
	if (!HasPen(lineObj))
		return (FALSE);
	return (PtNearLine(pt, &lineObj->Start, &lineObj->End,
					   (WORD) lineObj->PenWidth, (WORD) lineObj->PenHeight, 2));
}

/*
 *	Return handle number of given point, or -1 if not in a handle
 *	Point is relative to object rectangle
 */

WORD LineHandle(LineObjPtr lineObj, PointPtr pt, RectPtr handleRect)
{
	if (InHandle(pt, &lineObj->Start, handleRect))
		return (0);
	if (InHandle(pt, &lineObj->End, handleRect))
		return (1);
	return (-1);
}

/*
 *	Duplicate line data to new object
 *	Return success status
 */

BOOL LineDupData(LineObjPtr lineObj, LineObjPtr newObj)
{
	newObj->LineFlags	= lineObj->LineFlags;
	newObj->Start		= lineObj->Start;
	newObj->End			= lineObj->End;
	newObj->PenWidth	= lineObj->PenWidth;
	newObj->PenHeight	= lineObj->PenHeight;
	newObj->PenColor	= lineObj->PenColor;
	return (TRUE);
}

/*
 *	Set line arrows on or off
 *	1 = on, 0 = off, -1 = don't change
 */

void LineSetArrows(LineObjPtr lineObj, WORD atStart, WORD atEnd)
{
	if (atStart == 1)
		lineObj->LineFlags |= LINE_ARROWSTART;
	else if (atStart == 0)
		lineObj->LineFlags &= ~LINE_ARROWSTART;
	if (atEnd == 1)
		lineObj->LineFlags |= LINE_ARROWEND;
	else if (atEnd == 0)
		lineObj->LineFlags &= ~LINE_ARROWEND;
}

/*
 *	Draw routine for tracking line creation
 */

static void LineTrackCreate(WindowPtr window, WORD xStart, WORD yStart, WORD xEnd, WORD yEnd,
							BOOL change)
{
	LineObj lineObj;

	if (change) {
		BlockClear(&lineObj, sizeof(LineObj));
		SetFrameRect(&lineObj.DocObj.Frame, xStart, yStart, xEnd, yEnd);
		lineObj.Start.x = xStart - lineObj.DocObj.Frame.MinX;
		lineObj.Start.y = yStart - lineObj.DocObj.Frame.MinY;
		lineObj.End.x = xEnd - lineObj.DocObj.Frame.MinX;
		lineObj.End.y = yEnd - lineObj.DocObj.Frame.MinY;
		LineDrawOutline(window, &lineObj, 0, 0);
	}
}

/*
 *	Constrain routine for line creation
 */

static void LineConstrainCreate(WORD xStart, WORD yStart, WORD *xEnd, WORD *yEnd)
{
	WORD width, height;

	width = ABS(*xEnd - xStart);
	height = ABS(*yEnd - yStart);
	if (width < height/2)
		width = 0;
	else if (height < width/2)
		height = 0;
	else
		height = width = MIN(width, height);
	if (*xEnd >= xStart)
		*xEnd = xStart + width;
	else
		*xEnd = xStart - width;
	if (*yEnd >= yStart)
		*yEnd = yStart + height;
	else
		*yEnd = yStart - height;
}

/*
 *	Create line object
 */

DocObjPtr LineCreate(WindowPtr window, UWORD modifier, WORD mouseX, WORD mouseY)
{
	WORD xStart, yStart, xEnd, yEnd;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);

/*
	Track object size
*/
	WindowToDoc(window, mouseX, mouseY, &xStart, &yStart);
	SnapToGrid(docData, &xStart, &yStart);
	xEnd = xStart;
	yEnd = yStart;
	TrackMouse(window, modifier, &xEnd, &yEnd, LineTrackCreate, LineConstrainCreate);
/*
	Create line
*/
	return ((DocObjPtr) CreateLine(docData, xStart, yStart, xEnd, yEnd));
}

/*
 *	Draw routine for tracking line grow
 */

static void LineTrackGrow(WindowPtr window, WORD xStart, WORD yStart, WORD xEnd, WORD yEnd,
						  BOOL change)
{
	LineObj lineObj;

	if (change && (xStart != xEnd || yStart != yEnd || wasMoved)) {
		BlockClear(&lineObj, sizeof(LineObj));
		if (growHandle == 0) {
			xStart = xEnd;
			yStart = yEnd;
			xEnd = growLineObj->DocObj.Frame.MinX + growLineObj->End.x;
			yEnd = growLineObj->DocObj.Frame.MinY + growLineObj->End.y;
		}
		else {
			xStart = growLineObj->DocObj.Frame.MinX + growLineObj->Start.x;
			yStart = growLineObj->DocObj.Frame.MinY + growLineObj->Start.y;
		}
		SetFrameRect(&lineObj.DocObj.Frame, xStart, yStart, xEnd, yEnd);
		lineObj.Start.x = xStart - lineObj.DocObj.Frame.MinX;
		lineObj.Start.y = yStart - lineObj.DocObj.Frame.MinY;
		lineObj.End.x = xEnd - lineObj.DocObj.Frame.MinX;
		lineObj.End.y = yEnd - lineObj.DocObj.Frame.MinY;
		LineDrawOutline(window, &lineObj, 0, 0);
		wasMoved = TRUE;
	}
}

/*
 *	Track mouse and change line shape
 */

void LineGrow(WindowPtr window, LineObjPtr lineObj, UWORD modifier, WORD mouseX, WORD mouseY)
{
	WORD xStart, yStart, xEnd, yEnd;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
	Point pt;
	Rectangle handleRect;

	growLineObj = lineObj;
	GetHandleRect(window, &handleRect);
/*
	Track handle movement
*/
	wasMoved = FALSE;
	WindowToDoc(window, mouseX, mouseY, &xStart, &yStart);
	pt.x = xStart - lineObj->DocObj.Frame.MinX;
	pt.y = yStart - lineObj->DocObj.Frame.MinY;
	if ((growHandle = LineHandle(lineObj, &pt, &handleRect)) == -1)
		return;
	SnapToGrid(docData, &xStart, &yStart);
	xEnd = xStart;
	yEnd = yStart;
	TrackMouse(window, modifier, &xEnd, &yEnd, LineTrackGrow, NULL);
/*
	Set new object dimensions
*/
	if (xEnd != xStart || yEnd != yStart) {
		InvalObjectRect(window, (DocObjPtr) lineObj);
		HiliteSelectOff(window);
		if (growHandle == 0) {
			xStart = xEnd;
			yStart = yEnd;
			xEnd = lineObj->DocObj.Frame.MinX + lineObj->End.x;
			yEnd = lineObj->DocObj.Frame.MinY + lineObj->End.y;
		}
		else {
			xStart = lineObj->DocObj.Frame.MinX + lineObj->Start.x;
			yStart = lineObj->DocObj.Frame.MinY + lineObj->Start.y;
		}
		InvalObjectRect(window, (DocObjPtr) lineObj);
		SetFrameRect(&lineObj->DocObj.Frame, xStart, yStart, xEnd, yEnd);
		lineObj->Start.x = xStart - lineObj->DocObj.Frame.MinX;
		lineObj->Start.y = yStart - lineObj->DocObj.Frame.MinY;
		lineObj->End.x   = xEnd - lineObj->DocObj.Frame.MinX;
		lineObj->End.y   = yEnd - lineObj->DocObj.Frame.MinY;
		HiliteSelectOn(window);
		InvalObjectRect(window, (DocObjPtr) lineObj);
	}
}

/*
 *	Create new line given REXX arguments
 */

LineObjPtr LineNewREXX(DocDataPtr docData, TextPtr args)
{
	Point pt1, pt2;

/*
	Get start and end points
*/
	args = GetArgPoint(args, docData, &pt1);
	if (args)
		args = GetArgPoint(args, docData, &pt2);
	if (args == NULL ||
		pt1.x < 0 || pt1.y < 0 || pt1.x >= docData->DocWidth || pt1.y >= docData->DocHeight ||
		pt2.x < 0 || pt2.y < 0 || pt2.x >= docData->DocWidth || pt2.y >= docData->DocHeight)
		return (NULL);
/*
	Create line
*/
	return (CreateLine(docData, pt1.x, pt1.y, pt2.x, pt2.y));
}
