/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Screen routines
 */

#include <exec/types.h>
#include <graphics/displayinfo.h>
#include <intuition/intuition.h>
#include <intuition/intuitionbase.h>
#include <workbench/workbench.h>

#include <proto/graphics.h>
#include <proto/intuition.h>
#include <proto/icon.h>

#include <string.h>

#include <Toolbox/Window.h>
#include <Toolbox/Utility.h>

#include "Draw.h"
#include "Proto.h"

/*
 *	External variables
 */

extern WORD	intuiVersion;

extern struct WBStartup	*WBenchMsg;

extern TextChar	progPathName[];

extern struct NewScreen	newScreen;

extern ScreenPtr	screen;
extern WindowPtr	windowList[];
extern WORD			numWindows;

extern BOOL	onPubScreen, smartWindows;
extern WORD	xAspectShift, yAspectShift;

extern UBYTE	blackColor, whiteColor, darkColor, lightColor;

extern ColorTable		stdColors, screenColors;
extern InvColorTable	scrnInvColorTable;

extern TextChar	strScreenName[];

extern TextAttr		screenAttr, smallAttr;
extern TextFontPtr	screenFont, smallFont;

extern Options	options;

/*
 *	Local variables and definitions
 */

static ULONG	displayID;			/* OS 2.0 DisplayID */

static TextAttr screenAttr = {
	NULL, 11, FS_NORMAL, FPF_ROMFONT | FPF_DISKFONT | FPF_DESIGNED
};

static TextAttr smallAttr = {
	NULL, 8, FS_NORMAL, FPF_ROMFONT | FPF_DISKFONT | FPF_DESIGNED
};

static TextChar topazFontName[] = "topaz.font";
static TextChar	systemFontName[] = "System.font";
static TextChar	systemThinFontName[] = "SystemThin.font";

/*
 *	Color register numbers for standard colors
 *	Requirements are for:
 *		Blue is color 0
 *		White is color 1
 *		Black is inverse of white (ie color 6)
 *		Red is inverse of blue (ie color 7)
 */
 
enum {
	COLOR_BLUE,		COLOR_WHITE,	COLOR_GREEN,	COLOR_CYAN,
	COLOR_DKRED,	COLOR_DKYELLOW,	COLOR_DKGREEN,	COLOR_DKCYAN,
	COLOR_DKBLUE,	COLOR_DKMAGENTA,COLOR_LTGRAY,	COLOR_DKGRAY,
	COLOR_YELLOW,	COLOR_MAGENTA,	COLOR_BLACK,	COLOR_RED
};

static UWORD	newLookPens[] = {
	COLOR_BLUE, COLOR_WHITE,
	COLOR_BLACK, COLOR_WHITE, COLOR_BLACK, COLOR_CYAN, COLOR_BLACK,
	COLOR_BLUE, COLOR_WHITE,
	~0
};

#define DARK(color)	RGBCOLOR((RED(color)+1)/2,(GREEN(color)+1)/2,(BLUE(color)+1)/2)

/*
 *	Local prototypes
 */

void	BuildDefaultColorTable(void);

ScreenPtr	GetPublicScreen(TextPtr);

BOOL	MatchOptions(TextPtr, TextPtr, TextPtr);

void	DoScreenOption(struct NewScreen *, TextPtr);
BOOL	SetScreenData(struct NewScreen *, int, char **);
void	SetScreenColors(void);

ScreenPtr	GetNewScreen(struct NewScreen *);

/*
 *	Build default screen color tables
 */

static void BuildDefaultColorTable()
{
	stdColors[COLOR_BLACK]		= RGBCOLOR_BLACK;
	stdColors[COLOR_WHITE]		= RGBCOLOR_WHITE;
	stdColors[COLOR_YELLOW]		= options.PrtYellow;
	stdColors[COLOR_CYAN]		= options.PrtCyan;
	stdColors[COLOR_MAGENTA]	= options.PrtMagenta;
	stdColors[COLOR_RED]		= MixColors2(options.PrtMagenta, options.PrtYellow);
	stdColors[COLOR_GREEN]		= MixColors2(options.PrtCyan, options.PrtYellow);
	stdColors[COLOR_BLUE]		= MixColors2(options.PrtCyan, options.PrtMagenta);
	stdColors[COLOR_DKRED]		= DARK(stdColors[COLOR_RED]);
	stdColors[COLOR_DKYELLOW]	= DARK(stdColors[COLOR_YELLOW]);
	stdColors[COLOR_DKGREEN]	= DARK(stdColors[COLOR_GREEN]);
	stdColors[COLOR_DKCYAN]		= DARK(stdColors[COLOR_CYAN]);
	stdColors[COLOR_DKBLUE]		= DARK(stdColors[COLOR_BLUE]);
	stdColors[COLOR_DKMAGENTA]	= DARK(stdColors[COLOR_MAGENTA]);
	stdColors[COLOR_LTGRAY]		= RGBCOLOR(10, 10, 10);
	stdColors[COLOR_DKGRAY]		= RGBCOLOR( 5,  5,  5);
}

/*
 *	Get screen color table
 *	Return number of colors in table
 */

WORD GetColorTable(ScreenPtr scrn, ColorTablePtr colorTable)
{
	WORD i, numColors;

	numColors = 1 << scrn->BitMap.Depth;
	for (i = 0; i < numColors; i++)
		(*colorTable)[i] = GetRGB4(scrn->ViewPort.ColorMap, i);
	return (numColors);
}

/*
 *	Check current screen color table and adapt to any changes
 *	Assumes screen color depth does change
 */

void CheckColorTable()
{
	WORD i, numColors;
	BOOL changed;
	Rectangle rect;
	RGBColor colorTable[256];

	if (screen->Flags & BEEPING)
		return;
/*
	Check to see if color table has changed, and copy new color table
*/
	numColors = GetColorTable(screen, &colorTable);
	changed = FALSE;
	for (i = 0; i < numColors; i++) {
		if (colorTable[i] != screenColors[i]) {
			screenColors[i] = colorTable[i];	/* Save new value */
			changed = TRUE;
		}
	}
/*
	If changed, generate new inverse look-up table, and redraw all windows
*/
	if (changed) {
		MakeInvColorTable(&screenColors, numColors, &scrnInvColorTable);
		for (i = 0; i < numWindows; i++) {
			GetWindowRect(windowList[i], &rect);
			InvalRect(windowList[i], &rect);
		}
		DrawToolWindow();
		DrawPenWindow();
		DrawFillWindow();
	}
}

/*
 *	Return pointer to specified public screen
 *	If not running under 2.0 then only NULL is valid for screen name
 */

static ScreenPtr GetPublicScreen(TextPtr scrnName)
{
	LONG intuiLock;
	register ScreenPtr pubScreen;

	if (intuiVersion >= OSVERSION_2_0)
		pubScreen = LockPubScreen(scrnName);
	else if (scrnName == NULL) {
		intuiLock = LockIBase(0);
		for (pubScreen = IntuitionBase->FirstScreen; pubScreen; pubScreen = pubScreen->NextScreen) {
			if ((pubScreen->Flags & SCREENTYPE) == WBENCHSCREEN)
				break;
		}
		UnlockIBase(intuiLock);
	}
	else
		pubScreen = NULL;
	return (pubScreen);
}

/*
 *	Match screen options
 */

static BOOL MatchOptions(TextPtr text, TextPtr opt1, TextPtr opt2)
{
	WORD len;

/*
	Get command length
*/
	for (len = 0; text[len] && text[len] != ':' && text[len] != '='; len++) ;
	if (text[len])
		len++;
/*
	Check for match
*/
	return ((BOOL) (CmpString(text, opt1, len, strlen(opt1), FALSE) == 0 ||
					CmpString(text, opt2, len, strlen(opt2), FALSE) == 0));
}

/*
 *	Set screen mode from given option
 *	If public screen requested, get lock on public screen
 */

static void DoScreenOption(struct NewScreen *newScrn, TextPtr text)
{
	TextPtr param;
	LONG num;

/*
	Get pointer to command param (if any)
*/
	for (param = text; *param && *param != ':' && *param != '='; param++) ;
	if (*param)
		param++;
/*
	Check for non-screen related params
*/
	if (MatchOptions(text, "smartwindows", "sw")) {
		smartWindows = TRUE;
		return;
	}
/*
	If already have public screen, ignore all other screen commands
*/
	if (onPubScreen)
		return;
/*
	Check to open on workbench screen
*/
	if (MatchOptions(text, "workbench", "wb")) {
		screen = GetPublicScreen(NULL);
		if (screen != NULL)
			onPubScreen = TRUE;
		return;
	}
/*
	Check for other public screen request
*/
	if (MatchOptions(text, "screen:", "screen=")) {
		if (intuiVersion < OSVERSION_2_0)
			return;
		screen = GetPublicScreen(param);
		if (screen != NULL)
			onPubScreen = TRUE;
		return;
	}
/*
	Check for screen depth command
*/
	if (MatchOptions(text, "colors:", "colors=") ||
		MatchOptions(text, "colours:", "colours=")) {
		num = StringToNum(param);
		if (num <= 0 || num > 256)
			return;
		newScrn->Depth = 1;
		while (num > 2) {
			newScrn->Depth += 1;
			num >>= 1;
		}
		return;
	}
/*
	Check for other screen settings
*/
	if (MatchOptions(text, "productivity", "pr") && intuiVersion >= OSVERSION_2_0) {
		newScrn->ViewModes = (HIRES | LACE);
		displayID = VGAPRODUCT_KEY;
		return;
	}
	if (MatchOptions(text, "superhires", "sr") && intuiVersion >= OSVERSION_2_0) {
		newScrn->ViewModes = (HIRES | LACE);
		displayID = SUPERLACE_KEY;
		return;
	}
	if (MatchOptions(text, "hires", "hr")) {
		newScrn->ViewModes = (HIRES | LACE);
		displayID = HIRESLACE_KEY;
		return;
	}
	if (MatchOptions(text, "medres", "mr")) {
		newScrn->ViewModes = HIRES;
		displayID = HIRES_KEY;
		return;
	}
	if (MatchOptions(text, "lores", "lr")) {
		newScrn->ViewModes = 0;
		displayID = LORES_KEY;
		return;
	}
}

/*
 *	Set screen data
 *	If onPubScreen is set by this routine, then screen is set to public screen
 *		and newScrn settings are undefined
 */

static BOOL SetScreenData(struct NewScreen *newScrn, int argc, char *argv[])
{
	register WORD i;
	UWORD viewModes;
	BOOL highRes, interlace;
	register TextPtr text;
	struct DiskObject *icon;
	ScreenPtr pubScreen;
	Screen screenData;

/*
	Set default mode from Workbench screen settings
*/
	onPubScreen = FALSE;
	if (intuiVersion >= OSVERSION_2_0) {
		if ((pubScreen = LockPubScreen(NULL)) == NULL)
			return (FALSE);
		screenData = *pubScreen;			/* Copy default screen data */
		UnlockPubScreen(NULL, pubScreen);
	}
	else {
		if (!GetScreenData((BYTE *) &screenData, sizeof(Screen), WBENCHSCREEN, NULL))
			return (FALSE);
	}
	if (screenData.ViewPort.Modes & LACE) {
		newScrn->ViewModes = (HIRES | LACE);
		displayID = HIRESLACE_KEY;
	}
	else {
		newScrn->ViewModes = HIRES;
		displayID = HIRES_KEY;
	}
	newScrn->Depth = 3;
	newScrn->DetailPen = COLOR_BLUE;
	newScrn->BlockPen = COLOR_WHITE;
/*
	Get user parameters
*/
	if (argc) {				/* Running under CLI */
		for (i = 1; i < argc; i++) {
			text = argv[i];
			if (*text++ == '-')
				DoScreenOption(newScrn, text);
		}
	}
	else if ((icon = GetDiskObject(progPathName)) != NULL) {
		if (icon->do_ToolTypes) {
			for (i = 0; (text = icon->do_ToolTypes[i]) != NULL; i++)
				DoScreenOption(newScrn, text);
		}
		FreeDiskObject(icon);
	}
/*
	Set screen dimensions
*/
	if (!onPubScreen) {
		newScrn->Height = STDSCREENHEIGHT;
		if (intuiVersion >= OSVERSION_2_0)
			newScrn->Width = STDSCREENWIDTH;
		else if (newScrn->ViewModes & HIRES)
			newScrn->Width = screenData.Width;
		else
			newScrn->Width = screenData.Width/2;
	}
/*
	Set the default screen font
*/
	viewModes = (onPubScreen) ? screen->ViewPort.Modes : newScrn->ViewModes;
	highRes   = ((viewModes & HIRES) != 0);
	interlace = ((viewModes & LACE) != 0);
	if (highRes)
		screenAttr.ta_Name = (intuiVersion >= OSVERSION_2_0) ?
							 topazFontName : systemFontName;
	else
		screenAttr.ta_Name = systemThinFontName;
	screenAttr.ta_YSize = (interlace) ? 11 : 8;
	if ((screenFont = GetFont(&screenAttr)) == NULL) {
		screenAttr.ta_Name = topazFontName;
		if ((screenFont = GetFont(&screenAttr)) == NULL) {
			screenAttr.ta_YSize = 8;		/* Can always get this font */
			screenFont = GetFont(&screenAttr);
		}
	}
	newScrn->Font = &screenAttr;
/*
	Get 8 pixel high font (used in ruler and page indicator)
*/
	smallAttr.ta_Name  = (intuiVersion >= OSVERSION_2_0) ?
						 topazFontName : systemFontName;
	smallAttr.ta_YSize = 8;
	smallFont = GetFont(&smallAttr);
	if (smallFont == NULL) {
		smallAttr.ta_Name  = topazFontName;
		smallAttr.ta_YSize = 8;
		smallFont = GetFont(&smallAttr);
	}
	return (TRUE);
}

/*
 *	Set screen colors
 */

static void SetScreenColors()
{
	WORD i, entries, numColors;
	RGBColor colors[256];

/*
	Set screen colors
*/
	if (!onPubScreen) {
		numColors = GetColorTable(screen, &colors);	/* In case screen has more than 8 colors */
		if (numColors == 2) {
			colors[0] = stdColors[0];
			colors[1] = stdColors[1];
		}
		else {
			entries = MIN(numColors, NUM_STDCOLORS);
			for (i = 0; i < entries/2; i++) {
				colors[i] = stdColors[i];
				colors[numColors - i - 1] = stdColors[NUM_STDCOLORS - i - 1];
			}
		}
		LoadRGB4(&screen->ViewPort, colors, numColors);
	}
/*
	Set up toolbox colors for "new look"
*/
	if (onPubScreen) {
		blackColor = 2;
		whiteColor = 1;
		darkColor  = 0;
		lightColor = 1;		/* Use white for light color */
	}
	else {
		blackColor = newLookPens[SHADOWPEN];
		whiteColor = newLookPens[SHINEPEN];
		darkColor  = newLookPens[BACKGROUNDPEN];
		lightColor = newLookPens[FILLPEN];
	}
/*
	SetToolboxColors(blackColor, whiteColor, darkColor, lightColor);
*/
}

/*
 *	Open new screen from newScrn parameters
 */

static ScreenPtr GetNewScreen(struct NewScreen *newScrn)
{
	ScreenPtr screen;

	if (intuiVersion >= OSVERSION_2_0) {
		screen = LockPubScreen(strScreenName);
		if (screen == NULL) {
			screen = OpenScreenTags(newScrn,
									SA_Pens, newLookPens,
									SA_DisplayID, displayID,
									SA_PubName, strScreenName,
									TAG_END);
			if (screen)
				PubScreenStatus(screen, 0);
		}
		else {			/* Public screen already exists */
			UnlockPubScreen(NULL, screen);
			screen = OpenScreenTags(newScrn,
									SA_Pens, newLookPens,
									SA_DisplayID, displayID,
									TAG_END);
		}
	}
	else
		screen = OpenScreen(newScrn);
	return (screen);
}

/*
 *	Get screen settings and open screen (or get pointer to public screen)
 */

ScreenPtr GetScreen(int argc, char *argv[])
{
	WORD i, numColors;
	UWORD viewModes;
	BOOL highRes, interlace;

	if (!SetScreenData(&newScreen, argc, argv))
		return (NULL);
	if (!onPubScreen) {
		if (newScreen.Depth == 1) {
			newLookPens[FILLPEN] = COLOR_WHITE;
			newLookPens[TEXTPEN] = COLOR_WHITE;
		}
		else if (newScreen.Depth == 2)
			newLookPens[FILLPEN] = COLOR_WHITE;
		else if (newScreen.Depth > 4) {			/* Assumes NUM_STDCOLORS is 16 */
			numColors = 1 << newScreen.Depth;
			for (i = 0; i < 9; i++) {
				if (newLookPens[i] >= NUM_STDCOLORS/2)
					newLookPens[i] = numColors - (NUM_STDCOLORS - newLookPens[i]);
			}
		}
		screen = GetNewScreen(&newScreen);
	}
	if (screen == NULL)
		return (NULL);
/*
	Set default screen colors
*/
	SetStdOptions(&options);	/* Set color correction values */
	BuildDefaultColorTable();
	SetScreenColors();
/*
	Set aspect adjustments
*/
	viewModes = screen->ViewPort.Modes;
	highRes   = ((viewModes & HIRES) != 0);
	interlace = ((viewModes & LACE) != 0);
	xAspectShift = yAspectShift = 0;
	if (!highRes)
		xAspectShift = 1;
	if (!interlace || displayID == SUPERLACE_KEY)
		yAspectShift = 1;
	InitToolbox((screen->Font->ta_YSize > 8), screen->Font);
	return (screen);
}
