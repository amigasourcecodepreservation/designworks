/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *	All rights reserved
 *
 *	Support routines for graphics boards
 */

#include <exec/types.h>
#include <intuition/intuition.h>
#include <libraries/expansion.h>
#include <libraries/configvars.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>
#include <proto/expansion.h>

#include <Toolbox/Memory.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Window.h>
#include <Toolbox/Utility.h>

#include "Draw.h"
#include "Proto.h"

/*
 *	External variables
 */

extern struct Library	*ExpansionBase;

extern ScreenPtr	screen;
extern MsgPortPtr	mainMsgPort;

extern Options	options;

extern DlgTemplPtr	dlgList[];

extern TextChar	strPrtSetup[], strPrtRender[], strDspDisplay[];

/*
 *	Local variables and definitions
 */

#define INFO_TEXT	0

/*
 *	GfxBase GDA-1 definitions
 */

#include "GfxResource.h"

static struct cfbResource	*gda1_Resource;

/*
 *	Ameristar A1600GX definitions
 */

#define MFG_AMERISTAR	1053	// Mfg number for Ameristar
#define PROD_A1600GX	20		// Product num of 1600GX

#define VIDREG	(0x100000)		// Video register base
#define FB_BASE	(0x200000)		// Frame buffer base

#define D4_HRZBR	(0x40+4)	// Horizontal blank start register
#define D4_HRZBF	(0x40+5)	// Horizontal blank end register
#define D4_VRTBR	(0x40+10)	// Vertical start register
#define D4_VRTBF	(0x40+11)	// Vertical end register

#define BT468_LOADDR	(0x80+0)	// BT468 low order address bits
#define BT468_HIADDR	(0x80+1)	// BT468 high order address bits
#define BT468_DATA		(0x80+3)	// BT468 data register

#define A1600GX_DEPTH	8

static volatile UBYTE	*a1600GX_FrameBuff;	// Pointer to frame buffer
static volatile ULONG	*a1600GX_Regs;		// Pointer to video control registers

/*
 *	Local prototypes
 */

static void	DrawObjects(RastPtr, DocDataPtr, RectPtr);

static void	GetInfo(UWORD, WORD *, WORD *, WORD *);
static void	LoadCMap(UWORD, WORD, WORD, UBYTE *, UBYTE *, UBYTE *);

static BOOL	InitFastDraw(WORD, WORD, BitMapPtr *, UBYTE **);
static void	DisposeFastDraw(BitMapPtr, UBYTE *);

static void	DoA1600GX(RastPtr, WORD, WORD, WORD);
static void	DoGDA1(RastPtr, WORD, WORD, WORD);
static BOOL	DoDisplay(UWORD, DocDataPtr);

/*
 *	Draw objects into rPort
 */

static void DrawObjects(RastPtr rPort, DocDataPtr docData, RectPtr viewRect)
{
	WORD		xOffset, yOffset;
	DocObjPtr	docObj;
	DocLayerPtr	docLayer, currLayer;
	Rectangle	objRect, clipRect;

	xOffset = viewRect->MinX;
	yOffset = viewRect->MinY;
	clipRect = *viewRect;
	OffsetRect(&clipRect, -xOffset, -yOffset);
/*
	Draw objects
*/
	ClearRast(rPort);
	currLayer = CurrLayer(docData);
	docLayer = (docData->Flags & DOC_HIDEBACKLAY) ? currLayer : BottomLayer(docData);
	for (;;) {
		if (docLayer == currLayer || LayerVisible(docLayer)) {
			for (docObj = BottomObject(docLayer); docObj; docObj = NextObj(docObj)) {
				objRect = docObj->Frame;
				OffsetRect(&objRect, -xOffset, -yOffset);
				DrawObject(rPort, docObj, &objRect, &clipRect, docData->Scale);
			}
		}
		if (docLayer == currLayer)
			break;
		docLayer = NextLayer(docLayer);
	}
}

/*
 *	Return width, height, and depth of board display (limit depth to 8)
 */

static void GetInfo(UWORD item, WORD *width, WORD *height, WORD *depth)
{
	switch (item) {
	case A1600GX_SUBITEM:
		*width  = (a1600GX_Regs[D4_HRZBF] - a1600GX_Regs[D4_HRZBR])*8;
		*height = a1600GX_Regs[D4_VRTBF] - a1600GX_Regs[D4_VRTBR];
		*depth  = A1600GX_DEPTH;
		break;
	case GDA1_SUBITEM:
		*width  = gda1_Resource->maxx;
		*height = gda1_Resource->DisplayRows;
		*depth  = gda1_Resource->BitsPerPixel;
		break;
	default:
		*width = *height = *depth = 0;
		break;
	}
	if (*depth > 8)
		*depth = 8;
}

/*
 *	Load color map: values (R[], G[], B[]) -> [start..(start+count-1)]
 */

static void LoadCMap(UWORD item, WORD start, WORD count, UBYTE *r, UBYTE *g, UBYTE *b)
{
	switch (item) {
	case A1600GX_SUBITEM:
		a1600GX_Regs[BT468_LOADDR] = start;
		a1600GX_Regs[BT468_HIADDR] = 0;
		while (count-- > 0) {
			a1600GX_Regs[BT468_DATA] = *r++;
			a1600GX_Regs[BT468_DATA] = *g++;
			a1600GX_Regs[BT468_DATA] = *b++;
		}
		break;
	case GDA1_SUBITEM:
		while (count--)
			(*gda1_Resource->cfbSetRGB)(start++, (*r++ << 16) + (*g++ << 8) + (*b++));
		break;
	}
}

/*
 *	Initialize A1600GX frame buffer and video control pointers
 */

BOOL Ameristar1600GX_Init()
{
	struct Library		*oldExpansionBase;
	struct ConfigDev	*configDev;
	UBYTE				*base;

	a1600GX_FrameBuff = NULL;
	a1600GX_Regs = NULL;
	oldExpansionBase = ExpansionBase;
	ExpansionBase = (struct Library *) OpenLibrary(EXPANSIONNAME, OSVERSION_1_3);
	if (ExpansionBase) {
		configDev = FindConfigDev((struct ConfigDev *) NULL, MFG_AMERISTAR, PROD_A1600GX);
		if (configDev) {
			base = configDev->cd_BoardAddr;
			a1600GX_FrameBuff = base +  FB_BASE;
			a1600GX_Regs = (ULONG *) (base + VIDREG);
		}
		CloseLibrary(ExpansionBase);
	}
	ExpansionBase = oldExpansionBase;
	return ((a1600GX_FrameBuff != NULL));
}

/*
 *	Initialize GDA-1 info
 */

BOOL GfxBaseGDA1_Init()
{
	gda1_Resource = (struct cfbResource *) OpenResource("cfb.resource");
	return ((gda1_Resource != NULL));
}

/*
 *	Set up for fast drawing to board, if possible
 */

static BOOL InitFastDraw(WORD width, WORD depth, BitMapPtr *tempBitMap, UBYTE **penArray)
{
	*tempBitMap = NULL;
	*penArray = NULL;
	if (LibraryVersion((struct Library *) GfxBase) >= OSVERSION_2_0 &&
		(*tempBitMap = CreateBitMap(width, 1, depth, TRUE)) != NULL) {
		if ((*penArray = MemAlloc((*tempBitMap)->BytesPerRow*8, 0)) != NULL)
			return (TRUE);
		DisposeBitMap(*tempBitMap);
		*tempBitMap = NULL;
	}
	return (FALSE);
}

/*
 *	Dispose of structures used for fast draw
 */

static void DisposeFastDraw(BitMapPtr tempBitMap, UBYTE *penArray)
{
	if (tempBitMap) {
		if (penArray)
			MemFree(penArray, tempBitMap->BytesPerRow*8);
		DisposeBitMap(tempBitMap);
	}
}

/*
 *	Copy drawing to Ameristar 1600GX board
 */

static void DoA1600GX(RastPtr rPort, WORD width, WORD height, WORD depth)
{
	volatile UBYTE	*frameBuff;
	WORD			x, y;
	BOOL			fastDraw;
	BitMapPtr		tempBitMap;
	UBYTE			*penArray;
	RastPort		tempPort;

/*
	Set up for faster drawing if possible
*/
	fastDraw = InitFastDraw(width, depth, &tempBitMap, &penArray);
	if (fastDraw) {
		tempPort = *rPort;
		tempPort.BitMap = tempBitMap;
		tempPort.Layer = NULL;
	}
/*
	Copy drawing
*/
	frameBuff = a1600GX_FrameBuff;
	if (fastDraw) {
		for (y = 0; y < height; y++) {
			ReadPixelLine8(rPort, 0, y, width, penArray, &tempPort);
			for (x = 0; x < width; x++)
				*frameBuff++ = penArray[x];
		}
	}
	else {
		for (y = 0; y < height; y++) {
			for (x = 0; x < width; x++)
				*frameBuff++ = ReadPixel(rPort, x, y);
		}
	}
/*
	Clean up
*/
	if (fastDraw)
		DisposeFastDraw(tempBitMap, penArray);
}

/*
 *	Copy drawing to GfxBase GDA-1 board
 */

static void DoGDA1(RastPtr rPort, WORD width, WORD height, WORD depth)
{
	WORD		x, y;
	BOOL		fastDraw;
	BitMapPtr	tempBitMap;
	UBYTE		*penArray;
	RastPort	tempPort;

/*
	Set up for faster drawing if possible
*/
	fastDraw = InitFastDraw(width, depth, &tempBitMap, &penArray);
	if (fastDraw) {
		tempPort = *rPort;
		tempPort.BitMap = tempBitMap;
		tempPort.Layer = NULL;
	}
/*
	Copy drawing
*/
	if (fastDraw) {
		for (y = 0; y < height; y++) {
			ReadPixelLine8(rPort, 0, y, width, penArray, &tempPort);
			for (x = 0; x < width; x++)
				(*gda1_Resource->WritePixel)(penArray[x], x, y);
		}
	}
	else {
		for (y = 0; y < height; y++) {
			for (x = 0; x < width; x++)
				(*gda1_Resource->WritePixel)(ReadPixel(rPort, x, y), x, y);
		}
	}
/*
	Clean up
*/
	if (fastDraw)
		DisposeFastDraw(tempBitMap, penArray);
}

/*
 *	Draw on display board
 *	Return success status
 */

static BOOL DoDisplay(UWORD item, DocDataPtr docData)
{
	WORD		i, width, height, depth, numColors;
	UBYTE		r, g, b;
	BOOL		success;
	DialogPtr	dlg;
	RastPtr		rPort;
	RGBColor	color;
	PalettePtr	palette;
	Rectangle	viewRect;

	rPort = NULL;
	palette = NULL;
	success = FALSE;
/*
	Initialize board
*/
	switch (item) {
	case A1600GX_SUBITEM:
		success = Ameristar1600GX_Init();
		break;
	case GDA1_SUBITEM:
		success = GfxBaseGDA1_Init();
		break;
	}
	if (success)
		GetInfo(item, &width, &height, &depth);
	if (!success || depth <= 0 || width <= 0 || height <= 0 ||
		(!AdvancedGraphics() && (width > MAX_BLIT_SIZE || height > MAX_BLIT_SIZE)))
		return (FALSE);
/*
	Display info requester
*/
	BeginWait();
	if ((dlg = GetDialog(dlgList[DLG_INFO], screen, mainMsgPort)) == NULL) {
		EndWait();
		return (FALSE);
	}
	SetStdPointer(dlg, POINTER_WAIT);
	SetGadgetItemText(dlg->FirstGadget, INFO_TEXT, dlg, NULL, strPrtSetup);
	RefreshWindows();
/*
	Create offscreen port and set up palette
*/
	if ((rPort = CreateRastPort(width, height, depth)) == NULL ||
		(palette = MemAlloc(sizeof(Palette), 0)) == NULL)
		goto Exit;
	BuildExportPalette(palette, depth);
	MakeInvColorTable(palette);
	ColorCorrectEnable(palette, options.ColorCorrect);
	SetPalette(rPort, palette);
/*
	Draw objects
*/
	SetGadgetItemText(dlg->FirstGadget, INFO_TEXT, dlg, NULL, strPrtRender);
	viewRect.MinX = docData->LeftOffset;
	viewRect.MinY = docData->TopOffset;
	viewRect.MaxX = viewRect.MinX + width - 1;
	viewRect.MaxY = viewRect.MinY + height - 1;
	DrawObjects(rPort, docData, &viewRect);
/*
	Set color palette
*/
	SetGadgetItemText(dlg->FirstGadget, INFO_TEXT, dlg, NULL, strDspDisplay);
	numColors = 1 << depth;
	for (i = 0; i < numColors; i++) {
		color = palette->ColorTable[i];
		r = RED(color);		r = (r << 4) | r;
		g = GREEN(color);	g = (g << 4) | g;
		b = BLUE(color);	b = (b << 4) | b;
		LoadCMap(item, i, 1, &r, &g, &b);
	}
/*
	Copy drawing
*/
	switch (item) {
	case A1600GX_SUBITEM:
		DoA1600GX(rPort, width, height, depth);
		break;
	case GDA1_SUBITEM:
		DoGDA1(rPort, width, height, depth);
		break;
	}
	success = TRUE;
/*
	Clean up
*/
Exit:
	if (palette)
		MemFree(palette, sizeof(Palette));
	if (rPort)
		DisposeRastPort(rPort);
	DisposeDialog(dlg);
	EndWait();
	return (success);
}

/*
 *	Display window contents on display board
 */

BOOL DoDisplayMenu(WindowPtr window, UWORD sub)
{
	BOOL		success;
	DocDataPtr	docData = GetWRefCon(window);

	success = DoDisplay(sub, docData);
	if (!success)
		Error(ERR_NO_DISPLAY);
	return (success);
}
