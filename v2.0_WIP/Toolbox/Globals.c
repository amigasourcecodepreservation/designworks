/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Toolbox library
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Global variables
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <Typedefs.h>

BOOL	_tbNoShadows;	/* TRUE if cannot do shadow rendering */

TextAttr	_tbTextAttr = {
	"topaz.font", 8, FS_NORMAL, FPF_ROMFONT
};

ScreenPtr	_tbScreen;
BOOL		_tbOnPubScreen;
BOOL		_tbSmartWindows;

WORD	_tbXSize, _tbYSize, _tbBaseline;

UBYTE	_tbPenBlack, _tbPenWhite, _tbPenDark, _tbPenLight, _tbPenRed, _tbPenMenu;

BOOL	_tbAutoActivate = TRUE;
