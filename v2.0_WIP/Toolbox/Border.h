/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Toolbox library
 *	Copyright (c) 1991 New Horizons Software
 *
 *	Border definitions
 */

#ifndef TOOLBOX_BORDER_H
#define TOOLBOX_BORDER_H

#ifndef INTUITION_INTUITION_H
#include <intuition/intuition.h>
#endif

#ifndef TYPEDEFS_H
#include <Typedefs.h>
#endif

#ifndef TOOLBOX_COLOR_H
#include <Toolbox/Color.h>
#endif

/*
 *	Standard border types
 */

#define BORDER_LINE			0		// Black line
#define BORDER_BOX			1		// Black box
#define BORDER_SHADOWLINE	2		// Shadow line
#define BORDER_SHADOWBOX	3		// Shadow box

/*
 *	Prototypes
 */

BorderPtr	NewBorder(WORD, WORD, PenNum, WORD);
void		FreeBorder(BorderPtr);
void		AppendBorder(BorderPtr, BorderPtr);

BorderPtr	LineBorder(WORD, WORD, PenNum);
BorderPtr	ShadowLine(WORD, WORD);

BorderPtr	BoxBorder(WORD, WORD, WORD, WORD);
BorderPtr	ShadowBoxBorder(WORD, WORD, WORD, BOOL);

BorderPtr	GetStdBorder(WORD, WORD, WORD);

void		DrawShadowBox(RastPtr, RectPtr, WORD, BOOL);
void		DrawStdBorder(RastPtr, WORD, RectPtr);

#endif
