/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Toolbox library
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Requester routines
 */

#define INTUI_V36_NAMES_ONLY	1

#include <exec/types.h>
#include <intuition/intuition.h>
#include <intuition/intuitionbase.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include "typedefs.h"
#include "Request.h"
#include "Utility.h"
#include "Memory.h"

/*
 *	External variables
 */

extern struct IntuitionBase	*IntuitionBase;

extern TextAttrPtr	_tbTextAttr;

extern WORD	_tbBlackColor, _tbWhiteColor;
extern WORD	_tbDarkColor, _tbLightColor;

extern BOOL	_tbAutoActivate;

/*
 *	GetRequest
 *	Process requester template, and return pointer to Requester structure
 *	If showIt flag is TRUE then show requester
 *	Return NULL if error
 */

RequestPtr GetRequest(ReqTemplPtr reqTempl, WindowPtr window, BOOL showIt)
{
	register WORD leftEdge, topEdge, width, height;
	WORD xSize, ySize;
	register RequestPtr request;
	register BorderPtr border;
	IntuiTextPtr intuiText;
	GadgetPtr gadget;
	GadgTemplPtr gadgTemplate;

	if (window == NULL)
		return (NULL);
/*
	Get font dimensions
	(Use width of "e" rather than font's XSize,
		since this works better for proportional fonts)
*/
	intuiText = NewIntuiText(0, 0, "e", FS_NORMAL, 0);
	if (intuiText == NULL)
		xSize = 8;
	else {
		xSize = IntuiTextLength(intuiText);
		FreeIntuiText(intuiText);
	}
	ySize = _tbTextAttr->ta_YSize;
/*
	Allocate requester and initialize requester location
*/
	if ((request = MemAlloc(sizeof(struct Requester), MEMF_CLEAR)) == NULL)
		return (NULL);
	InitRequester(request);
	request->Width  = width  = (reqTempl->Width*xSize)/8;
	request->Height = height = (reqTempl->Height*ySize)/11;
	if ((leftEdge = reqTempl->LeftEdge) == -1)
		leftEdge = (window->Width - window->BorderLeft - window->BorderRight - width)/2
				   + window->BorderLeft;
	else
		leftEdge = (leftEdge*xSize)/8;
	if ((topEdge = reqTempl->TopEdge) == -1)
		topEdge = (window->Height - window->BorderTop - window->BorderBottom-height)/2
				  + window->BorderTop;
	else
		topEdge = (topEdge*ySize)/11;
	if (leftEdge < window->BorderLeft)
		leftEdge = window->BorderLeft;
	if (topEdge < window->BorderTop)
		topEdge = window->BorderTop;
	request->LeftEdge = leftEdge;
	request->TopEdge  = topEdge;
	request->Flags	  = NOISYREQ;
	request->BackFill = _tbLightColor;
/*
	Set requester border
*/
#ifdef NEW_LOOK
	if ((border = ShadowBoxBorder(width, height, 1, TRUE)) == NULL) {
#else
	if ((border = ShadowBoxBorder(width, height, 0, TRUE)) == NULL) {
#endif
		DisposeRequest(request);
		return (NULL);
	}
	request->ReqBorder = border;
/*
	Create gadget structures
*/
	gadgTemplate = reqTempl->Gadgets;
	if (gadgTemplate == NULL || (gadget = GetGadgets(gadgTemplate)) == NULL) {
		DisposeRequest(request);
		return (NULL);
	}
	request->ReqGadget = gadget;
/*
	Display requester
*/
	if (showIt && ShowRequest(request, window) == FALSE) {
		DisposeRequest(request);
		return (NULL);
	}
	return (request);
}

/*
 *	ShowRequest
 *	Display requester in specified window
 *	Return TRUE if successful, FALSE if not
 *
 *	If requester has string gadgets, then activate the first one
 */

BOOL ShowRequest(RequestPtr request, WindowPtr window)
{
	register ULONG intuiLock;
	register GadgetPtr gadget;
	WindowPtr activeWindow;
	LayerPtr frontLayer;
	ScreenPtr firstScreen;

	intuiLock = LockIBase(0);
	activeWindow = IntuitionBase->ActiveWindow;
	firstScreen = IntuitionBase->FirstScreen;
	UnlockIBase(intuiLock);
	if (window != activeWindow)
		ActivateWindow(window);
	if (window->WScreen != firstScreen)
		ScreenToFront(window->WScreen);
	frontLayer = (window->FirstRequest) ? window->FirstRequest->ReqLayer :
										  window->WLayer;
	if ((window->Flags & WFLG_BACKDROP) == 0 && frontLayer->front) {
		WindowToFront(window);
		Delay(5);						/* Wait until window is in front */
	}
	if (Request(request, window) == FALSE)
		return (FALSE);
/*
	Activate first text box in requester
*/
	if (_tbAutoActivate) {
		for (gadget = request->ReqGadget; gadget; gadget = gadget->NextGadget) {
			if (GadgetType(gadget) == GADG_EDIT_TEXT)
				break;
		}
		if (gadget) {
			Delay(5);						/* Wait until request is active */
			ActivateGadget(gadget, window, request);
		}
	}
	return (TRUE);
}

/*
 *	DisposeRequest
 *	Release all memory used by requester created with GetRequest
 */

void DisposeRequest(RequestPtr request)
{
	if (request) {
		DisposeGadgets(request->ReqGadget);
		if (request->ReqBorder)
			FreeBorder(request->ReqBorder);
		MemFree(request, sizeof(struct Requester));
	}
}

/*
 *	GadgetMsgAvail
 *	Check to see if gadget message is present for given window
 *	Return TRUE if yes, FALSE if not
 */

BOOL GadgetMsgAvail(MsgPortPtr msgPort, WindowPtr window)
{
	register BOOL found;
	register IntuiMsgPtr msg, nextMsg;

	found = FALSE;
	Forbid();
	msg = (IntuiMsgPtr) msgPort->mp_MsgList.lh_Head;
	while (nextMsg = (IntuiMsgPtr) msg->ExecMessage.mn_Node.ln_Succ) {
		if ((msg->Class == IDCMP_GADGETDOWN || msg->Class == IDCMP_GADGETUP) &&
			msg->IDCMPWindow == window) {
			found = TRUE;
			break;
		}
		msg = nextMsg;
	}
	Permit();
	return (found);
}
