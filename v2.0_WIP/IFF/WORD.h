/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	IFF Form WORD structures and defines
 *	Copyright (c) 1990 New Horizons Software, Inc.
 *
 *	Permission is hereby granted to use this file in any and all
 *	applications.  Modifying the structures or defines included
 *	in this file is not permitted without written consent of
 *	New Horizons Software, Inc.
 */

#include <IFF/ILBM.h>		/* Makes use of ILBM defines */

#include <hotlinks/hotlinks.h>

#define ID_WORD		MakeID('W','O','R','D')		/* Form type */

#define ID_FONT		MakeID('F','O','N','T')		/* Chunks */
#define ID_COLR		MakeID('C','O','L','R')
#define ID_PSET		MakeID('P','S','E','T')
#define ID_PREC		MakeID('P','R','E','C')
#define ID_DOC		MakeID('D','O','C',' ')
#define ID_SECT		MakeID('S','E','C','T')
#define ID_HEAD		MakeID('H','E','A','D')
#define ID_FOOT		MakeID('F','O','O','T')
#define ID_PCTS		MakeID('P','C','T','S')
#define ID_PARA		MakeID('P','A','R','A')
#define ID_TABS		MakeID('T','A','B','S')
#define ID_PAGE		MakeID('P','A','G','E')
#define ID_COLM		MakeID('C','O','L','M')
#define ID_TEXT		MakeID('T','E','X','T')
#define ID_FSCC		MakeID('F','S','C','C')
#define ID_PINF		MakeID('P','I','N','F')
#define ID_BMAP		MakeID('B','M','A','P')
#define ID_INFO		MakeID('I','N','F','O')
#define ID_PNAM		MakeID('P','N','A','M')
#define ID_LINK		MakeID('L','I','N','K')

/*
 *	Special text characters for page number, date, and time
 */

#define PAGENUM_CHAR	0x80
#define DATE_CHAR		0x81
#define TIME_CHAR		0x82
#define PAGECOUNT_CHAR	0x83

/*
 *	Chunk structures follow
 */

/*
 *	FONT - Font name/number table
 *	There are one of these for each font/size combination
 *	These chunks should appear at the top of the file (before document data)
 *	It is necessary to save chunks only for fonts actually used in document
 *	If no FONT chunks appear, then default font is assumed for entire document
 */

typedef struct {
	UBYTE	FontNumLo;			/* Low and high bytes of font num */
	UBYTE	FontNumHi;
	UWORD	Size;
/*	UBYTE	Name[];	*/		/* NULL terminated, without ".font" */
} FONT;

/*
 *	COLR - Color translation table
 *	Translates from color numbers used in file to ISO color numbers
 *	Should be at top of file (before document data)
 *	Note:  Currently ProWrite only checks these values to be its current map,
 *		it does no translation as it does for FONT chunks
 */

typedef struct {
	UBYTE	ISOColors[8];
} COLR;

/*
 *	PSET - Page setup information
 *	For non-custom size, program may ignore values in PageWidth, PageHeight
 */

#define PAGESIZE_CUSTOM		0
#define PAGESIZE_USLETTER	1
#define PAGESIZE_USLEGAL	2
#define PAGESIZE_A4LETTER	3
#define PAGESIZE_COMPPAPER	4

#define ORIENT_TALLADJ	0
#define ORIENT_TALL		1
#define ORIENT_WIDEADJ	2
#define ORIENT_WIDE		3

typedef struct {
	UBYTE	PageSize;
	UBYTE	Orientation;
	LONG	PageWidth, PageHeight;	/* Actual paper size, in decipoints */
	WORD	TopMargin, BottomMargin;/* In decipoints minus 360 */
} PSET;

/*
 *	PREC - Print record
 *	A ProWrite print record
 *	This information supercedes the PSET info, and should come after it
 *	(The PSET chunk should be saved only for backwards compatibility)
 */

typedef struct {
	UBYTE	PrintRec[120];
} PREC;

/*
 *	DOC  - Begin document section
 *	All text and paragraph formatting following this chunk and up to a
 *	HEAD, FOOT, or PICT chunk belong to the document section
 */

#define PAGESTYLE_1	0		/* 1, 2, 3 */
#define PAGESTYLE_I	1		/* I, II, III */
#define PAGESTYLE_i	2		/* i, ii, iii */
#define PAGESTYLE_A	3		/* A, B, C */
#define PAGESTYLE_a	4		/* a, b, c */

#define DATESTYLE_SHORT		0
#define DATESTYLE_ABBR		1
#define DATESTYLE_LONG		2
#define DATESTYLE_ABBRDAY	3
#define DATESTYLE_LONGDAY	4
#define DATESTYLE_MILITARY	5

#define TIMESTYLE_12HR		0
#define TIMESTYLE_24HR		1

typedef struct {
	UWORD	StartPage;		/* Starting page number */
	UBYTE	PageNumStyle;	/* From defines above */
	UBYTE	DateStyle;
	UBYTE	TimeStyle;
	UBYTE	TabChar;
	WORD	pad;
} DOC;

/*
 *	SECT - Begin new section
 *	This information supercedes the DOC info, and should come after it
 *	(The DOC chunk should be saved only for backwards compatibility)
 *	Note: ProWrite currently only supports one section per document
 */

#define COLUMN_SNAKING		0
#define COLUMN_SIDEBYSIDE	1

typedef struct {
	UWORD	StartPage;
	UBYTE	PageNumStyle, DateStyle, TimeStyle, TabChar;
	UBYTE	NumColumns, ColumnType;
	UWORD	TopMargin, BottomMargin;	/* Margins in decipoints */
	UWORD	LeftMargin, RightMargin, BindingMargin;
	UWORD	ColumnGap;
	UBYTE	pad[100];					/* For future expansion */
} SECT;

/*
 *	HEAD/FOOT - Begin header/footer section
 *	All text and paragraph formatting following this chunk and up to a
 *	DOC, HEAD, FOOT, or PICT chunk belong to this header/footer
 *	For odd pages, ShowIt flag specifies whether header/footers are shown
 *	For even pages, ShowIt flag specifies whether double sided is enabled
 *	Old format had ShowIt flag named PageType, and set it to 3 or 0
 *		if header/footer was shown or not shown
 *		(only one header/footer was supported)
 */

typedef struct {
	UBYTE	ShowIt;			/* Non-zero if should be shown */
	UBYTE	FirstPage;		/* 0 = Not on first page */
	UBYTE	EvenPage;		/* Non-zero if is even page header/footer */
	UBYTE	pad1;
	UWORD	pad2;
} HEAD;

/*
 *	PCTS - Begin picture section
 *	Note:  ProWrite 3.2 ignores this chunk
 */

typedef struct {
	UBYTE	NPlanes;		/* Number of planes used in picture bitmaps */
	UBYTE	pad;
} PCTS;

/*
 *	PARA - New paragraph format
 *	This chunk should be inserted first when a new section is started (DOC,
 *		HEAD, or FOOT), and again whenever the paragraph format changes
 */

#define SPACE_SINGLE	0
#define SPACE_1_HALF	0x08	/* 1 1/2 spacing */
#define SPACE_DOUBLE	0x10

#define FIXHEIGHT_AUTO	0
#define FIXHEIGHT_6LPI	12
#define FIXHEIGHT_8LPI	9

#define JUSTIFY_LEFT	0
#define JUSTIFY_CENTER	1
#define JUSTIFY_RIGHT	2
#define JUSTIFY_FULL	3

#define EXTRABEFORE_BITS	0x0F
#define EXTRAAFTER_BITS		0xF0
#define SPACE_BEFORE(extraSpace)	((extraSpace) & 0x0F)
#define SPACE_AFTER(extraSpace)		((extraSpace) >> 4 & 0x0F)

#define MISCSTYLE_SUPER		0x01		/* Superscript */
#define MISCSTYLE_SUB		0x02		/* Subscript */
#define MISCSTYLE_SHADOW	0x04

typedef struct {
	UWORD	LeftIndent;	/* In decipoints */
	UWORD	LeftMargin;
	UWORD	RightMargin;
	UBYTE	Spacing;	/* From defines above */
	UBYTE	Justify;	/* From defines above */
	UBYTE	FontNumLo;	/* FontNum, Style, etc. for first char in para*/
	UBYTE	Style;		/* Standard Amiga style bits */
	UBYTE	MiscStyle;	/* From defines above */
	UBYTE	Color;		/* Internal number, use COLR to translate */
	UBYTE	FixHeight;	/* Fixed line height in points */
	UBYTE	ExtraSpace;	/* Number of extra lines before/after para */
	UBYTE	FontNumHi;	/* Hi byte of font number */
	UBYTE	FontSize;	/* FontSize for first char in para */
} PARA;

/*
 *	TABS - New tab stop types/locations
 *	Use an array of values in each chunk
 *	Like the PARA chunk, this should be inserted whenever the tab settings
 *		for a paragraph change
 */

#define TAB_LEFT	0
#define TAB_CENTER	1
#define TAB_RIGHT	2
#define TAB_DECIMAL	3

typedef struct {
	UWORD	Position;		/* In decipoints */
	UBYTE	Type;
	UBYTE	pad;
} TABS;

/*
 *	PAGE - Page break
 *	Just a marker, no data in chunk
 */

/*
 *	COLM - Column break
 *	Just a marker, no data in chunk
 */

/*
 *	TEXT - Paragraph text (one block per paragraph)
 *	Block is actual text, no need for separate structure
 *	If the paragraph is empty, this is an empty chunk -- there MUST be
 *		a TEXT block for every paragraph
 */

/*
 *	FSCC - Font/Style/Color changes in previous TEXT block
 *	Use an array of values in each chunk
 *	Only include this chunk if the previous TEXT block did not have
 *		the same Font/Style/Color for all its characters
 */

typedef struct {
	UWORD	Location;	/* Character location in TEXT chunk of change */
	UBYTE	FontNumLo;	/* Low byte of font number */
	UBYTE	Style;
	UBYTE	MiscStyle;
	UBYTE	Color;
	UBYTE	FontNumHi;	/* High byte of font number */
	UBYTE	FontSize;
} FSCC;

/*
 *	PINF - Picture info (obsolete)
 *	This chunk must only be in a PCTS section
 *	Must be followed by ILBM BODY chunk
 *	Pictures are treated independently of the document text (like a
 *		page-layout system), this chunk includes information about what page
 *		and location on the page the picture is at
 *	Note:  ProWrite currently only supports mskTransparentColor and mskHasMask
 *		masking
 */

typedef struct {
	WORD		Width, Height;		/* In pixels */
	WORD		Page;				/* Which page picture is on (0..max) */
	WORD		XPos, YPos;			/* Location on page in decipoints */
	Masking		Masking;			/* Like ILBM format */
	Compression	Compression;		/* Like ILBM format */
	UBYTE		TransparentColor;	/* Like ILBM format */
	UBYTE		pad;
} PINF;

/*
 *	BMAP - Bitmap picture object
 */

#define BMAP_MASKNONE	0
#define BMAP_MASKPLANE	1

#define BMAP_CMPNONE	0
#define BMAP_CMPBYTERUN	1

typedef struct {
	WORD		Width, Height;		/* Display width/height, in pixels */
	WORD		Page;				/* Which page picture is on (0..max) */
	WORD		XPos, YPos;			/* Location on page in decipoints */
	UBYTE		Masking;
	UBYTE		Compression;
	WORD		DataWidth, DataHeight, Depth;
	LONG		ViewMode;			/* HAM, HALFBRITE, etc. */
	WORD		WrapFlags;			/* Format and type of text wrapping */
	WORD		WrapGap;			/* distance from picture */
	ULONG		pad[9];				/* Future expansion */
/*	RGBColor	ColorTable[1 << Depth];	*/
									/* Followed by non-interleaved bitmap data */
} BMAP;

/*
 *	PNAM - chunk contains name of picture, will only appear if
 *	previous picture has a name
 */

/*
 *  INFO - project information chunk
 */

#define INFO_PWLENGTH	20
#define INFO_COMMENTLEN 80

typedef struct {
	ULONG	CreationDays;
	ULONG	CreationTime;
	ULONG	CreationTick;
	ULONG	ModifyDays;
	ULONG	ModifyTime;
	ULONG	ModifyTick;
	UWORD	Flags;
	UBYTE	Password[INFO_PWLENGTH];
	UBYTE	Comment[INFO_COMMENTLEN];
	UBYTE	Pad[100];
} INFO;

/*
 * LINK - Infomation for hotlinks
 */

typedef struct {
	struct PubRecord PubRecord;
} LINK;