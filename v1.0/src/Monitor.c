/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	High-priority message monitor
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <proto/exec.h>
#include <proto/intuition.h>

#include <Toolbox/Window.h>
#include <Toolbox/Utility.h>

#include "Draw.h"
#include "Proto.h"

/*
 *	External variables
 */

extern ScreenPtr	screen;
extern WindowPtr	backWindow, toolWindow, penWindow, fillWindow;
extern MsgPortPtr	mainMsgPort;
extern MsgPort		monitorMsgPort;

extern BOOL	busy, inMacro;
extern WORD	waitCount;		/* > 0 if in requester or lengthy operation */
extern BOOL	closeAllFlag, quitFlag;

/*
 *	Task to continuously monitor important events
 *	This task is started up with a higher priority than main program
 *	This routine and all routines called by it must NOT have stack checking
 */

void __saveds MonitorTask()
{
	register UWORD code, modifier, windKind;
	register IntuiMsgPtr intuiMsg;
	register WindowPtr window;

/*
	Set up monitor's message port
*/
	monitorMsgPort.mp_Node.ln_Name = NULL;
	monitorMsgPort.mp_Node.ln_Pri = 0;
	monitorMsgPort.mp_Node.ln_Type = NT_MSGPORT;
	monitorMsgPort.mp_Flags = PA_SIGNAL;
	monitorMsgPort.mp_SigBit = AllocSignal(-1);
	monitorMsgPort.mp_SigTask = FindTask(NULL);
	NewList(&(monitorMsgPort.mp_MsgList));
/*
	Wait for intuition messages
*/
	for (;;) {
		Wait(1 << monitorMsgPort.mp_SigBit);
		while (intuiMsg = (IntuiMsgPtr) GetMsg(&monitorMsgPort)) {
			code = intuiMsg->Code;
			modifier = intuiMsg->Qualifier;
			window = intuiMsg->IDCMPWindow;
/*
	Process important events
*/
			switch (intuiMsg->Class) {
			case MENUVERIFY:
				if (code == MENUHOT) {
					windKind = GetWKind(window);
					if (waitCount || inMacro || closeAllFlag || quitFlag ||
						windKind == WKIND_TOOL || windKind == WKIND_DLOG)
						intuiMsg->Code = MENUCANCEL;
					else if (modifier & IEQUALIFIER_RBUTTON)
						SetStdPointer(window, POINTER_ARROW);
				}
				ReplyMsg((MsgPtr) intuiMsg);
				break;
			case RAWKEY:
				if ((modifier & IEQUALIFIER_REPEAT) && busy)
					ReplyMsg((MsgPtr) intuiMsg);
				else
					PutMsg(mainMsgPort, (MsgPtr) intuiMsg);
				break;
			case REFRESHWINDOW:
				if (window == backWindow) {
					ReplyMsg((MsgPtr) intuiMsg);
					DoWindowRefresh(window);
				}
				else
					PutMsg(mainMsgPort, (MsgPtr) intuiMsg);
				break;
/*
	Default case: let main loop handle message
*/
			default:
				PutMsg(mainMsgPort, (MsgPtr) intuiMsg);
				break;
			}
		}
	}
}
