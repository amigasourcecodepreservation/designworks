/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Toolbox library
 *	Copyright (c) 1991-93 New Horizons Software, Inc.
 *
 *	Pop-Up List handler
 */

#define	INTUI_V36_NAMES_ONLY	1

#include <exec/types.h>
#include <intuition/intuition.h>

#include <string.h>

#include <proto/graphics.h>
#include <proto/intuition.h>

#include <Toolbox/Globals.h>
#include <Toolbox/Memory.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Border.h>
#include <Toolbox/PopUpList.h>
#include <Toolbox/Window.h>
#include <Toolbox/Utility.h>

/*
 *	Local variables and definitions
 */

#define MARGIN_X	2		// Left, right margins
#define MARGIN_Y	1		// Top, bottom margins
#define GAP			3		// Gap between items

#define MIN(a, b)	((a)<(b)?(a):(b))

/*
 *	Local prototypes
 */

static void	DrawItem(PopUpListPtr, WORD, BOOL);
static WORD	WhichItem(PopUpListPtr);

/*
 *	Create pop-up list
 */

PopUpListPtr CreatePopUpList(WORD numItems, TextPtr items[])
{
	WORD			i;
	BOOL			success;
	PopUpListPtr	popUpList;

	success = FALSE;
	if ((popUpList = MemAlloc(sizeof(PopUpList), MEMF_CLEAR)) != NULL) {
		if ((popUpList->List = CreateList()) != NULL) {
			for (i = 0; i < numItems; i++) {
				if (!InsertListItem(popUpList->List, items[i], 0, -1))
					break;
			}
			if (i == numItems)
				success = TRUE;
		}
	}
	if (success) {
		if ((popUpList->Font = GetFont(&_tbTextAttr)) == NULL)
			success = FALSE;
	}
	if (!success) {
		DisposePopUpList(popUpList);
		popUpList = NULL;
	}
	return (popUpList);
}

/*
 *	Dispose of pop-up list
 */

void DisposePopUpList(PopUpListPtr popUpList)
{
	if (popUpList) {
		if (popUpList->List)
			DisposeList(popUpList->List);
		if (popUpList->Font)
			CloseFont(popUpList->Font);
		MemFree(popUpList, sizeof(PopUpList));
	}
}

/*
 *	Draw item in pop-up window
 */

static void DrawItem(PopUpListPtr popUpList, WORD item, BOOL selected)
{
	WORD		top, yPos;
	TextPtr		text;
	RastPtr		rPort = popUpList->Window->RPort;
	Rectangle	rect;

	if (item < 0 || item >= NumListItems(popUpList->List))
		return;
/*
	Get item rectangle
*/
	top = item*(popUpList->Font->tf_YSize + GAP) + MARGIN_Y;
	SetRect(&rect, 1, top, popUpList->Window->Width - 2,
			top + popUpList->Font->tf_YSize + GAP - 1);
/*
	Fill in background
*/
	SetAPen(rPort, (selected) ? _tbPenDark : _tbPenLight);
	FillRect(rPort, &rect);
	if (selected)
		DrawShadowBox(rPort, &rect, 0, FALSE);
	InsetRect(&rect, MARGIN_X, 0);
/*
	Draw text
*/
	text = GetListItem(popUpList->List, item);
	Move(rPort, rect.MinX, rect.MinY + popUpList->Font->tf_Baseline + 2);
	SetAPen(rPort, (selected) ? _tbPenWhite : _tbPenBlack);
	if (popUpList->DrawProc)
		(*(popUpList->DrawProc))(rPort, text, &rect);
	else {
		if (text[0] == '\0') {
			yPos = (rect.MaxY + rect.MinY)/2;
			Move(rPort, rect.MinX, yPos);
			Draw(rPort, rect.MaxX, yPos);
		}
		else
			TextInWidth(rPort, text, strlen(text), rect.MaxX - rect.MinX + 1, FALSE);
	}
}

/*
 *	Return item under mouse, or -1
 */

static WORD WhichItem(PopUpListPtr popUpList)
{
	register WORD		x, y, item;
	register WindowPtr	window = popUpList->Window;

	x = window->MouseX;
	y = window->MouseY - MARGIN_Y;
	if (x < 0 || x >= window->Width - 1 || y < 0)
		return (-1);
	item = y/(popUpList->Font->tf_YSize + GAP);
	if (item >= NumListItems(popUpList->List))
		return (-1);
	return (item);
	
}

/*
 *	Handle pop-up list
 *	Return item number selected, or -1 if out of bounds
 */

WORD PopUpListSelect(ScreenPtr screen, PopUpListPtr popUpList, WORD x, WORD y, WORD width)
{
	register WORD		i, numItems, height;
	WORD				item, prevItem;
	TextPtr				text;
	RastPtr				rPort;
	Rectangle			rect;
	WindowPtr			activeWindow;
	struct NewWindow	newWindow;

	numItems = NumListItems(popUpList->List);
	if (numItems == 0)
		return (-1);
	activeWindow = ActiveWindow();
	height = numItems*(popUpList->Font->tf_YSize + GAP) + 2*MARGIN_Y;
/*
	Set up newWindow
*/
	BlockClear(&newWindow, sizeof(struct NewWindow));
	newWindow.LeftEdge	= MIN(x, screen->Width - width);
	newWindow.TopEdge	= MIN(y, screen->Height - height);
	newWindow.Width		= width;
	newWindow.Height	= height;
	newWindow.DetailPen	= newWindow.BlockPen = -1;
	newWindow.IDCMPFlags= IDCMP_INTUITICKS | IDCMP_MOUSEBUTTONS | IDCMP_INACTIVEWINDOW;
	newWindow.Flags		= WFLG_SIMPLE_REFRESH | WFLG_BORDERLESS | WFLG_ACTIVATE |
							WFLG_NOCAREREFRESH;
	newWindow.Screen	= screen;
	newWindow.Type		= CUSTOMSCREEN;
/*
	Open window and draw contents
*/
	if ((popUpList->Window = OpenWindow(&newWindow)) == NULL)
		return (-1);
	SetStdPointer(popUpList->Window, POINTER_ARROW);
	SetWindowTitles(popUpList->Window, (UBYTE *) -1, activeWindow->ScreenTitle);
	rPort = popUpList->Window->RPort;
	SetFont(rPort, popUpList->Font);
	SetAPen(rPort, _tbPenLight);
	SetRect(&rect, 0, 0, width - 1, height - 1);
	FillRect(rPort, &rect);
	DrawShadowBox(rPort, &rect, 0, TRUE);
	for (i = 0; i < numItems; i++)
		DrawItem(popUpList, i, FALSE);
/*
	Handle events
*/
	item = prevItem = -1;
	while (WaitMouseUp(popUpList->Window->UserPort, popUpList->Window)) {
		item = WhichItem(popUpList);
		if (item != -1) {
			text = GetListItem(popUpList->List, item);
			if (text[0] == '\0')
				item = -1;					// Don't select separators
		}
		if (prevItem != item) {
			DrawItem(popUpList, prevItem, FALSE);
			DrawItem(popUpList, item, TRUE);
			prevItem = item;
		}
	}
/*
	Close window and return result
*/
	CloseWindow(popUpList->Window);
	popUpList->Window = NULL;
	return (item);
}

/*
 *	Set the draw proc for scroll list
 */

void PopUpSetDrawProc(PopUpListPtr popUpList, void (*drawProc)(RastPtr, TextPtr, RectPtr))
{
	popUpList->DrawProc = drawProc;
}
