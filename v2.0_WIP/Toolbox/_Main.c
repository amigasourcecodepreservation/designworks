/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Toolbox library
 *	Copyright (c) 1993 New Horizons Software, Inc.
 *
 *	_main routine, slightly modified from Lattice source
 */

#include <exec/types.h>
#include <workbench/startup.h>
#include <libraries/dos.h>
#include <libraries/dosextens.h>

#include <proto/exec.h>
#include <proto/dos.h>

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <ios1.h>
#include <string.h>

#define MAXARG 32		// Maximum command line arguments
#define QUOTE  '"'

extern struct UFB	_ufbs[];
extern int			_fmode;

extern struct WBStartup	*WBenchMsg;

int		argc;			// arg count
char	*argv[MAXARG];	// arg pointers

/*
 *	Prototypes
 */

void	main(int, char **);

/*
 * Name		_main - process command line, open files, and call "main"
 *
 * Synopsis	_main(line);
 *	char *line;	ptr to command line that caused execution
 *
 * Description
 *	This function performs the standard pre-processing for
 *	the main module of a C program.  It accepts a command
 *	line of the form
 *
 *	pgmname arg1 arg2 ...
 *
 *	and builds a list of pointers to each argument.  The first
 *	pointer is to the program name.  For some environments, the
 *	standard I/O files are also opened, using file names that
 *	were set up by the OS interface module XCMAIN.
 */

void _main(register char *line)
{
	register int		x;
	struct Process		*process;
	struct FileHandle	*handle;

/*
	Build argument pointer list
*/
	while (argc < MAXARG)  {
		while (*line == ' ')
			line++;
		if (*line == '\n' || *line == '\0')
			break;
		if (*line == QUOTE) {
			argv[argc++] = ++line;			// Ptr inside quoted string
			while (*line && *line != QUOTE && *line != '\n')
				line++;
		}
		else {								// Non-quoted arg
			argv[argc++] = line;
			while (*line && *line != ' ' && *line != '\n')
				line++;
		}
		if (*line == '\0')
			break;
		else
			*line++ = '\0';					// Terminate arg
	}
/*
	Open standard files
	Set up file handles only for running under CLI
		(for AREXX error message use)
*/
	if (argc == 0) {						// Running under Workbench
		_ufbs[0].ufbfh = Open("NIL:", MODE_NEWFILE);
		_ufbs[1].ufbfh = _ufbs[0].ufbfh;
		_ufbs[1].ufbflg = UFB_NC;
		_ufbs[2].ufbfh = _ufbs[0].ufbfh;
		_ufbs[2].ufbflg = UFB_NC;
		handle = (struct FileHandle *) BADDR(_ufbs[0].ufbfh);
		process = (struct Process *) FindTask(NULL);
		process->pr_ConsoleTask = (APTR) handle->fh_Type;
		x = 0;
	}
	else {									// Running under CLI
		_ufbs[0].ufbfh = Input();
		_ufbs[1].ufbfh = Output();
		_ufbs[2].ufbfh = Open("*", MODE_OLDFILE);
		x = UFB_NC;							// Do not close CLI defaults
	}
	_ufbs[0].ufbflg |= UFB_RA | O_RAW | x;
	_ufbs[1].ufbflg |= UFB_WA | O_RAW | x;
	_ufbs[2].ufbflg |= UFB_RA | UFB_WA | O_RAW;
	x = (_fmode) ? 0 : _IOXLAT;
	stdin->_file  = 0;
	stdin->_flag  = _IOREAD | x;
	stdout->_file = 1;
	stdout->_flag = _IOWRT | x;
	stderr->_file = 2;
	stderr->_flag = _IORW | x;
/*
	Call user's main program
*/
	main(argc, (argc) ? argv : (char **) WBenchMsg);	// Call main function
	exit(0);
}
