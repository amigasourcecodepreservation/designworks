/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	AREXX interface routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <proto/exec.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include <stdio.h>
#include <string.h>

#include <rexx/storage.h>
#include <rexx/rxslib.h>
#include <rexx/errors.h>

#include <Toolbox/Menu.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Window.h>
#include <Toolbox/DOS.h>
#include <Toolbox/AREXX.h>
#include <Toolbox/Utility.h>

#include "Draw.h"
#include "Proto.h"

/*
 *	AREXX definitions and prototypes (there is no REXX prototypes file)
 */

typedef struct RexxMsg	RexxMsg, *RexxMsgPtr;

ULONG		InitPort(MsgPortPtr, TextPtr);
void		FreePort(MsgPortPtr);
BOOL		IsRexxMsg(RexxMsgPtr);
RexxMsgPtr	CreateRexxMsg(MsgPortPtr, TextPtr, TextPtr);
BOOL		FillRexxMsg(RexxMsgPtr, ULONG, ULONG);
TextPtr		CreateArgstring(TextPtr, ULONG);
void		ClearRexxMsg(RexxMsgPtr, ULONG);
void		DeleteRexxMsg(RexxMsgPtr);

/*
 *	External variables
 */

extern struct RexxLib	*RexxSysBase;

extern MsgPort		rexxMsgPort;
extern MsgPortPtr	mainMsgPort;

extern ScreenPtr	screen;
extern WindowPtr	backWindow, windowList[];
extern WORD			numWindows;

extern WindowPtr	cmdWindow;

extern BOOL		inMacro, drawOff, fromCLI;

extern TextChar	strMacroDrawer[];
extern TextChar	strProgName[];
extern TextChar	strFKeysName[];
extern TextChar	strAutoExecName[];
extern TextChar	strMacroNames1[10][32], strMacroNames2[10][32];

extern FKey	fKeyTable1[], fKeyTable2[];

extern TextChar	strBuff[];

extern DlgTemplPtr	dlgList[];

/*
 *	Local variables and definitions
 */

#define USERREQ_EDIT	2
#define USERREQ_TEXT	3

#define MACRONAME_TEXT	2
#define MACROARG_TEXT	3

#define SAVE_BUTTON	2
#define NAME1_TEXT	3

static TextChar	progPortName[15];

#define MENUCMD_NULL	MENUITEM(NOMENU, NOITEM, NOSUB)

#define MENUCMD_NEW				MENUITEM(PROJECT_MENU, NEW_ITEM, NOSUB)
#define MENUCMD_OPEN			MENUITEM(PROJECT_MENU, OPEN_ITEM, NOSUB)
#define MENUCMD_QUIT			MENUITEM(PROJECT_MENU, QUIT_ITEM, NOSUB)
#define MENUCMD_PREFERENCES		MENUITEM(LAYOUT_MENU, PREFERENCES_ITEM, NOSUB)
#define MENUCMD_SCREENCOLORS	MENUITEM(LAYOUT_MENU, SCREENCOLORS_ITEM, NOSUB)

#define MENUCMD_IMPORTPICT		MENUITEM(PROJECT_MENU, IMPORT_ITEM, PICT_SUBITEM)

typedef struct MenuCmdList {
	TextPtr	Name;
	UWORD	Num;
} MenuCmdList;

#define NUM_MENU_COMMANDS	(sizeof(menuCmds)/sizeof(MenuCmdList))

static MenuCmdList menuCmds[] = {
	{ "New",			MENUITEM(PROJECT_MENU,	NEW_ITEM,			NOSUB) },
	{ "Open",			MENUITEM(PROJECT_MENU,	OPEN_ITEM,			NOSUB) },
	{ "Close",			MENUITEM(PROJECT_MENU,	CLOSE_ITEM,			NOSUB) },
	{ "ImportPict",		MENUITEM(PROJECT_MENU,	IMPORT_ITEM,		PICT_SUBITEM) },
	{ "ExportPict",		MENUITEM(PROJECT_MENU,	EXPORT_ITEM,		PICT_SUBITEM) },
	{ "Save",			MENUITEM(PROJECT_MENU,	SAVE_ITEM,			NOSUB) },
	{ "SaveAs",			MENUITEM(PROJECT_MENU,	SAVEAS_ITEM,		NOSUB) },
	{ "Revert",			MENUITEM(PROJECT_MENU,	REVERT_ITEM,		NOSUB) },
	{ "PageSetup",		MENUITEM(PROJECT_MENU,	PAGESETUP_ITEM,		NOSUB) },
	{ "PrintOne",		MENUITEM(PROJECT_MENU,	PRINTONE_ITEM,		NOSUB) },
	{ "Print",			MENUITEM(PROJECT_MENU,	PRINT_ITEM,			NOSUB) },
	{ "Quit",			MENUITEM(PROJECT_MENU,	QUIT_ITEM,			NOSUB) },

	{ "Cut",			MENUITEM(EDIT_MENU,		CUT_ITEM,			NOSUB) },
	{ "Copy",			MENUITEM(EDIT_MENU,		COPY_ITEM,			NOSUB) },
	{ "Paste",			MENUITEM(EDIT_MENU,		PASTE_ITEM,			NOSUB) },
	{ "Erase",			MENUITEM(EDIT_MENU,		ERASE_ITEM,			NOSUB) },
	{ "Duplicate",		MENUITEM(EDIT_MENU,		DUPLICATE_ITEM,		NOSUB) },
	{ "RotateLeft",		MENUITEM(EDIT_MENU,		ROTATE_ITEM,		LEFT_SUBITEM) },
	{ "RotateRight",	MENUITEM(EDIT_MENU,		ROTATE_ITEM,		RIGHT_SUBITEM) },
	{ "FlipHoriz",		MENUITEM(EDIT_MENU,		FLIP_ITEM,			HORIZONTAL_SUBITEM) },
	{ "FlipVert",		MENUITEM(EDIT_MENU,		FLIP_ITEM,			VERTICAL_SUBITEM) },
	{ "Scale",			MENUITEM(EDIT_MENU,		SCALE_ITEM,			NOSUB) },
	{ "ConvertToPoly",	MENUITEM(EDIT_MENU,		CONVERTTOPOLY_ITEM,	NOSUB) },
	{ "PolyClose",		MENUITEM(EDIT_MENU,		POLYGON_ITEM,		CLOSEPOLY_SUBITEM) },
	{ "PolyOpen",		MENUITEM(EDIT_MENU,		POLYGON_ITEM,		OPENPOLY_SUBITEM) },
	{ "PolySmooth",		MENUITEM(EDIT_MENU,		POLYGON_ITEM,		SMOOTH_SUBITEM) },
	{ "PolyUnsmooth",	MENUITEM(EDIT_MENU,		POLYGON_ITEM,		UNSMOOTH_SUBITEM) },
	{ "SelectAll",		MENUITEM(EDIT_MENU,		SELECTALL_ITEM,		NOSUB) },

	{ "NormalSize",		MENUITEM(LAYOUT_MENU,	NORMALSIZE_ITEM,	NOSUB) },
	{ "Enlarge",		MENUITEM(LAYOUT_MENU,	ENLARGE_ITEM,		NOSUB) },
	{ "Reduce",			MENUITEM(LAYOUT_MENU,	REDUCE_ITEM,		NOSUB) },
	{ "FitToWindow",	MENUITEM(LAYOUT_MENU,	FITTOWINDOW_ITEM,	NOSUB) },
	{ "GridSnap",		MENUITEM(LAYOUT_MENU,	GRIDSNAP_ITEM,		NOSUB) },
	{ "GridSize",		MENUITEM(LAYOUT_MENU,	GRIDSIZE_ITEM,		NOSUB) },
	{ "PenColors",		MENUITEM(LAYOUT_MENU,	PENCOLORS_ITEM,		NOSUB) },
	{ "FillPatterns",	MENUITEM(LAYOUT_MENU,	FILLPATTERNS_ITEM,	NOSUB) },
	{ "Layers",			MENUITEM(LAYOUT_MENU,	LAYERS_ITEM,		NOSUB) },
	{ "DrawingSize",	MENUITEM(LAYOUT_MENU,	DRAWINGSIZE_ITEM,	NOSUB) },
	{ "Preferences",	MENUITEM(LAYOUT_MENU,	PREFERENCES_ITEM,	NOSUB) },
	{ "ScreenColors",	MENUITEM(LAYOUT_MENU,	SCREENCOLORS_ITEM,	NOSUB) },

	{ "MoveForward",	MENUITEM(ARRANGE_MENU,	MOVEFORWARD_ITEM,	NOSUB) },
	{ "MoveToFront",	MENUITEM(ARRANGE_MENU,	MOVETOFRONT_ITEM,	NOSUB) },
	{ "MoveBackward",	MENUITEM(ARRANGE_MENU,	MOVEBACKWARD_ITEM,	NOSUB) },
	{ "MoveToBack",		MENUITEM(ARRANGE_MENU,	MOVETOBACK_ITEM,	NOSUB) },
	{ "AlignToGrid",	MENUITEM(ARRANGE_MENU,	ALIGNTOGRID_ITEM,	NOSUB) },
	{ "AlignObjects",	MENUITEM(ARRANGE_MENU,	ALIGNOBJECTS_ITEM,	NOSUB) },
	{ "Group",			MENUITEM(ARRANGE_MENU,	GROUP_ITEM,			NOSUB) },
	{ "Ungroup",		MENUITEM(ARRANGE_MENU,	UNGROUP_ITEM,		NOSUB) },
	{ "Lock",			MENUITEM(ARRANGE_MENU,	LOCK_ITEM,			NOSUB) },
	{ "Unlock",			MENUITEM(ARRANGE_MENU,	UNLOCK_ITEM,		NOSUB) },

	{ "NoArrows",		MENUITEM(PEN_MENU,		NOARROWS_ITEM,		NOSUB) },
	{ "ArrowAtStart",	MENUITEM(PEN_MENU,		ARROWSTART_ITEM,	NOSUB) },
	{ "ArrowAtEnd",		MENUITEM(PEN_MENU,		ARROWEND_ITEM,		NOSUB) },

	{ "Font",			MENUITEM(TEXT_MENU,		FONT_ITEM,			OTHERFONT_SUBITEM) },
	{ "StylePlain",		MENUITEM(TEXT_MENU,		PLAIN_ITEM,			NOSUB) },
	{ "StyleBold",		MENUITEM(TEXT_MENU,		BOLD_ITEM,			NOSUB) },
	{ "StyleItalic",	MENUITEM(TEXT_MENU,		ITALIC_ITEM,		NOSUB) },
	{ "StyleUnderline",	MENUITEM(TEXT_MENU,		UNDERLINE_ITEM,		NOSUB) },
	{ "AlignLeft",		MENUITEM(TEXT_MENU,		LEFTALIGN_ITEM,		NOSUB) },
	{ "AlignCenter",	MENUITEM(TEXT_MENU,		CENTERALIGN_ITEM,	NOSUB) },
	{ "AlignRight",		MENUITEM(TEXT_MENU,		RIGHTALIGN_ITEM,	NOSUB) },
	{ "SpaceSingle",	MENUITEM(TEXT_MENU,		SINGLESPACE_ITEM,	NOSUB) },
	{ "SpaceOneAndHalf",MENUITEM(TEXT_MENU,		ONEANDHALFSPACE_ITEM,NOSUB) },
	{ "SpaceDouble",	MENUITEM(TEXT_MENU,		DOUBLESPACE_ITEM,	NOSUB) }
};

#define NUM_MISC_COMMANDS	(sizeof(miscCmdNames)/sizeof(TextPtr))

static TextPtr miscCmdNames[] = {
	"ProgName", "ProgVersion", "DocName",
	"DrawOn", "DrawOff",
	"Type", "ASCII", "Backspace", "Delete",
	"CursorUp", "CursorDown", "CursorLeft", "CursorRight",
	"ShiftDown", "ShiftUp", "AltDown", "AltUp", "CtrlDown", "CtrlUp",
	"ScreenToFront", "ScreenToBack", "Window",
	"SetPrint",
	"LayerUp", "LayerDown", "Layer",
	"NewLine", "NewRect", "NewOval", "NewPoly", "NewText",
	"PenNum", "FillNum",
	"UnSelectAll",
	"Request1", "Request2", "Request3", "RequestText",
	"SetPath"
};

enum {
	CMD_PROGNAME,		CMD_PROGVERSION,	CMD_DOCNAME,
	CMD_DRAWON,			CMD_DRAWOFF,
	CMD_TYPE,			CMD_ASCII,			CMD_BACKSPACE,	CMD_DELETE,
	CMD_CURSORUP,		CMD_CURSORDOWN,		CMD_CURSORLEFT,	CMD_CURSORRIGHT,
	CMD_SHIFTDOWN,		CMD_SHIFTUP,		CMD_ALTDOWN,	CMD_ALTUP,
	CMD_CTRLDOWN,		CMD_CTRLUP,
	CMD_SCREENFRONT,	CMD_SCREENBACK,		CMD_WINDOW,
	CMD_SETPRINT,
	CMD_LAYERUP,		CMD_LAYERDOWN,		CMD_LAYER,
	CMD_NEWLINE,		CMD_NEWRECT,		CMD_NEWOVAL,	CMD_NEWPOLY,
	CMD_NEWTEXT,
	CMD_PENNUM,			CMD_FILLNUM,
	CMD_UNSELECTALL,
	CMD_REQUEST1,		CMD_REQUEST2,		CMD_REQUEST3,	CMD_REQUESTTEXT,
	CMD_SETPATH
};

static UWORD	rexxModifiers;		/* Inited to 0 */

static RexxMsgPtr	macroMsg;		/* For macro commands (Inited to NULL) */

/*
 *	Local prototypes
 */

static BOOL	MatchCommand(TextPtr, UWORD, TextPtr);

static void	UserRequest(WORD, RexxMsgPtr, TextPtr);

static BOOL	DoOtherMacro(void);

static void	SaveFKeys(void);

static BOOL	DoCustomizeMacro(WindowPtr);

/*
 *	Open REXX library and init REXX port
 *	If REXX port already exists, then do not init REXX interface
 */

void InitRexx()
{
	WORD portNum;

/*
	Open REXX
*/
	RexxSysBase = (struct RexxLib *) OpenLibrary("rexxsyslib.library", 0);
	if (RexxSysBase == NULL)
		return;
/*
	Find unused port name and set up port
*/
	portNum = -1;
	do {
		strcpy(progPortName, strProgName);
		portNum++;				/* Compiler bug, this must go after strcpy() */
		if (portNum > 0) {
			NumToString(portNum, strBuff);
			strcat(progPortName, ".");
			strcat(progPortName, strBuff);
		}
	} while (FindPort(progPortName));
	InitPort(&rexxMsgPort, progPortName);
	AddPort(&rexxMsgPort);
}

/*
 *	Shut down REXX port and close library
 */

void ShutDownRexx()
{
	if (RexxSysBase) {
		RemPort(&rexxMsgPort);
		FreePort(&rexxMsgPort);
		CloseLibrary(RexxSysBase);
	}
}

/*
 *	Check for a match to a command string
 *	Return TRUE if equal
 */

static BOOL MatchCommand(TextPtr cmdText, UWORD cmdLen, TextPtr text)
{
	if (CmpString(cmdText, text, cmdLen, strlen(text), FALSE) == 0)
		return (TRUE);
	return (FALSE);
}

/*
 *	Handle user request dialogs
 */

static void UserRequest(WORD cmdNum, RexxMsgPtr rexxMsg, TextPtr prompt)
{
	WORD dlgNum;
	DialogPtr dlg;

	switch (cmdNum) {
	case CMD_REQUEST1:
		dlgNum = DLG_USERREQ1;
		break;
	case CMD_REQUEST2:
		dlgNum = DLG_USERREQ2;
		break;
	case CMD_REQUEST3:
		dlgNum = DLG_USERREQ3;
		break;
	case CMD_REQUESTTEXT:
		if ((rexxMsg->rm_Action & RXFF_RESULT) == 0) {
			rexxMsg->rm_Result1 = RC_FATAL;
			return;
		}
		dlgNum = DLG_USERREQTEXT;
		break;
	default:
		rexxMsg->rm_Result1 = RC_FATAL;
		return;
	}
	dlgList[dlgNum]->Gadgets[USERREQ_TEXT].Info = prompt;
	BeginWait();
	if ((dlg = GetDialog(dlgList[dlgNum], screen, mainMsgPort)) == NULL) {
		Error(ERR_NO_MEM);
		EndWait();
		rexxMsg->rm_Result1 = RC_ERROR;
		return;
	}
	OutlineOKButton(dlg);
	rexxMsg->rm_Result1 = ModalDialog(mainMsgPort, dlg, DialogFilter);
	if (cmdNum == CMD_REQUESTTEXT && rexxMsg->rm_Result1 == OK_BUTTON) {
		GetEditItemText(dlg->FirstGadget, USERREQ_EDIT, strBuff);
		rexxMsg->rm_Result2 = (LONG) CreateArgstring(strBuff, strlen(strBuff));
	}
	DisposeDialog(dlg);
	EndWait();
}

/*
 *	Handle REXX messages
 */

void DoRexxMsg(RexxMsgPtr rexxMsg)
{
	register UWORD		i, cmd, cmdLen, len, menuCmd;
	BOOL				isDocWindow, resultEnabled, success;
	LONG				num;
	register TextPtr	cmdText, argText;
	register TextChar	ch;
	WindowPtr			activeWindow;
	DocDataPtr			docData;
	PrintRecPtr			printRec;
	Dir					dir;

	activeWindow = ActiveWindow();
/*
	Handle message reply
*/
	if (rexxMsg == macroMsg) {
		if (macroMsg->rm_Result1 != RC_OK)
			Error(ERR_MACRO_FAIL);
		ClearRexxMsg(macroMsg, (macroMsg->rm_Action & 0xFF) + 1);
		DeleteRexxMsg(macroMsg);
		macroMsg = NULL;
		rexxModifiers = 0;
		inMacro = FALSE;
		SetPointerShape();
		if (IsDocWindow(activeWindow))
			HiliteSelectOn(activeWindow);	/* May have been off */
		SetAllMenus();
		return;
	}
/*
	Handle other messages
*/
	if (!IsRexxMsg(rexxMsg))
		return;
	rexxMsg->rm_Result1 = RC_OK;		/* Assume success */
	rexxMsg->rm_Result2 = 0;
/*
	Get the command and data portions of argument
*/
	cmdText = rexxMsg->rm_Args[0];
	while (*cmdText == ' ')
		cmdText++;
	argText = cmdText;
	while (*argText && *argText != ' ')
		argText++;
	cmdLen = (UWORD) (argText - cmdText);
	while (*argText == ' ')
		argText++;
/*
	Get window to send messages to
*/
	if (IsDocWindow(activeWindow))
		cmdWindow = activeWindow;
	else if (!IsDocWindow(cmdWindow)) {
		if (numWindows)
			cmdWindow = windowList[numWindows - 1];
		else
			cmdWindow = backWindow;
	}
	isDocWindow = IsDocWindow(cmdWindow);
/*
	Check for menu commands
*/
	for (cmd = 0; cmd < NUM_MENU_COMMANDS; cmd++) {
		if (MatchCommand(cmdText, cmdLen, menuCmds[cmd].Name))
			break;
	}
/*
	Handle menu commands
*/
	if (cmd < NUM_MENU_COMMANDS) {
		menuCmd = menuCmds[cmd].Num;
		if (!isDocWindow &&
			menuCmd != MENUCMD_NEW && menuCmd != MENUCMD_OPEN &&
			menuCmd != MENUCMD_QUIT && menuCmd != MENUCMD_PREFERENCES &&
			menuCmd != MENUCMD_SCREENCOLORS)
			goto Error;
		if (menuCmd == MENUCMD_NULL)
			goto Error;
/*
	Handle modified menu commands
*/
		if (*argText) {
			switch (menuCmd) {
			case MENUCMD_OPEN:
				if (!DoOpen(cmdWindow, rexxModifiers, argText))
					rexxMsg->rm_Result1 = RC_ERROR;
				goto Done;
			case MENUCMD_IMPORTPICT:
				if (!DoImportPict(cmdWindow, argText))
					rexxMsg->rm_Result1 = RC_ERROR;
				goto Done;
			default:
				rexxMsg->rm_Result1 = RC_WARN;		/* Invalid option for command */
				break;
			}
		}
		if (!DoMenu(cmdWindow, menuCmd, rexxModifiers, FALSE))
			rexxMsg->rm_Result1 = RC_ERROR;
		goto Done;
	}
/*
	Check for non-menu commands
*/
	for (cmd = 0; cmd < NUM_MISC_COMMANDS; cmd++) {
		if (MatchCommand(cmdText, cmdLen, miscCmdNames[cmd]))
			break;
	}
	if (cmd >= NUM_MISC_COMMANDS)
		goto Error;
/*
	Handle non-menu commands
*/
	resultEnabled = ((rexxMsg->rm_Action & RXFF_RESULT) != 0);
	switch (cmd) {
	case CMD_PROGNAME:
		if (!resultEnabled)
			goto Error;
		rexxMsg->rm_Result2 = (LONG) CreateArgstring(strProgName, strlen(strProgName));
		break;
	case CMD_PROGVERSION:
		if (!resultEnabled)
			goto Error;
		NumToString((PROGRAM_VERSION >> 8) & 0xFF, strBuff);
		strcat(strBuff, ".");
		NumToString((PROGRAM_VERSION >> 4) & 0x0F, strBuff + strlen(strBuff));
		rexxMsg->rm_Result2 = (LONG) CreateArgstring(strBuff, strlen(strBuff));
		break;
	case CMD_DOCNAME:
		if (!resultEnabled || !isDocWindow)
			goto Error;
		GetWTitle(cmdWindow, strBuff);
		rexxMsg->rm_Result2 = (LONG) CreateArgstring(strBuff, strlen(strBuff));
		break;
	case CMD_DRAWON:
		drawOff = FALSE;
		if (isDocWindow)
			HiliteSelectOn(cmdWindow);
		break;
	case CMD_DRAWOFF:
		if (isDocWindow)
			HiliteSelectOff(cmdWindow);
		drawOff = TRUE;
		break;
	case CMD_TYPE:
		if (!isDocWindow || !TextInEdit(cmdWindow))
			goto Error;
		for (i = 0; i < strlen(argText); i++) {
			ch = argText[i];
			if ((ch & 0x7F) >= 0x20 && (ch & 0x7F) <= 0x7F && ch != 0x7F)
				TextInsert(cmdWindow, ch);
			else
				rexxMsg->rm_Result1 = RC_WARN;	/* Invalid character */
		}
		break;
	case CMD_ASCII:
		if (!isDocWindow || !TextInEdit(cmdWindow))
			goto Error;
		num = StringToNum(argText);
		if (num < 0xFF &&
			((num & 0x7F) >= 0x20 || num == TAB || num == CR) && num != 0x7F)
			TextInsert(cmdWindow, (TextChar) num);
		else
			rexxMsg->rm_Result1 = RC_WARN;	/* Invalid character */
		break;
	case CMD_BACKSPACE:
		if (!isDocWindow)
			goto Error;
		if (TextInEdit(cmdWindow))
			TextDelete(cmdWindow, BS, rexxModifiers);
		else
			rexxMsg->rm_Result1 = RC_WARN;
		break;
	case CMD_DELETE:
		if (!isDocWindow)
			goto Error;
		if (TextInEdit(cmdWindow))
			TextDelete(cmdWindow, DEL, rexxModifiers);
		else
			rexxMsg->rm_Result1 = RC_WARN;
		break;
	case CMD_CURSORUP:
		if (!isDocWindow)
			goto Error;
		if (!DoCursorKey(cmdWindow, CURSORUP, rexxModifiers))
			rexxMsg->rm_Result1 = RC_ERROR;
		break;
	case CMD_CURSORDOWN:
		if (!isDocWindow)
			goto Error;
		if (!DoCursorKey(cmdWindow, CURSORDOWN, rexxModifiers))
			rexxMsg->rm_Result1 = RC_ERROR;
		break;
	case CMD_CURSORLEFT:
		if (!isDocWindow)
			goto Error;
		if (!DoCursorKey(cmdWindow, CURSORLEFT, rexxModifiers))
			rexxMsg->rm_Result1 = RC_ERROR;
		break;
	case CMD_CURSORRIGHT:
		if (!isDocWindow)
			goto Error;
		if (!DoCursorKey(cmdWindow, CURSORRIGHT, rexxModifiers))
			rexxMsg->rm_Result1 = RC_ERROR;
		break;
	case CMD_SHIFTDOWN:
		rexxModifiers |= SHIFTKEYS;
		break;
	case CMD_SHIFTUP:
		rexxModifiers &= ~SHIFTKEYS;
		break;
	case CMD_ALTDOWN:
		rexxModifiers |= ALTKEYS;
		break;
	case CMD_ALTUP:
		rexxModifiers &= ~ALTKEYS;
		break;
	case CMD_CTRLDOWN:
		rexxModifiers |= IEQUALIFIER_CONTROL;
		break;
	case CMD_CTRLUP:
		rexxModifiers &= ~IEQUALIFIER_CONTROL;
		break;
	case CMD_SCREENFRONT:
		ScreenToFront(screen);
		break;
	case CMD_SCREENBACK:
		ScreenToBack(screen);
		break;
	case CMD_WINDOW:
		for (i = 0; i < numWindows; i++) {
			GetWTitle(windowList[i], strBuff);
			if (CmpString(argText, strBuff, strlen(argText), strlen(strBuff), FALSE) == 0)
				break;
		}
		if (i < numWindows)
			DoViewMenu(backWindow, i + WINDOW_ITEM, NOSUB);
		else
			goto Error;
		break;
	case CMD_SETPRINT:
		if (isDocWindow) {
			docData = GetWRefCon(cmdWindow);
			printRec = docData->PrintRec;
		}
		else
			printRec = NULL;
		while (*argText) {
			for (len = 0; argText[len] && argText[len] != ' '; len++) ;
			success = SetPrintOption(printRec, argText, len);
			if (!success)
				rexxMsg->rm_Result1 = RC_WARN;
			argText += len;
			while (*argText == ' ')
				argText++;
		}
		break;
	case CMD_LAYERUP:
		if (!isDocWindow)
			goto Error;
		if (!DoLayerUp(cmdWindow, FALSE))
			rexxMsg->rm_Result1 = RC_ERROR;
		break;
	case CMD_LAYERDOWN:
		if (!isDocWindow)
			goto Error;
		if (!DoLayerDown(cmdWindow, FALSE))
			rexxMsg->rm_Result1 = RC_ERROR;
		break;
	case CMD_LAYER:
		if (!isDocWindow)
			goto Error;
		num = StringToNum(argText);
		if (num <= 0 || !DoLayerNum(cmdWindow, num - 1))
			rexxMsg->rm_Result1 = RC_ERROR;
		break;
	case CMD_NEWLINE:
		if (!isDocWindow)
			goto Error;
		if (!NewREXXObject(cmdWindow, TYPE_LINE, argText))
			rexxMsg->rm_Result1 = RC_ERROR;
		break;
	case CMD_NEWRECT:
		if (!isDocWindow)
			goto Error;
		if (!NewREXXObject(cmdWindow, TYPE_RECT, argText))
			rexxMsg->rm_Result1 = RC_ERROR;
		break;
	case CMD_NEWOVAL:
		if (!isDocWindow)
			goto Error;
		if (!NewREXXObject(cmdWindow, TYPE_OVAL, argText))
			rexxMsg->rm_Result1 = RC_ERROR;
		break;
	case CMD_NEWPOLY:
		if (!isDocWindow)
			goto Error;
		if (!NewREXXObject(cmdWindow, TYPE_POLY, argText))
			rexxMsg->rm_Result1 = RC_ERROR;
		break;
	case CMD_NEWTEXT:
		if (!isDocWindow)
			goto Error;
		if (!NewREXXObject(cmdWindow, TYPE_TEXT, argText))
			rexxMsg->rm_Result1 = RC_ERROR;
		break;
	case CMD_PENNUM:
	case CMD_FILLNUM:
		if (*argText < '0' || *argText > '9')
			rexxMsg->rm_Result1 = RC_ERROR;
		else {
			num = StringToNum(argText);
			if (cmd == CMD_PENNUM)
				DoPenNum(cmdWindow, num);
			else
				DoFillNum(cmdWindow, num);
		}
		break;
	case CMD_UNSELECTALL:
		if (!isDocWindow)
			goto Error;
		docData = GetWRefCon(cmdWindow);
		HiliteSelectOff(cmdWindow);
		UnSelectAllObjects(docData);
		HiliteSelectOn(cmdWindow);
		break;
	case CMD_REQUEST1:
	case CMD_REQUEST2:
	case CMD_REQUEST3:
	case CMD_REQUESTTEXT:
		UserRequest(i, rexxMsg, argText);
		break;
	case CMD_SETPATH:
		if (strlen(argText) == 0 || (dir = Lock(argText, ACCESS_READ)) == NULL)
			goto Error;
		SetCurrentDir(dir);
		UnLock(dir);
		break;
	}
	goto Done;
/*
	Command not found
*/
Error:
	rexxMsg->rm_Result1 = RC_FATAL;
/*
	Reply to message
*/
Done:
	ReplyMsg((MsgPtr) rexxMsg);
}

/*
 *	Initiate AREXX macro
 *	If macroMsg is not NULL then we are already executing a macro, so ignore
 *	This routine will use a copy of macroName
 */

void DoMacro(TextPtr macroName, TextPtr arg)
{
	WORD		i, len, argCount;
	BOOL		success, hasPath;
	MsgPortPtr	rexxPort;
	TextChar	newMacroName[256];

	if (RexxSysBase == NULL || macroMsg)
		return;
/*
	Add path to program if no path specified
*/
	hasPath = FALSE;
	len = strlen(macroName);
	for (i = 0; i < len; i++) {
		if (macroName[i] == ':') {
			hasPath = TRUE;
			break;
		}
	}
	if (hasPath)
		strcpy(strBuff, macroName);
	else {
		strcpy(newMacroName, strMacroDrawer);
		strcat(newMacroName, macroName);
		SetPathName(strBuff, newMacroName, FALSE);
	}
/*
	Find REXX port and start macro
*/
	argCount = (arg && strlen(arg)) ? 2 : 1;
	success = FALSE;
	if ((macroMsg = (RexxMsgPtr) CreateRexxMsg(&rexxMsgPort, NULL, progPortName)) == NULL)
		goto Error;
	macroMsg->rm_Action = RXFUNC + argCount - 1;
	if (!fromCLI)
		macroMsg->rm_Action |= (1 << RXFB_NOIO);
	macroMsg->rm_Args[0] = (STRPTR) strBuff;
	macroMsg->rm_Args[1] = (STRPTR) arg;
	if (!FillRexxMsg(macroMsg, 1, 0))
		goto Error;
	Forbid();
	rexxPort = FindPort("REXX");
	if (rexxPort)
		PutMsg(rexxPort, (MsgPtr) macroMsg);
	Permit();
	if (rexxPort) {
		inMacro = success = TRUE;
		SetPointerShape();
	}
	else
		ClearRexxMsg(macroMsg, argCount);
Error:
	if (!success) {
		if (macroMsg) {
			DeleteRexxMsg(macroMsg);
			macroMsg = NULL;
		}
		Error((rexxPort) ? ERR_NO_MEM : ERR_NO_REXX);
	}
}

/*
 *	Do auto exec macro
 *	If auto exec macro does not exist, don't report an error
 */

void DoAutoExecMacro()
{
	LONG	lock;

	if (RexxSysBase == NULL)
		return;
	SetPathName(strBuff, strAutoExecName, TRUE);
	if ((lock = Lock(strBuff, ACCESS_READ)) != NULL) {
		UnLock(lock);
		DoMacro(strAutoExecName, NULL);
	}
}

/*
 *	Get macro name from user and execute
 */

static BOOL DoOtherMacro()
{
	BOOL		success;
	TextChar	macroName[GADG_MAX_STRING], arg[GADG_MAX_STRING];

	BeginWait();
	success = GetMacroName(screen, mainMsgPort, DialogFilter, macroName, arg);
	EndWait();
	if (success)
		DoMacro(macroName, arg);
	return (success);
}

/*
 *	Save FKeys file
 */

static void SaveFKeys()
{
	WORD		i, version;
	ULONG		fileID;
	BOOL		success;
	File		file;
	TextChar	pathName[200];

	success = FALSE;
	SetPathName(pathName, strFKeysName, TRUE);
	if ((file = Open(pathName, MODE_NEWFILE)) == NULL)
		goto Exit;
/*
	Write fileID and version
*/
	fileID = ID_FKEYSFILE;
	version = PROGRAM_VERSION;
	if (Write(file, &fileID, sizeof(ULONG)) != sizeof(ULONG) ||
		Write(file, &version, sizeof(WORD)) != sizeof(WORD))
		goto Exit;
/*
	Write FKey tables and data
*/
	for (i = 0; i < 10; i++) {
		if (Write(file, &fKeyTable1[i], sizeof(FKey)) != sizeof(FKey))
			goto Exit;
		if (fKeyTable1[i].Type == FKEY_MACRO &&
			Write(file, fKeyTable1[i].Data, 32) != 32)
			goto Exit;
	}
	for (i = 0; i < 10; i++) {
		if (Write(file, &fKeyTable2[i], sizeof(FKey)) != sizeof(FKey))
			goto Exit;
		if (fKeyTable2[i].Type == FKEY_MACRO &&
			Write(file, fKeyTable2[i].Data, 32) != 32)
			goto Exit;
	}
	success = TRUE;
/*
	All done
*/
Exit:
	if (file)
		Close(file);
	if (success)
		SaveIcon(pathName, ICON_FKEYS);
}

/*
 *	Load FKeys file
 */

void LoadFKeys()
{
	WORD		i, version;
	ULONG		fileID;
	File		file;
	FKey		fKey;
	TextChar	macroName[32], pathName[200];

	SetPathName(pathName, strFKeysName, TRUE);
	if ((file = Open(pathName, MODE_OLDFILE)) == NULL)
		goto Exit;
/*
	Read fileID and version
*/
	if (Read(file, &fileID, sizeof(ULONG)) != sizeof(ULONG) ||
		Read(file, &version, sizeof(WORD)) != sizeof(WORD) ||
		fileID != ID_FKEYSFILE || version != PROGRAM_VERSION)
		goto Exit;
/*
	Write FKey tables and data
*/
	for (i = 0; i < 10; i++) {
		if (Read(file, &fKey, sizeof(FKey)) != sizeof(FKey))
			goto Exit;
		if (fKey.Type == FKEY_MACRO &&
			Read(file, macroName, 32) != 32)
			goto Exit;
		fKeyTable1[i] = fKey;
		if (fKey.Type == FKEY_MACRO) {
			strncpy(strMacroNames1[i], macroName, 32);
			fKeyTable1[i].Data = strMacroNames1[i];
		}
	}
	for (i = 0; i < 10; i++) {
		if (Read(file, &fKey, sizeof(FKey)) != sizeof(FKey))
			goto Exit;
		if (fKey.Type == FKEY_MACRO &&
			Read(file, macroName, 32) != 32)
			goto Exit;
		fKeyTable2[i] = fKey;
		if (fKey.Type == FKEY_MACRO) {
			strncpy(strMacroNames2[i], macroName, 32);
			fKeyTable2[i].Data = strMacroNames2[i];
		}
	}
/*
	Set macro menu
*/
	for (i = 0; i < 10; i++)
		SetMenuItem(backWindow, MENUITEM(MACRO_MENU, i, NOSUB), strMacroNames2[i]);
/*
	All done
*/
Exit:
	if (file)
		Close(file);
}

/*
 *	Get custom macro names and insert in menu
 */

static BOOL DoCustomizeMacro(WindowPtr window)
{
	WORD		i, item;
	DialogPtr	dlg;
	TextChar	newNames[10][32];

/*
	Get macro name from user
*/
	BeginWait();
	if ((dlg = GetDialog(dlgList[DLG_CUSTOMMACRO], screen, mainMsgPort)) == NULL) {
		EndWait();
		Error(ERR_NO_MEM);
		return (FALSE);
	}
	OutlineOKButton(dlg);
	do {
		item = ModalDialog(mainMsgPort, dlg, DialogFilter);
	} while (item != OK_BUTTON && item != CANCEL_BUTTON && item != SAVE_BUTTON);
	for (i = 0; i < 10; i++) {
		GetEditItemText(dlg->FirstGadget, NAME1_TEXT + i, strBuff);
		strncpy(newNames[i], strBuff, 32);
		newNames[i][31] = '\0';
	}
	DisposeDialog(dlg);
	EndWait();
	if (item == CANCEL_BUTTON)
		return (FALSE);
/*
	Insert macro names into menu
*/
	for (i = 0; i < 10; i++) {
		strncpy(strMacroNames2[i], newNames[i], 32);
		SetMenuItem(backWindow, MENUITEM(MACRO_MENU, i, NOSUB), strMacroNames2[i]);
	}
	if (item == SAVE_BUTTON) {
		SetStdPointer(window, POINTER_WAIT);
		SaveFKeys();
	}
	return (TRUE);
}

/*
 *	Handle Macro menu
 */

BOOL DoMacroMenu(WindowPtr window, UWORD item, UWORD sub)
{
	BOOL	success;

	if (RexxSysBase == NULL || inMacro)
		return (FALSE);
	if (item >= 0 && item <= 9) {
		DoMacro(strMacroNames2[item], NULL);
		success = TRUE;
	}
	else if (item == OTHERMACRO_ITEM)
		success = DoOtherMacro();
	else if (item == CUSTOMMACRO_ITEM)
		success = DoCustomizeMacro(window);
	else
		success = FALSE;
	return (success);
}
