/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Toolbox
 *	Copyright (c) 1993 New Horizons Software, Inc.
 *
 *	Fixed-point transcendental routines
 */

#include <Toolbox/FixMath.h>

/*
 *	Sine table for 0 to 89 degrees
 *	(Has fractional portion of Fixed value)
 */

static UWORD sineTable[] = {
		0,	 1144,	 2287,	 3430,	 4572,	 5712,	 6850,	 7987,	 9121,	10252,
	11380,	12505,	13626,	14742,	15855,	16962,	18064,	19161,	20252,	21336,
	22415,	23486,	24550,	25607,	26656,	27697,	28729,	29753,	30767,	31772,
	32768,	33754,	34729,	35693,	36647,	37590,	38521,	39441,	40348,	41243,
	42126,	42995,	43852,	44695,	45525,	46341,	47143,	47930,	48703,	49461,
	50203,	50931,	51643,	52339,	53020,	53684,	54332,	54963,	55578,	56175,
	56756,	57319,	57865,	58393,	58903,	59396,	59870,	60326,	60764,	61183,
	61584,	61966,	62328,	62672,	62997,	63303,	63589,	63856,	64104,	64332,
	64540,	64729,	64898,	65048,	65177,	65287,	65376,	65446,	65496,	65526
};

/*
 *	Return sine of angle (in degrees)
 *	Note: Currently rounds angle to nearest degree
 */

Fixed FixSin(register Fixed angle)
{
	register Fixed	sine;
	register BOOL	negate;

/*
	Normalize angle to range 0..359
*/
	while (angle < 0)
		angle += Long2Fix(360);
	angle = FixRound(angle);
	while (angle >= 360)
		angle -= 360;
/*
	Adjust angle to look up in sine table
*/
	if (angle >= 180) {
		angle -= 180;
		negate = TRUE;
	}
	else
		negate = FALSE;
	if (angle > 90)
		angle = 180 - angle;
/*
	Get value
*/
	sine = (angle == 90) ? 0x10000 : (Fixed) sineTable[angle];
	if (negate)
		sine = -sine;
	return (sine);
}

/*
 *	Return cosine of angle (in degrees)
 */

Fixed FixCos(Fixed angle)
{
	return (FixSin(angle + Long2Fix(90)));
}
