/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software
 *
 *	Menu definitions
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <Toolbox/Menu.h>

#include "Draw.h"

#define MHEIGHT	10

extern TextPtr	macroTable1[], macroTable2[];

/*
 *	Project menu items
 */

static MenuItemTemplate projectItems[] = {
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'N', 0, 0, "New", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'O', 0, 0, "Open...", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'W', 0, 0, "Close", NULL },
	{ MENU_TEXT_ITEM, 0, 0, 0, 0, NULL, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Import Pict...", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Export Pict...", NULL },
	{ MENU_TEXT_ITEM, 0, 0, 0, 0, NULL, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'S', 0, 0, "Save", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Save As...", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Revert", NULL },
	{ MENU_TEXT_ITEM, 0, 0, 0, 0, NULL, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Page Setup...", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Print One", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Print...", NULL },
	{ MENU_TEXT_ITEM, 0, 0, 0, 0, NULL, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Save Defaults", NULL },
	{ MENU_TEXT_ITEM, 0, 0, 0, 0, NULL, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'Q', 0, 0, "Quit", NULL },
	{ MENU_NO_ITEM, 0, 0, 0, 0, NULL, NULL }
};

/*
 *	Edit menu items
 */

static MenuItemTemplate rotateItems[] = {
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Left", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Right", NULL },
	{ MENU_NO_ITEM, 0, 0, 0, 0, NULL, NULL }
};

static MenuItemTemplate flipItems[] = {
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Horizontal", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Vertical", NULL },
	{ MENU_NO_ITEM, 0, 0, 0, 0, NULL, NULL }
};
static MenuItemTemplate polyItems[] = {
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Close", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Open", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'E', 0, 0, "Smooth", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Unsmooth", NULL },
	{ MENU_NO_ITEM, 0, 0, 0, 0, NULL, NULL }
};

static MenuItemTemplate editItems[] = {
/*
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'Z', 0, 0, "Undo", NULL },
	{ MENU_TEXT_ITEM, 0, 0, 0, 0, NULL, NULL },
*/
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'X', 0, 0, "Cut", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'C', 0, 0, "Copy", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'V', 0, 0, "Paste", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Erase", NULL },
	{ MENU_TEXT_ITEM, 0, 0, 0, 0, NULL, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'D', 0, 0, "Duplicate", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Rotate", &rotateItems[0] },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Flip", &flipItems[0] },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Scale...", NULL },
	{ MENU_TEXT_ITEM, 0, 0, 0, 0, NULL, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Convert to Polygon", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Polygon", &polyItems[0] },
	{ MENU_TEXT_ITEM, 0, 0, 0, 0, NULL, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'A', 0, 0, "Select All", NULL },
	{ MENU_NO_ITEM, 0, 0, 0, 0, NULL, NULL }
};

/*
 *	Layout menu
 */

static MenuItemTemplate layoutItems[] = {
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKED, 0, 0, 0, "Normal Size", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 0, 0, 0, "Enlarge", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 0, 0, 0, "Reduce", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 0, 0, 0, "Fit to Window", NULL },
	{ MENU_TEXT_ITEM, 0, 0, 0, 0, NULL, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_TOGGLE, 0, 0, 0, "Grid Snap", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 0, 0, 0, "Grid Size...", NULL },
	{ MENU_TEXT_ITEM, 0, 0, 0, 0, NULL, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 0, 0, 0, "Pen Colors...", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 0, 0, 0, "Fill Patterns...", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 0, 0, 0, "Layers...", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 0, 0, 0, "Drawing Size...", NULL },
	{ MENU_TEXT_ITEM, 0, 0, 0, 0, NULL, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Preferences...", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Screen Colors...", NULL },
	{ MENU_NO_ITEM, 0, 0, 0, 0, NULL, NULL }
};

/*
 *	Objects menu
 */

static MenuItemTemplate arrangeItems[] = {
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'F', 0, 0, "Move Forward", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Move to Front", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'J', 0, 0, "Move Backward", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Move to Back", NULL },
	{ MENU_TEXT_ITEM, 0, 0, 0, 0, NULL, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'K', 0, 0, "Align to Grid", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Align Objects...", NULL },
	{ MENU_TEXT_ITEM, 0, 0, 0, 0, NULL, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'G', 0, 0, "Group", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Ungroup", NULL },
	{ MENU_TEXT_ITEM, 0, 0, 0, 0, NULL, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'L', 0, 0, "Lock", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Unlock", NULL },
	{ MENU_NO_ITEM, 0, 0, 0, 0, NULL, NULL }
};

/*
 *	Pen menu
 */

static MenuItemTemplate penSizeItems[] = {
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKED,   0, 0, 0x3E, " 1 Point", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE, 0, 0, 0x3D, " 2 Points", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE, 0, 0, 0x3B, " 4 Points", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE, 0, 0, 0x37, " 6 Points", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE, 0, 0, 0x2F, " 8 Points", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE, 0, 0, 0x1F, "10 Points", NULL },
	{ MENU_TEXT_ITEM, 0, 0, 0, 0, NULL, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 0, 0, 0x3F, "Other...", NULL },
	{ MENU_NO_ITEM, 0, 0, 0, 0, NULL, NULL }
};

static MenuItemTemplate penItems[] = {
	{ MENU_TEXT_ITEM, MENU_ENABLED, 0, 0, 0, "Width", &penSizeItems[0] },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 0, 0, 0, "Height", &penSizeItems[0] },
	{ MENU_TEXT_ITEM, 0, 0, 0, 0, NULL, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKED,   0, 0, 0x30, "No Arrows", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE, 0, 0, 0x08, "Arrow at Start", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE, 0, 0, 0x08, "Arrow at End", NULL },
	{ MENU_NO_ITEM, 0, 0, 0, 0, NULL, NULL }
};

/*
 *	Text menu
 */

static MenuItemTemplate fontItems[] = {
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'T', 0, 0, "Other...", NULL },
	{ MENU_TEXT_ITEM, 0, 0, 0, 0, NULL, NULL },
	{ MENU_NO_ITEM, 0, 0, 0, 0, NULL, NULL }
};

static MenuItemTemplate textItems[] = {
	{ MENU_TEXT_ITEM, MENU_ENABLED, 0, 0, 0, "Font", &fontItems[0] },
	{ MENU_TEXT_ITEM, 0, 0, 0, 0, NULL, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKED, 'P',              0, 0x38,
		"Plain", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_TOGGLE,  'B',       FSF_BOLD, 0x04,
		"Bold", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_TOGGLE,  'I',     FSF_ITALIC, 0x04,
		"Italic", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_TOGGLE,  'U', FSF_UNDERLINED, 0x04,
		"Underline", NULL },
	{ MENU_TEXT_ITEM, 0, 0, 0, 0, NULL, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKED,   '[', 0, 0x300, "Left Aligned", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE, '=', 0, 0x280, "Centered", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE, ']', 0, 0x180, "Right Aligned", NULL },
	{ MENU_TEXT_ITEM, 0, 0, 0, 0, NULL, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKED,   '1', 0, 0x3000, "Single Space", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE,   0, 0, 0x2800, "1-1/2 Space", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_CHECKABLE, '2', 0, 0x1800, "Double Space", NULL },
	{ MENU_NO_ITEM, 0, 0, 0, 0, NULL, NULL }
};

/*
 *	View menu
 */

static MenuItemTemplate viewItems[] = {
	{ MENU_TEXT_ITEM, MENU_ENABLED, 0, 0, 0, "About DesignWorks...", NULL },
	{ MENU_TEXT_ITEM, 0, 0, 0, 0, NULL, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_TOGGLE,   0, 0, 0, "Show Back Layers", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_TOGGLE, 'R', 0, 0, "Show Rulers", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_TOGGLE,   0, 0, 0, "Show Grid Lines", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_TOGGLE,   0, 0, 0, "Show Page Breaks", NULL },
	{ MENU_TEXT_ITEM, 0, 0, 0, 0, NULL, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_TOGGLE,   0, 0, 0, "Show Toolbox", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_TOGGLE,   0, 0, 0, "Show Pen Palette", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED | MENU_TOGGLE,   0, 0, 0, "Show Fill Palette", NULL },
	{ MENU_TEXT_ITEM, 0, 0, 0, 0, NULL, NULL },
	{ MENU_NO_ITEM, 0, 0, 0, 0, NULL, NULL }
};
	
/*
 *	Macro menu
 */

static MenuItemTemplate macroItems[] = {
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Macro_1", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Macro_2", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Macro_3", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Macro_4", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Macro_5", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Macro_6", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Macro_7", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Macro_8", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Macro_9", NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED,   0, 0, 0, "Macro_10", NULL },
	{ MENU_TEXT_ITEM, 0, 0, 0, 0, NULL, NULL },
	{ MENU_TEXT_ITEM, MENU_ENABLED, 'M', 0, 0, "Other...", NULL },
	{ MENU_NO_ITEM, 0, 0, 0, 0, NULL, NULL }
};

/*
 *	Menu strip templates for document window
 */

MenuTemplate docWindMenus[] = {
	{ " Project ", &projectItems[0] },
	{ " Edit ", &editItems[0] },
	{ " Layout ", &layoutItems[0] },
	{ " Arrange ", &arrangeItems[0] },
	{ " Pen ", &penItems[0] },
	{ " Text ", &textItems[0] },
	{ " View ", &viewItems[0] },
	{ " Macro ", &macroItems[0] },
	{ NULL, NULL }
};

MenuTemplate altWindMenus[] = {			/* For lo-res screens */
	{ " Pr", &projectItems[0] },
	{ " Ed", &editItems[0] },
	{ " Ly", &layoutItems[0] },
	{ " Ar", &arrangeItems[0] },
	{ " Pn", &penItems[0] },
	{ " Tx", &textItems[0] },
	{ " Vw", &viewItems[0] },
	{ " Mc", &macroItems[0] },
	{ NULL, NULL }
};
