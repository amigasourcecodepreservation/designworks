/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Toolbox library
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Color picker routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>

#include "Window.h"
#include "Dialog.h"
#include "Utility.h"
#include "Color.h"

/*
 *	External variables
 */

extern WORD	_tbDarkColor;

/*
 *	Local variables and definitions
 */

#define RED(rgb)	(((rgb) >> 8) & 0x0F)
#define GREEN(rgb)	(((rgb) >> 4) & 0x0F)
#define BLUE(rgb)	((rgb) & 0x0F)

#define RGBCOLOR(r,g,b)	(((r) << 8) + ((g) << 4) + (b))

static GadgetTemplate colorGadgets[] = {
	{ GADG_PUSH_BUTTON,  20, -30, 0, 0,  60, 20, 0, 0, "OK" },
	{ GADG_PUSH_BUTTON, 100, -30, 0, 0,  60, 20, 0, 0, "Cancel" },

	{ GADG_STAT_BORDER,  70,  30, 0, 0,  20, 20, IMAGE_ARROW_WIDTH, 0, NULL },

	{ GADG_PROP_HORIZ,  180,  45, 0, 0, 150, 10, 0, 0, NULL },
	{ GADG_PROP_HORIZ,  180,  90, 0, 0, 150, 10, 0, 0, NULL },
	{ GADG_PROP_HORIZ,  180, 135, 0, 0, 150, 10, 0, 0, NULL },

	{ GADG_ACTIVE_STDIMAGE,
		 80,  70, 0, -IMAGE_ARROW_HEIGHT/2, 0, 0, IMAGE_ARROW_WIDTH, IMAGE_ARROW_HEIGHT,
		(Ptr) IMAGE_UP_ARROW },
	{ GADG_ACTIVE_STDIMAGE,
		 80,  70, 0, IMAGE_ARROW_HEIGHT/2 + 1, 0, 0, IMAGE_ARROW_WIDTH, IMAGE_ARROW_HEIGHT,
		(Ptr) IMAGE_DOWN_ARROW },
	{ GADG_STAT_TEXT | GADG_NO_REPORT, 90, 70, IMAGE_ARROW_WIDTH, 0, 0, 0, 0, 0, NULL },

	{ GADG_ACTIVE_STDIMAGE,
		 80, 100, 0, -IMAGE_ARROW_HEIGHT/2, 0, 0, IMAGE_ARROW_WIDTH, IMAGE_ARROW_HEIGHT,
		(Ptr) IMAGE_UP_ARROW },
	{ GADG_ACTIVE_STDIMAGE,
		 80, 100, 0, IMAGE_ARROW_HEIGHT/2 + 1, 0, 0, IMAGE_ARROW_WIDTH, IMAGE_ARROW_HEIGHT,
		(Ptr) IMAGE_DOWN_ARROW },
	{ GADG_STAT_TEXT | GADG_NO_REPORT, 90, 100, IMAGE_ARROW_WIDTH, 0, 0, 0, 0, 0, NULL },

	{ GADG_ACTIVE_STDIMAGE,
		 80, 130, 0, -IMAGE_ARROW_HEIGHT/2, 0, 0, IMAGE_ARROW_WIDTH, IMAGE_ARROW_HEIGHT,
		(Ptr) IMAGE_UP_ARROW },
	{ GADG_ACTIVE_STDIMAGE,
		 80, 130, 0, IMAGE_ARROW_HEIGHT/2 + 1, 0, 0, IMAGE_ARROW_WIDTH, IMAGE_ARROW_HEIGHT,
		(Ptr) IMAGE_DOWN_ARROW },
	{ GADG_STAT_TEXT | GADG_NO_REPORT, 90, 130, IMAGE_ARROW_WIDTH, 0, 0, 0, 0, 0, NULL },

	{GADG_STAT_TEXT | GADG_NO_REPORT,  20,  10, 0, 0, 0, 0, 0, 0, NULL },

	{GADG_STAT_TEXT | GADG_NO_REPORT, 180,  30, 0, 0, 0, 0, 0, 0, "Hue" },
	{GADG_STAT_TEXT | GADG_NO_REPORT, 180,  75, 0, 0, 0, 0, 0, 0, "Brightness" },
	{GADG_STAT_TEXT | GADG_NO_REPORT, 180, 120, 0, 0, 0, 0, 0, 0, "Saturation" },
	{GADG_STAT_TEXT | GADG_NO_REPORT,  20,  70, 0, 0, 0, 0, 0, 0, "Red:" },
	{GADG_STAT_TEXT | GADG_NO_REPORT,  20, 100, 0, 0, 0, 0, 0, 0, "Green:" },
	{GADG_STAT_TEXT | GADG_NO_REPORT,  20, 130, 0, 0, 0, 0, 0, 0, "Blue:" },

	{ GADG_ITEM_NONE }
};

static DialogTemplate colorDlgTempl = {
	DLG_TYPE_ALERT, 0, -1, -1, 350, 190, &colorGadgets[0], NULL
};

enum {
	SAMPLE_USERITEM = 2,
	HUE_SLIDER,		BRIGHT_SLIDER,		SATURATE_SLIDER,
	REDUP_ARROW,	REDDOWN_ARROW,		RED_TEXT,
	GREENUP_ARROW,	GREENDOWN_ARROW,	GREEN_TEXT,
	BLUEUP_ARROW,	BLUEDOWN_ARROW,		BLUE_TEXT,
	PROMPT_TEXT
};

static DialogPtr	colorDlg;
static BOOL			(*dialogFilter)(IntuiMsgPtr, WORD *);

/*
 *	Local prototypes
 */

WORD	Max3(WORD, WORD, WORD);
WORD	Min3(WORD, WORD, WORD);
void	RGBtoHSV(UWORD, UWORD, UWORD, UWORD *, UWORD *, UWORD *);
void	HSVtoRGB(UWORD, UWORD, UWORD, UWORD *, UWORD *, UWORD *);

void	AdjustColorSliders(DialogPtr, WORD, WORD, WORD);
void	GetColorSliders(DialogPtr, WORD *, WORD *, WORD *);
void	ShowColorValues(DialogPtr, WORD, WORD, WORD);
void	DrawArrowBorder(DialogPtr, WORD);
void	SetRGBColor(ScreenPtr, WORD, RGBColor, RGBColor (*)(RGBColor));
BOOL	ColorDialogFilter(IntuiMsgPtr, WORD *);

/*
 *	Return maximum of three values
 */

static WORD Max3(register WORD a, register WORD b, register WORD c)
{
	register WORD max;

	max = a;
	if (max < b) max = b;
	if (max < c) max = c;
	return (max);
}

/*
 *	Return minimum of three values
 */

static WORD Min3(register WORD a, register WORD b, register WORD c)
{
	register WORD min;

	min = a;
	if (min > b) min = b;
	if (min > c) min = c;
	return (min);
}

/*
 *	Convert from rgb values (in range 0-15)
 *		to hsv (h in range 0-0xFFFF; s, v in range 0-15)
 */

static void RGBtoHSV(UWORD r, UWORD g, UWORD b, UWORD *pH, UWORD *pS, UWORD *pV)
{
	register UWORD v, s, m;
	register LONG r1, g1, b1;
	LONG h;

	v = Max3(r, g, b);
	m = Min3(r, g, b);
	s = (v - m)*15; s = (v) ? s/v : 0;
	if (s) {
		r1 = (v - r)*0x10000L; r1 /= (v - m);
		g1 = (v - g)*0x10000L; g1 /= (v - m);
		b1 = (v - b)*0x10000L; b1 /= (v - m);
		if (v == r)
			h = (m == g) ? 0x50000L + b1 : 0x10000L - g1;
		else if (v == g)
			h = (m == b) ? 0x10000L + r1 : 0x30000L - b1;
		else		/* v == b */
			h = (m == r) ? 0x30000L + g1 : 0x50000L - r1;
	}
	else
		h = 0;				/* Undefined hue */
	*pH = (h + 3)/6; *pS = s; *pV = v;
}

/*
 *	Convert hsv to rgb values
 */

static void HSVtoRGB(UWORD h, UWORD s, UWORD v, UWORD *pR, UWORD *pG, UWORD *pB)
{
	register UWORD r, g, b;
	register LONG p1, p2, p3;
	LONG i, f;

	i = h*6; i /= 0x10000L;		/* Don't round */
	f = h*6 - i*0x10000L;
	p1 = v*(15 - s);						p1 = (p1 + 7)/15;
	p2 = v*(15*0x10000L - s*f);				p2 = (p2 + 15*0x8000L)/(15*0x10000L);
	p3 = v*(15*0x10000L - s*(0x10000L - f));p3 = (p3 + 15*0x8000L)/(15*0x10000L);
	switch (i) {
	case 0:
		r = v;  g = p3; b = p1;
		break;
	case 1:
		r = p2; g = v;  b = p1;
		break;
	case 2:
		r = p1; g = v;  b = p3;
		break;
	case 3:
		r = p1; g = p2; b = v;
		break;
	case 4:
		r = p3; g = p1; b = v;
		break;
	case 5:
		r = v;  g = p1; b = p2;
		break;
	}
	*pR = r; *pG = g; *pB = b;
}

/*
 *	Adjust Color and Brightness sliders to actual color
 */

static void AdjustColorSliders(DialogPtr dlg, WORD red, WORD green, WORD blue)
{
	WORD hue, sat, value; /* Need to take address of these */
	LONG newPot, newBody;
	GadgetPtr gadget, gadgList;
	PropInfoPtr propInfo;

	gadgList = dlg->FirstGadget;
	RGBtoHSV(red, green, blue, &hue, &sat, &value);
/*
	Adjust color slider
*/
	gadget = GadgetItem(gadgList, HUE_SLIDER);
	propInfo = (PropInfoPtr) gadget->SpecialInfo;
	if (sat == 0) {
		newPot = 0xFFFF;
		newBody = 0xFFFF;
	}
	else {
		newPot = hue;
		newBody = 0x1111;
	}
	if (newPot != propInfo->HorizPot || newBody != propInfo->HorizBody)
		NewModifyProp(gadget, dlg, NULL, AUTOKNOB | FREEHORIZ,
					  newPot, 0, newBody, 0xFFFF, 1);
/*
	Adjust brightness slider
*/
	gadget = GadgetItem(gadgList, BRIGHT_SLIDER);
	propInfo = (PropInfoPtr) gadget->SpecialInfo;
	newPot = value*0xFFFFL;
	newPot /= 15;
	newBody = 0x1111;
	if (newPot != propInfo->HorizPot || newBody != propInfo->HorizBody)
		NewModifyProp(gadget, dlg, NULL, AUTOKNOB | FREEHORIZ,
					  newPot, 0, newBody, 0xFFFF, 1);
/*
	Adjust saturation slider
*/
	gadget = GadgetItem(gadgList, SATURATE_SLIDER);
	propInfo = (PropInfoPtr) gadget->SpecialInfo;
	newPot = sat*0xFFFFL;
	newPot /= 15;
	newBody = 0x1111;
 	if (newPot != propInfo->HorizPot || newBody != propInfo->HorizBody)
		NewModifyProp(gadget, dlg, NULL, AUTOKNOB | FREEHORIZ,
					  newPot, 0, newBody, 0xFFFF, 1);
}

/*
 *	Get color slider settings
 */

static void GetColorSliders(DialogPtr dlg, WORD *red, WORD *green, WORD *blue)
{
	WORD hue, sat, value;
	GadgetPtr gadget, gadgList;
	PropInfoPtr propInfo;

	gadgList = dlg->FirstGadget;
/*
	Get new color values
*/
	gadget = GadgetItem(gadgList, HUE_SLIDER);
	propInfo = (PropInfoPtr) gadget->SpecialInfo;
	hue = propInfo->HorizPot;
	gadget = GadgetItem(gadgList, SATURATE_SLIDER);
	propInfo = (PropInfoPtr) gadget->SpecialInfo;
	sat = ((LONG) (propInfo->HorizPot)*15L + 0x7FFFL)/0xFFFFL;
	gadget = GadgetItem(gadgList, BRIGHT_SLIDER);
	propInfo = (PropInfoPtr) gadget->SpecialInfo;
	value = ((LONG) (propInfo->HorizPot)*15L + 0x7FFFL)/0xFFFFL;
	HSVtoRGB(hue, sat, value, red, green, blue);
}

/*
 *	Show new rgb values (and enable/disable arrows)
 */

static void ShowColorValues(DialogPtr dlg, WORD red, WORD green, WORD blue)
{
	register GadgetPtr gadget, gadgList;
	TextChar text[10];

	gadgList = dlg->FirstGadget;
	NumToString(red, text);
	gadget = GadgetItem(gadgList, RED_TEXT);
	SetGadgetText(gadget, dlg, NULL, text);
	NumToString(green, text);
	gadget = GadgetItem(gadgList, GREEN_TEXT);
	SetGadgetText(gadget, dlg, NULL, text);
	NumToString(blue, text);
	gadget = GadgetItem(gadgList, BLUE_TEXT);
	SetGadgetText(gadget, dlg, NULL, text);
	EnableGadgetItem(gadgList, REDUP_ARROW, dlg, NULL, (red < 15));
	EnableGadgetItem(gadgList, REDDOWN_ARROW, dlg, NULL, (red > 0));
	EnableGadgetItem(gadgList, GREENUP_ARROW, dlg, NULL, (green < 15));
	EnableGadgetItem(gadgList, GREENDOWN_ARROW, dlg, NULL, (green > 0));
	EnableGadgetItem(gadgList, BLUEUP_ARROW, dlg, NULL, (blue < 15));
	EnableGadgetItem(gadgList, BLUEDOWN_ARROW, dlg, NULL, (blue > 0));
}

/*
 *	Draw border around dialog up/down arrows, given gadget number of up arrow
 */

static void DrawArrowBorder(DialogPtr dlg, WORD gadgNum)
{
	RastPtr rPort = dlg->RPort;
	GadgetPtr gadget;
	BorderPtr border;

	gadget = GadgetItem(dlg->FirstGadget, gadgNum);
	if ((border = BoxBorder(gadget->Width, (WORD) (2*gadget->Height), _tbDarkColor, -1))
		!= NULL) {
		DrawBorder(rPort, border, gadget->LeftEdge, gadget->TopEdge);
		FreeBorder(border);
	}
}

/*
 *	Set specified color register to given RGB color
 *	Color given is color corrected
 */

static void SetRGBColor(ScreenPtr scrn, WORD colorNum, RGBColor rgbColor,
						RGBColor (*colorCorrect)(RGBColor))
{
	if (colorCorrect)
		rgbColor = (*colorCorrect)(rgbColor);
	SetRGB4(&scrn->ViewPort, colorNum, RED(rgbColor), GREEN(rgbColor), BLUE(rgbColor));
}

/*
 *	Color dialog filter
 */

static BOOL ColorDialogFilter(register IntuiMsgPtr intuiMsg, WORD *item)
{
	register WORD itemHit;
	register ULONG class;

	class = intuiMsg->Class;
	if (intuiMsg->IDCMPWindow == colorDlg && (class == GADGETDOWN || class == GADGETUP)) {
		itemHit = GadgetNumber((GadgetPtr) intuiMsg->IAddress);
		if (itemHit == REDUP_ARROW || itemHit == REDDOWN_ARROW ||
			itemHit == GREENUP_ARROW || itemHit == GREENDOWN_ARROW ||
			itemHit == BLUEUP_ARROW || itemHit == BLUEDOWN_ARROW) {
			ReplyMsg((MsgPtr) intuiMsg);
			*item = (class == GADGETDOWN) ? itemHit : -1;
			return (TRUE);
		}
	}
	return ((*dialogFilter)(intuiMsg, item));
}

/*
 *	Get new color
 *	A penNum of -1 means to choose one
 *	If an actual penNum is specified, don't reset color on exit
 */

BOOL GetColor(ScreenPtr scrn, MsgPortPtr msgPort,
			  BOOL (*dlgFilter)(IntuiMsgPtr, WORD *), TextPtr prompt,
			  RGBColor (*colorCorrect)(RGBColor),
			  RGBColor inColor, RGBColor *outColor, WORD penNum)
{
	WORD item, numColors, red, green, blue;
	BOOL done, hasPen;
	RGBColor origColor, newColor;
	GadgetPtr gadget, gadgList, hueGadg, briGadg, satGadg;
	BorderPtr border;
	RastPtr rPort;

	dialogFilter = dlgFilter;
	red = RED(inColor);
	green = GREEN(inColor);
	blue = BLUE(inColor);
	numColors = 1 << scrn->BitMap.Depth;
	if (penNum == -1) {
		hasPen = FALSE;
		penNum = (numColors >= 8) ? numColors - 3 : numColors - 1;
	}
	else
		hasPen = TRUE;
	origColor = GetRGB4(scrn->ViewPort.ColorMap, penNum);
/*
	Get dialog
*/
	if ((colorDlg = GetDialog(&colorDlgTempl, scrn, msgPort)) == NULL)
		return (FALSE);
	OutlineButton(GadgetItem(colorDlg->FirstGadget, 0), colorDlg, NULL, TRUE);
	SetRGBColor(scrn, penNum, inColor, colorCorrect);
/*
	Draw dialog contents
*/
	rPort = colorDlg->RPort;
	gadgList = colorDlg->FirstGadget;
	gadget = GadgetItem(gadgList, PROMPT_TEXT);
	SetGadgetText(gadget, colorDlg, NULL, prompt);
	gadget = GadgetItem(gadgList, SAMPLE_USERITEM);
	if ((border = BoxBorder(gadget->Width, gadget->Height, _tbDarkColor, -1)) != NULL) {
		DrawBorder(rPort, border, gadget->LeftEdge, gadget->TopEdge);
		FreeBorder(border);
	}
	SetAPen(rPort, penNum);
	SetDrMd(rPort, JAM1);
	RectFill(rPort, gadget->LeftEdge, gadget->TopEdge,
			 gadget->LeftEdge + gadget->Width - 1, gadget->TopEdge + gadget->Height - 1);
	DrawArrowBorder(colorDlg, REDUP_ARROW);
	DrawArrowBorder(colorDlg, GREENUP_ARROW);
	DrawArrowBorder(colorDlg, BLUEUP_ARROW);
	ShowColorValues(colorDlg, red, green, blue);
	AdjustColorSliders(colorDlg, red, green, blue);
	hueGadg = GadgetItem(gadgList, HUE_SLIDER);
	briGadg = GadgetItem(gadgList, BRIGHT_SLIDER);
	satGadg = GadgetItem(gadgList, SATURATE_SLIDER);
	if ((border = BoxBorder(hueGadg->Width, hueGadg->Height, _tbDarkColor, -1)) != NULL) {
		DrawBorder(rPort, border, hueGadg->LeftEdge, hueGadg->TopEdge);
		DrawBorder(rPort, border, briGadg->LeftEdge, briGadg->TopEdge);
		DrawBorder(rPort, border, satGadg->LeftEdge, satGadg->TopEdge);
		FreeBorder(border);
	}
/*
	Handle requester
*/
	done = FALSE;
	do {
		WaitPort(msgPort);
		item = CheckDialog(msgPort, colorDlg, ColorDialogFilter);
		switch (item) {
		case OK_BUTTON:
		case CANCEL_BUTTON:
			done = TRUE;
			break;
		case HUE_SLIDER:
		case BRIGHT_SLIDER:
		case SATURATE_SLIDER:
			GetColorSliders(colorDlg, &red, &green, &blue);
			break;
		case REDUP_ARROW:
		case REDDOWN_ARROW:
			if (item == REDUP_ARROW) {
				if (red < 15)
					red++;
			}
			else {
				if (red > 0)
					red--;
			}
			break;
		case GREENUP_ARROW:
		case GREENDOWN_ARROW:
			if (item == GREENUP_ARROW) {
				if (green < 15)
					green++;
			}
			else {
				if (green > 0)
					green--;
			}
			break;
		case BLUEUP_ARROW:
		case BLUEDOWN_ARROW:
			if (item == BLUEUP_ARROW) {
				if (blue < 15)
					blue++;
			}
			else {
				if (blue > 0)
					blue--;
			}
			break;
		}
		*outColor = RGBCOLOR(red, green, blue);
		if (item != -1) {
			SetRGBColor(scrn, penNum, *outColor, colorCorrect);
			ShowColorValues(colorDlg, red, green, blue);
			AdjustColorSliders(colorDlg, red, green, blue);
		}
		else if ((hueGadg->Flags & SELECTED) || (briGadg->Flags & SELECTED) ||
				 (satGadg->Flags & SELECTED)) {
			GetColorSliders(colorDlg, &red, &green, &blue);
			if ((newColor = RGBCOLOR(red, green, blue)) != *outColor) {
				*outColor = newColor;
				SetRGBColor(scrn, penNum, *outColor, colorCorrect);
				ShowColorValues(colorDlg, red, green, blue);
			}
		}
	} while (!done);
	if (item == CANCEL_BUTTON || !hasPen)
		SetRGB4(&scrn->ViewPort, penNum, RED(origColor), GREEN(origColor), BLUE(origColor));
	DisposeDialog(colorDlg);
	if (item == CANCEL_BUTTON)
		return (FALSE);
	return (TRUE);
}
