/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Mouse routines
 */

#include <exec/types.h>
#include <graphics/gfxmacros.h>
#include <intuition/intuition.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>

#include <Toolbox/Graphics.h>
#include <Toolbox/Window.h>
#include <Toolbox/Utility.h>

#include "Draw.h"
#include "Proto.h"

/*
 *	External variables
 */

extern ScreenPtr	screen;
extern MsgPortPtr	mainMsgPort;

extern WindowPtr	toolWindow, penWindow, fillWindow;

extern WORD	drawTool;

extern BOOL	titleChanged;
extern BOOL	doubleClick, tripleClick;

extern Rectangle	dragRect;		// In document coordinates
extern BOOL			autoScroll;

/*
 *	Local variables and definitions
 */

static ULONG	prevSecs, prevMicros;		// Inited to 0
static WORD		prevMouseX, prevMouseY;		// Inited to 0

static BOOL	sizing, dragging, wasMoved;

static UWORD	selLinePat;

/*
 *	Local prototypes
 */

static BOOL	AddObjectHandle(WindowPtr, DocObjPtr, PointPtr);
static void	RemObjectHandle(WindowPtr, DocObjPtr, PointPtr, RectPtr);
static void	GrowObject(WindowPtr, DocObjPtr, UWORD, WORD);

static void	SelectTrack(WindowPtr, PointPtr, PointPtr, BOOL);
static void	DoSelectBox(WindowPtr, UWORD, WORD, WORD);

static void	DragTrack(WindowPtr, PointPtr, PointPtr, BOOL);
static void	DoDrag(WindowPtr, WORD, WORD);

static void	DoSelect(WindowPtr, UWORD, WORD, WORD);

static DocObjPtr	CreateObject(WindowPtr, UWORD, WORD, WORD);

static void	DoCreate(WindowPtr, UWORD, WORD, WORD);

static void DoCarryFill(WindowPtr, UWORD, WORD, WORD);

/*
 *	Set frame rectangle from start and end doc coordinates
 */

void SetFrameRect(RectPtr rect, WORD xStart, WORD yStart, WORD xEnd, WORD yEnd)
{
	rect->MinX = MIN(xStart, xEnd);
	rect->MinY = MIN(yStart, yEnd);
	rect->MaxX = MAX(xStart, xEnd);
	rect->MaxY = MAX(yStart, yEnd);
}

/*
 *	Get rectangle containing all selected objects (in document coordinates)
 *	Return FALSE if no object selected
 */

BOOL GetSelectRect(DocDataPtr docData, RectPtr rect)
{
	BOOL success;
	DocObjPtr docObj;
	Rectangle newRect;

	success = FALSE;
	for (docObj = FirstSelected(docData); docObj; docObj = NextSelected(docObj)) {
		newRect = docObj->Frame;
		if (!success)
			*rect = newRect;
		else
			UnionRect(&newRect, rect, rect);
		success = TRUE;
	}
	return (success);
}

/*
 *	Track mouse movement and draw object outline
 *	Constrain to square if SHIFT key down (or to horizontal/vertical line)
 *	If not dragging, this routine also sets dragRect to frame of mouse start/end
 *	Return final mouse position
 */

void TrackMouse(WindowPtr window, UWORD modifier, PointPtr pt,
				void (*trackDraw)(WindowPtr, PointPtr, PointPtr, BOOL),
				void (*constrain)(PointPtr, PointPtr))
{
	WORD		xOffset, yOffset, width, height;
	WORD		right, bottom;
	Point		mousePt, pt1, pt2, newPt;
	DocDataPtr	docData = GetWRefCon(window);
	Rectangle	origRect, contentRect;

	if (trackDraw == NULL)
		return;
	GetContentRect(window, &contentRect);
	DocToWindow(window, pt->x, pt->y, &mousePt.x, &mousePt.y);
	pt1 = pt2 = *pt;
	right = docData->DocWidth - 1;;
	bottom = docData->DocHeight - 1;
	if (dragging) {
		xOffset = pt1.x - dragRect.MinX;
		yOffset = pt1.y - dragRect.MinY;
		width = dragRect.MaxX - dragRect.MinX;
		height = dragRect.MaxY - dragRect.MinY;
		origRect = dragRect;
	}
	else
		xOffset = yOffset = width = height = 0;
/*
	Draw new ruler indicators
*/
	RulerIndicOff(window);
	if (!sizing)
		docData->Flags |= DOC_DRAGGING;		// So ruler indics will show rect
	if (!dragging)
		SetFrameRect(&dragRect, pt1.x, pt1.y, pt2.x, pt2.y);
	RulerIndicOn(window);
/*
	Track mouse movement and update display
	Try to minimize drawing operations
*/
	(*trackDraw)(window, &pt1, &pt2, TRUE);
	while (WaitMouseUp(mainMsgPort, window)) {
		WaitBOVP(&screen->ViewPort);			// Avoid beam collision
		if (mousePt.x == window->MouseX && mousePt.y == window->MouseY &&
			PtInRect(&mousePt, &contentRect)) {
			(*trackDraw)(window, &pt1, &pt2, FALSE);
			continue;
		}
/*
	Get new position in document
*/
		mousePt.x = window->MouseX;
		mousePt.y = window->MouseY;
		if (!PtInRect(&mousePt, &contentRect) && !ObjectsLocked(docData))
			(*trackDraw)(window, &pt1, &pt2, TRUE);
		if (autoScroll) {
			if (mousePt.x < contentRect.MinX)
				ScrollLeft(window, FALSE);
			else if (mousePt.x > contentRect.MaxX)
				ScrollRight(window, FALSE);
			if (mousePt.y < contentRect.MinY)
				ScrollUp(window, FALSE);
			else if (mousePt.y > contentRect.MaxY)
				ScrollDown(window, FALSE);
		}
		WindowToDoc(window, mousePt.x, mousePt.y, &newPt.x, &newPt.y);
		SnapToGrid(docData, &newPt.x, &newPt.y);
/*
	Clip position to edges of document
*/
		if (newPt.x - xOffset < 0)
			newPt.x = xOffset;
		else if (newPt.x + width - xOffset > right)
			newPt.x = right - width + xOffset;
		if (newPt.y - yOffset < 0)
			newPt.y = yOffset;
		else if (newPt.y + height - yOffset > bottom)
			newPt.y = bottom - height + yOffset;
/*
	Apply constraint
*/
		if ((modifier & SHIFTKEYS) && constrain)
			(*constrain)(&pt1, &newPt);
/*
	Set new drag rect and update display
	If any objects are locked, don't modify
*/
		if (EqualPt(&pt2, &newPt) && PtInRect(&mousePt, &contentRect))
			continue;
		if (ObjectsLocked(docData)) {
			Error(ERR_OBJ_LOCKED);
			break;
		}
		if (PtInRect(&mousePt, &contentRect))
			(*trackDraw)(window, &pt1, &pt2, TRUE);
		pt2 = newPt;
		if (dragging) {
			dragRect = origRect;
			OffsetRect(&dragRect, pt2.x - pt1.x, pt2.y - pt1.y);
		}
		else
			SetFrameRect(&dragRect, pt1.x, pt1.y, pt2.x, pt2.y);
		(*trackDraw)(window, &pt1, &pt2, TRUE);
		UpdateRulerIndic(window);
	}
	(*trackDraw)(window, &pt1, &pt2, TRUE);
/*
	Restore old ruler indicator
*/
	RulerIndicOff(window);
	docData->Flags &= ~DOC_DRAGGING;
	RulerIndicOn(window);
/*
	Return final position
*/
	*pt = pt2;
}

/*
 *	Attempt to add object handle
 *	Return TRUE if successful
 *	Only succeeds for polys
 */

static BOOL AddObjectHandle(WindowPtr window, DocObjPtr docObj, Point *pt)
{
	BOOL	added;
	Point	newPt;

	if (docObj->Type != TYPE_POLY)
		return (FALSE);
	if (ObjectLocked(docObj)) {
		Error(ERR_OBJ_LOCKED);
		return (FALSE);
	}
	PolyHilite(window, (PolyObjPtr) docObj);
	newPt.x = pt->x - docObj->Frame.MinX;
	newPt.y = pt->y - docObj->Frame.MinY;
	added = PolyAddHandle((PolyObjPtr) docObj, &newPt);
	PolyHilite(window, (PolyObjPtr) docObj);
	return (added);
}

/*
 *	Remove object handle
 *	Only succeeds for polys
 */

static void RemObjectHandle(WindowPtr window, DocObjPtr docObj, PointPtr pt, RectPtr handleRect)
{
	WORD handle;
	Point newPt;

	if (docObj->Type != TYPE_POLY)
		return;
	if (ObjectLocked(docObj)) {
		Error(ERR_OBJ_LOCKED);
		return;
	}
	PolyHilite(window, (PolyObjPtr) docObj);
	InvalObjectRect(window, docObj);
	newPt.x = pt->x - docObj->Frame.MinX;
	newPt.y = pt->y - docObj->Frame.MinY;
	handle = PolyHandle((PolyObjPtr) docObj, &newPt, handleRect);
	PolyRemHandle((PolyObjPtr) docObj, handle);
	InvalObjectRect(window, docObj);
	PolyHilite(window, (PolyObjPtr) docObj);
}

/*
 *	Handle mouse down in object grow handle
 *	Grow routines must call HiliteSelectOff/On when changing frame rect
 */

static void GrowObject(WindowPtr window, DocObjPtr docObj, UWORD modifier, WORD handle)
{
	if (ObjectLocked(docObj)) {
		Error(ERR_OBJ_LOCKED);
		return;
	}
	sizing = TRUE;
	SetStdPointer(window, POINTER_CROSS);
	switch (docObj->Type) {
	case TYPE_GROUP:
		GroupGrow(window, (GroupObjPtr) docObj, modifier, handle);
		break;
	case TYPE_LINE:
		LineGrow(window, (LineObjPtr) docObj, modifier, handle);
		break;
	case TYPE_RECT:
		RectGrow(window, (RectObjPtr) docObj, modifier, handle);
		break;
	case TYPE_OVAL:
		OvalGrow(window, (OvalObjPtr) docObj, modifier, handle);
		break;
	case TYPE_POLY:
		PolyGrow(window, (PolyObjPtr) docObj, modifier, handle);
		break;
	case TYPE_TEXT:
		TextGrow(window, (TextObjPtr) docObj, modifier, handle);
		break;
	case TYPE_BMAP:
		BMapGrow(window, (BMapObjPtr) docObj, modifier, handle);
		break;
	case TYPE_EPSF:
		EPSFGrow(window, (EPSFObjPtr) docObj, modifier, handle);
		break;
	}
	SetStdPointer(window, POINTER_WAITDELAY);
	UpdateWindow(window);			// Do it here for crisper response
	sizing = FALSE;
	SetPointerShape();
}

/*
 *	Draw routine for tracking selection
 */

static void SelectTrack(WindowPtr window, PointPtr pt1, PointPtr pt2, BOOL change)
{
	RastPtr		rPort = window->RPort;
	Rectangle	rect;

	SetFrameRect(&rect, pt1->x, pt1->y, pt2->x, pt2->y);
	DocToWindowRect(window, &rect, &rect);
	SetDrPt(rPort, selLinePat);
	SetDrawComplement(rPort);
	FrameRect(rPort, &rect);
	if (!change) {
		selLinePat = (selLinePat >> 2) | (selLinePat << 14);
		SetDrPt(rPort, selLinePat);
		FrameRect(rPort, &rect);
	}
	ClearDrawComplement(rPort);
}

/*
 *	Draw select box and select contained objects
 */

static void DoSelectBox(WindowPtr window, UWORD modifier, WORD mouseX, WORD mouseY)
{
	DocObjPtr	docObj;
	DocDataPtr	docData = GetWRefCon(window);
	Point		pt1, pt2;
	Rectangle	selRect;

/*
	Draw selection rectangle
*/
	selLinePat = 0xF0F0;
	WindowToDoc(window, mouseX, mouseY, &pt1.x, &pt1.y);
	SnapToGrid(docData, &pt1.x, &pt1.y);
	pt2 = pt1;
	TrackMouse(window, modifier, &pt2, SelectTrack, NULL);
	SetFrameRect(&selRect, pt1.x, pt1.y, pt2.x, pt2.y);
/*
	Select all objects completely within selection rectangle
*/
	for (docObj = FirstObject(docData); docObj; docObj = NextObj(docObj)) {
		if (docObj->Frame.MinX >= selRect.MinX &&
			docObj->Frame.MaxX <= selRect.MaxX &&
			docObj->Frame.MinY >= selRect.MinY &&
			docObj->Frame.MaxY <= selRect.MaxY)
			SelectObject(docObj);
	}
}

/*
 *	Draw routine for tracking drag
 */

static void DragTrack(WindowPtr window, PointPtr pt1, PointPtr pt2, BOOL change)
{
	WORD		numSelected;
	RastPtr		rPort = window->RPort;
	DocObjPtr	docObj;
	DocDataPtr	docData = GetWRefCon(window);
	Rectangle	rect;

	if (change && (!EqualPt(pt1, pt2) || wasMoved)) {
		numSelected = 0;
		for (docObj = FirstSelected(docData); docObj; docObj = NextSelected(docObj)) {
			DrawObjectOutline(window, docObj, pt2->x - pt1->x, pt2->y - pt1->y);
			numSelected++;
		}
		if (numSelected > 1) {
			DocToWindowRect(window, &dragRect, &rect);
			SetDrPt(rPort, 0xAAAA);
			SetDrawComplement(rPort);
			FrameRect(rPort, &rect);
			ClearDrawComplement(rPort);
		}
		wasMoved = TRUE;
	}
}

/*
 *	Draw selection to new location
 */

static void DoDrag(WindowPtr window, WORD mouseX, WORD mouseY)
{
	DocObjPtr	docObj;
	DocDataPtr	docData = GetWRefCon(window);
	Point		pt1, pt2;

/*
	Get rectangle containing all selected objects
*/
	if (!GetSelectRect(docData, &dragRect))
		return;
/*
	Drag outline of rectangle
*/
	wasMoved = FALSE;
	WindowToDoc(window, mouseX, mouseY, &pt1.x, &pt1.y);
	SnapToGrid(docData, &pt1.x, &pt1.y);
	pt2 = pt1;
	dragging = TRUE;
	TrackMouse(window, 0, &pt2, DragTrack, NULL);
	dragging = FALSE;
/*
	Move selected objects
*/
	if (!EqualPt(&pt1, &pt2)) {
		SetStdPointer(window, POINTER_WAITDELAY);
		HiliteSelectOff(window);
		for (docObj = FirstSelected(docData); docObj; docObj = NextSelected(docObj)) {
			InvalObjectRect(window, docObj);
			OffsetObject(docObj, pt2.x - pt1.x, pt2.y - pt1.y);
			InvalObjectRect(window, docObj);
		}
		UpdateWindow(window);		// Do it here for crisper response
		HiliteSelectOn(window);
		DocModified(docData);
		SetPointerShape();
	}
}

/*
 *	Select objects
 */

static void DoSelect(WindowPtr window, UWORD modifier, WORD mouseX, WORD mouseY)
{
	WORD		handle;
	BOOL		isSelected, shiftKey, altKey;
	DocObjPtr	clickObj;
	DocDataPtr	docData = GetWRefCon(window);
	Point		docPt;
	Rectangle	handleRect;

	WindowToDoc(window, mouseX, mouseY, &docPt.x, &docPt.y);
	GetHandleRect(window, &handleRect);
	shiftKey = ((modifier & SHIFTKEYS) != 0);
	altKey = ((modifier & ALTKEYS) != 0);
/*
	Check to see if over an object handle
	If ALT key down, attempt to remove the handle (will succeed only for polys)
	Otherwise, reshape the object
*/
	for (clickObj = LastSelected(docData); clickObj; clickObj = PrevSelected(clickObj)) {
		if ((handle = PointToHandle(clickObj, &docPt, &handleRect)) != -1)
			break;
	}
	if (clickObj) {
		GrowObject(window, clickObj, modifier, handle);
		DocModified(docData);
		return;
	}
/*
	Find object that is clicked on
*/
	for (clickObj = LastObject(docData); clickObj; clickObj = PrevObj(clickObj)) {
		if (PointInObject(clickObj, &docPt))
			break;
	}
	isSelected = (clickObj && ObjectSelected(clickObj));
/*
	If clickObj already selected, check for adding a handle
*/
	if (isSelected && !shiftKey && altKey) {
		if (AddObjectHandle(window, clickObj, &docPt)) {
			handle = PointToHandle(clickObj, &docPt, &handleRect);
			GrowObject(window, clickObj, modifier, handle);
			DocModified(docData);
		}
	}
/*
	Otherwise select or unselect and drag clickObj
*/
	else {
		if (!isSelected || shiftKey) {
			HiliteSelectOff(window);
/*
	If not multiple select, unselect all objects
*/
			if (!shiftKey)
				UnSelectAllObjects(docData);
/*
	Select or unselect clickObj
*/
			if (clickObj) {
				if (ObjectSelected(clickObj)) {
					UnSelectObject(clickObj);
					isSelected = FALSE;
				}
				else {
					SelectObject(clickObj);
					isSelected = TRUE;
				}
			}
/*
	If nothing selected, do selection rectangle
*/
			else
				DoSelectBox(window, modifier, mouseX, mouseY);
			HiliteSelectOn(window);
		}
/*
	Drag the selection
*/
		if (isSelected)
			DoDrag(window, mouseX, mouseY);
	}
	SetPenMenuDefaults(window);
	SetTextMenuDefaults(window);
}

/*
 *	Create object of type corresponding to current drawTool
 */

static DocObjPtr CreateObject(WindowPtr window, UWORD modifier, WORD mouseX, WORD mouseY)
{
	DocObjPtr docObj;

	switch (drawTool) {
	case TOOL_LINE:
		docObj = LineCreate(window, modifier, mouseX, mouseY);
		break;
	case TOOL_RECT:
		docObj = RectCreate(window, modifier, mouseX, mouseY);
		break;
	case TOOL_OVAL:
		docObj = OvalCreate(window, modifier, mouseX, mouseY);
		break;
	case TOOL_CURVE:
		docObj = CurveCreate(window, modifier, mouseX, mouseY);
		break;
	case TOOL_POLY:
		docObj = PolyCreate(window, modifier, mouseX, mouseY);
		break;
	case TOOL_TEXT:
		docObj = TextCreate(window, modifier, mouseX, mouseY);
		break;
	default:
		docObj = NULL;
		break;
	}
	return (docObj);
}

/*
 *	Create new object in front of all others
 *	Return FALSE if not enough memory
 */

static void DoCreate(WindowPtr window, UWORD modifier, WORD mouseX, WORD mouseY)
{
	DocObjPtr docObj;
	DocDataPtr docData = GetWRefCon(window);

	HiliteSelectOff(window);
	UnSelectAllObjects(docData);
/*
	Create new object
	Don't allow objects that are one pixel wide and tall
*/
	docObj = CreateObject(window, modifier, mouseX, mouseY);
	if (docObj == NULL)
		Error(ERR_NO_MEM);
	else {
		if (docObj->Frame.MinX == docObj->Frame.MaxX &&
			docObj->Frame.MinY == docObj->Frame.MaxY) {
			DetachObject(CurrLayer(docData), docObj);
			DisposeDocObject(docObj);
		}
		else {
			SelectObject(docObj);
			DocModified(docData);
			if (docObj->Type != TYPE_TEXT)		// New text objs are empty
				InvalObjectRect(window, docObj);
		}
	}
	HiliteSelectOn(window);
}

/*
 *	Pickup fill from where mouse is originally clicked and fill object where
 *	mouse button is released
 */

static void DoCarryFill(WindowPtr window, UWORD modifier, WORD mouseX, WORD mouseY)
{
	RGBPat8		fillPat;
	DocObjPtr	clickObj, fillObj;
	GroupObjPtr	groupObj;
	Point		startPt, endPt;
	DocDataPtr	docData = GetWRefCon(window);

	WindowToDoc(window, mouseX, mouseY, &startPt.x, &startPt.y);
/*
	Find object that is clicked on
*/
	for (clickObj = LastObject(docData); clickObj; clickObj = PrevObj(clickObj)) {
		if (PointInObject(clickObj, &startPt))
			break;
	}
	if (clickObj == NULL)
		return;
	if (clickObj->Type == TYPE_GROUP) {
		groupObj = (GroupObjPtr) clickObj;
		startPt.x -= groupObj->DocObj.Frame.MinX;
		startPt.y -= groupObj->DocObj.Frame.MinY;
		for (clickObj = groupObj->Objects; clickObj; clickObj = NextObj(clickObj)) {
			if (PointInObject(clickObj, &startPt))
				break;
		}
	}
	if (!GetObjectFillPat(clickObj, &fillPat)) {
		ErrBeep();
		return ;
	}
	while (WaitMouseUp(mainMsgPort, window));

	endPt.x = window->MouseX;
	endPt.y = window->MouseY;
	WindowToDoc(window, endPt.x, endPt.y, &endPt.x, &endPt.y);
/*
	Find object that mouse is released on
*/
	for (fillObj = LastObject(docData); fillObj; fillObj = PrevObj(fillObj)) {
		if (PointInObject(fillObj, &endPt))
			break;
	}
	if (fillObj == NULL)
		return;
/*
	fill new object
*/
	SetObjectFillPat(fillObj, &fillPat);
	if (fillObj->Type != TYPE_BMAP)
		InvalObjectRect(window, fillObj);
	DocModified(docData);
}

/*
 *	Handle mouse down
 */

void DoMouseDown(WindowPtr window, UWORD modifier, WORD mouseX, WORD mouseY,
				 ULONG seconds, ULONG micros)
{
	WORD left, top, right, bottom;
	BOOL textClick;
	DocDataPtr docData = GetWRefCon(window);
	Rectangle windRect, contRect;

	if (titleChanged)
		FixTitle();
/*
	Set double-click and triple-click flags
*/
	if (ABS(prevMouseX - mouseX) > 2 || ABS(prevMouseY - mouseY) > 2)
		tripleClick = doubleClick = FALSE;
	else if (tripleClick)
		tripleClick = doubleClick = FALSE;
	else if (doubleClick) {
		doubleClick = FALSE;
		tripleClick = DoubleClick(prevSecs, prevMicros, seconds, micros);
	}
	else
		doubleClick = DoubleClick(prevSecs, prevMicros, seconds, micros);
	prevSecs = seconds;
	prevMicros = micros;
	prevMouseX = mouseX;
	prevMouseY = mouseY;
/*
	If special window, handle it elsewhere
*/
	if (!IsDocWindow(window)) {
		if (window == toolWindow)
			DoToolWindow(mouseX, mouseY);
		return;
	}
/*
	Check for mouse in layer indic
 */
 	if (InLayerIndic(window, mouseX, mouseY)) {
 		DoInLayerIndic(window);
 		return;
 	}
/*
	Check for mouse out of window bounds
*/
	GetWindowRect(window, &windRect);
	if (mouseX < windRect.MinX || mouseX > windRect.MaxX ||
		mouseY < windRect.MinY || mouseY > windRect.MaxY) {
		ErrBeep();
		return;
	}
/*
	Check to see if in upper left ruler corner
*/
	GetContentRect(window, &contRect);
	if (mouseX < contRect.MinX || mouseY < contRect.MinY) {
		if (mouseX >= windRect.MinX && mouseX < contRect.MinX &&
			mouseY >= windRect.MinY && mouseY < contRect.MinY)
			DoSetRuler(window);
		else
			ErrBeep();
		return;
	}
/*
	Check for mouse out of document bounds
*/
	DocToWindow(window, 0, 0, &left, &top);
	DocToWindow(window, docData->DocWidth, docData->DocHeight, &right, &bottom);
	right--;
	bottom--;
	if (mouseX < left || mouseY < top || mouseX > right || mouseY > bottom) {
		ErrBeep();
		return;
	}
/*
	Check for selecting objects
*/
	if (drawTool == TOOL_SELECT)
		DoSelect(window, modifier, mouseX, mouseY);
/*
	Check for clicking in text object with text tool
*/
	else if (drawTool == TOOL_TEXT)
		textClick = TextMouseClick(window, modifier, mouseX, mouseY);
/*
	Check for filling eye dropper
*/
	else if (drawTool == TOOL_EYEDROP)
		DoCarryFill(window, modifier, mouseX, mouseY);
/*
	Otherwise create new object
*/
	if (drawTool != TOOL_SELECT && drawTool != TOOL_EYEDROP &&
					(drawTool != TOOL_TEXT || !textClick))
		DoCreate(window, modifier, mouseX, mouseY);
	SetAllMenus();
	TextPurge(docData);
}

/*
 *	Reset multiple-click flags so next click will be treated like single click
 */

void ResetMultiClick()
{
	prevSecs = prevMicros = 0;
}
