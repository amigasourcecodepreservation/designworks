/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Toolbox
 *	Copyright (c) 1993 New Horizons Software, Inc.
 *
 *	Serialization routines
 */

#ifndef TOOLBOX_SERIALINFO_H
#define TOOLBOX_SERIALINFO_H

#ifndef EXEC_TYPES_H
#include <exec/types.h>
#endif

#ifndef TYPEDEFS_H
#include <Typedefs.h>
#endif

/*
 *	Prototypes
 */

BOOL	GetSerialInfo(TextPtr *, TextPtr *, TextPtr *, ScreenPtr, MsgPortPtr,
					  TextPtr, WORD, TextPtr);

#endif
