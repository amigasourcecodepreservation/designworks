/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Amiga library support
 *	Copyright (c) 1989 New Horizons Software
 *
 *	Requester template definitions
 */

#ifndef TOOLBOX_REQUEST_H
#define TOOLBOX_REQUEST_H

#ifndef INTUITION_INTUITION_H
#include <intuition/intuition.h>
#endif

#ifndef TYPEDEFS_H
#include <TypeDefs.h>
#endif

#ifndef TOOLBOX_GADGET_H
#include <Toolbox/Gadget.h>
#endif

#ifndef TOOLBOX_DIALOG_H
#include <Toolbox/Dialog.h>
#endif

/*
 *	Requester template
 *
 *	Bounds field is dimension of requester
 *	Gadgets field points to first item in gadget template list
 *
 *	LeftEdge, TopEdge, Width, and Height are relative to toolbox font x & y size
 *	A value of -1 in LeftEdge or TopEdge will center the requester in the
 *		indicated direction within the window's borders
 */

typedef struct {
	WORD			LeftEdge, TopEdge, Width, Height;
	GadgTemplPtr	Gadgets;
} RequestTemplate, *ReqTemplPtr;

/*
 *	Suggested requester gadget item numbers
 */

#define REQ_OK_BUTTON		OK_BUTTON
#define REQ_CANCEL_BUTTON	CANCEL_BUTTON

/*
 *	Routines prototypes
 */

#define CheckRequest(msgPort, dlg, filter)	CheckDialog(msgPort, dlg, filter)
#define ModalRequest(msgPort, dlg, filter)	ModalDialog(msgPort, dlg, filter)

RequestPtr	GetRequest(ReqTemplPtr, WindowPtr, BOOL);
BOOL		ShowRequest(RequestPtr, WindowPtr);
void		DisposeRequest(RequestPtr);

#endif
