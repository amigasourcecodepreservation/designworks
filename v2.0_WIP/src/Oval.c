/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Oval handling routines
 */

#include <exec/types.h>
#include <graphics/gfxmacros.h>
#include <intuition/intuition.h>

#include <proto/graphics.h>

#include <Toolbox/Memory.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Window.h>

#include "Draw.h"
#include "Proto.h"

/*
 *	External variables
 */

extern Defaults	defaults;

/*
 *	Local variables and definitions
 */

#define LINE_PAT	0xFFFF		// Line pattern for object creation

/*
 *	Local prototypes
 */

static void	SetOvalFrame(OvalObjPtr);

static OvalObjPtr	CreateOval(DocDataPtr, PointPtr, PointPtr);

static void	OvalTrackCreate(WindowPtr, PointPtr, PointPtr, BOOL);
static void	OvalConstrainCreate(PointPtr, PointPtr);

/*
 *	Set oval frame from bounds and angle
 */

static void SetOvalFrame(OvalObjPtr ovalObj)
{
	Point	pt;

	ovalObj->DocObj.Frame = ovalObj->Rect;
	GetRectCenter(&ovalObj->Rect, &pt);
	RotateRect(&ovalObj->DocObj.Frame, pt.x, pt.y, ovalObj->Rotate);
}

/*
 *	Allocate a new oval object
 */

OvalObjPtr OvalAllocate()
{
	return (RectAllocate());
}

/*
 *	Dispose of oval object
 */

void OvalDispose(OvalObjPtr ovalObj)
{
	RectDispose(ovalObj);
}

/*
 *	Create oval with given start and end positions
 */

static OvalObjPtr CreateOval(DocDataPtr docData, PointPtr pt1, PointPtr pt2)
{
	register OvalObjPtr	ovalObj;

	if ((ovalObj = (OvalObjPtr) NewDocObject(CurrLayer(docData), TYPE_OVAL)) == NULL)
		return (NULL);
	SetFrameRect(&ovalObj->DocObj.Frame, pt1->x, pt1->y, pt2->x, pt2->y);
	ovalObj->DocObj.Flags	= defaults.ObjFlags;
	ovalObj->PenWidth		= defaults.PenWidth;
	ovalObj->PenHeight		= defaults.PenHeight;
	ovalObj->PenColor		= defaults.PenColor;
	CopyRGBPat8(&defaults.FillPat, &ovalObj->FillPat);
	return (ovalObj);
}

/*
 *	Draw oval object
 */

void OvalDrawObj(RastPtr rPort, OvalObjPtr ovalObj, RectPtr dstRect, RectPtr clipRect)
{
	PolyObjPtr	polyObj;

	if ((polyObj = OvalConvertToPoly(ovalObj)) != NULL) {
		PolyDrawObj(rPort, polyObj, dstRect, clipRect);
		PolyDispose(polyObj);
	}
}

/*
 *	Draw outline of oval (for creation/dragging)
 *	Offset is in document coordinates
 */

void OvalDrawOutline(WindowPtr window, OvalObjPtr ovalObj, WORD xOffset, WORD yOffset)
{
	PolyObjPtr	polyObj;

	if ((polyObj = OvalConvertToPoly(ovalObj)) != NULL) {
		PolyDrawOutline(window, polyObj, xOffset, yOffset);
		PolyDispose(polyObj);
	}
}

/*
 *	Draw selection hilighting for oval
 */

void OvalHilite(WindowPtr window, OvalObjPtr ovalObj)
{
	RectHilite(window, ovalObj);
}

/*
 *	Set oval pen color
 */

void OvalSetPenColor(OvalObjPtr ovalObj, RGBColor penColor)
{
	RectSetPenColor(ovalObj, penColor);
}

/*
 *	Set oval pen size
 *	If size is -1 then do not change
 */

void OvalSetPenSize(OvalObjPtr ovalObj, WORD penWidth, WORD penHeight)
{
	RectSetPenSize(ovalObj, penWidth, penHeight);
}

/*
 *	Set oval fill pattern
 */

void OvalSetFillPat(OvalObjPtr ovalObj, RGBPat8Ptr fillPat)
{
	RectSetFillPat(ovalObj, fillPat);
}

/*
 *	Get oval fill pattern
 */

BOOL OvalGetFillPat(OvalObjPtr ovalObj, RGBPat8Ptr fillPat)
{
	return (RectGetFillPat(ovalObj, fillPat));
}

/*
 *	Rotate oval by given angle about center
 */

void OvalRotate(OvalObjPtr ovalObj, WORD cx, WORD cy, WORD angle)
{
	RectRotate(ovalObj, cx, cy, angle);
}

/*
 *	Flip oval horizontally or vertically about center
 */

void OvalFlip(OvalObjPtr ovalObj, WORD cx, WORD cy, BOOL horiz)
{
	RectFlip(ovalObj, cx, cy, horiz);
}

/*
 *	Scale oval to new frame
 */

void OvalScale(OvalObjPtr ovalObj, RectPtr frame)
{
	RectScale(ovalObj, frame);
}

/*
 *	Create poly equivalent to given rect
 *	Returns pointer to poly, or NULL if error
 */

PolyObjPtr OvalConvertToPoly(OvalObjPtr ovalObj)
{
	PolyObjPtr	polyObj;
	Point		pt, pt1, pt2;

	if ((polyObj = (PolyObjPtr) NewDocObject(NULL, TYPE_POLY)) == NULL)
		return (NULL);
	polyObj->DocObj.Frame = ovalObj->Rect;
	pt.x = ovalObj->DocObj.Frame.MaxX - ovalObj->DocObj.Frame.MinX + 1;
	pt.y = ovalObj->DocObj.Frame.MaxY - ovalObj->DocObj.Frame.MinY + 1;
	pt1.x = (10824L*pt.x + 10000L)/20000L;
	pt2.x = ( 7654L*pt.x + 10000L)/20000L;
	pt1.y = (10824L*pt.y + 10000L)/20000L;
	pt2.y = ( 7654L*pt.y + 10000L)/20000L;
	pt.x /= 2;
	pt.y /= 2;
	if (!PolyAddPoint(polyObj, 0, 0x7FFF, pt.x        , pt.y + pt1.y) ||
		!PolyAddPoint(polyObj, 0, 0x7FFF, pt.x + pt2.x, pt.y + pt2.y) ||
		!PolyAddPoint(polyObj, 0, 0x7FFF, pt.x + pt1.x, pt.y        ) ||
		!PolyAddPoint(polyObj, 0, 0x7FFF, pt.x + pt2.x, pt.y - pt2.y) ||
		!PolyAddPoint(polyObj, 0, 0x7FFF, pt.x        , pt.y - pt1.y) ||
		!PolyAddPoint(polyObj, 0, 0x7FFF, pt.x - pt2.x, pt.y - pt2.y) ||
		!PolyAddPoint(polyObj, 0, 0x7FFF, pt.x - pt1.x, pt.y        ) ||
		!PolyAddPoint(polyObj, 0, 0x7FFF, pt.x - pt2.x, pt.y + pt2.y)) {
		DisposeDocObject(polyObj);
		return (NULL);
	}
	PolyAdjustFrame(polyObj);
	if (ovalObj->Rotate) {
		GetRectCenter(&ovalObj->Rect, &pt);
		PolyRotate(polyObj, pt.x, pt.y, ovalObj->Rotate);
	}
	PolySetPenColor(polyObj, ovalObj->PenColor);
	PolySetPenSize(polyObj, ovalObj->PenWidth, ovalObj->PenHeight);
	PolySetFillPat(polyObj, &ovalObj->FillPat);
	polyObj->DocObj.Flags = ovalObj->DocObj.Flags;
	PolySetSmooth(polyObj, TRUE);
	PolySetClosed(polyObj, TRUE);
	return (polyObj);
}

/*
 *	Determine if point is in oval
 */

BOOL OvalSelect(OvalObjPtr ovalObj, PointPtr pt)
{
	LONG	x, y, rx, ry, rx1, ry1;
	BOOL	hasPen, hasFill, inOuter, inInner;
	Point	newPt, cenPt;

	hasPen = HasPen(ovalObj);
	hasFill = HasFill(ovalObj);
	if (!hasPen && !hasFill)
		return (FALSE);
	newPt = *pt;
	GetRectCenter(&ovalObj->Rect, &cenPt);
	RotatePoint(&newPt, cenPt.x, cenPt.y, -ovalObj->Rotate);
	OffsetPoint(&newPt, -ovalObj->Rect.MinX, -ovalObj->Rect.MinY);
	rx = (ovalObj->Rect.MaxX - ovalObj->Rect.MinX)/2;
	ry = (ovalObj->Rect.MaxY - ovalObj->Rect.MinY)/2;
	if (rx == 0 || ry == 0)
		return (TRUE);
	x = (newPt.x - rx);
	y = (newPt.y - ry);
	rx1 = rx + 1;
	ry1 = ry + 1;
	while (x > 0x7F || y > 0x7F || rx1 > 0x7F || ry1 > 0x7F) {
		x >>= 1;
		y >>= 1;
		rx1 >>= 1;
		ry1 >>= 1;
	}
	inOuter = (x*x*ry1*ry1 + y*y*rx1*rx1 <= rx1*rx1*ry1*ry1);
	if (!inOuter)
		return (FALSE);
	if (hasFill)
		return (TRUE);
	x = (newPt.x - rx);
	y = (newPt.y - ry);
	rx1 = rx - ovalObj->PenWidth - 1;
	ry1 = ry - ovalObj->PenHeight - 1;
	while (x > 0x7F || y > 0x7F || rx1 > 0x7F || ry1 > 0x7F) {
		x >>= 1;
		y >>= 1;
		rx1 >>= 1;
		ry1 >>= 1;
	}
	inInner = (x*x*ry1*ry1 + y*y*rx1*rx1 <= rx1*rx1*ry1*ry1);
	return (!inInner);
}

/*
 *	Return handle number of given point, or -1 if not in a handle
 *	Point is relative to object rectangle
 */

WORD OvalHandle(OvalObjPtr ovalObj, PointPtr pt, RectPtr handleRect)
{
	return (RectHandle(ovalObj, pt, handleRect));
}

/*
 *	Duplicate oval data to new object
 *	Return success status
 */

BOOL OvalDupData(OvalObjPtr ovalObj, OvalObjPtr newObj)
{
	return (RectDupData(ovalObj, newObj));
}

/*
 *	Draw routine for tracking oval creation
 */

static void OvalTrackCreate(WindowPtr window, PointPtr pt1, PointPtr pt2, BOOL change)
{
	OvalObj	ovalObj;

	if (change) {
		BlockClear(&ovalObj, sizeof(OvalObj));
		SetFrameRect(&ovalObj.Rect, pt1->x, pt1->y, pt2->x, pt2->y);
		SetOvalFrame(&ovalObj);
		OvalDrawOutline(window, &ovalObj, 0, 0);
	}
}

/*
 *	Constrain routine for oval creation
 */

static void OvalConstrainCreate(PointPtr pt1, PointPtr pt2)
{
	WORD	width, height, min;

	width = ABS(pt2->x - pt1->x);
	height = ABS(pt2->y - pt1->y);
	min = MIN(width, height);
	pt2->x = (pt2->x >= pt1->x) ? pt1->x + min : pt1->x - min;
	pt2->y = (pt2->y >= pt1->y) ? pt1->y + min : pt1->y - min;
}

/*
 *	Create oval object
 */

DocObjPtr OvalCreate(WindowPtr window, UWORD modifier, WORD mouseX, WORD mouseY)
{
	DocDataPtr	docData = GetWRefCon(window);
	Point		pt1, pt2;

/*
	Track object size
*/
	WindowToDoc(window, mouseX, mouseY, &pt1.x, &pt1.y);
	SnapToGrid(docData, &pt1.x, &pt1.y);
	pt2 = pt1;
	TrackMouse(window, modifier, &pt2, OvalTrackCreate, OvalConstrainCreate);
/*
	Create oval
*/
	return (CreateOval(docData, &pt1, &pt2));
}

/*
 *	Track mouse and change oval shape
 */

void OvalGrow(WindowPtr window, OvalObjPtr ovalObj, UWORD modifier, WORD handle)
{
	RectGrow(window, ovalObj, modifier, handle);
}

/*
 *	Draw Oval object to PostScript file
 */

BOOL OvalDrawPSObj(PrintRecPtr printRec, OvalObjPtr ovalObj, RectPtr rect, RectPtr clipRect)
{
	BOOL hasPen, hasFill;
	Point pen;

	hasPen = HasPen(ovalObj);
	hasFill = HasFill(ovalObj);
	if (hasPen || hasFill) {
		if (hasFill) {
			if (!PSSetFill(printRec, &ovalObj->FillPat))
				return (FALSE);
		}
		if (hasPen) {
			pen.x = ovalObj->PenWidth;
			pen.y = ovalObj->PenHeight;
			PSScalePt(&pen, &ovalObj->DocObj.Frame, rect);
			if (!PSSetPen(printRec, &pen, ovalObj->PenColor))
				return (FALSE);
		}
		if (!PSOval(printRec, rect, hasPen, hasFill))
			return (FALSE);
	}
	return (TRUE);
}

/*
 *	Create new oval given REXX arguments
 */

OvalObjPtr OvalNewREXX(DocDataPtr docData, TextPtr args)
{
	Point pt1, pt2;

/*
	Get start and end points
*/
	args = GetArgPoint(args, docData, &pt1);
	if (args)
		args = GetArgPoint(args, docData, &pt2);
	if (args == NULL ||
		pt1.x < 0 || pt1.y < 0 || pt1.x >= docData->DocWidth || pt1.y >= docData->DocHeight ||
		pt2.x < 0 || pt2.y < 0 || pt2.x >= docData->DocWidth || pt2.y >= docData->DocHeight)
		return (NULL);
/*
	Create oval
*/
	return (CreateOval(docData, &pt1, &pt2));
}
