/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Toolbox
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *	All rights reserved
 *
 *	Language setting
 */

#ifndef TOOLBOX_LANGUAGE_H
#define TOOLBOX_LANGUAGE_H

#ifdef BRITISH
#undef BRITISH
#define AMERICAN	0
#define BRITISH		1
#else
#define BRITISH		0
#endif

#ifdef GERMAN
#undef GERMAN
#define AMERICAN	0
#define GERMAN		1
#else
#define GERMAN		0
#endif

#ifdef FRENCH
#undef FRENCH
#define AMERICAN	0
#define FRENCH		1
#else
#define FRENCH		0
#endif

#ifdef SWEDISH
#undef SWEDISH
#define AMERICAN	0
#define SWEDISH		1
#else
#define SWEDISH		0
#endif

#ifdef SPANISH
#undef SPANISH
#define AMERICAN	0
#define SPANISH		1
#else
#define SPANISH		0
#endif

#ifndef AMERICAN
#define AMERICAN	1
#endif

#endif
