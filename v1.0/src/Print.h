/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Standard print record
 *	Copyright (c) 1991 New Horizons Software, Inc.
 */

#define PRINTHANDLER_VERSION	1

enum {								/* Page Size */
	PRT_CUSTOM,
	PRT_USLETTER,	PRT_USLEGAL,
	PRT_A4LETTER,	PRT_WIDECARRIAGE
};

enum {								/* Orientation */
	PRT_PORTRAIT,	PRT_LANDSCAPE
};

enum {								/* Quality */
	PRT_DRAFT,		PRT_NLQ,	PRT_GRAPHIC,	PRT_POSTSCRIPT,
	PRT_FULLRES
};

enum {								/* Paper Feed */
	PRT_CONTINUOUS,	PRT_CUTSHEET
};

/*
 *	Flags
 */

#define PRT_ASPECTADJ		0x0001
#define PRT_NOGAPS			0x0002	/* No gaps between pages */
#define PRT_SMOOTH			0x0004	/* Smoothing on */
#define PRT_PICTURES		0x0008	/* Pictures with character print */
#define PRT_BACKTOFRONT		0x0010	/* Print back to front */
#define PRT_COLLATE			0x0020	/* Print in collated order */
#define PRT_ODDEVEN			0x0040	/* Print separate odd and even pages */
#define PRT_OPTSPACE		0x0080	/* Optimal spacing */
#define PRT_FONTS			0x0100	/* Bit-map fonts with character print */
#define PRT_TOFILE			0x0200	/* Print to file */

enum {								/* Print Pitch */
	PRT_PICA,	PRT_ELITE,	PRT_CONDENSED
};

enum {								/* Print Spacing */
	PRT_SIXLPI,	PRT_EIGHTLPI
};

#define PRT_DEPTHSTANDARD	3
#define PRT_DEPTHEXTENDED	6
#define PRT_DEPTHFULL		12

/*
 *	Print record
 */

typedef union  {
	struct IOStdReq		Std;
	struct IODRPReq		DRP;
	struct IOPrtCmdReq	Cmd;
} PrintIO, *PrintIOPtr;

typedef struct PrintRecord {
	WORD		Version;				/* Version number of record */
	Rectangle	PaperRect, PageRect;	/* Upper left of PaperRect is (0,0) */
	WORD		xDPI, yDPI;

	UWORD		PaperWidth, PaperHeight;	/* In decipoints */

	WORD		Copies, FirstPage, LastPage;

	UBYTE		PageSize;				/* From defines above */
	UBYTE		Orientation;
	UWORD		XScale, YScale;			/* Enlargement/reduction * 100 */
	UBYTE		PrintDensity;
	UBYTE		Quality;
	UBYTE		PaperFeed;
	UBYTE		FontNum;				/* Printer font number */
	UWORD		Flags;
	UBYTE		PrintPitch;
	UBYTE		PrintSpacing;
	UBYTE		Depth;					/* Bitmap depth */

	UBYTE		pad[120-103];			/* Pad to 120 bytes */

	TextChar	FileName[32];
	Dir			FileDir;

	RastPtr		RPort;
	PrintIOPtr	PrintIO;
	ColorMapPtr	ColorMap;
	WORD		Top, Left, Width, Height;
} *PrintRecPtr;

#ifndef c_plusplus
typedef struct PrintRecord PrintRecord;
#endif
