/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Fill Pattern routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <proto/exec.h>
#include <proto/intuition.h>

#include <string.h>

#include "Draw.h"
#include "Proto.h"

/*
 *	external variables
 */

extern FillPtnPtr	fillPatterns;

extern RGBPat8		fillPats[];
extern TextPtr		patternNames[];

extern TextChar		strNone[];

/*
 	local prototypes
 */

static FillPtnPtr	MakeFillPattern(TextPtr, RGBPat8);

/*
 *	return RGBPat8 value for given index
 */


void GetFillPattern(WORD num, RGBPat8Ptr pattern)
{
	FillPtnPtr	fill;

	for (num, fill = fillPatterns; num > 0 && fill->Next; num--, fill = fill->Next);

	CopyRGBPat8(&fill->Pattern, pattern);
}

/*
 *	return pattern name for given index
 */

void GetFillName(WORD num, TextPtr name)
{
	FillPtnPtr	fill;

	for (num, fill = fillPatterns; num > 0 && fill->Next; num--, fill = fill->Next);

	strcpy(name, fill->Name);
}

/*
 *	return pointer to first pattern excluding None
 */

FillPtnPtr FirstFillPattern()
{
	return (fillPatterns->Next);	// skip none
}

/*
 *	return number of fills in current fill list
 */

WORD NumFillPatterns()
{
	WORD 		num;
	FillPtnPtr	fill;
/*
	ignore none pattern
*/
	num = 0;
	for (fill = FirstFillPattern(); fill; fill = fill->Next)
		num++;

	return (num);
}

/*
 *	remove color from current list of fill patterns
 */

void RemoveFillPattern(WORD patNum)
{
	FillPtnPtr	fill, prevFill;

	RemoveFromFillWindow(patNum + 1);

	fill = FirstFillPattern();
	prevFill = fillPatterns;

	while (patNum && fill) {
		fill = fill->Next;
		prevFill = prevFill->Next;
		patNum--;
	}
	if (fill == NULL)
		return;
/*
	unlink and free fill
*/
	prevFill->Next = fill->Next;
	if (fill->Name)
		MemFree(fill->Name, strlen(fill->Name) + 1);
	MemFree(fill, sizeof(FillPattern));
}

/*
 *	return pointer to fillPattern filled with pattern and name
 */

static FillPtnPtr	MakeFillPattern(TextPtr name, RGBPat8 pattern)
{
	FillPtnPtr	fill;

	if ((fill = MemAlloc(sizeof(FillPattern), MEMF_CLEAR)) != NULL) {
		CopyRGBPat8(pattern, &fill->Pattern);
		if ((fill->Name = MemAlloc(strlen(name) + 1, MEMF_CLEAR)) == NULL) {
			MemFree(fill, sizeof(FillPattern));
			return (NULL);
		}
		strcpy(fill->Name, name);
	}
	return (fill);
}

/*
 *	add fill pattern to end of current list
 */

BOOL AddFillPattern(TextPtr name, RGBPat8 pattern, BOOL addWindow)
{
	FillPtnPtr 	fill, prevFill;

	if ((fill = MakeFillPattern(name, pattern)) == NULL)
		return (FALSE);

	if (fillPatterns->Next == NULL)		// this is first non-none pattern
		fillPatterns->Next = fill;
	else {
		for (prevFill = FirstFillPattern(); prevFill->Next; prevFill = prevFill->Next);
		prevFill->Next = fill;
	}

	if (addWindow)
		AddToFillWindow(name);

	return (TRUE);
}

/*
 *	init fill pattern list with only none entry
 */

BOOL InitFillPatterns()
{
	FillPtnPtr fill;

	if (fillPatterns)
		return (TRUE);

	if ((fill = MakeFillPattern(strNone, fillPats[0])) == NULL)
		return (FALSE);
	fillPatterns = fill;

	return (TRUE);
}

/*
 *	initialize table of FillPatterns
 */

BOOL InitStdPatterns()
{
	WORD		i;
	FillPtnPtr	fill, prevFill;
	BOOL		success;

	if (fillPatterns)
		return (TRUE);
	success = FALSE;
/*
	add none pattern
*/
	if (!InitFillPatterns())
		return (FALSE);
	prevFill = fillPatterns;

	for (i = 0; i < NUM_STDPATS; i++) {
		if ((fill = MakeFillPattern(patternNames[i], fillPats[i])) == NULL)
			goto Exit;
		prevFill->Next = fill;
		prevFill = fill;
	}
	success = TRUE;
Exit:
	if (!success)
		DisposeFillPatterns();

	return (success);
}

/*
 *	dispose of FillPatterns
 */

void DisposeFillPatterns()
{
	FillPtnPtr	fill, nextFill;

	fill = fillPatterns;
	while (fill) {
		nextFill = fill->Next;
		if (fill->Name)
			MemFree(fill->Name, strlen(fill->Name) + 1);
		MemFree(fill, sizeof(FillPattern));
		fill = nextFill;
	}
	fillPatterns = NULL;
}
