/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Toolbox library
 *	Copyright (c) 1990 New Horizons Software, Inc.
 *
 *	Utility routines
 */

#include <exec/types.h>
#include <exec/memory.h>
#include <devices/audio.h>
#include <intuition/intuition.h>
#include <intuition/intuitionbase.h>

#include <string.h>

#include "typedefs.h"

#include "Memory.h"
#include "Utility.h"

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>
#include <proto/diskfont.h>
#include <proto/console.h>

/*
 *	External variables
 */

extern struct IntuitionBase *IntuitionBase;
extern struct Library		*DiskfontBase;

extern BOOL			_tbHiRes;
extern TextAttrPtr	_tbTextAttr;

extern WORD	_tbBlackColor, _tbWhiteColor;
extern WORD	_tbDarkColor, _tbLightColor;

extern BOOL	_tbAutoActivate;

/*
 *	Local variables and definitions
 */

#define RAWKEY_F1	0x50
#define RAWKEY_F10	0x59
#define RAWKEY_HELP	0x5F

#define SELECTBUTTON(q)	\
	((q&IEQUALIFIER_LEFTBUTTON)||((q&AMIGAKEYS)&&(q&IEQUALIFIER_LALT)))

/*
 *	Pointers
 */

static UWORD chip nullPointerData[] = {
	0x0000, 0x0000, 0x0000, 0x0000
};

static UWORD chip arrowPointerData[] = {
	0x0000, 0x0000, 0x0000, 0xC000, 0x4000, 0xE000, 0x6000, 0xB000,
	0x7000, 0x9800, 0x7800, 0x8C00, 0x7C00, 0x8600, 0x7E00, 0x8300,
	0x7800, 0x8F00, 0x5800, 0xAC00, 0x0C00, 0xD600, 0x0C00, 0x1600,
	0x0000, 0x0E00, 0x0000, 0x0000
};

static UWORD chip iBeamPointerData[] = {
	0x0000, 0x0000, 0x4400, 0x8800, 0x2800, 0x5000, 0x1000, 0x2000,
	0x1000, 0x2000, 0x1000, 0x2000, 0x1000, 0x2000, 0x1000, 0x2000,
	0x1000, 0x2000, 0x1000, 0x2000, 0x2800, 0x5000, 0x4400, 0x8800,
	0x0000, 0x0000
};

static UWORD chip plusPointerData[] = {
	0x0000, 0x0000, 0x0000, 0x3E00, 0x1C00, 0x3E00, 0x1C00, 0xE780,
	0x7F00, 0xE780, 0x7F00, 0x8180, 0x7F00, 0x8180, 0x1C00, 0xE780,
	0x1C00, 0x2600, 0x0000, 0x3E00, 0x0000, 0x0000
};

static UWORD chip crossPointerData[] = {
	0x0000, 0x0000,
	0x0400, 0x0000, 0x0400, 0x0000, 0x0400, 0x0000, 0x0400, 0x0000,
	0x0000, 0x0000, 0xF1E0, 0x0400, 0x0000, 0x0000,
	0x0400, 0x0000, 0x0400, 0x0000, 0x0400, 0x0000, 0x0400, 0x0000,
	0x0000, 0x0000
};

static UWORD chip waitPointerData[] = {
	0x0000, 0x0000,
	0x0000, 0x0780, 0x0780, 0x1860, 0x1860, 0x2790, 0x37B0, 0x4FC8,
	0x2DD0, 0x5FE8, 0x5DE8, 0xBFF4, 0x5DE8, 0xBFF4, 0x5C68, 0xBFF4,
	0x5FE8, 0xBFF4, 0x2FD0, 0x5FE8, 0x37B0, 0x4FC8, 0x1860, 0x2790,
	0x0780, 0x1860, 0x0000, 0x0780,
	0x0000, 0x0000
};

static Pointer nullPointer = {
	0, 0, 0, 0, &nullPointerData[0]
};

static Pointer arrowPointer = {
	8, 12, -1, 0, &arrowPointerData[0]
};

static Pointer iBeamPointer = {
	6, 11, -4, -5, &iBeamPointerData[0]
};

static Pointer crossPointer = {
	11, 11, -6, -5, &crossPointerData[0]
};

static Pointer plusPointer = {
	9, 9, -5, -4, &plusPointerData[0]
};

static Pointer waitPointer = {
	14, 14, -7, -7, &waitPointerData[0]
};

/*
 *	Pointer array
 *	Must be in order given by pointer type definitions
 */

static Pointer *pointerArray[] = {
	&arrowPointer,
	&iBeamPointer,
	&crossPointer,
	&plusPointer,
	&waitPointer
};

/*
 *	Scroll arrows
 */

static UWORD chip upArrowData[] = {
	0xFFFF, 0xFE7F, 0xF81F, 0xE007, 0x8001, 0xF81F, 0xF81F, 0xF81F, 0xFFFF
};

static UWORD chip downArrowData[] = {
	0xFFFF, 0xF81F, 0xF81F, 0xF81F, 0x8001, 0xE007, 0xF81F, 0xFE7F, 0xFFFF
};

static UWORD chip leftArrowData[] = {
	0xFFFF, 0xFE7F, 0xF87F, 0xE001, 0x8001, 0xE001, 0xF87F, 0xFE7F, 0xFFFF
};

static UWORD chip rightArrowData[] = {
	0xFFFF, 0xFE7F, 0xFE1F, 0x8007, 0x8001, 0x8007, 0xFE1F, 0xFE7F, 0xFFFF
};

static Image upArrowImage = {
	0, 0, IMAGE_ARROW_WIDTH, IMAGE_ARROW_HEIGHT, 1, &upArrowData[0], 1, 0,
	NULL
};

static Image downArrowImage = {
	0, 0, IMAGE_ARROW_WIDTH, IMAGE_ARROW_HEIGHT, 1, &downArrowData[0], 1, 0,
	NULL
};

static Image leftArrowImage = {
	0, 0, IMAGE_ARROW_WIDTH, IMAGE_ARROW_HEIGHT, 1, &leftArrowData[0], 1, 0,
	NULL
};

static Image rightArrowImage = {
	0, 0, IMAGE_ARROW_WIDTH, IMAGE_ARROW_HEIGHT, 1, &rightArrowData[0], 1, 0,
	NULL
};

/*
 *	Requester icons
 *	Valid for up to 16 color screens
 */

static UWORD chip hrStopIconData[] = {
	0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF,
	0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFE, 0x7FFF,
	0xFFFC, 0x3FFF, 0xFFFC, 0x3FFF, 0xFFFC, 0x3FFF, 0xFFFC, 0x3FFF,
	0xFFFC, 0x3FFF, 0xFFFC, 0x3FFF, 0xFFFC, 0x3FFF, 0xFFFC, 0x3FFF,
	0xFFFC, 0x3FFF, 0xFFFC, 0x3FFF, 0xFFFE, 0x7FFF, 0xFFFE, 0x7FFF,
	0xFFFE, 0x7FFF, 0xFFFF, 0xFFFF, 0xFFFE, 0x7FFF, 0xFFFC, 0x3FFF,
	0xFFFE, 0x7FFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF,
	0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF,

	0x007F, 0xFE00, 0x00C0, 0x0300, 0x01BF, 0xFD80, 0x037F, 0xFEC0,
	0x06FF, 0xFF60, 0x0DFF, 0xFFB0, 0x1BFF, 0xFFD8, 0x37FF, 0xFFEC,
	0x6FFF, 0xFFF6, 0xDFFF, 0xFFFB, 0xBFFF, 0xFFFD, 0xBFFF, 0xFFFD,
	0xBFFF, 0xFFFD, 0xBFFF, 0xFFFD, 0xBFFF, 0xFFFD, 0xBFFF, 0xFFFD,
	0xBFFF, 0xFFFD, 0xBFFF, 0xFFFD, 0xBFFF, 0xFFFD, 0xBFFF, 0xFFFD,
	0xBFFF, 0xFFFD, 0xBFFF, 0xFFFD, 0xDFFF, 0xFFFB, 0x6FFF, 0xFFF6,
	0x37FF, 0xFFEC, 0x1BFF, 0xFFD8, 0x0DFF, 0xFFB0, 0x06FF, 0xFF60,
	0x037F, 0xFEC0, 0x01BF, 0xFD80, 0x00C0, 0x0300, 0x007F, 0xFE00,

	0x007F, 0xFE00, 0x00C0, 0x0300, 0x01BF, 0xFD80, 0x037F, 0xFEC0,
	0x06FF, 0xFF60, 0x0DFF, 0xFFB0, 0x1BFF, 0xFFD8, 0x37FF, 0xFFEC,
	0x6FFF, 0xFFF6, 0xDFFF, 0xFFFB, 0xBFFF, 0xFFFD, 0xBFFF, 0xFFFD,
	0xBFFF, 0xFFFD, 0xBFFF, 0xFFFD, 0xBFFF, 0xFFFD, 0xBFFF, 0xFFFD,
	0xBFFF, 0xFFFD, 0xBFFF, 0xFFFD, 0xBFFF, 0xFFFD, 0xBFFF, 0xFFFD,
	0xBFFF, 0xFFFD, 0xBFFF, 0xFFFD, 0xDFFF, 0xFFFB, 0x6FFF, 0xFFF6,
	0x37FF, 0xFFEC, 0x1BFF, 0xFFD8, 0x0DFF, 0xFFB0, 0x06FF, 0xFF60,
	0x037F, 0xFEC0, 0x01BF, 0xFD80, 0x00C0, 0x0300, 0x007F, 0xFE00,

	0x007F, 0xFE00, 0x00C0, 0x0300, 0x01BF, 0xFD80, 0x037F, 0xFEC0,
	0x06FF, 0xFF60, 0x0DFF, 0xFFB0, 0x1BFF, 0xFFD8, 0x37FF, 0xFFEC,
	0x6FFF, 0xFFF6, 0xDFFF, 0xFFFB, 0xBFFF, 0xFFFD, 0xBFFF, 0xFFFD,
	0xBFFF, 0xFFFD, 0xBFFF, 0xFFFD, 0xBFFF, 0xFFFD, 0xBFFF, 0xFFFD,
	0xBFFF, 0xFFFD, 0xBFFF, 0xFFFD, 0xBFFF, 0xFFFD, 0xBFFF, 0xFFFD,
	0xBFFF, 0xFFFD, 0xBFFF, 0xFFFD, 0xDFFF, 0xFFFB, 0x6FFF, 0xFFF6,
	0x37FF, 0xFFEC, 0x1BFF, 0xFFD8, 0x0DFF, 0xFFB0, 0x06FF, 0xFF60,
	0x037F, 0xFEC0, 0x01BF, 0xFD80, 0x00C0, 0x0300, 0x007F, 0xFE00,
};

static UWORD chip hrCautionIconData[] = {
	0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF,
	0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFC, 0x3FFF,
	0xFFF0, 0x0FFF, 0xFFE0, 0x07FF, 0xFFE1, 0x87FF, 0xFFC7, 0xC3FF,
	0xFFC7, 0xC3FF, 0xFFFF, 0xC3FF, 0xFFFF, 0x87FF, 0xFFFF, 0x87FF,
	0xFFFF, 0x0FFF, 0xFFFE, 0x1FFF, 0xFFFC, 0x3FFF, 0xFFFC, 0x3FFF,
	0xFFFE, 0x7FFF, 0xFFFF, 0xFFFF, 0xFFFE, 0x7FFF, 0xFFFC, 0x3FFF,
	0xFFFE, 0x7FFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF,
	0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF,

	0x0001, 0x8000, 0x0003, 0xC000, 0x0006, 0x6000, 0x000D, 0xB000,
	0x001B, 0xD800, 0x0037, 0xEC00, 0x006F, 0xF600, 0x00DF, 0xFB00,
	0x01BF, 0xFD80, 0x037F, 0xFEC0, 0x06FF, 0xFF60, 0x0DFF, 0xFFB0,
	0x1BFF, 0xFFD8, 0x37FF, 0xFFEC, 0x6FFF, 0xFFF6, 0xDFFF, 0xFFFB,
	0xDFFF, 0xFFFB, 0x6FFF, 0xFFF6, 0x37FF, 0xFFEC, 0x1BFF, 0xFFD8,
	0x0DFF, 0xFFB0, 0x06FF, 0xFF60, 0x037F, 0xFEC0, 0x01BF, 0xFD80,
	0x00DF, 0xFB00, 0x006F, 0xF600, 0x0037, 0xEC00, 0x001B, 0xD800,
	0x000D, 0xB000, 0x0006, 0x6000, 0x0003, 0xC000, 0x0001, 0x8000,

	0x0001, 0x8000, 0x0003, 0xC000, 0x0006, 0x6000, 0x000D, 0xB000,
	0x001B, 0xD800, 0x0037, 0xEC00, 0x006F, 0xF600, 0x00DF, 0xFB00,
	0x01BF, 0xFD80, 0x037F, 0xFEC0, 0x06FF, 0xFF60, 0x0DFF, 0xFFB0,
	0x1BFF, 0xFFD8, 0x37FF, 0xFFEC, 0x6FFF, 0xFFF6, 0xDFFF, 0xFFFB,
	0xDFFF, 0xFFFB, 0x6FFF, 0xFFF6, 0x37FF, 0xFFEC, 0x1BFF, 0xFFD8,
	0x0DFF, 0xFFB0, 0x06FF, 0xFF60, 0x037F, 0xFEC0, 0x01BF, 0xFD80,
	0x00DF, 0xFB00, 0x006F, 0xF600, 0x0037, 0xEC00, 0x001B, 0xD800,
	0x000D, 0xB000, 0x0006, 0x6000, 0x0003, 0xC000, 0x0001, 0x8000,

	0x0001, 0x8000, 0x0003, 0xC000, 0x0006, 0x6000, 0x000D, 0xB000,
	0x001B, 0xD800, 0x0037, 0xEC00, 0x006F, 0xF600, 0x00DF, 0xFB00,
	0x01BF, 0xFD80, 0x037F, 0xFEC0, 0x06FF, 0xFF60, 0x0DFF, 0xFFB0,
	0x1BFF, 0xFFD8, 0x37FF, 0xFFEC, 0x6FFF, 0xFFF6, 0xDFFF, 0xFFFB,
	0xDFFF, 0xFFFB, 0x6FFF, 0xFFF6, 0x37FF, 0xFFEC, 0x1BFF, 0xFFD8,
	0x0DFF, 0xFFB0, 0x06FF, 0xFF60, 0x037F, 0xFEC0, 0x01BF, 0xFD80,
	0x00DF, 0xFB00, 0x006F, 0xF600, 0x0037, 0xEC00, 0x001B, 0xD800,
	0x000D, 0xB000, 0x0006, 0x6000, 0x0003, 0xC000, 0x0001, 0x8000,
};

static UWORD chip hrNoteIconData[] = {
	0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF,
	0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF,
	0xFFFF, 0xFFFF, 0xFFFE, 0x7FFF, 0xFFFE, 0x7FFF, 0xFFE6, 0x67FF,
	0xFFE2, 0x47FF, 0xFFF0, 0x0FFF, 0xFFF8, 0x1FFF, 0xFF80, 0x01FF,
	0xFF80, 0x01FF, 0xFFF8, 0x1FFF, 0xFFF0, 0x0FFF, 0xFFE2, 0x47FF,
	0xFFE6, 0x67FF, 0xFFFE, 0x7FFF, 0xFFFE, 0x7FFF, 0xFFFF, 0xFFFF,
	0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF,
	0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF,

	0x0001, 0x8000, 0x0003, 0xC000, 0x0006, 0x6000, 0x000D, 0xB000,
	0x001B, 0xD800, 0x0037, 0xEC00, 0x006F, 0xF600, 0x00DF, 0xFB00,
	0x01BF, 0xFD80, 0x037F, 0xFEC0, 0x06FF, 0xFF60, 0x0DFF, 0xFFB0,
	0x1BFF, 0xFFD8, 0x37FF, 0xFFEC, 0x6FFF, 0xFFF6, 0xDFFF, 0xFFFB,
	0xDFFF, 0xFFFB, 0x6FFF, 0xFFF6, 0x37FF, 0xFFEC, 0x1BFF, 0xFFD8,
	0x0DFF, 0xFFB0, 0x06FF, 0xFF60, 0x037F, 0xFEC0, 0x01BF, 0xFD80,
	0x00DF, 0xFB00, 0x006F, 0xF600, 0x0037, 0xEC00, 0x001B, 0xD800,
	0x000D, 0xB000, 0x0006, 0x6000, 0x0003, 0xC000, 0x0001, 0x8000,

	0x0001, 0x8000, 0x0003, 0xC000, 0x0006, 0x6000, 0x000D, 0xB000,
	0x001B, 0xD800, 0x0037, 0xEC00, 0x006F, 0xF600, 0x00DF, 0xFB00,
	0x01BF, 0xFD80, 0x037F, 0xFEC0, 0x06FF, 0xFF60, 0x0DFF, 0xFFB0,
	0x1BFF, 0xFFD8, 0x37FF, 0xFFEC, 0x6FFF, 0xFFF6, 0xDFFF, 0xFFFB,
	0xDFFF, 0xFFFB, 0x6FFF, 0xFFF6, 0x37FF, 0xFFEC, 0x1BFF, 0xFFD8,
	0x0DFF, 0xFFB0, 0x06FF, 0xFF60, 0x037F, 0xFEC0, 0x01BF, 0xFD80,
	0x00DF, 0xFB00, 0x006F, 0xF600, 0x0037, 0xEC00, 0x001B, 0xD800,
	0x000D, 0xB000, 0x0006, 0x6000, 0x0003, 0xC000, 0x0001, 0x8000,

	0x0001, 0x8000, 0x0003, 0xC000, 0x0006, 0x6000, 0x000D, 0xB000,
	0x001B, 0xD800, 0x0037, 0xEC00, 0x006F, 0xF600, 0x00DF, 0xFB00,
	0x01BF, 0xFD80, 0x037F, 0xFEC0, 0x06FF, 0xFF60, 0x0DFF, 0xFFB0,
	0x1BFF, 0xFFD8, 0x37FF, 0xFFEC, 0x6FFF, 0xFFF6, 0xDFFF, 0xFFFB,
	0xDFFF, 0xFFFB, 0x6FFF, 0xFFF6, 0x37FF, 0xFFEC, 0x1BFF, 0xFFD8,
	0x0DFF, 0xFFB0, 0x06FF, 0xFF60, 0x037F, 0xFEC0, 0x01BF, 0xFD80,
	0x00DF, 0xFB00, 0x006F, 0xF600, 0x0037, 0xEC00, 0x001B, 0xD800,
	0x000D, 0xB000, 0x0006, 0x6000, 0x0003, 0xC000, 0x0001, 0x8000,
};

static Image hrStopIconImage = {
	0, 0, 32, 32, 4, &hrStopIconData[0], 0x0F, 0, NULL
};

static Image hrCautionIconImage = {
	0, 0, 32, 32, 4, &hrCautionIconData[0], 0x0F, 0, NULL
};

static Image hrNoteIconImage = {
	0, 0, 32, 32, 4, &hrNoteIconData[0], 0x0F, 0, NULL
};

static UWORD chip mrStopIconData[] = {
	0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFE, 0x7FFF,
	0xFFFC, 0x3FFF, 0xFFFC, 0x3FFF, 0xFFFC, 0x3FFF, 0xFFFC, 0x3FFF,
	0xFFFC, 0x3FFF, 0xFFFE, 0x7FFF, 0xFFFE, 0x7FFF, 0xFFFF, 0xFFFF,
	0xFFFE, 0x7FFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF,

	0x003F, 0xFC00, 0x00C0, 0x0300, 0x033F, 0xFCC0, 0x0CFF, 0xFF30,
	0x33FF, 0xFFCC, 0xCFFF, 0xFFF3, 0xCFFF, 0xFFF3, 0xCFFF, 0xFFF3,
	0xCFFF, 0xFFF3, 0xCFFF, 0xFFF3, 0xCFFF, 0xFFF3, 0x33FF, 0xFFCC,
	0x0CFF, 0xFF30, 0x033F, 0xFCC0, 0x00C0, 0x0300, 0x003F, 0xFC00,

	0x003F, 0xFC00, 0x00C0, 0x0300, 0x033F, 0xFCC0, 0x0CFF, 0xFF30,
	0x33FF, 0xFFCC, 0xCFFF, 0xFFF3, 0xCFFF, 0xFFF3, 0xCFFF, 0xFFF3,
	0xCFFF, 0xFFF3, 0xCFFF, 0xFFF3, 0xCFFF, 0xFFF3, 0x33FF, 0xFFCC,
	0x0CFF, 0xFF30, 0x033F, 0xFCC0, 0x00C0, 0x0300, 0x003F, 0xFC00,

	0x003F, 0xFC00, 0x00C0, 0x0300, 0x033F, 0xFCC0, 0x0CFF, 0xFF30,
	0x33FF, 0xFFCC, 0xCFFF, 0xFFF3, 0xCFFF, 0xFFF3, 0xCFFF, 0xFFF3,
	0xCFFF, 0xFFF3, 0xCFFF, 0xFFF3, 0xCFFF, 0xFFF3, 0x33FF, 0xFFCC,
	0x0CFF, 0xFF30, 0x033F, 0xFCC0, 0x00C0, 0x0300, 0x003F, 0xFC00,
};

static UWORD chip mrCautionIconData[] = {
	0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFC, 0x3FFF,
	0xFFE0, 0x07FF, 0xFFC7, 0xC3FF, 0xFFFF, 0xC3FF, 0xFFFF, 0x87FF,
	0xFFFF, 0x0FFF, 0xFFFC, 0x3FFF, 0xFFFE, 0x7FFF, 0xFFFF, 0xFFFF,
	0xFFFE, 0x7FFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF,

	0x0003, 0xC000, 0x000C, 0x3000, 0x0033, 0xCC00, 0x00CF, 0xF300,
	0x033F, 0xFCC0, 0x0CFF, 0xFF30, 0x33FF, 0xFFCC, 0xCFFF, 0xFFF3,
	0xCFFF, 0xFFF3, 0x33FF, 0xFFCC, 0x0CFF, 0xFF30, 0x033F, 0xFCC0,
	0x00CF, 0xF300, 0x0033, 0xCC00, 0x000C, 0x3000, 0x0003, 0xC000,

	0x0003, 0xC000, 0x000C, 0x3000, 0x0033, 0xCC00, 0x00CF, 0xF300,
	0x033F, 0xFCC0, 0x0CFF, 0xFF30, 0x33FF, 0xFFCC, 0xCFFF, 0xFFF3,
	0xCFFF, 0xFFF3, 0x33FF, 0xFFCC, 0x0CFF, 0xFF30, 0x033F, 0xFCC0,
	0x00CF, 0xF300, 0x0033, 0xCC00, 0x000C, 0x3000, 0x0003, 0xC000,

	0x0003, 0xC000, 0x000C, 0x3000, 0x0033, 0xCC00, 0x00CF, 0xF300,
	0x033F, 0xFCC0, 0x0CFF, 0xFF30, 0x33FF, 0xFFCC, 0xCFFF, 0xFFF3,
	0xCFFF, 0xFFF3, 0x33FF, 0xFFCC, 0x0CFF, 0xFF30, 0x033F, 0xFCC0,
	0x00CF, 0xF300, 0x0033, 0xCC00, 0x000C, 0x3000, 0x0003, 0xC000,
};

static UWORD chip mrNoteIconData[] = {
	0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF,
	0xFFFE, 0x7FFF, 0xFFE6, 0x67FF, 0xFFF8, 0x1FFF, 0xFF80, 0x01FF,
	0xFFF8, 0x1FFF, 0xFFE6, 0x67FF, 0xFFFE, 0x7FFF, 0xFFFF, 0xFFFF,
	0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF,

	0x0003, 0xC000, 0x000C, 0x3000, 0x0033, 0xCC00, 0x00CF, 0xF300,
	0x033F, 0xFCC0, 0x0CFF, 0xFF30, 0x33FF, 0xFFCC, 0xCFFF, 0xFFF3,
	0xCFFF, 0xFFF3, 0x33FF, 0xFFCC, 0x0CFF, 0xFF30, 0x033F, 0xFCC0,
	0x00CF, 0xF300, 0x0033, 0xCC00, 0x000C, 0x3000, 0x0003, 0xC000,

	0x0003, 0xC000, 0x000C, 0x3000, 0x0033, 0xCC00, 0x00CF, 0xF300,
	0x033F, 0xFCC0, 0x0CFF, 0xFF30, 0x33FF, 0xFFCC, 0xCFFF, 0xFFF3,
	0xCFFF, 0xFFF3, 0x33FF, 0xFFCC, 0x0CFF, 0xFF30, 0x033F, 0xFCC0,
	0x00CF, 0xF300, 0x0033, 0xCC00, 0x000C, 0x3000, 0x0003, 0xC000,

	0x0003, 0xC000, 0x000C, 0x3000, 0x0033, 0xCC00, 0x00CF, 0xF300,
	0x033F, 0xFCC0, 0x0CFF, 0xFF30, 0x33FF, 0xFFCC, 0xCFFF, 0xFFF3,
	0xCFFF, 0xFFF3, 0x33FF, 0xFFCC, 0x0CFF, 0xFF30, 0x033F, 0xFCC0,
	0x00CF, 0xF300, 0x0033, 0xCC00, 0x000C, 0x3000, 0x0003, 0xC000,
};

static Image mrStopIconImage = {
	0, 0, 32, 16, 4, &mrStopIconData[0], 0x0F, 0, NULL
};

static Image mrCautionIconImage = {
	0, 0, 32, 16, 4, &mrCautionIconData[0], 0x0F, 0, NULL
};

static Image mrNoteIconImage = {
	0, 0, 32, 16, 4, &mrNoteIconData[0], 0x0F, 0, NULL
};

/*
 *	Date info
 */

static TextPtr monthNames[] = {
	"January",		"February",		"March",		"April",
	"May",			"June",			"July",			"August",
	"September",	"October",		"November",		"December"
};

static TextPtr dayNames[] = {
	"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
};

static UBYTE daysPerMonth[] = {
	31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
};

#define DAYS_IN_YEAR(yr)	((yr % 4) ? 365 : ((yr % 400) ? 366 : 365))

/*
 *	Image array
 *	Must be in order given by image type definitions
 */

static ImagePtr hrImageArray[] = {
	&upArrowImage,
	&downArrowImage,
	&leftArrowImage,
	&rightArrowImage,
	&hrStopIconImage,
	&hrCautionIconImage,
	&hrNoteIconImage
};

static ImagePtr mrImageArray[] = {
	&upArrowImage,
	&downArrowImage,
	&leftArrowImage,
	&rightArrowImage,
	&mrStopIconImage,
	&mrCautionIconImage,
	&mrNoteIconImage
};

/*
 *	Array of upper and lower case letters for Amiga character set
 */

TextChar toUpper[] = {
	0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0A,0x0B,0x0C,0x0D,0x0E,0x0F,
	0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x1A,0x1B,0x1C,0x1D,0x1E,0x1F,
	 ' ', '!','\"', '#', '$', '%', '&','\'', '(', ')', '*', '+', ',', '-', '.', '/',
	 '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ':', ';', '<', '=', '>', '?',
	 '@', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O',
	 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '[','\\', ']', '^', '_',
	 '`', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O',
	 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '{', '|', '}', '~',0x7F,
	0x80,0x81,0x82,0x83,0x84,0x85,0x86,0x87,0x88,0x89,0x8A,0x8B,0x8C,0x8D,0x8E,0x8F,
	0x90,0x91,0x92,0x93,0x94,0x95,0x96,0x97,0x98,0x99,0x9A,0x9B,0x9C,0x9D,0x9E,0x9F,
	0xA0,0xA1,0xA2,0xA3,0xA4,0xA5,0xA6,0xA7,0xA8,0xA9,0xAA,0xAB,0xAC,0xAD,0xAE,0xAF,
	0xB0,0xB1,0xB2,0xB3,0xB4,0xB5,0xB6,0xB7,0xB8,0xB9,0xBA,0xBB,0xBC,0xBD,0xBE,0xBF,
	 '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�',
	 '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�',
	 '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�',
	 '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�',
};

TextChar toLower[] = {
	0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0A,0x0B,0x0C,0x0D,0x0E,0x0F,
	0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x1A,0x1B,0x1C,0x1D,0x1E,0x1F,
	 ' ', '!', '"', '#', '$', '%', '&','\'', '(', ')', '*', '+', ',', '-', '.', '/',
	 '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ':', ';', '<', '=', '>', '?',
	 '@', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o',
	 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '[','\\', ']', '^', '_',
	 '`', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o',
	 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '{', '|', '}', '~',0x7F,
	0x80,0x81,0x82,0x83,0x84,0x85,0x86,0x87,0x88,0x89,0x8A,0x8B,0x8C,0x8D,0x8E,0x8F,
	0x90,0x91,0x92,0x93,0x94,0x95,0x96,0x97,0x98,0x99,0x9A,0x9B,0x9C,0x9D,0x9E,0x9F,
	0xA0,0xA1,0xA2,0xA3,0xA4,0xA5,0xA6,0xA7,0xA8,0xA9,0xAA,0xAB,0xAC,0xAD,0xAE,0xAF,
	0xB0,0xB1,0xB2,0xB3,0xB4,0xB5,0xB6,0xB7,0xB8,0xB9,0xBA,0xBB,0xBC,0xBD,0xBE,0xBF,
	 '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�',
	 '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�',
	 '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�',
	 '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�',

};

/*
 *	Flag for which characters are valid word characters
 *	A word character is either a number, a single quote, a soft hyphen
 *		or a character that could be part of a spelling check dictionary
 */

BYTE wordChar[] = {
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
	0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0,
	0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1,
};

/*
 *	Local prototypes
 */

BorderPtr	NewBorder(WORD, WORD, WORD, WORD);

/*
 *	InitToolbox
 *	Set the resolution used for the toolbox and font used for toolbox
 *		IntuiText (used in gadgets and menus)
 *	If textAttr is NULL then don't change current setting
 *	Default state is med-res and Topaz-8 font
 */

void InitToolbox(BOOL hiRes, TextAttrPtr textAttr)
{
	_tbHiRes = hiRes;
	if (textAttr) {
		_tbTextAttr = textAttr;
		_tbTextAttr->ta_Style = FS_NORMAL;	/* Needs to be like this */
	}
}

/*
 *	SetToolboxColors
 *	Set default colors for toolbox to use
 */

void SetToolboxColors(WORD black, WORD white, WORD dark, WORD light)
{
	_tbBlackColor = black;
	_tbWhiteColor = white;
	_tbDarkColor = dark;
	_tbLightColor = light;
}

/*
 *	Enable or disable auto-activation of text boxes in requesters and dialogs
 *	Default is enabled
 */

void AutoActivateEnable(BOOL enable)
{
	_tbAutoActivate = enable;
}

/*
 *	LibraryVersion
 *	Return version number of library
 */

UWORD LibraryVersion(struct Library *libBase)
{
	return (libBase->lib_Version);
}

/*
 *	SystemVersion
 *	Return version number of system
 */

UWORD SystemVersion()
{
	struct Library *AbsExecBase;

	AbsExecBase = *((struct Library **) 4L);		/* Absolute address */
	return (LibraryVersion(AbsExecBase));
}

/*
 *	SetStdPointer
 *	Set pointer to one of standard types
 */

static UWORD	*sysPointerData;

void SetStdPointer(WindowPtr window, UWORD pointerType)
{
	register PointerPtr pointer;

	if (pointerType > POINTER_MAX_TYPE)
		return;
	if (pointerType == POINTER_ARROW &&
		LibraryVersion((struct Library *) IntuitionBase) >= OSVERSION_2_0) {
		if (window->Pointer != sysPointerData) {
			ClearPointer(window);
			sysPointerData = window->Pointer;
		}
	}
	else {
		pointer = pointerArray[pointerType];
		if (window->Pointer != pointer->PointerData)
			SetPointer(window, pointer->PointerData,
					   pointer->Height, pointer->Width,
					   pointer->XOffset, pointer->YOffset);
	}
}

/*
 *	ObscurePointer
 *	Hide the pointer from view
 *	(Since we compile with -b option, we must reference off a Pointer struct)
 */

void ObscurePointer(WindowPtr window)
{
	if (window->Pointer != nullPointer.PointerData)
		SetPointer(window, nullPointer.PointerData, 0, 0, 0, 0);
}

/*
 *	GetImage
 *	Return pointer to actual Image structure requested
 *	If user wishes to modify image, they must make a copy of it
 *	Return NULL if no such image
 */

ImagePtr GetImage(UWORD imageNum)
{
	ImagePtr *imageArray;

	if (imageNum > IMAGE_MAX_TYPE)
		return (NULL);
	imageArray = (_tbHiRes) ? hrImageArray : mrImageArray;
	return (imageArray[imageNum]);
}

/*
 *	SysBeep
 *	Produce beep for specified ticks (1/50 of sec.)
 */

#define SOUND_FREQ	330		/* In Hertz */

static BYTE chip soundData[] = {
	0,  64,  127,  127,  127,  127,  127,  64,
	0, -64, -127, -127, -127, -127, -127, -64
};

#define LEFTCHAN_MASK	0x09
#define RIGHTCHAN_MASK	0x06

static UBYTE audChanMasks[] = { 0x03, 0x05, 0x0A, 0x0C };

void SysBeep(UWORD beepTime)
{
	WORD channels;
	MsgPortPtr replyPort;
	struct IOAudio ioa1, ioa2;

	ioa1.ioa_Request.io_Message.mn_Node.ln_Pri = 80;
	ioa1.ioa_Data = audChanMasks;
	ioa1.ioa_Length = sizeof(audChanMasks);
	if ((replyPort = CreatePort(NULL, 0)) == NULL)
		return;
	ioa1.ioa_Request.io_Message.mn_ReplyPort = replyPort;
/*
	Open device, allocate left & right channels, and set up for sound
*/
	if (OpenDevice(AUDIONAME, 0, (IOReqPtr) &ioa1, 0) == 0) {
		ioa1.ioa_Request.io_Command = CMD_WRITE;
		ioa1.ioa_Request.io_Flags = ADIOF_PERVOL;
		ioa1.ioa_Data = soundData;
		ioa1.ioa_Length = 16;
		ioa1.ioa_Period = (3579545L/(16*SOUND_FREQ));
		ioa1.ioa_Volume = 64;
		ioa1.ioa_Cycles = (SOUND_FREQ*beepTime)/50;
/*
	Do both left and right channels
*/
		ioa2 = ioa1;
		channels = (WORD) ioa1.ioa_Request.io_Unit;
		ioa1.ioa_Request.io_Unit = (struct Unit *) (channels & LEFTCHAN_MASK);
		ioa2.ioa_Request.io_Unit = (struct Unit *) (channels & RIGHTCHAN_MASK);
		BeginIO((IOReqPtr) &ioa1);
		BeginIO((IOReqPtr) &ioa2);
		WaitPort(replyPort);
		(void) GetMsg(replyPort);
		WaitPort(replyPort);
		(void) GetMsg(replyPort);
/*
	Free channels and close device
*/
		ioa1.ioa_Request.io_Unit = (struct Unit *) channels;
		CloseDevice((IOReqPtr) &ioa1);
	}
	DeletePort(replyPort);
}

/*
 *	ConvertKeyMsg
 *	Convert RAWKEY messages for ASCII keys to VANILLAKEY messages
 *	Must be called before message is replied to,
 *		and ConsoleDevice base address must have obtained from console.device
 */

static struct InputEvent keyEvent = {
	NULL, IECLASS_RAWKEY, 0, 0, 0, (WORD) 0, (WORD) 0, (ULONG) 0, (ULONG) 0
};

void ConvertKeyMsg(intuiMsg)
register IntuiMsgPtr intuiMsg;
{
	register UWORD code;
	register UWORD qualifier;
	register WORD num;
	UBYTE key;				/* Need to take its address */

	code = intuiMsg->Code;
	qualifier = intuiMsg->Qualifier;
	if (intuiMsg->Class != RAWKEY					||
		(code & IECODE_UP_PREFIX)					||
		(qualifier & AMIGAKEYS)						||
		(code >= CURSORUP && code <= CURSORLEFT)	||
		(code >= RAWKEY_F1 && code <= RAWKEY_F10)	||
		code == RAWKEY_HELP)
		return;
	keyEvent.ie_Code = code;
	keyEvent.ie_Qualifier = qualifier;
	keyEvent.ie_position.ie_addr = *((APTR *) intuiMsg->IAddress);
	if ((num = RawKeyConvert(&keyEvent, &key, 1, NULL)) == 1) {
		intuiMsg->Code = key;
		intuiMsg->Class = VANILLAKEY;
	}
	else if (num == 0)		/* Was SHIFT, ALT, etc. */
		intuiMsg->Code |= IECODE_UP_PREFIX;	/* Make caller ignore it */
}

/*
 *	WaitMouseUp
 *	Check msgPort for mouse up event in window
 *	If message is not for specified window, or is not MOUSEMOVE, MOUSEBUTTONS,
 *	RAWKEY or INTUITICKS, put message back and return FALSE
 *	Otherwise reply to message and return status of select button
 *	Will wait until at least one message is received
 *	(Having INTUITICKS enabled allows us to auto-scroll while dragging)
 */

BOOL WaitMouseUp(MsgPortPtr msgPort, WindowPtr window)
{
	register BOOL mouseDown;
	register ULONG class;
	register UWORD code, modifier;
	register IntuiMsgPtr intuiMsg;

	mouseDown = TRUE;		/* Assume mouse down (for INTUITICKS msgs) */
	(void) WaitPort(msgPort);
	while (mouseDown && (intuiMsg = (IntuiMsgPtr) GetMsg(msgPort)) != NULL) {
		class = intuiMsg->Class;
		code = intuiMsg->Code;
		modifier = intuiMsg->Qualifier;
		if (intuiMsg->IDCMPWindow != window ||
			(class != MOUSEMOVE && class != MOUSEBUTTONS &&
			 class != RAWKEY && class != INTUITICKS)) {
			PutMsg(msgPort, (MsgPtr) intuiMsg);
			return (FALSE);
		}
		ReplyMsg((MsgPtr) intuiMsg);
		if (class == MOUSEBUTTONS && code == SELECTUP)
			mouseDown = FALSE;
		else if (class != INTUITICKS)
			mouseDown = SELECTBUTTON(modifier);
		if (class == MOUSEMOVE)
			return (mouseDown);			/* Return immediately with MOUSEMOVE */
	}
	return (mouseDown);
}

/*
 *	ActiveWindow
 *	Return pointer to window that is currently active
 */

WindowPtr ActiveWindow()
{
	register LONG intuiLock;
	register WindowPtr window;

	intuiLock = LockIBase(0);
	window = IntuitionBase->ActiveWindow;
	UnlockIBase(intuiLock);
	return (window);
}

/*
 *	CmpString
 *	Compare to strings, ignoring capitalization if caseSense == FALSE
 *	Return -1 if s1 < s2, 0 if s1 = s2, +1 if s1 > s2
 */

WORD CmpString(s1, s2, l1, l2, caseSense)
register TextPtr s1, s2;
WORD l1, l2;
BOOL caseSense;
{
	register TextChar ch1, ch2;
	register WORD len;

	len = (l1 < l2) ? l1 : l2;
	if (caseSense) {
		while (len--) {
			if ((ch1 = *s1++) != (ch2 = *s2++)) {
				if (ch1 < ch2)
					return (-1);
				return (1);
			}
		}
	}
	else {
		while (len--) {
			ch1 = *s1++;
			ch2 = *s2++;
			if ((ch1 >= 'A' && ch1 <= 'Z') || (ch1 >= 0xC0 && ch1 <= 0xDF))
				ch1 += 0x20;
			if ((ch2 >= 'A' && ch2 <= 'Z') || (ch2 >= 0xC0 && ch2 <= 0xDF))
				ch2 += 0x20;
			if (ch1 != ch2) {
				if (ch1 < ch2)
					return (-1);
				return (1);
			}
		}
	}
	if (l1 < l2)
		return (-1);
	else if (l1 > l2)
		return (1);
	return (0);
}

/*
 *	NumToString
 *	Convert number to string
 *	String buffer must have enough room for string and trailing NULL
 */

void NumToString(num, s)
register LONG num;
register TextPtr s;
{
	register LONG divisor;

	if (num < 0) {
		*s++ = '-';
		num = -num;
	}
	for (divisor = 1; num/divisor > 9;	divisor *= 10) ;
	while  (divisor) {
		*s++ = num/divisor + '0';
		num %= divisor;
		divisor /= 10;
	}
	*s = '\0';
}

/*
 *	StringToNum
 *	Convert string to number (long)
 *	Skips initial spaces
 */

LONG StringToNum(s)
register TextPtr s;
{
	register WORD sign;
	register LONG num;

	while (*s == ' ')
		s++;
	if (*s == '-') {
		sign = -1;
		s++;
	}
	else
		sign = 1;
	num = 0;
	while (*s >= '0' && *s <= '9') {
		num *= 10;
		num += *s++ & 0x0F;
	}
	if (sign == -1)
		num = -num;
	return (num);
}

/*
 *	DateString
 *	Return date string in indicated form
 */

void DateString(struct DateStamp *dateStamp, WORD dateForm, TextPtr date)
{
	register WORD day, month, year, daysInYear;
	UBYTE buff[5];

/*
	Get year, month, and day
*/
	day = dateStamp->ds_Days;
	year = 1978;
	while (day >= (daysInYear = DAYS_IN_YEAR(year))) {
		day -= daysInYear;
		year++;
	}
	for (month = 0; month < 12; month++) {
		if (day < daysPerMonth[month] || (month == 1 && daysInYear == 366 && day < 29))
			break;
		day -= daysPerMonth[month];
		if (daysInYear == 366 && month == 1)
			day--;
	}
	day++;		/* Real days are numbered from 1 */
/*
	Build date string
*/
	switch (dateForm) {
	case DATE_SHORT:
		month++;			/* Range is 1..12 */
		if (month < 10)
			*date++ = '0';
		NumToString((LONG) month, date);
		date += strlen(date);
		*date++ = '/';
		if (day < 10)
			*date++ = '0';
		NumToString((LONG) day, date);
		date += strlen(date);
		*date++ = '/';
		NumToString((LONG) year, buff);
		strcpy(date, buff + 2);
		break;
	case DATE_MILITARY:
		if (day < 10)
			*date++ = '0';
		NumToString((LONG) day, date);
		date += strlen(date);
		*date++ = ' ';
		memcpy(date, monthNames[month], 3);
		date += 3;
		*date++ = ' ';
		NumToString((LONG) year, buff);
		strcpy(date, buff + 2);
		break;
	case DATE_ABBRDAY:
	case DATE_LONGDAY:
		strcpy(date, dayNames[dateStamp->ds_Days % 7]);
		date += (dateForm == DATE_ABBRDAY) ? 3 : strlen(date);
		*date++ = ',';
		*date++ = ' ';			/* Fall through to next case */
	case DATE_ABBR:
	case DATE_LONG:
		strcpy(date, monthNames[month]);
		date += (dateForm == DATE_ABBR || dateForm == DATE_ABBRDAY) ?
				3 : strlen(date);
		*date++ = ' ';
		NumToString((LONG) day, date);
		date += strlen(date);
		*date++ = ',';
		*date++ = ' ';
		NumToString((LONG) year, date);
		break;
	}
}

/*
 *	TimeString
 *	Return time string
 */

void TimeString(dateStamp, needSecs, hour12, time)
struct DateStamp *dateStamp;
BOOL needSecs, hour12;
register TextPtr time;
{
	register WORD second, minute, hour;
	register BOOL am;

/*
	Get hour, minute, and second
*/
	second = dateStamp->ds_Tick/TICKS_PER_SECOND;
	minute = dateStamp->ds_Minute;
	hour = minute/60;
	minute -= hour*60;
	if (hour12) {
		am = (hour < 12);
		if (hour > 12)
			hour -= 12;
		else if (hour == 0)
			hour = 12;
	}
/*
	Build time string
*/
	NumToString((LONG) hour, time);
	time += strlen(time);
	*time++ = ':';
	if (minute < 10)
		*time++ = '0';
	NumToString((LONG) minute, time);
	time += strlen(time);
	if (needSecs) {
		*time++ = ':';
		if (second < 10)
			*time++ = '0';
		NumToString((LONG) second, time);
		time += strlen(time);
	}
	if (hour12)
		strcpy(time, (am) ? " AM" : " PM");
}

/*
 *	NewBorder
 *	Allocate and initialize a new border structure
 *		and allocate data for points (and init to 0,0)
 */

static BorderPtr NewBorder(WORD leftEdge, WORD topEdge, WORD color, WORD points)
{
	BorderPtr border;

	if ((border = MemAlloc(sizeof(Border), MEMF_CLEAR)) == NULL ||
		(border->XY = MemAlloc(points*sizeof(SHORT)*2, MEMF_CLEAR)) == NULL) {
		if (border)
			MemFree(border, sizeof(Border));
		return (NULL);
	}
	border->LeftEdge = leftEdge;
	border->TopEdge  = topEdge;
	border->FrontPen = color;
	border->DrawMode = JAM1;
	border->Count    = points;
	return (border);
}

/*
 *	BoxBorder
 *	Allocate box border
 *	Set to given color, with lines inset from width/height by dx/dy
 *	Return border address, or NULL
 */

BorderPtr BoxBorder(WORD width, WORD height, WORD color, WORD inset)
{
	WORD dx, dy;
	register SHORT *xy;
	register BorderPtr border;

	if (_tbHiRes) {
		dx = dy = inset;
		if ((border = NewBorder(dx, dy, color, 5)) == NULL)
			return (NULL);
		xy = border->XY;
		width  -= dx + dx;
		height -= dy + dy;
		xy[2] = xy[4] = width - 1;
		xy[5] = xy[7] = height - 1;
	}
	else {
		dx = inset << 1;
		dy = inset;
		if ((border = NewBorder(dx, dy, color, 9)) == NULL)
			return (NULL);
		xy = border->XY;
		width  -= dx + dx;
		height -= dy + dy;
		xy[2]  = xy[4]  = width - 1;
		xy[5]  = xy[7]  = xy[13] = xy[15] = height - 1;
		xy[10] = xy[12] = width - 2;
		xy[14] = xy[16] = 1;
	}
	return (border);
}

/*
 *	ShadowBoxBorder
 *	Create shadowed box border
 *	If up is TRUE, create a shadow for object coming up from screen
 *	If light (background) color is the same as the white color, draw a box
 *		around the shado
 *	Note: Called by requester and gadget routines, so cannot be local
 */

BorderPtr ShadowBoxBorder(WORD width, WORD height, WORD inset, BOOL up)
{
	WORD points;
	register SHORT *xy1, *xy2;
	register BorderPtr border, border1, border2;

	points = (_tbHiRes) ? 3 : 5;
	if ((border1 = NewBorder(0, 0, _tbBlackColor, points)) == NULL ||
		(border2 = NewBorder(0, 0, _tbWhiteColor, points)) == NULL) {
		FreeBorder(border1);
		return (NULL);
	}
	border1->NextBorder = border2;
#ifdef NEW_LOOK
	if (_tbLightColor == _tbWhiteColor) {
		if (inset < 0)
			inset--;
		if ((border = BoxBorder(width, height, _tbBlackColor, inset)) == NULL) {
			FreeBorder(border1);
			return (NULL);
		}
		border->NextBorder = border1;
		inset++;
	}
	else
		border = border1;
#else
	if ((border = BoxBorder(width, height, _tbBlackColor, inset)) == NULL ||
		(border->NextBorder = BoxBorder(width, height, _tbBlackColor, (WORD) (inset + 2))) == NULL) {
		FreeBorder(border);
		FreeBorder(border1);
		return (NULL);
	}
	border->NextBorder->NextBorder = border1;
	inset++;
#endif
	if (!up) {
		border1->FrontPen = _tbWhiteColor;
		border2->FrontPen = _tbBlackColor;
	}
	xy1 = border1->XY;
	xy2 = border2->XY;
	if (_tbHiRes) {
		xy1[0] = 1 + inset;
		xy1[1] = xy1[3] = height - 1 - inset;
		xy1[2] = xy1[4] = width - 1 - inset;
		xy1[5] = 1 + inset;
		xy2[0] = width - 1 - inset;
		xy2[1] = xy2[3] = inset;
		xy2[2] = xy2[4] = inset;
		xy2[5] = height - 1 - inset;
	}
	else {
		xy1[0] = 1 + (inset << 1);
		xy1[1] = xy1[3] = xy1[9] = height - 1 - inset;
		xy1[2] = xy1[4] = width - 1 - (inset << 1);
		xy1[5] = inset;
		xy1[6] = xy1[8] = width - 2 - (inset << 1);
		xy1[7] = 1 + inset;
		xy2[0] = width - 2 - (inset << 1);
		xy2[1] = xy2[3] = xy2[9] = inset;
		xy2[2] = xy2[4] = (inset << 1);
		xy2[5] = height - 1 - inset;
		xy2[6] = xy2[8] = 1 + (inset << 1);
		xy2[7] = height - 2 - inset;
	}
	return (border);
}

/*
 *	AppendBorder
 *	Append border to end of another border
 */

void AppendBorder(BorderPtr border1, BorderPtr border2)
{
	while (border1->NextBorder)
		border1 = border1->NextBorder;
	border1->NextBorder = border2;
}

/*
 *	FreeBorder
 *	Free memory allocated by BoxBorder() and ShadowBoxBorder()
 */

void FreeBorder(border)
register BorderPtr border;
{
	register BorderPtr nextBorder;

	while (border) {
		nextBorder = border->NextBorder;
		if (border->XY)
			MemFree(border->XY, 2*(border->Count)*sizeof(SHORT));
		MemFree(border, sizeof(Border));
		border = nextBorder;
	}
}

/*
 *	NewIntuiText
 *	Allocate new IntuiText and set to given parameters
 *	A copy of the text string is used
 *	Return NULL if error
 */

IntuiTextPtr NewIntuiText(WORD leftEdge, WORD topEdge, TextPtr text,
						  WORD style, WORD color)
{
	TextPtr newText;
	IntuiTextPtr intuiText;
	TextAttrPtr textAttr;

	if ((intuiText = MemAlloc(sizeof(IntuiText), MEMF_CLEAR)) == NULL)
		return (NULL);
	if (style != FS_NORMAL &&
		(textAttr = MemAlloc(sizeof(TextAttr), 0)) == NULL) {
		MemFree(intuiText, sizeof(IntuiText));
		return (NULL);
	}
	if (text == NULL)
		newText = NULL;
	else {
		if ((newText = MemAlloc(strlen(text) + 1, 0)) == NULL) {
			MemFree(textAttr, sizeof(TextAttr));
			MemFree(intuiText, sizeof(IntuiText));
			return (NULL);
		}
		strcpy(newText, text);
	}
	intuiText->FrontPen	= color;
	intuiText->DrawMode	= JAM1;
	intuiText->LeftEdge	= leftEdge;
	intuiText->TopEdge	= topEdge;
	intuiText->IText	= newText;
	if (style != FS_NORMAL) {
		*textAttr = *_tbTextAttr;
		textAttr->ta_Style = style;
		intuiText->ITextFont = textAttr;
	}
	else
		intuiText->ITextFont = _tbTextAttr;
	return (intuiText);
}

/*
 *	FreeIntuiText
 *	Free memory allocated by NewIntuiText
 */

void FreeIntuiText(IntuiTextPtr intuiText)
{
	IntuiTextPtr nextText;

	while (intuiText) {
		nextText = intuiText->NextText;
		if (intuiText->ITextFont != _tbTextAttr)
			MemFree(intuiText->ITextFont, sizeof(TextAttr));
		if (intuiText->IText)
			MemFree(intuiText->IText, strlen(intuiText->IText) + 1);
		MemFree(intuiText, sizeof(IntuiText));
		intuiText = nextText;
	}
}

/*
 *	ChangeIntuiText
 *	Change text used by intuiText structure
 *	A copy of the text is used
 *	Return success status
 */

BOOL ChangeIntuiText(IntuiTextPtr intuiText, TextPtr text)
{
	TextPtr oldText, newText;

	if (intuiText == NULL)
		return (FALSE);
	if (text == NULL)
		newText = NULL;
	else {
		if ((newText = MemAlloc(strlen(text) + 1, 0)) == NULL)
			return (FALSE);
		strcpy(newText, text);
	}
	oldText = intuiText->IText;
	intuiText->IText = newText;
	if (oldText)
		MemFree(oldText, strlen(oldText) + 1);
	return (TRUE);
}

/*
 *	SetIntuiTextFont
 *	Set font and style of intuiText, uses a copy of the textAttr passed
 *	Return success status
 */

BOOL SetIntuiTextFont(IntuiTextPtr intuiText, TextAttrPtr textAttr)
{
	TextAttrPtr newAttr;

	if (intuiText == NULL ||
		(newAttr = MemAlloc(sizeof(TextAttr), 0)) == NULL)
		return (FALSE);
	*newAttr = *textAttr;
	if (intuiText->ITextFont != _tbTextAttr)
		MemFree(intuiText->ITextFont, sizeof(TextAttr));
	intuiText->ITextFont = newAttr;
	return (TRUE);
}

/*
 *	GetFont
 *	Get specified font, either from memory or disk
 */

TextFontPtr GetFont(TextAttrPtr textAttr)
{
	register TextFontPtr font;

	font = OpenFont(textAttr);
	if (font == NULL || font->tf_YSize != textAttr->ta_YSize) {
		if (font)
			CloseFont(font);
		if (DiskfontBase) {
			font = OpenDiskFont(textAttr);
			if (font == NULL || font->tf_YSize != textAttr->ta_YSize) {
				if (font)
					CloseFont(font);
				font = NULL;
			}
		}
		else
			font = NULL;
	}
	return (font);
}
