/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Toolbox
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	Date and time routines
 */

#include <exec/types.h>

#include <string.h>

#include <Typedefs.h>

#include <Toolbox/Utility.h>
#include <Toolbox/DateTime.h>

#include <proto/exec.h>

/*
 *	External variables
 */

extern TextPtr	_monthNames[], _monthNamesAbbr[], _dayNames[], _dayNamesAbbr[];
extern TextPtr	_dateFormats[];

/*
 *	Date info
 */

static UBYTE daysPerMonth[] = {
	31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
};

#define DAYS_IN_YEAR(yr)	((yr % 4) ? 365 : 366)	/* Valid till 2100 */

/*
 *	MonthName
 *	Return month name in buffer
 *	Buffer must be large enough for entire month name, even if abbrev is TRUE
 *	monthNum is in range 1..12
 */

void MonthName(WORD month, BOOL abbrev, TextPtr buff)
{
	month--;
	if (month < 0)
		month = 0;
	else if (month > 11)
		month = 11;
	strcpy(buff, (abbrev) ? _monthNamesAbbr[month] : _monthNames[month]);
}

/*
 *	DateString
 *	Return date string in indicated form
 */

void DateString(struct DateStamp *dateStamp, WORD dateForm, TextPtr date)
{
	register WORD day, month, year, daysInYear, weekDay;
	register TextPtr s1, s2;
	UBYTE yearBuff[5], buff[100];

/*
	Get year, month, and day
*/
	day = dateStamp->ds_Days;
	weekDay = day % 7;				/* 0 to 6 for Sunday through Saturday */
	year = 1978;
	while (day >= (daysInYear = DAYS_IN_YEAR(year))) {
		day -= daysInYear;
		year++;
	}
	daysPerMonth[1] = (year % 4) ? 28 : 29;		/* Valid till 2100 */
	for (month = 0; month < 12; month++) {
		if (day < daysPerMonth[month])
			break;
		day -= daysPerMonth[month];
	}
	day++;		/* Days are numbered from 1 */
	month++;	/* Months are numbered from 1 */
/*
	Get the format string
*/
	switch (dateForm) {
	case DATE_SHORT:
	case DATE_ABBR:
	case DATE_LONG:
	case DATE_ABBRDAY:
	case DATE_LONGDAY:
	case DATE_MILITARY:
		strcpy(buff, _dateFormats[dateForm]);
		break;
	case DATE_CUSTOM:
		strcpy(buff, date);
		break;
	default:
		return;
	}
/*
	Build date string
*/
	for (s1 = buff, s2 = date; *s1; s1++) {
		switch (*s1) {
		case DAY_NUMZERO:
			if (day < 10)
				*s2++ = '0';
			;						/* Fall through */
		case DAY_NUMBER:
			NumToString(day, s2);
			s2 += strlen(s2);
			break;
		case DAY_NAME:
			strcpy(s2, _dayNames[weekDay]);
			s2 += strlen(s2);
			break;
		case DAY_NAMEABBREV:
			strcpy(s2, _dayNamesAbbr[weekDay]);
			s2 += strlen(s2);
			break;
		case MONTH_NUMZERO:
			if (month < 10)
				*s2++ = '0';
			;						/* Fall through */
		case MONTH_NUMBER:
			NumToString(month, s2);
			s2 += strlen(s2);
			break;
		case MONTH_NAME:
			strcpy(s2, _monthNames[month - 1]);
			s2 += strlen(s2);
			break;
		case MONTH_NAMEABBREV:
			strcpy(s2, _monthNamesAbbr[month - 1]);
			s2 += strlen(s2);
			break;
		case YEAR_LONG:
		case YEAR_SHORT:
			NumToString(year, yearBuff);
			strcpy(s2, (*s1 == YEAR_SHORT) ? yearBuff + 2 : yearBuff);
			s2 += strlen(s2);
			break;
		default:
			*s2++ = *s1;
			break;
		}
	}
	*s2 = '\0';
}

/*
 *	TimeString
 *	Return time string
 */

void TimeString(struct DateStamp *dateStamp, BOOL needSecs, BOOL hour12, register TextPtr time)
{
	register WORD second, minute, hour;
	register BOOL am;

/*
	Get hour, minute, and second
*/
	second = dateStamp->ds_Tick/TICKS_PER_SECOND;
	minute = dateStamp->ds_Minute;
	hour = minute/60;
	minute -= hour*60;
	if (hour12) {
		am = (hour < 12);
		if (hour > 12)
			hour -= 12;
		else if (hour == 0)
			hour = 12;
	}
/*
	Build time string
*/
	NumToString(hour, time);
	time += strlen(time);
	*time++ = ':';
	if (minute < 10)
		*time++ = '0';
	NumToString(minute, time);
	time += strlen(time);
	if (needSecs) {
		*time++ = ':';
		if (second < 10)
			*time++ = '0';
		NumToString(second, time);
		time += strlen(time);
	}
	if (hour12)
		strcpy(time, (am) ? " AM" : " PM");
}

