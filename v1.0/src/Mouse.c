/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Mouse routines
 */

#include <exec/types.h>
#include <graphics/gfxmacros.h>
#include <intuition/intuition.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>

#include <Toolbox/Graphics.h>
#include <Toolbox/Window.h>
#include <Toolbox/Utility.h>

#include "Draw.h"
#include "Proto.h"

/*
 *	External variables
 */

extern ScreenPtr	screen;
extern MsgPortPtr	mainMsgPort;

extern WindowPtr	toolWindow, penWindow, fillWindow;

extern WORD	drawTool;

extern BOOL	titleChanged;
extern BOOL	doubleClick, tripleClick;

extern Rectangle	dragRect;		/* In document coordinates */
extern BOOL			autoScroll;

/*
 *	Local variables and definitions
 */

static ULONG	prevSecs, prevMicros;		/* Inited to 0 */
static WORD		prevMouseX, prevMouseY;		/* Inited to 0 */

static BOOL	sizing, dragging, wasMoved;

static UWORD	selLinePat;

/*
 *	Local prototypes
 */

BOOL	AddObjectHandle(WindowPtr, DocObjPtr, PointPtr);
void	RemObjectHandle(WindowPtr, DocObjPtr, PointPtr, RectPtr);
void	GrowObject(WindowPtr, DocObjPtr, UWORD, WORD, WORD);

void	SelectTrack(WindowPtr, WORD, WORD, WORD, WORD, BOOL);
void	DoSelectBox(WindowPtr, UWORD, WORD, WORD);

void	DragTrack(WindowPtr, WORD, WORD, WORD, WORD, BOOL);
void	DoDrag(WindowPtr, WORD, WORD);

void	DoSelect(WindowPtr, UWORD, WORD, WORD);

DocObjPtr	CreateObject(WindowPtr, UWORD, WORD, WORD);

void	DoCreate(WindowPtr, UWORD, WORD, WORD);

/*
 *	Set frame rectangle from start and end doc coordinates
 */

void SetFrameRect(RectPtr rect, WORD xStart, WORD yStart, WORD xEnd, WORD yEnd)
{
	rect->MinX = MIN(xStart, xEnd);
	rect->MinY = MIN(yStart, yEnd);
	rect->MaxX = MAX(xStart, xEnd);
	rect->MaxY = MAX(yStart, yEnd);
}

/*
 *	Get rectangle containing all selected objects (in document coordinates)
 *	Return FALSE if no object selected
 */

BOOL GetSelectRect(DocDataPtr docData, RectPtr rect)
{
	BOOL success;
	DocObjPtr docObj;

	success = FALSE;
	for (docObj = FirstSelected(docData); docObj; docObj = NextSelected(docObj)) {
		if (!success)
			*rect = docObj->Frame;
		else
			UnionRect(&docObj->Frame, rect, rect);
		success = TRUE;
	}
	return (success);
}

/*
 *	Track mouse movement and draw object outline
 *	Constrain to square if SHIFT key down (or to horizontal/vertical line)
 *	If not dragging, this routine also sets dragRect to frame of mouse start/end
 *	Return final mouse position
 */

void TrackMouse(WindowPtr window, UWORD modifier, WORD *x, WORD *y,
				void (*trackDraw)(WindowPtr, WORD, WORD, WORD, WORD, BOOL),
				void (*constrain)(WORD, WORD, WORD *, WORD *))
{
	WORD xStart, yStart, xEnd, yEnd, xOffset, yOffset, width, height;
	WORD newXEnd, newYEnd;
	WORD right, bottom;
	Point mousePt;
	DocDataPtr docData = GetWRefCon(window);
	Rectangle origRect, contentRect;

	if (trackDraw == NULL)
		return;
	GetContentRect(window, &contentRect);
	DocToWindow(window, *x, *y, &mousePt.x, &mousePt.y);
	xStart = xEnd = *x;
	yStart = yEnd = *y;
	right = docData->DocWidth - 1;;
	bottom = docData->DocHeight - 1;
	if (dragging) {
		xOffset = xStart - dragRect.MinX;
		yOffset = yStart - dragRect.MinY;
		width = dragRect.MaxX - dragRect.MinX;
		height = dragRect.MaxY - dragRect.MinY;
		origRect = dragRect;
	}
	else
		xOffset = yOffset = width = height = 0;
/*
	Draw new ruler indicators
*/
	RulerIndicOff(window);
	if (!sizing)
		docData->Flags |= DOC_DRAGGING;		/* So ruler indics will show rect */
	if (!dragging)
		SetFrameRect(&dragRect, xStart, yStart, xEnd, yEnd);
	RulerIndicOn(window);
/*
	Track mouse movement and update display
	Try to minimize drawing operations
*/
	(*trackDraw)(window, xStart, yStart, xEnd, yEnd, TRUE);
	while (WaitMouseUp(mainMsgPort, window)) {
		WaitBOVP(&screen->ViewPort);			/* Avoid beam collision */
		if (mousePt.x == window->MouseX && mousePt.y == window->MouseY &&
			PtInRect(&mousePt, &contentRect)) {
			(*trackDraw)(window, xStart, yStart, xEnd, yEnd, FALSE);
			continue;
		}
/*
	Get new position in document
*/
		mousePt.x = window->MouseX;
		mousePt.y = window->MouseY;
		if (!PtInRect(&mousePt, &contentRect) && !ObjectsLocked(docData))
			(*trackDraw)(window, xStart, yStart, xEnd, yEnd, TRUE);
		if (autoScroll) {
			if (mousePt.x < contentRect.MinX)
				ScrollLeft(window, FALSE);
			else if (mousePt.x > contentRect.MaxX)
				ScrollRight(window, FALSE);
			if (mousePt.y < contentRect.MinY)
				ScrollUp(window, FALSE);
			else if (mousePt.y > contentRect.MaxY)
				ScrollDown(window, FALSE);
		}
		WindowToDoc(window, mousePt.x, mousePt.y, &newXEnd, &newYEnd);
		SnapToGrid(docData, &newXEnd, &newYEnd);
/*
	Clip position to edges of document
*/
		if (newXEnd - xOffset < 0)
			newXEnd = xOffset;
		else if (newXEnd + width - xOffset > right)
			newXEnd = right - width + xOffset;
		if (newYEnd - yOffset < 0)
			newYEnd = yOffset;
		else if (newYEnd + height - yOffset > bottom)
			newYEnd = bottom - height + yOffset;
/*
	Apply constraint
*/
		if ((modifier & SHIFTKEYS) && constrain)
			(*constrain)(xStart, yStart, &newXEnd, &newYEnd);
/*
	Set new drag rect and update display
	If any objects are locked, don't modify
*/
		if (newXEnd == xEnd && newYEnd == yEnd && PtInRect(&mousePt, &contentRect))
			continue;
		if (ObjectsLocked(docData)) {
			Error(ERR_OBJ_LOCKED);
			break;
		}
		if (PtInRect(&mousePt, &contentRect))
			(*trackDraw)(window, xStart, yStart, xEnd, yEnd, TRUE);
		xEnd = newXEnd;
		yEnd = newYEnd;
		if (dragging) {
			dragRect = origRect;
			OffsetRect(&dragRect, xEnd - xStart, yEnd - yStart);
		}
		else
			SetFrameRect(&dragRect, xStart, yStart, xEnd, yEnd);
		(*trackDraw)(window, xStart, yStart, xEnd, yEnd, TRUE);
		UpdateRulerIndic(window);
	}
	(*trackDraw)(window, xStart, yStart, xEnd, yEnd, TRUE);
/*
	Restore old ruler indicator
*/
	RulerIndicOff(window);
	docData->Flags &= ~DOC_DRAGGING;
	RulerIndicOn(window);
/*
	Return final position
*/
	*x = xEnd;
	*y = yEnd;
}

/*
 *	Attempt to add object handle
 *	Return TRUE if successful
 *	Only succeeds for polys
 */

static BOOL AddObjectHandle(WindowPtr window, DocObjPtr docObj, Point *pt)
{
	BOOL added;
	Point newPt;

	if (docObj->Type != TYPE_POLY)
		return (FALSE);
	if (ObjectLocked(docObj)) {
		Error(ERR_OBJ_LOCKED);
		return (FALSE);
	}
	PolyHilite(window, (PolyObjPtr) docObj);
	newPt.x = pt->x - docObj->Frame.MinX;
	newPt.y = pt->y - docObj->Frame.MinY;
	added = PolyAddHandle((PolyObjPtr) docObj, &newPt);
	PolyHilite(window, (PolyObjPtr) docObj);
	return (added);
}

/*
 *	Remove object handle
 *	Only succeeds for polys
 */

static void RemObjectHandle(WindowPtr window, DocObjPtr docObj, PointPtr pt, RectPtr handleRect)
{
	WORD handle;
	Point newPt;

	if (docObj->Type != TYPE_POLY)
		return;
	if (ObjectLocked(docObj)) {
		Error(ERR_OBJ_LOCKED);
		return;
	}
	PolyHilite(window, (PolyObjPtr) docObj);
	InvalObjectRect(window, docObj);
	newPt.x = pt->x - docObj->Frame.MinX;
	newPt.y = pt->y - docObj->Frame.MinY;
	handle = PolyHandle((PolyObjPtr) docObj, &newPt, handleRect);
	PolyRemHandle((PolyObjPtr) docObj, handle);
	InvalObjectRect(window, docObj);
	PolyHilite(window, (PolyObjPtr) docObj);
}

/*
 *	Handle mouse down in object grow handle
 *	Grow routines must call HiliteSelectOff/On when changing frame rect
 */

static void GrowObject(WindowPtr window, DocObjPtr docObj, UWORD modifier,
					   WORD mouseX, WORD mouseY)
{
	if (ObjectLocked(docObj)) {
		Error(ERR_OBJ_LOCKED);
		return;
	}
	sizing = TRUE;
	SetStdPointer(window, POINTER_CROSS);
	switch (docObj->Type) {
	case TYPE_GROUP:
		GroupGrow(window, (GroupObjPtr) docObj, modifier, mouseX, mouseY);
		break;
	case TYPE_LINE:
		LineGrow(window, (LineObjPtr) docObj, modifier, mouseX, mouseY);
		break;
	case TYPE_RECT:
		RectGrow(window, (RectObjPtr) docObj, modifier, mouseX, mouseY);
		break;
	case TYPE_OVAL:
		OvalGrow(window, (OvalObjPtr) docObj, modifier, mouseX, mouseY);
		break;
	case TYPE_POLY:
		PolyGrow(window, (PolyObjPtr) docObj, modifier, mouseX, mouseY);
		break;
	case TYPE_TEXT:
		TextGrow(window, (TextObjPtr) docObj, modifier, mouseX, mouseY);
		break;
	case TYPE_BMAP:
		BMapGrow(window, (BMapObjPtr) docObj, modifier, mouseX, mouseY);
		break;
	}
	SetStdPointer(window, POINTER_WAIT);
	UpdateWindow(window);			/* Do it here for crisper response */
	sizing = FALSE;
	SetPointerShape();
}

/*
 *	Draw routine for tracking selection
 */

static void SelectTrack(WindowPtr window, WORD xStart, WORD yStart, WORD xEnd, WORD yEnd,
						BOOL change)
{
	RastPtr rPort = window->RPort;
	Rectangle rect;

	SetFrameRect(&rect, xStart, yStart, xEnd, yEnd);
	DocToWindowRect(window, &rect, &rect);
	SetDrMd(rPort, COMPLEMENT);
	SetDrPt(rPort, selLinePat);
	FrameRect(rPort, &rect, 1, 1);
	if (!change) {
		selLinePat = (selLinePat >> 2) | (selLinePat << 14);
		SetDrPt(rPort, selLinePat);
		FrameRect(rPort, &rect, 1, 1);
	}
}

/*
 *	Draw select box and select contained objects
 */

static void DoSelectBox(WindowPtr window, UWORD modifier, WORD mouseX, WORD mouseY)
{
	WORD xStart, yStart, xEnd, yEnd;
	DocObjPtr docObj;
	DocDataPtr docData = GetWRefCon(window);
	Rectangle selRect;

/*
	Draw selection rectangle
*/
	selLinePat = 0xF0F0;
	WindowToDoc(window, mouseX, mouseY, &xStart, &yStart);
	SnapToGrid(docData, &xStart, &yStart);
	xEnd = xStart;
	yEnd = yStart;
	TrackMouse(window, modifier, &xEnd, &yEnd, SelectTrack, NULL);
	SetFrameRect(&selRect, xStart, yStart, xEnd, yEnd);
/*
	Select all objects completely within selection rectangle
*/
	for (docObj = FirstObject(docData); docObj; docObj = NextObj(docObj)) {
		if (docObj->Frame.MinX >= selRect.MinX &&
			docObj->Frame.MaxX <= selRect.MaxX &&
			docObj->Frame.MinY >= selRect.MinY &&
			docObj->Frame.MaxY <= selRect.MaxY)
			SelectObject(docObj);
	}
}

/*
 *	Draw routine for tracking drag
 */

static void DragTrack(WindowPtr window, WORD xStart, WORD yStart, WORD xEnd, WORD yEnd,
					  BOOL change)
{
	WORD numSelected;
	RastPtr rPort = window->RPort;
	DocObjPtr docObj;
	DocDataPtr docData = GetWRefCon(window);
	Rectangle rect;

	if (change && (xStart != xEnd || yStart != yEnd || wasMoved)) {
		numSelected = 0;
		for (docObj = FirstSelected(docData); docObj; docObj = NextSelected(docObj)) {
			DrawObjectOutline(window, docObj, xEnd - xStart, yEnd - yStart);
			numSelected++;
		}
		if (numSelected > 1) {
			DocToWindowRect(window, &dragRect, &rect);
			SetDrMd(rPort, COMPLEMENT);
			SetDrPt(rPort, 0xAAAA);
			FrameRect(rPort, &rect, 1, 1);
		}
		wasMoved = TRUE;
	}
}

/*
 *	Draw selection to new location
 */

static void DoDrag(WindowPtr window, WORD mouseX, WORD mouseY)
{
	WORD xStart, yStart, xEnd, yEnd;
	DocObjPtr docObj;
	DocDataPtr docData = GetWRefCon(window);

/*
	Get rectangle containing all selected objects
*/
	if (!GetSelectRect(docData, &dragRect))
		return;
/*
	Drag outline of rectangle
*/
	dragging = TRUE;
	wasMoved = FALSE;
	WindowToDoc(window, mouseX, mouseY, &xStart, &yStart);
	SnapToGrid(docData, &xStart, &yStart);
	xEnd = xStart;
	yEnd = yStart;
	TrackMouse(window, 0, &xEnd, &yEnd, DragTrack, NULL);
	dragging = FALSE;
/*
	Move selected objects
*/
	if (xEnd != xStart || yEnd != yStart) {
		SetStdPointer(window, POINTER_WAIT);
		HiliteSelectOff(window);
		for (docObj = FirstSelected(docData); docObj; docObj = NextSelected(docObj)) {
			InvalObjectRect(window, docObj);
			OffsetObject(docObj, xEnd - xStart, yEnd - yStart);
			InvalObjectRect(window, docObj);
		}
		UpdateWindow(window);		/* Do it here for crisper response */
		HiliteSelectOn(window);
		DocModified(docData);
		SetPointerShape();
	}
}

/*
 *	Select objects
 */

static void DoSelect(WindowPtr window, UWORD modifier, WORD mouseX, WORD mouseY)
{
	WORD handleX, handleY;
	BOOL isSelected, shiftKey, altKey;
	DocObjPtr clickObj;
	DocDataPtr docData = GetWRefCon(window);
	Point docPt, handlePt;
	Rectangle handleRect;

	WindowToDoc(window, mouseX, mouseY, &docPt.x, &docPt.y);
	GetHandleRect(window, &handleRect);
	shiftKey = ((modifier & SHIFTKEYS) != 0);
	altKey = ((modifier & ALTKEYS) != 0);
/*
	Check to see if over an object handle
	If ALT key down, attempt to remove the handle (will succeed only for polys)
	Otherwise, reshape the object
*/
	for (clickObj = LastSelected(docData); clickObj; clickObj = PrevSelected(clickObj)) {
		if (PointInHandle(clickObj, &docPt, &handleRect))
			break;
	}
	if (clickObj) {
		if (altKey)
			RemObjectHandle(window, clickObj, &docPt, &handleRect);
		else
			GrowObject(window, clickObj, modifier, mouseX, mouseY);
		DocModified(docData);
		return;
	}
/*
	Find object that is clicked on
*/
	for (clickObj = LastObject(docData); clickObj; clickObj = PrevObj(clickObj)) {
		if (PointInObject(clickObj, &docPt))
			break;
	}
	isSelected = (clickObj && ObjectSelected(clickObj));
/*
	If clickObj already selected, check for adding a handle
*/
	if (isSelected && !shiftKey && altKey) {
		handlePt = docPt;
		DocToWindow(window, handlePt.x, handlePt.y, &handleX, &handleY);
		if (AddObjectHandle(window, clickObj, &handlePt)) {
			GrowObject(window, clickObj, modifier, handleX, handleY);
			DocModified(docData);
		}
	}
/*
	Otherwise select or unselect and drag clickObj
*/
	else {
		if (!isSelected || shiftKey) {
			HiliteSelectOff(window);
/*
	If not multiple select, unselect all objects
*/
			if (!shiftKey)
				UnSelectAllObjects(docData);
/*
	Select or unselect clickObj
*/
			if (clickObj) {
				if (ObjectSelected(clickObj)) {
					UnSelectObject(clickObj);
					isSelected = FALSE;
				}
				else {
					SelectObject(clickObj);
					isSelected = TRUE;
				}
			}
/*
	If nothing selected, do selection rectangle
*/
			else
				DoSelectBox(window, modifier, mouseX, mouseY);
			HiliteSelectOn(window);
		}
/*
	Drag the selection
*/
		if (isSelected)
			DoDrag(window, mouseX, mouseY);
	}
	SetPenMenuDefaults(window);
	SetTextMenuDefaults(window);
}

/*
 *	Create object of type corresponding to current drawTool
 */

static DocObjPtr CreateObject(WindowPtr window, UWORD modifier, WORD mouseX, WORD mouseY)
{
	DocObjPtr docObj;

	switch (drawTool) {
	case TOOL_LINE:
		docObj = LineCreate(window, modifier, mouseX, mouseY);
		break;
	case TOOL_HVLINE:
		docObj = LineCreate(window, (modifier | SHIFTKEYS), mouseX, mouseY);
		break;
	case TOOL_RECT:
		docObj = RectCreate(window, modifier, mouseX, mouseY);
		break;
	case TOOL_OVAL:
		docObj = OvalCreate(window, modifier, mouseX, mouseY);
		break;
	case TOOL_CURVE:
		docObj = CurveCreate(window, modifier, mouseX, mouseY);
		break;
	case TOOL_POLY:
		docObj = PolyCreate(window, modifier, mouseX, mouseY);
		break;
	case TOOL_TEXT:
		docObj = TextCreate(window, modifier, mouseX, mouseY);
		break;
	default:
		docObj = NULL;
		break;
	}
	return (docObj);
}

/*
 *	Create new object in front of all others
 *	Return FALSE if not enough memory
 */

static void DoCreate(WindowPtr window, UWORD modifier, WORD mouseX, WORD mouseY)
{
	DocObjPtr docObj;
	DocDataPtr docData = GetWRefCon(window);

	HiliteSelectOff(window);
	UnSelectAllObjects(docData);
/*
	Create new object
	Don't allow objects that are one pixel wide and tall
*/
	docObj = CreateObject(window, modifier, mouseX, mouseY);
	if (docObj == NULL)
		Error(ERR_NO_MEM);
	else {
		if (docObj->Frame.MinX == docObj->Frame.MaxX &&
			docObj->Frame.MinY == docObj->Frame.MaxY) {
			DetachObject(CurrLayer(docData), docObj);
			DisposeDocObject(docObj);
		}
		else {
			SelectObject(docObj);
			DocModified(docData);
			if (docObj->Type != TYPE_TEXT)		/* New text objs are empty */
				InvalObjectRect(window, docObj);
		}
	}
	HiliteSelectOn(window);
}

/*
 *	Handle mouse down
 */

void DoMouseDown(WindowPtr window, UWORD modifier, WORD mouseX, WORD mouseY,
				 ULONG seconds, ULONG micros)
{
	WORD left, top, right, bottom;
	BOOL textClick;
	DocDataPtr docData = GetWRefCon(window);
	Rectangle windRect, contRect;

	if (titleChanged)
		FixTitle();
/*
	Set double-click and triple-click flags
*/
	if (ABS(prevMouseX - mouseX) > 2 || ABS(prevMouseY - mouseY) > 2)
		tripleClick = doubleClick = FALSE;
	else if (tripleClick)
		tripleClick = doubleClick = FALSE;
	else if (doubleClick) {
		doubleClick = FALSE;
		tripleClick = DoubleClick(prevSecs, prevMicros, seconds, micros);
	}
	else
		doubleClick = DoubleClick(prevSecs, prevMicros, seconds, micros);
	prevSecs = seconds;
	prevMicros = micros;
	prevMouseX = mouseX;
	prevMouseY = mouseY;
/*
	If special window, handle it elsewhere
*/
	if (!IsDocWindow(window)) {
		if (window == toolWindow)
			DoToolWindow(mouseX, mouseY);
		else if (window == penWindow)
			DoPenWindow(mouseX, mouseY);
		else if (window == fillWindow)
			DoFillWindow(mouseX, mouseY);
		return;
	}
/*
	Check for mouse out of window bounds
*/
	GetWindowRect(window, &windRect);
	if (mouseX < windRect.MinX || mouseX > windRect.MaxX ||
		mouseY < windRect.MinY || mouseY > windRect.MaxY) {
		ErrBeep();
		return;
	}
/*
	Check to see if in upper left ruler corner
*/
	GetContentRect(window, &contRect);
	if (mouseX < contRect.MinX || mouseY < contRect.MinY) {
		if (mouseX >= windRect.MinX && mouseX < contRect.MinX &&
			mouseY >= windRect.MinY && mouseY < contRect.MinY)
			DoSetRuler(window, mouseX, mouseY);
		else
			ErrBeep();
		return;
	}
/*
	Check for mouse out of document bounds
*/
	DocToWindow(window, 0, 0, &left, &top);
	DocToWindow(window, docData->DocWidth, docData->DocHeight, &right, &bottom);
	right--;
	bottom--;
	if (mouseX < left || mouseY < top || mouseX > right || mouseY > bottom) {
		ErrBeep();
		return;
	}
/*
	Check for selecting objects
*/
	if (drawTool == TOOL_SELECT)
		DoSelect(window, modifier, mouseX, mouseY);
/*
	Check for clicking in text object with text tool
*/
	else if (drawTool == TOOL_TEXT)
		textClick = TextMouseClick(window, modifier, mouseX, mouseY);
/*
	Otherwise create new object
*/
	if (drawTool != TOOL_SELECT && (drawTool != TOOL_TEXT || !textClick))
		DoCreate(window, modifier, mouseX, mouseY);
	SetAllMenus();
	TextPurge(docData);
}

/*
 *	Reset multiple-click flags so next click will be treated like single click
 */

void ResetMultiClick()
{
	prevSecs = prevMicros = 0;
}
