/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Group handling routines
 */

#include <exec/types.h>
#include <graphics/gfxmacros.h>
#include <intuition/intuition.h>

#include <proto/graphics.h>

#include <Toolbox/Memory.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Utility.h>
#include <Toolbox/Window.h>

#include "Draw.h"
#include "Proto.h"

/*
 *	External variables
 */

/*
 *	Local variables and definitions
 */

#define LINE_PAT	0xFFFF		/* Line pattern for object creation */

static GroupObjPtr	growGroupObj;
static BOOL			wasMoved;
static WORD			growHandle;

/*
 *	Local prototypes
 */

void	SetGroupFrame(GroupObjPtr);

void	GroupTrackGrow(WindowPtr, WORD, WORD, WORD, WORD, BOOL);

/*
 *	Allocate a new group object
 */

GroupObjPtr GroupAllocate()
{
	return ((GroupObjPtr) MemAlloc(sizeof(GroupObj), MEMF_CLEAR));
}

/*
 *	Dispose of group object
 */

void GroupDispose(GroupObjPtr groupObj)
{
	DisposeAllDocObjects(groupObj->Objects);
	MemFree(groupObj, sizeof(GroupObj));
}

/*
 *	Draw group object
 */

void GroupDrawObj(RastPtr rPort, GroupObjPtr groupObj, RectPtr rect, RectPtr clipRect)
{
	DocObjPtr docObj;
	Rectangle objRect;

	for (docObj = groupObj->Objects; docObj; docObj = NextObj(docObj)) {
		objRect = docObj->Frame;
		OffsetRect(&objRect, groupObj->DocObj.Frame.MinX, groupObj->DocObj.Frame.MinY);
		MapRect(&objRect, &groupObj->DocObj.Frame, rect);
		DrawObject(rPort, docObj, &objRect, clipRect);
	}
}

/*
 *	Draw outline of group (for dragging)
 */

void GroupDrawOutline(WindowPtr window, GroupObjPtr groupObj, WORD xOffset, WORD yOffset)
{
	RectDrawOutline(window, (RectObjPtr) groupObj, xOffset, yOffset);
}

/*
 *	Draw selection hilighting for group
 */

void GroupHilite(WindowPtr window, GroupObjPtr groupObj)
{
	RectHilite(window, (RectObjPtr) groupObj);
}

/*
 *	Set group pen color
 */

void GroupSetPenColor(GroupObjPtr groupObj, RGBColor penColor)
{
	DocObjPtr docObj;

	for (docObj = groupObj->Objects; docObj; docObj = NextObj(docObj))
		SetObjectPenColor(docObj, penColor);
}

/*
 *	Set group pen size
 *	If size is -1 then do not change
 */

void GroupSetPenSize(GroupObjPtr groupObj, WORD penWidth, WORD penHeight)
{
	DocObjPtr docObj;

	for (docObj = groupObj->Objects; docObj; docObj = NextObj(docObj))
		SetObjectPenSize(docObj, penWidth, penHeight);
}

/*
 *	Set group fill pattern
 */

void GroupSetFillPat(GroupObjPtr groupObj, RGBPat8Ptr fillPat)
{
	DocObjPtr docObj;

	for (docObj = groupObj->Objects; docObj; docObj = NextObj(docObj))
		SetObjectFillPat(docObj, fillPat);
}

/*
 *	Set text parameters for group text
 */

void GroupSetTextParams(GroupObjPtr groupObj, FontNum fontNum, FontSize fontSize,
						WORD style, WORD miscStyle, WORD justify, WORD spacing)
{
	DocObjPtr docObj;

	for (docObj = groupObj->Objects; docObj; docObj = NextObj(docObj))
		SetObjectTextParams(docObj, fontNum, fontSize, style, miscStyle, justify, spacing);
	GroupAdjustFrame(groupObj);
}

/*
 *	Set group frame from object frames, and offset object frames
 */

static void SetGroupFrame(GroupObjPtr groupObj)
{
	DocObjPtr docObj;
	Rectangle rect;

	for (docObj = groupObj->Objects; docObj; docObj = NextObj(docObj)) {
		if (docObj == groupObj->Objects)
			rect = docObj->Frame;
		else
			UnionRect(&docObj->Frame, &rect, &rect);
	}
	groupObj->DocObj.Frame = rect;
	for (docObj = groupObj->Objects; docObj; docObj = NextObj(docObj))
		OffsetRect(&docObj->Frame, (WORD) -rect.MinX, (WORD) -rect.MinY);
}

/*
 *	Rotate group by given angle about center
 */

void GroupRotate(GroupObjPtr groupObj, WORD cx, WORD cy, WORD angle)
{
	DocObjPtr docObj;

	for (docObj = groupObj->Objects; docObj; docObj = NextObj(docObj)) {
		OffsetRect(&docObj->Frame, groupObj->DocObj.Frame.MinX, groupObj->DocObj.Frame.MinY);
		RotateObject(docObj, cx, cy, angle);
	}
	SetGroupFrame(groupObj);
}

/*
 *	Flip group horizontally or vertically about center
 */

void GroupFlip(GroupObjPtr groupObj, WORD cx, WORD cy, BOOL horiz)
{
	DocObjPtr docObj;

	for (docObj = groupObj->Objects; docObj; docObj = NextObj(docObj)) {
		OffsetRect(&docObj->Frame, groupObj->DocObj.Frame.MinX, groupObj->DocObj.Frame.MinY);
		FlipObject(docObj, cx, cy, horiz);
	}
	SetGroupFrame(groupObj);
}

/*
 *	Scale group to new frame
 *	Scale group objects proportionally
 */

void GroupScale(GroupObjPtr groupObj, RectPtr frame)
{
	DocObjPtr docObj;
	Rectangle rect;

	for (docObj = groupObj->Objects; docObj; docObj = NextObj(docObj)) {
		OffsetRect(&docObj->Frame, groupObj->DocObj.Frame.MinX, groupObj->DocObj.Frame.MinY);
		rect = docObj->Frame;
		MapRect(&rect, &groupObj->DocObj.Frame, frame);
		ScaleObject(docObj, &rect);
		OffsetRect(&docObj->Frame, (WORD) -frame->MinX, (WORD) -frame->MinY);
	}
	groupObj->DocObj.Frame = *frame;
	GroupAdjustFrame(groupObj);
}

/*
 *	Determine if point is in group
 *	Point is relative to object rectangle
 */

BOOL GroupSelect(GroupObjPtr groupObj, Point *pt)
{
	DocObjPtr docObj;

	for (docObj = groupObj->Objects; docObj; docObj = NextObj(docObj)) {
		if (PointInObject(docObj, pt))
			return (TRUE);
	}
	return (FALSE);
}

/*
 *	Return handle number of given point, or -1 if not in a handle
 *	Point is relative to object rectangle
 */

WORD GroupHandle(GroupObjPtr groupObj, PointPtr pt, RectPtr handleRect)
{
	return (RectHandle((RectObjPtr) groupObj, pt, handleRect));
}

/*
 *	Duplicate group data to new object
 *	Return success status
 */

BOOL GroupDupData(GroupObjPtr groupObj, GroupObjPtr newObj)
{
	DocObjPtr docObj, newSubObj;

	for (docObj = groupObj->Objects; docObj; docObj = NextObj(docObj)) {
		if ((newSubObj = DuplicateObject(NULL, docObj)) == NULL)
			return (FALSE);
		AppendToGroup(newObj, newSubObj);
	}
	return (TRUE);
}

/*
 *	Adjust group frame
 */

void GroupAdjustFrame(GroupObjPtr groupObj)
{
	DocObjPtr docObj;

	for (docObj = groupObj->Objects; docObj; docObj = NextObj(docObj))
		OffsetRect(&docObj->Frame, groupObj->DocObj.Frame.MinX, groupObj->DocObj.Frame.MinY);
	SetGroupFrame(groupObj);
}

/*
 *	Make a group out of currently selected objects
 *	Group is linked in at location of front-most object in group
 */

GroupObjPtr MakeGroup(DocDataPtr docData)
{
	DocObjPtr docObj, nextObj, linkObj;
	GroupObjPtr groupObj;

/*
	Make sure there are at least two objects selected
*/
	if ((docObj = FirstSelected(docData)) == NULL || NextSelected(docObj) == NULL)
		return (NULL);
/*
	Allocate group object
*/
	if ((groupObj = (GroupObjPtr) NewDocObject(CurrLayer(docData), TYPE_GROUP)) == NULL)
		return (NULL);
/*
	First remove all selected objects from document and add them to group list
*/
	docObj = FirstObject(docData);
	while (docObj) {
		nextObj = NextObj(docObj);
		if (ObjectSelected(docObj)) {
			linkObj = PrevObj(docObj);
			DetachObject(CurrLayer(docData), docObj);
			AppendToGroup(groupObj, docObj);
		}
		docObj = nextObj;
	}
	SetGroupFrame(groupObj);
/*
	Now link group into document at position of last grouped object
*/
	for (docObj = groupObj->Objects; docObj; docObj = NextObj(docObj)) {
		UnSelectObject(docObj);
		docObj->Group = groupObj;
	}
	DetachObject(CurrLayer(docData), (DocObjPtr) groupObj);
	InsertObject(CurrLayer(docData), linkObj, (DocObjPtr) groupObj);
	SelectObject((DocObjPtr) groupObj);
	return (groupObj);
}

/*
 *	Un-make specified group
 */

void UnMakeGroup(DocDataPtr docData, GroupObjPtr groupObj)
{
	DocObjPtr docObj;

/*
	Adjust object frames and re-link into document
*/
	for (docObj = groupObj->Objects; docObj; docObj = NextObj(docObj)) {
		OffsetObject(docObj, groupObj->DocObj.Frame.MinX, groupObj->DocObj.Frame.MinY);
		SelectObject(docObj);
	}
	InsertAllObjects(CurrLayer(docData), (DocObjPtr) groupObj, groupObj->Objects);
	groupObj->Objects = NULL;
/*
	Dispose of group object
*/
	DetachObject(CurrLayer(docData), (DocObjPtr) groupObj);
	DisposeDocObject((DocObjPtr) groupObj);
}

/*
 *	Draw routine for tracking group grow
 */

static void GroupTrackGrow(WindowPtr window, WORD xStart, WORD yStart, WORD xEnd, WORD yEnd,
						   BOOL change)
{
	GroupObj groupObj;
	Rectangle rect;

	if (change && (xStart != xEnd || yStart != yEnd || wasMoved)) {
		BlockClear(&groupObj, sizeof(GroupObj));
		rect = growGroupObj->DocObj.Frame;
		if (growHandle == 0 || growHandle == 2)
			rect.MinX = xEnd;
		else
			rect.MaxX = xEnd;
		if (growHandle == 0 || growHandle == 1)
			rect.MinY = yEnd;
		else
			rect.MaxY = yEnd;
		SetFrameRect(&groupObj.DocObj.Frame, rect.MinX, rect.MinY, rect.MaxX, rect.MaxY);
		GroupDrawOutline(window, &groupObj, 0, 0);
		wasMoved = TRUE;
	}
}

/*
 *	Track mouse and change group shape
 */

void GroupGrow(WindowPtr window, GroupObjPtr groupObj, UWORD modifier, WORD mouseX, WORD mouseY)
{
	WORD xStart, yStart, xEnd, yEnd;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
	Point pt;
	Rectangle rect, frame, handleRect;

	growGroupObj = groupObj;
	GetHandleRect(window, &handleRect);
/*
	Track handle movement
*/
	wasMoved = FALSE;
	WindowToDoc(window, mouseX, mouseY, &xStart, &yStart);
	pt.x = xStart - groupObj->DocObj.Frame.MinX;
	pt.y = yStart - groupObj->DocObj.Frame.MinY;
	if ((growHandle = GroupHandle(groupObj, &pt, &handleRect)) == -1)
		return;
	SnapToGrid(docData, &xStart, &yStart);
	xEnd = xStart;
	yEnd = yStart;
	TrackMouse(window, modifier, &xEnd, &yEnd, GroupTrackGrow, NULL);
/*
	Set new object dimensions
*/
	if (xEnd != xStart || yEnd != yStart) {
		SetStdPointer(window, POINTER_WAIT);	/* Pointer will be reset by caller */
		InvalObjectRect(window, (DocObjPtr) groupObj);
		HiliteSelectOff(window);
		rect = groupObj->DocObj.Frame;
		if (growHandle == 0 || growHandle == 2)
			rect.MinX = xEnd;
		else
			rect.MaxX = xEnd;
		if (growHandle == 0 || growHandle == 1)
			rect.MinY = yEnd;
		else
			rect.MaxY = yEnd;
		SetFrameRect(&frame, rect.MinX, rect.MinY, rect.MaxX, rect.MaxY);
		GroupScale(groupObj, &frame);
		HiliteSelectOn(window);
		InvalObjectRect(window, (DocObjPtr) groupObj);
	}
}
