/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Text strings
 */

#include <exec/types.h>

#include <Toolbox/TypeDefs.h>

#include "Version.h"

TextPtr initErrors[] = {
	"Bad system state.",
	"Can't find icon.library.",
	"Can't find diskfont.library.",
	"Can't open screen.",
	"Not enough memory."
};

TextPtr strsErrors[] = {
	" Unknown internal error.",
	" Not enough memory.",
	" Unable to open file.",
	" Unable to save file.",
	" Not enough memory - saving as 'DesignWorks.Recover' on drive 0.",
	" Unable to print.",
	" Improper page size.",
	" Improper page number.",
	" Improper number of copies.",
	" No such page.",
	" Unable to get font.",
	" AREXX not present.",
	" Improper macro name.",
	" Macro execution failed.",
	" Object is locked.",
	" Improper pen size.",
	" Improper scale value.",
	" Unable to import picture.",
	" There is no visible layer.",
	" Cannot delete final layer."
};

TextChar strDemo[]		= " Demo version.";
TextChar strBadFile[]	= " Bad file contents.";
TextChar strMemory[]	= " Getting low on memory -- please save your work.";

TextChar strDOSError[]		= " DOS error: 000.";
TextChar strFileNotFound[]	= " File not found.";
TextChar strDiskLocked[]	= " Disk is locked.";
TextChar strFileNoDelete[]	= " File is delete protected.";
TextChar strFileNoRead[]	= " File is read protected.";

TextChar strUntitled[]		= "Untitled";
TextChar strOpenFile[]		= "Open document";
TextChar strSaveAs[]		= "Save document as:";
TextChar strImportPict[]	= "Import picture";
TextChar strExportPict[]	= "Export picture as:";

TextChar strRAMName[]		= "RAM:";

TextChar strProgName[] 		= "DesignWorks";
TextChar strScreenName[] 	= "DesignWorks";
TextChar strPrefsName[]		= "DesignWorks Defaults";
TextChar strAutoExecName[]	= "DesignWorks Startup";
TextChar strAppIconName[]	= "DesignWorks Deposit";
TextChar strDefaultDir[]	= "DesignWorks:";
TextChar strEmergencySave[]	= "DesignWorks.Recover";

TextChar strSampleText[]	= "The quick brown fox jumps over the lazy dog.";

TextChar strPenHeight[]	= "Pen height:";	/* For "Other size" dialogs */
TextChar strPenWidth[]	= "Pen width:";

TextChar strCancel[]	= "Cancel";

TextChar strLayer[]		= "Layer ";
TextChar strToolTitle[]	= "Toolbox";
TextChar strPenTitle[]	= "Pen";
TextChar strFillTitle[]	= "Fill";

TextChar strNone[]	= "None";

TextChar strInch[]	= " in.";
TextChar strCM[]	= " cm";
TextChar strMM[]	= " mm";

TextChar strPrtSetup[]	= "Setting up...";		/* For "Cancel print" dialog */
TextChar strPrtRender[]	= "Rendering...";
TextChar strPrtPrint[]	= "Printing...";
TextChar strPrtCancel[]	= "Cancelling...";

TextChar strPenColor[]		= "Select a pen color";
TextChar strScreenColor[]	= "Select a screen color";
