/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Pen menu routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <proto/exec.h>
#include <proto/intuition.h>

#include <string.h>

#include <Toolbox/Window.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Utility.h>

#include "Draw.h"
#include "Proto.h"

/*
 *	External variables
 */

extern MsgPortPtr	mainMsgPort;
extern ScreenPtr	screen;

extern Defaults	defaults;

extern UBYTE	penSizes[];

extern TextChar	strPenHeight[], strPenWidth[];

extern DlgTemplPtr	dlgList[];

/*
 *	Local variables and definitions
 */

enum {
	PENSIZE_TEXT = 2,
	PENSIZE_PROMPT
};

/*
 *	Local prototypes
 */

void	SetPenSizeText(DialogPtr, WORD);
BOOL	DoPenSize(WindowPtr, WORD, WORD);
BOOL	DoArrows(WindowPtr, WORD, WORD);

/*
 *	Set defaults for pen menu from current selection
 *	Note: we only modify the pen menu, not pen colors or fill patterns
 *		when a selection is made
 */

void SetPenMenuDefaults(WindowPtr window)
{
	BOOL gotSize, gotFlags;
	WORD penWidth, penHeight;
	DocObjPtr docObj;
	DocDataPtr docData = GetWRefCon(window);

	gotSize = gotFlags = FALSE;
	for (docObj = FirstSelected(docData); docObj; docObj = NextSelected(docObj)) {
		if (!gotSize && GetObjectPenSize(docObj, &penWidth, &penHeight)) {
			defaults.PenWidth  = penWidth;
			defaults.PenHeight = penHeight;
			gotSize = TRUE;
		}
		if (!gotFlags && docObj->Type == TYPE_LINE) {
			defaults.LineFlags = ((LineObjPtr) docObj)->LineFlags;
			gotFlags = TRUE;
		}
		if (gotSize && gotFlags)
			break;
	}
	SetPenMenu();
}

/*
 *	Set pen size text
 */

static void SetPenSizeText(DialogPtr dlg, WORD size)
{
	TextChar sizeText[GADG_MAX_STRING];

	NumToString(size, sizeText);
	SetEditItemText(dlg, NULL, PENSIZE_TEXT, sizeText);
}

/*
 *	Set pen size
 *	If size number is -1 then do not change,
 *	If size number >= NUM_PENSIZES then get value from user.
 */

static BOOL DoPenSize(WindowPtr window, WORD widthNum, WORD heightNum)
{
	BOOL changed, done;
	WORD item, size, penWidth, penHeight;
	TextPtr prompt;
	DocObjPtr docObj;
	DocDataPtr docData = GetWRefCon(window);
	DialogPtr dlg;
	GadgetPtr gadgList, gadget;
	TextChar sizeText[GADG_MAX_STRING];

/*
	If size number is "Other", get value from user
*/
	if (widthNum >= NUM_PENSIZES || heightNum >= NUM_PENSIZES) {
		BeginWait();
		if ((dlg = GetDialog(dlgList[DLG_OTHERPEN], screen, mainMsgPort)) == NULL) {
			EndWait();
			Error(ERR_NO_MEM);
			return (FALSE);
		}
		gadgList = dlg->FirstGadget;
		prompt = (widthNum >= NUM_PENSIZES) ? strPenWidth : strPenHeight;
		SetGadgetItemText(gadgList, PENSIZE_PROMPT, dlg, NULL, prompt);
		gadget = GadgetItem(gadgList, PENSIZE_TEXT);
		gadget->Activation |= LONGINT;
		OutlineOKButton(dlg);
		size = (widthNum >= NUM_PENSIZES) ? defaults.PenWidth : defaults.PenHeight;
		SetPenSizeText(dlg, size);
		done = FALSE;
		do {
			WaitPort(mainMsgPort);
			item = CheckDialog(mainMsgPort, dlg, DialogFilter);
			GetEditItemText(gadgList, PENSIZE_TEXT, sizeText);
			switch(item) {
			case -1:					/* INTUITICKS message */
				EnableGadgetItem(gadgList, OK_BUTTON, dlg, NULL, (strlen(sizeText) != 0));
				break;
			case CANCEL_BUTTON:
				done = TRUE;
				break;
			case OK_BUTTON:
				size = StringToNum(sizeText);
				if (CheckNumber(sizeText) && size > 0 && size <= MAX_PENSIZE) {
					done = TRUE;
					break;
				}
				Error(ERR_PEN_SIZE);
				if (size <= 0)
					size = 1;
				else if (size > MAX_PENSIZE)
					size = MAX_PENSIZE;
				SetPenSizeText(dlg, size);
				gadget = GadgetItem(gadgList, PENSIZE_TEXT);
				ActivateGadget(gadget, dlg, NULL);
				break;
			}
		} while (!done);
		DisposeDialog(dlg);
		EndWait();
		if (item == CANCEL_BUTTON)
			return (FALSE);
	}
/*
	Set pen sizes
*/
	if (widthNum >= NUM_PENSIZES)
		penWidth = size;
	else if (widthNum >= 0)
		penWidth = penSizes[widthNum];
	else
		penWidth = -1;
	if (heightNum >= NUM_PENSIZES)
		penHeight = size;
	else if (heightNum >= 0)
		penHeight = penSizes[heightNum];
	else
		penHeight = -1;
/*
	Set defaults for new objects
*/
	if (penWidth != -1)
		defaults.PenWidth = penWidth;
	if (penHeight != -1)
		defaults.PenHeight = penHeight;
	SetPenMenu();
/*
	Modify selected objects
*/
	if (ObjectsLocked(docData)) {
		Error(ERR_OBJ_LOCKED);
		return (FALSE);
	}
	for (docObj = FirstSelected(docData); docObj; docObj = NextSelected(docObj)) {
		InvalObjectRect(window, docObj);	/* Obj rect size may change */
		SetObjectPenSize(docObj, penWidth, penHeight);
		InvalObjectRect(window, docObj);
		changed = TRUE;
	}
	if (changed)
		DocModified(docData);
	return (TRUE);
}

/*
 *	Set pen arrows
 *	1 = on, 0 = off, -1 = don't change
 */

static BOOL DoArrows(WindowPtr window, WORD atStart, WORD atEnd)
{
	BOOL changed;
	DocObjPtr docObj;
	LineObj lineObj;
	DocDataPtr docData = GetWRefCon(window);

/*
	Set defaults for new lines
*/
	lineObj.LineFlags = defaults.LineFlags;
	LineSetArrows(&lineObj, atStart, atEnd);
	defaults.LineFlags = lineObj.LineFlags;
	SetPenMenu();
/*
	Modify selected objects
*/
	if (ObjectsLocked(docData)) {
		Error(ERR_OBJ_LOCKED);
		return (FALSE);
	}
	for (docObj = FirstSelected(docData); docObj; docObj = NextSelected(docObj)) {
		if (docObj->Type == TYPE_LINE) {
			InvalObjectRect(window, docObj);	/* Obj rect may change size */
			LineSetArrows((LineObjPtr) docObj, atStart, atEnd);
			InvalObjectRect(window, docObj);
			changed = TRUE;
		}
	}
	if (changed);
		DocModified(docData);
	return (TRUE);
}

/*
 *	Handle Pen menu
 */

BOOL DoPenMenu(WindowPtr window, UWORD item, UWORD sub, UWORD modifier)
{
	BOOL success;

	if (!IsDocWindow(window))
		return (FALSE);
	UndoOff();
	success = FALSE;
	switch (item) {
	case WIDTH_ITEM:
		success = DoPenSize(window, sub, -1);
		break;
	case HEIGHT_ITEM:
		success = DoPenSize(window, -1, sub);
		break;
	case NOARROWS_ITEM:
		success = DoArrows(window, 0, 0);
		break;
	case ARROWSTART_ITEM:
		success = DoArrows(window, 1, -1);
		break;
	case ARROWEND_ITEM:
		success = DoArrows(window, -1, 1);
		break;
	}
	return (success);
}
