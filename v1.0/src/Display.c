/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Display routines
 */

#include <exec/types.h>
#include <graphics/gfxmacros.h>
#include <intuition/intuition.h>

#include <proto/graphics.h>

#include <Toolbox/Window.h>

#include "Draw.h"
#include "Proto.h"

/*
 *	External variables
 */

extern WORD	xAspectShift, yAspectShift;

/*
 *	Local prototypes
 */

void	DrawPageBoundary(WindowPtr);
void	DrawGridLines(WindowPtr);
void	DrawPageBreaks(WindowPtr);

/*
 *	Scale from window to doc values
 */

void ScaleWinToDoc(Fixed scale, WORD winX, WORD winY, WORD *docX, WORD *docY)
{
	winX <<= xAspectShift;
	winY <<= yAspectShift;
	if (scale != SCALE_FULL) {
		*docX = ((LONG) winX*SCALE_FULL)/scale;
		*docY = ((LONG) winY*SCALE_FULL)/scale;
	}
	else {
		*docX = winX;
		*docY = winY;
	}
}

/*
 *	Scale from doc to window values
 */

void ScaleDocToWin(Fixed scale, WORD docX, WORD docY, WORD *winX, WORD *winY)
{
	if (scale != SCALE_FULL) {
		*winX = ((LONG) docX*scale)/SCALE_FULL;
		*winY = ((LONG) docY*scale)/SCALE_FULL;
	}
	else {
		*winX = docX;
		*winY = docY;
	}
	*winX >>= xAspectShift;
	*winY >>= yAspectShift;
}

/*
 *	Convert window coordinates to document coordinates
 */

void WindowToDoc(WindowPtr window, WORD winX, WORD winY, WORD *docX, WORD *docY)
{
	WORD x, y;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);

	x = winX - window->BorderLeft - RulerWidth(window);
	y = winY - window->BorderTop - RulerHeight(window);
	ScaleWinToDoc(docData->Scale, x, y, docX, docY);
	*docX += docData->LeftOffset;
	*docY += docData->TopOffset;
}

/*
 *	Convert document coordinates to window coordinates
 */

void DocToWindow(WindowPtr window, WORD docX, WORD docY, WORD *winX, WORD *winY)
{
	WORD x, y;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);

	x = docX - docData->LeftOffset;
	y = docY - docData->TopOffset;
	ScaleDocToWin(docData->Scale, x, y, winX, winY);
	*winX += window->BorderLeft + RulerWidth(window);
	*winY += window->BorderTop + RulerHeight(window);
}

/*
 *	Convert window point to document point
 */

void WindowToDocPoint(WindowPtr window, PointPtr winPt, PointPtr docPt)
{
	WindowToDoc(window, winPt->x, winPt->y, &docPt->x, &docPt->y);
}

/*
 *	Convert document point to window point
 */

void DocToWindowPoint(WindowPtr window, PointPtr docPt, PointPtr winPt)
{
	DocToWindow(window, docPt->x, docPt->y, &winPt->x, &winPt->y);
}

/*
 *	Convert window rectangle to document rectangle
 *	Will not return with empty rectangle
 */

void WindowToDocRect(WindowPtr window, RectPtr winRect, register RectPtr docRect)
{
	WORD maxX, maxY;

	WindowToDoc(window, winRect->MinX, winRect->MinY, &docRect->MinX, &docRect->MinY);
	WindowToDoc(window, winRect->MaxX + 1, winRect->MaxY + 1, &maxX, &maxY);
	docRect->MaxX = (maxX > docRect->MinX) ? maxX - 1 : docRect->MinX;
	docRect->MaxY = (maxY > docRect->MinY) ? maxY - 1 : docRect->MinY;
}

/*
 *	Convert document rectangle to window rectangle
 *	Will not return with empty rectangle
 */

void DocToWindowRect(WindowPtr window, RectPtr docRect, register RectPtr winRect)
{
	WORD maxX, maxY;

	DocToWindow(window, docRect->MinX, docRect->MinY, &winRect->MinX, &winRect->MinY);
	DocToWindow(window, docRect->MaxX + 1, docRect->MaxY + 1, &maxX, &maxY);
	winRect->MaxX = (maxX > winRect->MinX) ? maxX - 1 : winRect->MinX;
	winRect->MaxY = (maxY > winRect->MinY) ? maxY - 1 : winRect->MinY;
}

/*
 *	Draw page boundary and clear page contents
 */

static void DrawPageBoundary(WindowPtr window)
{
	WORD right, bottom;
	RastPtr rPort = window->RPort;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
	Rectangle rect;

	GetWindowRect(window, &rect);
/*
	Get position of edge of last page
*/
	DocToWindow(window, docData->DocWidth, docData->DocHeight, &right, &bottom);
	right--;
	bottom--;
/*
	First fill pages with white
*/
	PenNormal(rPort);
	RGBForeColor(rPort, RGBCOLOR_WHITE);
	if (right >= rect.MinX && bottom >= rect.MinY)
		RectFill(rPort, rect.MinX, rect.MinY, right, bottom);
/*
	Next fill outside of pages with blue
*/
	RGBForeColor(rPort, RGBCOLOR_BLUE);
	if (right + 2 <= rect.MaxX)
		RectFill(rPort, right + 2, rect.MinY, rect.MaxX, rect.MaxY);
	if (bottom + 2 <= rect.MaxY)
		RectFill(rPort, rect.MinX, bottom + 2, rect.MaxX, rect.MaxY);
/*
	Finally, draw page edges
*/
	RGBForeColor(rPort, RGBCOLOR_BLACK);
	if (right + 1 <= rect.MaxX) {
		Move(rPort, right + 1, rect.MinY);
		Draw(rPort, right + 1, bottom + 1);
	}
	if (bottom + 1 <= rect.MaxY) {
		Move(rPort, rect.MinX, bottom + 1);
		Draw(rPort, right + 1, bottom + 1);
	}
}

/*
 *	Draw grid lines, if turned on
 */

static void DrawGridLines(WindowPtr window)
{
	WORD i, x, y, left, top, right, bottom;
	RastPtr rPort = window->RPort;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
	Rectangle rect;

	if ((docData->Flags & DOC_SHOWGRID) == 0)
		return;
	GetWindowRect(window, &rect);
	DocToWindow(window, 0, 0, &left, &top);
	DocToWindow(window, docData->DocWidth, docData->DocHeight, &right, &bottom);
	right--;
	bottom--;
/*
	Set up for draw
*/
	PenNormal(rPort);
	SetDrMd(rPort, COMPLEMENT);
	SetDrPt(rPort, 0x8080);
/*
	Draw grid lines
	Always draw from top and left, so when scrolled the patterns will align
*/
	for (i = 0; ; i++) {
		GridDrawLoc(docData, i, &x, &y);
		DocToWindow(window, x, y, &x, &y);
		if (x > right && y > bottom)
			break;
		if (x >= rect.MinX && x <= right) {
			Move(rPort, x, top);
			Draw(rPort, x, bottom);
		}
		if (y >= rect.MinY && y <= bottom) {
			Move(rPort, left, y);
			Draw(rPort, right, y);
		}
	}
}

/*
 *	Draw page breaks, if turned on
 */

static void DrawPageBreaks(WindowPtr window)
{
	WORD x, y, left, top, right, bottom, width, height;
	RastPtr rPort = window->RPort;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
	Rectangle rect;

	if ((docData->Flags & DOC_SHOWPAGE) == 0)
		return;
	GetWindowRect(window, &rect);
	DocToWindow(window, 0, 0, &left, &top);
	DocToWindow(window, docData->DocWidth, docData->DocHeight, &right, &bottom);
	ScaleDocToWin(docData->Scale, docData->PageWidth, docData->PageHeight, &width, &height);
	right--;
	bottom--;
	PenNormal(rPort);
	SetDrMd(rPort, COMPLEMENT);
	SetDrPt(rPort, 0xFF00);
/*
	Draw vertical page breaks
	Always draw from top, so when scrolled the patterns will align
*/
	for (x = left; x <= right; x += width) {
		if (x >= rect.MinX) {
			Move(rPort, x, top);
			Draw(rPort, x, bottom);
		}
	}
/*
	Draw horizontal page breaks
	Always draw from left, so when scrolled the patterns will align
*/
	for (y = top; y <= bottom; y += height) {
		if (y >= rect.MinY) {
			Move(rPort, left, y);
			Draw(rPort, right, y);
		}
	}
}

/*
 *	Draw document
 *	Only need to draw objects that have parts in given rect
 *	If rect is NULL, then draw entire window contents
 */

void DrawDocument(WindowPtr window, RectPtr rect)
{
	RastPtr rPort = window->RPort;
	DocObjPtr docObj;
	DocLayerPtr docLayer, currLayer;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
	Rectangle clipRect, objRect;

	BNDRYOFF(rPort);
	DrawPageBoundary(window);
	if (rect)
		clipRect = *rect;
	else
		GetContentRect(window, &clipRect);
	currLayer = CurrLayer(docData);
	docLayer = (docData->Flags & DOC_HIDEBACKLAY) ? currLayer : BottomLayer(docData);
	for (;;) {
		if (docLayer == currLayer || (docLayer->Flags & LAYER_VISIBLE) != 0) {
			for (docObj = BottomObject(docLayer); docObj; docObj = NextObj(docObj)) {
				DocToWindowRect(window, &docObj->Frame, &objRect);
				DrawObject(window->RPort, docObj, &objRect, &clipRect);
			}
		}
		if (docLayer == currLayer)
			break;
		docLayer = NextLayer(docLayer);
	}
	DrawGridLines(window);
	DrawPageBreaks(window);
	HiliteSelect(window);
}
