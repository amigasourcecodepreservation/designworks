/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991-93 New Horizons Software, Inc.
 *
 *	Initialization
 */

#include <exec/types.h>
#include <exec/execbase.h>
#include <intuition/intuition.h>
#include <workbench/startup.h>
#include <libraries/dos.h>
#include <libraries/dosextens.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include <string.h>
#include <stdlib.h>				// For exit() prototype

#include <Toolbox/Globals.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Menu.h>
#include <Toolbox/Image.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Window.h>
#include <Toolbox/Screen.h>
#include <Toolbox/Utility.h>
#include <Toolbox/Font.h>
#include <Toolbox/DOS.h>
#include <Toolbox/StdFile.h>
#include <Toolbox/StdInit.h>
#include <Toolbox/SerialInfo.h>

#include "Draw.h"
#include "Proto.h"

/*
 *	External variables
 */

extern struct IFFParseBase	*IFFParseBase;
extern struct Library		*IntuitionBase;

extern Options	options;

extern TextChar	strProgName[], progPathName[];

extern BOOL	fromCLI;

extern WORD	intuiVersion;

extern struct WBStartup	*WBenchMsg;

extern MenuTemplate		docWindMenus[], altWindMenus[];

extern MenuPtr		docMenuStrip;
extern ScreenPtr	screen;
extern WindowPtr	backWindow;
extern MsgPortPtr	mainMsgPort;
extern MsgPort		monitorMsgPort;

extern WORD	xAspectShift, yAspectShift;

extern Palette		screenPalette;

extern GadgetTemplate	windowGadgets[];

extern TextFontPtr	smallFont;

extern TextPtr	initErrors[];
extern TextChar	strCancel[];

extern TextChar	strScreenTitle[], strScreenName[];

extern DlgTemplPtr	dlgList[];

extern RGBColor	colors[];
extern TextPtr	colorNames[];

/*
 *	Local variables and definitions
 */

enum {
	NAME_STATTEXT = 0,
	COMPANY_STATTEXT,
	SERIAL_STATTEXT
};

#define MAX_FILENAME_LEN	31

static TaskPtr	monitorTask;

static struct NewScreen newScreen = {
	0, 0, 640, STDSCREENHEIGHT, 4, 0, 1,
	HIRES | LACE, CUSTOMSCREEN,
	NULL, &strScreenTitle[0], NULL, NULL
};

static TextAttr smallAttr = {
	NULL, 8, FS_NORMAL, FPF_ROMFONT | FPF_DISKFONT | FPF_DESIGNED
};

static TextChar topazFontName[] = "topaz.font";
static TextChar	systemFontName[] = "System.font";
static TextChar	systemThinFontName[] = "SystemThin.font";

/*
 *	Local prototypes
 */

static void	BuildProgPathName(TextPtr, Dir);
static void	SetAspectAdjust(ScreenPtr, WORD *, WORD *);

static BOOL	IsPubScreen(ScreenPtr);
static void	WaitVisitor(void);

/*
 *	Build path name to program from given parameters
 *	progPathName[] is 100 bytes in size
 *	Note: Cannot use ProgDir: in system 2.0, since this path & name is used for
 *		document icon default tools
 */

static void BuildProgPathName(TextPtr fileName, Dir dirLock)
{
	register TextPtr		newName;
	register WORD			nameLen, newLen;
	register Dir			dir, parentDir;
	struct FileInfoBlock	*fib;

	strcpy(progPathName, fileName);
	if ((fib = MemAlloc(sizeof(struct FileInfoBlock), 0)) == NULL)
		return;
	newName = fib->fib_FileName;
	dir = DupLock(dirLock);
	while (dir) {
		if (!Examine(dir, fib))
			break;
		nameLen = strlen(progPathName);
		newLen = strlen(newName);
		if (nameLen + newLen > 98)
			break;
		BlockMove(progPathName, progPathName + newLen + 1, nameLen + 1);
		BlockMove(newName, progPathName, newLen);
		parentDir = ParentDir(dir);
		progPathName[newLen] = (parentDir) ? '/' : ':';
		UnLock(dir);
		dir = parentDir;
	}
	if (dir)
		UnLock(dir);
	MemFree(fib, sizeof(struct FileInfoBlock));
}

/*
 *	Set aspect adjustments for screen aspect
 */

static void SetAspectAdjust(ScreenPtr screen, WORD *xShift, WORD *yShift)
{
	ULONG	modeID;

	*xShift = ((screen->ViewPort.Modes & HIRES) == 0) ? 1 : 0;
	*yShift = ((screen->ViewPort.Modes & LACE) == 0)  ? 1 : 0;
	if (intuiVersion >= OSVERSION_2_0) {
		modeID = GetVPModeID(&screen->ViewPort);
		switch (modeID) {
		case SUPERLACE_KEY:
			*xShift = 0;
			*yShift = 1;
			break;
		}
	}
}

/*
 *	Initialization routine
 *	Open necessary libraries and devices, and open background window
 */

void Init(int argc, char *argv[])
{
	WORD			i, barHeight, arrowWidth, arrowHeight, leftEdge;
	LONG			dlgStartSecs, currSecs, dummy;
	Dir				dir;
	TextPtr			progName, userName, companyName, serialNum;
	ProcessPtr		process;
	GadgetPtr		gadgList;
	DialogPtr		initDlg;
	MenuTemplPtr	menuTempl;
	struct ExecBase	*execBase;

/*
	Do standard initialization first
*/
	if (!StdInit(OSVERSION_1_2)) {
		ShutDown();
		exit(RETURN_FAIL);
	}
	intuiVersion = LibraryVersion(IntuitionBase);
/*
	Get misc info
*/
	fromCLI = (argc > 0);
	process = (ProcessPtr) FindTask(NULL);
/*
	Build path name to program
	(Used to find program icon, prefs file and dictionary)
*/
	progName = (fromCLI) ? argv[0] : WBenchMsg->sm_ArgList->wa_Name;
	dir = ConvertFileName(progName);
	BuildProgPathName(progName, dir);
	UnLock(dir);
/*
	Open screen (or get pointer to public screen) and set colors
*/
	execBase = *((struct ExecBase **) 4L);
	newScreen.Depth = (execBase->AttnFlags & AFF_68020) ? 4 : 3;
	if ((screen = GetScreen(argc, argv, &newScreen, NULL, 0, strScreenName)) == NULL) {
		InitError(initErrors[INIT_NOSCREEN]);
		ShutDown();
		exit(RETURN_FAIL);
	}
	InitToolbox(screen);
	SetAspectAdjust(screen, &xAspectShift, &yAspectShift);
	IFFParseBase = (struct IFFParseBase *)
				OpenLibrary("iffparse.library",OSVERSION_2_0_4);
/*
	Get 8 pixel high font (used in ruler and page indicator)
*/
	smallAttr.ta_Name  = systemFontName;
	smallAttr.ta_YSize = 8;
	smallFont = GetFont(&smallAttr);
	if (smallFont == NULL) {
		smallAttr.ta_Name  = topazFontName;
		smallFont = GetFont(&smallAttr);
	}
/*
	Adjust parameters for document windows and gadgets
*/
	barHeight = screen->WBorTop + screen->Font->ta_YSize + 1;
	arrowWidth  = ARROW_WIDTH;
	arrowHeight = ARROW_HEIGHT;
	leftEdge	= 3;
	if (intuiVersion < OSVERSION_2_0) {
		arrowWidth  -= 2;
		arrowHeight -= 1;
		leftEdge--;
	}

	for (i = UP_ARROW; i <= RIGHT_ARROW; i++) {
		windowGadgets[i].LeftOffset		= 1 - arrowWidth;
		windowGadgets[i].TopOffset		= 1 - arrowHeight;
		windowGadgets[i].WidthOffset	= arrowWidth;
		windowGadgets[i].HeightOffset	= arrowHeight;
	}
	windowGadgets[UP_ARROW].TopOffset		-= 2*arrowHeight;
	windowGadgets[DOWN_ARROW].TopOffset		-= arrowHeight;
	windowGadgets[LEFT_ARROW].LeftOffset	-= 2*arrowWidth;
	windowGadgets[RIGHT_ARROW].LeftOffset	-= arrowWidth;

	windowGadgets[VERT_SCROLL].LeftOffset	= 1 - arrowWidth;
	windowGadgets[VERT_SCROLL].TopOffset	= barHeight;
	windowGadgets[VERT_SCROLL].WidthOffset	= arrowWidth;
	windowGadgets[VERT_SCROLL].HeightOffset	= -3*arrowHeight - barHeight;
	windowGadgets[HORIZ_SCROLL].LeftOffset	= leftEdge + 2*arrowWidth + LYINDIC_WIDTH;
	windowGadgets[HORIZ_SCROLL].TopOffset	= 1 - arrowHeight;
	windowGadgets[HORIZ_SCROLL].WidthOffset	= -leftEdge - 5*arrowWidth - LYINDIC_WIDTH;
	windowGadgets[HORIZ_SCROLL].HeightOffset= arrowHeight;
	if (intuiVersion >= OSVERSION_2_0) {
		windowGadgets[VERT_SCROLL].LeftOffset	+= 4;
		windowGadgets[VERT_SCROLL].TopOffset	+= 1;
		windowGadgets[VERT_SCROLL].WidthOffset	-= 8;
		windowGadgets[VERT_SCROLL].HeightOffset	-= 2;
		windowGadgets[HORIZ_SCROLL].LeftOffset	+= 2;
		windowGadgets[HORIZ_SCROLL].TopOffset	+= 2;
		windowGadgets[HORIZ_SCROLL].WidthOffset	-= 4;
		windowGadgets[HORIZ_SCROLL].HeightOffset-= 4;
	}
	else {
		windowGadgets[VERT_SCROLL].TopOffset	-= 1;
		windowGadgets[VERT_SCROLL].HeightOffset	+= 1;
	}

	windowGadgets[LAYERUP_ARROW].LeftOffset		= leftEdge;
	windowGadgets[LAYERDOWN_ARROW].LeftOffset	= leftEdge + arrowWidth;
	for (i = LAYERUP_ARROW; i <= LAYERDOWN_ARROW; i++) {
		windowGadgets[i].TopOffset		= 1 - arrowHeight;
		windowGadgets[i].WidthOffset	= arrowWidth;
		windowGadgets[i].HeightOffset	= arrowHeight;
	}
/*
	Start the monitor task
*/
	if ((mainMsgPort = CreatePort(NULL, 0)) == NULL) {
		InitError(initErrors[INIT_NOMEM]);
		ShutDown();
		exit(RETURN_FAIL);
	}
	monitorTask = CreateTask(NULL,
							 (UBYTE) (process->pr_Task.tc_Node.ln_Pri + 1),
							 (APTR) MonitorTask, 1000L);
	if (monitorTask == NULL) {
		InitError(initErrors[INIT_NOMEM]);
		ShutDown();
		exit(RETURN_FAIL);
	}
/*
	Open background window
	(Monitor task will have set up monitorMsgPort)
*/
	if ((backWindow = OpenBackWindow()) == NULL) {
		InitError(initErrors[INIT_NOMEM]);
		ShutDown();
		exit(RETURN_FAIL);
	}
	SetStdPointer(backWindow, POINTER_WAIT);
	if (_tbOnPubScreen && intuiVersion >= OSVERSION_2_0)
		UnlockPubScreen(NULL, screen);				// Recommended procedure
/*
	Have system requesters appear in this screen
*/
	SetSysRequestWindow(backWindow);
/*
	Display Init dialog
*/
	if (!GetSerialInfo(&userName, &companyName, &serialNum, screen, mainMsgPort,
					   strProgName, PROGRAM_VERSION, progPathName)) {
		ShutDown();
		exit(RETURN_FAIL);
	}
	if ((initDlg = GetDialog(dlgList[DLG_INIT], screen, mainMsgPort)) == NULL) {
		InitError(initErrors[INIT_NOMEM]);
		ShutDown();
		exit(RETURN_FAIL);
	}
	gadgList = initDlg->FirstGadget;
	SetGadgetItemText(gadgList, NAME_STATTEXT, initDlg, NULL, userName);
	SetGadgetItemText(gadgList, COMPANY_STATTEXT, initDlg, NULL, companyName);
	SetGadgetItemText(gadgList, SERIAL_STATTEXT, initDlg, NULL, serialNum);
	SetStdPointer(initDlg, POINTER_WAIT);
	CurrentTime(&dlgStartSecs, &dummy);
/*
	Set up menus
*/
	menuTempl = (screen->ViewPort.Modes & HIRES) ? docWindMenus : altWindMenus;
	if ((docMenuStrip = GetMenuStrip(menuTempl)) == NULL) {
		DisposeDialog(initDlg);
		InitError(initErrors[INIT_NOMEM]);
		ShutDown();
		exit(RETURN_FAIL);
	}
	InsertMenuStrip(backWindow, docMenuStrip);
/*
	Misc initialization
*/
	InitClipboard();
	SetAllMenus();			// Must do before BeginWait()
	UndoOff();
	BeginWait();
	if (!InitFonts()) {
		DisposeDialog(initDlg);
		InitError(initErrors[INIT_NOMEM]);
		ShutDown();
		exit(RETURN_FAIL);
	}
	GetSysPrefs();
	GetProgPrefs(NULL);			// Do this here to minimize disk swapping
								// Do this here so only necessary if no colors saved
	if (!InitPenColors(NUM_STDPENS, colors, colorNames) ||
		!InitStdPatterns()) {
		DisposeDialog(initDlg);
		InitError(initErrors[INIT_NOMEM]);
		ShutDown();
		exit(RETURN_FAIL);
	}
	InitToolbox(screen);	// In case color palette changed
	LoadFKeys();
	InitRexx();
	SetMacroMenu();
	InitIcons();
	InitHotLinks();
	screenPalette.NumPens = GetColorTable(screen, &screenPalette.ColorTable);
	MakeInvColorTable(&screenPalette);
	do {
		CurrentTime(&currSecs, &dummy);
		Delay(5);						// 1/10 second
	} while (currSecs < dlgStartSecs + 6);
	DisposeDialog(initDlg);
	SetStdPointer(backWindow, POINTER_ARROW);
	EndWait();
}

/*
 *	Wait until all visitor windows are closed on screen
 */

static void WaitVisitor()
{
	register WindowPtr	window;
	register MsgPtr		msg;

	if (_tbOnPubScreen || screen == NULL || backWindow == NULL)
		return;
/*
	Busy loop, since we won't receive INTUITICKS if backWindow is not active
*/
	ModifyIDCMP(backWindow, REFRESHWINDOW);
	for (;;) {
		while (msg = GetMsg(mainMsgPort))
			ReplyMsg(msg);
		for (window = screen->FirstWindow; window; window = window->NextWindow) {
			if (window != backWindow)
				break;
		}
		if (window == NULL) {			// No visitor windows found
			if (intuiVersion < OSVERSION_2_0 ||
				!IsPubScreen(screen) ||
				(PubScreenStatus(screen, PSNF_PRIVATE) & PSNF_PRIVATE))
				break;
		}
		Delay(10L);						// Wait 1/5 second before checking again
	}
}

/*
 *	Shut down program
 *	Close backWindow, close libraries, and quit
 */

void ShutDown()
{
	MsgPtr	msg;

	CloseToolWindow();
	ClosePenWindow();
	CloseFillWindow();
	DisposePenColors();
	DisposeFillPatterns();
	ShutDownRexx();
	ShutDownClipboard();
	ShutDownHotLinks();
	ShutDownFonts();
	if (smallFont)
		CloseFont(smallFont);
	ShutDownIcons();			// May need to open Workbench, so free memory first
	if (backWindow) {
		ClearSysRequestWindow();
		ClearMenuStrip(backWindow);
		WaitVisitor();
		CloseWindowSafely(backWindow, mainMsgPort);
	}
	if (monitorTask) {		// Need Forbid/Permit to work with MemMung
		Forbid();
		DeleteTask(monitorTask);
		Permit();
	}
	if (mainMsgPort) {
		while ((msg = GetMsg(mainMsgPort)) != NULL)
			ReplyMsg(msg);
		DeletePort(mainMsgPort);
	}
	if (docMenuStrip)
		DisposeMenuStrip(docMenuStrip);
	DisposeScreen(screen);
	if (IFFParseBase)
		CloseLibrary(IFFParseBase);
	StdShutDown();
}

/*
 *	Open initial files, or open new window if none given
 */

void SetUp(int argc, char *argv[])
{
	WORD			i, numFiles, len;
	BOOL			fileOpened;
	TextPtr			fileName;
	Dir				dir, startupDir;
	ProcessPtr		process;
	struct WBArg	*wbArgList;

/*
	Open files
	First check to see if this is a prefs file, if so then load prefs
*/
	fileOpened = FALSE;
	numFiles = (fromCLI) ? argc : WBenchMsg->sm_NumArgs;
	process = (ProcessPtr) FindTask(NULL);
	startupDir = DupLock(process->pr_CurrentDir);
	for (i = 1; i < numFiles; i++) {
		if (fromCLI) {
			fileName = argv[i];
			if (*fileName == '-')			// Ignore program options
				continue;
			SetCurrentDir(startupDir);		// Path is relative to startup dir
			dir = ConvertFileName(fileName);
		}
		else {
			wbArgList = WBenchMsg->sm_ArgList;
			fileName = wbArgList[i].wa_Name;
			dir = DupLock(wbArgList[i].wa_Lock);
		}
		len = strlen(fileName);
		if (len == 0 || len > MAX_FILENAME_LEN)
			UnLock(dir);
		else {
			SetCurrentDir(dir);
			if (ReadProgPrefs(fileName))
				UnLock(dir);
			else if (OpenFile(fileName, dir))
				fileOpened = TRUE;
		}
	}
	UnLock(startupDir);
/*
	If no file opened, then open new window
*/
	if (!fileOpened)
		DoNew();
}
