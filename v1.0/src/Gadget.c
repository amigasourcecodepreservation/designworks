/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Gadget routines
 */

#include <exec/types.h>
#include <graphics/gfxmacros.h>
#include <graphics/regions.h>
#include <intuition/intuition.h>

#include <proto/graphics.h>
#include <proto/layers.h>
#include <proto/intuition.h>

#include <string.h>

#include <Toolbox/Graphics.h>
#include <Toolbox/Gadget.h>
#include <Toolbox/Window.h>
#include <Toolbox/Utility.h>

#include "Draw.h"
#include "Proto.h"

/*
 *	External variables
 */

extern WORD	intuiVersion;

extern MsgPortPtr	mainMsgPort;
extern ScreenPtr	screen;

extern UBYTE	blackColor;

extern BOOL	titleChanged, onPubScreen;

extern TextFontPtr	smallFont;

extern TextChar	strLayer[];
extern TextChar	strBuff[];

/*
 *	Local variables and definitions
 */

static BOOL	altKey;		/* Set by DoGadgetDown() for gadget tracking routines */

/*
 *	Local prototypes
 */

void	AdjustScrollOffsets(WindowPtr, WORD *, WORD *);
void	MaxWindowOffsets(WindowPtr, WORD *, WORD *);

void	GetLayerIndicRect(WindowPtr, RectPtr);

void	TrackSliders(WindowPtr, WORD);
void	ScrollWindow(WindowPtr, WORD);

/*
 *	Adjust scroll offsets for pattern alignment
 */

static void AdjustScrollOffsets(WindowPtr window, WORD *left, WORD *top)
{
	WORD x, y;
	Fixed scale;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);

	scale = docData->Scale;
	x = ((LONG) (*left)*scale)/SCALE_FULL;
	y = ((LONG)  (*top)*scale)/SCALE_FULL;
	x = (((x + 7) >> 3) << 3);			/* Nearest multiple of 8 */
	y = (((y + 7) >> 3) << 3);
	*left = ((LONG) x*SCALE_FULL)/scale;
	*top  = ((LONG) y*SCALE_FULL)/scale;
}

/*
 *	Return maximum permitted page, top, and left offsets
 */

static void MaxWindowOffsets(WindowPtr window, WORD *left, WORD *top)
{
	WORD x, y;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
	Rectangle rect;

	GetContentRect(window, &rect);
	ScaleWinToDoc(docData->Scale, rect.MaxX - rect.MinX, rect.MaxY - rect.MinY,
				  &x, &y);
	*left = docData->DocWidth - x + 8;
	if (*left < 0)
		*left = 0;
	*top = docData->DocHeight - y + 8;
	if (*top < 0)
		*top = 0;
	AdjustScrollOffsets(window, left, top);
}

/*
 *	Get rectangle of layer indicator
 */

static void GetLayerIndicRect(WindowPtr window, Rectangle *rect)
{
	SetRect(rect, LYINDIC_EDGE, window->Height - window->BorderBottom + 1,
			LYINDIC_EDGE + LYINDIC_WIDTH - 1, window->Height - 1);
}

/*
 *	Set layer indicator to current layer
 */

void SetLayerIndic(WindowPtr window)
{
	LayerPtr layer = window->WLayer;
	RastPtr rPort = window->RPort;
	RegionPtr oldRgn;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
	Rectangle rect;

	GetLayerIndicRect(window, &rect);
	oldRgn = GetClip(layer);
	SetRectClip(layer, &rect);
	SetRast(rPort, screen->BlockPen);
	SetFont(rPort, smallFont);
	SetSoftStyle(rPort, FS_NORMAL, 0xFF);
	SetDrMd(rPort, JAM1);
	SetAPen(rPort, screen->DetailPen);
	Move(rPort, rect.MinX + 2, rect.MinY + 7);
	Text(rPort, strLayer, strlen(strLayer));
	NumToString(LayerNum(docData, CurrLayer(docData)) + 1, strBuff);
	Text(rPort, strBuff, strlen(strBuff));
	if (intuiVersion >= OSVERSION_2_0 && !onPubScreen) {
		SetAPen(rPort, blackColor);
		SetDrPt(rPort, 0xFFFF);
		Move(rPort, rect.MinX, rect.MaxY);
		Draw(rPort, rect.MaxX, rect.MaxY);
	}
	SetClip(layer, oldRgn);
}

/*
 *	Adjust scroll bars to correspond to current offsets
 *	Don't adjust scroll bars if they are selected (the user is dragging them)
 */

void AdjustScrollBars(WindowPtr window)
{
	WORD width, height, maxLeft, maxTop;
	LONG visAmount, totAmount, currPos, newPot, newBody;
	GadgetPtr gadget;
	PropInfoPtr propInfo;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
	Rectangle rect;

	GetContentRect(window, &rect);
	ScaleWinToDoc(docData->Scale, rect.MaxX - rect.MinX, rect.MaxY - rect.MinY,
				  &width, &height);
	MaxWindowOffsets(window, &maxLeft, &maxTop);
/*
	Adjust vertical scroll bar
*/
	if ((gadget = GadgetItem(window->FirstGadget, VERT_SCROLL)) != NULL &&
		!(gadget->Flags & SELECTED)) {
		propInfo = (PropInfoPtr) gadget->SpecialInfo;
		visAmount = height;
		totAmount = visAmount + maxTop;
		currPos = docData->TopOffset;
		if (totAmount < visAmount + currPos)
			totAmount = visAmount + currPos;
		newPot = (totAmount == visAmount) ?
				 0xFFFF : (0xFFFFL*currPos)/(totAmount - visAmount);
		newBody = (0xFFFFL*visAmount)/totAmount;
		if (newBody == 0)
			newBody =  1;
		if (newPot != propInfo->VertPot || newBody != propInfo->VertBody) {
			WaitBOVP(&screen->ViewPort);
			NewModifyProp(gadget, window, NULL, AUTOKNOB | FREEVERT,
						  0, newPot, 0xFFFF, newBody, 1);
		}
	}
/*
	Adjust horizontal scroll bar
*/
	if ((gadget = GadgetItem(window->FirstGadget, HORIZ_SCROLL)) != NULL &&
		!(gadget->Flags & SELECTED)) {
		propInfo = (PropInfoPtr) gadget->SpecialInfo;
		visAmount = width;
		totAmount = visAmount + maxLeft;
		currPos = docData->LeftOffset;
		if (totAmount < visAmount + currPos)
			totAmount = visAmount + currPos;
		newPot = (totAmount == visAmount) ?
				 0xFFFF : (0xFFFFL*currPos)/(totAmount - visAmount);
		newBody = (0xFFFFL*visAmount)/totAmount;
		if (newPot != propInfo->HorizPot || newBody != propInfo->HorizBody) {
			WaitBOVP(&screen->ViewPort);
			NewModifyProp(gadget, window, NULL, AUTOKNOB | FREEHORIZ,
						  newPot, 0, newBody, 0xFFFF, 1);
		}
	}
}

/*
 *	Set scroll offsets, without scrolling window
 *	Clips offsets to legal values
 *	Note: Caller should call InvalRect() on entire window when using this routine
 */

void SetScrollOffset(WindowPtr window, WORD leftOffset, WORD topOffset)
{
	WORD maxLeft, maxTop;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);

	MaxWindowOffsets(window, &maxLeft, &maxTop);
	if (leftOffset < 0)
		leftOffset = 0;
	if (topOffset < 0)
		topOffset = 0;
	if (leftOffset > maxLeft)
		leftOffset = maxLeft;
	if (topOffset > maxTop)
		topOffset = maxTop;
	AdjustScrollOffsets(window, &leftOffset, &topOffset);
	docData->LeftOffset = leftOffset;
	docData->TopOffset = topOffset;
	AdjustScrollBars(window);
}

/*
 *	Adjust scroll offsets to legal values, scrolling if necessary
 */

void AdjustScrollOffset(WindowPtr window)
{
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);

	ScrollToOffset(window, docData->LeftOffset, docData->TopOffset);
}

/*
 *	Scroll to the given top and left offsets
 *	Clips offsets to legal values
 */

void ScrollToOffset(WindowPtr window, WORD leftOffset, WORD topOffset)
{
	WORD dx, dy, maxX, maxY;
	LayerPtr layer = window->WLayer;
	RegionPtr oldClip;
	register DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
	Rectangle rect;

	if (leftOffset == docData->LeftOffset && topOffset == docData->TopOffset)
		return;
	MaxWindowOffsets(window, &maxX, &maxY);
	if (leftOffset < 0)
		leftOffset = 0;
	if (topOffset < 0)
		topOffset = 0;
	if (leftOffset > maxX)
		leftOffset = maxX;
	if (topOffset > maxY)
		topOffset = maxY;
	AdjustScrollOffsets(window, &leftOffset, &topOffset);
	if (leftOffset == docData->LeftOffset && topOffset == docData->TopOffset) {
		AdjustScrollBars(window);
		return;
	}
/*
	Set bounds rectangle to content region of window
*/
	GetWindowRect(window, &rect);
	DocToWindow(window, docData->DocWidth, docData->DocHeight, &maxX, &maxY);
	if (maxX > rect.MaxX)
		maxX = rect.MaxX;
	if (maxY > rect.MaxY)
		maxY = rect.MaxY;
	oldClip = GetClip(layer);
	SetWindowClip(window);
	RulerIndicOff(window);
/*
	Scroll to new vertical position
*/
	if (topOffset != docData->TopOffset) {
		ScaleDocToWin(docData->Scale, 0, topOffset - docData->TopOffset, &dx, &dy);
		rect.MinY += RulerHeight(window);
		ScrollRect(layer, 0, dy, rect.MinX, rect.MinY, maxX, rect.MaxY);
		rect.MinY -= RulerHeight(window);
		docData->TopOffset = topOffset;
	}
/*
	Scroll to new horizontal position
*/
	if (leftOffset != docData->LeftOffset) {
		ScaleDocToWin(docData->Scale, leftOffset - docData->LeftOffset, 0, &dx, &dy);
		rect.MinX += RulerWidth(window);
		ScrollRect(layer, dx, 0, rect.MinX, rect.MinY, rect.MaxX, maxY);
		rect.MinX -= RulerWidth(window);
		docData->LeftOffset = leftOffset;
	}
/*
	Update the cleared regions of the window
*/
	RulerIndicOn(window);
	AdjustScrollBars(window);
	UpdateWindow(window);
	SetClip(layer, oldClip);
}

/*
 *	Adjust window to vertical scroll bar settings
 */

void AdjustToVertScroll(WindowPtr window)
{
	WORD width, height, maxLeft, maxTop;
	LONG visAmount, totAmount, currPos, newPos;
	GadgetPtr gadget;
	PropInfoPtr propInfo;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
	Rectangle rect;

	if ((gadget = GadgetItem(window->FirstGadget, VERT_SCROLL)) == NULL)
		return;
	propInfo = (PropInfoPtr) gadget->SpecialInfo;
	GetContentRect(window, &rect);
	ScaleWinToDoc(docData->Scale, rect.MaxX - rect.MinX, rect.MaxY - rect.MinY,
				  &width, &height);
	MaxWindowOffsets(window, &maxLeft, &maxTop);
/*
	Get the new position in dots
*/
	visAmount = height;
	totAmount = visAmount + maxTop;
	currPos = docData->TopOffset;
	if (totAmount < visAmount + currPos)
		totAmount = visAmount + currPos;
	newPos = propInfo->VertPot*(totAmount - visAmount);
	newPos = (newPos + 0x7FFFL)/0xFFFFL;
/*
	Scroll to new position and update window
*/
	ScrollToOffset(window, docData->LeftOffset, newPos);
}

/*
 *	Adjust window to horizontal scroll bar settings
 */

void AdjustToHorizScroll(WindowPtr window)
{
	WORD width, height, maxLeft, maxTop;
	LONG visAmount, totAmount, currPos, newPos;
	GadgetPtr gadget;
	PropInfoPtr propInfo;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
	Rectangle rect;

	if ((gadget = GadgetItem(window->FirstGadget, HORIZ_SCROLL)) == NULL)
		return;
	propInfo = (PropInfoPtr) gadget->SpecialInfo;
	GetContentRect(window, &rect);
	ScaleWinToDoc(docData->Scale, rect.MaxX - rect.MinX, rect.MaxY - rect.MinY,
				  &width, &height);
	MaxWindowOffsets(window, &maxLeft, &maxTop);
/*
	Get the new position in dots
*/
	visAmount = width;
	totAmount = visAmount + maxLeft;
	currPos = docData->LeftOffset;
	if (totAmount < visAmount + currPos)
		totAmount = visAmount + currPos;
	newPos = propInfo->HorizPot*(totAmount - visAmount);
	newPos = (newPos + 0x7FFFL)/0xFFFFL;
/*
	Scroll to new position and update window
*/
	ScrollToOffset(window, newPos, docData->TopOffset);
}

/*
 *	Scroll window up
 *	Scrolling occurs in 1 inch increments or full window
 */

void ScrollUp(WindowPtr window, BOOL scrollFull)
{
	WORD inc, topOffset, dx, dy;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
	Rectangle rect;

	GetContentRect(window, &rect);
	topOffset = docData->TopOffset;
	if (topOffset <= 0)
		return;
	inc = ((LONG) (docData->yDPI)*SCALE_FULL)/docData->Scale;
	if (scrollFull) {
		ScaleWinToDoc(docData->Scale, 0, rect.MaxY - rect.MinY, &dx, &dy);
		if (dy > inc)
			dy -= inc;
	}
	else
		dy = inc;
	ScrollToOffset(window, docData->LeftOffset, topOffset - dy);
}

/*
 *	Scroll window down
 *	Scrolling occurs in 1 inch increments or full window
 */

void ScrollDown(WindowPtr window, BOOL scrollFull)
{
	WORD inc, topOffset, dx, dy;
	WORD maxLeft, maxTop;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
	Rectangle rect;

	GetContentRect(window, &rect);
	MaxWindowOffsets(window, &maxLeft, &maxTop);
	topOffset = docData->TopOffset;
	if (topOffset >= maxTop)
		return;
	inc = ((LONG) (docData->yDPI)*SCALE_FULL)/docData->Scale;
	if (scrollFull) {
		ScaleWinToDoc(docData->Scale, 0, rect.MaxY - rect.MinY, &dx, &dy);
		if (dy > inc)
			dy -= inc;
	}
	else
		dy = inc;
	ScrollToOffset(window, docData->LeftOffset, topOffset + dy);
}

/*
 *	Scroll window left
 *	Scrolling occurs in 1 inch increments or full window
 */

void ScrollLeft(WindowPtr window, BOOL scrollFull)
{
	WORD inc, leftOffset, dx, dy;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
	Rectangle rect;

	GetContentRect(window, &rect);
	leftOffset = docData->LeftOffset;
	if (leftOffset <= 0)
		return;
	inc = ((LONG) (docData->xDPI)*SCALE_FULL)/docData->Scale;
	if (scrollFull) {
		ScaleWinToDoc(docData->Scale, rect.MaxX - rect.MinY, 0, &dx, &dy);
		if (dx > inc)
			dx -= inc;
	}
	else
		dx = inc;
	ScrollToOffset(window, leftOffset - dx, docData->TopOffset);
}

/*
 *	Scroll window right
 *	Scrolling occurs in 1 inch increments or full window
 */

void ScrollRight(WindowPtr window, BOOL scrollFull)
{
	WORD inc, leftOffset, dx, dy;
	WORD maxLeft, maxTop;
	register DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
	Rectangle rect;

	GetContentRect(window, &rect);
	MaxWindowOffsets(window, &maxLeft, &maxTop);
	leftOffset = docData->LeftOffset;
	if (leftOffset >= maxLeft)
		return;
	inc = ((LONG) (docData->xDPI)*SCALE_FULL)/docData->Scale;
	if (scrollFull) {
		ScaleWinToDoc(docData->Scale, rect.MaxX - rect.MinX, 0, &dx, &dy);
		if (dx > inc)
			dx -= inc;
	}
	else
		dx = inc;
	ScrollToOffset(window, leftOffset + dx, docData->TopOffset);
}

/*
 *	Track vertical and horizontal sliders and update page number indicator
 *	Update window as scrolling if ALT key down
 */

static void TrackSliders(WindowPtr window, WORD gadgNum)
{
/*
	Handle horizontal scroll box
*/
	if (gadgNum == HORIZ_SCROLL) {
		if (altKey)
			AdjustToHorizScroll(window);
	}
/*
	Handle vertical scroll box
*/
	else if (gadgNum == VERT_SCROLL) {
		if (altKey)
			AdjustToVertScroll(window);
	}
}

/*
 *	Scroll window in direction indicated by gadget
 *	Called by TrackGadget()
 */

static void ScrollWindow(WindowPtr window, WORD gadgNum)
{
	switch (gadgNum) {
	case UP_ARROW:
		ScrollUp(window, altKey);
		break;
	case DOWN_ARROW:
		ScrollDown(window, altKey);
		break;
	case LEFT_ARROW:
		ScrollLeft(window, altKey);
		break;
	case RIGHT_ARROW:
		ScrollRight(window, altKey);
		break;
	}
}

/*
 *	Handle gadget down event
 */

void DoGadgetDown(WindowPtr window, GadgetPtr gadget, UWORD modifier)
{
	register WORD gadgNum;

	if (titleChanged)
		FixTitle();
	altKey = ((modifier & ALTKEYS) != 0);
/*
	Ignore if not a window gadget
*/
	gadgNum = GadgetNumber(gadget);
	if (gadget != GadgetItem(window->FirstGadget, gadgNum))
		return;
/*
	Handle gadget down
*/
	RulerIndicOff(window);
	switch (gadgNum) {
	case UP_ARROW:
	case DOWN_ARROW:
	case LEFT_ARROW:
	case RIGHT_ARROW:
		SetStdPointer(window, POINTER_ARROW);
		TrackGadget(mainMsgPort, window, gadget, ScrollWindow);
		SetPointerShape();
		break;
	case VERT_SCROLL:
	case HORIZ_SCROLL:
		SetStdPointer(window, POINTER_ARROW);
		TrackGadget(mainMsgPort, window, gadget, TrackSliders);
		break;
	case LAYERUP_ARROW:
		DoLayerUp(window, altKey);
		break;
	case LAYERDOWN_ARROW:
		DoLayerDown(window, altKey);
		break;
	}
	RulerIndicOn(window);
}

/*
 *	Handle gadget up event
 */

void DoGadgetUp(WindowPtr window, GadgetPtr gadget)
{
	WORD gadgNum;

/*
	Ignore if not a window gadget
*/
	gadgNum = GadgetNumber(gadget);
	if (gadget != GadgetItem(window->FirstGadget, gadgNum))
		return;
/*
	Handle gadget up
*/
	SetPointerShape();
	RulerIndicOff(window);
	switch (gadgNum) {
	case VERT_SCROLL:
		AdjustToVertScroll(window);
		break;
	case HORIZ_SCROLL:
		AdjustToHorizScroll(window);
		break;
	}
	RulerIndicOn(window);
}
