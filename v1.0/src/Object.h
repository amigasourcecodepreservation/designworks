/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software, Inc.
 */

#ifndef DEVICES_PRINTER_H
#include <devices/printer.h>
#endif

#ifndef TYPEDEFS_H
#include <TypeDefs.h>
#endif

#include <IFF/DRAW.h>

#include "Print.h"

/*
 *	Useful macros
 */

#define DocModified(docData)	((docData)->Flags |= DOC_MODIFIED)

#define SelectObject(docObj)	((docObj)->Flags |= OBJ_SELECTED)
#define UnSelectObject(docObj)	((docObj)->Flags &= ~OBJ_SELECTED)
#define ObjectSelected(docObj)	((docObj)->Flags & OBJ_SELECTED)

#define LockObject(docObj)		((docObj)->Flags |= OBJ_LOCKED)
#define UnLockObject(docObj)	((docObj)->Flags &= ~OBJ_LOCKED)
#define ObjectLocked(docObj)	((docObj)->Flags & OBJ_LOCKED)

#define HasPen(docObj)			(((DocObjPtr) (docObj))->Flags & OBJ_DO_PEN)

#define HasFill(docObj)			(((DocObjPtr) (docObj))->Flags & OBJ_DO_FILL)

#define PolySmoothed(polyObj)	((polyObj)->PolyFlags & POLY_SMOOTH)
#define PolyClosed(polyObj)		((polyObj)->PolyFlags & POLY_CLOSED)

#define ShowLayer(docLayer)		((docLayer)->Flags |= LAYER_VISIBLE)
#define HideLayer(docLayer)		((docLayer)->Flags &= ~LAYER_VISIBLE)
#define LayerVisible(docLayer)	((docLayer)->Flags & LAYER_VISIBLE)

#define CopyRGBPat8(src, dst)	(BlockMove((src), (dst), sizeof(RGBPat8)))

/*
 *	Note: intuition uses the name NextObject()
 */

#define NextObj(docObj)	((docObj)->Next)
#define PrevObj(docObj)	((docObj)->Prev)

/*
 *	Default data
 */

typedef struct Defaults {
	PrintRecord	PrintRec;
	UWORD		DocFlags;
	UWORD		ObjFlags;
	UWORD		LineFlags;			/* For default arrows */
	FontNum		FontNum;
	FontSize	FontSize;
	UBYTE		Style, MiscStyle;
	UBYTE		Justify, Spacing;
	UBYTE		PenWidth, PenHeight;
	RGBColor	PenColor;
	RGBPat8		FillPat;
	UBYTE		GridUnitsIndex;
	UBYTE		pad[99];
} *DefaultsPtr;
	
/*
 *	Doc objects
 *	All measurements are in screen dots, unless otherwise noted
 */

enum {
	TYPE_GROUP,	TYPE_TEXT,	TYPE_BMAP,	TYPE_LINE,
	TYPE_RECT,	TYPE_OVAL,	TYPE_POLY
};

#define TYPE_MAXTYPE	TYPE_POLY

typedef struct DocObject {
	WORD		Type;
	UWORD		Flags;

	struct DocObject	*Next, *Prev;
	struct GroupObj		*Group;		/* NULL if not part of a group */

	Rectangle	Frame;			/* Location and size, relative to group origin */
} *DocObjPtr;

typedef struct GroupObj {
	struct DocObject	DocObj;

	struct DocObject	*Objects;
} *GroupObjPtr;

typedef struct LineObj {
	struct DocObject	DocObj;

	UWORD		LineFlags;
	UBYTE		PenWidth, PenHeight;
	RGBColor	PenColor;
	Point		Start, End;			/* Relative to Frame */
} *LineObjPtr;

typedef struct RectObj {
	struct DocObject	DocObj;

	UBYTE		PenWidth, PenHeight;
	RGBColor	PenColor;
	RGBPat8		FillPat;
} *RectObjPtr;

typedef struct OvalObj {
	struct DocObject	DocObj;

	UBYTE		PenWidth, PenHeight;
	RGBColor	PenColor;
	RGBPat8		FillPat;
} *OvalObjPtr;

typedef struct PolyObj {
	struct DocObject	DocObj;

	UWORD		PolyFlags;
	UBYTE		PenWidth, PenHeight;
	RGBColor	PenColor;
	RGBPat8		FillPat;
	UWORD		NumPoints;
	PointPtr	Points;			/* Array of points, relative to frame */
} *PolyObjPtr;

typedef struct TextObj {
	struct DocObject	DocObj;

	UWORD		TextFlags;
	RGBColor	PenColor;
	RGBPat8		FillPat;
	WORD		Rotate;				/* Degrees clockwise */
	TextPtr		Text;				/* Pointer to TextLen characters */
	WORD		TextLen;
	WORD		SelStart, SelEnd;	/* Used when text is selected */
	WORD		DragStart;			/* Used when selecting text */
	FontNum		FontNum;
	FontSize	FontSize;
	UBYTE		Style, MiscStyle;
	UBYTE		Justify, Spacing;
} *TextObjPtr;

typedef struct BMapObj {
	struct DocObject	DocObj;

	WORD		Rotate;				/* Degrees clockwise */
	WORD		Width, Height;
	RGBColor	*Data;				/* Width*Height color values */
} *BMapObjPtr;

/*
 *	IFF picture data (used when converting iff pictures to BMapObj's)
 */

typedef struct {
	WORD			Width, Height;
	BOOL			IsHAM;
	WORD			TranspColor;		/* Transparent color, or -1 if none */
	BitMapPtr		BitMap;
	PLANEPTR		Mask;				/* NULL if no mask */
	WORD			NumColors;
	RGBColor		ColorTable[256];	/* Up to eight planes */
} IFFPict, *IFFPictPtr;

/*
 *	Layers
 */

typedef struct DocLayer {
	struct DocLayer	*Next, *Prev;

	ULONG			Flags;			/* Flags from above defines */

	DocObjPtr		Objects;		/* Objects in layer, back-to-front order */
	TextPtr			Name;			/* Layer name (or NULL if none) */
} *DocLayerPtr;

/*
 *	Document window data structure
 *	Pointed to by UserData field of Window structure
 */

#define DOC_MODIFIED	0x0001	/* Data has been modified */
#define DOC_SHOWRULER	0x0002
#define DOC_SHOWGRID	0x0004	/* Show grid lines */
#define DOC_SHOWPAGE	0x0008	/* Show page breaks */
#define DOC_GRIDSNAP	0x0010
#define DOC_RULERINDIC	0x0020	/* Ruler indicators are on */
#define DOC_HILITE		0x0040	/* Selection hilight is on */
#define DOC_DRAGGING	0x0080	/* Currently dragging an object */
#define DOC_HIDEBACKLAY	0x0100	/* Hide back layers */

#define SCALE_FULL	FIXED_UNITY
#define SCALE_MAX	(SCALE_FULL << 1)
#define SCALE_MIN	(SCALE_FULL >> 5)

#define PagesAcross(docData)	(((docData)->DocWidth - 1)/(docData)->PageWidth + 1)
#define PagesDown(docData)		(((docData)->DocHeight - 1)/(docData)->PageHeight + 1)

typedef struct DocData {
	ULONG		Flags;				/* Flags from above defines */
	DocLayerPtr	CurrLayer;			/* Current layer */
	DocLayerPtr	Layers;				/* All layers, back-to-front order */

	WindowPtr	Window;				/* Owner window */

	PrintRecPtr	PrintRec;

	WORD		TopOffset;			/* Scroll offset from top */
	WORD		LeftOffset;			/* Scroll offset from left */

	Fixed		Scale;				/* Reduction/enlargement */
	WORD		GridUnitsIndex;		/* Index into grid spacing table */
	Point		RulerOffset;		/* Ruler zero-point offset */
	Rectangle	RulerIndic;			/* Location of ruler indicator */
									/* If not dragging, only show MinX, MinY */

	WORD		xDPI, yDPI;			/* x and y dots per inch */
	WORD		PageWidth, PageHeight;	/* In pixels */
	WORD		DocWidth, DocHeight;	/* In pixels */

	WORD		BlinkCount;			/* Countdown timer for cursor blink */

	TextChar	FileName[32];		/* Name of file */
	Dir			DirLock;			/* Lock of file directory */
	TextChar	WindowName[32];		/* Without layer name appended */
} *DocDataPtr;

#ifndef c_plusplus
typedef struct Defaults		Defaults;
typedef struct DocObject	DocObject;
typedef struct GroupObj		GroupObj;
typedef struct LineObj		LineObj;
typedef struct RectObj		RectObj;
typedef struct OvalObj		OvalObj;
typedef struct PolyObj		PolyObj;
typedef struct TextObj		TextObj;
typedef struct BMapObj		BMapObj;
typedef struct DocLayer		DocLayer;
typedef struct DocData		DocData;
#endif
