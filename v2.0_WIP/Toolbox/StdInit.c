/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Toolbox library
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	Standard initialization/shut down routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>
#include <libraries/dos.h>

#include <proto/exec.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include <string.h>

#include <Toolbox/StdInit.h>
#include <Toolbox/Utility.h>

/*
 *	External variables
 */

extern struct Library	*IntuitionBase, *GfxBase, *LayersBase, *IconBase, *DiskfontBase;
extern struct Device	*ConsoleDevice;

extern TextChar	_strCancel[];
extern TextPtr	_strInitErrors[];

/*
 *	Local variables and definitions
 */

static Dir			initDir;
static WindowPtr	initProcWindow;

static struct IOStdReq	consoleIOReq;

enum {
	INIT_BADSYS = 0,
	INIT_NOICON,
	INIT_NOFONT
};

/*
 *	Report initialization failure
 */

void InitError(TextPtr text)
{
	IntuiText	bodyText, negText;

	if (IntuitionBase) {
		bodyText.FrontPen	= negText.FrontPen  = AUTOFRONTPEN;
		bodyText.BackPen	= negText.BackPen   = AUTOBACKPEN;
		bodyText.DrawMode	= negText.DrawMode  = AUTODRAWMODE;
		bodyText.LeftEdge	= negText.LeftEdge  = AUTOLEFTEDGE;
		bodyText.TopEdge	= negText.TopEdge   = AUTOTOPEDGE;
		bodyText.ITextFont	= negText.ITextFont = AUTOITEXTFONT;
		bodyText.NextText	= negText.NextText  = AUTONEXTTEXT;
		bodyText.IText		= text;
		negText.IText		= _strCancel;
		SysBeep(5);
		DisplayBeep(NULL);
		AutoRequest(NULL, &bodyText, NULL, &negText, 0L, 0L, strlen(text)*8 + 50, 50);
	}
}

/*
 *	Standard initialization
 *	Open libraries and devices
 */

BOOL StdInit(WORD libVersion)
{
	ProcessPtr	process;
	Dir			dir;

	process = (ProcessPtr) FindTask(NULL);
/*
	Save current dir lock to restore later
	(Workbench requires us to be at the same lock on exit)
*/
	dir = DupLock(process->pr_CurrentDir);
	initDir = CurrentDir(dir);
/*
	Save process window pointer to restore later
*/
	initProcWindow = process->pr_WindowPtr;
/*
	Open system libraries
*/
	IntuitionBase	= OpenLibrary("intuition.library", libVersion);
	GfxBase			= OpenLibrary("graphics.library", libVersion);
	LayersBase		= OpenLibrary("layers.library", libVersion);
	if (IntuitionBase == NULL || GfxBase == NULL || LayersBase == NULL) {
		InitError(_strInitErrors[INIT_BADSYS]);
		return (FALSE);
	}
	IconBase		= OpenLibrary("icon.library", libVersion);
	if (IconBase == NULL) {
		InitError(_strInitErrors[INIT_NOICON]);
		return (FALSE);
	}
	DiskfontBase	= OpenLibrary("diskfont.library", libVersion);
	if (DiskfontBase == NULL) {
		InitError(_strInitErrors[INIT_NOFONT]);
		return (FALSE);
	}
/*
	Open console device and get base vector
	Needed for ConvertKeyMsg() function call
*/
	if (OpenDevice("console.device", -1, (IOReqPtr) &consoleIOReq, 0)) {
		InitError(_strInitErrors[INIT_BADSYS]);
		return (FALSE);
	}
	ConsoleDevice = consoleIOReq.io_Device;
/*
	All done
*/
	return (TRUE);
}

/*
 *	Standard shut down
 *	Close libraries and devices opened in StdInit()
 */

void StdShutDown()
{
	Dir	dir;

	if (ConsoleDevice)
		CloseDevice((IOReqPtr) &consoleIOReq);
	if (DiskfontBase)
		CloseLibrary(DiskfontBase);
	if (IconBase)
		CloseLibrary(IconBase);
	if (LayersBase)
		CloseLibrary(LayersBase);
	if (GfxBase)
		CloseLibrary(GfxBase);
	if (IntuitionBase) {
		OpenWorkBench();
		CloseLibrary(IntuitionBase);
	}
	dir = CurrentDir(initDir);
	UnLock(dir);
}

/*
 *	Set window pointer for system requesters
 */

void SetSysRequestWindow(WindowPtr window)
{
	ProcessPtr	process;

	process = (ProcessPtr) FindTask(NULL);
	process->pr_WindowPtr = window;
}

/*
 *	Restore window pointer for system requesters
 */

void ClearSysRequestWindow()
{
	ProcessPtr	process;

	process = (ProcessPtr) FindTask(NULL);
	process->pr_WindowPtr = initProcWindow;
}
