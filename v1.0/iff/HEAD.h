/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	IFF Form HEAD structures and defines
 *	Copyright (c) 1987 New Horizons Software, Inc.
 *
 *	Permission is hereby granted to use this file in any and all
 *	applications.  Modifying the structures or defines included
 *	in this file is not permitted without written consent of
 *	New Horizons Software, Inc.
 */

#define ID_HEAD		MakeID('H','E','A','D')		/* Form type */

#define ID_PAGE		MakeID('P','A','G','E')		/* Chunks */
#define ID_OPTS		MakeID('O','P','T','S')
#define ID_TEXT		MakeID('T','E','X','T')
#define ID_NEST		MakeID('N','E','S','T')
#define ID_HSTL		MakeID('H','S','T','L')
#define ID_HEDR		MakeID('H','E','D','R')
#define ID_FOTR		MakeID('F','O','T','R')

/*
 *  PAGE - Page setup information
 *  This chunk can occur anywhere in the form
 *  Default values are taken from Preferences
 */

typedef struct {
    UBYTE	PageWidth;		/* Width in characters */
    UBYTE	PageHeight;		/* Height in lines */
    UBYTE	PitchCPI;		/* Pitch in characters per inch */
    UBYTE	SpacingLPI;		/* Spacing in lines per inch */
    UBYTE	LeftMargin;		/* Char's from left of page (0..) */
    UBYTE	RightMargin;		/* Char's from right of page (0..) */
    LONG	pad;
} FlowPageInfo;

/*
 *  OPTS - File options
 *  This chunk can occur anywhere in the form
 *  Default value is for IndentSpace = 5
 */

typedef struct {
    BYTE	IndentSpace;		/* Number of spaces per indent */
    BYTE	pad1;
    WORD	pad2;
} FileOptions;

/*
 *  TEXT - Heading text (one block per heading)
 *  Block is actual text, no need for separate structure
 *  If the heading is empty, this is an empty chunk -- there MUST be
 *  a TEXT block for every heading
 *  Note:  There must be no control characters in this chunk
 */

/*
 *  NEST - Depth of heading nesting
 *  This chunk appears whenever the nesting level changes
 *  The initial nesting level is assumed to be zero
 */

typedef struct {
    WORD	Nest;		/* The nesting level */
} NestLevel;

/*
 *  HSTL - Heading style
 *  This chunk appears whenever the heading style changes
 *  The style applies to the entire heading
 *  The initial style is assumed to be FS_NORMAL
 */

typedef struct {
    UBYTE	Style;		/* Style bits */
    UBYTE	pad;
} HeadStyle;

/*
 *  HEDR - Print header
 *  Block is actual text, no need for separate structure
 *  Note:  There must be no control characters in this chunk, currently the
 *	maximum size is 80 characters
 */

/*
 *  FOTR - Print footer
 *  Block is actual text, no need for separate structure
 *  Note:  There must be no control characters in this chunk, currently the
 *	maximum size is 80 characters
 */
