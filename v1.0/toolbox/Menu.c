/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Toolbox library
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Menu routines
 */

#define INTUI_V36_NAMES_ONLY	1

#include <exec/types.h>
#include <exec/memory.h>
#include <intuition/intuition.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/layers.h>
#include <proto/intuition.h>

#include <string.h>

#include "Memory.h"
#include "Menu.h"
#include "Utility.h"

/*
 *	External variables
 */

extern TextAttrPtr	_tbTextAttr;

/*
 *	Local variables and definitions
 */

static WORD	dashWidth;		/* Width of dash character in toolbox font */

/*
 *	Local routines prototypes
 */

void	GetDashWidth(void);
void	SetSubMarker(MenuItemPtr);
void	CalcItemSize(WindowPtr, MenuItemPtr, WORD *, WORD *);
void	SetItemSize(MenuItemPtr, WORD, WORD, WORD);
void	CalcMenuSubSize(WindowPtr, MenuPtr, MenuItemPtr);
void	CalcMenuSize(WindowPtr, MenuPtr);

/*
 *	Get the width of dash character in toolbox font
 */

static void	GetDashWidth()
{
	register IntuiTextPtr intuiText;

	intuiText = NewIntuiText(0, 0, "-", FS_NORMAL, 0);
	if (intuiText == NULL)
		dashWidth = 8;
	else {
		dashWidth = IntuiTextLength(intuiText);
		FreeIntuiText(intuiText);
	}
}

/*
 *	Set or remove submenu marker from menu item
 *	DashWidth must already have been obtained
 *	Note: Only text items can have sub markers
 */

static void SetSubMarker(MenuItemPtr menuItem)
{
	register IntuiTextPtr intuiText, nextText, prevText;

	if ((menuItem->Flags & ITEMTEXT) == 0)
		return;
	intuiText = (IntuiTextPtr) (menuItem->ItemFill);
	prevText = NULL;
	nextText = intuiText;
	while (nextText->NextText) {
		prevText = nextText;
		nextText = nextText->NextText;
	}
	if (prevText && *(nextText->IText) == '�') {
		if (menuItem->SubItem == NULL) {
			FreeIntuiText(nextText);
			prevText->NextText = NULL;
		}
		else								/* Menu width may have changed */
			nextText->LeftEdge = menuItem->Width - dashWidth;
		return;
	}
	if (menuItem->SubItem) {
		while (intuiText->NextText)
			intuiText = intuiText->NextText;
		intuiText->NextText =
			NewIntuiText(menuItem->Width - dashWidth, 1, "�", FS_NORMAL, 0);
	}
}

/*
 *	Calculate the width and height of menu item, and set item height
 *	DashWidth must already have been obtained
 */

static void	CalcItemSize(WindowPtr window, MenuItemPtr menuItem,
						 register WORD *width, register WORD *height)
{
	register TextPtr iText;
	register IntuiTextPtr intuiText;
	register ImagePtr image;

	if (menuItem->ItemFill == NULL)
		*width = *height = 0;
	else {
		if (menuItem->Flags & ITEMTEXT) {
			intuiText = (IntuiTextPtr) (menuItem->ItemFill);
			if (*(iText = intuiText->IText) == '-')
				*width = 0;
			else {
				if (intuiText->ITextFont == _tbTextAttr &&
					(_tbTextAttr->ta_Flags & FPF_PROPORTIONAL) == 0)
					*width = dashWidth*strlen(iText);
				else
					*width = IntuiTextLength(intuiText);
				*width += CHECKWIDTH + dashWidth;
				if (menuItem->SubItem)
					*width += dashWidth;
			}
			*height = intuiText->ITextFont->ta_YSize + 1;
			if (window->WScreen->ViewPort.Modes & LACE)
				(*height)++;
		}
		else {
			image = (ImagePtr) (menuItem->ItemFill);
			*width  = image->LeftEdge + image->Width;
			*height = image->TopEdge + image->Height;
		}
		if (menuItem->Flags & COMMSEQ)
			*width += COMMWIDTH + dashWidth;
		if (*height < 9)
			*height = 9;
	}
	menuItem->Height = *height;
}

/*
 *	Set the top edge, left edge, and width of menu item
 *	DashWidth must already have been obtained
 */

static void SetItemSize(MenuItemPtr menuItem, WORD topEdge, WORD leftEdge, WORD width)
{
	WORD numDash;
	register IntuiTextPtr intuiText;
	TextChar dashText[101];

	menuItem->TopEdge = topEdge;
	menuItem->LeftEdge = leftEdge;
	menuItem->Width	= width;
	if (menuItem->Flags & ITEMTEXT) {
		intuiText = (IntuiTextPtr) (menuItem->ItemFill);
		if (*(intuiText->IText) == '-') {
			numDash = width/dashWidth;
			if (numDash > 100)
				numDash = 100;
			setmem(dashText, numDash, '-');
			dashText[numDash] = '\0';
			(void) ChangeIntuiText(intuiText, dashText);
		}
		SetSubMarker(menuItem);
	}
}

/*
 *	CalcMenuSubSize
 *	Adjust the width, height, and location of the menu item's sub-items
 *	Note:  Assumes that DashWidth() has already been called
 */

static void CalcMenuSubSize(WindowPtr window, MenuPtr menu, MenuItemPtr menuItem)
{
	register WORD leftEdge, topEdge, maxWidth, totHeight;
	WORD width, height;				/* Need to take address of these */
	register MenuItemPtr subItem;
	ScreenPtr screen;

	screen = window->WScreen;
/*
	Find the width and height of the menu item list
*/
	maxWidth = totHeight = 0;
	for (subItem = menuItem->SubItem; subItem; subItem = subItem->NextItem) {
		CalcItemSize(window, subItem, &width, &height);
		if (width > maxWidth)
			maxWidth = width;
		totHeight += height;
	}
/*
	Adjust the top edge, left edge, and width of each menu sub-item
	Place sub-item menu on right side of menu
		If not enough room then place on left side
*/
	topEdge = 0;
	if (screen->BarHeight + menuItem->TopEdge + totHeight + 4 >= screen->Height)
		topEdge = screen->Height - screen->BarHeight - menuItem->TopEdge - totHeight - 4;
	leftEdge = menuItem->Width - dashWidth;
	if (menu->LeftEdge + menuItem->LeftEdge + leftEdge + maxWidth + 8 >= screen->Width)
		leftEdge = CHECKWIDTH - 8 - maxWidth;
	for (subItem = menuItem->SubItem; subItem; subItem = subItem->NextItem) {
		SetItemSize(subItem, topEdge, leftEdge, maxWidth);
		topEdge += subItem->Height;
	}
}

/*
 *	CalcMenuSize
 *	Adjust the width, height, and location of the menu's items
 */

static void CalcMenuSize(WindowPtr window, MenuPtr menu)
{
	register WORD leftEdge, topEdge, maxWidth, screenWidth;
	WORD width, height;				/* Need to take address of these */
	register MenuItemPtr menuItem;
	ScreenPtr screen;

	screen = window->WScreen;
	screenWidth = screen->Width - 8;
	GetDashWidth();
/*
	Find the width of the menu item list
*/
	maxWidth = 0;
	for (menuItem = menu->FirstItem; menuItem; menuItem = menuItem->NextItem) {
		CalcItemSize(window, menuItem, &width, &height);
		if (width > maxWidth)
			maxWidth = width;
	}
/*
	Adjust the tope edge, left edge, and width of each menu item
*/
	topEdge = 0;
	leftEdge = (menu->LeftEdge + maxWidth >= screenWidth) ?
			   screenWidth - menu->LeftEdge - maxWidth - 1 : 0;
	for (menuItem = menu->FirstItem; menuItem; menuItem = menuItem->NextItem) {
		SetItemSize(menuItem, topEdge, leftEdge, maxWidth);
		if (menuItem->SubItem) {
			CalcMenuSubSize(window, menu, menuItem);
		}
		topEdge += menuItem->Height;
	}
}

/*
 *	GetMenuStrip
 *	Process array of MenuTemplate definitions, return pointer to menu strip
 *	Last entry in list must have NULL entry for Menu Name
 *	Menu width and height are filled in when InsertMenuStrip is called
 *	Return NULL if error
 */

MenuPtr GetMenuStrip(register MenuTemplate menuList[])
{
	register WORD item;
	register MenuPtr menu, prevMenu, menuStrip;

	prevMenu = menuStrip = NULL;
	for (item = 0; menuList[item].Name; item++) {
		if ((menu = GetMenu(&menuList[item])) == NULL) {
			DisposeMenuStrip(menuStrip);
			return (NULL);
		}
		if (prevMenu == NULL)
			prevMenu = menuStrip = menu;
		else {
			prevMenu->NextMenu = menu;
			prevMenu = menu;
		}
	}
	return (menuStrip);
}

/*
 *	DisposeMenuStrip
 *	Release all memory allocated by GetMenuStrip
 */

void DisposeMenuStrip(register MenuPtr menuStrip)
{
	register MenuPtr menu;

	while (menu = menuStrip) {
		menuStrip = menuStrip->NextMenu;
		DisposeMenu(menu);
	}
}

/*
 *	InsertMenuStrip
 *	Set the given window's menu strip to the one specified
 */

void InsertMenuStrip(WindowPtr window, MenuPtr menuStrip)
{
	register WORD leftEdge;
	register MenuPtr menu;
	RastPtr rPort;

	rPort = &(window->WScreen->RastPort);
	ClearMenuStrip(window);				/* Clear any previous menu strip */
	leftEdge = 0;
	for (menu = menuStrip; menu; menu = menu->NextMenu) {
		menu->Width = TextLength(rPort, menu->MenuName, strlen(menu->MenuName)) + 8;
		menu->Height = rPort->Font->tf_YSize + 2;
		menu->LeftEdge = leftEdge;
		CalcMenuSize(window, menu);
		leftEdge += menu->Width;
	}
	SetMenuStrip(window, menuStrip);
}

/*
 *	GetMenu
 *	Process menu template and return pointer to menu
 *	Menu width and height are filled in when InsertMenu is called
 *	Return NULL if error
 */

MenuPtr GetMenu(register MenuTemplPtr menuTemplate)
{
	register MenuPtr menu;

	if ((menu = MemAlloc(sizeof(Menu), MEMF_CLEAR)) == NULL)
		return (NULL);
	menu->Flags = MENUENABLED;
	menu->MenuName = menuTemplate->Name;
	if (menuTemplate->Items &&
		(menu->FirstItem = GetMenuItems(menuTemplate->Items)) == NULL) {
		DisposeMenu(menu);
		return (NULL);
	}
	return (menu);
}

/*
 *	DisposeMenu
 *	Release all memory allocated by GetMenu
 */

void DisposeMenu(MenuPtr menu)
{
	if (menu) {
		DisposeMenuItems(menu->FirstItem);
		MemFree(menu, sizeof(Menu));
	}
}

/*
 *	InsertMenu
 *	Insert a menu into the menu strip of the specified window
 *		at the specified position
 *	If the menuNum is beyond the end of the list, the menu will be
 *		added to the end of the list
 */

void InsertMenu(WindowPtr window, register MenuPtr menu, register UWORD menuNum)
{
	register WORD leftEdge;
	register MenuPtr menuStrip, prevMenu;
	RastPtr rPort;

	rPort = &(window->WScreen->RastPort);
	menu->Width = TextLength(rPort, menu->MenuName, strlen(menu->MenuName)) + 8;
	menu->Height = rPort->Font->tf_YSize + 2;
	menu->NextMenu = NULL;
	leftEdge = 0;
	if ((menuStrip = window->MenuStrip) == NULL)
		menuStrip = menu;
	else {
		ClearMenuStrip(window);
		if (menuNum-- == 0) {
			menu->NextMenu = menuStrip;
			menuStrip = menu;
		}
		else {
			prevMenu = menuStrip;
			leftEdge = prevMenu->Width;
			while (menuNum-- && prevMenu->NextMenu) {
				prevMenu = prevMenu->NextMenu;
				leftEdge += prevMenu->Width;
			}
			menu->NextMenu = prevMenu->NextMenu;
			prevMenu->NextMenu = menu;
		}
	}
/*
	Adjust the position of new and following menus
*/
	while (menu) {
		menu->LeftEdge = leftEdge;
		CalcMenuSize(window, menu);
		leftEdge += menu->Width;
		menu = menu->NextMenu;
	}
	SetMenuStrip(window, menuStrip);
}

/*
 *	InsertSubMenu
 *	Attach submenu to given menu item
 *	Does not free any previous submenu
 */

void InsertSubMenu(WindowPtr window, UWORD item, MenuItemPtr subItems)
{
	MenuPtr menuStrip, menu;
	MenuItemPtr menuItem;

	item = (item & ~SHIFTSUB(NOSUB)) | SHIFTSUB(NOSUB);
	if ((menuStrip = window->MenuStrip) == NULL ||
		(menu = MenuAddress(menuStrip, MENUNUM(item))) == NULL ||
		(menuItem = ItemAddress(menuStrip, item)) == NULL)
		return;
/*
	The actual dirty work, so do it quickly
*/
	ClearMenuStrip(window);
	menuItem->SubItem = subItems;
	CalcMenuSize(window, menu);			/* Sub marker may change width */
	SetMenuStrip(window, menuStrip);
}

/*
 *	GetMenuItems
 *	Process menu items template and return pointer to menu item list
 *	Does not set LeftEdge, TopEdge, Width, or Height
 *		(These values are set when CalcMenuSize is called)
 *	Return NULL if error
 *
 *	If Type is MENU_TEXT_ITEM and Info is NULL, then put separating line
 *		in this item position
 */

MenuItemPtr GetMenuItems(register MenuItemTemplate itemTempl[])
{
	register WORD item;
	register UBYTE type, flags;
	register BYTE commKey;
	register Ptr info;
	MenuItemTemplPtr subItems;
	register MenuItemPtr menuItem, firstItem, prevItem;
	IntuiTextPtr intuiText;
	TextPtr text;

	firstItem = prevItem = NULL;
	for (item = 0; (type = itemTempl[item].Type) != MENU_NO_ITEM; item++) {
		if ((menuItem = MemAlloc(sizeof(MenuItem), MEMF_CLEAR)) == NULL) {
			DisposeMenuItems(firstItem);
			return (NULL);
		}
		if (firstItem == NULL)
			firstItem = menuItem;
		if (prevItem)
			prevItem->NextItem = menuItem;
		flags = itemTempl[item].Flags;
		info  = itemTempl[item].Info;
/*
	Set up menu item parameters
*/
		if (flags & MENU_ENABLED)
			menuItem->Flags  = ITEMENABLED;
		if (flags & MENU_CHECKABLE)
			menuItem->Flags |= CHECKIT;
		if (flags & MENU_CHECKED)
			menuItem->Flags |= CHECKIT | CHECKED;
		if (flags & MENU_TOGGLE)
			menuItem->Flags |= CHECKIT | MENUTOGGLE;
		if (type == MENU_TEXT_ITEM) {
			menuItem->Flags |= ITEMTEXT | HIGHCOMP;
			text = (TextPtr) info;
			if (text == NULL || *text == '-')
				intuiText = NewIntuiText(0, 1, "-", FS_NORMAL, 0);
			else
				intuiText = NewIntuiText(CHECKWIDTH, 1, text, itemTempl[item].Style, 0);
			if (intuiText == NULL) {
				DisposeMenuItems(firstItem);
				return (NULL);
			}
			menuItem->ItemFill = (APTR) intuiText;
		}
		else if (type == MENU_IMAGE_ITEM) {
			menuItem->Flags   |= HIGHBOX;
			menuItem->ItemFill = info;
		}
		if (commKey = itemTempl[item].CommKey) {
			menuItem->Flags |= COMMSEQ;
			menuItem->Command = commKey;
		}
		menuItem->MutualExclude = itemTempl[item].MutualExclude;
/*
	If sub-items, then create and link them in
*/
		if (subItems = itemTempl[item].SubItems) {
			if ((menuItem->SubItem = GetMenuItems(subItems)) == NULL) {
				DisposeMenuItems(firstItem);
				return (NULL);
			}
		}
		prevItem = menuItem;
	}
	return (firstItem);
}

/*
 *	DisposeMenuItems
 *	Release all memory allocated by GetMenuItems
 */

void DisposeMenuItems(register MenuItemPtr menuItem)
{
	register MenuItemPtr nextItem;
	register IntuiTextPtr intuiText;

	while (menuItem) {
		nextItem = menuItem->NextItem;
		if ((menuItem->Flags & ITEMTEXT) &&
			(intuiText = (IntuiTextPtr) menuItem->ItemFill)) {
			FreeIntuiText(intuiText);
		}
		if (menuItem->SubItem)
			DisposeMenuItems(menuItem->SubItem);
		MemFree(menuItem, sizeof(MenuItem));
		menuItem = nextItem;
	}
}

/*
 *	MenuAddress
 *	Return the address of the specified menu
 *	menuNum = 0 returns menuStrip, menuNum = 1 returns menuStrip->NextMenu, etc.
 */

MenuPtr MenuAddress(MenuPtr menuStrip, register UWORD menuNum)
{
	register MenuPtr menu;

	menu = menuStrip;
	while (menuNum-- && menu)
		menu = menu->NextMenu;
	return (menu);
}

/*
 *	SetMenuItem
 *	Change text of specified menu item to given text
 *	If item is not a text item, then do nothing
 *	(Note: does not use Clear/SetMenuStrip due to speed considerations)
 */

void SetMenuItem(WindowPtr window, UWORD item, TextPtr text)
{
	WORD oldLeftEdge, oldWidth, diffLeftEdge, diffWidth;
	register MenuPtr menuStrip, menu;
	register MenuItemPtr menuItem;
	register IntuiTextPtr intuiText;

	if ((menuStrip = window->MenuStrip) == NULL ||
		(menu = MenuAddress(menuStrip, MENUNUM(item))) == NULL ||
		(menuItem = ItemAddress(menuStrip, item)) == NULL ||
		(menuItem->Flags & ITEMTEXT) == 0 ||
		(intuiText = (IntuiTextPtr) menuItem->ItemFill) == NULL)
		return;
	if (text == NULL || *text == '-')
		text = "-";
	oldLeftEdge = menuItem->LeftEdge;
	oldWidth = menuItem->Width;
	diffLeftEdge = menu->JazzX - oldLeftEdge;
	diffWidth = menu->BeatX - oldWidth;
/*
	The actual dirty work, so do it quickly
	It would be better to wait on (menu->Flags & MIDRAWN), but that bit is sometimes
		set even when menus are not being shown
*/
	while (window->Flags & WFLG_MENUSTATE) ;	/* Wait until items are not shown */
	ChangeIntuiText(intuiText, text);
	CalcMenuSize(window, menu);
/*
	Do this way instead of ClearMenuStrip/SetMenuStrip for speed
*/
	if (menuItem->LeftEdge != oldLeftEdge)
		menu->JazzX = menuItem->LeftEdge + diffLeftEdge;
	if (menuItem->Width != oldWidth)
		menu->BeatX = menuItem->Width + diffWidth;
}

/*
 *	SetMenuItemFont
 *	Set font of menu item
 */

void SetMenuItemFont(WindowPtr window, UWORD item, TextAttrPtr textAttr)
{
	register MenuPtr menuStrip, menu;
	register MenuItemPtr menuItem;
	IntuiTextPtr intuiText;

	if ((menuStrip = window->MenuStrip) == NULL ||
		(menu = MenuAddress(menuStrip, MENUNUM(item))) == NULL ||
		(menuItem = ItemAddress(menuStrip, item)) == NULL ||
		(menuItem->Flags & ITEMTEXT) == 0)
		return;
/*
	The actual dirty work, so do it quickly
*/
	ClearMenuStrip(window);
	intuiText = (IntuiTextPtr) (menuItem->ItemFill);
	SetIntuiTextFont(intuiText, textAttr);
	CalcMenuSize(window, menu);
	SetMenuStrip(window, menuStrip);
}

/*
 *	CheckMenu
 *	Set or clear a check mark next to the specified menu item
 *	If checkIt = TRUE then check it, else un-check it
 *	(Note: not using Clear/SetMenuStrip() for speed considerations)
 */

void CheckMenu(WindowPtr window, UWORD item, BOOL checkIt)
{
	register MenuPtr menuStrip, menu;
	register MenuItemPtr menuItem;

	if ((menuStrip = window->MenuStrip) == NULL ||
		(menu = MenuAddress(menuStrip, MENUNUM(item))) == NULL ||
		(menuItem = ItemAddress(menuStrip, item)) == NULL)
		return;
/*
	The actual dirty work, so do it quickly
	It would be better to wait on (menu->Flags & MIDRAWN), but that bit is sometimes
		set even when menus are not being shown
*/
	while (window->Flags & WFLG_MENUSTATE) ;	/* Wait until items are not shown */
	if (checkIt)
		menuItem->Flags |= (CHECKIT | CHECKED);
	else
		menuItem->Flags &= ~CHECKED;
}

/*
 *	InsertMenuItem
 *	Insert menu item before given item
 *	If item is beyond end of list, append new text item to end of menu
 */

static MenuItemTemplate menuItemTempl[] = {
	{ MENU_TEXT_ITEM, MENU_ENABLED, 0, 0, 0, NULL, NULL },
	{ MENU_NO_ITEM, 0, 0, 0, 0, NULL, NULL }
};

void InsertMenuItem(WindowPtr window, UWORD item, TextPtr text)
{
	UWORD menuNum, itemNum, subNum;
	BOOL isFirstSub;
	MenuPtr menuStrip, menu;
	register MenuItemPtr menuItem, prevItem, newItem;

	menuNum = MENUNUM(item);
	itemNum = ITEMNUM(item);
	subNum = SUBNUM(item);
	if ((menuStrip = window->MenuStrip) == NULL ||
		(menu = MenuAddress(menuStrip, menuNum)) == NULL)
		return;
	menuItemTempl[0].Info = text;
	if ((newItem = GetMenuItems(menuItemTempl)) == NULL)
		return;
/*
	Find previous and following menu item
*/
	prevItem = NULL;
	menuItem = menu->FirstItem;
	isFirstSub = FALSE;
	while (menuItem && itemNum--) {
		prevItem = menuItem;
		menuItem = menuItem->NextItem;
	}
	if (subNum != NOSUB) {
		if (menuItem == NULL)
			return;
		isFirstSub = TRUE;
		prevItem = menuItem;
		menuItem = menuItem->SubItem;
		while (menuItem && subNum--) {
			isFirstSub = FALSE;
			prevItem = menuItem;
			menuItem = menuItem->NextItem;
		}
	}
/*
	The actual dirty work, so do it quickly
*/
	ClearMenuStrip(window);
	newItem->NextItem = menuItem;
	if (isFirstSub)
		prevItem->SubItem = newItem;
	else if (prevItem)
		prevItem->NextItem = newItem;
	else
		menu->FirstItem = newItem;
	CalcMenuSize(window, menu);
	SetMenuStrip(window, menuStrip);
}

/*
 *	DeleteMenuItem
 *	Remove menu item from menu and free memory it uses
 */

void DeleteMenuItem(WindowPtr window, UWORD item)
{
	register UWORD menuNum, itemNum, subNum;
	BOOL isFirstSub;
	MenuPtr menuStrip, menu;
	register MenuItemPtr menuItem, prevItem;

	menuNum = MENUNUM(item);
	itemNum = ITEMNUM(item);
	subNum = SUBNUM(item);
	if ((menuStrip = window->MenuStrip) == NULL ||
		(menu = MenuAddress(menuStrip, menuNum)) == NULL)
		return;
/*
	Find previous menu item
*/
	prevItem = NULL;
	menuItem = menu->FirstItem;
	isFirstSub = FALSE;
	while (itemNum--) {
		prevItem = menuItem;
		menuItem = menuItem->NextItem;
		if (menuItem == NULL)
			return;
	}
	if (menuItem->SubItem && subNum != NOSUB) {
		isFirstSub = TRUE;
		prevItem = menuItem;
		menuItem = menuItem->SubItem;
		while (subNum--) {
			isFirstSub = FALSE;
			prevItem = menuItem;
			menuItem = menuItem->NextItem;
			if (menuItem == NULL)
				return;
		}
	}
/*
	The actual dirty work, so do it quickly
*/
	ClearMenuStrip(window);
	if (isFirstSub)
		prevItem->SubItem = menuItem->NextItem;
	else if (prevItem)
		prevItem->NextItem = menuItem->NextItem;
	else
		menu->FirstItem = menuItem->NextItem;
	menuItem->NextItem = NULL;
	CalcMenuSize(window, menu);
	SetMenuStrip(window, menuStrip);
	DisposeMenuItems(menuItem);
}
