/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Toolbox
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	Font routines
 */

#ifndef TOOLBOX_FONT_H
#define TOOLBOX_FONT_H

#ifndef TYPEDEFS_H
#include <Typedefs.h>
#endif

/*
 *	Definitions
 */

#define FONT_MAXSIZE	250

typedef WORD	FontNum;	/* Signed, so -1 can be used as "no such font" */
typedef WORD	FontSize;	/* Signed, so -1 can be used as flag */

/*
 *	Prototypes
 */

FontNum		NumFonts(void);
FontNum		FontNumber(TextPtr);
void		GetFontName(FontNum, TextPtr);
BOOL		IsPostScriptFont(FontNum);
BOOL		IsOutlineFont(FontNum);

FontSize	FontScaleSize(FontNum, FontSize);
TextFontPtr	GetNumFont(FontNum, FontSize);

WORD		NumFontAttrs(void);
TextAttrPtr	FontAttrTable(void);

BOOL	InitFonts(void);
void	ShutDownFonts(void);

#endif
