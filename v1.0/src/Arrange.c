/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Arrange menu routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <proto/intuition.h>

#include <Toolbox/Dialog.h>
#include <Toolbox/Window.h>

#include "Draw.h"
#include "Proto.h"

/*
 *	External variables
 */

extern ScreenPtr	screen;
extern MsgPortPtr	mainMsgPort;

extern DlgTemplPtr	dlgList[];

/*
 *	Local variables and definitions
 */

enum {
	LEDGE_RADBTN = 2,
	LRCENTER_RADBTN,
	REDGE_RADBTN,
	LRNONE_RADBTN,
	TEDGE_RADBTN,
	TBCENTER_RADBTN,
	BEDGE_RADBTN,
	TBNONE_RADBTN
};

static WORD lrAlign = LEDGE_RADBTN;
static WORD tbAlign = TEDGE_RADBTN;

/*
 *	Local prototypes
 */

BOOL	DoMoveForward(WindowPtr);
BOOL	DoMoveToFront(WindowPtr);
BOOL	DoMoveBackward(WindowPtr);
BOOL	DoMoveToBack(WindowPtr);

BOOL	DoAlignToGrid(WindowPtr);
BOOL	DoAlign(WindowPtr);

BOOL	DoGroup(WindowPtr, BOOL);
BOOL	DoLock(WindowPtr, BOOL);

/*
 *	Move objects forward
 *	If multiple objects are selected, retain their relative ordering
 */

static BOOL DoMoveForward(WindowPtr window)
{
	BOOL success;
	DocObjPtr docObj, prevObj;
	DocLayerPtr currLayer;
	DocDataPtr docData = GetWRefCon(window);

	currLayer = CurrLayer(docData);
	docObj = TopObject(currLayer);
	success = FALSE;
	while (docObj != NULL) {
		prevObj = PrevObj(docObj);
		if (ObjectSelected(docObj)) {
			success = TRUE;
			ObjectForward(currLayer, docObj);
			InvalObjectRect(window, docObj);
		}
		docObj = prevObj;
	}
	DocModified(docData);
	return (success);
}

/*
 *	Move objects to front
 *	If multiple objects are selected, retain their relative ordering
 */

static BOOL DoMoveToFront(WindowPtr window)
{
	BOOL success;
	DocObjPtr docObj, nextObj, endObj;
	DocLayerPtr currLayer;
	DocDataPtr docData = GetWRefCon(window);

	currLayer = CurrLayer(docData);
	docObj = BottomObject(currLayer);
	endObj = TopObject(currLayer);
	success = FALSE;
	while (docObj) {
		nextObj = NextObj(docObj);
		if (ObjectSelected(docObj)) {
			success = TRUE;
			ObjectToFront(currLayer, docObj);
			InvalObjectRect(window, docObj);
		}
		if (docObj == endObj)
			break;
		docObj = nextObj;
	}
	DocModified(docData);
	return (success);
}

/*
 *	Move objects backward
 *	If multiple objects are selected, retain their relative ordering
 */

static BOOL DoMoveBackward(WindowPtr window)
{
	BOOL success;
	DocObjPtr docObj, nextObj;
	DocLayerPtr currLayer;
	DocDataPtr docData = GetWRefCon(window);

	currLayer = CurrLayer(docData);
	docObj = BottomObject(currLayer);
	success = FALSE;
	while (docObj != NULL) {
		nextObj = NextObj(docObj);
		if (ObjectSelected(docObj)) {
			success = TRUE;
			ObjectBackward(currLayer, docObj);
			InvalObjectRect(window, docObj);
		}
		docObj = nextObj;
	}
	DocModified(docData);
	return (success);
}

/*
 *	Move objects to back
 *	If multiple objects are selected, retain their relative ordering
 */

static BOOL DoMoveToBack(WindowPtr window)
{
	BOOL success;
	DocObjPtr docObj, prevObj, endObj;
	DocLayerPtr currLayer;
	DocDataPtr docData = GetWRefCon(window);

	currLayer = CurrLayer(docData);
	endObj = BottomObject(currLayer);
	docObj = TopObject(currLayer);
	success = FALSE;
	while (docObj) {
		prevObj = PrevObj(docObj);
		if (ObjectSelected(docObj)) {
			success = TRUE;
			ObjectToBack(currLayer, docObj);
			InvalObjectRect(window, docObj);
		}
		if (docObj == endObj)
			break;
		docObj = prevObj;
	}
	DocModified(docData);
	return (success);
}

/*
 *	Align objects to grid
 */

static BOOL DoAlignToGrid(WindowPtr window)
{
	WORD x, y;
	DocObjPtr docObj;
	DocDataPtr docData = GetWRefCon(window);
	Rectangle rect;

	if (ObjectsLocked(docData)) {
		Error(ERR_OBJ_LOCKED);
		return (FALSE);
	}
/*
	Get rectangle of all selected objects
*/
	if (!GetSelectRect(docData, &rect))
		return (FALSE);
/*
	Snap rect upper left to grid coordinate
*/
	x = rect.MinX;
	y = rect.MinY;
	SnapToGrid(docData, &x, &y);
	x -= rect.MinX;
	y -= rect.MinY;
	if (x == 0 && y == 0)
		return (TRUE);
/*
	Offset all selected objects to new location
*/
	HiliteSelectOff(window);
	for (docObj = FirstSelected(docData); docObj; docObj = NextSelected(docObj)) {
		InvalObjectRect(window, docObj);
		OffsetObject(docObj, x, y);
		AdjustToDocBounds(docData, &docObj->Frame);
		InvalObjectRect(window, docObj);
	}
	HiliteSelectOn(window);
	DocModified(docData);
	return (TRUE);
}

/*
 *	Align objects to each other
 */

static BOOL DoAlign(WindowPtr window)
{
	WORD item, newLRAlign, newTBAlign, dx, dy, a;
	BOOL done;
	DocObjPtr docObj;
	DocDataPtr docData = GetWRefCon(window);
	GadgetPtr gadgList;
	DialogPtr dlg;
	Rectangle rect;

	if (ObjectsLocked(docData)) {
		Error(ERR_OBJ_LOCKED);
		return (FALSE);
	}
/*
	Display align dialog
*/
	BeginWait();
	if ((dlg = GetDialog(dlgList[DLG_ALIGN], screen, mainMsgPort)) == NULL) {
		EndWait();
		Error(ERR_NO_MEM);
		return (FALSE);
	}
	OutlineOKButton(dlg);
	newLRAlign = lrAlign;
	newTBAlign = tbAlign;
	gadgList = GadgetItem(dlg->FirstGadget, 0);	/* First user gadget */
	SetGadgetItemValue(gadgList, newLRAlign, dlg, NULL, 1);
	SetGadgetItemValue(gadgList, newTBAlign, dlg, NULL, 1);
/*
	Handle dialog
*/
	done = FALSE;
	do {
		item = ModalDialog(mainMsgPort, dlg, DialogFilter);
		switch (item) {
		case OK_BUTTON:
		case CANCEL_BUTTON:
			done = TRUE;
			break;
		case LEDGE_RADBTN:
		case LRCENTER_RADBTN:
		case REDGE_RADBTN:
		case LRNONE_RADBTN:
			SetGadgetItemValue(gadgList, newLRAlign, dlg, NULL, 0);
			newLRAlign = item;
			SetGadgetItemValue(gadgList, newLRAlign, dlg, NULL, 1);
			break;
		case TEDGE_RADBTN:
		case TBCENTER_RADBTN:
		case BEDGE_RADBTN:
		case TBNONE_RADBTN:
			SetGadgetItemValue(gadgList, newTBAlign, dlg, NULL, 0);
			newTBAlign = item;
			SetGadgetItemValue(gadgList, newTBAlign, dlg, NULL, 1);
			break;
		}
	} while (!done);
	DisposeDialog(dlg);
	EndWait();
	if (item == CANCEL_BUTTON)
		return (FALSE);
	lrAlign = newLRAlign;
	tbAlign = newTBAlign;
/*
	Get rectangle of selected objects
*/
	if (!GetSelectRect(docData, &rect))
		return (FALSE);
/*
	Align objects
*/
	HiliteSelectOff(window);
	for (docObj = FirstSelected(docData); docObj; docObj = NextSelected(docObj)) {
		dx = rect.MinX - docObj->Frame.MinX;
		a = (rect.MaxX - rect.MinX) - (docObj->Frame.MaxX - docObj->Frame.MinX);
		if (lrAlign == LRCENTER_RADBTN)
			dx += a/2;
		else if (lrAlign == REDGE_RADBTN)
			dx += a;
		else if (lrAlign == LRNONE_RADBTN)
			dx = 0;
		dy = rect.MinY - docObj->Frame.MinY;
		a = (rect.MaxY - rect.MinY) - (docObj->Frame.MaxY - docObj->Frame.MinY);
		if (tbAlign == TBCENTER_RADBTN)
			dy += a/2;
		else if (tbAlign == BEDGE_RADBTN)
			dy += a;
		else if (tbAlign == TBNONE_RADBTN)
			dy = 0;
		if (dx == 0 && dy == 0)
			continue;
		InvalObjectRect(window, docObj);
		OffsetObject(docObj, dx, dy);
		InvalObjectRect(window, docObj);
			
	}
	HiliteSelectOn(window);
	DocModified(docData);
	return (TRUE);
}

/*
 *	Group or ungroup objects
 */

static BOOL DoGroup(WindowPtr window, BOOL group)
{
	BOOL success;
	DocObjPtr docObj, nextObj;
	GroupObjPtr groupObj;
	DocDataPtr docData = GetWRefCon(window);

	if (ObjectsLocked(docData)) {
		Error(ERR_OBJ_LOCKED);
		return (FALSE);
	}
	success = TRUE;
	HiliteSelectOff(window);
/*
	Making a group can change object depth arrangement, so we must re-draw it
*/
	if (group) {
		if ((groupObj = MakeGroup(docData)) == NULL)
			success = FALSE;
		else
			InvalObjectRect(window, (DocObjPtr) groupObj);
	}
/*
	Un-making a group does not change depth arrangement, so no need for re-draw
*/
	else {
		docObj = FirstSelected(docData);
		while (docObj) {
			nextObj = NextSelected(docObj);
			if (docObj->Type == TYPE_GROUP)
				UnMakeGroup(docData, (GroupObjPtr) docObj);
			docObj = nextObj;
		}
	}
	HiliteSelectOn(window);
	DocModified(docData);
	SetEditMenu();
	SetArrangeMenu();
	return (success);
}

/*
 *	Lock or unlock selected objects
 */

static BOOL DoLock(WindowPtr window, BOOL lock)
{
	BOOL success;
	register DocObjPtr docObj;
	DocDataPtr docData = GetWRefCon(window);

	success = FALSE;
	for (docObj = FirstSelected(docData); docObj; docObj = NextSelected(docObj)) {
		success = TRUE;
		if (lock)
			LockObject(docObj);
		else
			UnLockObject(docObj);
	}
	DocModified(docData);
	SetArrangeMenu();
	return (success);
}

/*
 *	Handle Arrange menu
 */

BOOL DoArrangeMenu(WindowPtr window, UWORD item, UWORD sub, UWORD modifier)
{
	BOOL success;

	if (!IsDocWindow(window))
		return (FALSE);
	UndoOff();
	success = FALSE;
	if ((modifier & SHIFTKEYS) && (modifier & CMDKEY)) {
		if (item == MOVEFORWARD_ITEM)
			item = MOVETOFRONT_ITEM;
		else if (item == MOVEBACKWARD_ITEM)
			item = MOVETOBACK_ITEM;
		else if (item == GROUP_ITEM)
			item = UNGROUP_ITEM;
		else if (item == LOCK_ITEM)
			item = UNLOCK_ITEM;
	}
	switch (item) {
	case MOVEFORWARD_ITEM:
		success = DoMoveForward(window);
		break;
	case MOVETOFRONT_ITEM:
		success = DoMoveToFront(window);
		break;
	case MOVEBACKWARD_ITEM:
		success = DoMoveBackward(window);
		break;
	case MOVETOBACK_ITEM:
		success = DoMoveToBack(window);
		break;
	case ALIGNTOGRID_ITEM:
		success = DoAlignToGrid(window);
		break;
	case ALIGNOBJECTS_ITEM:
		success = DoAlign(window);
		break;
	case GROUP_ITEM:
	case UNGROUP_ITEM:
		success = DoGroup(window, (BOOL) (item == GROUP_ITEM));
		break;
	case LOCK_ITEM:
	case UNLOCK_ITEM:
		DoLock(window, (BOOL) (item == LOCK_ITEM));
		break;
	}
	return (success);
}
