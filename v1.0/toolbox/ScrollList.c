/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Amiga Toolbox
 *	Copyright (c) 1990 New Horizons Software, Inc.
 *
 *	Scroll List handler
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include "ScrollList.h"
#include "Gadget.h"
#include "Utility.h"
#include "Graphics.h"

#include <string.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>

/*
 *	External variables
 */

extern TextAttrPtr	_tbTextAttr;

extern WORD	_tbBlackColor, _tbWhiteColor;
extern WORD	_tbDarkColor, _tbLightColor;

/*
 *	Local variables and definitions
 */

#define SHIFTKEYS	(IEQUALIFIER_LSHIFT | IEQUALIFIER_RSHIFT)

static ScrollListPtr	trackScrollList;	/* Scroll list for TrackGadget call back */

static TextChar	ellipsis[] = "...";

/*
	ScrollList flags
*/

#define SL_MULTISELECT	0x01
#define SL_DRAWON		0x10

/*
	Item flags
*/

#define STYLE_BITS		0x07
#define DISABLE_FLAG	0x10
#define SELECT_FLAG		0x20
#define	DUMMY_FLAG		0x80	/* Dummy to keep entry from starting with \0 */

/*
 *	Local prototypes
 */

WORD	NumVisibleItems(ScrollListPtr);
WORD	ItemHeight(ScrollListPtr);
void	DrawItem(ScrollListPtr, WORD);
void	ScrollToOffset(ScrollListPtr, WORD);
void	ScrollUp(WindowPtr, WORD);
void	ScrollDown(WindowPtr, WORD);
void	TrackSlider(WindowPtr, WORD);
void	AdjustToSlider(ScrollListPtr);
void	AdjustSlider(ScrollListPtr);
void	TrackListBox(MsgPortPtr, ScrollListPtr, UWORD, ULONG, ULONG);

/*
 *	Get number of items in viewable list
 */

static WORD NumVisibleItems(ScrollListPtr scrollList)
{
	return ((WORD) (scrollList->ListBox->Height/(scrollList->Font->ta_YSize + 1)));
}

/*
 *	Return height of each item in viewable list
 */

static WORD ItemHeight(ScrollListPtr scrollList)
{
	return ((WORD) (scrollList->ListBox->Height/NumVisibleItems(scrollList)));
}

/*
 *	Draw specified item in list box
 */

static void DrawItem(ScrollListPtr scrollList, WORD item)
{
	register TextPtr text;
	WORD top, left, bottom, right, len, width, ellipWidth, color;
	BOOL selected;
	RastPtr rPort;
	TextFontPtr font;

	item -= scrollList->Offset;
	if (item < 0 || item >= NumVisibleItems(scrollList))
		return;
	if (item < NumListItems(scrollList->List))
		text = GetListItem(scrollList->List, item + scrollList->Offset);
	else
		text = NULL;
	selected = (text && (*text & SELECT_FLAG));
/*
	Get bounds of item in port
*/
	rPort = (scrollList->Request) ?
			scrollList->Request->ReqLayer->rp : scrollList->Window->RPort;
	width = scrollList->ListBox->Width;
	top = scrollList->ListBox->TopEdge + item*ItemHeight(scrollList);
	left = scrollList->ListBox->LeftEdge;
	bottom = top + ItemHeight(scrollList) - 1;
	right = left + width - 1;
/*
	Clear previous item
*/
	SetDrMd(rPort, JAM1);
#ifdef NEW_LOOK
	color = (selected) ? _tbBlackColor : _tbLightColor;
#else
	color = (selected) ? ~_tbLightColor : _tbLightColor;
#endif
	SetAPen(rPort, color);
	RectFill(rPort, left, top, right, bottom);
/*
	Draw new item
*/
	if (text == NULL)
		return;
	font = GetFont(scrollList->Font);
	if (font == NULL)
		return;
	SetFont(rPort, font);
	SetSoftStyle(rPort, (*text & STYLE_BITS), 0xFF);
#ifdef NEW_LOOK
	color = (selected) ? _tbWhiteColor : _tbBlackColor;
#else
	color = (selected) ? ~_tbBlackColor : _tbBlackColor;
#endif
	SetAPen(rPort, color);
	Move(rPort, left + 1, top + font->tf_Baseline + 1);
	len = strlen(text + 1);
	if (TextLength(rPort, text + 1, len) <= width - 1)
		Text(rPort, text + 1, len);
	else {
		ellipWidth = TextLength(rPort, ellipsis, 3);
		while (len &&
			   TextLength(rPort, text + 1, len) + ellipWidth > width - 1)
			len--;
		Text(rPort, text + 1, len);
		Text(rPort, ellipsis, 3);
	}
	CloseFont(font);
	SetSoftStyle(rPort, FS_NORMAL, 0xFF);
}

/*
 *	Scroll to new offset
 */

static void ScrollToOffset(ScrollListPtr scrollList, WORD offset)
{
	register WORD i, start, end, num;
	WORD numItems, numVis, maxOffset;
	WORD xMin, xMax, yMin, yMax;
	GadgetPtr listBox;
	LayerPtr layer;

	numItems = NumListItems(scrollList->List);
	numVis = NumVisibleItems(scrollList);
	maxOffset = numItems - numVis;
	if (maxOffset < 0)
		maxOffset = 0;
	if (offset < 0)
		offset = 0;
	else if (offset > maxOffset)
		offset = maxOffset;
	if (offset == scrollList->Offset)
		return;
	if ((scrollList->Flags & SL_DRAWON) == 0) {
		scrollList->Offset = offset;
		return;
	}
/*
	Scroll list and redraw items
*/
	listBox = scrollList->ListBox;
	layer = (scrollList->Request) ?
			scrollList->Request->ReqLayer : scrollList->Window->WLayer;
	xMin = listBox->LeftEdge;
	yMin = listBox->TopEdge;
	xMax = xMin + listBox->Width - 1;
	yMax = yMin + listBox->Height - 1;
	num = offset - scrollList->Offset;
	if (num > numVis)
		num = numVis;
	else if (num < -numVis)
		num = -numVis;
	SetBPen(layer->rp, _tbLightColor);
	ScrollRect(layer, 0, (WORD) (num*ItemHeight(scrollList)),
			   xMin, yMin, xMax, yMax);
	scrollList->Offset = offset;
	if (num > 0) {
		start = numVis - num;
		end = numVis;
	}
	else {
		start = 0;
		end = -num;			/* num < 0 */
	}
	for (i = start; i < end; i++)
		DrawItem(scrollList, (WORD) (i + offset));
}

/*
 *	Scroll up
 *	Called by TrackGadget()
 */

static void ScrollUp(WindowPtr window, WORD gadgNum)
{
	ScrollToOffset(trackScrollList, (WORD) (trackScrollList->Offset - 1));
	AdjustSlider(trackScrollList);
}

/*
 *	Scroll down
 *	Called by TrackGadget()
 */

static void ScrollDown(WindowPtr window, WORD gadgNum)
{
	ScrollToOffset(trackScrollList, (WORD) (trackScrollList->Offset + 1));
	AdjustSlider(trackScrollList);
}

/*
 *	Track slider
 *	Called by TrackGadget()
 */

static void TrackSlider(WindowPtr window, WORD gadgNum)
{
	AdjustToSlider(trackScrollList);
}

/*
 *	Adjust offset to current scroll bar setting
 */

static void AdjustToSlider(ScrollListPtr scrollList)
{
	WORD numItems, numVis;
	LONG offset;
	PropInfoPtr propInfo;

	propInfo = (PropInfoPtr) scrollList->Slider->SpecialInfo;
	numItems = NumListItems(scrollList->List);
	numVis = NumVisibleItems(scrollList);
	if (numItems <= numVis)
		offset = 0;
	else {
		offset = (LONG) propInfo->VertPot*(numItems - numVis);
		offset = (offset + 0x7FFF)/0xFFFF;
	}
	ScrollToOffset(scrollList, (WORD) offset);
}

/*
 *	Adjust scroll bar setting
 */

static void AdjustSlider(ScrollListPtr scrollList)
{
	WORD numItems, numVis;
	LONG newPot, newBody;
	PropInfoPtr propInfo;

	numItems = NumListItems(scrollList->List);
	numVis = NumVisibleItems(scrollList);
	propInfo = (PropInfoPtr) scrollList->Slider->SpecialInfo;
	if (numItems > numVis) {
		newPot = (0xFFFFL*scrollList->Offset)/(numItems - numVis);
		newBody = 0xFFFFL*numVis/numItems;
	}
	else
		newPot = newBody = 0xFFFF;
	if (propInfo->VertPot != newPot || propInfo->VertBody != newBody)
		NewModifyProp(scrollList->Slider, scrollList->Window, scrollList->Request,
					  AUTOKNOB | FREEVERT, 0, newPot, 0xFFFF, newBody, 1);
}

/*
 *	Handle mouse down in list box
 */

static void TrackListBox(MsgPortPtr msgPort, ScrollListPtr scrollList,
						 UWORD modifier, ULONG seconds, ULONG micros)
{
	register WORD mouseX, mouseY;
	WORD i, item, selItem, prevItem, startItem, numItems;
	WORD firstItem, lastItem;
	BOOL select;
	GadgetPtr listBox;
	RequestPtr request;
	WindowPtr window;

	listBox = scrollList->ListBox;
	request = scrollList->Request;
	window = scrollList->Window;
/*
	If no shift key, then un-select all other items
*/
	numItems = NumListItems(scrollList->List);
	if ((modifier & SHIFTKEYS) == 0) {
		for (item = 0; item < numItems; item++)
			SLSelectItem(scrollList, item, FALSE);
	}
/*
	Drag-select new items
*/
	prevItem = startItem = -1;
	do {
		mouseX = window->MouseX - listBox->LeftEdge;
		mouseY = window->MouseY - listBox->TopEdge;
		if (request) {
			mouseX -= request->LeftEdge;
			mouseY -= request->TopEdge;
		}
		if (mouseX < 0 || mouseX >= listBox->Width)
			continue;
/*
	Find item under mouse
*/
		if (mouseY < 0)
			item = -1;
		else if (mouseY >= listBox->Height)
			item = NumVisibleItems(scrollList);
		else
			item = mouseY/ItemHeight(scrollList);
		item += scrollList->Offset;
		if (item < 0)
			selItem = 0;
		else if (item >= numItems)
			selItem = numItems - 1;
		else
			selItem = item;
/*
	If multi-select, make sure we select all items that we drag over
*/
		if (startItem == -1)
			startItem = selItem;
		if (prevItem == -1)
			prevItem = selItem;
		firstItem = lastItem = selItem;
		if (scrollList->Flags & SL_MULTISELECT) {
			if (firstItem > startItem)
				firstItem = startItem;
			if (firstItem > prevItem)
				firstItem = prevItem;
			if (lastItem < startItem)
				lastItem = startItem;
			if (lastItem < prevItem)
				lastItem = prevItem;
		}
		for (i = firstItem; i <= lastItem; i++) {
			select = (startItem <= selItem) ?
					 (i >= startItem && i <= selItem) :
					 (i >= selItem && i <= startItem);
			SLSelectItem(scrollList, i, select);
		}
		prevItem = selItem;
/*
	Auto-scroll the list
*/
		if (mouseY < 0)
			ScrollUp(window, GadgetNumber(scrollList->UpArrow));
		else if (mouseY >= listBox->Height)
			ScrollDown(window, GadgetNumber(scrollList->DownArrow));
	} while (WaitMouseUp(msgPort, window));
/*
	Check for double-click
*/
	if (item < 0 || item >= numItems)
		item = -1;
	scrollList->DoubleClick = 
		(item != -1 && item == scrollList->PrevItem &&
		 DoubleClick(scrollList->PrevSeconds, scrollList->PrevMicros, seconds, micros));
	scrollList->PrevItem = item;
	scrollList->PrevSeconds = seconds;
	scrollList->PrevMicros = micros;
}

/*
 *	NewScrollList
 *	Allocate a new scroll list
 *	After calling this routine, caller should set up values for:
 *		ListBox, UpArrow, DownArrow, Slider, Window, Request, and Font
 */

ScrollListPtr NewScrollList(BOOL multiSelect)
{
	register ScrollListPtr scrollList;

	scrollList = MemAlloc(sizeof(ScrollList), MEMF_CLEAR);
	if (scrollList == NULL)
		return (NULL);
	scrollList->List = CreateList();
	if (scrollList->List == NULL) {
		MemFree(scrollList, sizeof(ScrollList));
		return (NULL);
	}
	scrollList->Flags = SL_DRAWON;
	if (multiSelect)
		scrollList->Flags |= SL_MULTISELECT;
	return (scrollList);
}

/*
 *	InitScrollList
 *	Initialize scroll list
 *	Assumes gadget order is: list box, up arrow, down arrow, slider
 *	Will add to window's IDCMP: GADGETDOWN, GADGETUP, RAWKEY, INTUITICKS and MOUSEBUTTONS
 */

void InitScrollList(ScrollListPtr scrollList, GadgetPtr listBox, WindowPtr window, RequestPtr request)
{
	register GadgetPtr gadget;

	gadget = listBox;
	gadget->Flags = (gadget->Flags & ~GADGHIGHBITS) | GADGHNONE;
	gadget->Activation &= ~RELVERIFY;
	scrollList->ListBox = gadget;
	gadget = gadget->NextGadget;
	scrollList->UpArrow = gadget;
	gadget = gadget->NextGadget;
	scrollList->DownArrow = gadget;
	gadget = gadget->NextGadget;
	scrollList->Slider = gadget;
	scrollList->Window = window;
	scrollList->Request = request;
	scrollList->Font = _tbTextAttr;
	ModifyIDCMP(window, window->IDCMPFlags | (GADGETDOWN|GADGETUP|RAWKEY|INTUITICKS|MOUSEBUTTONS));
}

/*
 *	DisposeScrollList
 *	Free memory allocated by NewScrollList
 */

void DisposeScrollList(ScrollListPtr scrollList)
{
	if (scrollList) {
		if (scrollList->List)
			DisposeList(scrollList->List);
		MemFree(scrollList, sizeof(ScrollList));
	}
}

/*
 *	SLDoDraw
 *	Turn drawing on or off while adding to list
 */

void SLDoDraw(ScrollListPtr scrollList, BOOL drawOn)
{
	if (drawOn) {
		scrollList->Flags |= SL_DRAWON;
		SLDrawList(scrollList);
	}
	else
		scrollList->Flags &= ~SL_DRAWON;
}

/*
 *	SLDrawBorder
 *	Draw standard border around scroll list
 */

void SLDrawBorder(ScrollListPtr scrollList)
{
	register WORD xPos;
	register GadgetPtr gadget;
	register BorderPtr border;
	RastPtr rPort;

	rPort = (scrollList->Request) ?
			scrollList->Request->ReqLayer->rp : scrollList->Window->RPort;
	gadget = scrollList->ListBox;
#ifdef NEW_LOOK
	border = ShadowBoxBorder((WORD) (gadget->Width + IMAGE_ARROW_WIDTH + 1), gadget->Height,
							 -1, TRUE);
#else
	border = BoxBorder((WORD) (gadget->Width + IMAGE_ARROW_WIDTH + 1), gadget->Height,
					   _tbBlackColor, -1);
#endif
	DrawBorder(rPort, border, gadget->LeftEdge, gadget->TopEdge);
	FreeBorder(border);
	xPos = gadget->LeftEdge + gadget->Width;
	SetAPen(rPort, _tbBlackColor);
	SetDrMd(rPort, JAM1);
	Move(rPort, xPos, gadget->TopEdge);
	Draw(rPort, xPos, (WORD) (gadget->TopEdge + gadget->Height - 1));
}

/*
 *	SLDrawList
 *	Draw viewable list
 */

void SLDrawList(ScrollListPtr scrollList)
{
	register WORD i, numVis;

	numVis = NumVisibleItems(scrollList);
	for (i = 0; i < numVis; i++)
		DrawItem(scrollList, (WORD) (i + scrollList->Offset));
	AdjustSlider(scrollList);
}

/*
 *	SLAddItem
 *	Add item to list before specified item
 */

void SLAddItem(ScrollListPtr scrollList, TextPtr text, WORD len, WORD before)
{
	TextPtr itemText;

	if ((itemText = MemAlloc(len + 2, 0)) == NULL)
		return;
	itemText[0] = DUMMY_FLAG;
	BlockMove(text, itemText + 1, len);
	itemText[len + 1] = '\0';
	(void) InsertListItem(scrollList->List, itemText, (WORD) (len + 1), before);
	MemFree(itemText, len + 2);
	if (scrollList->Flags & SL_DRAWON)
		SLDrawList(scrollList);
	scrollList->DoubleClick = FALSE;
	scrollList->PrevSeconds = scrollList->PrevMicros = 0;
}

/*
 *	SLRemoveItem
 *	Remove item from list
 */

void SLRemoveItem(ScrollListPtr scrollList, WORD item)
{
	RemoveListItem(scrollList->List, item);
	if (scrollList->Offset > 0 &&
		scrollList->Offset >= NumListItems(scrollList->List))
		scrollList->Offset--;
	if (scrollList->Flags & SL_DRAWON)
		SLDrawList(scrollList);
	scrollList->DoubleClick = FALSE;
	scrollList->PrevSeconds = scrollList->PrevMicros = 0;
}

/*
 *	SLRemoveAll
 *	Remove all items from list
 */

void SLRemoveAll(ScrollListPtr scrollList)
{
	register WORD numItems;

	numItems = NumListItems(scrollList->List);
	while (numItems--)
		RemoveListItem(scrollList->List, 0);
	scrollList->Offset = 0;
	scrollList->DoubleClick = FALSE;
	scrollList->PrevSeconds = scrollList->PrevMicros = 0;
	if (scrollList->Flags & SL_DRAWON)
		SLDrawList(scrollList);
}

/*
 *	SLNumItems
 *	Return number of items in list
 */

WORD SLNumItems(ScrollListPtr scrollList)
{
	return ((WORD) NumListItems(scrollList->List));
}

/*
 *	SLGetItem
 *	Return item text in buffer
 */

void SLGetItem(ScrollListPtr scrollList, WORD item, TextPtr buff)
{
	TextPtr text;

	text = GetListItem(scrollList->List, item);
	if (text)
		strcpy(buff, text + 1);
	else
		*buff = '\0';
}

/*
 *	SLNextSelect
 *	Return item number of next item selected after given one
 *	If item given is -1 then return first selected item
 *	Return -1 if no more items selected
 */

WORD SLNextSelect(ScrollListPtr scrollList, WORD after)
{
	register WORD i;
	register ListItemPtr listItem;

	for (i = 0, listItem = scrollList->List->First; i < after + 1 && listItem;
		 i++, listItem = listItem->Next) ;
	for (; listItem; i++, listItem = listItem->Next) {
		if (*(listItem->Text) & SELECT_FLAG)
			return (i);
	}
	return (-1);
}

/*
 *	SLIsSelected
 *	Return TRUE if specified item is selected
 */

BOOL SLIsSelected(ScrollListPtr scrollList, WORD item)
{
	TextPtr text;

	if (item < 0 || item >= NumListItems(scrollList->List))
		return (FALSE);
	text = GetListItem(scrollList->List, item);
	if (*text & SELECT_FLAG)
		return (TRUE);
	return (FALSE);
}

/*
 *	SLSetItemStyle
 *	Set style of specified item
 */

void SLSetItemStyle(ScrollListPtr scrollList, WORD item, WORD style)
{
	register TextPtr text;

	if (item < 0 || item >= NumListItems(scrollList->List))
		return;
	text = GetListItem(scrollList->List, item);
	*text &= ~STYLE_BITS;
	*text |= style;
	if (scrollList->Flags & SL_DRAWON)
		DrawItem(scrollList, item);
}

/*
 *	SLEnableItem
 *	Enable or disable item
 */

void SLEnableItem(ScrollListPtr scrollList, WORD item, BOOL enable)
{
	register TextPtr text;

	if (item < 0 || item >= NumListItems(scrollList->List))
		return;
	text = GetListItem(scrollList->List, item);
	if (enable)
		*text &= ~DISABLE_FLAG;
	else {
		*text |= DISABLE_FLAG;
		*text &= ~SELECT_FLAG;
	}
	if (scrollList->Flags & SL_DRAWON)
		DrawItem(scrollList, item);
}

/*
 *	SLSelectItem
 *	Select or deselect specified item in list
 */

void SLSelectItem(ScrollListPtr scrollList, WORD item, BOOL select)
{
	register WORD i, numItems;
	BOOL isSelected;
	register TextPtr text;
	register ListItemPtr listItem;

	numItems = NumListItems(scrollList->List);
	if (item < 0 || item >= numItems)
		return;
/*
	If selecting and not multi-select, then deselect everything else
*/
	if (select && (scrollList->Flags & SL_MULTISELECT) == 0) {
		for (i = 0, listItem = scrollList->List->First; i < numItems;
			 i++, listItem = listItem->Next) {
			if (i != item && (text = listItem->Text) != NULL &&
				(*text & SELECT_FLAG)) {
				*text &= ~SELECT_FLAG;
				if (scrollList->Flags & SL_DRAWON)
					DrawItem(scrollList, i);
			}
		}
	}
/*
	Select or deselect specified item
*/
	text = GetListItem(scrollList->List, item);
	isSelected = ((*text & SELECT_FLAG) != 0);
	if (*text & DISABLE_FLAG)
		select = FALSE;
	if ((select && !isSelected) || (!select && isSelected)) {
		if (select)
			*text |= SELECT_FLAG;
		else
			*text &= ~SELECT_FLAG;
		if (scrollList->Flags & SL_DRAWON)
			DrawItem(scrollList, item);
	}
}

/*
 *	SLGadgetMessage
 *	Handle gadget down/up in scroll list gadget
 *	This routine will reply to the message
 */

void SLGadgetMessage(ScrollListPtr scrollList, MsgPortPtr msgPort,
					 IntuiMsgPtr intuiMsg)
{
	register GadgetPtr gadget = (GadgetPtr) intuiMsg->IAddress;
	ULONG class = intuiMsg->Class;
	ULONG seconds = intuiMsg->Seconds;
	ULONG micros = intuiMsg->Micros;
	UWORD modifier = intuiMsg->Qualifier;
	WindowPtr window = intuiMsg->IDCMPWindow;
	
	ReplyMsg((MsgPtr) intuiMsg);
	if (window != scrollList->Window)
		return;
	switch (class) {
	case GADGETDOWN:
		trackScrollList = scrollList;
		if (gadget == scrollList->UpArrow)
			TrackGadget(msgPort, scrollList->Window, gadget, ScrollUp);
		else if (gadget == scrollList->DownArrow)
			TrackGadget(msgPort, scrollList->Window, gadget, ScrollDown);
		else if (gadget == scrollList->Slider)
			TrackGadget(msgPort, scrollList->Window, gadget, TrackSlider);
		else if (gadget == scrollList->ListBox)
			TrackListBox(msgPort, scrollList, modifier, seconds, micros);
		break;
	case GADGETUP:
		if (gadget == scrollList->Slider) {
			AdjustToSlider(scrollList);
			AdjustSlider(scrollList);
		}
		break;
	}
}

/*
 *	SLDoubleClick
 *	Return TRUE if last item selected was double-clicked on
 */

BOOL SLDoubleClick(ScrollListPtr scrollList)
{
	return (scrollList->DoubleClick);
}

/*
 *	SLAutoScroll
 *	Scroll list to make item visible
 */

void SLAutoScroll(ScrollListPtr scrollList, WORD item)
{
	WORD offset, numVis;

	if (item < 0 || item >= NumListItems(scrollList->List))
		return;
	numVis = NumVisibleItems(scrollList);
	if (item >= scrollList->Offset && item < scrollList->Offset + numVis)
		return;
	offset = (item < scrollList->Offset) ? item : item - numVis + 1;
	ScrollToOffset(scrollList, offset);
	AdjustSlider(scrollList);
}
