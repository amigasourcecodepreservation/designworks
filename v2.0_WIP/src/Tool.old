/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Tool window routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>
#include <proto/dos.h>			// For Delay() prototype

#include <string.h>

#include <Toolbox/Globals.h>
#include <Toolbox/Memory.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Border.h>
#include <Toolbox/Window.h>
#include <Toolbox/Utility.h>

#include "Draw.h"
#include "Proto.h"

/*
 *	External variables
 */

extern ScreenPtr	screen;
extern MsgPort		monitorMsgPort;
extern MsgPortPtr	mainMsgPort;

extern WindowPtr	toolWindow, penWindow, fillWindow;
extern WindowPtr	cmdWindow, windowList[];
extern WORD			numWindows;

extern Palette	screenPalette;

extern TextFontPtr	smallFont;

extern WORD		drawTool;		// Current draw tool
extern Defaults	defaults;		// Current pen color and fill pattern
extern RGBColor	penColors[];
extern RGBPat8	fillPats[];

extern Options	options;

extern Image	arrowIcon, textIcon, lineIcon, hvLineIcon;
extern Image	rectIcon, ovalIcon, curveIcon, polyIcon;

extern TextChar	strToolTitle[], strPenTitle[], strFillTitle[], strNone[];
extern TextChar	strScreenTitle[];

/*
 *	Local variables and definitions
 */

static struct NewWindow newExtraWindow = {
	0, 0, 640, 400, -1, -1,
	MOUSEBUTTONS | GADGETDOWN | GADGETUP | CLOSEWINDOW | MENUVERIFY | INTUITICKS,
	WINDOWCLOSE | WINDOWDRAG | WINDOWDEPTH | SMART_REFRESH,
	NULL, NULL, NULL, NULL, NULL,
	0, 0, 0xFFFF, 0xFFFF,
	CUSTOMSCREEN
};

#define LEFT_EDGE(i, w)		((i)*((w) + 1))
#define TOP_EDGE(i, h)		((i)*((h) + 1))
#define RIGHT_EDGE(i, w)	(LEFT_EDGE(i, w) + (w) - 1)
#define BOTTOM_EDGE(i, h)	(TOP_EDGE(i, h) + (h) - 1)

#define CELL_LEFT(x, w)	((x)/((w) + 1))
#define CELL_TOP(y, h)	((y)/((h) + 1))

typedef struct {
	WORD		Left, Top;	/* Index of cell */
} CellLoc;

/*
	Tool window parameters
*/

static WORD toolWindowLeft = -1;
static WORD toolWindowTop = -1;

static WORD	toolCellWidth, toolCellHeight;

#define TOOL_WIDTH	32		/* Values for hires-interlace screen */
#define TOOL_HEIGHT	20

typedef struct {
	WORD		Left, Top;	/* Index of cell */
	ImagePtr	Icon;
} ToolEntry;

#define NUM_TOOLS	(sizeof(tools)/sizeof(ToolEntry))

static ToolEntry tools[] = {	/* Same order as TOOL defines */
	{ 0, 0, &arrowIcon },
	{ 1, 0, &textIcon },
	{ 2, 0, &lineIcon },
	{ 3, 0, &hvLineIcon },
	{ 0, 1, &rectIcon },
	{ 1, 1, &ovalIcon },
	{ 2, 1, &curveIcon },
	{ 3, 1, &polyIcon }
};

/*
	Pen window parameters
*/

static WORD penWindowLeft = -1;
static WORD penWindowTop = -1;

static WORD	penCellWidth, penCellHeight;

#define PEN_WIDTH	20		/* Values for hires-interlace screen */
#define PEN_HEIGHT	16

static CellLoc penCells[] = {
	{ 4, 0 },	{ 5, 0 },
	{ 0, 1 },	{ 1, 1 },	{ 2, 1 },	{ 3, 1 },	{ 4, 1 },	{ 5, 1 },
};

#define PEN_NONE_LEFT	2
#define PEN_NONE_TOP	0

#define PEN_NONE		-1
#define PEN_CURRENT		-2
#define PEN_NOCHANGE	-3

/*
	Fill window parameters
*/

static WORD fillWindowLeft = -1;
static WORD fillWindowTop = -1;

static WORD	fillCellWidth, fillCellHeight;

#define FILL_WIDTH	20		/* Values for hires-interlace screen */
#define FILL_HEIGHT	16

static CellLoc fillCells[NUM_FILLPATS] = {
	{ 4, 0 },	{ 5, 0 },
	{ 0, 1 },	{ 1, 1 },	{ 2, 1 },	{ 3, 1 },	{ 4, 1 },	{ 5, 1 },
	{ 0, 2 },	{ 1, 2 },	{ 2, 2 },	{ 3, 2 },	{ 4, 2 },	{ 5, 2 },
	{ 0, 3 },	{ 1, 3 },	{ 2, 3 },	{ 3, 3 },	{ 4, 3 },	{ 5, 3 },
};

#define FILL_NONE_LEFT	2
#define FILL_NONE_TOP	0

#define FILL_NONE		-1
#define FILL_CURRENT	-2
#define FILL_NOCHANGE	-3

/*
 *	Local prototypes
 */

static void	SetCellSizes(void);

static WORD	ToolWindowHeight(void);
static WORD	PenWindowHeight(void);
static WORD	FillWindowHeight(void);

static WindowPtr	OpenSpecialWindow(void);

static void	DrawToolCell(WORD, BOOL);
static void	DrawNoneText(WindowPtr, WORD, WORD, WORD, WORD);
static void	ShowDefaultPenColor(void);
static void	ShowDefaultFillPat(void);

static void	SetSelectPenFill(WindowPtr, WORD, WORD);

static void	BltIcon(RastPtr, ImagePtr, WORD, WORD);

/*
 *	Set tool, pen, and fill window defaults
 */

void SetToolDefaults()
{
	drawTool = TOOL_SELECT;
	DrawToolCell(drawTool, TRUE);
	ShowDefaultPenColor();
	ShowDefaultFillPat();
}

/*
 *	Bring tool windows to front if they are open
 */

void ToolToFront()
{
	if (toolWindow && LayerObscured(toolWindow->WLayer))
		WindowToFront(toolWindow);
	if (penWindow && LayerObscured(penWindow->WLayer))
		WindowToFront(penWindow);
	if (fillWindow && LayerObscured(fillWindow->WLayer))
		WindowToFront(fillWindow);
}

/*
 *	Set tool, pen, and fill window cell sizes
 */

static void SetCellSizes()
{
	toolCellHeight = TOOL_HEIGHT;
	toolCellWidth = TOOL_WIDTH;
	penCellHeight = PEN_HEIGHT;
	penCellWidth = PEN_WIDTH;
	fillCellWidth = FILL_WIDTH;
	fillCellHeight = FILL_HEIGHT;
	if ((screen->ViewPort.Modes & LACE) == 0) {
		toolCellHeight -= 5;
		penCellHeight -= 6;
		fillCellHeight -= 6;
	}
	if ((screen->ViewPort.Modes & HIRES) == 0) {
		toolCellWidth -= 8;
		penCellWidth -= 4;
		fillCellWidth -= 4;
	}
}

/*
 *	Return height of tool window
 */

static WORD ToolWindowHeight()
{
	return ((WORD) (BOTTOM_EDGE(1, toolCellHeight) + 1
					+ screen->WBorTop + screen->WBorBottom
					+ screen->Font->ta_YSize + 1));
}

/*
 *	Return height of pen window
 */

static WORD PenWindowHeight()
{
	return ((WORD) (BOTTOM_EDGE(1, penCellHeight) + 1
					+ screen->WBorTop + screen->WBorBottom
					+ screen->Font->ta_YSize + 1));
}

/*
 *	Return height of fill window
 */

static WORD FillWindowHeight()
{
	return ((WORD) (BOTTOM_EDGE(3, fillCellHeight) + 1
					+ screen->WBorTop + screen->WBorBottom
					+ screen->Font->ta_YSize + 1));
}

/*
 *	Open extra window with current parameters
 *	Also sets screen pointer for new window struct
 */

static WindowPtr OpenSpecialWindow()
{
	ULONG oldIDCMPFlags;
	WindowPtr window;

	newExtraWindow.Screen = screen;
	oldIDCMPFlags = newExtraWindow.IDCMPFlags;
	newExtraWindow.IDCMPFlags = 0;
	window = OpenWindow(&newExtraWindow);
	newExtraWindow.IDCMPFlags = oldIDCMPFlags;
	if (window) {
		window->UserPort = &monitorMsgPort;
		SetStdPointer(window, POINTER_ARROW);
		ModifyIDCMP(window, oldIDCMPFlags);
		SetWindowTitles(window, (TextPtr) -1, strScreenTitle);
		SetPalette(window->RPort, &screenPalette);
		SetWKind(window, WKIND_TOOL);
	}
	return (window);
}

/*
 *	Open tool window
 */

void OpenToolWindow()
{
	if (toolWindow)
		return;
/*
	Set window parameters
*/
	SetCellSizes();
	newExtraWindow.Width = RIGHT_EDGE(3, toolCellWidth) + 1
							+ screen->WBorLeft + screen->WBorRight;
	newExtraWindow.Height = ToolWindowHeight();
	newExtraWindow.LeftEdge = (toolWindowLeft == -1) ?
							  screen->Width - newExtraWindow.Width - 10 :
							  toolWindowLeft;
	newExtraWindow.TopEdge = (toolWindowTop == -1) ?
							 screen->BarHeight + 10 :
							 toolWindowTop;
	newExtraWindow.Title = strToolTitle;
/*
	Open the window
*/
	toolWindow = OpenSpecialWindow();
	if (toolWindow)
		DrawToolWindow();
}

/*
 *	Open pen window
 */

void OpenPenWindow()
{
	if (penWindow)
		return;
/*
	Set window parameters
*/
	SetCellSizes();
	newExtraWindow.Width = RIGHT_EDGE(5, penCellWidth) + 1
							+ screen->WBorLeft + screen->WBorRight;
	newExtraWindow.Height = PenWindowHeight();
	newExtraWindow.LeftEdge = (penWindowLeft == -1) ?
							  screen->Width - newExtraWindow.Width - 10 :
							  penWindowLeft;
	newExtraWindow.TopEdge = (penWindowTop == -1) ?
							 screen->BarHeight + ToolWindowHeight() + 15 :
							 penWindowTop;
	newExtraWindow.Title = strPenTitle;
/*
	Open the window
*/
	penWindow = OpenSpecialWindow();
	if (penWindow)
		DrawPenWindow();
}

/*
 *	Open fill window
 */

void OpenFillWindow()
{
	if (fillWindow)
		return;
/*
	Set window parameters
*/
	SetCellSizes();
	newExtraWindow.Width = RIGHT_EDGE(5, fillCellWidth) + 1
							+ screen->WBorLeft + screen->WBorRight;
	newExtraWindow.Height = FillWindowHeight();
	newExtraWindow.LeftEdge = (fillWindowLeft == -1) ?
							  screen->Width - newExtraWindow.Width - 10 :
							  fillWindowLeft;
	newExtraWindow.TopEdge = (fillWindowTop == -1) ?
							 screen->BarHeight + ToolWindowHeight() + PenWindowHeight() + 20 :
							 fillWindowTop;
	if (newExtraWindow.TopEdge + newExtraWindow.Height > screen->Height)
		newExtraWindow.TopEdge = screen->Height - newExtraWindow.Height;
	newExtraWindow.Title = strFillTitle;
/*
	Open the window
*/
	fillWindow = OpenSpecialWindow();
	if (fillWindow)
		DrawFillWindow();
}

/*
 *	Close tool window
 */

void CloseToolWindow()
{
	if (toolWindow == NULL)
		return;
	toolWindowLeft = toolWindow->LeftEdge;
	toolWindowTop = toolWindow->TopEdge;
	CloseWindowSafely(toolWindow, mainMsgPort);
	toolWindow = NULL;
	if (numWindows)
		Delay(5);		/* Wait until Intuition notices window is gone */
}

/*
 *	Close pen window
 */

void ClosePenWindow()
{
	if (penWindow == NULL)
		return;
	penWindowLeft = penWindow->LeftEdge;
	penWindowTop = penWindow->TopEdge;
	CloseWindowSafely(penWindow, mainMsgPort);
	penWindow = NULL;
	if (numWindows)
		Delay(5);		/* Wait until Intuition notices window is gone */
}

/*
 *	Close fill window
 */

void CloseFillWindow()
{
	if (fillWindow == NULL)
		return;
	fillWindowLeft = fillWindow->LeftEdge;
	fillWindowTop = fillWindow->TopEdge;
	CloseWindowSafely(fillWindow, mainMsgPort);
	fillWindow = NULL;
	if (numWindows)
		Delay(5);		/* Wait until Intuition notices window is gone */
}

/*
 *	Draw contents of tool window
 */

void DrawToolWindow()
{
	RastPtr rPort;
	register WORD i, x, y;
	Rectangle rect;

	if (toolWindow == NULL)
		return;
	rPort = toolWindow->RPort;
	SetDrMd(rPort, JAM1);
/*
	Clear the window
*/
	GetWindowRect(toolWindow, &rect);
	SetAPen(rPort, _tbPenLight);
	FillRect(rPort, &rect);
/*
	Draw lines
*/
	SetAPen(rPort, _tbPenLight);
	for (i = 0; i < 3; i++) {
		x = RIGHT_EDGE(i, toolCellWidth) + 1 + rect.MinX;
		Move(rPort, x, rect.MinY);
		Draw(rPort, x, rect.MaxY);
	}
	y = BOTTOM_EDGE(0, toolCellHeight) + 1 + rect.MinY;
	Move(rPort, rect.MinX, y);
	Draw(rPort, rect.MaxX, y);
/*
	Draw tool icons
*/
	if (drawTool > NUM_TOOLS)
		drawTool = TOOL_SELECT;
	for (i = 0; i < NUM_TOOLS; i++)
		DrawToolCell(i, (i == drawTool));
}

/*
 *	Draw the specified tool window cell, hilited or not
 */

static void DrawToolCell(WORD tool, BOOL on)
{
	UBYTE forePen, backPen;
	WORD iconX, iconY;
	RastPtr rPort;
	Rectangle rect;

	if (toolWindow == NULL)
		return;
	rPort = toolWindow->RPort;
	SetDrMd(rPort, JAM1);
/*
	Get fore and back colors
*/
	if (on) {
		forePen = _tbPenWhite;
		backPen = _tbPenDark;
	}
	else {
		forePen = _tbPenBlack;
		backPen = _tbPenLight;
	}
/*
	Draw tool cell
*/
	GetWindowRect(toolWindow, &rect);
	rect.MinX += LEFT_EDGE(tools[tool].Left, toolCellWidth);
	rect.MinY += TOP_EDGE(tools[tool].Top, toolCellHeight);
	rect.MaxX  = rect.MinX + toolCellWidth - 1;
	rect.MaxY  = rect.MinY + toolCellHeight - 1;
	SetAPen(rPort, backPen);
	FillRect(rPort, &rect);
	iconX = rect.MinX + (toolCellWidth - tools[tool].Icon->Width)/2;
	iconY = rect.MinY + (toolCellHeight - tools[tool].Icon->Height)/2;
	SetAPen(rPort, forePen);
	BltIcon(rPort, tools[tool].Icon, iconX, iconY);
/*
	Draw shadow
*/
	DrawShadowBox(rPort, &rect, 0, !on);
}

/*
 *	Draw "None" text in window at given cell
 */

static void DrawNoneText(WindowPtr window, WORD left, WORD top, WORD width, WORD height)
{
	WORD x, y, len;
	RastPtr rPort = window->RPort;
	Rectangle rect;

	GetWindowRect(window, &rect);
	SetFont(rPort, smallFont);
	len = strlen(strNone);
	x = LEFT_EDGE(left, width) + (width*2 - TextLength(rPort, strNone, len))/2
		+ rect.MinX + 1;
	y = TOP_EDGE(top, height) + (height - smallFont->tf_YSize)/2
		+ smallFont->tf_Baseline + rect.MinY;
	SetAPen(rPort, _tbPenBlack);
	SetDrMd(rPort, JAM1);
	Move(rPort, x, y);
	Text(rPort, strNone, len);
}

/*
 *	Draw pen window
 */

void DrawPenWindow()
{
	register WORD	i, x, y;
	RastPtr			rPort;
	Rectangle		windRect, rect;

	if (penWindow == NULL)
		return;
	rPort = penWindow->RPort;
	SetDrMd(rPort, JAM1);
/*
	Clear the window
*/
	GetWindowRect(penWindow, &windRect);
	SetAPen(rPort, _tbPenWhite);
	FillRect(rPort, &windRect);
/*
	Draw lines
*/
	SetAPen(rPort, _tbPenBlack);
	y = BOTTOM_EDGE(0, penCellHeight) + 1 + windRect.MinY;
	for (i = 0; i < 5; i++) {
		x = RIGHT_EDGE(i, penCellWidth) + 1 + windRect.MinX;
		Move(rPort, x, (i == 0 || i == 2) ? y : windRect.MinY);
		Draw(rPort, x, windRect.MaxY);
	}
	Move(rPort, windRect.MinX, y);
	Draw(rPort, windRect.MaxX, y);
/*
	Draw pen colors
*/
	for (i = 0; i < NUM_PENS; i++) {
		x = LEFT_EDGE(penCells[i].Left, penCellWidth) + windRect.MinX;
		y = TOP_EDGE(penCells[i].Top, penCellHeight) + windRect.MinY;
		RGBForeColor(rPort, penColors[i]);
		SetRect(&rect, x, y, x + penCellWidth - 1, y + penCellHeight - 1);
		FillRect(rPort, &rect);
	}
	DrawNoneText(penWindow, 2, 0, penCellWidth, penCellHeight);
/*
	Show current pen color
*/
	ShowDefaultPenColor();
}

/*
 *	Show default pen color
 */

static void ShowDefaultPenColor()
{
	RGBColor penColor;
	RastPtr rPort;
	Rectangle rect;

	if (penWindow == NULL)
		return;
	rPort = penWindow->RPort;
	SetDrMd(rPort, JAM1);
	GetWindowRect(penWindow, &rect);
	rect.MaxX = rect.MinX + RIGHT_EDGE(1, penCellWidth);
	rect.MaxY = rect.MinY + BOTTOM_EDGE(0, penCellHeight);
	penColor = (defaults.ObjFlags & OBJ_DO_PEN) ? defaults.PenColor : RGBCOLOR_WHITE;
	InsetRect(&rect, 1, 1);
	DrawShadowBox(rPort, &rect, 0, FALSE);
	InsetRect(&rect, 1, 1);
	SetAPen(rPort, _tbPenDark);
	FrameRect(rPort, &rect);
	InsetRect(&rect, 1, 1);
	RGBForeColor(rPort, penColor);
	FillRect(rPort, &rect);
	if ((defaults.ObjFlags & OBJ_DO_PEN) == 0)
		DrawNoneText(penWindow, 0, 0, penCellWidth, penCellHeight);
}

/*
 *	Draw fill window
 */

void DrawFillWindow()
{
	RastPtr rPort;
	register WORD i, x, y;
	Rectangle rect, patRect;

	if (fillWindow == NULL)
		return;
	rPort = fillWindow->RPort;
	SetDrMd(rPort, JAM1);
/*
	Clear the window
*/
	GetWindowRect(fillWindow, &rect);
	SetAPen(rPort, _tbPenWhite);
	FillRect(rPort, &rect);
/*
	Draw lines
*/
	SetAPen(rPort, _tbPenBlack);
	y = BOTTOM_EDGE(0, fillCellHeight) + 1 + rect.MinY;
	for (i = 0; i < 5; i++) {
		x = RIGHT_EDGE(i, fillCellWidth) + 1 + rect.MinX;
		Move(rPort, x, (i == 0 || i == 2) ? y : rect.MinY);
		Draw(rPort, x, rect.MaxY);
	}
	for (i = 0; i < 3; i++) {
		y = BOTTOM_EDGE(i, fillCellHeight) + 1 + rect.MinY;
		Move(rPort, rect.MinX, y);
		Draw(rPort, rect.MaxX, y);
	}
/*
	Draw fill patterns
*/
	for (i = 0; i < NUM_FILLPATS; i++) {
		x = LEFT_EDGE(fillCells[i].Left, fillCellWidth) + rect.MinX;
		y = TOP_EDGE(fillCells[i].Top, fillCellHeight) + rect.MinY;
		SetRect(&patRect, x, y, x + fillCellWidth - 1, y + fillCellHeight - 1);
		FillRectNew(rPort, &patRect, &fillPats[i]);
	}
	DrawNoneText(fillWindow, 2, 0, fillCellWidth, fillCellHeight);
/*
	Show defaults fill pattern
*/
	ShowDefaultFillPat();
}

/*
 *	Show default fill pattern
 */

static void ShowDefaultFillPat()
{
	RastPtr rPort;
	Rectangle rect;

	if (fillWindow == NULL)
		return;
	rPort = fillWindow->RPort;
	SetDrMd(rPort, JAM1);
	GetWindowRect(fillWindow, &rect);
	rect.MaxX = rect.MinX + RIGHT_EDGE(1, penCellWidth);
	rect.MaxY = rect.MinY + BOTTOM_EDGE(0, penCellHeight);
	InsetRect(&rect, 1, 1);
	DrawShadowBox(rPort, &rect, 0, FALSE);
	InsetRect(&rect, 1, 1);
	SetAPen(rPort, _tbPenDark);
	FrameRect(rPort, &rect);
	InsetRect(&rect, 1, 1);
	if (defaults.ObjFlags & OBJ_DO_FILL)
		FillRectNew(rPort, &rect, &defaults.FillPat);
	else {
		RGBForeColor(rPort, RGBCOLOR_WHITE);
		FillRect(rPort, &rect);
		DrawNoneText(fillWindow, 0, 0, fillCellWidth, fillCellHeight);
	}
}

/*
 *	Set draw tool
 */

void SetDrawTool(WORD tool)
{
	if (tool < 0 || tool > NUM_TOOLS)
		return;
	if (tool != drawTool) {
		DrawToolCell(drawTool, FALSE);
		DrawToolCell(tool, TRUE);
		drawTool = tool;
	}
}

/*
 *	Handle mouse down in tool window
 */

void DoToolWindow(WORD mouseX, WORD mouseY)
{
	WORD i, left, top, tool;
	DocDataPtr docData;

/*
	Change the draw tool
*/
	mouseX -= toolWindow->BorderLeft;
	mouseY -= toolWindow->BorderTop;
	left = CELL_LEFT(mouseX, toolCellWidth);
	top = CELL_TOP(mouseY, toolCellHeight);
	for (tool = 0; tool <= NUM_TOOLS; tool++) {
		if (left == tools[tool].Left && top == tools[tool].Top)
			break;
	}
	SetDrawTool(tool);
/*
	Unselect all objects in all windows
*/
	for (i = 0; i < numWindows; i++) {
		docData = (DocDataPtr) GetWRefCon(windowList[i]);
		if (windowList[i] != cmdWindow || drawTool != TOOL_SELECT)
			UnSelectAllObjects(docData);
		if (windowList[i] == cmdWindow)
			TextPurge(docData);
	}
/*
	If last active doc window is still open, then activate it
*/
	if (IsDocWindow(cmdWindow))
		ActivateWindow(cmdWindow);
}

/*
 *	Set pen and fill pattern for selection
 */

static void SetSelectPenFill(WindowPtr window, WORD penNum, WORD fillNum)
{
	RGBColor penColor;
	RGBPat8Ptr fillPat;
	DocObjPtr docObj;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);

	if (ObjectsLocked(docData)) {
		Error(ERR_OBJ_LOCKED);
		return;
	}
	for (docObj = FirstSelected(docData); docObj; docObj = NextSelected(docObj)) {
		if (penNum != PEN_NOCHANGE) {
			if (penNum == PEN_NONE) {
				EnableObjectPen(docObj, FALSE);
				SetObjectPenColor(docObj, RGBCOLOR_WHITE);
			}
			else {
				penColor = (penNum == PEN_CURRENT) ?
						   defaults.PenColor : penColors[penNum];
				EnableObjectPen(docObj, TRUE);
				SetObjectPenColor(docObj, penColor);
			}
		}
		if (fillNum != FILL_NOCHANGE) {
			if (fillNum == FILL_NONE) {
				EnableObjectFill(docObj, FALSE);
				SetObjectFillPat(docObj, &fillPats[0]);
			}
			else {
				fillPat = (fillNum == FILL_CURRENT) ?
						  &defaults.FillPat : &fillPats[fillNum];
				EnableObjectFill(docObj, TRUE);
				SetObjectFillPat(docObj, fillPat);
			}
		}
		if (docObj->Type != TYPE_BMAP)
			InvalObjectRect(window, docObj);
		DocModified(docData);
	}
}

/*
 *	Wait for window to become active
 *	Should only be called after ActivateWindow()
 */

static void WaitWindowActive(WindowPtr window)
{
	BOOL isActive;
	IntuiMsgPtr intuiMsg;

	isActive = FALSE;
	for (;;) {
		while (!isActive && (intuiMsg = (IntuiMsgPtr) GetMsg(mainMsgPort)) != NULL) {
			if (intuiMsg->Class == ACTIVEWINDOW && intuiMsg->IDCMPWindow == window)
				isActive = TRUE;
			DoIntuiMessage(intuiMsg);
		}
		if (isActive)
			break;
		Wait(1 << mainMsgPort->mp_SigBit);
	}
}

/*
 *	Handle mouse down in pen window
 */

void DoPenWindow(WORD mouseX, WORD mouseY)
{
	WORD left, top, penNum;

	mouseX -= penWindow->BorderLeft;
	mouseY -= penWindow->BorderTop;
	left = CELL_LEFT(mouseX, penCellWidth);
	top = CELL_TOP(mouseY, penCellHeight);
	if (top == 0 && (left >= 0 && left <= 3))
		penNum = (left >= 2) ? PEN_NONE : PEN_CURRENT;
	else {
		for (penNum = 0; penNum < NUM_PENS; penNum++) {
			if (left == penCells[penNum].Left && top == penCells[penNum].Top)
				break;
		}
		if (penNum >= NUM_PENS)
			return;
	}
	if (penNum != PEN_CURRENT) {
		if (penNum == PEN_NONE) {
			defaults.PenColor = RGBCOLOR_WHITE;
			defaults.ObjFlags &= ~OBJ_DO_PEN;
		}
		else {
			defaults.PenColor = penColors[penNum];
			defaults.ObjFlags |= OBJ_DO_PEN;
		}
		ShowDefaultPenColor();
	}
/*
	If last active doc window is still open, then activate it
		and set pen for current selection
*/
	if (IsDocWindow(cmdWindow)) {
		ActivateWindow(cmdWindow);
		WaitWindowActive(cmdWindow);
		SetSelectPenFill(cmdWindow, penNum, FILL_NOCHANGE);
	}
}

/*
 *	Set to specified pen number (penNum 0 is "None")
 *	Invoked by AREXX macro
 */

void DoPenNum(WindowPtr window, WORD penNum)
{
	if (penNum == 0) {
		defaults.PenColor = RGBCOLOR_WHITE;
		defaults.ObjFlags &= ~OBJ_DO_PEN;
		penNum = PEN_NONE;
	}
	else if (penNum <= NUM_PENS) {
		defaults.PenColor = penColors[penNum - 1];
		defaults.ObjFlags |= OBJ_DO_PEN;
		penNum--;
	}
	else				/* penNum too big */
		return;
	ShowDefaultPenColor();
	if (IsDocWindow(window))
		SetSelectPenFill(window, penNum, FILL_NOCHANGE);
}

/*
 *	Handle mouse down in fill window
 */

void DoFillWindow(WORD mouseX, WORD mouseY)
{
	WORD left, top, fillNum;

	mouseX -= fillWindow->BorderLeft;
	mouseY -= fillWindow->BorderTop;
	left = CELL_LEFT(mouseX, fillCellWidth);
	top = CELL_TOP(mouseY, fillCellHeight);
	if (top == 0 && (left >= 0 && left <= 3))
		fillNum = (left >= 2) ? FILL_NONE : FILL_CURRENT;
	else {
		for (fillNum = 0; fillNum < NUM_FILLPATS; fillNum++) {
			if (left == fillCells[fillNum].Left && top == fillCells[fillNum].Top)
				break;
		}
		if (fillNum >= NUM_FILLPATS)
			return;
	}
	if (fillNum != FILL_CURRENT) {
		if (fillNum == FILL_NONE) {
			CopyRGBPat8(&fillPats[0], defaults.FillPat);
			defaults.ObjFlags &= ~OBJ_DO_FILL;
		}
		else {
			CopyRGBPat8(&fillPats[fillNum], defaults.FillPat);
			defaults.ObjFlags |= OBJ_DO_FILL;
		}
		ShowDefaultFillPat();
	}
/*
	If last active doc window is still open, then activate it
		and set fill for current selection
*/
	if (IsDocWindow(cmdWindow)) {
		ActivateWindow(cmdWindow);
		WaitWindowActive(cmdWindow);
		SetSelectPenFill(cmdWindow, PEN_NOCHANGE, fillNum);
	}
}

/*
 *	Set to specified fill number (fillNum 0 is "None")
 *	Invoked by AREXX macro
 */

void DoFillNum(WindowPtr window, WORD fillNum)
{
	if (fillNum == 0) {
		CopyRGBPat8(&fillPats[0], defaults.FillPat);
		defaults.ObjFlags &= ~OBJ_DO_FILL;
		fillNum = FILL_NONE;
	}
	else if (fillNum <= NUM_FILLPATS) {
		CopyRGBPat8(&fillPats[fillNum - 1], defaults.FillPat);
		defaults.ObjFlags |= OBJ_DO_FILL;
		fillNum--;
	}
	else				/* fillNum too big */
		return;
	ShowDefaultFillPat();
	if (IsDocWindow(window))
		SetSelectPenFill(cmdWindow, PEN_NOCHANGE, fillNum);
}

/*
 *	Blit single bit-plane image into rPort using current FgPen
 *	Note: This routine ignores all image data other than first bit plane
 *		  Also ignores linked list of images
 */

static void BltIcon(RastPtr rPort, register ImagePtr image, WORD left, WORD top)
{
	register WORD width, height, bytesPerRow;

	left += image->LeftEdge;
	top += image->TopEdge;
	width = image->Width;
	height = image->Height;
	bytesPerRow = ((width + 15) >> 3) & 0xFFFE;
	BltTemplate((PLANEPTR) image->ImageData, 0, bytesPerRow, rPort, left, top,
				width, height);
}
