/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Pen menu and Pen Color routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <proto/exec.h>
#include <proto/intuition.h>

#include <string.h>

#include <Toolbox/Window.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Utility.h>

#include "Draw.h"
#include "Proto.h"

/*
 *	External variables
 */

extern MsgPortPtr	mainMsgPort;
extern ScreenPtr	screen;

extern Defaults	defaults;

extern UBYTE	penSizes[];

extern TextChar	strPenHeight[], strPenWidth[], strNone[], strBlack[], strWhite[];

extern DlgTemplPtr	dlgList[];

extern PenColorPtr	penColors;

/*
 *	Local variables and definitions
 */

enum {
	PENSIZE_TEXT = 2,
	PENSIZE_PROMPT
};

/*
 *	Local prototypes
 */

static PenColorPtr	MakePenColor(TextPtr, RGBColor);

static void	SetPenSizeText(DialogPtr, WORD);
static BOOL	DoPenSize(WindowPtr, WORD, WORD);
static BOOL	DoArrows(WindowPtr, WORD, WORD);

/*
 *	return RGBColor value for given index
 */

RGBColor GetPenColor(WORD num)
{
	PenColorPtr	pen;

	for (num, pen = penColors; num > 0 && pen->Next; num--, pen = pen->Next);
	return (pen->Color);
}

/*
 *	return RGBColor value for given index
 */

void GetPenName(WORD num, TextPtr name)
{
	PenColorPtr	pen;

	for (num, pen = penColors; num > 0 && pen->Next; num--, pen = pen->Next);
	strcpy(name, pen->Name);
}

/*
 *	return pointer to first color excluding None, Black and White
 */

PenColorPtr FirstPenColor()
{
	PenColorPtr	pen;

	pen = penColors;	// None
	pen = pen->Next;	// White
	pen = pen->Next;	// Black

	return (pen->Next);	// first color after black
}

/*
 *	return number of pens in current pen list
 */

WORD NumPens()
{
	WORD 		num;
	PenColorPtr	pen;
/*
	ignore none, black, & white pencolor
*/
	num = 0;
	for (pen = FirstPenColor(); pen; pen = pen->Next)
		num++;

	return (num);
}

/*
 *	remove color from current list of pen colors
 */

void RemovePenColor(WORD colorNum)
{
	PenColorPtr	pen, prevPen;

	RemoveFromPenWindow(colorNum + 3);

	pen = FirstPenColor();
	prevPen = penColors->Next->Next;

	while (colorNum && pen) {
		pen = pen->Next;
		prevPen = prevPen->Next;
		colorNum--;
	}
	if (pen == NULL)
		return;
/*
	unlink and free pen
*/
	prevPen->Next = pen->Next;
	if (pen->Name)
		MemFree(pen->Name, strlen(pen->Name) + 1);
	MemFree(pen, sizeof(PenColor));
}

/*
 *	return pointer to penColor filled with color and name
 */

static PenColorPtr	MakePenColor(TextPtr name, RGBColor color)
{
	PenColorPtr	pen;

	if ((pen = MemAlloc(sizeof(PenColor), MEMF_CLEAR)) != NULL) {
		pen->Color 	= color;
		if ((pen->Name = MemAlloc(strlen(name) + 1, MEMF_CLEAR)) == NULL) {
			MemFree(pen, sizeof(PenColor));
			return (NULL);
		}
		strcpy(pen->Name, name);
	}
	return (pen);
}

/*
 *	add pen color to end of current list
 */

BOOL AddPenColor(TextPtr name, RGBColor color)
{
	PenColorPtr 	pen, prevPen;

	if ((pen = MakePenColor(name, color)) == NULL)
		return (FALSE);

	for (prevPen = FirstPenColor(); prevPen->Next; prevPen = prevPen->Next);
	prevPen->Next = pen;
	AddToPenWindow(name);

	return (TRUE);
}

/*
 *	initialize table of pencolors
 */

BOOL InitPenColors(WORD numPens, RGBColor colors[], TextPtr names[])
{
	WORD		i;
	PenColorPtr	pen, prevPen;
	BOOL		success;

	if (penColors)
		return (TRUE);
	success = FALSE;
/*
	add none, black, and white color
*/
	if ((pen = MakePenColor(strNone, RGBCOLOR_WHITE)) == NULL)
		goto Exit;
	penColors = pen;
	if ((pen->Next = MakePenColor(strWhite, RGBCOLOR_WHITE)) == NULL)
		goto Exit;
	pen = pen->Next;
	if ((pen->Next = MakePenColor(strBlack, RGBCOLOR_BLACK)) == NULL)
		goto Exit;

	prevPen = pen->Next;
	for (i = 0; i < numPens; i++) {
		if ((pen = MakePenColor(names[i], colors[i])) == NULL)
			goto Exit;
		prevPen->Next = pen;
		prevPen = pen;
	}
	success = TRUE;
Exit:
	if (!success)
		DisposePenColors();

	return (success);
}

/*
 *	dispose of pencolors
 */

void DisposePenColors()
{
	PenColorPtr	pen, nextPen;

	pen = penColors;
	while (pen) {
		nextPen = pen->Next;
		if (pen->Name)
			MemFree(pen->Name, strlen(pen->Name) + 1);
		MemFree(pen, sizeof(PenColor));
		pen = nextPen;
	}
	penColors = NULL;
}

/*
 *	Set defaults for pen menu from current selection
 *	Note: we only modify the pen menu, not pen colors or fill patterns
 *		when a selection is made
 */

void SetPenMenuDefaults(WindowPtr window)
{
	BOOL gotSize, gotFlags;
	WORD penWidth, penHeight;
	DocObjPtr docObj;
	DocDataPtr docData = GetWRefCon(window);

	gotSize = gotFlags = FALSE;
	for (docObj = FirstSelected(docData); docObj; docObj = NextSelected(docObj)) {
		if (!gotSize && GetObjectPenSize(docObj, &penWidth, &penHeight)) {
			defaults.PenWidth  = penWidth;
			defaults.PenHeight = penHeight;
			gotSize = TRUE;
		}
		if (!gotFlags && docObj->Type == TYPE_LINE) {
			defaults.LineFlags = ((LineObjPtr) docObj)->LineFlags;
			gotFlags = TRUE;
		}
		if (gotSize && gotFlags)
			break;
	}
	SetPenMenu();
}

/*
 *	Set pen size text
 */

static void SetPenSizeText(DialogPtr dlg, WORD size)
{
	TextChar sizeText[GADG_MAX_STRING];

	NumToString(size, sizeText);
	SetEditItemText(dlg->FirstGadget, PENSIZE_TEXT, dlg, NULL, sizeText);
}

/*
 *	Set pen size
 *	If size number is -1 then do not change,
 *	If size number >= NUM_PENSIZES then get value from user.
 */

static BOOL DoPenSize(WindowPtr window, WORD widthNum, WORD heightNum)
{
	BOOL changed, done;
	WORD item, size, penWidth, penHeight;
	TextPtr prompt;
	DocObjPtr docObj;
	DocDataPtr docData = GetWRefCon(window);
	DialogPtr dlg;
	GadgetPtr gadgList, gadget;
	TextChar sizeText[GADG_MAX_STRING];

/*
	If size number is "Other", get value from user
*/
	if (widthNum >= NUM_PENSIZES || heightNum >= NUM_PENSIZES) {
		BeginWait();
		if ((dlg = GetDialog(dlgList[DLG_OTHERPEN], screen, mainMsgPort)) == NULL) {
			EndWait();
			Error(ERR_NO_MEM);
			return (FALSE);
		}
		gadgList = dlg->FirstGadget;
		prompt = (widthNum >= NUM_PENSIZES) ? strPenWidth : strPenHeight;
		SetGadgetItemText(gadgList, PENSIZE_PROMPT, dlg, NULL, prompt);
		OutlineOKButton(dlg);
		size = (widthNum >= NUM_PENSIZES) ? defaults.PenWidth : defaults.PenHeight;
		SetPenSizeText(dlg, size);
		done = FALSE;
		do {
			WaitPort(mainMsgPort);
			item = CheckDialog(mainMsgPort, dlg, DialogFilter);
			GetEditItemText(gadgList, PENSIZE_TEXT, sizeText);
			switch(item) {
			case -1:					/* INTUITICKS message */
				EnableGadgetItem(gadgList, OK_BUTTON, dlg, NULL, (strlen(sizeText) != 0));
				break;
			case CANCEL_BUTTON:
				done = TRUE;
				break;
			case OK_BUTTON:
				size = StringToNum(sizeText);
				if (CheckNumber(sizeText) && size >= 0 && size <= MAX_PENSIZE) {
					done = TRUE;
					break;
				}
				Error(ERR_PEN_SIZE);
				if (size < 0)
					size = 0;
				else if (size > MAX_PENSIZE)
					size = MAX_PENSIZE;
				SetPenSizeText(dlg, size);
				gadget = GadgetItem(gadgList, PENSIZE_TEXT);
				ActivateGadget(gadget, dlg, NULL);
				break;
			}
		} while (!done);
		DisposeDialog(dlg);
		EndWait();
		if (item == CANCEL_BUTTON)
			return (FALSE);
	}
/*
	Set pen sizes
*/
	if (widthNum >= NUM_PENSIZES)
		penWidth = size;
	else if (widthNum >= 0)
		penWidth = penSizes[widthNum];
	else
		penWidth = -1;
	if (heightNum >= NUM_PENSIZES)
		penHeight = size;
	else if (heightNum >= 0)
		penHeight = penSizes[heightNum];
	else
		penHeight = -1;
/*
	Set defaults for new objects
*/
	if (penWidth != -1)
		defaults.PenWidth = penWidth;
	if (penHeight != -1)
		defaults.PenHeight = penHeight;
	SetPenMenu();
/*
	Modify selected objects
*/
	if (ObjectsLocked(docData)) {
		Error(ERR_OBJ_LOCKED);
		return (FALSE);
	}
	for (docObj = FirstSelected(docData); docObj; docObj = NextSelected(docObj)) {
		InvalObjectRect(window, docObj);	/* Obj rect size may change */
		SetObjectPenSize(docObj, penWidth, penHeight);
		InvalObjectRect(window, docObj);
		changed = TRUE;
	}
	if (changed)
		DocModified(docData);
	return (TRUE);
}

/*
 *	Set pen arrows
 *	1 = on, 0 = off, -1 = don't change
 */

static BOOL DoArrows(WindowPtr window, WORD atStart, WORD atEnd)
{
	BOOL changed;
	DocObjPtr docObj;
	LineObj lineObj;
	DocDataPtr docData = GetWRefCon(window);

/*
	Set defaults for new lines
*/
	lineObj.LineFlags = defaults.LineFlags;
	LineSetArrows(&lineObj, atStart, atEnd);
	defaults.LineFlags = lineObj.LineFlags;
	SetPenMenu();
/*
	Modify selected objects
*/
	if (ObjectsLocked(docData)) {
		Error(ERR_OBJ_LOCKED);
		return (FALSE);
	}
	for (docObj = FirstSelected(docData); docObj; docObj = NextSelected(docObj)) {
		if (docObj->Type == TYPE_LINE) {
			InvalObjectRect(window, docObj);	/* Obj rect may change size */
			LineSetArrows((LineObjPtr) docObj, atStart, atEnd);
			InvalObjectRect(window, docObj);
			changed = TRUE;
		}
	}
	if (changed);
		DocModified(docData);
	return (TRUE);
}

/*
 *	Handle Pen menu
 */

BOOL DoPenMenu(WindowPtr window, UWORD item, UWORD sub, UWORD modifier)
{
	BOOL success;

	if (!IsDocWindow(window))
		return (FALSE);
	UndoOff();
	success = FALSE;
	switch (item) {
	case WIDTH_ITEM:
		success = DoPenSize(window, sub, -1);
		break;
	case HEIGHT_ITEM:
		success = DoPenSize(window, -1, sub);
		break;
	case NOARROWS_ITEM:
		success = DoArrows(window, 0, 0);
		break;
	case ARROWSTART_ITEM:
		success = DoArrows(window, 1, -1);
		break;
	case ARROWEND_ITEM:
		success = DoArrows(window, -1, 1);
		break;
	}
	return (success);
}

