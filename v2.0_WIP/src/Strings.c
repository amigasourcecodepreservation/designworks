/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991-93 New Horizons Software, Inc.
 *
 *	Text strings
 */

#include <exec/types.h>

#include <TypeDefs.h>

#include "Version.h"

/*
 *	Screen title
 */

#ifdef DEMO
char strScreenTitle[] = " DesignWorks DEMO 2.0B1 - \251 1991-93 New Horizons Software, Inc.";
#else
char strScreenTitle[] = " DesignWorks 2.0B1 - \251 1991-93 New Horizons Software, Inc.";
#endif

/*
 *	Version string for AmigaDOS 2.0
 */

static char	version[] = "$VER: DesignWorks 2.0";

/*
 *	Error messages
 */

TextPtr initErrors[] = {
	"Can't open screen.",
	"Not enough memory."
};

TextPtr strsErrors[] = {
	" Unrecognized error type.",
	" Not enough memory.",
	" Unable to open file.",
	" Unable to save file.",
	" Not enough memory - saving as 'DesignWorks.Recover' on drive 0.",
	" Unable to print.",
	" Improper page size.",
	" Improper page number.",
	" Improper number of copies.",
	" No such page.",
	" Unable to get font.",
	" AREXX not present.",
	" Improper macro name.",
	" Macro execution failed.",
	" Object is locked.",
	" Improper pen size.",
	" Improper scale value.",
	" Unable to import picture.",
	" Unable to import EPS file.",
	" There is no visible layer.",
	" Cannot delete final layer.",
	" Video board not present\n or not set up.",
	" Unable to open clipboard.\n Full clipboard support\n is not available",
	" Can't find iffparse.library.\n Full clipboard support\n is not available",
	" Unable to read edition.",
	" Unable to write edition.",
	" Unable to obtain write lock. ",
	" HotLinks not present.\n Link to edition is not possible.",
	" Unable to update link. "
};

/*
 *	Misc text
 */

TextChar strDemo[]		= " Demo version.";
TextChar strBadFile[]	= " Bad file contents.";
TextChar strMemory[]	= " Getting low on memory -- please save your work.";

TextChar strDOSError[]		= " DOS error: 000";
TextChar strFileNotFound[]	= " File not found.";
TextChar strDiskLocked[]	= " Disk is locked.";
TextChar strFileNoDelete[]	= " File is delete protected.";
TextChar strFileNoRead[]	= " File is read protected.";

TextChar strUntitled[]		= "Untitled";
TextChar strOpenFile[]		= "Select a document to open";
TextChar strSaveAs[]		= "Save document as:";
TextChar strImportPict[]	= "Select a picture to import";
TextChar strExportPict[]	= "Export picture as:";
TextChar strImportEPSF[]	= "Select an EPS file to import";
TextChar strILBM[] 			= "ILBM";
TextChar strEPSF[]			= "EPSF";
TextChar strBytes[]			= " Bytes";
TextChar strTitle[]			= "Title: ";
TextChar strCreationDate[] 	= "Created: ";
TextChar strCreator[]		= "Created by: ";

TextChar strRAMName[]		= "RAM:";

TextChar strProgName[] 		= "DesignWorks";
TextChar strScreenName[] 	= "DesignWorks";
TextChar strPrefsName[]		= "DesignWorks Defaults";
TextChar strFKeysName[]		= "DesignWorks FKeys";
TextChar strAutoExecName[]	= "DesignWorks Startup";
TextChar strAppIconName[]	= "DesignWorks Deposit";
TextChar strDefaultDir[]	= "DesignWorks:";
TextChar strEmergencySave[]	= "DesignWorks.Recover";
TextChar strMacroDrawer[]	= "Macros/";
TextChar strSampleText[]	= "The quick brown fox jumps over the lazy dog.";

TextChar strPenHeight[]	= "Pen height:";	// For "Other size" dialogs
TextChar strPenWidth[]	= "Pen width:";

TextChar strCancel[]	= "Cancel";

TextChar strLayer[]		= "Layer ";
TextChar strToolTitle[]	= "Toolbox";
TextChar strPenTitle[]	= "Pen";
TextChar strFillTitle[]	= "Fill";

/*
 *	Default colors and patterns
 */

TextPtr	colorNames[] = {
	"Red", "Yellow", "Green", "Cyan",	"Blue", "Magenta",
	"Dark Red", "Dark Yellow", "Dark Green", "Dark Cyan", "Dark Blue", "Dark Magenta",
	"Light Gray", "Dark Gray"
};

TextPtr	patternNames[] = {
	"White", "Black", "Red", "Yellow", "Green", "Cyan",	"Blue", "Magenta",
	"Red/Black", "Yellow/Black", "Green/Black", "Cyan/Black", "Blue/Black", "Magenta/Black",
	"Red/White", "Yellow/White", "Green/White", "Cyan/White", "Blue/White", "Magenta/White"
};

TextChar strNone[]	= "None";
TextChar strBlack[] = "Black";
TextChar strWhite[] = "White";

TextChar strInch[]	= " in.";
TextChar strCM[]	= " cm";
TextChar strMM[]	= " mm";

TextChar strPrtSetup[]	= "Setting up...";		// For "Cancel print" dialog
TextChar strPrtRender[]	= "Rendering...";
TextChar strPrtPrint[]	= "Printing...";
TextChar strPrtCancel[]	= "Cancelling...";
TextChar strDspDisplay[]= "Displaying...";		// For "Display" dialog

TextChar strPenColor[]		= "Select a pen color";

TextChar strLoadDefaults[]		= "Select the settings to use";
TextChar strSaveAsDefaults[]	= "Save settings as:";

TextChar strBackupSuffix[] = ".old";

/*
 *	Default macro names
 */

TextChar	strMacroNames1[10][32];		// Not assigned

TextChar	strMacroNames2[10][32] = {
	"Macro_1", "Macro_2", "Macro_3", "Macro_4", "Macro_5",
	"Macro_6", "Macro_7", "Macro_8", "Macro_9", "Macro_10"
};
