/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Rectangle handling routines
 */

#include <exec/types.h>
#include <graphics/gfxmacros.h>
#include <intuition/intuition.h>

#include <proto/graphics.h>

#include <Toolbox/Memory.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Window.h>

#include "Draw.h"
#include "Proto.h"

/*
 *	External variables
 */

extern Defaults	defaults;

/*
 *	Local variables and definitions
 */

#define LINE_PAT	0xFFFF		/* Line pattern for object creation */

static RectObjPtr	growRectObj;
static BOOL			wasMoved;
static WORD			growHandle;

/*
 *	Local prototypes
 */

RectObjPtr	CreateRect(DocDataPtr, WORD, WORD, WORD, WORD);

void	RectTrackCreate(WindowPtr, WORD, WORD, WORD, WORD, BOOL);
void	RectConstrainCreate(WORD, WORD, WORD *, WORD *);

void	RectTrackGrow(WindowPtr, WORD, WORD, WORD, WORD, BOOL);

/*
 *	Allocate a new rect object
 */

RectObjPtr RectAllocate()
{
	return ((RectObjPtr) MemAlloc(sizeof(RectObj), MEMF_CLEAR));
}

/*
 *	Dispose of rect object
 */

void RectDispose(RectObjPtr rectObj)
{
	MemFree(rectObj, sizeof(RectObj));
}

/*
 *	Create rect with given start and end positions
 */

static RectObjPtr CreateRect(DocDataPtr docData, WORD xStart, WORD yStart, WORD xEnd, WORD yEnd)
{
	register RectObjPtr rectObj;

	if ((rectObj = (RectObjPtr) NewDocObject(CurrLayer(docData), TYPE_RECT)) == NULL)
		return (NULL);
	SetFrameRect(&rectObj->DocObj.Frame, xStart, yStart, xEnd, yEnd);
	rectObj->DocObj.Flags	= defaults.ObjFlags;
	rectObj->PenWidth		= defaults.PenWidth;
	rectObj->PenHeight		= defaults.PenHeight;
	rectObj->PenColor		= defaults.PenColor;
	CopyRGBPat8(&defaults.FillPat, &rectObj->FillPat);
	return (rectObj);
}

/*
 *	Draw rectangle object
 */

void RectDrawObj(RastPtr rPort, RectObjPtr rectObj, RectPtr rect, RectPtr clipRect)
{
	Point pen;
	Rectangle drawRect;

	if (HasFill(rectObj))
		FillRect(rPort, rect, &rectObj->FillPat);
	if (HasPen(rectObj)) {
		pen.x = rectObj->PenWidth;
		pen.y = rectObj->PenHeight;
		ScalePt(&pen, &rectObj->DocObj.Frame, rect);
		drawRect = *rect;
		OffsetRect(&drawRect, -pen.x/2, -pen.y/2);
		PenNormal(rPort);
		RGBForeColor(rPort, rectObj->PenColor);
		FrameRect(rPort, &drawRect, pen.x, pen.y);
	}
}

/*
 *	Draw outline of rect (for creation/dragging)
 *	Offset is in document coordinates
 */

void RectDrawOutline(WindowPtr window, RectObjPtr rectObj, WORD xOffset, WORD yOffset)
{
	RastPtr rPort = window->RPort;
	Rectangle rect;

	rect = rectObj->DocObj.Frame;
	OffsetRect(&rect, xOffset, yOffset);
	DocToWindowRect(window, &rect, &rect);
	SetDrMd(rPort, COMPLEMENT);
	SetDrPt(rPort, LINE_PAT);
	FrameRect(rPort, &rect, 1, 1);
}

/*
 *	Draw selection handles for rect
 */

void RectHilite(WindowPtr window, RectObjPtr rectObj)
{
	RastPtr rPort = window->RPort;
	Rectangle rect;

	DocToWindowRect(window, &rectObj->DocObj.Frame, &rect);
	DrawHandle(rPort, rect.MinX, rect.MinY);
	if (rect.MaxY != rect.MinY)
		DrawHandle(rPort, rect.MinX, rect.MaxY);
	if (rect.MaxX != rect.MinX)
		DrawHandle(rPort, rect.MaxX, rect.MinY);
	if (rect.MaxX != rect.MinX && rect.MaxY != rect.MinY)
		DrawHandle(rPort, rect.MaxX, rect.MaxY);
}

/*
 *	Set rect pen color
 */

void RectSetPenColor(RectObjPtr rectObj, RGBColor penColor)
{
	rectObj->PenColor = penColor;
}

/*
 *	Set rect pen size
 *	If size is -1 then do not change
 */

void RectSetPenSize(RectObjPtr rectObj, WORD penWidth, WORD penHeight)
{
	if (penWidth != -1)
		rectObj->PenWidth = penWidth;
	if (penHeight != -1)
		rectObj->PenHeight = penHeight;
}

/*
 *	Set rect fill pattern
 */

void RectSetFillPat(RectObjPtr rectObj, RGBPat8Ptr fillPat)
{
	CopyRGBPat8(fillPat, &rectObj->FillPat);
}

/*
 *	Rotate rect by given angle about center
 */

void RectRotate(RectObjPtr rectObj, WORD cx, WORD cy, WORD angle)
{
	RotateRect(&rectObj->DocObj.Frame, cx, cy, angle);
}

/*
 *	Flip rect horizontally or vertically about center
 */

void RectFlip(RectObjPtr rectObj, WORD cx, WORD cy, BOOL horiz)
{
	FlipRect(&rectObj->DocObj.Frame, cx, cy, horiz);
}

/*
 *	Scale rect to new frame
 */

void RectScale(RectObjPtr rectObj, RectPtr frame)
{
	rectObj->DocObj.Frame = *frame;
}

/*
 *	Create poly equivalent to given rect
 *	Returns pointer to poly, or NULL if error
 */

PolyObjPtr RectConvertToPoly(RectObjPtr rectObj)
{
	PolyObjPtr polyObj;
	Rectangle rect;

	if ((polyObj = (PolyObjPtr) NewDocObject(NULL, TYPE_POLY)) == NULL)
		return (NULL);
	rect = polyObj->DocObj.Frame = rectObj->DocObj.Frame;
	OffsetRect(&rect, -rect.MinX, -rect.MinY);
	if (!PolyAddPoint(polyObj, 0x7FFF, rect.MinX, rect.MinY) ||
		!PolyAddPoint(polyObj, 0x7FFF, rect.MaxX, rect.MinY) ||
		!PolyAddPoint(polyObj, 0x7FFF, rect.MaxX, rect.MaxY) ||
		!PolyAddPoint(polyObj, 0x7FFF, rect.MinX, rect.MaxY)) {
		DisposeDocObject((DocObjPtr) polyObj);
		return (NULL);
	}
	PolyAdjustFrame(polyObj);
	PolySetPenColor(polyObj, rectObj->PenColor);
	PolySetPenSize(polyObj, rectObj->PenWidth, rectObj->PenHeight);
	PolySetFillPat(polyObj, &rectObj->FillPat);
	polyObj->DocObj.Flags = rectObj->DocObj.Flags;
	PolySetSmooth(polyObj, FALSE);
	PolySetClosed(polyObj, TRUE);
	return (polyObj);
}

/*
 *	Determine if point is in rect
 *	Point is relative to object rectangle
 */

BOOL RectSelect(RectObjPtr rectObj, PointPtr pt)
{
	WORD maxX, maxY, slopX, slopY;

	if (HasFill(rectObj))
		return (TRUE);
	if (!HasPen(rectObj))
		return (FALSE);
	maxX = rectObj->DocObj.Frame.MaxX - rectObj->DocObj.Frame.MinX;
	maxY = rectObj->DocObj.Frame.MaxY - rectObj->DocObj.Frame.MinY;
	slopX = rectObj->PenWidth + 1;
	slopY = rectObj->PenHeight + 1;
	return ((pt->x < slopX || pt->y < slopY ||
			 pt->x > maxX - slopX || pt->y > maxY - slopY));
}

/*
 *	Return handle number of given point, or -1 if not in handle
 *	Point is relative to object rectangle
 */

WORD RectHandle(RectObjPtr rectObj, PointPtr pt, RectPtr handleRect)
{
	WORD maxX, maxY;
	Point handlePt;

	maxX = rectObj->DocObj.Frame.MaxX - rectObj->DocObj.Frame.MinX;
	maxY = rectObj->DocObj.Frame.MaxY - rectObj->DocObj.Frame.MinY;
	handlePt.x = handlePt.y = 0;
	if (InHandle(pt, &handlePt, handleRect))
		return (0);
	handlePt.x = maxX;
	if (InHandle(pt, &handlePt, handleRect))
		return (1);
	handlePt.x = 0;	handlePt.y = maxY;
	if (InHandle(pt, &handlePt, handleRect))
		return (2);
	handlePt.x = maxX;
	if (InHandle(pt, &handlePt, handleRect))
		return (3);
	return (-1);
}

/*
 *	Duplicate rect data to new object
 *	Return success status
 */

BOOL RectDupData(RectObjPtr rectObj, RectObjPtr newObj)
{
	newObj->PenWidth	= rectObj->PenWidth;
	newObj->PenHeight	= rectObj->PenHeight;
	newObj->PenColor	= rectObj->PenColor;
	CopyRGBPat8(&rectObj->FillPat, &newObj->FillPat);
	return (TRUE);
}

/*
 *	Draw routine for tracking rectangle creation
 */

static void RectTrackCreate(WindowPtr window, WORD xStart, WORD yStart, WORD xEnd, WORD yEnd,
							BOOL change)
{
	RectObj rectObj;

	if (change) {
		BlockClear(&rectObj, sizeof(RectObj));
		SetFrameRect(&rectObj.DocObj.Frame, xStart, yStart, xEnd, yEnd);
		RectDrawOutline(window, &rectObj, 0, 0);
	}
}

/*
 *	Constrain routine for rect creation
 */

static void RectConstrainCreate(WORD xStart, WORD yStart, WORD *xEnd, WORD *yEnd)
{
	WORD width, height, min;

	width = ABS(*xEnd - xStart);
	height = ABS(*yEnd - yStart);
	min = MIN(width, height);
	if (*xEnd >= xStart)
		*xEnd = xStart + min;
	else
		*xEnd = xStart - min;
	if (*yEnd >= yStart)
		*yEnd = yStart + min;
	else
		*yEnd = yStart - min;
}

/*
 *	Create rectangle object
 */

DocObjPtr RectCreate(WindowPtr window, UWORD modifier, WORD mouseX, WORD mouseY)
{
	WORD xStart, yStart, xEnd, yEnd;
	DocDataPtr docData = GetWRefCon(window);

/*
	Track object size
*/
	WindowToDoc(window, mouseX, mouseY, &xStart, &yStart);
	SnapToGrid(docData, &xStart, &yStart);
	xEnd = xStart;
	yEnd = yStart;
	TrackMouse(window, modifier, &xEnd, &yEnd, RectTrackCreate, RectConstrainCreate);
/*
	Create rect
*/
	return ((DocObjPtr) CreateRect(docData, xStart, yStart, xEnd, yEnd));
}

/*
 *	Draw routine for tracking rect grow
 */

static void RectTrackGrow(WindowPtr window, WORD xStart, WORD yStart, WORD xEnd, WORD yEnd,
						  BOOL change)
{
	RectObj rectObj;
	Rectangle rect;

	if (change && (xStart != xEnd || yStart != yEnd || wasMoved)) {
		BlockClear(&rectObj, sizeof(RectObj));
		rect = growRectObj->DocObj.Frame;
		if (growHandle == 0 || growHandle == 2)
			rect.MinX = xEnd;
		else
			rect.MaxX = xEnd;
		if (growHandle == 0 || growHandle == 1)
			rect.MinY = yEnd;
		else
			rect.MaxY = yEnd;
		SetFrameRect(&rectObj.DocObj.Frame, rect.MinX, rect.MinY, rect.MaxX, rect.MaxY);
		RectDrawOutline(window, &rectObj, 0, 0);
		wasMoved = TRUE;
	}
}

/*
 *	Track mouse and change rect shape
 */

void RectGrow(WindowPtr window, RectObjPtr rectObj, UWORD modifier, WORD mouseX, WORD mouseY)
{
	WORD xStart, yStart, xEnd, yEnd;
	DocDataPtr docData = GetWRefCon(window);
	Point pt;
	Rectangle rect, handleRect;

	growRectObj = rectObj;
	GetHandleRect(window, &handleRect);
/*
	Track handle movement
*/
	wasMoved = FALSE;
	WindowToDoc(window, mouseX, mouseY, &xStart, &yStart);
	pt.x = xStart - rectObj->DocObj.Frame.MinX;
	pt.y = yStart - rectObj->DocObj.Frame.MinY;
	if ((growHandle = RectHandle(rectObj, &pt, &handleRect)) == -1)
		return;
	SnapToGrid(docData, &xStart, &yStart);
	xEnd = xStart;
	yEnd = yStart;
	TrackMouse(window, modifier, &xEnd, &yEnd, RectTrackGrow, NULL);
/*
	Set new object dimensions
*/
	if (xEnd != xStart || yEnd != yStart) {
		InvalObjectRect(window, (DocObjPtr) rectObj);
		HiliteSelectOff(window);
		rect = rectObj->DocObj.Frame;
		if (growHandle == 0 || growHandle == 2)
			rect.MinX = xEnd;
		else
			rect.MaxX = xEnd;
		if (growHandle == 0 || growHandle == 1)
			rect.MinY = yEnd;
		else
			rect.MaxY = yEnd;
		SetFrameRect(&rectObj->DocObj.Frame, rect.MinX, rect.MinY, rect.MaxX, rect.MaxY);
		HiliteSelectOn(window);
		InvalObjectRect(window, (DocObjPtr) rectObj);
	}
}

/*
 *	Create new rect given REXX arguments
 */

RectObjPtr RectNewREXX(DocDataPtr docData, TextPtr args)
{
	Point pt1, pt2;

/*
	Get start and end points
*/
	args = GetArgPoint(args, docData, &pt1);
	if (args)
		args = GetArgPoint(args, docData, &pt2);
	if (args == NULL ||
		pt1.x < 0 || pt1.y < 0 || pt1.x >= docData->DocWidth || pt1.y >= docData->DocHeight ||
		pt2.x < 0 || pt2.y < 0 || pt2.x >= docData->DocWidth || pt2.y >= docData->DocHeight)
		return (NULL);
/*
	Create rect
*/
	return (CreateRect(docData, pt1.x, pt1.y, pt2.x, pt2.y));
}
