/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Object handling routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <Toolbox/Memory.h>
#include <Toolbox/Window.h>

#include "Draw.h"
#include "Proto.h"

/*
 *	External variables and definitions
 */

extern Options	options;

/*
 *	Create a new object and append to given layer
 *	If layer is NULL, just create object
 *	(Can't call this NewObject() since intuition uses that name)
 */

DocObjPtr NewDocObject(DocLayerPtr docLayer, WORD type)
{
	DocObjPtr newObj, topObj;

	switch (type) {
	case TYPE_GROUP:
		newObj = (DocObjPtr) GroupAllocate();
		break;
	case TYPE_LINE:
		newObj = (DocObjPtr) LineAllocate();
		break;
	case TYPE_RECT:
		newObj = (DocObjPtr) RectAllocate();
		break;
	case TYPE_OVAL:
		newObj = (DocObjPtr) OvalAllocate();
		break;
	case TYPE_POLY:
		newObj = (DocObjPtr) PolyAllocate();
		break;
	case TYPE_TEXT:
		newObj = (DocObjPtr) TextAllocate();
		break;
	case TYPE_BMAP:
		newObj = (DocObjPtr) BMapAllocate();
		break;
	default:
		newObj = NULL;
		break;
	}
	if (newObj == NULL)
		return (NULL);
	newObj->Type = type;
	if (docLayer) {
		if (docLayer->Objects == NULL)
			docLayer->Objects = newObj;
		else {
			topObj = TopObject(docLayer);
			topObj->Next = newObj;
			newObj->Prev = topObj;
		}
	}
	return (newObj);
}

/*
 *	Duplicate specified object and add to top of given layer
 *	Return pointer to object
 */

DocObjPtr DuplicateObject(DocLayerPtr docLayer, DocObjPtr docObj)
{
	BOOL success;
	DocObjPtr newObj;

	if ((newObj = NewDocObject(docLayer, docObj->Type)) == NULL)
		return (NULL);
	newObj->Flags = docObj->Flags;
	newObj->Frame = docObj->Frame;
	success = FALSE;
	switch (docObj->Type) {
	case TYPE_GROUP:
		success = GroupDupData((GroupObjPtr) docObj, (GroupObjPtr) newObj);
		break;
	case TYPE_LINE:
		success = LineDupData((LineObjPtr) docObj, (LineObjPtr) newObj);
		break;
	case TYPE_RECT:
		success = RectDupData((RectObjPtr) docObj, (RectObjPtr) newObj);
		break;
	case TYPE_OVAL:
		success = OvalDupData((OvalObjPtr) docObj, (OvalObjPtr) newObj);
		break;
	case TYPE_POLY:
		success = PolyDupData((PolyObjPtr) docObj, (PolyObjPtr) newObj);
		break;
	case TYPE_TEXT:
		success = TextDupData((TextObjPtr) docObj, (TextObjPtr) newObj);
		break;
	case TYPE_BMAP:
		success = BMapDupData((BMapObjPtr) docObj, (BMapObjPtr) newObj);
		break;
	}
	if (!success) {
		if (docLayer)
			DetachObject(docLayer, newObj);
		DisposeDocObject(newObj);
		newObj = NULL;
	}
	return (newObj);
}

/*
 *	Detach object from layer list (but don't dispose)
 */

void DetachObject(DocLayerPtr docLayer, DocObjPtr docObj)
{
	if (docObj->Group && docObj->Group->Objects == docObj)
		docObj->Group->Objects = docObj->Next;
	if (docObj->Next)
		docObj->Next->Prev = docObj->Prev;
	if (docObj->Prev)
		docObj->Prev->Next = docObj->Next;
	if (docLayer->Objects == docObj)
		docLayer->Objects = docObj->Next;
	docObj->Next = docObj->Prev = NULL;
}

/*
 *	Free memory used by object
 *	Does not adjust object links
 *	(Can't call this DisposeObject() since intuition uses that name)
 */

void DisposeDocObject(DocObjPtr docObj)
{
	if (docObj == NULL)
		return;
	switch (docObj->Type) {
	case TYPE_GROUP:
		GroupDispose((GroupObjPtr) docObj);
		break;
	case TYPE_LINE:
		LineDispose((LineObjPtr) docObj);
		break;
	case TYPE_RECT:
		RectDispose((RectObjPtr) docObj);
		break;
	case TYPE_OVAL:
		OvalDispose((OvalObjPtr) docObj);
		break;
	case TYPE_POLY:
		PolyDispose((PolyObjPtr) docObj);
		break;
	case TYPE_TEXT:
		TextDispose((TextObjPtr) docObj);
		break;
	case TYPE_BMAP:
		BMapDispose((BMapObjPtr) docObj);
		break;
	}
}

/*
 *	Free of all objects in list
 *	Does not adjust object links
 */

void DisposeAllDocObjects(DocObjPtr docObj)
{
	DocObjPtr nextObj;

	while (docObj) {
		nextObj = NextObj(docObj);
		DisposeDocObject(docObj);
		docObj = nextObj;
	}
}

/*
 *	Return pointer to last (i.e. top) object in layer
 */

DocObjPtr TopObject(DocLayerPtr docLayer)
{
	register DocObjPtr docObj;

	if (docLayer->Objects == NULL)
		return (NULL);
	for (docObj = docLayer->Objects; NextObj(docObj); docObj = NextObj(docObj)) ;
	return (docObj);
}

/*
 *	Return pointer to first (i.e. bottom) object in layer
 */

DocObjPtr BottomObject(DocLayerPtr docLayer)
{
	return (docLayer->Objects);
}

/*
 *	Insert object into layer object list after given object
 *	If afterObj is NULL, insert at start of list
 */

void InsertObject(DocLayerPtr docLayer, DocObjPtr afterObj, DocObjPtr docObj)
{
	if (docObj == NULL)
		return;
	docObj->Group = NULL;
	if (afterObj == NULL) {
		docObj->Prev = NULL;
		docObj->Next = docLayer->Objects;
		if (docLayer->Objects)
			docLayer->Objects->Prev = docObj;
		docLayer->Objects = docObj;
	}
	else {
		docObj->Prev = afterObj;
		docObj->Next = afterObj->Next;
		if (afterObj->Next)
			afterObj->Next->Prev = docObj;
		afterObj->Next = docObj;
	}
}

/*
 *	Insert all objects in list after given object
 *	If afterObj is NULL, insert at start of list
 */

void InsertAllObjects(DocLayerPtr docLayer, DocObjPtr afterObj, DocObjPtr firstObj)
{
	DocObjPtr lastObj;

	if (firstObj == NULL)
		return;
	for (lastObj = firstObj; NextObj(lastObj); lastObj = NextObj(lastObj))
		lastObj->Group = NULL;
	lastObj->Group = NULL;
	if (afterObj == NULL) {
		firstObj->Prev = NULL;
		lastObj->Next = docLayer->Objects;
		if (docLayer->Objects)
			docLayer->Objects->Prev = lastObj;
		docLayer->Objects = firstObj;
	}
	else {
		firstObj->Prev = afterObj;
		lastObj->Next = afterObj->Next;
		if (afterObj->Next)
			afterObj->Next->Prev = lastObj;
		afterObj->Next = firstObj;
	}
}

/*
 *	Append object to end of list
 */

void AppendObject(DocLayerPtr docLayer, DocObjPtr docObj)
{
	InsertObject(docLayer, TopObject(docLayer), docObj);
}

/*
 *	Append object to end of group
 */

void AppendToGroup(GroupObjPtr groupObj, DocObjPtr docObj)
{
	DocObjPtr topObj;

	docObj->Group = groupObj;
	if (groupObj->Objects == NULL)
		groupObj->Objects = docObj;
	else {
		for (topObj = groupObj->Objects; NextObj(topObj); topObj = NextObj(topObj)) ;
		topObj->Next = docObj;
		docObj->Prev = topObj;
		docObj->Next = NULL;
	}
}

/*
 *	Move object forward
 */

void ObjectForward(DocLayerPtr docLayer, DocObjPtr docObj)
{
	DocObjPtr nextObj;

	if (docObj == NULL || (nextObj = NextObj(docObj)) == NULL)
		return;
	DetachObject(docLayer, docObj);
	InsertObject(docLayer, nextObj, docObj);
}

/*
 *	Move object to front
 */

void ObjectToFront(DocLayerPtr docLayer, DocObjPtr docObj)
{
	DocObjPtr topObj;

	if (docObj == NULL)
		return;
	topObj = TopObject(docLayer);
	if (topObj == NULL || topObj == docObj)
		return;
	DetachObject(docLayer, docObj);
	AppendObject(docLayer, docObj);
}

/*
 *	Move object backward
 */

void ObjectBackward(DocLayerPtr docLayer, DocObjPtr docObj)
{
	DocObjPtr prevObj;

	if (docObj == NULL || (prevObj = docObj->Prev) == NULL)
		return;
	DetachObject(docLayer, prevObj);
	InsertObject(docLayer, docObj, prevObj);
}

/*
 *	Move object to back
 */

void ObjectToBack(DocLayerPtr docLayer, DocObjPtr docObj)
{
	DocObjPtr bottomObj;

	if (docObj == NULL)
		return;
	bottomObj = BottomObject(docLayer);
	if (bottomObj == NULL || bottomObj == docObj)
		return;
	DetachObject(docLayer, docObj);
	InsertObject(docLayer, NULL, docObj);
}

/*
 *	Return pointer to first object in current layer
 */

DocObjPtr FirstObject(DocDataPtr docData)
{
	DocLayerPtr currLayer = CurrLayer(docData);

	if (currLayer == NULL)
		return (NULL);
	return (BottomObject(currLayer));
}

/*
 *	Return pointer to last object in current layer
 */

DocObjPtr LastObject(DocDataPtr docData)
{
	DocLayerPtr currLayer = CurrLayer(docData);

	if (currLayer == NULL)
		return (NULL);
	return (TopObject(currLayer));
}

/*
 *	Return pointer to first selected object in current layer (or NULL)
 */

DocObjPtr FirstSelected(DocDataPtr docData)
{
	register DocObjPtr docObj;

	for (docObj = FirstObject(docData); docObj; docObj = NextObj(docObj)) {
		if (ObjectSelected(docObj))
			break;
	}
	return (docObj);
}

/*
 *	Return pointer to last selected object in current layer (or NULL)
 */

DocObjPtr LastSelected(DocDataPtr docData)
{
	register DocObjPtr docObj;

	for (docObj = LastObject(docData); docObj; docObj = docObj->Prev) {
		if (ObjectSelected(docObj))
			break;
	}
	return (docObj);
}

/*
 *	Return pointer to next selected object in current layer (or NULL)
 */

DocObjPtr NextSelected(DocObjPtr docObj)
{
	for (docObj = NextObj(docObj); docObj; docObj = NextObj(docObj)) {
		if (ObjectSelected(docObj))
			break;
	}
	return (docObj);
}

/*
 *	Return pointer to prev selected object in current layer (or NULL)
 */

DocObjPtr PrevSelected(DocObjPtr docObj)
{
	for (docObj = docObj->Prev; docObj; docObj = docObj->Prev) {
		if (ObjectSelected(docObj))
			break;
	}
	return (docObj);
}

/*
 *	Un-select all objects in current layer
 */

void UnSelectAllObjects(DocDataPtr docData)
{
	DocObjPtr docObj;

	for (docObj = FirstObject(docData); docObj; docObj = NextObj(docObj))
		UnSelectObject(docObj);
}

/*
 *	Return TRUE if any selected object is locked
 */

BOOL ObjectsLocked(DocDataPtr docData)
{
	register DocObjPtr docObj;

	for (docObj = FirstSelected(docData); docObj; docObj = NextSelected(docObj)) {
		if (ObjectLocked(docObj))
			return (TRUE);
	}
	return (FALSE);
}

/*
 *	Draw object, scaling frame into dstRect
 *	Should only draw portions in clipRect (in rPort coordinates)
 */

void DrawObject(RastPtr rPort, DocObjPtr docObj, RectPtr dstRect, RectPtr clipRect)
{
	if (dstRect->MinX > clipRect->MaxX || dstRect->MinY > clipRect->MaxY ||
		dstRect->MaxX < clipRect->MinX || dstRect->MaxY < clipRect->MinY)
		return;
	switch (docObj->Type) {
	case TYPE_GROUP:
		GroupDrawObj(rPort, (GroupObjPtr) docObj, dstRect, clipRect);
		break;
	case TYPE_LINE:
		LineDrawObj(rPort, (LineObjPtr) docObj, dstRect, clipRect);
		break;
	case TYPE_RECT:
		RectDrawObj(rPort, (RectObjPtr) docObj, dstRect, clipRect);
		break;
	case TYPE_OVAL:
		OvalDrawObj(rPort, (OvalObjPtr) docObj, dstRect, clipRect);
		break;
	case TYPE_POLY:
		PolyDrawObj(rPort, (PolyObjPtr) docObj, dstRect, clipRect);
		break;
	case TYPE_TEXT:
		TextDrawObj(rPort, (TextObjPtr) docObj, dstRect, clipRect);
		break;
	case TYPE_BMAP:
		BMapDrawObj(rPort, (BMapObjPtr) docObj, dstRect, clipRect);
		break;
	}
}

/*
 *	Draw object outline
 *	Offset is in document coordinates
 */

void DrawObjectOutline(WindowPtr window, DocObjPtr docObj, WORD xOffset, WORD yOffset)
{
	if (options.DragOutlines) {
		switch (docObj->Type) {
		case TYPE_GROUP:
			GroupDrawOutline(window, (GroupObjPtr) docObj, xOffset, yOffset);
			break;
		case TYPE_LINE:
			LineDrawOutline(window, (LineObjPtr) docObj, xOffset, yOffset);
			break;
		case TYPE_RECT:
			RectDrawOutline(window, (RectObjPtr) docObj, xOffset, yOffset);
			break;
		case TYPE_OVAL:
			OvalDrawOutline(window, (OvalObjPtr) docObj, xOffset, yOffset);
			break;
		case TYPE_POLY:
			PolyDrawOutline(window, (PolyObjPtr) docObj, xOffset, yOffset);
			break;
		case TYPE_TEXT:
			TextDrawOutline(window, (TextObjPtr) docObj, xOffset, yOffset);
			break;
		case TYPE_BMAP:
			BMapDrawOutline(window, (BMapObjPtr) docObj, xOffset, yOffset);
			break;
		}
	}
	else
		RectDrawOutline(window, (RectObjPtr) docObj, xOffset, yOffset);
}

/*
 *	Hilite object
 */

void HiliteObject(WindowPtr window, DocObjPtr docObj)
{
	switch (docObj->Type) {
	case TYPE_GROUP:
		GroupHilite(window, (GroupObjPtr) docObj);
		break;
	case TYPE_LINE:
		LineHilite(window, (LineObjPtr) docObj);
		break;
	case TYPE_RECT:
		RectHilite(window, (RectObjPtr) docObj);
		break;
	case TYPE_OVAL:
		OvalHilite(window, (OvalObjPtr) docObj);
		break;
	case TYPE_POLY:
		PolyHilite(window, (PolyObjPtr) docObj);
		break;
	case TYPE_TEXT:
		TextHilite(window, (TextObjPtr) docObj);
		break;
	case TYPE_BMAP:
		BMapHilite(window, (BMapObjPtr) docObj);
		break;
	}
}

/*
 *	Enable or disable object pen
 */

void EnableObjectPen(DocObjPtr docObj, BOOL enable)
{
	if (enable)
		docObj->Flags |= OBJ_DO_PEN;
	else
		docObj->Flags &= ~OBJ_DO_PEN;
	if (docObj->Type == TYPE_GROUP) {
		for (docObj = ((GroupObjPtr) docObj)->Objects; docObj; docObj = NextObj(docObj))
			EnableObjectPen(docObj, enable);
	}
}

/*
 *	Set object pen color
 */

void SetObjectPenColor(DocObjPtr docObj, RGBColor penColor)
{
	switch (docObj->Type) {
	case TYPE_GROUP:
		GroupSetPenColor((GroupObjPtr) docObj, penColor);
		break;
	case TYPE_LINE:
		LineSetPenColor((LineObjPtr) docObj, penColor);
		break;
	case TYPE_RECT:
		RectSetPenColor((RectObjPtr) docObj, penColor);
		break;
	case TYPE_OVAL:
		OvalSetPenColor((OvalObjPtr) docObj, penColor);
		break;
	case TYPE_POLY:
		PolySetPenColor((PolyObjPtr) docObj, penColor);
		break;
	case TYPE_TEXT:
		TextSetPenColor((TextObjPtr) docObj, penColor);
		break;
	case TYPE_BMAP:
		BMapSetPenColor((BMapObjPtr) docObj, penColor);
		break;
	}
}

/*
 *	Return object pen size
 *	Return FALSE (and -1 for size) if object does not have a pen size
 */

BOOL GetObjectPenSize(DocObjPtr docObj, WORD *penWidth, WORD *penHeight)
{
	switch (docObj->Type) {
	case TYPE_LINE:
		*penWidth  = ((LineObjPtr) docObj)->PenWidth;
		*penHeight = ((LineObjPtr) docObj)->PenHeight;
		break;
	case TYPE_RECT:
		*penWidth  = ((RectObjPtr) docObj)->PenWidth;
		*penHeight = ((RectObjPtr) docObj)->PenHeight;
		break;
	case TYPE_OVAL:
		*penWidth  = ((OvalObjPtr) docObj)->PenWidth;
		*penHeight = ((OvalObjPtr) docObj)->PenHeight;
		break;
	case TYPE_POLY:
		*penWidth  = ((PolyObjPtr) docObj)->PenWidth;
		*penHeight = ((PolyObjPtr) docObj)->PenHeight;
		break;
	default:
		*penWidth = *penHeight = -1;
		break;
	}
	return ((BOOL) (*penWidth != -1 && *penHeight != -1));
}

/*
 *	Set object pen size
 *	If size is -1 then do not change
 */

void SetObjectPenSize(DocObjPtr docObj, WORD penWidth, WORD penHeight)
{
	switch (docObj->Type) {
	case TYPE_GROUP:
		GroupSetPenSize((GroupObjPtr) docObj, penWidth, penHeight);
		break;
	case TYPE_LINE:
		LineSetPenSize((LineObjPtr) docObj, penWidth, penHeight);
		break;
	case TYPE_RECT:
		RectSetPenSize((RectObjPtr) docObj, penWidth, penHeight);
		break;
	case TYPE_OVAL:
		OvalSetPenSize((OvalObjPtr) docObj, penWidth, penHeight);
		break;
	case TYPE_POLY:
		PolySetPenSize((PolyObjPtr) docObj, penWidth, penHeight);
		break;
	case TYPE_TEXT:
		TextSetPenSize((TextObjPtr) docObj, penWidth, penHeight);
		break;
	case TYPE_BMAP:
		BMapSetPenSize((BMapObjPtr) docObj, penWidth, penHeight);
		break;
	}
}

/*
 *	Enable or disable object fill
 */

void EnableObjectFill(DocObjPtr docObj, BOOL enable)
{
	if (enable)
		docObj->Flags |= OBJ_DO_FILL;
	else
		docObj->Flags &= ~OBJ_DO_FILL;
	if (docObj->Type == TYPE_GROUP) {
		for (docObj = ((GroupObjPtr) docObj)->Objects; docObj; docObj = NextObj(docObj))
			EnableObjectFill(docObj, enable);
	}
}

/*
 *	Set object fill pattern
 */

void SetObjectFillPat(DocObjPtr docObj, RGBPat8Ptr fillPat)
{
	switch (docObj->Type) {
	case TYPE_GROUP:
		GroupSetFillPat((GroupObjPtr) docObj, fillPat);
		break;
	case TYPE_LINE:
		LineSetFillPat((LineObjPtr) docObj, fillPat);
		break;
	case TYPE_RECT:
		RectSetFillPat((RectObjPtr) docObj, fillPat);
		break;
	case TYPE_OVAL:
		OvalSetFillPat((OvalObjPtr) docObj, fillPat);
		break;
	case TYPE_POLY:
		PolySetFillPat((PolyObjPtr) docObj, fillPat);
		break;
	case TYPE_TEXT:
		TextSetFillPat((TextObjPtr) docObj, fillPat);
		break;
	case TYPE_BMAP:
		BMapSetFillPat((BMapObjPtr) docObj, fillPat);
		break;
	}
}

/*
 *	Set object text parameters
 *	If parameter is -1 then do not change
 */

void SetObjectTextParams(DocObjPtr docObj, FontNum fontNum, FontSize fontSize,
						 WORD style, WORD miscStyle, WORD justify, WORD spacing)
{
	switch (docObj->Type) {
	case TYPE_TEXT:
		TextSetTextParams((TextObjPtr) docObj, fontNum, fontSize, style, miscStyle,
						  justify, spacing);
		break;
	case TYPE_GROUP:
		GroupSetTextParams((GroupObjPtr) docObj, fontNum, fontSize, style, miscStyle,
						   justify, spacing);
		break;
	}
}

/*
 *	Rotate object by given angle about center
 */

void RotateObject(DocObjPtr docObj, WORD cx, WORD cy, WORD angle)
{
	if (angle != 90 && angle != -90)
		return;
	switch (docObj->Type) {
	case TYPE_GROUP:
		GroupRotate((GroupObjPtr) docObj, cx, cy, angle);
		break;
	case TYPE_LINE:
		LineRotate((LineObjPtr) docObj, cx, cy, angle);
		break;
	case TYPE_RECT:
		RectRotate((RectObjPtr) docObj, cx, cy, angle);
		break;
	case TYPE_OVAL:
		OvalRotate((OvalObjPtr) docObj, cx, cy, angle);
		break;
	case TYPE_POLY:
		PolyRotate((PolyObjPtr) docObj, cx, cy, angle);
		break;
	case TYPE_TEXT:
		TextRotate((TextObjPtr) docObj, cx, cy, angle);
		break;
	case TYPE_BMAP:
		BMapRotate((BMapObjPtr) docObj, cx, cy, angle);
		break;
	}
}

/*
 *	Flip object horizontally or vertically
 */

void FlipObject(DocObjPtr docObj, WORD cx, WORD cy, BOOL horiz)
{
	switch (docObj->Type) {
	case TYPE_GROUP:
		GroupFlip((GroupObjPtr) docObj, cx, cy, horiz);
		break;
	case TYPE_LINE:
		LineFlip((LineObjPtr) docObj, cx, cy, horiz);
		break;
	case TYPE_RECT:
		RectFlip((RectObjPtr) docObj, cx, cy, horiz);
		break;
	case TYPE_OVAL:
		OvalFlip((OvalObjPtr) docObj, cx, cy, horiz);
		break;
	case TYPE_POLY:
		PolyFlip((PolyObjPtr) docObj, cx, cy, horiz);
		break;
	case TYPE_TEXT:
		TextFlip((TextObjPtr) docObj, cx, cy, horiz);
		break;
	case TYPE_BMAP:
		BMapFlip((BMapObjPtr) docObj, cx, cy, horiz);
		break;
	}
}

/*
 *	Determine if point is in object
 */

BOOL PointInObject(DocObjPtr docObj, PointPtr pt)
{
	BOOL select;
	WORD penWidth, penHeight;
	Point point;
	Rectangle rect;

/*
	Pre-screen for point inside of object rectangle
	(Include one pixel slop on either side)
*/
	rect = docObj->Frame;
	if (GetObjectPenSize(docObj, &penWidth, &penHeight))
		InsetRect(&rect, -penWidth/2, -penHeight/2);
	InsetRect(&rect, -2, -2);
	if (!PtInRect(pt, &rect))
		return (FALSE);
/*
	In rect, so offset to rect start and do specific check
*/
	point.x = pt->x - docObj->Frame.MinX;
	point.y = pt->y - docObj->Frame.MinY;
	switch (docObj->Type) {
	case TYPE_GROUP:
		select = GroupSelect((GroupObjPtr) docObj, &point);
		break;
	case TYPE_LINE:
		select = LineSelect((LineObjPtr) docObj, &point);
		break;
	case TYPE_RECT:
		select = RectSelect((RectObjPtr) docObj, &point);
		break;
	case TYPE_OVAL:
		select = OvalSelect((OvalObjPtr) docObj, &point);
		break;
	case TYPE_POLY:
		select = PolySelect((PolyObjPtr) docObj, &point);
		break;
	case TYPE_TEXT:
		select = TextSelect((TextObjPtr) docObj, &point);
		break;
	case TYPE_BMAP:
		select = BMapSelect((BMapObjPtr) docObj, &point);
		break;
	default:
		select = FALSE;
		break;
	}
	return (select);
}

/*
 *	Determine if point is in object grow handle
 */

BOOL PointInHandle(DocObjPtr docObj, PointPtr pt, RectPtr handleRect)
{
	WORD handle;
	Point point;
	Rectangle rect;

/*
	Pre-screen for point inside of object handle rectangle
*/
	rect = docObj->Frame;
	InsetRect(&rect, handleRect->MinX, handleRect->MinY);
	if (!PtInRect(pt, &rect))
		return (FALSE);
/*
	In rect, so offset to rect start and do specific check
*/
	point.x = pt->x - docObj->Frame.MinX;
	point.y = pt->y - docObj->Frame.MinY;
	switch (docObj->Type) {
	case TYPE_GROUP:
		handle = GroupHandle((GroupObjPtr) docObj, &point, handleRect);
		break;
	case TYPE_LINE:
		handle = LineHandle((LineObjPtr) docObj, &point, handleRect);
		break;
	case TYPE_RECT:
		handle = RectHandle((RectObjPtr) docObj, &point, handleRect);
		break;
	case TYPE_OVAL:
		handle = OvalHandle((OvalObjPtr) docObj, &point, handleRect);
		break;
	case TYPE_POLY:
		handle = PolyHandle((PolyObjPtr) docObj, &point, handleRect);
		break;
	case TYPE_TEXT:
		handle = TextHandle((TextObjPtr) docObj, &point, handleRect);
		break;
	case TYPE_BMAP:
		handle = BMapHandle((BMapObjPtr) docObj, &point, handleRect);
		break;
	default:
		handle = -1;
		break;
	}
	return ((BOOL) (handle >= 0));
}

/*
 *	Offset object by given x and y displacement
 */

void OffsetObject(DocObjPtr docObj, WORD xOffset, WORD yOffset)
{
	OffsetRect(&docObj->Frame, xOffset, yOffset);
}

/*
 *	Scale object to new frame rectangle
 */

void ScaleObject(DocObjPtr docObj, RectPtr frame)
{
/*
	Don't allow resize to single pixel size
*/
	if (frame->MaxX <= frame->MinX)
		frame->MaxX = frame->MinX + 1;
	if (frame->MaxY <= frame->MinY)
		frame->MaxY = frame->MinY + 1;
/*
	Do scale
*/
	switch (docObj->Type) {
	case TYPE_GROUP:
		GroupScale((GroupObjPtr) docObj, frame);
		break;
	case TYPE_LINE:
		LineScale((LineObjPtr) docObj, frame);
		break;
	case TYPE_RECT:
		RectScale((RectObjPtr) docObj, frame);
		break;
	case TYPE_OVAL:
		OvalScale((OvalObjPtr) docObj, frame);
		break;
	case TYPE_POLY:
		PolyScale((PolyObjPtr) docObj, frame);
		break;
	case TYPE_TEXT:
		TextScale((TextObjPtr) docObj, frame);
		break;
	case TYPE_BMAP:
		BMapScale((BMapObjPtr) docObj, frame);
		break;
	}
}

/*
 *	Convert object to poly
 */

PolyObjPtr ConvertObjectToPoly(DocObjPtr docObj)
{
	PolyObjPtr polyObj;

	switch (docObj->Type) {
	case TYPE_LINE:
		polyObj = LineConvertToPoly((LineObjPtr) docObj);
		break;
	case TYPE_RECT:
		polyObj = RectConvertToPoly((RectObjPtr) docObj);
		break;
	case TYPE_OVAL:
		polyObj = OvalConvertToPoly((OvalObjPtr) docObj);
		break;
	default:
		polyObj = NULL;
		break;
	}
	return (polyObj);
}

/*
 *	Create a new REXX object in windo
 */

BOOL NewREXXObject(WindowPtr window, WORD type, TextPtr args)
{
	DocObjPtr docObj;
	DocDataPtr docData = GetWRefCon(window);

	HiliteSelectOff(window);
	UnSelectAllObjects(docData);
	switch (type) {
	case TYPE_LINE:
		docObj = (DocObjPtr) LineNewREXX(docData, args);
		break;
	case TYPE_RECT:
		docObj = (DocObjPtr) RectNewREXX(docData, args);
		break;
	case TYPE_OVAL:
		docObj = (DocObjPtr) OvalNewREXX(docData, args);
		break;
	case TYPE_POLY:
		docObj = (DocObjPtr) PolyNewREXX(docData, args);
		break;
	case TYPE_TEXT:
		docObj = (DocObjPtr) TextNewREXX(docData, args);
		break;
	default:				/* Unknown type */
		docObj = NULL;
		break;
	}
	if (docObj) {
		SelectObject(docObj);
		InvalObjectRect(window, docObj);
		DocModified(docData);
	}
	if (window->Flags & WINDOWACTIVE)
		HiliteSelectOn(window);
	return ((BOOL) (docObj != NULL));
}

/*
 *	Adjust rectangle to be within document bounds
 */

void AdjustToDocBounds(DocDataPtr docData, RectPtr rect)
{
	if (rect->MaxX >= docData->DocWidth)
		OffsetRect(rect, docData->DocWidth - rect->MaxX - 1, 0);
	if (rect->MaxY >= docData->DocHeight)
		OffsetRect(rect, 0, docData->DocHeight - rect->MaxY - 1);
	if (rect->MinX < 0)
		OffsetRect(rect, -rect->MinX, 0);
	if (rect->MinY < 0)
		OffsetRect(rect, 0, -rect->MinY);
}

/*
 *	Invalidate display rectangle of object in window
 *	(Adjusts for line widths and arrows)
 */

void InvalObjectRect(WindowPtr window, DocObjPtr docObj)
{
	LineObjPtr lineObj;
	GroupObjPtr group;
	Point pen;
	Rectangle rect, contRect;

/*
	Special case for groups:  invalidate each individual object rect
	(Since objects with thick pens may be outside the group rect)
*/
	if (docObj->Type == TYPE_GROUP) {
		for (docObj = ((GroupObjPtr) docObj)->Objects; docObj; docObj = NextObj(docObj))
			InvalObjectRect(window, docObj);
	}
/*
	Handle non-group objects
*/
	else {
		rect = docObj->Frame;
		for (group = docObj->Group; group; group = group->DocObj.Group)
			OffsetRect(&rect, group->DocObj.Frame.MinX, group->DocObj.Frame.MinY);
		if (GetObjectPenSize(docObj, &pen.x, &pen.y)) {
			InsetRect(&rect, -pen.x/2 - 1, -pen.y/2 - 1);	/* Extra for security */
			if (docObj->Type == TYPE_LINE) {
				lineObj = (LineObjPtr) docObj;
				if (lineObj->LineFlags & (LINE_ARROWSTART | LINE_ARROWEND))
					InsetRect(&rect, -4, -4);
			}
		}
		DocToWindowRect(window, &rect, &rect);
		GetContentRect(window, &contRect);
		if (SectRect(&rect, &contRect, &rect))
			InvalRect(window, &rect);
	}
}
