/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Layout menu routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>

#include <string.h>

#include <Toolbox/Graphics.h>
#include <Toolbox/Window.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/ScrollList.h>
#include <Toolbox/Utility.h>
#include <Toolbox/Color.h>

#include "Draw.h"
#include "Proto.h"

/*
 *	External variables
 */

extern MsgPortPtr	mainMsgPort;
extern ScreenPtr	screen;

extern Defaults	defaults;
extern Options	options;
extern RGBColor	penColors[];
extern RGBPat8	fillPats[];

extern UBYTE	gridSixteenthUnits[], gridMMUnits[];

extern InvColorTable	scrnInvColorTable;

extern UBYTE	blackColor, whiteColor, darkColor, lightColor;

extern TextChar	strInch[], strCM[], strMM[], strPenColor[], strLayer[];
extern TextChar	strBuff[];

extern DlgTemplPtr	dlgList[];

/*
 *	Local variables and definitions
 */

enum {
	GRIDUP_ARROW = 2,
	GRIDDOWN_ARROW,
	GRID_TEXT
};

enum {
	PENCOLORS_USERITEM = 2,
	CHANGEPEN_BTN,
	FILLPATS_USERITEM,
	EDITPAT_USERITEM,
	SAMPLEPAT_USERITEM,
	CLEAR_BTN,
	REVERT_BTN
};

enum {
	NEW_BTN = 0,
	MOVEUP_ARROW,
	MOVEDOWN_ARROW,
	RENAME_BTN,
	DELETE_BTN,
	SHOW_BTN,
	HIDE_BTN,
	DONE_BTN,
	LAYERS_LIST,
	LAYERS_UP,
	LAYERS_DOWN,
	LAYERS_SCROLL,
	NAME_TEXT
};

enum {
	DRAWSIZE_USERITEM = 2,
	WIDTH_TEXT,
	HEIGHT_TEXT,
	PAGES_TEXT
};

static DialogPtr	gridSizeDlg, penColorsDlg, fillPatsDlg, layersDlg, drawSizeDlg;

static ScrollListPtr	layersScrollList;

/*
 *	Local prototypes
 */

Fixed	MinScale(WindowPtr);

void	SetDocScale(WindowPtr, Fixed);

BOOL	DoNormalSize(WindowPtr);
BOOL	DoEnlarge(WindowPtr);
BOOL	DoReduce(WindowPtr);
BOOL	DoFitToWindow(WindowPtr);

BOOL	DoGridSnap(WindowPtr);
void	ShowGridSize(DialogPtr, WORD);
BOOL	GridSizeDialogFilter(IntuiMsgPtr, WORD *);
BOOL	DoGridSize(WindowPtr);

void	DrawPenItem(DialogPtr, WORD, RGBColor, BOOL);
BOOL	PenColorsDialogFilter(IntuiMsgPtr, WORD *);
BOOL	DoPenColors(void);

void	DrawFillItem(DialogPtr, WORD, RGBPat8Ptr, BOOL);
void	ShowEditPat(DialogPtr, RGBPat8Ptr);
void	DrawEditPixel(DialogPtr, RGBPat8Ptr, WORD, WORD);
BOOL	FillPatsDialogFilter(IntuiMsgPtr, WORD *);
BOOL	DoFillPatterns(void);

void	ShowLayerNames(DocDataPtr, DialogPtr, ScrollListPtr, DocLayerPtr);
void	SetLayerButtons(DialogPtr, DocLayerPtr);
BOOL	LayersDialogFilter(IntuiMsgPtr, WORD *);
BOOL	DoLayers(WindowPtr);

void	MinDrawSize(DocDataPtr, WORD *, WORD *);
void	ShowDrawSize(DialogPtr, DocDataPtr, WORD, WORD);
BOOL	DrawSizeDialogFilter(IntuiMsgPtr, WORD *);
BOOL	DoDrawingSize(WindowPtr);

/*
 *	Determine minimum scale value for given document size
 *	Minimum value is value that will have entire document shown in window
 *	Will not return a value greater than SCALE_FULL
 */

static Fixed MinScale(WindowPtr window)
{
	Fixed scale, xScale, yScale;
	WORD width, height;
	DocDataPtr docData = GetWRefCon(window);
	Rectangle rect;

	GetContentRect(window, &rect);
	ScaleDocToWin(SCALE_FULL, docData->DocWidth, docData->DocHeight, &width, &height);
	xScale = FixRatio(rect.MaxX - rect.MinX, width);
	yScale = FixRatio(rect.MaxY - rect.MinY, height);
	for (scale = SCALE_FULL; scale > SCALE_MIN; scale >>= 1) {
		if (scale <= xScale && scale <= yScale)
			break;
	}
	return (scale);
}

/*
 *	Set document scale to given value
 */

static void SetDocScale(WindowPtr window, Fixed scale)
{
	WORD leftOffset, topOffset;
	DocDataPtr docData = GetWRefCon(window);
	Point center;
	Rectangle rect;

/*
	Find coordinates of center of selected objects, or of window
*/
	if (!GetSelectRect(docData, &rect)) {
		if (docData->Scale <= MinScale(window))
			SetRect(&rect, 0, 0, docData->DocWidth - 1, docData->DocHeight - 1);
		else {
			GetContentRect(window, &rect);
			WindowToDocRect(window, &rect, &rect);
		}
	}
	center.x = (rect.MaxX + rect.MinX + 1)/2;
	center.y = (rect.MaxY + rect.MinY + 1)/2;
/*
	Adjust scale
*/
	if (scale > SCALE_MAX)
		scale = SCALE_MAX;
	else if (scale < SCALE_MIN)
		scale = SCALE_MIN;
	if (scale == docData->Scale)
		return;
	docData->Scale = scale;
/*
	Set scroll offset that will keep window center in center
*/
	GetContentRect(window, &rect);
	WindowToDocRect(window, &rect, &rect);
	leftOffset = center.x - (rect.MaxX - rect.MinX + 1)/2;
	topOffset  = center.y - (rect.MaxY - rect.MinY + 1)/2;
	SetScrollOffset(window, leftOffset, topOffset);
	GetWindowRect(window, &rect);
	InvalRect(window, &rect);
	SetLayoutMenu();
}

/*
 *	Enlarge to normal size
 */

static BOOL DoNormalSize(WindowPtr window)
{
	SetDocScale(window, SCALE_FULL);
	return (TRUE);
}

/*
 *	Enlarge display
 */

static BOOL DoEnlarge(WindowPtr window)
{
	DocDataPtr docData = GetWRefCon(window);

	SetDocScale(window, docData->Scale << 1);
	return (TRUE);
}

/*
 *	Reduce display
 */

static BOOL DoReduce(WindowPtr window)
{
	DocDataPtr docData = GetWRefCon(window);

	SetDocScale(window, docData->Scale >> 1);
	return (TRUE);
}

/*
 *	Reduce to fit in window
 */

static BOOL DoFitToWindow(WindowPtr window)
{
	SetDocScale(window, MinScale(window));
	return (TRUE);
}

/*
 *	Turn grid snap on/off
 */

static BOOL DoGridSnap(WindowPtr window)
{
	DocDataPtr docData = GetWRefCon(window);

	if (docData->Flags & DOC_GRIDSNAP)
		docData->Flags &= ~DOC_GRIDSNAP;
	else
		docData->Flags |= DOC_GRIDSNAP;
	SetLayoutMenu();
	return (TRUE);
}

/*
 *	Show grid size in dialog, and enable/disable up & down arrows
 */

static void ShowGridSize(DialogPtr dlg, WORD index)
{
	WORD numer, denom;
	GadgetPtr gadget, gadgList;
	TextChar denomText[5];

	if (options.MeasureUnit == MEASURE_CM) {
		NumToString(gridMMUnits[index], strBuff);
		strcat(strBuff, strMM);
	}
	else {
		numer = gridSixteenthUnits[index];
		denom = 16;
		while ((numer & 1) == 0 && (denom & 1) == 0) {
			numer >>= 1;
			denom >>= 1;
		}
		NumToString(numer, strBuff);
		if (denom > 1) {
			strcat(strBuff, "/");
			NumToString(denom, denomText);
			strcat(strBuff, denomText);
		}
		strcat(strBuff, strInch);
	}
	gadgList = dlg->FirstGadget;
	SetGadgetItemText(gadgList, GRID_TEXT, dlg, NULL, strBuff);
		gadget = GadgetItem(gadgList, GRIDDOWN_ARROW);
	if (index <= 0)
		OffGList(gadget, dlg, NULL, 1);
	else
		OnGList(gadget, dlg, NULL, 1);
		gadget = GadgetItem(gadgList, GRIDUP_ARROW);
	if (index >= NUM_GRIDINDEX - 1)
		OffGList(gadget, dlg, NULL, 1);
	else
		OnGList(gadget, dlg, NULL, 1);
}

/*
 *	Grid size dialog filter
 */

static BOOL GridSizeDialogFilter(register IntuiMsgPtr intuiMsg, WORD *item)
{
	register WORD itemHit;
	register ULONG class;

	class = intuiMsg->Class;
	if (intuiMsg->IDCMPWindow == gridSizeDlg && (class == GADGETDOWN || class == GADGETUP)) {
		itemHit = GadgetNumber((GadgetPtr) intuiMsg->IAddress);
		if (itemHit == GRIDUP_ARROW || itemHit == GRIDDOWN_ARROW) {
			ReplyMsg((MsgPtr) intuiMsg);
			*item = (class == GADGETDOWN) ? itemHit : -1;
			return (TRUE);
		}
	}
	return (DialogFilter(intuiMsg, item));
}

/*
 *	Change grid size
 */

static BOOL DoGridSize(WindowPtr window)
{
	WORD item, index;
	BOOL done;
	DocDataPtr docData = GetWRefCon(window);

	index = docData->GridUnitsIndex;
	if (index < 0)
		index = 0;
	else if (index >= NUM_GRIDINDEX)
		index = NUM_GRIDINDEX - 1;
/*
	Bring up dialog
*/
	BeginWait();
	if ((gridSizeDlg = GetDialog(dlgList[DLG_GRIDSIZE], screen, mainMsgPort)) == NULL) {
		EndWait();
		Error(ERR_NO_MEM);
		return (FALSE);
	}
	OutlineOKButton(gridSizeDlg);
	DrawArrowBorder(gridSizeDlg, GRIDUP_ARROW);
	ShowGridSize(gridSizeDlg, index);
/*
	Handle dialog
*/
	done = FALSE;
	do {
		item = ModalDialog(mainMsgPort, gridSizeDlg, GridSizeDialogFilter);
		switch (item) {
		case OK_BUTTON:
		case CANCEL_BUTTON:
			done = TRUE;
			break;
		case GRIDUP_ARROW:
			if (index < NUM_GRIDINDEX - 1)
				index++;
			ShowGridSize(gridSizeDlg, index);
			break;
		case GRIDDOWN_ARROW:
			if (index > 0)
				index--;
			ShowGridSize(gridSizeDlg, index);
			break;
		}
	} while (!done);
	DisposeDialog(gridSizeDlg);
	EndWait();
	if (item == OK_BUTTON)
		docData->GridUnitsIndex = index;
	return (item == OK_BUTTON);
}

/*
 *	Draw specified pen item in dialog, hiliting if selected
 */

static void DrawPenItem(DialogPtr dlg, WORD penNum, RGBColor color, BOOL hilite)
{
	WORD x, y, width, height;
	RastPtr rPort;
	GadgetPtr gadget;
	Rectangle rect;

	rPort = dlg->RPort;
	gadget = GadgetItem(dlg->FirstGadget, PENCOLORS_USERITEM);
	width = gadget->Width;
	height = gadget->Height;
	x = gadget->LeftEdge + penNum*width/NUM_PENS;
	y = gadget->TopEdge;
	SetRect(&rect,  x, y, x + width/NUM_PENS - 1, y + height - 1);
	if (hilite) {
		SetAPen(rPort, blackColor);
		FrameRect(rPort, &rect, 1, 1);
		InsetRect(&rect, 1, 1);
		FrameRect(rPort, &rect, 1, 1);
	}
	else {
		SetAPen(rPort, whiteColor);
		FrameRect(rPort, &rect, 1, 1);
	}
	InsetRect(&rect, 1, 1);
	RGBForeColor(rPort, color);
	FillRect(rPort, &rect, NULL);
}

/*
 *	Pen colors dialog filter
 */

static BOOL PenColorsDialogFilter(IntuiMsgPtr intuiMsg, WORD *item)
{
	register WORD itemHit;
	register ULONG class;

	class = intuiMsg->Class;
	if (intuiMsg->IDCMPWindow == penColorsDlg && (class == GADGETDOWN || class == GADGETUP)) {
		itemHit = GadgetNumber((GadgetPtr) intuiMsg->IAddress);
		if (itemHit == PENCOLORS_USERITEM) {
			ReplyMsg((MsgPtr) intuiMsg);
			*item = (class == GADGETDOWN) ? itemHit : -1;
			return (TRUE);
		}
	}
	return (DialogFilter(intuiMsg, item));
}

/*
 *	Adjust pen colors
 */

static BOOL DoPenColors()
{
	WORD i, item, selColor;
	BOOL done, doubleClick;
	ULONG secs, micros, prevSecs, prevMicros;
	GadgetPtr colorsGadg;
	RastPtr rPort;
	BorderPtr border;
	RGBColor newColor, colors[NUM_PENS];

	for (i = 0; i < NUM_PENS; i++)
		colors[i] = penColors[i];
/*
	Bring up dialog
*/
	BeginWait();
	if ((penColorsDlg = GetDialog(dlgList[DLG_PENCOLORS], screen, mainMsgPort)) == NULL) {
		EndWait();
		Error(ERR_NO_MEM);
		return (FALSE);
	}
	OutlineOKButton(penColorsDlg);
	rPort = penColorsDlg->RPort;
	rPort->RP_User = (APTR *) &scrnInvColorTable;
	colorsGadg = GadgetItem(penColorsDlg->FirstGadget, PENCOLORS_USERITEM);
	colorsGadg->Activation |= GADGIMMEDIATE;
	selColor = 0;
/*
	Draw dark border around pens and draw pen colors
*/
	if ((border = BoxBorder(colorsGadg->Width, colorsGadg->Height, darkColor, -1)) != NULL) {
		DrawBorder(rPort, border, colorsGadg->LeftEdge, colorsGadg->TopEdge);
		FreeBorder(border);
	}
	for (i = 0; i < NUM_PENS; i++)
		DrawPenItem(penColorsDlg, i, colors[i], (i == selColor));
/*
	Handle dialog
*/
	prevSecs = prevMicros = 0;
	done = FALSE;
	do {
		item = ModalDialog(mainMsgPort, penColorsDlg, PenColorsDialogFilter);
		CurrentTime(&secs, &micros);
		switch (item) {
		case OK_BUTTON:
		case CANCEL_BUTTON:
			done = TRUE;
			break;
		case PENCOLORS_USERITEM:
			i = (penColorsDlg->MouseX - colorsGadg->LeftEdge)*NUM_PENS/colorsGadg->Width;
			if (i < 0 || i >= NUM_PENS)
				break;
			if (i != selColor) {
				DrawPenItem(penColorsDlg, selColor, colors[selColor], FALSE);
				selColor = i;
				DrawPenItem(penColorsDlg, selColor, colors[selColor], TRUE);
				doubleClick = FALSE;
			}
			else
				doubleClick = DoubleClick(prevSecs, prevMicros, secs, micros);
			if (!doubleClick) {
				prevSecs = secs;
				prevMicros = micros;
				break;
			}
			item = CHANGEPEN_BTN;		/* Fall through */
		case CHANGEPEN_BTN:
			if (GetColor(screen, mainMsgPort, DialogFilter, strPenColor, ColorCorrect,
						 colors[selColor], &newColor, -1)) {
				colors[selColor] = newColor;
				DrawPenItem(penColorsDlg, selColor, newColor, TRUE);
			}
			break;
		}
	} while (!done);
	DisposeDialog(penColorsDlg);
	EndWait();
/*
	Change pen colors
*/
	if (item == OK_BUTTON) {
		for (i = 0; i < NUM_PENS; i++)
			penColors[i] = colors[i];
		defaults.PenColor = penColors[selColor];
		defaults.ObjFlags |= OBJ_DO_PEN;
		DrawPenWindow();
	}
	return (item == OK_BUTTON);
}

/*
 *	Draw specified fill item in dialog, hiliting if selected
 */

static void DrawFillItem(DialogPtr dlg, WORD fillNum, RGBPat8Ptr pat, BOOL hilite)
{
	WORD x, y, width, height;
	RastPtr rPort;
	GadgetPtr gadget;
	Rectangle rect;

	rPort = dlg->RPort;
	gadget = GadgetItem(dlg->FirstGadget, FILLPATS_USERITEM);
	width = gadget->Width;
	height = gadget->Height;
	x = gadget->LeftEdge + (fillNum % (NUM_FILLPATS/2))*width/(NUM_FILLPATS/2);
	y = gadget->TopEdge + (fillNum/(NUM_FILLPATS/2))*height/2;
	SetRect(&rect,  x, y, x + width/(NUM_FILLPATS/2) - 1, y + height/2 - 1);
	if (hilite) {
		SetAPen(rPort, blackColor);
		FrameRect(rPort, &rect, 1, 1);
		InsetRect(&rect, 1, 1);
		FrameRect(rPort, &rect, 1, 1);
	}
	else {
		SetAPen(rPort, whiteColor);
		FrameRect(rPort, &rect, 1, 1);
	}
	InsetRect(&rect, 1, 1);
	FillRect(rPort, &rect, pat);
}

/*
 *	Draw pattern in edit and sample boxes
 */

static void ShowEditPat(DialogPtr dlg, RGBPat8Ptr pat)
{
	WORD x, y, width, height;
	GadgetPtr gadget;
	RastPtr rPort = dlg->RPort;
	Rectangle rect;

/*
	Fill in sample box
*/
	gadget = GadgetItem(dlg->FirstGadget, SAMPLEPAT_USERITEM);
	SetRect(&rect, gadget->LeftEdge, gadget->TopEdge,
			gadget->LeftEdge + gadget->Width - 1,
			gadget->TopEdge + gadget->Height - 1);
	FillRect(rPort, &rect, pat);
/*
	Fill in edit box
*/
	gadget = GadgetItem(dlg->FirstGadget, EDITPAT_USERITEM);
	width = gadget->Width;
	height = gadget->Height;
	for (y = 0; y < 8; y++) {
		rect.MinY = (y*height)/8 + gadget->TopEdge;
		rect.MaxY = ((y + 1)*height)/8 + gadget->TopEdge - 1;
		for (x = 0; x < 8; x++) {
			rect.MinX = (x*width)/8 + gadget->LeftEdge;
			rect.MaxX = ((x + 1)*width)/8 + gadget->LeftEdge - 1;
			RGBForeColor(rPort, (*pat)[y*8 + x]);
			FillRect(rPort, &rect, NULL);
		}
	}
}

/*
 *	Draw single pixel of edit pattern and entire sample box
 */

static void DrawEditPixel(DialogPtr dlg, RGBPat8Ptr pat, WORD x, WORD y)
{
	GadgetPtr gadget;
	RastPtr rPort = dlg->RPort;
	Rectangle rect;

/*
	Draw pixel in edit box
*/
	gadget = GadgetItem(dlg->FirstGadget, EDITPAT_USERITEM);
	rect.MinX = (x*gadget->Width)/8 + gadget->LeftEdge;
	rect.MaxX = ((x + 1)*gadget->Width)/8 + gadget->LeftEdge - 1;
	rect.MinY = (y*gadget->Height)/8 + gadget->TopEdge;
	rect.MaxY = ((y + 1)*gadget->Height)/8 + gadget->TopEdge - 1;
	RGBForeColor(rPort, (*pat)[y*8 + x]);
	FillRect(rPort, &rect, NULL);
/*
	Fill in sample box
*/
	gadget = GadgetItem(dlg->FirstGadget, SAMPLEPAT_USERITEM);
	SetRect(&rect, gadget->LeftEdge, gadget->TopEdge,
			gadget->LeftEdge + gadget->Width - 1,
			gadget->TopEdge + gadget->Height - 1);
	FillRect(rPort, &rect, pat);
}

/*
 *	Fill patterns dialog filter
 */

static BOOL FillPatsDialogFilter(IntuiMsgPtr intuiMsg, WORD *item)
{
	register WORD itemHit;
	register ULONG class;

	class = intuiMsg->Class;
	if (intuiMsg->IDCMPWindow == fillPatsDlg && (class == GADGETDOWN || class == GADGETUP)) {
		itemHit = GadgetNumber((GadgetPtr) intuiMsg->IAddress);
		if (itemHit == PENCOLORS_USERITEM || itemHit == FILLPATS_USERITEM ||
			itemHit == EDITPAT_USERITEM) {
			ReplyMsg((MsgPtr) intuiMsg);
			*item = (class == GADGETDOWN) ? itemHit : -1;
			return (TRUE);
		}
	}
	return (DialogFilter(intuiMsg, item));
}

/*
 *	Adjust fill patterns
 */

static BOOL DoFillPatterns()
{
	WORD i, item, selColor, selPat;
	WORD x, y, prevX, prevY, mouseX, mouseY;
	BOOL done, doubleClick;
	ULONG secs, micros, prevSecs, prevMicros;
	GadgetPtr gadgList, colorsGadg, patsGadg, editGadg, sampleGadg;
	RastPtr rPort;
	BorderPtr border;
	RGBPat8Ptr pats;
	RGBColor newColor, colors[NUM_PENS];

	if ((pats = MemAlloc(NUM_FILLPATS*sizeof(RGBPat8), 0)) == NULL)
		return (FALSE);
	for (i = 0; i < NUM_PENS; i++)
		colors[i] = penColors[i];
	BlockMove(fillPats, pats, NUM_FILLPATS*sizeof(RGBPat8));
/*
	Bring up dialog
*/
	BeginWait();
	if ((fillPatsDlg = GetDialog(dlgList[DLG_FILLPATTERNS], screen, mainMsgPort)) == NULL) {
		EndWait();
		Error(ERR_NO_MEM);
		return (FALSE);
	}
	OutlineOKButton(fillPatsDlg);
	rPort = fillPatsDlg->RPort;
	rPort->RP_User = (APTR *) &scrnInvColorTable;
	ModifyIDCMP(fillPatsDlg, fillPatsDlg->IDCMPFlags | MOUSEMOVE);
	gadgList = fillPatsDlg->FirstGadget;
	colorsGadg = GadgetItem(gadgList, PENCOLORS_USERITEM);
	colorsGadg->Activation |= GADGIMMEDIATE;
	patsGadg = GadgetItem(gadgList, FILLPATS_USERITEM);
	patsGadg->Activation |= GADGIMMEDIATE;
	editGadg = GadgetItem(gadgList, EDITPAT_USERITEM);
	editGadg->Activation |= GADGIMMEDIATE | FOLLOWMOUSE | RELVERIFY;	/* Need RELVERIFY */
	sampleGadg = GadgetItem(gadgList, SAMPLEPAT_USERITEM);
	selColor = selPat = 0;
/*
	Draw borders and contents of user items
*/
	if ((border = BoxBorder(colorsGadg->Width, colorsGadg->Height, darkColor, -1)) != NULL) {
		DrawBorder(rPort, border, colorsGadg->LeftEdge, colorsGadg->TopEdge);
		FreeBorder(border);
	}
	if ((border = BoxBorder(patsGadg->Width, patsGadg->Height, darkColor, -1)) != NULL) {
		DrawBorder(rPort, border, patsGadg->LeftEdge, patsGadg->TopEdge);
		FreeBorder(border);
	}
	if ((border = BoxBorder(editGadg->Width, editGadg->Height, darkColor, -1)) != NULL) {
		DrawBorder(rPort, border, editGadg->LeftEdge, editGadg->TopEdge);
		DrawBorder(rPort, border, sampleGadg->LeftEdge, sampleGadg->TopEdge);
		FreeBorder(border);
	}
	for (i = 0; i < NUM_PENS; i++)
		DrawPenItem(fillPatsDlg, i, colors[i], (i == selColor));
	for (i = 0; i < NUM_FILLPATS; i++)
		DrawFillItem(fillPatsDlg, i, &pats[i], (i == selPat));
	ShowEditPat(fillPatsDlg, &pats[selPat]);
/*
	Handle dialog
*/
	prevSecs = prevMicros = 0;
	done = FALSE;
	do {
		item = ModalDialog(mainMsgPort, fillPatsDlg, FillPatsDialogFilter);
		CurrentTime(&secs, &micros);
		mouseX = fillPatsDlg->MouseX;
		mouseY = fillPatsDlg->MouseY;
		switch (item) {
		case OK_BUTTON:
		case CANCEL_BUTTON:
			done = TRUE;
			break;
		case PENCOLORS_USERITEM:
			i = (mouseX - colorsGadg->LeftEdge)*NUM_PENS/colorsGadg->Width;
			if (i < 0 || i >= NUM_PENS)
				break;
			if (i != selColor) {
				DrawPenItem(fillPatsDlg, selColor, colors[selColor], FALSE);
				selColor = i;
				DrawPenItem(fillPatsDlg, selColor, colors[selColor], TRUE);
				doubleClick = FALSE;
			}
			else
				doubleClick = DoubleClick(prevSecs, prevMicros, secs, micros);
			if (!doubleClick) {
				prevSecs = secs;
				prevMicros = micros;
				break;
			}
			item = CHANGEPEN_BTN;		/* Fall through */
		case CHANGEPEN_BTN:
			if (GetColor(screen, mainMsgPort, DialogFilter, strPenColor, ColorCorrect,
						 colors[selColor], &newColor, -1)) {
				colors[selColor] = newColor;
				DrawPenItem(fillPatsDlg, selColor, newColor, TRUE);
			}
			break;
		case FILLPATS_USERITEM:
			x = (mouseX - patsGadg->LeftEdge)*(NUM_FILLPATS/2)/(patsGadg->Width);
			y = (mouseY - patsGadg->TopEdge)*2/(patsGadg->Height);
			if (x < 0 || x >= NUM_FILLPATS/2 || y < 0 || y > 1)
				break;
			i = x + (NUM_FILLPATS/2)*y;
			if (i != selPat) {
				DrawFillItem(fillPatsDlg, selPat, &pats[selPat], FALSE);
				selPat = i;
				DrawFillItem(fillPatsDlg, selPat, &pats[selPat], TRUE);
				ShowEditPat(fillPatsDlg, &pats[selPat]);
			}
			break;
		case EDITPAT_USERITEM:
			prevX = prevY = -1;
			do {
				x = (fillPatsDlg->MouseX - editGadg->LeftEdge)*8/editGadg->Width;
				y = (fillPatsDlg->MouseY - editGadg->TopEdge)*8/editGadg->Height;
				if (x < 0 || x > 7 || y < 0 || y > 7)
					continue;
				if (x != prevX || y != prevY) {
					pats[selPat][y*8 + x] = colors[selColor];
					DrawEditPixel(fillPatsDlg, &pats[selPat], x, y);
					prevX = x;
					prevY = y;
				}
			} while (WaitMouseUp(mainMsgPort, fillPatsDlg));
			break;
		case CLEAR_BTN:
			for (i = 0; i < 64; i++)
				pats[selPat][i] = colors[selColor];
			ShowEditPat(fillPatsDlg, &pats[selPat]);
			break;
		case REVERT_BTN:
			CopyRGBPat8(&fillPats[selPat], &pats[selPat]);
			ShowEditPat(fillPatsDlg, &pats[selPat]);
			break;
		}
	} while (!done);
	DisposeDialog(fillPatsDlg);
	EndWait();
/*
	Change patterns
*/
	if (item == OK_BUTTON) {
		BlockMove(pats, fillPats, NUM_FILLPATS*sizeof(RGBPat8));
		CopyRGBPat8(&fillPats[selPat], &defaults.FillPat);
		defaults.ObjFlags |= OBJ_DO_FILL;
		DrawFillWindow();
	}
	MemFree(pats, NUM_FILLPATS*sizeof(RGBPat8));
	return (item == OK_BUTTON);
}

/*
 *	Show names of document layers in scroll box
 */

static void ShowLayerNames(DocDataPtr docData, DialogPtr dlg, ScrollListPtr scrollList,
						   DocLayerPtr selLayer)
{
	WORD i, numLayers;
	TextPtr name;
	DocLayerPtr docLayer;
	TextChar text[20];

	SLDoDraw(scrollList, FALSE);
	SLRemoveAll(scrollList);
	numLayers = NumLayers(docData);
	i = 0;
	for (docLayer = TopLayer(docData); docLayer; docLayer = PrevLayer(docLayer)) {
		if ((name = GetLayerName(docLayer)) == NULL) {
			strcpy(text, "(");
			strcat(text, strLayer);
			NumToString(numLayers - i, text + strlen(text));
			strcat(text, ")");
			name = text;
		}
		SLAddItem(scrollList, name, strlen(name), i);
		if ((docLayer->Flags & LAYER_VISIBLE) == 0)
			SLSetItemStyle(scrollList, i, FSF_ITALIC);
		i++;
	}
	i = numLayers - LayerNum(docData, selLayer) - 1;
	SLSelectItem(scrollList, i, TRUE);
	SLAutoScroll(scrollList, i);
	SLDoDraw(scrollList, TRUE);
}

/*
 *	Set layers dialog buttons
 */

static void SetLayersButtons(DialogPtr dlg, DocLayerPtr selLayer)
{
	BOOL hasPrev, hasNext, visible;
	GadgetPtr gadgList = dlg->FirstGadget;

	hasPrev = (selLayer && PrevLayer(selLayer) != NULL);
	hasNext = (selLayer && NextLayer(selLayer) != NULL);
	visible = (selLayer && LayerVisible(selLayer));
	EnableGadgetItem(gadgList, DELETE_BTN, dlg, NULL, (hasPrev || hasNext));
	EnableGadgetItem(gadgList, MOVEUP_ARROW, dlg, NULL, hasNext);
	EnableGadgetItem(gadgList, MOVEDOWN_ARROW, dlg, NULL, hasPrev);
	EnableGadgetItem(gadgList, SHOW_BTN, dlg, NULL, !visible);
	EnableGadgetItem(gadgList, HIDE_BTN, dlg, NULL, visible);
}

/*
 *	Layers dialog filter
 */

static BOOL LayersDialogFilter(register IntuiMsgPtr intuiMsg, WORD *item)
{
	WORD gadgNum, selItem;
	ULONG class = intuiMsg->Class;
	UWORD code = intuiMsg->Code;

	switch (class) {
/*
	Handle gadget down of up/down scroll arrows and move arrows
*/
	case GADGETDOWN:
	case GADGETUP:
		gadgNum = GadgetNumber((GadgetPtr) intuiMsg->IAddress);
		if (gadgNum == LAYERS_LIST || gadgNum == LAYERS_SCROLL ||
			gadgNum == LAYERS_UP || gadgNum == LAYERS_DOWN) {
			SLGadgetMessage(layersScrollList, mainMsgPort, intuiMsg);
			*item = gadgNum;
			return (TRUE);
		}
		if (gadgNum == MOVEUP_ARROW || gadgNum == MOVEDOWN_ARROW) {
			ReplyMsg((MsgPtr) intuiMsg);
			*item = (class == GADGETDOWN) ? gadgNum : -1;
			return (TRUE);
		}
		break;
/*
	Handle keyboard events
*/
	case RAWKEY:
		if (code == CURSORUP || code == CURSORDOWN) {
			ReplyMsg((MsgPtr) intuiMsg);
			selItem = SLNextSelect(layersScrollList, -1);
			if (selItem == -1)
				selItem = 0;
			else if (code == CURSORUP)
				selItem--;
			else
				selItem++;
			SLSelectItem(layersScrollList, selItem, TRUE);
			return (TRUE);
		}
		break;
	}
	return (DialogFilter(intuiMsg, item));
}

/*
 *	Modify document layers
 */

static BOOL DoLayers(WindowPtr window)
{
	WORD item, selLayerNum;
	BOOL done, success;
	GadgetPtr gadgList, gadget;
	DocLayerPtr selLayer, docLayer;
	DocDataPtr docData = GetWRefCon(window);
	Rectangle rect;
	TextChar nameText[GADG_MAX_STRING];

	success = FALSE;
	layersScrollList = NULL;
	layersDlg = NULL;
/*
	Bring layers dialog
*/
	BeginWait();
	if ((layersScrollList = NewScrollList(FALSE)) == NULL ||
		(layersDlg = GetDialog(dlgList[DLG_LAYERS], screen, mainMsgPort)) == NULL) {
		EndWait();
		Error(ERR_NO_MEM);
		goto Exit;
	}
	gadgList = layersDlg->FirstGadget;
	InitScrollList(layersScrollList, GadgetItem(gadgList, LAYERS_LIST), layersDlg, NULL);
	OutlineOKButton(layersDlg);
	SLDrawBorder(layersScrollList);
	DrawArrowBorder(layersDlg, MOVEUP_ARROW);
	selLayer = CurrLayer(docData);
	ShowLayerNames(docData, layersDlg, layersScrollList, selLayer);
	SetLayersButtons(layersDlg, selLayer);
	SetEditItemText(layersDlg, NULL, NAME_TEXT, GetLayerName(selLayer));
/*
	Handle dialog
*/
	done = FALSE;
	do {
		item = ModalDialog(mainMsgPort, layersDlg, LayersDialogFilter);
		selLayerNum = NumLayers(docData) - SLNextSelect(layersScrollList, -1) - 1;
		selLayer = GetLayer(docData, selLayerNum);
		GetEditItemText(gadgList, NAME_TEXT, nameText);
		switch (item) {
		case LAYERS_LIST:
			SetEditItemText(layersDlg, NULL, NAME_TEXT, GetLayerName(selLayer));
			if (!SLDoubleClick(layersScrollList))
				break;
			item = DONE_BTN;				/* Fall through */
		case DONE_BTN:
			if (LayerVisible(selLayer))
				docLayer = selLayer;
			else {
				docLayer = NextVisLayer(selLayer);
				if (docLayer == NULL)
					docLayer = PrevVisLayer(selLayer);
			}
			if (docLayer == NULL) {
				Error(ERR_NO_VIS_LAYER);
				break;
			}
			SetCurrLayer(docData, docLayer);
			SetLayerIndic(window);
			AdjustWindowTitle(window);
			GetContentRect(window, &rect);
			InvalRect(window, &rect);
			done = TRUE;
			break;
		case NEW_BTN:
			docLayer = NewDocLayer(docData, (nameText[0]) ? nameText : NULL);
			if (docLayer) {
				ShowLayerNames(docData, layersDlg, layersScrollList, docLayer);
				DocModified(docData);
			}
			break;
		case RENAME_BTN:
			SetLayerName(selLayer, (nameText[0]) ? nameText : NULL);
			ShowLayerNames(docData, layersDlg, layersScrollList, selLayer);
			DocModified(docData);
			break;
		case DELETE_BTN:
			if (NumLayers(docData) <= 1) {
				Error(ERR_LAST_LAYER);
				break;
			}
			if (BottomObject(selLayer) != NULL &&
				StdDialog(DLG_DELETELAYER) != OK_BUTTON)
				break;
			docLayer = NextLayer(selLayer);
			if (docLayer == NULL)
				docLayer = PrevLayer(selLayer);
			DetachLayer(docData, selLayer);
			DisposeDocLayer(selLayer);
			ShowLayerNames(docData, layersDlg, layersScrollList, docLayer);
			DocModified(docData);
			break;
		case HIDE_BTN:
		case SHOW_BTN:
			if (item == HIDE_BTN)
				HideLayer(selLayer);
			else
				ShowLayer(selLayer);
			ShowLayerNames(docData, layersDlg, layersScrollList, selLayer);
			DocModified(docData);
			break;
		case MOVEUP_ARROW:
		case MOVEDOWN_ARROW:
			if (item == MOVEUP_ARROW)
				LayerForward(docData, selLayer);
			else
				LayerBackward(docData, selLayer);
			ShowLayerNames(docData, layersDlg, layersScrollList, selLayer);
			DocModified(docData);
			break;
		}
		if (!done && item != -1) {
			gadget = GadgetItem(gadgList, NAME_TEXT);
			ActivateGadget(gadget, layersDlg, NULL);
			selLayerNum = NumLayers(docData) - SLNextSelect(layersScrollList, -1) - 1;
			selLayer = GetLayer(docData, selLayerNum);
			SetLayersButtons(layersDlg, selLayer);
		}
	} while (!done);
	DisposeDialog(layersDlg);
	EndWait();
	success = TRUE;
/*
	Clean up
*/
Exit:
	if (layersScrollList)
		DisposeScrollList(layersScrollList);
	return (success);
}

/*
 *	Find minimum drawing size that will contain all objects
 */

static void MinDrawSize(DocDataPtr docData, WORD *minWidth, WORD *minHeight)
{
	DocObjPtr docObj;
	DocLayerPtr docLayer;

	*minWidth = docData->PageWidth;
	*minHeight = docData->PageHeight;
	for (docLayer = BottomLayer(docData); docLayer; docLayer = NextLayer(docLayer)) {
		for (docObj = BottomObject(docLayer); docObj; docObj = NextObj(docObj)) {
			if (*minWidth <= docObj->Frame.MaxX)
				*minWidth = docObj->Frame.MaxX + 1;
			if (*minHeight <= docObj->Frame.MaxY)
				*minHeight = docObj->Frame.MaxY + 1;
		}
	}
	if (*minWidth > MAX_DOCWIDTH)
		*minWidth = MAX_DOCWIDTH;
	if (*minHeight > MAX_DOCHEIGHT)
		*minHeight = MAX_DOCHEIGHT;
}

/*
 *	Show current drawing size in draw size dialog
 */

static void ShowDrawSize(DialogPtr dlg, DocDataPtr docData, WORD docWidth, WORD docHeight)
{
	WORD x, y, minX, minY, maxX, maxY, docX, docY;
	WORD page, pages, pagesAcross, pagesDown;
	RastPtr rPort = dlg->RPort;
	GadgetPtr gadget, gadgList;
	BorderPtr border;

	gadgList = GadgetItem(dlg->FirstGadget, 0);		/* First user gadget */
	gadget = GadgetItem(gadgList, DRAWSIZE_USERITEM);
	minX = gadget->LeftEdge;
	minY = gadget->TopEdge;
	maxX = minX + gadget->Width - 1;
	maxY = minY + gadget->Height - 1;
/*
	Draw black border around size area
*/
	if ((border = BoxBorder(gadget->Width, gadget->Height, blackColor, -1)) != NULL) {
		DrawBorder(rPort, border, minX, minY);
		FreeBorder(border);
	}
/*
	First fill in pages used
	Do it this way to avoid flickering
*/
	docX = minX + ((LONG) docWidth*gadget->Width + MAX_DOCWIDTH/2)/MAX_DOCWIDTH - 1;
	docY = minY + ((LONG) docHeight*gadget->Height + MAX_DOCHEIGHT/2)/MAX_DOCHEIGHT - 1;
	SetDrMd(rPort, JAM1);
	SetAPen(rPort, whiteColor);
	RectFill(rPort, minX, minY, docX, docY);
	SetAPen(rPort, darkColor);
	if (docX < maxX)
		RectFill(rPort, docX + 1, minY, maxX, maxY);
	if (docY < maxY)
		RectFill(rPort, minX, docY + 1, maxX, maxY);
/*
	Now draw lines between pages
*/
	SetAPen(rPort, blackColor);
	pages = docWidth/docData->PageWidth;
	for (page = 1; page <= pages; page++) {
		x = ((LONG) page*docData->PageWidth*gadget->Width + MAX_DOCWIDTH/2)/MAX_DOCWIDTH;
		if (x >= gadget->Width)
			x = gadget->Width - 1;
		Move(rPort, minX + x, minY);
		Draw(rPort, minX + x, docY);
	}
	pages = docHeight/docData->PageHeight;
	for (page = 1; page <= pages; page++) {
		y = ((LONG) page*docData->PageHeight*gadget->Height + MAX_DOCHEIGHT/2)/MAX_DOCHEIGHT;
		if (y >= gadget->Height)
			y = gadget->Height - 1;
		Move(rPort, minX, minY + y);
		Draw(rPort, docX, minY + y);
	}
/*
	Show dimensions
*/
	pagesAcross = (docWidth - 1)/docData->PageWidth + 1;
	pagesDown = (docHeight - 1)/docData->PageHeight + 1;
	DotToText(docWidth, docData->xDPI, strBuff, 2);
	strcat(strBuff, (options.MeasureUnit == MEASURE_CM) ? strCM : strInch);
	SetGadgetItemText(gadgList, WIDTH_TEXT, dlg, NULL, strBuff);
	DotToText(docHeight, docData->yDPI, strBuff, 2);
	strcat(strBuff, (options.MeasureUnit == MEASURE_CM) ? strCM : strInch);
	SetGadgetItemText(gadgList, HEIGHT_TEXT, dlg, NULL, strBuff);
	NumToString(pagesAcross*pagesDown, strBuff);
	SetGadgetItemText(gadgList, PAGES_TEXT, dlg, NULL, strBuff);
}

/*
 *	Drawing size dialog filter
 */

static BOOL DrawSizeDialogFilter(IntuiMsgPtr intuiMsg, WORD *item)
{
	register WORD itemHit;
	register ULONG class;

	class = intuiMsg->Class;
	if (intuiMsg->IDCMPWindow == drawSizeDlg && (class == GADGETDOWN || class == GADGETUP)) {
		itemHit = GadgetNumber((GadgetPtr) intuiMsg->IAddress);
		if (itemHit == DRAWSIZE_USERITEM) {
			ReplyMsg((MsgPtr) intuiMsg);
			*item = (class == GADGETDOWN) ? itemHit : -1;
			return (TRUE);
		}
	}
	return (DialogFilter(intuiMsg, item));
}

/*
 *	Set drawing size
 */

static BOOL DoDrawingSize(WindowPtr window)
{
	WORD item;
	WORD pageWidth, pageHeight, minWidth, minHeight, docWidth, docHeight, pages;
	WORD prevWidth, prevHeight;
	LONG x, y;						/* To force LONG calculations */
	GadgetPtr sizeGadg;
	DocDataPtr docData = GetWRefCon(window);
	Rectangle rect;

	pageWidth = docData->PageWidth;
	pageHeight = docData->PageHeight;
	docWidth = docData->DocWidth;
	docHeight = docData->DocHeight;
	MinDrawSize(docData, &minWidth, &minHeight);
	if (docWidth < minWidth)
		docWidth = minWidth;
	if (docHeight < minHeight)
		docHeight = minHeight;
/*
	Set up drawing size dialog
*/
	BeginWait();
	if ((drawSizeDlg = GetDialog(dlgList[DLG_DRAWSIZE], screen, mainMsgPort)) == NULL) {
		EndWait();
		Error(ERR_NO_MEM);
		return (FALSE);
	}
	OutlineOKButton(drawSizeDlg);
	ShowDrawSize(drawSizeDlg, docData, docWidth, docHeight);
	sizeGadg = GadgetItem(drawSizeDlg->FirstGadget, DRAWSIZE_USERITEM);
	sizeGadg->Activation |= GADGIMMEDIATE;
/*
	Handle dialog
*/
	for (;;) {
		item = ModalDialog(mainMsgPort, drawSizeDlg, DrawSizeDialogFilter);
		if (item == OK_BUTTON || item == CANCEL_BUTTON)
			break;
		if (item == DRAWSIZE_USERITEM) {
			do {
				prevWidth  = docWidth;
				prevHeight = docHeight;
				x = drawSizeDlg->MouseX - sizeGadg->LeftEdge;
				y = drawSizeDlg->MouseY - sizeGadg->TopEdge;
				docWidth = (x*MAX_DOCWIDTH)/sizeGadg->Width;
				if (docWidth < minWidth)
					docWidth = minWidth;
				pages = (docWidth + pageWidth - 1)/pageWidth;
				docWidth = pages*pageWidth;
				if (docWidth > MAX_DOCWIDTH)
					docWidth = MAX_DOCWIDTH;
				docHeight = (y*MAX_DOCHEIGHT)/sizeGadg->Height;
				if (docHeight < minHeight)
					docHeight = minHeight;
				pages = (docHeight + pageHeight - 1)/pageHeight;
				docHeight = pages*pageHeight;
				if (docHeight > MAX_DOCHEIGHT)
					docHeight = MAX_DOCHEIGHT;
				if (docWidth != prevWidth || docHeight != prevHeight)
					ShowDrawSize(drawSizeDlg, docData, docWidth, docHeight);
			} while (WaitMouseUp(mainMsgPort, drawSizeDlg));
		}
	}
	DisposeDialog(drawSizeDlg);
	EndWait();
	if (item == CANCEL_BUTTON)
		return (FALSE);
/*
	Set result
*/
	docData->DocWidth = docWidth;
	docData->DocHeight = docHeight;
	AdjustScrollOffset(window);
	AdjustScrollBars(window);
	GetContentRect(window, &rect);
	InvalRect(window, &rect);
	DocModified(docData);
	return (TRUE);
}

/*
 *	Handle Layout menu
 */

BOOL DoLayoutMenu(WindowPtr window, UWORD item, UWORD sub, UWORD modifier)
{
	BOOL success;

	if (!IsDocWindow(window) && item != PENCOLORS_ITEM &&
		item != FILLPATTERNS_ITEM && item != PREFERENCES_ITEM &&
		item != SCREENCOLORS_ITEM)
		return (FALSE);
	UndoOff();
	success = FALSE;
	switch (item) {
	case NORMALSIZE_ITEM:
		success = DoNormalSize(window);
		break;
	case ENLARGE_ITEM:
		success = DoEnlarge(window);
		break;
	case REDUCE_ITEM:
		success = DoReduce(window);
		break;
	case FITTOWINDOW_ITEM:
		success = DoFitToWindow(window);
		break;
	case GRIDSNAP_ITEM:
		success = DoGridSnap(window);
		break;
	case GRIDSIZE_ITEM:
		success = DoGridSize(window);
		break;
	case PENCOLORS_ITEM:
		success = DoPenColors();
		break;
	case FILLPATTERNS_ITEM:
		success = DoFillPatterns();
		break;
	case LAYERS_ITEM:
		success = DoLayers(window);
		break;
	case DRAWINGSIZE_ITEM:
		success = DoDrawingSize(window);
		break;
	case PREFERENCES_ITEM:
		success = DoOptions();
		break;
	case SCREENCOLORS_ITEM:
		success = DoScreenColors();
		break;
	}
	return (success);
}
