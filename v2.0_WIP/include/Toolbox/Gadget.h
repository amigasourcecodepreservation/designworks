/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Toolbox library
 *	Copyright (c) 1987-92 New Horizons Software
 *
 *	Gadget template definitions
 */

#ifndef TOOLBOX_GADGET_H
#define TOOLBOX_GADGET_H

#ifndef INTUITION_INTUITION_H
#include <intuition/intuition.h>
#endif

#ifndef TOOLBOX_MEMORY_H
#include <ToolBox/Memory.h>
#endif

#ifndef TYPEDEFS_H
#include <Typedefs.h>
#endif

/*
 *	Gadget template
 *
 *	LeftEdge and TopEdge are relative to upper left of containing item
 *	Type field is gadget type from list below (NOT intuition defines)
 *	Info field has following definition for each gadget type:
 *
 *	GADG_USER_ITEM			Pointer to Gadget structure
 *	GADG_PUSH_BUTTON		Pointer to text of item
 *	GADG_CHECK_BOX			"
 *	GADG_RADIO_BUTTON		"
 *	GADG_STAT_TEXT			"
 *	GADG_EDIT_TEXT			Pointer to initial text of item
 *	GADG_STAT_BORDER		Pointer to Border structure
 *	GADG_ACTIVE_BORDER		"
 *	GADG_STAT_IMAGE			Pointer to Image structure
 *	GADG_ACTIVE_IMAGE		"
 *	GADG_STAT_STDIMAGE		Item number of standard image
 *	GADG_ACTIVE_STDIMAGE	"
 *	GADG_PROP_HORIZ			Ignored
 *	GADG_PROP_VERT			"
 *	GADG_POPUP				Pointer to text entries, ended by NULL entry
 *
 *	LeftEdge, TopEdge, Width, and Height are relative to current font x & y size,
 *		normalized to 10 pixels for an 8 by 11 pixel font
 *
 *	Offsets are absolute values that are added to the relative values
 *
 *	If LeftEdge and/or TopEdge are negative, then gadget is specified to be
 *		GRELRIGHT and/or GRELBOTTOM respectively
 *
 *	If Width and/or Height are negative, then gadget is specified to be
 *		GRELWIDTH and/or GRELHEIGHT respectively
 *
 *	The actual template is an array of items, with the last item having
 *		a type of GADG_ITEM_NONE
 */

typedef struct {
	UWORD		Type;
	WORD		LeftEdge, TopEdge, LeftOffset, TopOffset;
	WORD		Width, Height, WidthOffset, HeightOffset;
	TextChar	KeyEquiv;
	UBYTE		Value;			// Initial value
	Ptr			Info;
} GadgetTemplate, *GadgTemplPtr;

/*
 *	Gadget types
 */

#define GADG_TYPEBITS			0xFF

#define GADG_USER_ITEM			0x00

#define GADG_PUSH_BUTTON		0x10
#define GADG_CHECK_BOX			0x11
#define GADG_RADIO_BUTTON		0x12

#define GADG_STAT_TEXT			0x20

#define GADG_EDIT_TEXT			0x30

#define GADG_STAT_BORDER		0x40
#define GADG_ACTIVE_BORDER		0x41
#define GADG_STAT_IMAGE			0x42
#define GADG_ACTIVE_IMAGE		0x43
#define GADG_STAT_STDBORDER		0x44
#define GADG_ACTIVE_STDBORDER	0x45
#define GADG_STAT_STDIMAGE		0x46
#define GADG_ACTIVE_STDIMAGE	0x47

#define GADG_PROP_VERT			0x50
#define GADG_PROP_HORIZ			0x51

#define GADG_POPUP				0x60

#define GADG_STAT_ICON			0x70
#define GADG_ACTIVE_ICON		0x71

/*
 *	Gadget flags (contained in the Type field)
 */

#define GADG_FLAGBITS			0xFF00

#define GADG_PROP_NOBORDER		0x100		// Flags for PROP gadgets
#define GADG_PROP_NEWLOOK		0x200

#define GADG_EDIT_NUMONLY		0x100		// Flags for EDIT_TEXT gadgets
#define GADG_EDIT_RETURNCYCLE	0x200

/*
 *	This item type identifies the end of the gadget template array
 */

#define GADG_ITEM_NONE		0xFFFF

/*
 *	Size of buffer needed for all string gadgets (type GADG_EDIT_TEXT)
 */

#define GADG_MAX_STRING		100

/*
 *	Prototypes
 */

GadgetPtr	GetGadgets(GadgTemplPtr);
void		DisposeGadgets(GadgetPtr);

GadgetPtr	GadgetItem(GadgetPtr, WORD);
WORD		GadgetNumber(GadgetPtr);
WORD		GadgetType(GadgetPtr);

WORD	GetGadgetValue(GadgetPtr);
void	SetGadgetValue(GadgetPtr, WindowPtr, RequestPtr, WORD);
void	SetGadgetItemValue(GadgetPtr, WORD, WindowPtr, RequestPtr, WORD);

void	HiliteGadget(GadgetPtr, WindowPtr, RequestPtr, BOOL);
BOOL	GadgetSelected(GadgetPtr, WORD);

void	OnOffGList(GadgetPtr, WindowPtr, RequestPtr, WORD, BOOL);
void	OnGList(GadgetPtr, WindowPtr, RequestPtr, WORD);
void	OffGList(GadgetPtr, WindowPtr, RequestPtr, WORD);
void	EnableGadgetItem(GadgetPtr, WORD, WindowPtr, RequestPtr, WORD);

void	OutlineButton(GadgetPtr, WindowPtr, RequestPtr, BOOL);

void	GetEditItemText(GadgetPtr, WORD, TextPtr);
void	SetEditItemText(GadgetPtr, WORD, WindowPtr, RequestPtr, TextPtr);

void	GetGadgetItemText(GadgetPtr, WORD, TextPtr);
void	SetGadgetText(GadgetPtr, WindowPtr, RequestPtr, TextPtr);
void	SetGadgetItemText(GadgetPtr, WORD, WindowPtr, RequestPtr, TextPtr);
void	SetButtonItem(GadgetPtr, WORD, WindowPtr, RequestPtr, TextPtr, TextChar);

void	GetGadgetRect(GadgetPtr, WindowPtr, RequestPtr, RectPtr);

void	TrackGadget(MsgPortPtr, WindowPtr, GadgetPtr, void (*)(WindowPtr, WORD));

BOOL	IsEditReturnCycle(GadgetPtr);
BOOL	IsGadgetKey(GadgetPtr, TextChar);

void	DoPopUpGadget(GadgetPtr, WindowPtr);

#endif
