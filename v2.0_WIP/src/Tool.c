/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991-93 New Horizons Software, Inc.
 *
 *	Tool window routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>
#include <proto/dos.h>			// For Delay() prototype
#include <proto/layers.h>

#include <string.h>

#include <Toolbox/Globals.h>
#include <Toolbox/Memory.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Border.h>
#include <Toolbox/Window.h>
#include <Toolbox/Utility.h>
#include <Toolbox/Gadget.h>
#include <Toolbox/ScrollList.h>

#include "Draw.h"
#include "Proto.h"

/*
 *	External variables
 */

extern WORD	intuiVersion;

extern ScreenPtr	screen;
extern MsgPort		monitorMsgPort;
extern MsgPortPtr	mainMsgPort;

extern WindowPtr	toolWindow, penWindow, fillWindow;
extern WindowPtr	cmdWindow, windowList[];
extern WORD			numWindows;

extern Palette	screenPalette;

extern TextFontPtr	smallFont;

extern WORD		drawTool;		// Current draw tool
extern PenColorPtr	penColors;
extern RGBPat8	fillPats[];
extern FillPtnPtr	fillPatterns;

extern Options	options;
extern Defaults	defaults;		// Current pen color and fill pattern

extern Image	arrowIcon, textIcon, lineIcon, eyeDropIcon;
extern Image	rectIcon, ovalIcon, curveIcon, polyIcon;

extern TextChar	strToolTitle[], strPenTitle[], strFillTitle[];
extern TextChar strNone[];
extern TextChar	strScreenTitle[];

extern UBYTE	strBuff[];

/*
 *	Local variables and definitions
 */

static struct NewWindow newExtraWindow = {
	0, 0, 640, 400, -1, -1,
	MOUSEBUTTONS | GADGETDOWN | GADGETUP | CLOSEWINDOW | MENUVERIFY | INTUITICKS,
	WINDOWCLOSE | WINDOWDRAG | WINDOWDEPTH | SMART_REFRESH,
	NULL, NULL, NULL, NULL, NULL,
	0, 0, 0xFFFF, 0xFFFF,
	CUSTOMSCREEN
};

static struct NewWindow newColorWindow = {
	0, 0, 640, 400, -1, -1,
	MOUSEBUTTONS | GADGETDOWN | GADGETUP |
	CLOSEWINDOW | MENUVERIFY | INTUITICKS | REFRESHWINDOW,
	WINDOWSIZING | WINDOWCLOSE | WINDOWDRAG | WINDOWDEPTH | SMART_REFRESH,
	NULL, NULL, NULL, NULL, NULL,
	0, 0, 0xFFFF, 0xFFFF,
	CUSTOMSCREEN
};

#define LEFT_EDGE(i, w)		((i)*((w) + 1))
#define TOP_EDGE(i, h)		((i)*((h) + 1))
#define RIGHT_EDGE(i, w)	(LEFT_EDGE(i, w) + (w) - 1)
#define BOTTOM_EDGE(i, h)	(TOP_EDGE(i, h) + (h) - 1)

#define CELL_LEFT(x, w)	((x)/((w) + 1))
#define CELL_TOP(y, h)	((y)/((h) + 1))

typedef struct {
	WORD		Left, Top;	/* Index of cell */
} CellLoc;

/*
	Tool window parameters
*/

static WORD toolWindowLeft = -1;
static WORD toolWindowTop = -1;

static WORD	toolCellWidth, toolCellHeight;

#define TOOL_WIDTH	32		/* Values for hires-interlace screen */
#define TOOL_HEIGHT	20

typedef struct {
	WORD		Left, Top;	/* Index of cell */
	ImagePtr	Icon;
} ToolEntry;

#define NUM_TOOLS	(sizeof(tools)/sizeof(ToolEntry))

static ToolEntry tools[] = {	/* Same order as TOOL defines */
	{ 0, 0, &arrowIcon },
	{ 1, 0, &textIcon },
	{ 2, 0, &lineIcon },
	{ 3, 0, &eyeDropIcon },
	{ 0, 1, &rectIcon },
	{ 1, 1, &ovalIcon },
	{ 2, 1, &curveIcon },
	{ 3, 1, &polyIcon }
};

/*
	Pen window parameters
*/

#define SCALE(x, src, dst)	(((LONG) (x)*(dst) + (src)/2)/(src))

#define SAMPLE_HEIGHT	22

static GadgetTemplate colorGadgets[] = {
	{ GADG_ACTIVE_BORDER,
		0, SAMPLE_HEIGHT + 1, 0, -1,
		0, -(SAMPLE_HEIGHT + 1 + 17), -(ARROW_WIDTH + 4), 0,
		0, 0, NULL },

	{ GADG_ACTIVE_STDIMAGE,
		0, 0, 1 - ARROW_WIDTH, 1 - 3*ARROW_HEIGHT,
		0, 0, ARROW_WIDTH, ARROW_HEIGHT,
		0, 0, (Ptr) IMAGE_ARROW_UP },

	{ GADG_ACTIVE_STDIMAGE,
		0, 0, 1 - ARROW_WIDTH, 1 - 2*ARROW_HEIGHT,
		0, 0, ARROW_WIDTH, ARROW_HEIGHT,
		0, 0, (Ptr) IMAGE_ARROW_DOWN },

	{ GADG_PROP_VERT | GADG_PROP_NEWLOOK,
		0, SAMPLE_HEIGHT + 1, 1 - ARROW_WIDTH + 4, 2,
		0, -(SAMPLE_HEIGHT + 3), ARROW_WIDTH - 8, -4*ARROW_HEIGHT - 6,
		0, 0, NULL },

	{ GADG_ITEM_NONE }
};

static ScrollListPtr	penScrollList;

enum {
	PEN_BOX,
	PEN_UP,
	PEN_DOWN,
	PEN_SCROLL
};

static WORD penWindowLeft = -1;
static WORD penWindowTop = -1;
static WORD penWindowHeight = -1;
static WORD penWindowWidth = -1;

static WORD	penCellWidth, penCellHeight;

#define PEN_NONE		-1
#define PEN_CURRENT		-2
#define PEN_NOCHANGE	-3

/*
	Fill window parameters
*/

static WORD fillWindowLeft = -1;
static WORD fillWindowTop = -1;
static WORD fillWindowHeight = -1;
static WORD fillWindowWidth = -1;

static WORD	fillCellWidth, fillCellHeight;

static ScrollListPtr	fillScrollList;

enum {
	FILL_BOX,
	FILL_UP,
	FILL_DOWN,
	FILL_SCROLL
};

#define FILL_NONE		-1
#define FILL_CURRENT	-2
#define FILL_NOCHANGE	-3

/*
 *	Local prototypes
 */

static void	SetCellSizes(void);

static WORD	ToolWindowHeight(void);
static WORD	PenWindowHeight(void);
static WORD	FillWindowHeight(void);

static WindowPtr	OpenSpecialWindow(struct NewWindow);
static WindowPtr	OpenColorWindow(struct NewWindow);

static void	DrawToolCell(WORD, BOOL);
static void	DrawNoneText(WindowPtr, WORD, WORD, WORD, WORD);
static void	DrawCurrPenFill(BOOL);

static void	SetSelectPenFill(WindowPtr, WORD, WORD);

static void	BltIcon(RastPtr, ImagePtr, WORD, WORD);

/*
 *	Set tool, pen, and fill window defaults
 */

void SetToolDefaults()
{
	drawTool = TOOL_SELECT;
	DrawToolCell(drawTool, TRUE);
	DrawCurrPenFill(FALSE);
	DrawCurrPenFill(TRUE);
}

/*
 *	Bring tool windows to front if they are open
 */

void ToolToFront()
{
	if (toolWindow && LayerObscured(toolWindow->WLayer))
		WindowToFront(toolWindow);
	if (penWindow && LayerObscured(penWindow->WLayer))
		WindowToFront(penWindow);
	if (fillWindow && LayerObscured(fillWindow->WLayer))
		WindowToFront(fillWindow);
}

/*
 *	Set tool, pen, and fill window cell sizes
 */

static void SetCellSizes()
{
	toolCellWidth  = TOOL_WIDTH;
	toolCellHeight = TOOL_HEIGHT;
	penCellWidth  = fillCellWidth  = 3*_tbXSize;
	penCellHeight = fillCellHeight = (3*_tbYSize)/2;
	if ((screen->ViewPort.Modes & LACE) == 0)
		toolCellHeight -= 5;
	if ((screen->ViewPort.Modes & HIRES) == 0)
		toolCellWidth -= 8;
}

/*
 *	Return height of tool window
 */

static WORD ToolWindowHeight()
{
	return ((BOTTOM_EDGE(1, toolCellHeight) + 1
			+ screen->WBorTop + screen->WBorBottom
			+ screen->Font->ta_YSize + 1));
}

/*
 *	Return height of pen window
 */

static WORD PenWindowHeight()
{
	return (72);
}

/*
 *	Return height of fill window
 */

static WORD FillWindowHeight()
{
	return (84);
}

/*
 *	Open tool window with current parameters
 *	Also sets screen pointer for new window struct
 */

static WindowPtr OpenSpecialWindow(struct NewWindow newWindow)
{
	ULONG oldIDCMPFlags;
	WindowPtr window;

	newWindow.Screen = screen;
	oldIDCMPFlags = newWindow.IDCMPFlags;
	newWindow.IDCMPFlags = 0;
	window = OpenWindow(&newWindow);
	newWindow.IDCMPFlags = oldIDCMPFlags;
	if (window) {
		window->UserPort = &monitorMsgPort;
		SetStdPointer(window, POINTER_ARROW);
		ModifyIDCMP(window, oldIDCMPFlags);
		SetWindowTitles(window, (TextPtr) -1, strScreenTitle);
		SetPalette(window->RPort, &screenPalette);
		SetWKind(window, WKIND_TOOL);
	}
	return (window);
}

/*
 *	Open extra window that has deals with colors with current parameters
 *	Also sets screen pointer for new window struct
 */

static WindowPtr OpenColorWindow(struct NewWindow newWindow)
{
	ULONG oldIDCMPFlags;
	WindowPtr window;

	newWindow.Screen = screen;
	oldIDCMPFlags = newWindow.IDCMPFlags;
	newWindow.IDCMPFlags = 0;
	window = OpenWindow(&newWindow);
	newWindow.IDCMPFlags = oldIDCMPFlags;
	if (window) {
		window->UserPort = &monitorMsgPort;
		SetStdPointer(window, POINTER_ARROW);
		ModifyIDCMP(window, oldIDCMPFlags);
		SetWindowTitles(window, (TextPtr) -1, strScreenTitle);
		SetPalette(window->RPort, &screenPalette);
		SetWKind(window, WKIND_TOOL);
	}

	return (window);
}

/*
 *	Open tool window
 */

void OpenToolWindow()
{
	if (toolWindow)
		return;
/*
	Set window parameters
*/
	SetCellSizes();
	newExtraWindow.Width = RIGHT_EDGE(3, toolCellWidth) + 1
							+ screen->WBorLeft + screen->WBorRight;
	newExtraWindow.Height = ToolWindowHeight();
	newExtraWindow.LeftEdge = (toolWindowLeft == -1) ?
							  screen->Width - newExtraWindow.Width - 10 :
							  toolWindowLeft;
	newExtraWindow.TopEdge = (toolWindowTop == -1) ?
							 screen->BarHeight + 10 :
							 toolWindowTop;
	newExtraWindow.Title = strToolTitle;
/*
	Open the window
*/
	toolWindow = OpenSpecialWindow(newExtraWindow);
	if (toolWindow)
		DrawToolWindow();
}

/*
 *	Open pen window
 */

void OpenPenWindow()
{
	GadgetPtr	gadgList, gadget;
	WORD		len, width, height, titleHeight, i;
	PenColorPtr	newPen;

	if (penWindow)
		return;
/*
	Set window parameters
*/
	SetCellSizes();

	if ((penScrollList = NewScrollList(SL_SINGLESELECT)) == NULL)
		return;

	titleHeight = (screen->WBorTop + screen->Font->ta_YSize + 1);
	newColorWindow.Title = strPenTitle;
	newColorWindow.Width = RIGHT_EDGE(3, toolCellWidth) + 1
							+ screen->WBorLeft + screen->WBorRight;
	newColorWindow.Height = PenWindowHeight();

	width  = SCALE(newColorWindow.Width, 8, _tbXSize);
	height = SCALE(newColorWindow.Height, 11, _tbYSize);
	width  += screen->WBorLeft + screen->WBorRight;
	height += titleHeight + screen->WBorBottom;
	newColorWindow.Width		= (penWindowWidth == -1) ? width : penWindowWidth;
	newColorWindow.Height		= (penWindowHeight == -1) ? height : penWindowHeight;
	height = SCALE(SAMPLE_HEIGHT + 3*(screen->RastPort.Font->tf_YSize + 1), 11, _tbYSize);
	height += titleHeight + screen->WBorBottom;
	newColorWindow.MinWidth  	= width;
	newColorWindow.MinHeight	= height;
	newColorWindow.MaxWidth	= 640;
	newColorWindow.MaxHeight	= 400;
	newColorWindow.LeftEdge = (penWindowLeft == -1) ?
							  screen->Width - newColorWindow.Width - 10 :
							  penWindowLeft;
	newColorWindow.TopEdge = (penWindowTop == -1) ?
							 screen->BarHeight + ToolWindowHeight() + 15 :
							 penWindowTop;
/*
	Create and attach window gadgets
*/
	if ((gadgList = GetGadgets(colorGadgets)) == NULL) {
		DisposeScrollList(penScrollList);
		return;
	}
	GadgetItem(gadgList, PEN_UP)->Activation		|= GACT_RIGHTBORDER;
	GadgetItem(gadgList, PEN_DOWN)->Activation		|= GACT_RIGHTBORDER;
	GadgetItem(gadgList, PEN_SCROLL)->Activation	|= GACT_RIGHTBORDER;
	for (gadget = gadgList; gadget; gadget = gadget->NextGadget) {
		if ((gadget->Flags & GFLG_RELRIGHT) == 0)
			gadget->LeftEdge += screen->WBorLeft;
		if ((gadget->Flags & GFLG_RELBOTTOM) == 0)
			gadget->TopEdge  += titleHeight;
	}
	newColorWindow.FirstGadget = gadgList;
/*
	Open the window
*/
	penWindow = OpenColorWindow(newColorWindow);
	if (penWindow == NULL) {
		DisposeGadgets(gadgList);
		DisposeScrollList(penScrollList);
		return;
	}
/*
	get scrollList of color Names
*/
	InitScrollList(penScrollList, GadgetItem(penWindow->FirstGadget, PEN_BOX), penWindow, NULL);
	SLSetDrawProc(penScrollList, PenDrawProc);
	SLDoDraw(penScrollList, FALSE);
	for (i = 0, newPen = penColors; newPen; i++, newPen = newPen->Next) {
		len = GetPenScrollListItem(strBuff, newPen->Name, i);
		SLAddItem(penScrollList, strBuff, len, SLNumItems(penScrollList));
	}
	SLDoDraw(penScrollList, TRUE);
	DrawPenWindow();
}

/*
 *	Open fill window
 */

void OpenFillWindow()
{
	GadgetPtr	gadgList, gadget;
	WORD		len, width, height, titleHeight, i;
	FillPtnPtr	newPat;

	if (fillWindow)
		return;
/*
	Set window parameters
*/
	SetCellSizes();

	if ((fillScrollList = NewScrollList(SL_SINGLESELECT)) == NULL)
		return;

	titleHeight = (screen->WBorTop + screen->Font->ta_YSize + 1);
	newColorWindow.Title = strFillTitle;
	newColorWindow.Width = RIGHT_EDGE(3, toolCellWidth) + 1		// same as tool window
							+ screen->WBorLeft + screen->WBorRight;
	newColorWindow.Height = FillWindowHeight();

	width  = SCALE(newColorWindow.Width, 8, _tbXSize);
	height = SCALE(newColorWindow.Height, 11, _tbYSize);
	width  += screen->WBorLeft + screen->WBorRight;
	height += titleHeight + screen->WBorBottom;
	newColorWindow.Width		= (fillWindowWidth == -1) ? width : fillWindowWidth;
	newColorWindow.Height		= (fillWindowHeight == -1) ? height : fillWindowHeight;
	height = SCALE(SAMPLE_HEIGHT + 3*(screen->RastPort.Font->tf_YSize + 1), 11, _tbYSize);
	height += titleHeight + screen->WBorBottom;
	newColorWindow.MinWidth  	= width;
	newColorWindow.MinHeight	= height;
	newColorWindow.MaxWidth		= 640;
	newColorWindow.MaxHeight	= 400;
	newColorWindow.LeftEdge = (fillWindowLeft == -1) ?
							  screen->Width - newColorWindow.Width - 10 :
							  fillWindowLeft;
	newColorWindow.TopEdge = (fillWindowTop == -1) ?
				2 * screen->BarHeight + ToolWindowHeight() + PenWindowHeight() + 20 :
							 fillWindowTop;
	if (newColorWindow.TopEdge + newColorWindow.Height > screen->Height)
		newColorWindow.TopEdge = screen->Height - newColorWindow.Height;
/*
	Create and attach window gadgets
*/
	if ((gadgList = GetGadgets(colorGadgets)) == NULL) {
		DisposeScrollList(fillScrollList);
		return;
	}
	GadgetItem(gadgList, FILL_UP)->Activation		|= GACT_RIGHTBORDER;
	GadgetItem(gadgList, FILL_DOWN)->Activation		|= GACT_RIGHTBORDER;
	GadgetItem(gadgList, FILL_SCROLL)->Activation	|= GACT_RIGHTBORDER;
	for (gadget = gadgList; gadget; gadget = gadget->NextGadget) {
		if ((gadget->Flags & GFLG_RELRIGHT) == 0)
			gadget->LeftEdge += screen->WBorLeft;
		if ((gadget->Flags & GFLG_RELBOTTOM) == 0)
			gadget->TopEdge  += titleHeight;
	}
	newColorWindow.FirstGadget = gadgList;
/*
	Open the window
*/
	fillWindow = OpenColorWindow(newColorWindow);
	if (fillWindow == NULL) {
		DisposeGadgets(gadgList);
		DisposeScrollList(fillScrollList);
		return;
	}
/*
	get scrollList of fill patterns
*/
	InitScrollList(fillScrollList, GadgetItem(fillWindow->FirstGadget, FILL_BOX), fillWindow, NULL);
	SLSetDrawProc(fillScrollList, FillDrawProc);
	SLDoDraw(fillScrollList, FALSE);
	for (i = 0, newPat = fillPatterns; newPat; i++, newPat = newPat->Next) {
		len = GetFillScrollListItem(strBuff, newPat->Name, i);
		SLAddItem(fillScrollList, strBuff, len, SLNumItems(fillScrollList));
	}
	SLDoDraw(fillScrollList, TRUE);
	DrawFillWindow();
}

/*
 *	Close tool window
 */

void CloseToolWindow()
{
	if (toolWindow == NULL)
		return;
	toolWindowLeft = toolWindow->LeftEdge;
	toolWindowTop = toolWindow->TopEdge;
	CloseWindowSafely(toolWindow, mainMsgPort);
	toolWindow = NULL;
	if (numWindows)
		Delay(5);		/* Wait until Intuition notices window is gone */
}

/*
 *	Close pen window
 */

void ClosePenWindow()
{
	GadgetPtr	gadgList;

	if (penWindow == NULL)
		return;
	penWindowLeft = penWindow->LeftEdge;
	penWindowTop = penWindow->TopEdge;
	penWindowHeight = penWindow->Height;
	penWindowWidth = penWindow->Width;
	gadgList = GadgetItem(penWindow->FirstGadget, 0);
	CloseWindowSafely(penWindow, mainMsgPort);
	if (gadgList)
		DisposeGadgets(gadgList);
	if (penScrollList)
		DisposeScrollList(penScrollList);
	penScrollList = NULL;
	penWindow = NULL;
	if (numWindows)
		Delay(5);		/* Wait until Intuition notices window is gone */
}

/*
 *	Close fill window
 */

void CloseFillWindow()
{
	GadgetPtr	gadgList;

	if (fillWindow == NULL)
		return;
	fillWindowLeft = fillWindow->LeftEdge;
	fillWindowTop = fillWindow->TopEdge;
	fillWindowHeight = fillWindow->Height;
	fillWindowWidth = fillWindow->Width;
	gadgList = GadgetItem(fillWindow->FirstGadget, 0);
	CloseWindowSafely(fillWindow, mainMsgPort);
	if (gadgList)
		DisposeGadgets(gadgList);
	if (fillScrollList)
		DisposeScrollList(fillScrollList);
	fillWindow = NULL;
	if (numWindows)
		Delay(5);		// Wait until Intuition notices window is gone
}

/*
 *	Draw contents of tool window
 */

void DrawToolWindow()
{
	register WORD	i, x, y;
	RastPtr			rPort;
	Rectangle		rect;

	if (toolWindow == NULL)
		return;
	rPort = toolWindow->RPort;
	SetDrMd(rPort, JAM1);
/*
	Clear the window
*/
	GetWindowRect(toolWindow, &rect);
	SetAPen(rPort, _tbPenLight);
	FillRect(rPort, &rect);
/*
	Draw lines
*/
	SetAPen(rPort, _tbPenLight);
	for (i = 0; i < 3; i++) {
		x = RIGHT_EDGE(i, toolCellWidth) + 1 + rect.MinX;
		Move(rPort, x, rect.MinY);
		Draw(rPort, x, rect.MaxY);
	}
	y = BOTTOM_EDGE(0, toolCellHeight) + 1 + rect.MinY;
	Move(rPort, rect.MinX, y);
	Draw(rPort, rect.MaxX, y);
/*
	Draw tool icons
*/
	if (drawTool > NUM_TOOLS)
		drawTool = TOOL_SELECT;
	for (i = 0; i < NUM_TOOLS; i++)
		DrawToolCell(i, (i == drawTool));
}

/*
 *	Draw the specified tool window cell, hilited or not
 */

static void DrawToolCell(WORD tool, BOOL on)
{
	UBYTE		forePen, backPen;
	WORD		iconX, iconY;
	RastPtr		rPort;
	Rectangle	rect;

	if (toolWindow == NULL)
		return;
	rPort = toolWindow->RPort;
	SetDrMd(rPort, JAM1);
/*
	Get fore and back colors
*/
	if (on) {
		forePen = _tbPenWhite;
		backPen = _tbPenDark;
	}
	else {
		forePen = _tbPenBlack;
		backPen = _tbPenLight;
	}
/*
	Draw tool cell
*/
	GetWindowRect(toolWindow, &rect);
	rect.MinX += LEFT_EDGE(tools[tool].Left, toolCellWidth);
	rect.MinY += TOP_EDGE(tools[tool].Top, toolCellHeight);
	rect.MaxX  = rect.MinX + toolCellWidth - 1;
	rect.MaxY  = rect.MinY + toolCellHeight - 1;
	SetAPen(rPort, backPen);
	FillRect(rPort, &rect);
	iconX = rect.MinX + (toolCellWidth - tools[tool].Icon->Width)/2;
	iconY = rect.MinY + (toolCellHeight - tools[tool].Icon->Height)/2;
	SetAPen(rPort, forePen);
	BltIcon(rPort, tools[tool].Icon, iconX, iconY);
/*
	Draw shadow
*/
	DrawShadowBox(rPort, &rect, 0, !on);
}

/*
 *	Show current pen color
 */

static void DrawCurrPenFill(BOOL doFill)
{
	WORD		baseline, width;
	BOOL		isNone;
	RGBColor 	penColor;
	TextPtr		text;
	RastPtr 	rPort;
	WindowPtr	window;
	Rectangle 	rect, windRect, gadgRect;

	window = (doFill) ? fillWindow : penWindow;
	if (window == NULL)
		return;
	rPort = window->RPort;
	SetDrMd(rPort, JAM1);
	isNone = ((defaults.ObjFlags & ((doFill) ? OBJ_DO_FILL : OBJ_DO_PEN)) == 0);
/*
	Draw pen cell
*/
	GetWindowRect(window, &windRect);
	GetGadgetRect(GadgetItem(window->FirstGadget, PEN_BOX), window, NULL, &gadgRect);
	rect.MinX = windRect.MinX + _tbXSize/2;
	rect.MinY = windRect.MinY + ((gadgRect.MinY - windRect.MinY - penCellHeight)/2) - 1;
	rect.MaxX = rect.MinX + penCellWidth;
	rect.MaxY = rect.MinY + penCellHeight;
	penColor = (defaults.ObjFlags & OBJ_DO_PEN) ? defaults.PenColor : RGBCOLOR_WHITE;
	InsetRect(&rect, 1, 1);
	DrawShadowBox(rPort, &rect, 0, FALSE);
	InsetRect(&rect, 1, 1);
	SetAPen(rPort, _tbPenDark);
	FrameRect(rPort, &rect);
	InsetRect(&rect, 1, 1);
	if (isNone) {
		RGBForeColor(rPort, RGBCOLOR_WHITE);
		FillRect(rPort, &rect);
		SetAPen(rPort, _tbPenBlack);
		Move(rPort, rect.MinX, rect.MinY);
		Draw(rPort, rect.MaxX, rect.MaxY);
		Move(rPort, rect.MaxX, rect.MinY);
		Draw(rPort, rect.MinX, rect.MaxY);
	}
	else if (doFill)
		FillRectNew(rPort, &rect, &defaults.FillPat);
	else {
		RGBForeColor(rPort, penColor);
		FillRect(rPort, &rect);
	}
	InsetRect(&rect, -3, -3);
/*
	Draw text
*/
	rect.MinX = rect.MaxX + _tbXSize;
	rect.MaxX = windRect.MaxX;
	baseline = (rect.MaxY - rect.MinY - rPort->Font->tf_Baseline)/2 + rPort->Font->tf_Baseline;
	width = rect.MaxX - rect.MinX + 1;
	SetAPen(rPort, _tbPenLight);
	FillRect(rPort, &rect);
	SetAPen(rPort, _tbPenBlack);
	Move(rPort, rect.MinX, rect.MinY + baseline);
	text = (doFill) ? defaults.FillName : defaults.PenName;
	TextInWidth(rPort, text, strlen(text), width, FALSE);
}

/*
 *	Draw pen window
 */

void DrawPenWindow()
{
	RastPtr		rPort;
	Rectangle	windRect, gadgRect;
	WORD		y;

	if (penWindow == NULL)
		return;
	rPort = penWindow->RPort;
	SetDrMd(rPort, JAM1);
/*
	Clear the window
*/
	GetWindowRect(penWindow, &windRect);
	GetGadgetRect(GadgetItem(penWindow->FirstGadget, PEN_BOX), penWindow, NULL, &gadgRect);
	SetAPen(rPort, _tbPenLight);
	FillRect(rPort, &windRect);
/*
	Draw window contents
*/
	SetAPen(rPort, _tbPenBlack);
	y = gadgRect.MinY - 2;
	Move(rPort, windRect.MinX, y);
	Draw(rPort, windRect.MaxX, y);
	Move(rPort, windRect.MinX, y + 1);
	Draw(rPort, windRect.MaxX, y + 1);
	SLDrawList(penScrollList);
	DrawCurrPenFill(FALSE);
}

/*
 *	Draw fill window
 */

void DrawFillWindow()
{
	RastPtr		rPort;
	Rectangle	windRect, gadgRect;
	WORD		y;

	if (fillWindow == NULL)
		return;
	rPort = fillWindow->RPort;
	SetDrMd(rPort, JAM1);
/*
	Clear the window
*/
	GetWindowRect(fillWindow, &windRect);
	GetGadgetRect(GadgetItem(fillWindow->FirstGadget, FILL_BOX), fillWindow, NULL, &gadgRect);
	SetAPen(rPort, _tbPenLight);
	FillRect(rPort, &windRect);
/*
	Draw window contents
*/
	SetAPen(rPort, _tbPenBlack);
	y = gadgRect.MinY - 2;
	Move(rPort, windRect.MinX, y);
	Draw(rPort, windRect.MaxX, y);
	Move(rPort, windRect.MinX, y + 1);
	Draw(rPort, windRect.MaxX, y + 1);
	SLDrawList(fillScrollList);
	DrawCurrPenFill(TRUE);
}

/*
 *	Set draw tool
 */

void SetDrawTool(WORD tool)
{
	if (tool < 0 || tool > NUM_TOOLS)
		return;
	if (tool != drawTool) {
		DrawToolCell(drawTool, FALSE);
		DrawToolCell(tool, TRUE);
		drawTool = tool;
	}
}

/*
 *	Handle mouse down in tool window
 */

void DoToolWindow(WORD mouseX, WORD mouseY)
{
	WORD		i, left, top, tool;
	DocDataPtr	docData;

/*
	Change the draw tool
*/
	mouseX -= toolWindow->BorderLeft;
	mouseY -= toolWindow->BorderTop;
	left = CELL_LEFT(mouseX, toolCellWidth);
	top = CELL_TOP(mouseY, toolCellHeight);
	for (tool = 0; tool <= NUM_TOOLS; tool++) {
		if (left == tools[tool].Left && top == tools[tool].Top)
			break;
	}
	SetDrawTool(tool);
/*
	Unselect all objects in all windows
*/
	for (i = 0; i < numWindows; i++) {
		docData = (DocDataPtr) GetWRefCon(windowList[i]);
		if (windowList[i] != cmdWindow || drawTool != TOOL_SELECT)
			UnSelectAllObjects(docData);
		if (windowList[i] == cmdWindow)
			TextPurge(docData);
	}
/*
	If last active doc window is still open, then activate it
*/
	if (IsDocWindow(cmdWindow))
		ActivateWindow(cmdWindow);
}

/*
 *	Set pen and fill pattern for selection
 */

static void SetSelectPenFill(WindowPtr window, WORD penNum, WORD fillNum)
{
	RGBColor 	penColor;
	RGBPat8 	fillPat;
	DocObjPtr 	docObj;
	DocDataPtr 	docData = (DocDataPtr) GetWRefCon(window);

	if (ObjectsLocked(docData)) {
		Error(ERR_OBJ_LOCKED);
		return;
	}
	for (docObj = FirstSelected(docData); docObj; docObj = NextSelected(docObj)) {
		if (penNum != PEN_NOCHANGE) {
			if (penNum == PEN_NONE) {
				EnableObjectPen(docObj, FALSE);
				SetObjectPenColor(docObj, RGBCOLOR_WHITE);
			}
			else {
				penColor = (penNum == PEN_CURRENT) ?
						   defaults.PenColor : GetPenColor(penNum);
				EnableObjectPen(docObj, TRUE);
				SetObjectPenColor(docObj, penColor);
			}
		}
		if (fillNum != FILL_NOCHANGE) {
			if (fillNum == FILL_NONE) {
				EnableObjectFill(docObj, FALSE);
				SetObjectFillPat(docObj, &fillPats[0]);
			}
			else {
				if (fillNum == FILL_CURRENT)
					CopyRGBPat8(&defaults.FillPat, fillPat);
				else
					GetFillPattern(fillNum, &fillPat);
				EnableObjectFill(docObj, TRUE);
				SetObjectFillPat(docObj, &fillPat);
			}
		}
		if (docObj->Type != TYPE_BMAP)
			InvalObjectRect(window, docObj);
		DocModified(docData);
	}
}

/*
 *	Wait for window to become active
 *	Should only be called after ActivateWindow()
 */

static void WaitWindowActive(WindowPtr window)
{
	BOOL isActive;
	IntuiMsgPtr intuiMsg;

	isActive = FALSE;
	for (;;) {
		while (!isActive && (intuiMsg = (IntuiMsgPtr) GetMsg(mainMsgPort)) != NULL) {
			if (intuiMsg->Class == ACTIVEWINDOW && intuiMsg->IDCMPWindow == window)
				isActive = TRUE;
			DoIntuiMessage(intuiMsg);
		}
		if (isActive)
			break;
		Wait(1 << mainMsgPort->mp_SigBit);
	}
}

/*
 *	Set to specified pen number (penNum 0 is "None")
 *	Invoked by AREXX macro
 */

void DoPenNum(WindowPtr window, WORD penNum)
{
	if (penNum == 0) {
		defaults.PenColor = RGBCOLOR_WHITE;
		strcpy(defaults.PenName, strNone);
		defaults.ObjFlags &= ~OBJ_DO_PEN;
		penNum = PEN_NONE;
	}
	else if (penNum <= NumPens()) {
		defaults.PenColor = GetPenColor(penNum - 1);
		GetPenName(penNum - 1, defaults.PenName);
		defaults.ObjFlags |= OBJ_DO_PEN;
		penNum--;
	}
	else				/* penNum too big */
		return;
	DrawCurrPenFill(FALSE);
	if (IsDocWindow(window))
		SetSelectPenFill(window, penNum, FILL_NOCHANGE);
}

/*
 *	Set to specified fill number (fillNum 0 is "None")
 *	Invoked by AREXX macro
 */

void DoFillNum(WindowPtr window, WORD fillNum)
{
	if (fillNum == 0) {
		CopyRGBPat8(&fillPats[0], defaults.FillPat);
		strcpy(defaults.FillName, strNone);
		defaults.ObjFlags &= ~OBJ_DO_FILL;
		fillNum = FILL_NONE;
	}
	else if (fillNum <= NumFillPatterns()) {
		GetFillPattern(fillNum - 1, &defaults.FillPat);
		GetFillName(fillNum - 1, defaults.FillName);
		defaults.ObjFlags |= OBJ_DO_FILL;
		fillNum--;
	}
	else				/* fillNum too big */
		return;
	DrawCurrPenFill(TRUE);
	if (IsDocWindow(window))
		SetSelectPenFill(cmdWindow, PEN_NOCHANGE, fillNum);
}

/*
 *	Blit single bit-plane image into rPort using current FgPen
 *	Note: This routine ignores all image data other than first bit plane
 *		  Also ignores linked list of images
 */

static void BltIcon(RastPtr rPort, register ImagePtr image, WORD left, WORD top)
{
	register WORD width, height, bytesPerRow;

	left += image->LeftEdge;
	top += image->TopEdge;
	width = image->Width;
	height = image->Height;
	bytesPerRow = ((width + 15) >> 3) & 0xFFFE;
	BltTemplate((PLANEPTR) image->ImageData, 0, bytesPerRow, rPort, left, top,
				width, height);
}

/*
 *	Draw proc for pen and fill windows
 */

static void PenFillDrawProc(RastPtr rPort, TextPtr text, RectPtr rect, BOOL isFill)
{
	UBYTE		num;
	RGBColor	color;
	RGBPat8		pattern;
	Rectangle	box;

	SetPalette(rPort, &screenPalette);
	num = *text++ - 1;
	if (isFill)
		GetFillPattern(num, &pattern);
	else
		color = GetPenColor(num);
/*
	Draw text
*/
	Move(rPort, rect->MinX + _tbXSize*3, rPort->cp_y);
	if (num == 0)
		SetSoftStyle(rPort, FSF_ITALIC, 0xFF);
	TextInWidth(rPort, text, strlen(text), rect->MaxX - rPort->cp_x + 1, FALSE);
/*
	Draw sample box
*/
	box = *rect;
	box.MaxX = box.MinX + (5*_tbXSize)/2;
	SetAPen(rPort, _tbPenBlack);
	InsetRect(&box, 1, 1);
	FrameRect(rPort, &box);
	InsetRect(&box, 1, 1);
	if (isFill)
		FillRectNew(rPort, &box, &pattern);
	else {
		RGBForeColor(rPort, color);
		FillRect(rPort, &box);
	}
	if (num == 0) {
		SetAPen(rPort, _tbPenBlack);
		Move(rPort, box.MinX, box.MinY);
		Draw(rPort, box.MaxX, box.MaxY);
		Move(rPort, box.MaxX, box.MinY);
		Draw(rPort, box.MinX, box.MaxY);
	}
}

/*
 *	Pen window custom draw procedure
 */

void PenDrawProc(RastPtr rPort, TextPtr text, RectPtr rect)
{
	PenFillDrawProc(rPort, text, rect, FALSE);
}

/*
 *	Create string for pencolor scrollList
 */

WORD GetPenScrollListItem(TextPtr buff, TextPtr name, UBYTE colorNum)
{
	buff[0] = colorNum + 1;
	strcpy(&buff[1], name);

	return (strlen(name) + 1);
}

/*
 *	change item in current pen list
 */

void ChangePenWindow(WORD colorNum, TextPtr name)
{
	WORD len;

	len = GetPenScrollListItem(strBuff, name, colorNum);
	SLChangeItem(penScrollList, colorNum, strBuff, len);
}

/*
 *	remove item from current penlist
 */

void RemoveFromPenWindow(WORD num)
{
	SLRemoveItem(penScrollList, num);
}

/*
 *	add item to current penlist
 */

void AddToPenWindow(TextPtr name)
{
	WORD len;

	len = GetPenScrollListItem(strBuff, name, SLNumItems(penScrollList));
	SLAddItem(penScrollList, strBuff, len, SLNumItems(penScrollList));
}

/*
 *	process pen window intuition messages
 */

void DoPenWindowMessage(register IntuiMsgPtr intuiMsg)
{
	register ULONG 	class = intuiMsg->Class;
	register UWORD 	code = intuiMsg->Code;
	WORD			penNum, prevPenNum;

	prevPenNum = SLNextSelect(penScrollList, -1);
	if (class == GADGETDOWN)
		SLGadgetMessage(penScrollList, mainMsgPort, intuiMsg);
	else if (class == RAWKEY && (code == CURSORUP || code == CURSORDOWN))
		SLCursorKey(penScrollList, code);
	else
		ReplyMsg((MsgPtr) intuiMsg);
/*
	Handle new selection
*/
	if ((penNum = SLNextSelect(penScrollList, -1)) == -1)
		return;
	if (penNum == prevPenNum &&
		GadgetNumber((GadgetPtr) intuiMsg->IAddress) != PEN_BOX)
		return;

	GetPenName(penNum, strBuff);
	if (penNum == 0)
		penNum = PEN_NONE;

	defaults.PenColor = GetPenColor(penNum);
	GetPenName(penNum, defaults.PenName);

	if (penNum == PEN_NONE)
		defaults.ObjFlags &= ~OBJ_DO_PEN;
	else
		defaults.ObjFlags |= OBJ_DO_PEN;

	DrawCurrPenFill(FALSE);
/*
	If last active doc window is still open, then activate it
		and set pen for current selection
*/
	if (IsDocWindow(cmdWindow)) {
		ActivateWindow(cmdWindow);
		WaitWindowActive(cmdWindow);
		SetSelectPenFill(cmdWindow, penNum, FILL_NOCHANGE);
	}
}

/*
 *	FillWindow custom draw procedure
 */

void FillDrawProc(RastPtr rPort, TextPtr text, RectPtr rect)
{
	PenFillDrawProc(rPort, text, rect, TRUE);
}

/*
 *	create string for fillpattern scrollList
 */

WORD GetFillScrollListItem(TextPtr buff, TextPtr name, UBYTE patNum)
{
	buff[0] = patNum + 1;
	strcpy(&buff[1], name);

	return (strlen(name) + 1);
}

/*
 *	change item in current fillpattern list
 */

void ChangeFillWindow(WORD patNum, TextPtr name)
{
	WORD len;

	len = GetFillScrollListItem(strBuff, name, patNum);
	SLChangeItem(fillScrollList, patNum, strBuff, len);
}

/*
 *	remove item from current fill pattern list
 */

void RemoveFromFillWindow(WORD num)
{
	SLRemoveItem(fillScrollList, num);
}

/*
 *	add item to current penlist
 */

void AddToFillWindow(TextPtr name)
{
	WORD len;

	len = GetFillScrollListItem(strBuff, name, SLNumItems(fillScrollList));
	SLAddItem(fillScrollList, strBuff, len, SLNumItems(fillScrollList));
}

/*
 *	process fill window intuition messages
 */

void DoFillWindowMessage(register IntuiMsgPtr intuiMsg)
{
	register ULONG 		class;
	register UWORD 		code;
	WORD				fillNum, prevFillNum;

	class = intuiMsg->Class;
	code = intuiMsg->Code;
	prevFillNum = SLNextSelect(fillScrollList, -1);

	if (class == GADGETDOWN)
		SLGadgetMessage(fillScrollList, mainMsgPort, intuiMsg);
	else if (class == RAWKEY && (code == CURSORUP || code == CURSORDOWN))
		SLCursorKey(fillScrollList, code);
	else
		ReplyMsg((MsgPtr) intuiMsg);

	if ((fillNum = SLNextSelect(fillScrollList, -1)) == -1)
		return;
	if (fillNum == prevFillNum &&
		GadgetNumber((GadgetPtr) intuiMsg->IAddress) != FILL_BOX)
		return;

	GetFillName(fillNum, strBuff);
	if (CmpString(strBuff, strNone, strlen(strBuff), strlen(strNone), FALSE) == 0)
		fillNum = FILL_NONE;

	GetFillPattern(fillNum, &defaults.FillPat);
	GetFillName(fillNum, defaults.FillName);

	if (fillNum == FILL_NONE)
		defaults.ObjFlags &= ~OBJ_DO_FILL;
	else
		defaults.ObjFlags |= OBJ_DO_FILL;

	DrawCurrPenFill(TRUE);
/*
	If last active doc window is still open, then activate it
		and set pen for current selection
*/

	if (IsDocWindow(cmdWindow)) {
		ActivateWindow(cmdWindow);
		WaitWindowActive(cmdWindow);
		SetSelectPenFill(cmdWindow, PEN_NOCHANGE, fillNum);
	}
}