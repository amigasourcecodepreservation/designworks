/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Toolbox library
 *	Copyright (c) 1992 New Horizons Software, Inc.
 *
 *	Requester routines
 */

#define INTUI_V36_NAMES_ONLY	1

#include <exec/types.h>
#include <intuition/intuition.h>
#include <intuition/intuitionbase.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include <TypeDefs.h>

#include <Toolbox/Globals.h>
#include <Toolbox/Memory.h>
#include <Toolbox/Border.h>
#include <Toolbox/Request.h>
#include <Toolbox/Utility.h>

/*
 *	External variables
 */

extern struct IntuitionBase	*IntuitionBase;

/*
 *	Local variables and definitions
 */

#define SCALE(x, src, dst)	(((LONG) (x)*(dst) + (src)/2)/(src))

/*
 *	GetRequest
 *	Process requester template, and return pointer to Requester structure
 *	If showIt flag is TRUE then show requester
 *	Return NULL if error
 */

RequestPtr GetRequest(ReqTemplPtr reqTempl, WindowPtr window, BOOL showIt)
{
	register WORD		leftEdge, topEdge, width, height;
	register RequestPtr	request;
	GadgetPtr			gadget;
	GadgTemplPtr		gadgTemplate;
	BorderPtr			border1, border2;

	if (window == NULL)
		return (NULL);
/*
	Allocate requester and initialize requester location
*/
	if ((request = MemAlloc(sizeof(struct Requester), MEMF_CLEAR)) == NULL)
		return (NULL);
	InitRequester(request);
	request->Width  = width  = SCALE(reqTempl->Width, 8, _tbXSize);
	request->Height = height = SCALE(reqTempl->Height, 11, _tbYSize);
	if ((leftEdge = reqTempl->LeftEdge) == -1)
		leftEdge = (window->Width - window->BorderLeft - window->BorderRight - width)/2
				   + window->BorderLeft;
	else
		leftEdge = SCALE(leftEdge, 8, _tbXSize);
	if ((topEdge = reqTempl->TopEdge) == -1)
		topEdge = (window->Height - window->BorderTop - window->BorderBottom-height)/2
				  + window->BorderTop;
	else
		topEdge = SCALE(topEdge, 11, _tbYSize);
	if (leftEdge < window->BorderLeft)
		leftEdge = window->BorderLeft;
	if (topEdge < window->BorderTop)
		topEdge = window->BorderTop;
	request->LeftEdge = leftEdge;
	request->TopEdge  = topEdge;
	request->Flags	  = NOISYREQ;
	request->BackFill = _tbPenLight;
/*
	Create and attach gadget structures
*/
	gadgTemplate = reqTempl->Gadgets;
	if (gadgTemplate == NULL || (gadget = GetGadgets(gadgTemplate)) == NULL) {
		DisposeRequest(request);
		return (NULL);
	}
	request->ReqGadget = gadget;
/*
	Create and attach border
*/
	if (_tbNoShadows) {
		border1 = BoxBorder(request->Width, request->Height, _tbPenBlack, 0);
		border2 = BoxBorder(request->Width, request->Height, _tbPenBlack, 2);
		if (border1)
			AppendBorder(border1, border2);
	}
	else
		border1 = ShadowBoxBorder(request->Width, request->Height, 1, TRUE);
	request->ReqBorder = border1;
/*
	Display requester
*/
	if (showIt && ShowRequest(request, window) == FALSE) {
		DisposeRequest(request);
		return (NULL);
	}
	return (request);
}

/*
 *	ShowRequest
 *	Display requester in specified window
 *	Return TRUE if successful, FALSE if not
 *
 *	If requester has string gadgets, then activate the first one
 */

BOOL ShowRequest(RequestPtr request, WindowPtr window)
{
	register WORD		gadgType;
	register ULONG		intuiLock;
	register GadgetPtr	gadget;
	WindowPtr			activeWindow;
	LayerPtr			frontLayer;
	ScreenPtr			firstScreen;

	intuiLock = LockIBase(0);
	activeWindow = IntuitionBase->ActiveWindow;
	firstScreen = IntuitionBase->FirstScreen;
	UnlockIBase(intuiLock);
/*
	If requester requires interaction, bring it to front
*/
	for (gadget = request->ReqGadget; gadget; gadget = gadget->NextGadget) {
		gadgType = GadgetType(gadget);
		if (gadgType != GADG_STAT_TEXT &&
			gadgType != GADG_STAT_BORDER && gadgType != GADG_STAT_STDBORDER &&
			gadgType != GADG_STAT_IMAGE && gadgType != GADG_STAT_STDIMAGE)
			break;
	}
	if (gadget) {
		if (window != activeWindow)
			ActivateWindow(window);
		if (window->WScreen != firstScreen)
			ScreenToFront(window->WScreen);
		frontLayer = (window->FirstRequest) ? window->FirstRequest->ReqLayer :
											  window->WLayer;
		if ((window->Flags & WFLG_BACKDROP) == 0 && frontLayer->front) {
			WindowToFront(window);
			Delay(5);						/* Wait until window is in front */
		}
	}
/*
	Bring up requester
*/
	if (Request(request, window) == FALSE)
		return (FALSE);
	Delay(5);							/* Wait until requester is active */
/*
	Activate first text box in requester
*/
	if (_tbAutoActivate) {
		for (gadget = request->ReqGadget; gadget; gadget = gadget->NextGadget) {
			if (GadgetType(gadget) == GADG_EDIT_TEXT)
				break;
		}
		if (gadget)
			ActivateGadget(gadget, window, request);
	}
	return (TRUE);
}

/*
 *	DisposeRequest
 *	Release all memory used by requester created with GetRequest
 */

void DisposeRequest(RequestPtr request)
{
	if (request) {
		FreeBorder(request->ReqBorder);
		DisposeGadgets(request->ReqGadget);
		MemFree(request, sizeof(struct Requester));
	}
}
