/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Toolbox library
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Misc graphics routines
 */

#include <clib/macros.h>
#include <exec/types.h>
#include <graphics/gfxmacros.h>
#include <intuition/intuition.h>
#include <intuition/intuitionbase.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/layers.h>
#include <proto/intuition.h>

#include <TypeDefs.h>

#include <Toolbox/Memory.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Utility.h>

/*
 *	External variables
 */

extern struct IntuitionBase *IntuitionBase;
extern struct GfxBase		*GfxBase;

/*
 *	LocalToGlobal(window, xp, yp)
 *	Convert local window coordinates to global coordinates
 *	Global coordinates are for HIRES LACE screens
 */

void LocalToGlobal(register WindowPtr window, register WORD *xp, register WORD *yp)
{
	register WORD		modes;
	register ScreenPtr	screen;

	screen = window->WScreen;
	*xp += window->LeftEdge + screen->LeftEdge;
	*yp += window->TopEdge + screen->TopEdge;
	if (((modes = ViewPortAddress(window)->Modes) & HIRES) == 0)
		*xp <<= 1;
	if ((modes & LACE) == 0)
		*yp <<= 1;
}

/*
 *	WhichScreen(x, y)
 *	Return pointer to screen the (global) point is in, or NULL
 */

ScreenPtr WhichScreen(register WORD x, register WORD y)
{
	register WORD		x1, y1;
	register UWORD		modes;
	register LONG		intuiLock;
	register ScreenPtr	screen;

	intuiLock = LockIBase(0);
	for (screen = IntuitionBase->FirstScreen; screen; screen = screen->NextScreen) {
		modes = screen->ViewPort.Modes;
		x1 = (modes & HIRES) ? x : x >> 1;
		y1 = (modes & LACE)  ? y : y >> 1;
		if (x1 >= screen->LeftEdge && x1 < screen->LeftEdge + screen->Width &&
			y1 >= screen->TopEdge  && y1 < screen->TopEdge + screen->Height)
			break;
	}
	UnlockIBase(intuiLock);
	return (screen);
}

/*
 *	ScrollRect(layer, dx, dy, xMin, yMin, xMax, yMax)
 *	Move the bits in the raster by (dx,dy) towards (0,0)
 *	Limit the scroll operation to the rectangle (xMin,yMin,xMax,yMax)
 *	Add the exposed area to the layer's damage list
 */

void ScrollRect(register LayerPtr layer, register WORD dx, register WORD dy,
				register WORD xMin, register WORD yMin,
				register WORD xMax, register WORD yMax)
{
	register LayerPtr	frontLayer;
	register WORD		xOffset, yOffset;
//	WORD				gfxVersion;
	BOOL				isSimpleRefresh;
	RegionPtr			updateRgn;
	Rectangle			rect;

	if ((dx == 0 && dy == 0) || (updateRgn = NewRegion()) == NULL)
		return;
//	gfxVersion = LibraryVersion((struct Library *) GfxBase);
	isSimpleRefresh = (layer->Flags & LAYERSIMPLE);
/*
	Handle actually scrolling in rectangle
*/
	if (ABS(dx) <= xMax - xMin && ABS(dy) <= yMax - yMin) {
		ScrollRaster(layer->rp, dx, dy, xMin, yMin, xMax, yMax);
		if (dx) {
			if (dx > 0) {
				rect.MinX = xMax - dx + 1;
				rect.MaxX = xMax;
			}
			else {						// dx < 0
				rect.MinX = xMin;
				rect.MaxX = xMin - dx - 1;
			}
			rect.MinY = yMin;
			rect.MaxY = yMax;
//			if (gfxVersion < OSVERSION_2_0 || !isSimpleRefresh)
				(void) OrRectRegion(updateRgn, &rect);
		}
		if (dy) {
			if (dy > 0) {
				rect.MinY = yMax - dy + 1;
				rect.MaxY = yMax;
			}
			else {						// dy < 0
				rect.MinY = yMin;
				rect.MaxY = yMin - dy - 1;
			}
			rect.MinX = xMin;
			rect.MaxX = xMax;
//			if (gfxVersion < OSVERSION_2_0 || !isSimpleRefresh)
				(void) OrRectRegion(updateRgn, &rect);
		}
/*
	Add any overlapping layer's rectangles to the update region
*/
		if (isSimpleRefresh) {
			xOffset = layer->bounds.MinX + dx;
			yOffset = layer->bounds.MinY + dy;
			for (frontLayer = layer->front; frontLayer; frontLayer = frontLayer->front) {
				rect.MinX = frontLayer->bounds.MinX - xOffset;
				rect.MinY = frontLayer->bounds.MinY - yOffset;
				rect.MaxX = frontLayer->bounds.MaxX - xOffset;
				rect.MaxY = frontLayer->bounds.MaxY - yOffset;
				if (rect.MinX > xMax || rect.MinY > yMax ||
					rect.MaxX < xMin || rect.MaxY < yMin)
					continue;
				if (rect.MinX < xMin)
					rect.MinX = xMin;
				if (rect.MinY < yMin)
					rect.MinY = yMin;
				if (rect.MaxX > xMax)
					rect.MaxX = xMax;
				if (rect.MaxY > yMax)
					rect.MaxY = yMax;
				(void) OrRectRegion(updateRgn, &rect);
			}
		}
	}
/*
	Scrolling more than the entire contents of the rectangle, just update all
*/
	else {
		rect.MinX = xMin;
		rect.MinY = yMin;
		rect.MaxX = xMax;
		rect.MaxY = yMax;
		(void) OrRectRegion(updateRgn, &rect);
	}
	if (layer->ClipRegion)
		(void) AndRegionRegion(layer->ClipRegion, updateRgn);
	(void) OrRegionRegion(updateRgn, layer->DamageList);
	DisposeRegion(updateRgn);
}

/*
 *	LoadRGB4CM
 *	Load a set of color values into a color map structure
 */

void LoadRGB4CM(register ColorMapPtr colorMap, register UWORD *colors,
				register WORD count)
{
	register WORD	i, color;

	for (i = 0; i < count; i++) {
		color = colors[i];
		SetRGB4CM(colorMap, i, (color >> 8) & 0x0F, (color >> 4) & 0x0F,
				  color & 0x0F);
	}
}

/*
 *	Initialize rast port and attach layer
 *	Does not set bit map plane pointers
 */

RastPtr OpenPort()
{
	LayerPtr		layer;
	LayerInfoPtr	layerInfo;
	BitMapPtr		bitMap;

/*
	Allocate and initialize structures
*/
	if ((bitMap = MemAlloc(sizeof(BitMap), MEMF_CLEAR)) != NULL) {
		if ((layerInfo = NewLayerInfo()) != NULL) {
			if ((layer = CreateUpfrontLayer(layerInfo, bitMap, 0, 0, MAX_BLIT_SIZE,
											MAX_BLIT_SIZE, LAYERSIMPLE, NULL)) != NULL)
				return (layer->rp);
			DisposeLayerInfo(layerInfo);
		}
		MemFree(bitMap, sizeof(BitMap));
	}
	return (NULL);
}

/*
 *	Dispose of memory allocated by OpenPort
 */

void ClosePort(RastPtr rPort)
{
	LayerPtr		layer;
	LayerInfoPtr	layerInfo;
	BitMapPtr		bitMap;

	layer = rPort->Layer;
	layerInfo = layer->LayerInfo;
	bitMap = rPort->BitMap;
	MemFree(bitMap, sizeof(BitMap));
	DeleteLayer(NULL, layer);
	DisposeLayerInfo(layerInfo);
}

/*
 *	Get current user clip region for layer
 */

RegionPtr GetClip(LayerPtr layer)
{
	register RegionPtr	clipRgn;

	if (layer->ClipRegion == NULL || (clipRgn = NewRegion()) == NULL)
		return (NULL);
	(void) OrRegionRegion(layer->ClipRegion, clipRgn);
	return (clipRgn);
}

/*
 *	Set clip region, disposing of old clip region
 */

void SetClip(LayerPtr layer, RegionPtr clipRgn)
{
	register RegionPtr	oldRgn;

	oldRgn = InstallClipRegion(layer, clipRgn);
	if (oldRgn)
		DisposeRegion(oldRgn);
}

/*
 *	Set rectangular clip region, disposing of old clip region
 */

void SetRectClip(LayerPtr layer, RectPtr rect)
{
	register RegionPtr	clipRgn;

	if ((clipRgn = NewRegion()) != NULL) {
		if (OrRectRegion(clipRgn, rect))
			SetClip(layer, clipRgn);
		else
			DisposeRegion(clipRgn);
	}
}

/*
 *	Determine if layer is covered by front layers
 */

BOOL LayerObscured(LayerPtr layer)
{
	Rectangle	bounds;

	bounds = layer->bounds;
	while (layer->front) {
		layer = layer->front;
		if (bounds.MinX <= layer->bounds.MaxX && bounds.MaxX >= layer->bounds.MinX &&
			bounds.MinY <= layer->bounds.MaxY && bounds.MaxY >= layer->bounds.MinY)
			return (TRUE);
	}
	return (FALSE);
}

/*
 *	Allocate and initialize off-screen drawing area
 */

RastPtr CreateRastPort(WORD width, WORD height, WORD depth)
{
	LayerPtr		layer;
	LayerInfoPtr	layerInfo;
	BitMapPtr		bitMap;

/*
	Allocate and initialize structures
*/
	if ((bitMap = CreateBitMap(width, height, depth, TRUE)) == NULL)
		goto Exit1;
	if ((layerInfo = NewLayerInfo()) == NULL)
		goto Exit2;
	if ((layer = CreateUpfrontLayer(layerInfo, bitMap, 0, 0, width - 1,
									height - 1, LAYERSIMPLE, NULL)) == NULL)
		goto Exit3;
	return (layer->rp);
/*
	Error during allocation, clean up and return NULL
*/
Exit3:
	DisposeLayerInfo(layerInfo);
Exit2:
	DisposeBitMap(bitMap);
Exit1:
	return (NULL);
}

/*
 *	Free memory allocated by CreateRastPort()
 *	Note: Caller may set BitMap to NULL before calling this routine to
 *		  retain pointer to bitmap
 */

void DisposeRastPort(RastPtr rPort)
{
	BitMapPtr		bitMap;
	LayerPtr		layer;
	LayerInfoPtr	layerInfo;

	if (rPort) {
		bitMap = rPort->BitMap;
		layer = rPort->Layer;
		layerInfo = layer->LayerInfo;
		DeleteLayer(NULL, layer);
		DisposeLayerInfo(layerInfo);
		DisposeBitMap(bitMap);
	}
}

/*
 *	Allocate and initialize bitmap and bit planes
 */

BitMapPtr CreateBitMap(WORD width, WORD height, WORD depth, BOOL needGraphicMem)
{
	register WORD		i;
	WORD				bmSize, flags;
	LONG				byteCount;
	register BitMapPtr	bitMap;

/*
	Allocate and initialize structures
*/
	bmSize = sizeof(BitMap) + sizeof(PLANEPTR)*(depth - 8);
	if ((bitMap = MemAlloc(bmSize, MEMF_CLEAR)) == NULL)
		goto Exit1;
	InitBitMap(bitMap, depth, width, height);
	byteCount = (LONG) bitMap->BytesPerRow*bitMap->Rows;
	flags = (needGraphicMem) ? MEMF_CHIP : 0;
	for (i = 0; i < depth; i++) {
		if ((bitMap->Planes[i] = AllocMem(byteCount, flags)) == NULL)
			goto Exit2;
		if (needGraphicMem)
			BltClear(bitMap->Planes[i], byteCount, 1);
		else
			BlockClear(bitMap->Planes[i], byteCount);
	}
	return (bitMap);
/*
	Error during allocation, clean up and return NULL
*/
Exit2:
	for (i = 0; i < depth; i++) {
		if (bitMap->Planes[i])
			FreeMem(bitMap->Planes[i], byteCount);
	}
	MemFree(bitMap, bmSize);
Exit1:
	return (NULL);
}

/*
 *	Dispose of bitmap created by CreateBitMap
 *	Note: Caller may set Planes pointers to NULL before calling this routine to
 *		  retain pointers to planes
 */

void DisposeBitMap(register BitMapPtr bitMap)
{
	register WORD	i;
	WORD			bmSize;
	LONG			byteCount;

	if (bitMap) {
		bmSize = sizeof(BitMap) + sizeof(PLANEPTR)*(bitMap->Depth - 8);
		byteCount = (LONG) bitMap->BytesPerRow*bitMap->Rows;
		WaitBlit();						// Make sure we are not using this memory
		for (i = 0; i < bitMap->Depth; i++) {
			if (bitMap->Planes[i])
				FreeMem(bitMap->Planes[i], byteCount);
		}
		MemFree(bitMap, bmSize);
	}
}

/*
 *	Set rectangle to given values
 */

void SetRect(register RectPtr rect, WORD minX, WORD minY, WORD maxX, WORD maxY)
{
	rect->MinX = minX;		rect->MinY = minY;
	rect->MaxX = maxX;		rect->MaxY = maxY;
}

/*
 *	Offset rectangle by given x and y amounts
 *	Movement for positive values is right and down
 */

void OffsetRect(register RectPtr rect, WORD dx, WORD dy)
{
	rect->MinX += dx;
	rect->MinY += dy;
	rect->MaxX += dx;
	rect->MaxY += dy;
}

/*
 *	Move rectangle edges inward by given amount
 */

void InsetRect(register RectPtr rect, WORD dx, WORD dy)
{
	rect->MinX += dx;
	rect->MinY += dy;
	rect->MaxX -= dx;
	rect->MaxY -= dy;
}

/*
 *	Return the intersection of two rectangles
 *	Returns TRUE if rectangles intersect
 *	Returns with dstRect start > end if rectangles do not intersect
 */

BOOL SectRect(register RectPtr src1, register RectPtr src2, register RectPtr dstRect)
{
	dstRect->MinX = MAX(src1->MinX, src2->MinX);
	dstRect->MinY = MAX(src1->MinY, src2->MinY);
	dstRect->MaxX = MIN(src1->MaxX, src2->MaxX);
	dstRect->MaxY = MIN(src1->MaxY, src2->MaxY);
	return ((BOOL) !EmptyRect(dstRect));
}

/*
 *	Calculate the union of two rectangles
 *	Works properly if destRect is a source rect
 */

void UnionRect(register RectPtr src1, register RectPtr src2, register RectPtr dstRect)
{
	dstRect->MinX = MIN(src1->MinX, src2->MinX);
	dstRect->MinY = MIN(src1->MinY, src2->MinY);
	dstRect->MaxX = MAX(src1->MaxX, src2->MaxX);
	dstRect->MaxY = MAX(src1->MaxY, src2->MaxY);
}

/*
 *	Return TRUE if rect start is greater than end
 */

BOOL EmptyRect(register RectPtr rect)
{
	return ((BOOL) (rect->MinX > rect->MaxX || rect->MinY > rect->MaxY));
}

/*
 *	Return TRUE if rectangles are equal
 */

BOOL EqualRect(register RectPtr rect1, register RectPtr rect2)
{
	return ((BOOL) (rect1->MinX == rect2->MinX && rect1->MaxX == rect2->MaxX &&
					rect1->MinY == rect2->MinY && rect1->MaxY == rect2->MaxY));
}

/*
 *	Determine if given point is in rectangle
 */

BOOL PtInRect(register PointPtr pt, register RectPtr rect)
{
	return ((BOOL) (pt->x >= rect->MinX && pt->x <= rect->MaxX &&
					pt->y >= rect->MinY && pt->y <= rect->MaxY));
}

/*
 *	Draw single pixel wide rectangle
 */

void FrameRect(register RastPtr rPort, register RectPtr rect)
{
	Move(rPort, rect->MinX, rect->MinY);
	Draw(rPort, rect->MaxX, rect->MinY);
	Draw(rPort, rect->MaxX, rect->MaxY);
	Draw(rPort, rect->MinX, rect->MaxY);
	if (rect->MinY < rect->MaxY - 1)
		Draw(rPort, rect->MinX, rect->MinY + 1);
	WaitBlit();
}

/*
 *	Fill rect with FgPen
 */

void FillRect(RastPtr rPort, register RectPtr rect)
{
	if (EmptyRect(rect))
		return;
	SetDrMd(rPort, JAM1);
	BNDRYOFF(rPort);
	RectFill(rPort, rect->MinX, rect->MinY, rect->MaxX, rect->MaxY);
	WaitBlit();
}

/*
 *	Draw poly frame
 */

void FramePoly(RastPtr rPort, WORD numPts, PointPtr pts)
{
	Move(rPort, pts[0].x, pts[0].y);
	PolyDraw(rPort, numPts - 1, (WORD *) &pts[1]);
	WaitBlit();
}

/*
 *	Fill polygon with pattern
 */

void FillPoly(RastPtr rPort, WORD numPts, PointPtr pts)
{
	WORD			i, width, height;
	UWORD			*areaBuff;
	PLANEPTR		planePtr;
	struct AreaInfo	areaInfo;
	struct TmpRas	tmpRas;
	Rectangle		rect;

	if (numPts < 3)
		return;
/*
	If poly is wider than system can handle, do filled rect instead
*/
	rect.MinX = rect.MaxX = pts[0].x;
	rect.MinY = rect.MaxY = pts[0].y;
	for (i = 1; i < numPts; i++) {
		if (pts[i].x < rect.MinX)
			rect.MinX = pts[i].x;
		if (pts[i].x > rect.MaxX)
			rect.MaxX = pts[i].x;
		if (pts[i].y < rect.MinY)
			rect.MinY = pts[i].y;
		if (pts[i].y > rect.MaxY)
			rect.MaxY = pts[i].y;
	}
	width = rect.MaxX - rect.MinX + 1 + 16;		// System needs extra size
	height = rect.MaxY - rect.MinY + 1;
	if (width > MAX_BLIT_SIZE || height > MAX_BLIT_SIZE) {
		FillRect(rPort, &rect);
		return;
	}
/*
	Do fill
*/
	SetDrMd(rPort, JAM2);
	BNDRYOFF(rPort);
	if ((areaBuff = MemAlloc((numPts + 1)*5, MEMF_CLEAR)) != NULL) {
		if ((planePtr = AllocRaster(width, height)) != NULL) {
			InitArea(&areaInfo, areaBuff, numPts + 1);
			InitTmpRas(&tmpRas, planePtr, RASSIZE((LONG) width, (LONG) height));
			rPort->AreaInfo = &areaInfo;
			rPort->TmpRas = &tmpRas;
			AreaMove(rPort, pts[0].x, pts[0].y);
			for (i = 1; i < numPts; i++)
				AreaDraw(rPort, pts[i].x, pts[i].y);
			AreaEnd(rPort);
			WaitBlit();
			FreeRaster(planePtr, width, height);
			rPort->AreaInfo = NULL;
			rPort->TmpRas = NULL;
		}
		MemFree(areaBuff, (numPts + 1)*5);
	}
}
