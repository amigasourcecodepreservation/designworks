/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Font routines
 */

#include <exec/types.h>
#include <intuition/intuition.h>
#include <libraries/diskfont.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>
#include <proto/diskfont.h>

#include <string.h>

#include <Toolbox/Memory.h>
#include <Toolbox/List.h>

#include "Draw.h"
#include "Proto.h"

/*
 *	Global variables
 */

extern TextFontPtr	screenFont, smallFont;

extern Defaults	defaults;

/*
 *	Local variables and definitions
 */

typedef struct FontEntry {
	struct FontEntry	*Next;
	FontNum				OldFontNum, NewFontNum;
} FontEntry, *FontEntryPtr;

static FontEntry	*fontTranslate;		/* Pointer to first entry in table */
										/* Inited to NULL */
/*
 *	Add font entry to font table
 *	Do not add if entry for given oldFontNum already exists
 *	Return FALSE if not enough memory
 */

BOOL AddFontEntry(FontNum oldFontNum, FontNum newFontNum)
{
	register FontEntryPtr newEntry, prevEntry;

	prevEntry = NULL;
	for (newEntry = fontTranslate; newEntry; newEntry = newEntry->Next) {
		if (newEntry->OldFontNum == oldFontNum)
			return (TRUE);
		prevEntry = newEntry;
	}
	if ((newEntry = MemAlloc(sizeof(FontEntry), 0)) == NULL)
		return (FALSE);
	newEntry->Next = NULL;
	newEntry->OldFontNum = oldFontNum;
	newEntry->NewFontNum = newFontNum;
	if (prevEntry)
		prevEntry->Next = newEntry;
	else
		fontTranslate = newEntry;
	return (TRUE);
}

/*
 *	Get new font number from old font number
 *	Return FALSE if entry not found (and set to default font)
 */

BOOL GetFontEntry(FontNum oldFontNum, FontNum *newFontNum)
{
	register FontEntryPtr fontEntry;

	for (fontEntry = fontTranslate; fontEntry; fontEntry = fontEntry->Next) {
		if (fontEntry->OldFontNum == oldFontNum) {
			*newFontNum = fontEntry->NewFontNum;
			return (TRUE);
		}
	}
	*newFontNum = defaults.FontNum;
	return (FALSE);
}

/*
 *	Get font number of given table entry number
 *	Return FALSE if no such item
 */

BOOL FontTableItem(register UWORD itemNum, FontNum *oldFontNum)
{
	UWORD i;
	register FontEntryPtr fontEntry;

	fontEntry = fontTranslate;
	i = itemNum;
	while (itemNum--) {
		if (fontEntry == NULL)
			return (FALSE);
		fontEntry = fontEntry->Next;
	}
	*oldFontNum = fontEntry->OldFontNum;
	return (TRUE);
}

/*
 *	Dispose of all table entries
 */

void DisposeFontTable()
{
	register FontEntryPtr fontEntry, nextEntry;

	for (fontEntry = fontTranslate; fontEntry; fontEntry = nextEntry) {
		nextEntry = fontEntry->Next;
		MemFree(fontEntry, sizeof(FontEntry));
	}
	fontTranslate = NULL;
}
