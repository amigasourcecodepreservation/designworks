/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Create dictionary for WordMaker/ProWrite
 */

#ifdef MAC
#include <stdio.h>
#undef NULL
#include <MiscTypes.h>
#endif

#ifdef AMIGA
#include <exec/types.h>
#include <stdio.h>
typedef void	*Ptr;
#endif

/*
 *	External routines
 */

#ifdef MAC
extern UBYTE	*MemAlloc(LONG, WORD);
extern void		MemFree(UBYTE *, LONG);
#endif

#ifdef AMIGA
#define MemAlloc(a,b)		AllocMem(a,b)
#define MemFree(a,b)		FreeMem(a,b)
#define BlockMove(a,b,c)	memcpy(b,a,c)
#define MIN(a,b)			((a) < (b) ? (a) : (b))
#endif

/*
 *	Variables and definitions
 */

#define MAX_WORD_SIZE	50

#define MAGIC_NUM	0x06660138L

#define MAX_LETTERS	50

WORD numLetters;

LONG commonLoc, tableLoc, nibblePos[MAX_LETTERS*MAX_LETTERS];

UBYTE mainFileName[] = "MainWords";
UBYTE commonFileName[] = "CommonWords";

#ifdef MAC
UBYTE outFileName[] = "WordMaker Dictionary";

#define DICT_CREATOR	'WMkr';
#define DICT_TYPE		'MDic';
#endif

#ifdef AMIGA
UBYTE outFileName[] = "Main Dictionary";
#endif

typedef struct _ListItem {
	struct _ListItem	*Next, *Prev;
	UBYTE				Text[1];			/* NULL terminated text string */
} ListItem;

typedef struct {
	ListItem	*First, *Last;
} ListHead;

/*
 *	Table of characters for each nibble in compressed dictionary
 *	Nibble of 0 signifies end of word
 *	If nibble is < 15 then use first table for character
 *	If nibble is 15 then use second table with second nibble for character
 *
 *	Format of dictionary is:
 *	LONG	magicNum
 *	LONG	offset to tables
 *	NIBBLES	words of main dictionary, final zero length word denotes end
 *	NIBBLES	words of common dictionary, final zero length word denotes end
 *			Tables follow
 *	WORD	numLetters, number of letters (and other chars) used in dictionary
 *	LONG	offset to common words
 *	UBYTE	decodeTable1[16], decodeTable2[16], decodeTable3[16]
 *	BYTE	charIndex[256]
 *	LONG	numLetters*numLetters nibble indexes of word starts
 *
 *	Word entries have the following format:
 *	NIBBLE	number of characters from previous word to use
 *	NIBBLES	characters to add to word, nibble of 0 ends word
 *
 *	Each word that begins with a new first two character set is pointed to in
 *	the index, and the first nibble of these word entries is always zero
 */

#ifdef MAC
#define QUOTE		'\''
#define QUOTE_CLOSE	0xD5
#endif

#ifdef AMIGA
#define QUOTE		'\''
#define QUOTE_CLOSE	QUOTE
#endif

UBYTE decodeTable1[16], decodeTable2[16], decodeTable3[16];

/*
 *	Table of nibble values for each character
 *	If value in first table is 15 then must also put value from second table
 */

UBYTE encodeTable1[256], encodeTable2[256], encodeTable3[256];

/*
 *	Index of given character (0 .. numLetters-1) or -1 if not a valid char
 */

BYTE charIndex[256];			/* Must be signed */

/*
 *	Character table for character number (0 .. numLetters-1)
 */

UBYTE charTable[MAX_LETTERS];

/*
 *	Translation of upper to lower case characters
 */

#ifdef MAC
static UBYTE toLower[] = {
	0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0A,0x0B,0x0C,0x0D,0x0E,0x0F,
	0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x1A,0x1B,0x1C,0x1D,0x1E,0x1F,
	 ' ', '!', '"', '#', '$', '%', '&','\'', '(', ')', '*', '+', ',', '-', '.','/',
	 '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ':', ';', '<', '=', '>','?',
	 '@', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n','o',
	 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '[','\\', ']', '~','_',
	 '`', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n','o',
	 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '{', '|', '}','~',0x7F,
	 ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '	', ' ', ' ', ' ', ' ', ' ',' ',
	 ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
	 ' ', '!', '"', '#', '$', '%', '&', '\'', '(', ')', '*', '+', ',', '-', '>','?',
	 '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ':', ';', '<', '=', '>','?',
	 '@', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', ' ', ' ', ' ', ' ', 'O','O',
	 'P', 'Q', '"', '"','\'','\'', 'V', 'W', 'X', 'X', 'Z', '[', '\', ']', '^','_',
	 '`', 'a', 'b', 'c', 'd', '	', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',' ',
	 'p', ' ', ' ', ' ', ' ', 'u', 'v', 'w', 'x', 'y', 'z', '{', '|', '}', '~',' '
};
#endif

#ifdef AMIGA
static UBYTE toLower[] = {
	0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0A,0x0B,0x0C,0x0D,0x0E,0x0F,
	0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x1A,0x1B,0x1C,0x1D,0x1E,0x1F,
	 ' ', '!', '"', '#', '$', '%', '&','\'', '(', ')', '*', '+', ',', '-', '.','/',
	 '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ':', ';', '<', '=', '>','?',
	 '@', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n','o',
	 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '[','\\', ']', '~','_',
	 '`', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n','o',
	 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '{', '|', '}','~',0x7F,
	0x80,0x81,0x82,0x83,0x84,0x85,0x86,0x87,0x88,0x89,0x8A,0x8B,0x8C,0x8D,0x8E,0x8F,
	0x90,0x91,0x92,0x93,0x94,0x95,0x96,0x97,0x98,0x99,0x9A,0x9B,0x9C,0x9D,0x9E,0x9F,
	0xA0,0xA1,0xA2,0xA3,0xA4,0xA5,0xA6,0xA7,0xA8,0xA9,0xAA,0xAB,0xAC,0xAD,0xAE,0xAF,
	0xB0,0xB1,0xB2,0xB3,0xB4,0xB5,0xB6,0xB7,0xB8,0xB9,0xBA,0xBB,0xBC,0xBD,0xBE,0xBF,
	 '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�',
	 '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�',
	 '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�',
	 '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�',
};
#endif

/*
 *	Build optimal dictionary tables
 *	Note: words file MUST be in lower case
 */

void BuildTables(FILE *file)
{
	register int i, ch, best, max, table, entry;
	int freqs[256];

/*
	Get character frequencies
*/
	for (ch = 0; ch < 256; ch++)
		freqs[ch] = 0;
	while ((ch = getc(file)) != EOF) {
		ch = toLower[ch];
		if ((ch >= 0x20 && ch <= 0x7E) || (ch >= 0xA0 && ch <= 0xFF))
			freqs[ch]++;
	}
/*
	Make character table
*/
	numLetters = 0;
	for (ch = 0; ch < 256; ch++) {
		if (freqs[ch]) {
			charIndex[ch] = numLetters;
			charIndex[ch - 0x20] = numLetters;
			charTable[numLetters] = ch;
			numLetters++;
		}
		else
			charIndex[ch] = -1;
	}
#ifdef MAC
	charIndex[QUOTE_CLOSE] = charIndex['\''];
#endif
/*
	Build optimal decode tables
*/
	for (i = 0; i < 16; i++)
		decodeTable1[i] = decodeTable2[i] = decodeTable3[i] = 0;
	table = 0;
	entry = 1;			/* Skip first entry, since zero indicates end-of-word */
	do {
		max = 0;
		for (ch = 0; ch < 256; ch++) {
			if (freqs[ch] > max) {
				best = ch;
				max = freqs[ch];
			}
		}
		if (max > 0) {
			if (table == 0) {
				encodeTable1[best] = entry;
				decodeTable1[entry] = best;
			}
			else if (table == 1) {
				encodeTable1[best] = 0x0F;
				encodeTable2[best] = entry;
				decodeTable2[entry] = best;
			}
			else {
				encodeTable1[best] = encodeTable2[best] = 0x0F;
				encodeTable3[best] = entry;
				decodeTable3[entry] = best;
			}
			freqs[best] = 0;
			entry++;
			if (entry > 0x0E) {
				table++;
				entry = 0;
			}
		}
	} while (max > 0);
}

/*
 *	Compare two text strings
 *	Return -1 if first less than second, 0 if equal, +1 if first greater than second
 *	Comparison is case sensitive
 */

static WORD CompareText(s1, s2, len1, len2)
register UBYTE *s1, *s2;
register WORD len1, len2;
{
	register UBYTE ch1, ch2;
	register WORD len;

	len = MIN(len1, len2);
	while (len--) {
		if ((ch1 = *s1++) != (ch2 = *s2++)) {
			if (ch1 < ch2)
				return (-1);
			return (1);
		}
	}
	if (len1 < len2)
		return (-1);
	if (len1 > len2)
		return (1);
	return (0);
}

/*
 *	List handler routines
 */

static ListHead *CreateList()
{
	ListHead *listHead;

	listHead = (ListHead *) MemAlloc(sizeof(ListHead), 0);
	listHead->First = listHead->Last = NULL;
	return (listHead);
}

static void DisposeList(list)
ListHead *list;
{
	register ListItem *item, *nextItem;

	if (list) {
		for (item = list->First; item; item = nextItem) {
			nextItem = item->Next;
			MemFree((UBYTE *) item, (LONG) (sizeof(ListItem) + strlen(item->Text)));
		}
		MemFree((UBYTE *) list, sizeof(ListHead));
	}
}

/*
 *	Add items to list in reverse alphabetical order
 */

static BOOL AddListItem(list, text)
ListHead *list;
UBYTE *text;
{
	register WORD cmp, len;
	register ListItem *item, *prevItem, *newItem;

	len = strlen(text);
	if (list == NULL ||
		(newItem = (ListItem *) MemAlloc((LONG) (sizeof(ListItem) + len), 0)) == NULL)
		return (FALSE);
	BlockMove((Ptr) text, (Ptr) newItem->Text, (LONG) len);
	newItem->Text[len] = '\0';
	item = list->First;
	prevItem = NULL;
	while (item && (cmp = CompareText(text, item->Text, len, strlen(item->Text))) < 0) {
		prevItem = item;
		item = item->Next;
	}
	if (item && cmp == 0) {
		printf("Duplicate word: %s\n", text);
		MemFree((UBYTE *) newItem, (LONG) (sizeof(ListItem) + len));
	}
	else {
		newItem->Next = item;
		newItem->Prev = prevItem;
		if (item)
			item->Prev = newItem;
		else
			list->Last = newItem;
		if (prevItem)
			prevItem->Next = newItem;
		else
			list->First = newItem;
	}
	return (TRUE);
}

static LONG NumListItems(list)
ListHead *list;
{
	register LONG num;
	register ListItem *item;

	num = 0;
	if (list) {
		for (item = list->First; item; item = item->Next)
			num++;
	}
	return (num);
}

/*
 *	Put long value to file
 */

int fputl(l, fp)
LONG l;
FILE *fp;
{
	return (fwrite((BYTE *) &l, 4, 1, fp));
}

/*
 *	Put word value to file
 */

int fputw(w, fp)
WORD w;
FILE *fp;
{
	return (fwrite((BYTE *) &w, 2, 1, fp));
}

/*
 *	Put a single nibble to file
 */

BOOL loNibble = FALSE;		/* Which nibble we are writing */
BYTE nibbleBuff;

int fputnibble(nibble, fp)
BYTE nibble;
FILE *fp;
{
	if (loNibble) {
		nibbleBuff = (nibbleBuff & 0xF0) | (nibble & 0x0F);
		loNibble = FALSE;
		return (putc(nibbleBuff, fp));
	}
	nibbleBuff = nibble << 4;
	loNibble = TRUE;
	return ((int) nibble);
}

/*
 *	Put nibbles of encoded word starting at index to file
 *	Update nibPos
 *	Return 0, or EOF on error
 */

int fputdictword(word, index, nibPos, file)
UBYTE *word;
WORD index;
LONG *nibPos;
FILE *file;
{
	register UBYTE ch;

	if (fputnibble(index, file) == EOF)
		return (EOF);
	(*nibPos)++;
	while (ch = word[index]) {
		if (fputnibble(encodeTable1[ch], file) == EOF)
			return (EOF);
		(*nibPos)++;
		if (encodeTable1[ch] == 0x0F) {
			if (fputnibble(encodeTable2[ch], file) == EOF)
				return (EOF);
			(*nibPos)++;
			if (encodeTable2[ch] == 0x0F) {
				if (fputnibble(encodeTable3[ch], file) == EOF)
					return (EOF);
				(*nibPos)++;
			}
		}
		index++;
	}
	fputnibble(0, file);
	(*nibPos)++;
	return (0);
}

/*
 *	Main program
 */

void main()
{
	FILE *file;
	register UBYTE *word;
	register UWORD i, ch, len, index, patNum, pat1, pat2;
	LONG numWords, nibPos;
	BOOL addWord;
	ListHead *wordList, *commonList;
	ListItem *listItem;
	UBYTE newWord[256], prevWord[256];
#ifdef MAC
	FInfo fInfo;
#endif

/*
	Create word lists
*/
	if ((wordList = CreateList()) == NULL || (commonList = CreateList()) == NULL) {
		DisposeList(wordList);
		printf("Unable To create word list\n");
		return;
	}
	if ((file = fopen(mainFileName, "r")) == NULL) {
		printf("Unable to find main words file\n");
		DisposeList(wordList);
		DisposeList(commonList);
		return;
	}
/*
	Make optimal tables
*/
	printf("Building tables\n");
	BuildTables(file);
/*
	Get words and add to list, making sure they are sorted
*/
	printf("\nReading Main Words file\n");
	fseek(file, 0L, 0);
	pat1 = 0;
	while (fgets(newWord, 256, file) != NULL) {
		if (newWord[0] == '\0') {
			for (i = 1; newWord[i]; i++)
				newWord[i - 1] = newWord[i];
			newWord[i - 1] = '\0';
		}
		len = strlen(newWord);
		if (newWord[len - 1] == '\n')
			newWord[--len] = '\0';
		if (len < 2 || len > MAX_WORD_SIZE ||
			newWord[0] == QUOTE || newWord[len - 1] == QUOTE ||
			(newWord[len - 2] == QUOTE && newWord[len - 1] == 's')) {
			printf("Removing Word: %s\n", newWord);
			continue;
		}
		addWord = TRUE;
		for (i = 0; i < len; i++) {			/* Convert to lower case */
			ch = toLower[newWord[i]];
			newWord[i] = ch;
			if (charIndex[ch] == -1) {
				printf("Removing Word: %s\n", newWord);
				addWord = FALSE;
			}
		}
		if (newWord[0] != pat1) {
			printf("  %s\n", newWord);
			pat1 = newWord[0];
		}
		if (addWord && !AddListItem(wordList, newWord)) {
			printf("Unable To Add Word: %s\n", newWord);
			DisposeList(wordList);
			DisposeList(commonList);
			fclose(file);
			return;
		}
	}
	fclose(file);
	printf("\nReading Common Words file\n");
	if ((file = fopen(commonFileName, "r")) == NULL) {
		printf("Unable to find common words file\n");
		DisposeList(wordList);
		DisposeList(commonList);
		return;
	}
	while (fgets(newWord, 256, file) != NULL) {
		len = strlen(newWord);
		if (newWord[len - 1] == '\n')
			newWord[--len] = '\0';
		if (len < 2 || len > MAX_WORD_SIZE ||
			newWord[0] == QUOTE || newWord[len - 1] == QUOTE ||
			(newWord[len - 2] == QUOTE && newWord[len - 1] == 's')) {
			printf("Removing Word: %s\n", newWord);
			continue;
		}
		addWord = TRUE;
		for (i = 0; i < len; i++) {			/* Convert to lower case */
			ch = toLower[newWord[i]];
			newWord[i] = ch;
			if (charIndex[ch] == -1) {
				printf("Removing Word: %s\n", newWord);
				addWord = FALSE;
			}
		}
		if (addWord && !AddListItem(commonList, newWord)) {
			printf("Unable To Add Word: %s\n", newWord);
			DisposeList(wordList);
			DisposeList(commonList);
			fclose(file);
			return;
		}
	}
	fclose(file);
/*
	Now write out the sorted word list
*/
	if ((file = fopen(mainFileName, "w")) == NULL) {
		printf("Unable To Create Main Words File\n");
		DisposeList(wordList);
		DisposeList(commonList);
		return;
	}
	numWords = NumListItems(wordList);
	printf("\nWriting Main Words file, %ld words\n", numWords);
	for (listItem = wordList->Last; numWords--; listItem = listItem->Prev) {
		word = listItem->Text;
		if (fputs(word, file) == -1 || putc('\n', file) == EOF) {
			printf("Unable To Write Word: %s\n", word);
			DisposeList(wordList);
			DisposeList(commonList);
			fclose(file);
			return;
		}
	}
	fclose(file);
	if ((file = fopen(commonFileName, "w")) == NULL) {
		printf("Unable To Create Common Words File\n");
		DisposeList(wordList);
		DisposeList(commonList);
		return;
	}
	numWords = NumListItems(commonList);
	printf("Writing Common Words file, %ld words\n", numWords);
	for (listItem = commonList->Last; numWords--; listItem = listItem->Prev) {
		word = listItem->Text;
		if (fputs(word, file) == -1 || putc('\n', file) == EOF) {
			printf("Unable To Write Word: %s\n", word);
			DisposeList(wordList);
			DisposeList(commonList);
			fclose(file);
			return;
		}
	}
	fclose(file);
/*
	Create dictionary
*/
	if ((file = fopen(outFileName, "wb")) == NULL) {
		printf("Unable To Create Dictionary File\n");
		return;
	}
	printf("\nCreating Dictionary\n");
/*
	Write dictionary header information
*/
	fputl(MAGIC_NUM, file);
	fputl(0L, file);			/* Space for offset to tables */
	nibPos = 16;
/*
	Write main words table
*/
	printf("Saving main words\n");
	patNum = pat1 = pat2 = 0;
	prevWord[0] = '\0';
	nibblePos[patNum++] = nibPos;
	numWords = NumListItems(wordList);
	for (listItem = wordList->Last; numWords--; listItem = listItem->Prev) {
		word = listItem->Text;
		len = strlen(word);
/*
	Find commonality between this word and previous word
*/
		for (index = 0; index < 15 && word[index] == prevWord[index]; index++) ;
/*
	Check to see if this word has a new beginning
*/
		if (index < 2) {
			index = 0;
			while (word[0] != charTable[pat1] || word[1] != charTable[pat2]) {
				nibblePos[patNum++] = nibPos;
				pat2++;
				if (pat2 == numLetters) {
					pat2 = 0;
					pat1++;
				}
			}
		}
/*
	Write out word
*/
		if (fputdictword(word, index, &nibPos, file) == EOF) {
			printf("Unable To Write Word: %s\n", word);
			break;
		}
		strcpy(prevWord, word);
	}
/*
	Put zero length word at end and pad outFile to byte boundary
*/
	while (patNum < numLetters*numLetters)
		nibblePos[patNum++] = nibPos;
	fputnibble(0, file);
	fputnibble(0, file);
	nibPos += 2;
	if (nibPos & 1) {
		fputnibble(0,file);
		nibPos++;
	}
/*
	Write common words table
*/
	printf("Saving common words\n");
	prevWord[0] = '\0';
	commonLoc = nibPos/2;
	numWords = NumListItems(commonList);
	for (listItem = commonList->Last; numWords--; listItem = listItem->Prev) {
		word = listItem->Text;
		len = strlen(word);
/*
	Find commonality between this word and previous word
*/
		for (index = 0; index < 15 && word[index] == prevWord[index]; index++) ;
/*
	Write out word
*/
		if (fputdictword(word, index, &nibPos, file) == EOF) {
			printf("Unable To Write Word: %s\n", word);
			break;
		}
		strcpy(prevWord, word);
	}
/*
	Put zero length word at end and pad outFile to byte boundary
*/
	fputnibble(0, file);
	fputnibble(0, file);
	nibPos += 2;
	if (nibPos & 1) {
		fputnibble(0,file);
		nibPos++;
	}
/*
	Write out tables
*/
	printf("Writing tables\n");
	tableLoc = nibPos/2;
	fputw((WORD) numLetters, file);
	fputl((LONG) commonLoc, file);
	for (i = 0; i < 16; i++)
		fputc(decodeTable1[i], file);
	for (i = 0; i < 16; i++)
		fputc(decodeTable2[i], file);
	for (i = 0; i < 16; i++)
		fputc(decodeTable3[i], file);
	for (i = 0; i < 256; i++)
		fputc(charIndex[i], file);
	for (patNum = 0; patNum < numLetters*numLetters; patNum++)
		fputl(nibblePos[patNum], file);
/*
	Write table offset position
*/
	printf("Setting table location pointer\n");
	fseek(file, 4L, 0);
	fputl(tableLoc, file);
/*
	Finish up
*/
	printf("\nFreeing word lists\n");
	fclose(file);
	DisposeList(wordList);
	DisposeList(commonList);
#ifdef MAC
	CtoPstr((char *) outFileName);
	if ((i = GetFInfo(outFileName, 0, &fInfo)) != noErr) {
		printf("Unable To Get File Info\n");
	}
	else {
		fInfo.fdType = DICT_TYPE;
		fInfo.fdCreator = DICT_CREATOR;
		if (SetFInfo(outFileName, 0, &fInfo) != noErr)
			printf("Unable To Set File Info\n");
	}
#endif
	printf("\nDone\n");
}
