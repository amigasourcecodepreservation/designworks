/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Ruler and grid routines
 */

#include <exec/types.h>
#include <graphics/gfxmacros.h>
#include <intuition/intuition.h>

#include <proto/graphics.h>
#include <proto/layers.h>

#include <string.h>

#include <Toolbox/Graphics.h>
#include <Toolbox/Window.h>
#include <Toolbox/Utility.h>
#include <Toolbox/LocalData.h>

#include "Draw.h"
#include "Proto.h"

/*
 *	External variables
 */

extern UBYTE	gridSixteenthUnits[], gridMMUnits[];

extern Options	options;

extern TextFontPtr	smallFont;

extern Rectangle	dragRect;
extern BOOL			autoScroll;

/*
 *	Local definitions and variables
 */

#define RULER_HEIGHT	12
#define RULER_WIDTH		12

#define RULER_TOPEDGE	(RULER_HEIGHT - 1)
#define RULER_LEFTEDGE	(RULER_WIDTH - 1)

#define INCH_HEIGHT			10
#define HALF_HEIGHT			6
#define QUARTER_HEIGHT		3
#define EIGHTH_HEIGHT		2
#define SIXTEENTH_HEIGHT	1

#define InchToPoints(i)			( (LONG) (i)*72)
#define EighthToPoints(i)		( (LONG) (i)*9)
#define SixteenthToPoints(i)	(((LONG) (i)*9 + 1L)/2L)

#define PointsToInch(i)			((i) + 36)/72)
#define PointsToEighth(i)		((i) + 4)/9)
#define PointsToSixteenth(i)	(((i)*2 + 4)/9)

#define CMToPoints(i)	((100L*(i)*72 + 127L)/254L)
#define MMToPoints(i)	(( 10L*(i)*72 + 127L)/254L)

#define PointsToCM(i)	((254L*(i) + 50L*72)/(100L*72))
#define PointsToMM(i)	((254L*(i) +  5L*72)/( 10L*72))

/*
 *	Local prototypes
 */

static WORD	Round(WORD, WORD);

static void	DrawValue(RastPtr, BOOL, WORD, WORD, WORD);

static void	RulerTrack(WindowPtr, PointPtr, PointPtr, BOOL);

static void	DrawRulerIndic(WindowPtr);

/*
 *	Return horizontal and vertical grid draw position for given index
 *	(Index 0 is left edge of document)
 *	Value returned is in document coordinates
 *	Note: Grid draw locations are not the same as grid snap locations
 */

void GridDrawLoc(WindowPtr window, WORD i, WORD *x, WORD *y)
{
	LONG		unitsPerDiv;			// To force LONG calculations
	DocDataPtr	docData = GetWRefCon(window);

	switch (options.MeasureUnit) {
/*
	Centimeter ruler
*/
	case MEASURE_CM:
		unitsPerDiv = 1;				/* Units are cm */
		for (;;) {
			ScaleDocToWin(window, CMToPoints(unitsPerDiv), 0, x, y);
			if (*x >= 30)
				break;
			unitsPerDiv *= 2;
			ScaleDocToWin(window, CMToPoints(unitsPerDiv), 0, x, y);
			if (*x >= 30)
				break;
			unitsPerDiv *= 5;
		}
		*x = CMToPoints(i*unitsPerDiv) + (docData->RulerOffset.x % CMToPoints(unitsPerDiv));
		*y = CMToPoints(i*unitsPerDiv) + (docData->RulerOffset.y % CMToPoints(unitsPerDiv));
		break;
/*
	Inch ruler (and default)
*/
	case MEASURE_INCH:
	default:
		unitsPerDiv = 1;				/* Units are 1/8 inch */
		for (;;) {
			ScaleDocToWin(window, EighthToPoints(unitsPerDiv), 0, x, y);
			if (*x >= 30)
				break;
			unitsPerDiv *= 2;
		}
		*x = EighthToPoints(i*unitsPerDiv) + (docData->RulerOffset.x % EighthToPoints(unitsPerDiv));
		*y = EighthToPoints(i*unitsPerDiv) + (docData->RulerOffset.y % EighthToPoints(unitsPerDiv));
		break;
	}
}

/*
 *	Return (approximate) grid snap spacing
 */

void GetGridSpacing(DocDataPtr docData, WORD *x, WORD *y)
{
	WORD gridUnits;

	if ((docData->Flags & DOC_GRIDSNAP) == 0) {
		*x = *y = 1;
		return;
	}
	switch (options.MeasureUnit) {
	case MEASURE_CM:
		gridUnits = gridMMUnits[docData->GridUnitsIndex];
		*x = MMToPoints(gridUnits);
		*y = MMToPoints(gridUnits);
		break;
	case MEASURE_INCH:
	default:
		gridUnits = gridSixteenthUnits[docData->GridUnitsIndex];
		*x = SixteenthToPoints(gridUnits);
		*y = SixteenthToPoints(gridUnits);
		break;
	}
}

/*
 *	Round value to nearest multiple of grid
 */

static WORD Round(WORD value, WORD grid)
{
	value = (value + grid/2)/grid;
	value = value*grid;
	return (value);
}

/*
 *	Adjust position (in document coordinates) to grid
 */

void SnapToGrid(DocDataPtr docData, WORD *x, WORD *y)
{
	LONG	units, gridUnits;		// To force LONG calculations

	if ((docData->Flags & DOC_GRIDSNAP) == 0)
		return;
	*x -= docData->RulerOffset.x;
	*y -= docData->RulerOffset.y;
	switch (options.MeasureUnit) {
/*
	Centimeter ruler
*/
	case MEASURE_CM:
		gridUnits = gridMMUnits[docData->GridUnitsIndex];
		if (*x < 0)
			*x -= MMToPoints(gridUnits);
		units = Round(PointsToMM(*x), gridUnits);
		*x = MMToPoints(units);
		if (*y < 0)
			*y -= MMToPoints(gridUnits);
		units = Round(PointsToMM(*y), gridUnits);
		*y = MMToPoints(units);
		break;
/*
	Inch ruler (and default)
*/
	case MEASURE_INCH:
	default:
		gridUnits = gridSixteenthUnits[docData->GridUnitsIndex];
		if (*x < 0)
			*x -= SixteenthToPoints(gridUnits);
		units = Round(PointsToSixteenth(*x), gridUnits);
		*x = SixteenthToPoints(units);
		if (*y < 0)
			*y -= SixteenthToPoints(gridUnits);
		units = Round(PointsToSixteenth(*y), gridUnits);
		*y = SixteenthToPoints(units);
		break;
	}
	*x += docData->RulerOffset.x;
	*y += docData->RulerOffset.y;
}

/*
 *	Return width of ruler
 */

WORD RulerWidth(WindowPtr window)
{
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);

	return ((WORD) ((docData->Flags & DOC_SHOWRULER) ? RULER_WIDTH : 0));
}

/*
 *	Return height of ruler
 */

WORD RulerHeight(WindowPtr window)
{
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);

	return ((WORD) ((docData->Flags & DOC_SHOWRULER) ? RULER_HEIGHT : 0));
}

/*
 *	Display measurement value in ruler
 */

static void DrawValue(RastPtr rPort, BOOL horiz, WORD x, WORD y, WORD value)
{
	WORD i;
	TextChar numString[10];

	NumToString((LONG) value, numString);
	if (horiz) {
		Move(rPort, x + 2, y + smallFont->tf_Baseline + 1);
		Text(rPort, numString, strlen(numString));
	}
	else {
		for (i = 0; numString[i]; i++, y += smallFont->tf_YSize) {
			Move(rPort, x + 1, y + smallFont->tf_Baseline + 2);
			Text(rPort, numString + i, 1);
		}
	}
}

/*
 *	Draw rulers in window
 */

void DrawRuler(WindowPtr window)
{
	WORD				x, y, units, unitsPerDiv, unitsPerLabel;
	register RastPtr	rPort = window->RPort;
	register DocDataPtr	docData = (DocDataPtr) GetWRefCon(window);
	Rectangle			rect;

	if ((docData->Flags & DOC_SHOWRULER) == 0)
		return;
/*
	Clear the ruler area
*/
	GetWindowRect(window, &rect);
	RGBForeColor(rPort, RGBCOLOR_WHITE);
	SetDrMd(rPort, JAM1);
	SetDrPt(rPort, 0xFFFF);
	RectFill(rPort, rect.MinX, rect.MinY, rect.MaxX, rect.MinY + RULER_TOPEDGE);
	RectFill(rPort, rect.MinX, rect.MinY, rect.MinX + RULER_LEFTEDGE, rect.MaxY);
/*
	Draw ruler edges
*/
	RGBForeColor(rPort, RGBCOLOR_BLACK);
	SetFont(rPort, smallFont);
	Move(rPort, rect.MinX, rect.MinY + RULER_TOPEDGE);
	Draw(rPort, rect.MaxX, rect.MinY + RULER_TOPEDGE);
	Move(rPort, rect.MinX + RULER_LEFTEDGE, rect.MinY);
	Draw(rPort, rect.MinX + RULER_LEFTEDGE, rect.MaxY);
/*
	Draw ruler divisions
*/
	switch (options.MeasureUnit) {
/*
	Inch ruler
*/
	case MEASURE_INCH:
		unitsPerDiv = 1;				/* Units are 1/16 inch */
		for (;;) {
			ScaleDocToWin(window, SixteenthToPoints(unitsPerDiv), 0, &x, &y);
			if (x >= 9)
				break;
			unitsPerDiv *= 2;
		}
		unitsPerLabel = 16;
		for (;;) {
			ScaleDocToWin(window, SixteenthToPoints(unitsPerLabel), 0, &x, &y);
			if (x >= 36)
				break;
			unitsPerLabel *= 2;
		}
		units = PointsToSixteenth(docData->RulerOffset.x);
		units += unitsPerDiv - (units % unitsPerDiv);
		units = -units;
		for (;;) {
			DocToWindow(window, SixteenthToPoints(units) + docData->RulerOffset.x, 0,
						&x, &y);
			if (x > rect.MaxX)
				break;
			if (x >= rect.MinX - 32) {
				y = rect.MinY + RULER_TOPEDGE;
				Move(rPort, x, y);
				if (units % 16 == 0)
					y -= INCH_HEIGHT;
				else if (units % 8 == 0)
					y -= HALF_HEIGHT;
				else if (units % 4 == 0)
					y -= QUARTER_HEIGHT;
				else if (units % 2 == 0)
					y -= EIGHTH_HEIGHT;
				else
					y -= SIXTEENTH_HEIGHT;
				Draw(rPort, x, y);
				if (units % unitsPerLabel == 0)
					DrawValue(rPort, TRUE, x, rect.MinY, units/16);
			}
			units += unitsPerDiv;
		}
		units = PointsToSixteenth(docData->RulerOffset.y);
		units += unitsPerDiv - (units % unitsPerDiv);
		units = -units;
		for (;;) {
			DocToWindow(window, 0, SixteenthToPoints(units) + docData->RulerOffset.y,
						&x, &y);
			if (y > rect.MaxY)
				break;
			if (y >= rect.MinY - 32) {
				x = rect.MinX + RULER_LEFTEDGE;
				Move(rPort, x, y);
				if (units % 16 == 0)
					x -= INCH_HEIGHT;
				else if (units % 8 == 0)
					x -= HALF_HEIGHT;
				else if (units % 4 == 0)
					x -= QUARTER_HEIGHT;
				else if (units % 2 == 0)
					x -= EIGHTH_HEIGHT;
				else
					x -= SIXTEENTH_HEIGHT;
				Draw(rPort, x, y);
				if (units % unitsPerLabel == 0)
					DrawValue(rPort, FALSE, rect.MinX, y, units/16);
			}
			units += unitsPerDiv;
		}
		break;
/*
	CM ruler
*/
	case MEASURE_CM:
		unitsPerDiv = 1;				/* Units are mm */
		for (;;) {
			ScaleDocToWin(window, MMToPoints(unitsPerDiv), 0, &x, &y);
			if (x >= 5)
				break;
			unitsPerDiv *= 2;
			ScaleDocToWin(window, MMToPoints(unitsPerDiv), 0, &x, &y);
			if (x >= 5)
				break;
			unitsPerDiv *= 5;
		}
		unitsPerLabel = 10;
		for (;;) {
			ScaleDocToWin(window, MMToPoints(unitsPerLabel), 0, &x, &y);
			if (x >= 28)
				break;
			unitsPerLabel *= 2;
			ScaleDocToWin(window, MMToPoints(unitsPerLabel), 0, &x, &y);
			if (x >= 28)
				break;
			unitsPerLabel *= 5;
		}
		units = PointsToMM(docData->RulerOffset.x);
		units += unitsPerDiv - (units % unitsPerDiv);
		units = -units;
		for (;;) {
			DocToWindow(window, MMToPoints(units) + docData->RulerOffset.x, 0,
						&x, &y);
			if (x > rect.MaxX)
				break;
			if (x >= rect.MinX - 32) {
				y = rect.MinY + RULER_TOPEDGE;
				Move(rPort, x, y);
				if (units % 10 == 0)
					y -= INCH_HEIGHT;
				else if (units % 5 == 0)
					y -= HALF_HEIGHT;
				else
					y -= EIGHTH_HEIGHT;
				Draw(rPort, x, y);
				if (units % unitsPerLabel == 0)
					DrawValue(rPort, TRUE, x, rect.MinY, units/10);
			}
			units += unitsPerDiv;
		}
		units = PointsToMM(docData->RulerOffset.y);
		units += unitsPerDiv - (units % unitsPerDiv);
		units = -units;
		for (;;) {
			DocToWindow(window, 0, MMToPoints(units) + docData->RulerOffset.y,
						&x, &y);
			if (y > rect.MaxY)
				break;
			if (y >= rect.MinY - 32) {
				x = rect.MinX + RULER_LEFTEDGE;
				Move(rPort, x, y);
				if (units % 10 == 0)
					x -= INCH_HEIGHT;
				else if (units % 5 == 0)
					x -= HALF_HEIGHT;
				else
					x -= EIGHTH_HEIGHT;
				Draw(rPort, x, y);
				if (units % unitsPerLabel == 0)
					DrawValue(rPort, FALSE, rect.MinX, y, units/10);
			}
			units += unitsPerDiv;
		}
		break;
	}
/*
	Clear upper left corner (in case we rendered into it)
*/
	RGBForeColor(rPort, RGBCOLOR_WHITE);
	RectFill(rPort, rect.MinX, rect.MinY, rect.MinX + RULER_LEFTEDGE - 1,
			 rect.MinY + RULER_TOPEDGE - 1);
}

/*
 *	Draw routine for tracking ruler offset
 */

static void RulerTrack(WindowPtr window, PointPtr pt1, PointPtr pt2, BOOL change)
{
	WORD		x, y;
	RastPtr		rPort = window->RPort;
	Rectangle	rect;

	if (change) {
		GetContentRect(window, &rect);
		DocToWindow(window, pt2->x, pt2->y, &x, &y);
		SetDrPt(rPort, 0xFFFF);
		SetDrawComplement(rPort);
		Move(rPort, x, rect.MinY);
		Draw(rPort, x, rect.MaxY);
		Move(rPort, rect.MinX, y);
		Draw(rPort, rect.MaxX, y);
		ClearDrawComplement(rPort);
	}
}

/*
 *	Get new ruler offset
 */

void DoSetRuler(WindowPtr window)
{
	Point		pt;
	Rectangle	rect;
	DocDataPtr	docData = (DocDataPtr) GetWRefCon(window);

	pt.x = pt.y = 0;
	autoScroll = FALSE;
	TrackMouse(window, 0, &pt, RulerTrack, NULL);
	autoScroll = TRUE;
	GetContentRect(window, &rect);
	if (window->MouseX < rect.MinX && window->MouseY < rect.MinY)
		pt.x = pt.y = 0;
	docData->RulerOffset = pt;
	GetWindowRect(window, &rect);
	InvalRect(window, &rect);
}

/*
 *	Draw ruler indicator
 */

static void DrawRulerIndic(WindowPtr window)
{
	WORD x, y;
	RastPtr rPort = window->RPort;
	LayerPtr layer = window->WLayer;
	RegionPtr oldClip;
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
	Rectangle rect;

	if ((docData->Flags & DOC_SHOWRULER) == 0)
		return;
	oldClip = InstallClipRegion(layer, NULL);	/* Remove clipping for speed */
/*
	Set up for drawing
*/
	GetWindowRect(window, &rect);
	SetDrawComplement(rPort);
/*
	Draw ruler indicator
*/
	SetDrPt(rPort, 0xAAAA);
	DocToWindow(window, docData->RulerIndic.MinX, docData->RulerIndic.MinY, &x, &y);
	if (x >= rect.MinX + RULER_LEFTEDGE + 1 && x <= rect.MaxX) {
		Move(rPort, x, rect.MinY);
		Draw(rPort, x, rect.MinY + RULER_TOPEDGE - 1);
	}
	if (y >= rect.MinY + RULER_TOPEDGE + 1 && y <= rect.MaxY) {
		Move(rPort, rect.MinX, y);
		Draw(rPort, rect.MinX + RULER_LEFTEDGE - 1, y);
	}
/*
	If dragging, draw rectangle
*/
	if (docData->Flags & DOC_DRAGGING) {
		SetDrPt(rPort, 0x5555);			/* So as not to erase other markers */
		DocToWindow(window, docData->RulerIndic.MaxX, docData->RulerIndic.MaxY, &x, &y);
		if (x >= rect.MinX + RULER_LEFTEDGE + 1 && x <= rect.MaxX) {
			Move(rPort, x, rect.MinY);
			Draw(rPort, x, rect.MinY + RULER_TOPEDGE - 1);
		}
		if (y >= rect.MinY + RULER_TOPEDGE + 1 && y <= rect.MaxY) {
			Move(rPort, rect.MinX, y);
			Draw(rPort, rect.MinX + RULER_LEFTEDGE - 1, y);
		}
	}
/*
	Restore old clip region
*/
	ClearDrawComplement(rPort);
	(void) InstallClipRegion(layer, oldClip);
}

/*
 *	Turn on ruler indicator
 */

void RulerIndicOn(WindowPtr window)
{
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);

	if ((window->Flags & WINDOWACTIVE) == 0 ||
		(docData->Flags & DOC_RULERINDIC) != 0 ||
		docData->Scale == 0)
		return;
	docData->Flags |= DOC_RULERINDIC;
	if ((docData->Flags & DOC_SHOWRULER) == 0)
		return;
	if (docData->Flags & DOC_DRAGGING)
		docData->RulerIndic = dragRect;
	else {
		WindowToDoc(window, window->MouseX, window->MouseY, &docData->RulerIndic.MinX, &docData->RulerIndic.MinY);
		SnapToGrid(docData, &docData->RulerIndic.MinX, &docData->RulerIndic.MinY);
	}
	DrawRulerIndic(window);
}

/*
 *	Turn off ruler indicator
 */

void RulerIndicOff(WindowPtr window)
{
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);

	if (docData->Flags & DOC_RULERINDIC) {
		DrawRulerIndic(window);
		docData->Flags &= ~DOC_RULERINDIC;
	}
}

/*
 *	Update ruler indicator
 */

void UpdateRulerIndic(WindowPtr window)
{
	DocDataPtr docData = (DocDataPtr) GetWRefCon(window);
	Rectangle rect;

	if ((docData->Flags & DOC_RULERINDIC) == 0)
		return;
	if (docData->Flags & DOC_DRAGGING) {
		rect = dragRect;
		if (rect.MinX == docData->RulerIndic.MinX && rect.MinY == docData->RulerIndic.MinY &&
			rect.MaxX == docData->RulerIndic.MaxX && rect.MaxY == docData->RulerIndic.MaxY)
			return;
	}
	else {
		WindowToDoc(window, window->MouseX, window->MouseY, &rect.MinX, &rect.MinY);
		SnapToGrid(docData, &rect.MinX, &rect.MinY);
		if (rect.MinX == docData->RulerIndic.MinX && rect.MinY == docData->RulerIndic.MinY)
			return;
	}
	DrawRulerIndic(window);
	docData->RulerIndic = rect;
	DrawRulerIndic(window);
}
