/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Toolbox library
 *	Copyright (c) 1991-93 New Horizons Software, Inc.
 *
 *	Scroll List handler
 */

#include <exec/types.h>
#include <intuition/intuition.h>

#include <string.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/layers.h>
#include <proto/intuition.h>

#include <Toolbox/Globals.h>
#include <Toolbox/Graphics.h>
#include <Toolbox/Border.h>
#include <Toolbox/Gadget.h>
#include <Toolbox/ScrollList.h>
#include <Toolbox/Utility.h>

/*
 *	Local variables and definitions
 */

#define SHIFTKEYS	(IEQUALIFIER_LSHIFT | IEQUALIFIER_RSHIFT)

#define MIN(a,b)	(((a) < (b)) ? (a) : (b))

static ScrollListPtr	trackScrollList;	// Scroll list for TrackGadget call back

/*
	ScrollList flags
*/

#define SL_SELECTFLAGS	0xFF

#define SL_DRAWON		0x1000

#define IS_DRAWON(sl)		((sl)->Flags & SL_DRAWON)
#define IS_NOSELECT(sl)		(((sl)->Flags & SL_SELECTFLAGS) == SL_NOSELECT)
#define IS_SINGLESELECT(sl)	(((sl)->Flags & SL_SELECTFLAGS) == SL_SINGLESELECT)
#define IS_MULTISELECT(sl)	(((sl)->Flags & SL_SELECTFLAGS) == SL_MULTISELECT)

/*
	Item flags
*/

#define STYLE_BITS		0x07
#define DISABLE_FLAG	0x10
#define SELECT_FLAG		0x20
#define	DUMMY_FLAG		0x80	// Dummy to keep entry from starting with \0

#define IS_SELECTED(text)	((text)[0] & SELECT_FLAG)
#define IS_DISABLED(text)	((text)[0] & DISABLE_FLAG)

#define SELECT_ON(text)		((text)[0] |= SELECT_FLAG)
#define SELECT_OFF(text)	((text)[0] &= ~SELECT_FLAG)
#define DISABLE_ON(text)	((text)[0] |= DISABLE_FLAG)
#define DISABLE_OFF(text)	((text)[0] &= ~DISABLE_FLAG)

/*
 *	Local prototypes
 */

static void	DefaultDrawProc(RastPtr, TextPtr, RectPtr);
static void	DrawItem(ScrollListPtr, WORD);
static WORD	NumVisibleItems(ScrollListPtr);

static WORD	NumFullyVisibleItems(ScrollListPtr);
static WORD	ItemHeight(ScrollListPtr);

static void	ScrollToOffset(ScrollListPtr, WORD);
static void	ScrollUp(WindowPtr, WORD);
static void	ScrollDown(WindowPtr, WORD);
static void	TrackSlider(WindowPtr, WORD);
static void	AdjustToSlider(ScrollListPtr);
static void	AdjustSlider(ScrollListPtr);

static WORD MouseItem(ScrollListPtr);
static void	TrackListBox(MsgPortPtr, ScrollListPtr, UWORD, ULONG, ULONG);

static void	UpdateList(ScrollListPtr, WORD, BOOL);

static BOOL	SetScrollClip(ScrollListPtr, RegionPtr *);
static void	ResetScrollClip(ScrollListPtr, RegionPtr);

/*
 *	Default draw proc
 */

static void DefaultDrawProc(RastPtr rPort, register TextPtr text, RectPtr rect)
{
	register WORD	yPos, len;

	if (text[0] == '\0') {
		yPos = (rect->MaxY + rect->MinY)/2;
		Move(rPort, rect->MinX, yPos);
		Draw(rPort, rect->MaxX, yPos);
	}
	else {
		for (len = 0; text[len] == ' '; len++) ;
		if (len) {
			Move(rPort, rPort->cp_x + len*TextLength(rPort, " ", 1), rPort->cp_y);
			text += len;
		}
		TextInWidth(rPort, text, strlen(text), rect->MaxX - rPort->cp_x + 1, FALSE);
	}
}

/*
 *	Draw scroll list item
 */

static void DrawItem(ScrollListPtr scrollList, WORD item)
{
	WORD				i;
	PenNum				pen;
	BOOL				selected;
	register TextPtr	text;
	RastPtr				rPort;
	Rectangle			rect;

	i = item - scrollList->YOffset;
	if (i < 0 || i >= NumVisibleItems(scrollList))
		return;
	if (item < NumListItems(scrollList->List))
		text = GetListItem(scrollList->List, item);
	else
		text = NULL;
	selected = (text && IS_SELECTED(text));
/*
	Get bounds rect
*/
	GetGadgetRect(scrollList->ListBox, scrollList->Window, scrollList->Request, &rect);
	rect.MinY += i*ItemHeight(scrollList);
	rect.MaxY = rect.MinY + ItemHeight(scrollList) - 1;
/*
	Clear previous item
*/
	rPort = (scrollList->Request) ?
			scrollList->Request->ReqLayer->rp : scrollList->Window->RPort;
	pen = (selected) ? _tbPenBlack : _tbPenLight;
	SetAPen(rPort, pen);
	SetBPen(rPort, pen);
	FillRect(rPort, &rect);
/*
	Draw new item
*/
	if (text) {
		SetFont(rPort, scrollList->Font);
		SetSoftStyle(rPort, (text[0] & STYLE_BITS), 0xFF);
		pen = (selected) ? _tbPenWhite : _tbPenBlack;
		SetAPen(rPort, pen);
		Move(rPort, rect.MinX + 1 - scrollList->XOffset,
			 rect.MinY + scrollList->Font->tf_Baseline + 1);
		if (scrollList->DrawProc)
			(*(scrollList->DrawProc))(rPort, text + 1, &rect);
		else
			DefaultDrawProc(rPort, text + 1, &rect);
		SetSoftStyle(rPort, FS_NORMAL, 0xFF);
	}
}

/*
 *	Get number of items fully or partially visible in list
 */

static WORD NumVisibleItems(ScrollListPtr scrollList)
{
	WORD		ySize = scrollList->Font->tf_YSize;
	Rectangle	rect;

	GetGadgetRect(scrollList->ListBox, scrollList->Window, scrollList->Request, &rect);
	return ((rect.MaxY - rect.MinY + 1 + ySize)/(ySize + 1));
}

/*
 *	Get number of items fully visible in list
 */

static WORD NumFullyVisibleItems(ScrollListPtr scrollList)
{
	WORD		ySize = scrollList->Font->tf_YSize;
	Rectangle	rect;

	GetGadgetRect(scrollList->ListBox, scrollList->Window, scrollList->Request, &rect);
	return ((rect.MaxY - rect.MinY + 1)/(ySize + 1));
}

/*
 *	Return height of each item in viewable list
 */

static WORD ItemHeight(ScrollListPtr scrollList)
{
	return (scrollList->Font->tf_YSize + 1);
}

/*
 *	Scroll to new offset
 */

static void ScrollToOffset(ScrollListPtr scrollList, WORD offset)
{
	register WORD	i, start, end, num;
	WORD			numItems, numVis, numFullVis, maxOffset;
	GadgetPtr		listBox;
	LayerPtr		layer;
	RegionPtr		oldClip;
	Rectangle		rect;

	numItems = NumListItems(scrollList->List);
	numVis = NumVisibleItems(scrollList);
	numFullVis = NumFullyVisibleItems(scrollList);
	maxOffset = numItems - numFullVis;
	if (maxOffset < 0)
		maxOffset = 0;
	if (offset < 0)
		offset = 0;
	else if (offset > maxOffset)
		offset = maxOffset;
	if (offset == scrollList->YOffset)
		return;
	if (!IS_DRAWON(scrollList)) {
		scrollList->YOffset = offset;
		return;
	}
/*
	Scroll list and redraw items
*/
	listBox = scrollList->ListBox;
	layer = (scrollList->Request) ?
			scrollList->Request->ReqLayer : scrollList->Window->WLayer;
	GetGadgetRect(listBox, scrollList->Window, scrollList->Request, &rect);
	num = offset - scrollList->YOffset;
	if (num > numVis)
		num = numVis;
	else if (num < -numVis)
		num = -numVis;
	SetBPen(layer->rp, _tbPenLight);
	ScrollRect(layer, 0, num*ItemHeight(scrollList), rect.MinX, rect.MinY, rect.MaxX,
			   rect.MaxY);
	scrollList->YOffset = offset;
	if (num > 0) {
		start = numFullVis - num;
		end = numVis;
	}
	else {
		start = 0;
		end = -num;			// num < 0
	}
	if (SetScrollClip(scrollList, &oldClip)) {
		for (i = start; i < end; i++)
			DrawItem(scrollList, i + offset);
		ResetScrollClip(scrollList, oldClip);
	}
}

/*
 *	Scroll up
 *	Called by TrackGadget()
 */

static void ScrollUp(WindowPtr window, WORD dummy)
{
	ScrollToOffset(trackScrollList, trackScrollList->YOffset - 1);
	AdjustSlider(trackScrollList);
}

/*
 *	Scroll down
 *	Called by TrackGadget()
 */

static void ScrollDown(WindowPtr window, WORD dummy)
{
	ScrollToOffset(trackScrollList, trackScrollList->YOffset + 1);
	AdjustSlider(trackScrollList);
}

/*
 *	Track slider
 *	Called by TrackGadget()
 */

static void TrackSlider(WindowPtr window, WORD dummy)
{
	AdjustToSlider(trackScrollList);
}

/*
 *	Adjust offset to current scroll bar setting
 */

static void AdjustToSlider(ScrollListPtr scrollList)
{
	WORD		numItems, numFullVis;
	LONG		offset;
	PropInfoPtr	propInfo;

	propInfo = (PropInfoPtr) scrollList->Slider->SpecialInfo;
	numItems = NumListItems(scrollList->List);
	numFullVis = NumFullyVisibleItems(scrollList);
	if (numItems <= numFullVis)
		offset = 0;
	else {
		offset = (LONG) propInfo->VertPot*(numItems - numFullVis);
		offset = (offset + 0x7FFF)/0xFFFF;
	}
	ScrollToOffset(scrollList, offset);
}

/*
 *	Adjust scroll bar setting
 */

static void AdjustSlider(register ScrollListPtr scrollList)
{
	register WORD	offset, numItems, numVis, numFullVis;
	LONG			newPot, newBody;
	PropInfoPtr		propInfo;

	offset = scrollList->YOffset;
	numItems = NumListItems(scrollList->List);
	numFullVis = NumFullyVisibleItems(scrollList);
	numVis = MIN(numItems - offset, numFullVis);
	if (numItems > numVis) {
		newPot = (0xFFFFL*offset)/(numItems - numVis);
		newBody = 0xFFFFL*numVis/numItems;
	}
	else
		newPot = newBody = 0xFFFF;
	propInfo = (PropInfoPtr) scrollList->Slider->SpecialInfo;
	if (propInfo->VertPot != newPot || propInfo->VertBody != newBody)
		NewModifyProp(scrollList->Slider, scrollList->Window, scrollList->Request,
					  propInfo->Flags, 0, newPot, 0xFFFF, newBody, 1);
}

/*
 *	Return item number under mouse, or -1 if out of list-box bounds
 */

static WORD MouseItem(register ScrollListPtr scrollList)
{
	register WORD	item, mouseX, mouseY, numItems;
	GadgetPtr		listBox;
	RequestPtr		request;
	WindowPtr		window;
	Rectangle		rect;

	listBox = scrollList->ListBox;
	request = scrollList->Request;
	window = scrollList->Window;
/*
	Get and adjust mouse coordinates
*/
	mouseX = window->MouseX;
	mouseY = window->MouseY;
	if (request) {
		mouseX -= request->LeftEdge;
		mouseY -= request->TopEdge;
	}
/*
	Find item under mouse
*/
	GetGadgetRect(listBox, window, request, &rect);
	if (mouseX < rect.MinX || mouseX > rect.MaxX)
		return (-1);
	numItems = NumListItems(scrollList->List);
	if (numItems == 0)
		return (-1);
	if (mouseY < rect.MinY)
		item = -1;
	else if (mouseY > rect.MaxY)
		item = NumVisibleItems(scrollList);
	else
		item = (mouseY - rect.MinY)/ItemHeight(scrollList);
	item += scrollList->YOffset;
	if (item < 0)
		item = 0;
	else if (item >= numItems)
		item = numItems - 1;
	return (item);
}

/*
 *	Handle mouse down in list box
 */

static void TrackListBox(MsgPortPtr msgPort, ScrollListPtr scrollList,
						 UWORD modifier, ULONG seconds, ULONG micros)
{
	WORD		i, selItem, prevItem, startItem, firstItem, lastItem;
	BOOL		select, isSelected, addSelect;
	WindowPtr	window = scrollList->Window;

	addSelect = ((modifier & SHIFTKEYS) != 0);
/*
	Get item currently under mouse
*/
	selItem = MouseItem(scrollList);
	isSelected = SLIsSelected(scrollList, selItem);		// Returns FALSE for item -1
/*
	If shift-key over selected item in multi-select list, just de-select item
*/
	if (addSelect && IS_MULTISELECT(scrollList) && isSelected) {
		SLSelectItem(scrollList, selItem, FALSE);
		return;
	}
/*
	If not adding selection, then un-select all other items
*/
	if (!addSelect || IS_SINGLESELECT(scrollList))
		SLUnSelectAll(scrollList, selItem);
/*
	Make sure item under mouse at entry is selected
*/
	if (selItem != -1)
		SLSelectItem(scrollList, selItem, TRUE);
/*
	Drag-select new items
*/
	prevItem = startItem = selItem;			// Could be -1
	while (WaitMouseUp(msgPort, window)) {
		if ((selItem = MouseItem(scrollList)) == -1)
			continue;
/*
	If multi-select, make sure we select all items that we drag over
*/
		if (startItem == -1)
			startItem = selItem;
		if (prevItem == -1)
			prevItem = selItem;
		firstItem = lastItem = selItem;
		if (IS_MULTISELECT(scrollList)) {
			if (firstItem > startItem)
				firstItem = startItem;
			if (firstItem > prevItem)
				firstItem = prevItem;
			if (lastItem < startItem)
				lastItem = startItem;
			if (lastItem < prevItem)
				lastItem = prevItem;
		}
		for (i = firstItem; i <= lastItem; i++) {
			select = (startItem <= selItem) ?
					 (i >= startItem && i <= selItem) :
					 (i >= selItem && i <= startItem);
			SLSelectItem(scrollList, i, select);
		}
		prevItem = selItem;
/*
	Auto-scroll the list
*/
		if (selItem < scrollList->YOffset)
			ScrollUp(window, 0);
		else if (selItem >= scrollList->YOffset + NumFullyVisibleItems(scrollList))
			ScrollDown(window, 0);
	}
/*
	Check for double-click
*/
	scrollList->DoubleClick = 
		(selItem != -1 && selItem == scrollList->PrevItem &&
		 DoubleClick(scrollList->PrevSeconds, scrollList->PrevMicros, seconds, micros));
	scrollList->PrevItem = selItem;
	scrollList->PrevSeconds = seconds;
	scrollList->PrevMicros = micros;
}

/*
 *	Set layer clip rectangle, returning old clip region
 *	Return TRUE if successful
 */

static BOOL SetScrollClip(ScrollListPtr scrollList, RegionPtr *oldClip)
{
	BOOL		updating;
	RastPtr		rPort;
	LayerPtr	layer;
	RegionPtr	newClip;
	Rectangle	rect;

	GetGadgetRect(scrollList->ListBox, scrollList->Window, scrollList->Request, &rect);
	if ((newClip = NewRegion()) == NULL)
		return (FALSE);
	OrRectRegion(newClip, &rect);
	rPort = (scrollList->Request) ?
			scrollList->Request->ReqLayer->rp : scrollList->Window->RPort;
	layer = rPort->Layer;
	updating = (layer->Flags & LAYERUPDATING);
	if (updating)
		EndUpdate(layer, FALSE);
	*oldClip = InstallClipRegion(layer, newClip);
	if (updating)
		BeginUpdate(layer);
	return (TRUE);
}

/*
 *	Reset to old clip region
 */

static void ResetScrollClip(ScrollListPtr scrollList, RegionPtr oldClip)
{
	BOOL		updating;
	RastPtr		rPort;
	LayerPtr	layer;
	RegionPtr	newClip;

	rPort = (scrollList->Request) ?
			scrollList->Request->ReqLayer->rp : scrollList->Window->RPort;
	layer = rPort->Layer;
	updating = (layer->Flags & LAYERUPDATING);
	if (updating)
		EndUpdate(layer, FALSE);
	newClip = InstallClipRegion(layer, oldClip);
	if (updating)
		BeginUpdate(layer);
	DisposeRegion(newClip);
}

/*
 *	Update list when item is added or removed
 */

static void UpdateList(ScrollListPtr scrollList, WORD item, BOOL added)
{
	register WORD	topItem, bottomItem, lastItem;
	BOOL			needDraw;
	RegionPtr		oldClip;

	topItem = scrollList->YOffset;
	bottomItem = topItem + NumVisibleItems(scrollList) - 1;
	lastItem = NumListItems(scrollList->List) - 1;
	needDraw = FALSE;
	if (item < topItem) {
		if (added)
			scrollList->YOffset++;
		else if (topItem > 0)
			scrollList->YOffset--;
		else
			needDraw = TRUE;
	}
	else if (item < bottomItem && item < lastItem)
		needDraw = TRUE;
	if (needDraw)
		SLDrawList(scrollList);
	else {
		if (item == bottomItem || item >= lastItem) {
			if (SetScrollClip(scrollList, &oldClip)) {
				DrawItem(scrollList, item);
				ResetScrollClip(scrollList, oldClip);
			}
			if (item == lastItem && !added)
				DrawItem(scrollList, item + 1);		// Erase previous display location
		}
		AdjustSlider(scrollList);
	}
}

/*
 *	NewScrollList
 *	Allocate a new scroll list
 *	After calling this routine, caller should set up values for:
 *		ListBox, UpArrow, DownArrow, Slider, Window, Request, and Font
 */

ScrollListPtr NewScrollList(WORD mode)
{
	register ScrollListPtr	scrollList;

	scrollList = MemAlloc(sizeof(ScrollList), MEMF_CLEAR);
	if (scrollList == NULL)
		return (NULL);
	scrollList->List = CreateList();
	if (scrollList->List == NULL) {
		MemFree(scrollList, sizeof(ScrollList));
		return (NULL);
	}
	scrollList->Flags = (SL_SELECTFLAGS & mode) | SL_DRAWON;
	return (scrollList);
}

/*
 *	InitScrollList
 *	Initialize scroll list
 *	Assumes gadget order is: list box, up arrow, down arrow, slider
 *	Will add to window's IDCMP: GADGETDOWN, GADGETUP, RAWKEY, INTUITICKS and MOUSEBUTTONS
 */

void InitScrollList(ScrollListPtr scrollList, GadgetPtr listBox, WindowPtr window, RequestPtr request)
{
	register GadgetPtr	gadget;

	gadget = listBox;
	gadget->Flags = (gadget->Flags & ~GADGHIGHBITS) | GADGHNONE;
	gadget->Activation &= ~RELVERIFY;
	scrollList->ListBox = gadget;
	gadget = gadget->NextGadget;
	scrollList->UpArrow = gadget;
	gadget = gadget->NextGadget;
	scrollList->DownArrow = gadget;
	gadget = gadget->NextGadget;
	scrollList->Slider = gadget;
	scrollList->Window = window;
	scrollList->Request = request;
	scrollList->Font = GetFont(&_tbTextAttr);
	ModifyIDCMP(window, window->IDCMPFlags | (GADGETDOWN|GADGETUP|RAWKEY|INTUITICKS|MOUSEBUTTONS));
}

/*
 *	DisposeScrollList
 *	Free memory allocated by NewScrollList
 */

void DisposeScrollList(ScrollListPtr scrollList)
{
	if (scrollList) {
		if (scrollList->List)
			DisposeList(scrollList->List);
		if (scrollList->Font)
			CloseFont(scrollList->Font);
		MemFree(scrollList, sizeof(ScrollList));
	}
}

/*
 *	SLDoDraw
 *	Turn drawing on or off while adding to list
 */

void SLDoDraw(ScrollListPtr scrollList, BOOL drawOn)
{
	if (drawOn) {
		scrollList->Flags |= SL_DRAWON;
		SLDrawList(scrollList);
	}
	else
		scrollList->Flags &= ~SL_DRAWON;
}

/*
 *	SLDrawBorder
 *	Draw standard border around scroll list
 */

void SLDrawBorder(ScrollListPtr scrollList)
{
	WORD		offset;
	RastPtr		rPort;
	Rectangle	rect;

	rPort = (scrollList->Request) ?
			scrollList->Request->ReqLayer->rp : scrollList->Window->RPort;
	GetGadgetRect(scrollList->ListBox, scrollList->Window, scrollList->Request, &rect);
	DrawShadowBox(rPort, &rect, -1, !IS_NOSELECT(scrollList));
	GetGadgetRect(scrollList->Slider, scrollList->Window, scrollList->Request, &rect);
	offset = (((PropInfoPtr) scrollList->Slider->SpecialInfo)->Flags & PROPBORDERLESS) ?
			 -2 : -1;
	DrawShadowBox(rPort, &rect, offset, TRUE);
}

/*
 *	SLDrawItem
 *	Draw specified item in list box
 *	Should not be called when doing a Begin/EndUpdate or Begin/EndRefresh
 */

void SLDrawItem(ScrollListPtr scrollList, WORD item)
{
	RegionPtr	oldClip;

	if (item < scrollList->YOffset ||
		item >= NumVisibleItems(scrollList) + scrollList->YOffset)
		return;
/*
	Draw item
*/
	if (SetScrollClip(scrollList, &oldClip)) {
		DrawItem(scrollList, item);
		ResetScrollClip(scrollList, oldClip);
	}
}

/*
 *	SLDrawList
 *	Draw viewable list
 */

void SLDrawList(ScrollListPtr scrollList)
{
	register WORD	i, numVis;
	RegionPtr		oldClip;

	numVis = NumVisibleItems(scrollList);
	if (SetScrollClip(scrollList, &oldClip)) {
		for (i = 0; i < numVis; i++)
			DrawItem(scrollList, i + scrollList->YOffset);
		ResetScrollClip(scrollList, oldClip);
	}
	AdjustSlider(scrollList);
}

/*
 *	SLAddItem
 *	Add item to list before specified item
 */

void SLAddItem(ScrollListPtr scrollList, TextPtr text, WORD len, WORD before)
{
	TextPtr	itemText;

	if (len == 0)
		len = strlen(text);
	if ((itemText = MemAlloc(len + 2, 0)) == NULL)
		return;
	itemText[0] = DUMMY_FLAG;
	BlockMove(text, itemText + 1, len);
	itemText[len + 1] = '\0';
	(void) InsertListItem(scrollList->List, itemText, len + 1, before);
	MemFree(itemText, len + 2);
	if (IS_DRAWON(scrollList))
		UpdateList(scrollList, before, TRUE);
	scrollList->DoubleClick = FALSE;
	scrollList->PrevSeconds = scrollList->PrevMicros = 0;
}

/*
 *	SLChangeItem
 *	Change item in list
 */

void SLChangeItem(ScrollListPtr scrollList, WORD item, TextPtr text, WORD len)
{
	TextPtr	itemText;

	if (len == 0)
		len = strlen(text);
	if ((itemText = MemAlloc(len + 2, 0)) == NULL)
		return;
	itemText[0] = DUMMY_FLAG;
	BlockMove(text, itemText + 1, len);
	itemText[len + 1] = '\0';
	(void) InsertListItem(scrollList->List, itemText, len + 1, item);
	MemFree(itemText, len + 2);
	RemoveListItem(scrollList->List, item + 1);
	if (IS_DRAWON(scrollList))
		SLDrawItem(scrollList, item);
}

/*
 *	SLRemoveItem
 *	Remove item from list
 */

void SLRemoveItem(ScrollListPtr scrollList, WORD item)
{
	RemoveListItem(scrollList->List, item);
	if (scrollList->YOffset > 0 &&
		scrollList->YOffset >= NumListItems(scrollList->List))
		scrollList->YOffset--;
	if (IS_DRAWON(scrollList))
		UpdateList(scrollList, item, FALSE);
	scrollList->DoubleClick = FALSE;
	scrollList->PrevSeconds = scrollList->PrevMicros = 0;
}

/*
 *	SLRemoveAll
 *	Remove all items from list
 */

void SLRemoveAll(ScrollListPtr scrollList)
{
	register WORD	numItems;

	numItems = NumListItems(scrollList->List);
	while (numItems--)
		RemoveListItem(scrollList->List, 0);
	scrollList->YOffset = 0;
	scrollList->DoubleClick = FALSE;
	scrollList->PrevSeconds = scrollList->PrevMicros = 0;
	if (IS_DRAWON(scrollList))
		SLDrawList(scrollList);
}

/*
 *	SLNumItems
 *	Return number of items in list
 */

WORD SLNumItems(ScrollListPtr scrollList)
{
	return (NumListItems(scrollList->List));
}

/*
 *	SLGetItem
 *	Return item text in buffer
 */

void SLGetItem(ScrollListPtr scrollList, WORD item, TextPtr buff)
{
	TextPtr	text;

	text = GetListItem(scrollList->List, item);
	if (text)
		strcpy(buff, text + 1);
	else
		*buff = '\0';
}

/*
 *	SLNextSelect
 *	Return item number of next item selected after given one
 *	If item given is -1 then return first selected item
 *	Return -1 if no more items selected
 */

WORD SLNextSelect(ScrollListPtr scrollList, WORD after)
{
	register WORD			i;
	register ListItemPtr	listItem;

	for (i = 0, listItem = scrollList->List->First; i < after + 1 && listItem;
		 i++, listItem = listItem->Next) ;
	for (; listItem; i++, listItem = listItem->Next) {
		if (IS_SELECTED(listItem->Text))
			return (i);
	}
	return (-1);
}

/*
 *	SLIsSelected
 *	Return TRUE if specified item is selected
 */

BOOL SLIsSelected(ScrollListPtr scrollList, WORD item)
{
	TextPtr	text;

	if (item < 0 || item >= NumListItems(scrollList->List))
		return (FALSE);
	text = GetListItem(scrollList->List, item);
	if (IS_SELECTED(text))
		return (TRUE);
	return (FALSE);
}

/*
 *	SLSelectItem
 *	Select or deselect specified item in list
 */

void SLSelectItem(ScrollListPtr scrollList, WORD item, BOOL select)
{
	BOOL				isSelected;
	register TextPtr	text;

	if (item < 0 || item >= NumListItems(scrollList->List))
		return;
/*
	If selecting and not multi-select, then deselect everything but this item
*/
	if (select && IS_SINGLESELECT(scrollList))
		SLUnSelectAll(scrollList, item);
/*
	Select or deselect specified item
*/
	text = GetListItem(scrollList->List, item);
	if (IS_DISABLED(text))
		select = FALSE;
	isSelected = (IS_DISABLED(text)) ? FALSE : IS_SELECTED(text);
	if ((select && !isSelected) || (!select && isSelected)) {
		if (select)
			SELECT_ON(text);
		else
			SELECT_OFF(text);
		if (IS_DRAWON(scrollList))
			SLDrawItem(scrollList, item);
	}
}

/*
 *	SLUnSelectAll
 *	Unselect all but specified item in list
 */

void SLUnSelectAll(ScrollListPtr scrollList, WORD item)
{
	register WORD			i, numItems;
	register TextPtr		text;
	register ListItemPtr	listItem;
	RegionPtr				oldClip;

	numItems = NumListItems(scrollList->List);
	if (SetScrollClip(scrollList, &oldClip)) {
		for (i = 0, listItem = scrollList->List->First; i < numItems; i++, listItem = listItem->Next) {
			if (i != item && (text = listItem->Text) != NULL && IS_SELECTED(text)) {
				SELECT_OFF(text);
				if (IS_DRAWON(scrollList))
					DrawItem(scrollList, i);
			}
		}
		ResetScrollClip(scrollList, oldClip);
	}
}

/*
 *	SLSetItemStyle
 *	Set style of specified item
 */

void SLSetItemStyle(ScrollListPtr scrollList, WORD item, WORD style)
{
	register TextPtr	text;

	if (item < 0 || item >= NumListItems(scrollList->List))
		return;
	text = GetListItem(scrollList->List, item);
	text[0] &= ~STYLE_BITS;
	text[0] |= style;
	if (IS_DRAWON(scrollList))
		SLDrawItem(scrollList, item);
}

/*
 *	SLEnableItem
 *	Enable or disable item
 */

void SLEnableItem(ScrollListPtr scrollList, WORD item, BOOL enable)
{
	register TextPtr	text;

	if (item < 0 || item >= NumListItems(scrollList->List))
		return;
	text = GetListItem(scrollList->List, item);
	if (enable)
		DISABLE_OFF(text);
	else {
		DISABLE_ON(text);
		SELECT_OFF(text);
	}
	if (IS_DRAWON(scrollList))
		SLDrawItem(scrollList, item);
}

/*
 *	SLGadgetMessage
 *	Handle gadget down/up in scroll list gadget
 *	This routine will reply to the message
 */

void SLGadgetMessage(ScrollListPtr scrollList, MsgPortPtr msgPort,
					 IntuiMsgPtr intuiMsg)
{
	GadgetPtr	gadget = (GadgetPtr) intuiMsg->IAddress;
	WORD		mouseX = intuiMsg->MouseX;
	WORD		mouseY = intuiMsg->MouseY;
	ULONG		class = intuiMsg->Class;
	ULONG		seconds = intuiMsg->Seconds;
	ULONG		micros = intuiMsg->Micros;
	UWORD		modifier = intuiMsg->Qualifier;
	WindowPtr	window = intuiMsg->IDCMPWindow;
	
	ReplyMsg((MsgPtr) intuiMsg);
	if (window != scrollList->Window)
		return;
	switch (class) {
	case GADGETDOWN:
		trackScrollList = scrollList;
		if (gadget == scrollList->UpArrow)
			TrackGadget(msgPort, window, gadget, ScrollUp);
		else if (gadget == scrollList->DownArrow)
			TrackGadget(msgPort, window, gadget, ScrollDown);
		else if (gadget == scrollList->Slider)
			TrackGadget(msgPort, window, gadget, TrackSlider);
		else if (gadget == scrollList->ListBox && !IS_NOSELECT(scrollList)) {
			window->MouseX = mouseX;			// Initialize to msg values
			window->MouseY = mouseY;
			TrackListBox(msgPort, scrollList, modifier, seconds, micros);
		}
		break;
	case GADGETUP:
		if (gadget == scrollList->Slider) {
			AdjustToSlider(scrollList);
			AdjustSlider(scrollList);
		}
		break;
	}
}

/*
 *	SLCursorKey
 *	Handle cursor up/down key
 */

void SLCursorKey(ScrollListPtr scrollList, TextChar code)
{
	WORD		item, numItems;
	TextPtr		text;
	WindowPtr	window;
	RequestPtr	request;

	if (IS_NOSELECT(scrollList)) {
		trackScrollList = scrollList;
		window = scrollList->Window;
		request = scrollList->Request;
		switch (code) {
		case CURSORUP:
			HiliteGadget(scrollList->UpArrow, window, request, TRUE);
			ScrollUp(window, 0);
			HiliteGadget(scrollList->UpArrow, window, request, FALSE);
			break;
		case CURSORDOWN:
			HiliteGadget(scrollList->DownArrow, window, request, TRUE);
			ScrollDown(window, 0);
			HiliteGadget(scrollList->DownArrow, window, request, FALSE);
			break;
		}
	}
	else {
		numItems = SLNumItems(scrollList);
		item = SLNextSelect(scrollList, -1);
		if (item == -1)
			code = CURSORDOWN;			// Select first item in list
		switch (code) {
		case CURSORUP:
			while (--item >= 0) {
				text = GetListItem(scrollList->List, item);
				if (!IS_DISABLED(text))
					break;
			}
			break;
		case CURSORDOWN:
			while (++item < numItems) {
				text = GetListItem(scrollList->List, item);
				if (!IS_DISABLED(text))
					break;
			}
			break;
		}
		if (item >= 0 && item < numItems) {
			SLUnSelectAll(scrollList, item);		// In case it's a multi-select list
			SLSelectItem(scrollList, item, TRUE);
			SLAutoScroll(scrollList, item);
		}
	}
}

/*
 *	SLIsDoubleClick
 *	Return TRUE if last item selected was double-clicked on
 */

BOOL SLIsDoubleClick(ScrollListPtr scrollList)
{
	return (scrollList->DoubleClick);
}

/*
 *	SLDoubleClickItem
 *	Return item number that was double-clicked on
 *	(This can differ from the first selected item only with multi-select lists
 */

WORD SLDoubleClickItem(ScrollListPtr scrollList)
{
	return (scrollList->PrevItem);
}

/*
 *	SLAutoScroll
 *	Scroll list to make item visible
 */

void SLAutoScroll(ScrollListPtr scrollList, WORD item)
{
	WORD	offset, numFullVis;

	if (item < 0 || item >= NumListItems(scrollList->List))
		return;
	numFullVis = NumFullyVisibleItems(scrollList);
	if (item >= scrollList->YOffset && item < scrollList->YOffset + numFullVis)
		return;
	offset = (item < scrollList->YOffset) ? item : item - numFullVis + 1;
	ScrollToOffset(scrollList, offset);
	AdjustSlider(scrollList);
}

/*
 *	Scroll the list horizontally
 */

void SLHorizScroll(ScrollListPtr scrollList, WORD dx)
{
	scrollList->XOffset += dx;
	if (IS_DRAWON(scrollList))
		SLDrawList(scrollList);
}

/*
 *	Set select mode
 */

void SLSetSelectMode(ScrollListPtr scrollList, WORD mode)
{
	register TextPtr		text;
	register ListItemPtr	listItem;

/*
	First deselect all items
*/
	for (listItem = scrollList->List->First; listItem; listItem = listItem->Next) {
		if ((text = listItem->Text) != NULL)
			SELECT_OFF(text);
	}
	if (IS_DRAWON(scrollList))
		SLDrawList(scrollList);
/*
	Now set mode
*/
	scrollList->Flags = (scrollList->Flags & ~SL_SELECTFLAGS) | mode;
}

/*
 *	Set the draw proc for scroll list
 */

void SLSetDrawProc(ScrollListPtr scrollList, void (*drawProc)(RastPtr, TextPtr, RectPtr))
{
	scrollList->DrawProc = drawProc;
	if (IS_DRAWON(scrollList))
		SLDrawList(scrollList);
}
