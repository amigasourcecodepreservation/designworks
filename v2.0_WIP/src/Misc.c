/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	DesignWorks
 *	Copyright (c) 1991 New Horizons Software, Inc.
 *
 *	Utility functions
 */

#include <exec/types.h>
#include <graphics/gfxmacros.h>
#include <intuition/intuition.h>
#include <intuition/intuitionbase.h>
#include <libraries/dos.h>
#include <libraries/dosextens.h>

#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/layers.h>
#include <proto/intuition.h>
#include <proto/dos.h>

#include <string.h>

#include <Toolbox/Graphics.h>
#include <Toolbox/Border.h>
#include <Toolbox/Dialog.h>
#include <Toolbox/Window.h>
#include <Toolbox/Utility.h>
#include <Toolbox/FixMath.h>
#include <Toolbox/LocalData.h>

#include "Draw.h"
#include "Proto.h"

/*
 *	External variables
 */

extern MsgPortPtr	mainMsgPort;
extern ScreenPtr	screen;

extern WORD			numWindows;
extern WindowPtr	backWindow, windowList[];

extern WORD	drawTool;

extern BOOL	inMacro;
extern WORD	waitCount;

extern Options	options;

extern WORD intuiVersion;

extern DlgTemplPtr	dlgList[];

/*
 *	Local variables and definitions
 */

#define POINTER_EYEDROP	-1

#define DOCWIND_MSGS	(MENUVERIFY|GADGETDOWN|GADGETUP|NEWSIZE|REFRESHWINDOW|ACTIVEWINDOW|INACTIVEWINDOW)
#define BACKWIND_MSGS	(MENUVERIFY|REFRESHWINDOW|NEWPREFS)

static ULONG	oldIDCMPFlags[MAX_WINDOWS + 1];	/* Extra space for backWindow */

/*
 *	Busy pointer images
 */

static UWORD chip busyPointerData[] = {
	0x0000, 0x0000,
	0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
	0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
	0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
	0x0000, 0x0000, 0x0000, 0x0000,
	0x0000, 0x0000
};

static Pointer busyPointer = {
	14, 14, -7, -7, &busyPointerData[0]
};

static UWORD busyData1[] = {
	0x0000, 0x0000,
	0x0000, 0x0780, 0x0780, 0x1E60, 0x1FE0, 0x3E10, 0x3FF0, 0x7E08,
	0x3FF0, 0x7E08, 0x7FF8, 0xFE04, 0x7FF8, 0xFE04, 0x7FF8, 0x81FC,
	0x7FF8, 0x81FC, 0x3FF0, 0x41F8, 0x3FF0, 0x41F8, 0x1FE0, 0x21F0,
	0x0780, 0x19E0, 0x0000, 0x0780,
	0x0000, 0x0000
};

static UWORD busyData2[] = {
	0x0000, 0x0000,
	0x0000, 0x0780, 0x0780, 0x1FE0, 0x1FE0, 0x3FF0, 0x3FF0, 0x5FC8,
	0x3FF0, 0x4F88, 0x7FF8, 0x8704, 0x7FF8, 0x8204, 0x7FF8, 0x8104,
	0x7FF8, 0x8384, 0x3FF0, 0x47C8, 0x3FF0, 0x4FE8, 0x1FE0, 0x3FF0,
	0x0780, 0x1FE0, 0x0000, 0x0780,
	0x0000, 0x0000
};

static UWORD busyData3[] = {
	0x0000, 0x0000,
	0x0000, 0x0780, 0x0780, 0x19E0, 0x1FE0, 0x21F0, 0x3FF0, 0x41F8,
	0x3FF0, 0x41F8, 0x7FF8, 0x81FC, 0x7FF8, 0x81FC, 0x7FF8, 0xFE04,
	0x7FF8, 0xFE04, 0x3FF0, 0x7E08, 0x3FF0, 0x7E08, 0x1FE0, 0x3E10,
	0x0780, 0x1E60, 0x0000, 0x0780,
	0x0000, 0x0000
};

static UWORD busyData4[] = {
	0x0000, 0x0000,
	0x0000, 0x0780, 0x0780, 0x1860, 0x1FE0, 0x2010, 0x3FF0, 0x6038,
	0x3FF0, 0x7078, 0x7FF8, 0xF8FC, 0x7FF8, 0xFDFC, 0x7FF8, 0xFEFC,
	0x7FF8, 0xFC7C, 0x3FF0, 0x7838, 0x3FF0, 0x7018, 0x1FE0, 0x2010,
	0x0780, 0x1860, 0x0000, 0x0780,
	0x0000, 0x0000
};

static WORD busyNum;

static UWORD *busyData[] = {
	&busyData1[0], &busyData2[0], &busyData3[0], &busyData4[0]
};

/*
	Eyedropper pointer data
*/
static UWORD chip eyeDropPointerData[] = {
	0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0180, 0x1C00, 0x03C0,
	0x1000, 0x0FC0, 0x0800, 0x0780, 0x0C00, 0x1B00, 0x1E80, 0x3D00,
	0x3980, 0x7C00, 0x7000, 0xF800, 0x6000, 0xF000, 0x0000, 0x6000,
};

static Pointer eyeDropPointer = {
	16, 11, -3, -3, &eyeDropPointerData[0]
};

/*
 *	Local prototypes
 */

static TextPtr	GetArgValue(TextPtr, WORD *);

/*
 *	Set pointer to proper appearance for location in active window
 */

void SetPointerShape()
{
	WORD				x, y, ptrType;
	WORD				globalX, globalY;	// Need to take address of these
	register WindowPtr	window;
	Rectangle			rect;
	PointerPtr			pointer;

	window = ActiveWindow();
	if (!IsDocWindow(window))
		return;
	if (waitCount || inMacro) {
		SetStdPointer(window, POINTER_WAIT);
		return;
	}
	if (window->FirstRequest || (window->Flags & (INREQUEST | MENUSTATE))) {
		SetStdPointer(window, POINTER_ARROW);
		return;
	}
/*
	Set to appropriate tool pointer
*/
	ptrType = POINTER_ARROW;
	globalX = x = window->MouseX;
	globalY = y = window->MouseY;
	LocalToGlobal(window, &globalX, &globalY);
	GetContentRect(window, &rect);
	if (drawTool != TOOL_SELECT &&
		WhichScreen(globalX, globalY) == screen &&
		x >= rect.MinX && x <= rect.MaxX &&
		y >= rect.MinY && y <= rect.MaxY &&
		WhichLayer(&(screen->LayerInfo), x + window->LeftEdge, y + window->TopEdge)
			== window->WLayer) {
		if (drawTool == TOOL_TEXT)
			ptrType = POINTER_IBEAM;
		else if (drawTool == TOOL_EYEDROP)
			ptrType = POINTER_EYEDROP;
		else
			ptrType = POINTER_CROSS;
	}
	if (ptrType == POINTER_EYEDROP) {
		pointer = &eyeDropPointer;
		if (window->Pointer != pointer->PointerData) {
			SetPointer(window, pointer->PointerData,
					   pointer->Height, pointer->Width,
					   pointer->XOffset, pointer->YOffset);
		}
	}
	else
		SetStdPointer(window, ptrType);
}

/*
 *	Handle simple dialog and return result
 *	Return -1 if not enough memory for dialog
 */

WORD StdDialog(WORD dlgNum)
{
	WORD	item;

	BeginWait();
	item = StdAlert(dlgList[dlgNum], screen, mainMsgPort, DialogFilter);
	if (item == -1)
		Error(ERR_NO_MEM);
	EndWait();
	return (item);
}

/*
 *	Outline first button in dialog
 */

void OutlineOKButton(DialogPtr dlg)
{
	OutlineButton(GadgetItem(dlg->FirstGadget, 0), dlg, NULL, TRUE);
}

/*
 *	Enable or disable Cancel button in dialog
 */

void EnableCancelButton(DialogPtr dlg, BOOL enable)
{
	EnableGadgetItem(dlg->FirstGadget, CANCEL_BUTTON, dlg, NULL, enable);
}

/*
 *	Filter function for ModalDialog and ModalRequest
 *	Handles ALL window re-sizes and updates
 */

BOOL DialogFilter(IntuiMsgPtr intuiMsg, WORD *item)
{
	ULONG		class;
	WindowPtr	window;

	class = intuiMsg->Class;
	window = intuiMsg->IDCMPWindow;
	*item = -1;
	if ((IsDocWindow(window) && (class & DOCWIND_MSGS)) ||
		(window == backWindow && (class & BACKWIND_MSGS))) {
		DoIntuiMessage(intuiMsg);
		return (TRUE);
	}
	return (FALSE);
}

/*
 *	Set up system for lengthy operation or dialog
 */

void BeginWait()
{
	register WORD		i;
	register WindowPtr	window;

	if (waitCount++)
		return;
/*
	Modify IDCMP to send only important messages
	(Leave MENUVERIFY set so monitor task can cancel menus)
*/
	Forbid();
	for (i = 0; i < numWindows; i++) {
		window = windowList[i];
		oldIDCMPFlags[i] = window->IDCMPFlags;
		ModifyIDCMP(window, DOCWIND_MSGS);
	}
	oldIDCMPFlags[MAX_WINDOWS] = backWindow->IDCMPFlags;
	ModifyIDCMP(backWindow, BACKWIND_MSGS);
	Permit();
}

/*
 *	Reset system to normal state
 */

void EndWait()
{
	register WORD	i;

/*
	Handle BeginWait/EndWait nesting
*/
	Delay(5);
	if (--waitCount)
		return;
/*
	Enable messages
*/
	for (i = 0; i < numWindows; i++)
		ModifyIDCMP(windowList[i], oldIDCMPFlags[i]);
	ModifyIDCMP(backWindow, oldIDCMPFlags[MAX_WINDOWS]);
/*
	Set window pointer
*/
	SetPointerShape();			/* For active window */
	ToolToFront();
}

/*
 *	Set pointer for window to busy pointer
 */

void SetBusyPointer(WindowPtr window)
{
	register WORD	i;

	busyNum = 0;
	for (i = 2; i < 2*busyPointer.Height + 2; i++)
		busyPointerData[i] = busyData[busyNum][i];
	SetPointer(window, busyPointer.PointerData, busyPointer.Height,
			   busyPointer.Width, busyPointer.XOffset, busyPointer.YOffset);
}

/*
 *	Set busy pointer to next in sequence
 */

void NextBusyPointer()
{
	register WORD i;

	if (++busyNum >= 4)
		busyNum = 0;
	for (i = 2; i < 2*busyPointer.Height + 2; i++)
		busyPointerData[i] = busyData[busyNum][i];
}

/*
 *	Flash the screen and beep the speaker
 */

void ErrBeep()
{
	if (options.BeepFlash || !options.BeepSound)
		DisplayBeep(screen);
	if (options.BeepSound)
		SysBeep(5);
}

/*
 *	Beep the speaker
 */

void StdBeep()
{
	SysBeep(5);
}

/*
 *	Check string for positive numeric value
 */

BOOL CheckNumber(register TextPtr s)
{
	while (*s == ' ')
		s++;
	if (strlen(s) == 0)
		return (FALSE);
	while (*s) {
		if (*s < '0' || *s > '9')
			return (FALSE);
		s++;
	}
	return (TRUE);
}

/*
 *	Check to see if given device is mounted
 */

typedef struct DeviceList *DevListPtr;

BOOL DevMounted(TextPtr devName)
{
	WORD				len;
	register TextPtr	name;
	register DevListPtr	device;
	struct DosInfo		*dosInfo;
	struct RootNode		*rootNode;

	len = strlen(devName) - 1;
	rootNode = (struct RootNode *) DOSBase->dl_Root;
	dosInfo = (struct DosInfo *) BADDR(rootNode->rn_Info);
	Forbid();
	for (device = (DevListPtr) BADDR(dosInfo->di_DevInfo);
		 device;
		 device = (DevListPtr) BADDR(device->dl_Next)) {
		name = (TextPtr) BADDR(device->dl_Name);
		if (device->dl_Type == DLT_DEVICE && device->dl_Task &&
			CmpString(devName, name + 1, len, (WORD) *name, FALSE) == 0)
			break;
	}
	Permit();
	return ((BOOL) (device != NULL));
}

/*
 *	Check to see if given directory is assigned or mounted as a volume
 */

BOOL DirAssigned(TextPtr volName)
{
	WORD				len;
	register TextPtr	name;
	register DevListPtr	device;
	struct DosInfo		*dosInfo;
	struct RootNode		*rootNode;

	len = strlen(volName) - 1;
	rootNode = (struct RootNode *) DOSBase->dl_Root;
	dosInfo = (struct DosInfo *) BADDR(rootNode->rn_Info);
	Forbid();
	for (device = (DevListPtr) BADDR(dosInfo->di_DevInfo);
		 device;
		 device = (DevListPtr) BADDR(device->dl_Next)) {
		name = (TextPtr) BADDR(device->dl_Name);
		if ((device->dl_Type == DLT_DIRECTORY || device->dl_Type == DLT_VOLUME) &&
			CmpString(volName, name + 1, len, (WORD) *name, FALSE) == 0)
			break;
	}
	Permit();
	return ((BOOL) (device != NULL));
}

/*
 *	Convert dot value into decimal string in current measurement units
 */

void DotToText(WORD dots, WORD dpi, TextPtr text, WORD places)
{
	Fixed	inches;

	inches = FixRatio(dots, dpi);
	if (options.MeasureUnit == MEASURE_CM)
		Fix2Ascii(FixMul(inches, FixRatio(254, 100)), text, places);
	else
		Fix2Ascii(inches, text, places);
}

/*
 *	Convert decipoint value into decimal string in current measurement units
 *	Only handles values up to about 44 inches
 */

void DecipointToText(WORD num, TextPtr text, WORD places)
{
	Fixed	inches;

	inches = FixRatio(num, 720);
	if (options.MeasureUnit == MEASURE_CM)
		Fix2Ascii(FixMul(inches, FixRatio(254, 100)), text, places);
	else
		Fix2Ascii(inches, text, places);
}

/*
 *	Convert decimal string into decipoint value using current measurement units
 *	Clip to maximum value of 44 inches (less than 32,000 decipoints)
 */

WORD TextToDecipoint(TextPtr text)
{
	Fixed	num;

	num = Ascii2Fix(text);
	if (options.MeasureUnit == MEASURE_CM)
		num = FixDiv(num, FixRatio(254, 100));	/* Convert to inches */
	if (num < 0)
		num = 0;
	else if (num > Long2Fix(44L))
		num = Long2Fix(44L);
	return (FixRound(FixMul(num, Long2Fix(720L))));
}

/*
 *	Get fractional inch value from gadget buffer and return value in decipoints
 */

WORD GetFracValue(GadgetPtr gadgList, WORD item)
{
	GadgetPtr	gadget;

	gadget = GadgetItem(gadgList, item);
	return (TextToDecipoint(((StrInfoPtr) gadget->SpecialInfo)->Buffer));
}

/*
 *	Get point argument (two numeric values)
 *	Return next arg location in string, or NULL if two values not found
 */

static TextPtr GetArgValue(TextPtr arg, WORD *value)
{
	TextPtr	start;
	Fixed	fixValue;

	if (arg == NULL)
		return (NULL);
/*
	Skip initial spaces
*/
	while (*arg == ' ')
		arg++;
	start = arg;
/*
	Get value
*/
	fixValue = Ascii2Fix(arg);
	*value = FixRound(fixValue);
/*
	Skip over value and trailing spaces
*/
	if (*arg == '-')
		arg++;
	while (*arg >= '0' && *arg <= '9')
		arg++;
	if (*arg == '.')
		arg++;
	while (*arg >= '0' && *arg <= '9')
		arg++;
	while (*arg == ' ')
		arg++;
/*
	Return NULL if there was no value
*/
	if (arg == start)
		return (NULL);
	return (arg);
}

TextPtr GetArgPoint(TextPtr args, DocDataPtr docData, PointPtr pt)
{
	args = GetArgValue(args, &pt->x);
	if (args)
		args = GetArgValue(args, &pt->y);
	if (args == NULL)
		return (NULL);
	pt->x += docData->RulerOffset.x;
	pt->y += docData->RulerOffset.y;
	return (args);
}

/*
 *	Return TRUE if screen font is topaz-8 or equivalent
 */

BOOL IsTopaz8ScreenFont(ScreenPtr screen)
{
	TextFontPtr	font = screen->RastPort.Font;

	return (font->tf_YSize == 8 && font->tf_XSize == 8 &&
			(font->tf_Flags & FPF_PROPORTIONAL) == 0);
}

/*
 *	Return coordinates of center of rectangle
 */

void GetRectCenter(register RectPtr rect, register PointPtr pt)
{
	pt->x = (rect->MaxX + rect->MinX)/2;
	pt->y = (rect->MaxY + rect->MinY)/2;
}
