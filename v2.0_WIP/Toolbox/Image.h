/*
* This file is part of DesignWorks.
* Copyright (C) 1996-2018 Canux Corporation
* 
* DesignWorks is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DesignWorks is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DesignWorks.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/*
 *	Toolbox library
 *	Copyright (c) 1991 New Horizons Software
 *
 *	Image definitions
 */

#ifndef TOOLBOX_IMAGE_H
#define TOOLBOX_IMAGE_H

#ifndef INTUITION_INTUITION_H
#include <intuition/intuition.h>
#endif

#ifndef TYPEDEFS_H
#include <Typedefs.h>
#endif

/*
 *	Icon
 *	These are converted into screen-dependent images by MakeIconImage()
 */

typedef struct {
	WORD		Width, Height, Depth;
	UWORD		*ColorTable;
	PLANEPTR	Data, Mask;
} Icon, *IconPtr;

/*
 *	Standard image types
 */

#define IMAGE_ARROW_UP		0
#define IMAGE_ARROW_DOWN	1
#define IMAGE_ARROW_LEFT	2
#define IMAGE_ARROW_RIGHT	3
#define IMAGE_ICON_STOP		4
#define IMAGE_ICON_CAUTION	5
#define IMAGE_ICON_NOTE		6

/*
 *	Size of standard arrow image
 */

#define IMAGE_ARROW_WIDTH	18
#define IMAGE_ARROW_HEIGHT	10

#define ARROW_WIDTH		IMAGE_ARROW_WIDTH
#define ARROW_HEIGHT	IMAGE_ARROW_HEIGHT

/*
 *	Standard icon size
 */

#define ICON_WIDTH	32
#define ICON_HEIGHT	32

/*
 *	Types of boxes to draw around icon in MakeIconImage
 */

#define ICON_BOX_NONE		0
#define ICON_BOX_SHADOWIN	1
#define ICON_BOX_SHADOWOUT	2

/*
 *	Prototypes
 */

ImagePtr	NewImage(WORD, WORD, WORD);
void		FreeImage(ImagePtr);

void		AppendImage(ImagePtr, ImagePtr);

ImagePtr	MakeIconImage(IconPtr, WORD, WORD, WORD);

ImagePtr	GetStdImage(WORD, WORD, WORD, BOOL);

#endif
